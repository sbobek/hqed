/*
 *	   $Id: vasProlog.cpp,v 1.7.8.4 2011-09-29 09:18:04 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>

#include "../M/hType.h"
#include "../M/hTypeList.h"
#include "../M/hFunctionList.h"
#include "../M/hMultipleValue.h"

#include "../M/XTT/XTT_Row.h"
#include "../M/XTT/XTT_Cell.h"
#include "../M/XTT/XTT_Table.h"
#include "../M/XTT/XTT_State.h"
#include "../M/XTT/XTT_States.h"
#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_Attribute.h"
#include "../M/XTT/XTT_Statesgroup.h"

#include "../M/XTT/XTT_Connections.h"
#include "../M/XTT/XTT_AttributeList.h"
#include "../M/XTT/XTT_ConnectionsList.h"
#include "../M/XTT/XTT_AttributeGroups.h"
#include "../M/XTT/XTT_CallbackContainer.h"

#include "../namespaces/ns_xtt.h"
#include "../namespaces/ns_hqed.h"

#include "vasProlog.h"
// -----------------------------------------------------------------------------

/// \brief Exports the given model to SWI Prolog language format
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __msg - result message which contains na error message when exit code indicates na error result
/// \param __parts - map of the required parts
/// \param __prefix - this argument contains string that is addes before generated code
/// \param __postfix - this argument contains string that is addes after generated code
/// \param __protocolversion - indicates if the model should be formated according to protocol format
/// \return exit code:
/// \li VAS_SWI_PROLOG_SUCCES - on succes
/// \li VAS_SWI_PROLOG_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE - when device is not writable
/// \li VAS_SWI_PROLOG_ERROR - when during the generation error occurs
int vas_out_SWI_Prolog(QIODevice* __device, QString& __msg, QMap<QString, QString> __parts, QString __prefix, QString __postfix, bool __protocolversion)
{
    // Checking device accesibility
    if(__device == NULL)
    {
        __msg = "The given device is unknown";
        return VAS_SWI_PROLOG_UNKNOWN_DEVICE;
    }
    if(!__device->isWritable())
    {
        __msg = "The given device is not writable";
        return VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE;
    }

    int result = VAS_SWI_PROLOG_SUCCES;
    QTextStream devicewritter(__device);
    devicewritter << __prefix;

    // Retriving model parts
    for(int i=0;i<__parts.keys().size();++i)
    {
        QString key = __parts.keys().at(i);
        QString val = __parts[key];
        QStringList args;

        // These commands require an argument which is a list of element ID
        if((key == "tpe") || (key == "tbl") || (key == "atr") || (key == "clb"))
        {
            if(val.isEmpty())
            {
                __msg = "Missing argument for \"" + key + "\" command";
                return VAS_SWI_PROLOG_ERROR;
            }

            // Try to split the list of arguments into ids
            args = val.split(",");
        }

        // These commands do not require arguments
        if((key == "all") || (key == "tpes") || (key == "tbls") || (key == "atrs") || (key == "clbs"))
        {
            if(!val.isEmpty())
            {
                __msg = "Too many arguments for \"" + key + "\" command";
                return VAS_SWI_PROLOG_ERROR;
            }
        }

        // Retrieving model part

        // When all model is required
        if(key == "all")
            result = vas_out_SWI_Prolog(__device, __msg, 0, __prefix, __postfix, __protocolversion);

        QString outhmr;
        if(key == "tpes")
            outhmr = typeList->toProlog();
        else if(key == "tbls")
        {
            outhmr = TableList->schemasToProlog();
            outhmr += TableList->rulesToProlog();
        }
        else if(key == "atrs")
            outhmr = AttributeGroups->toProlog();
        else if(key == "clbs")
            outhmr = CallbackContainer->toProlog();
        else if(key == "tpe")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = typeList->indexOfID(id);
                if(idx == -1)
                {
                    __msg = "Can't find type ID \"" + id + "\"";
                    return VAS_SWI_PROLOG_ERROR;
                }
                outhmr = typeList->at(idx)->toProlog();
            }
        }
        else if(key == "tbl")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = TableList->IndexOfID(id);
                if(idx == -1)
                {
                    __msg = "Can't find table ID \"" + id + "\"";
                    return VAS_SWI_PROLOG_ERROR;
                }
                outhmr = TableList->Table(idx)->schemaToProlog();
                outhmr += TableList->Table(idx)->rulesToProlog();
            }
        }
        else if(key == "clb")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = CallbackContainer->getByID(id);
                if(idx == -1)
                {
                    __msg = "Can't find callback ID \"" + id + "\"";
                    return VAS_SWI_PROLOG_ERROR;
                }
                outhmr = CallbackContainer->get(idx)->toProlog();
            }
        }
        else if(key == "atr")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                XTT_Attribute* attr = AttributeGroups->indexOfAttId(id);
                if(attr == NULL)
                {
                    __msg = "Can't find attribute ID \"" + id + "\"";
                    return VAS_SWI_PROLOG_ERROR;
                }
                outhmr = attr->toProlog();
            }
        }
        devicewritter << outhmr << " ";
    }
    devicewritter << __postfix;

    return result;
}
// -----------------------------------------------------------------------------

// #brief Exports the given model to SWI Prolog language format
//
// #param __msg - reference to the variable where the result (info, errors) messages are stored
// #param __parts - The parts of the file to generation, if 0 the all parts are used
// #param __device - the output device (File, Socket, Process, ...)
// #param __prefix - this argument contains string that is addes before generated code
// #param __postfix - this argument contains string that is addes after generated code
// #param __protocolversion - indicates if the model should be formated according to protocol format
// #return exit code:
// #li VAS_SWI_PROLOG_SUCCES - on succes
// #li VAS_SWI_PROLOG_UNKNOWN_DEVICE - when device is not specified
// #li VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE - when device is not writable
// #li VAS_SWI_PROLOG_ERROR - when during the generation error occurs
int vas_out_SWI_Prolog(QIODevice* __device, QString& __msg, int __parts, QString __prefix, QString __postfix, bool __protocolversion)
{
     if(__device == NULL)
          return VAS_SWI_PROLOG_UNKNOWN_DEVICE;
     if(!__device->isWritable())
          return VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE;

     if(__parts == 0)
          __parts = VAS_HMR_MODEL_PARTS_XTT_TYPES | VAS_HMR_MODEL_PARTS_XTT_ATTRS | VAS_HMR_MODEL_PARTS_XTT_SCHMS | VAS_HMR_MODEL_PARTS_XTT_RULES | VAS_HMR_MODEL_PARTS_XTT_STATS | VAS_HMR_MODEL_PARTS_XTT_CLBCS;
     
     bool __succes = true;                             // the result of generation
     bool pv = __protocolversion;

     QString error_msgs = "";
     if((!xtt::fullVerification(error_msgs, false)) && (__protocolversion == false))      // We assume that the protocol version is always generated, even when syntax errors exists.
     {
         __succes = false;
         __msg = "HMR GENERATION ERROR(S):\n" + error_msgs;
         return VAS_SWI_PROLOG_ERROR;
     }

     QString strout = "";
     if(!pv) strout +=  "%\n";
     if(!pv) strout += "% $Id: vasProlog.cpp,v 1.7.8.4 2011-09-29 09:18:04 kkr Exp $\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "% File generated by " + hqed::hqedName() + "\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "% HeaRT case\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     Copyright (C) 2006-9 by the HeKatE Project\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     HeaRT has been develped by the HeKatE Project, \n";
     if(!pv) strout += "%     see http://hekate.ia.agh.edu.pl\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     This file is part of HeaRT.\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     HeaRT is free software: you can redistribute it and/or modify\n";
     if(!pv) strout += "%     it under the terms of the GNU General Public License as published by\n";
     if(!pv) strout += "%     the Free Software Foundation, either version 3 of the License, or\n";
     if(!pv) strout += "%     (at your option) any later version.\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     HeaRT is distributed in the hope that it will be useful,\n";
     if(!pv) strout += "%     but WITHOUT ANY WARRANTY; without even the implied warranty of\n";
     if(!pv) strout += "%     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n";
     if(!pv) strout += "%     GNU General Public License for more details.\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%     You should have received a copy of the GNU General Public License\n";
     if(!pv) strout += "%     along with HeaRT.  If not, see <http://www.gnu.org/licenses/>.\n";
     if(!pv) strout += "%\n";
     if(!pv) strout += "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
     if(!pv) strout += ":- ensure_loaded(\'heart.pl\').\n";

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_TYPES) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% TYPES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += typeList->toProlog();
     }

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_ATTRS) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%% ATTRIBUTES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += AttributeGroups->toProlog();
     }

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_SCHMS) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%% TABLE SCHEMAS DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += TableList->schemasToProlog();
     }

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_RULES) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%% RULES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += TableList->rulesToProlog();
     }

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_STATS) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%% STATES DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += States->toProlog();
     }

     if((__parts & VAS_HMR_MODEL_PARTS_XTT_CLBCS) > 0)
     {
          if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%% CALLBACKS DEFINITIONS %%%%%%%%%%%%%%%%%%%%%%%%%%\n\n";
          strout += CallbackContainer->toProlog();
     }

     if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";
     if(!pv) strout += "\n% File generated by " + hqed::hqedName();
     if(!pv) strout += "\n%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%";

     
     // When error occurs
     if(!__succes)
     {
          __msg = strout;
          return VAS_SWI_PROLOG_ERROR;
     }
     
     QTextStream out(__device);
     out << __prefix;
     out << strout;
     out << __postfix;
           
     return VAS_SWI_PROLOG_SUCCES;
}
// -----------------------------------------------------------------------------
