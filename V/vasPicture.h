 /**
 * \file	vasPicture.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	30.04.2009 
 * \version	1.0
 * \brief	This file contains declarations of functions that maps the graphical mode to picture
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef VAS_PICTURE
#define VAS_PICTURE
// -----------------------------------------------------------------------------

#include <QIODevice>
#include <QGraphicsScene>
// -----------------------------------------------------------------------------

#define VAS_PICTURE_SUCCES 0
#define VAS_PICTURE_UNKNOWN_FORMAT 1
#define VAS_PICTURE_NOT_SAVED 2
#define VAS_PICTURE_UNKNOWN_DEVICE 3
#define VAS_PICTURE_DEVICE_NOT_WRITABLE 4
// -----------------------------------------------------------------------------

/// \brief Exports the given scene to SVG file
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __scene - the scene that should be mapped
/// \return exit code:
/// \li VAS_PICTURE_SUCCES - on succes
/// \li VAS_PICTURE_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_PICTURE_DEVICE_NOT_WRITABLE - when device is not writable
int vasSVG(QIODevice* __device, QGraphicsScene* __scene);

/// \brief Exports the given scene to Graphic file
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __scene - the scene that should be mapped
/// \param __format - the file format (BMP,JPG,PNG,PPM,XBM,XPM)
/// \param __compression - the level of commpression
/// \return exit code:
/// \li VAS_PICTURE_SUCCES - on succes
/// \li VAS_PICTURE_UNKNOWN_FORMAT - when given format is not supported
/// \li VAS_PICTURE_NOT_SAVED - when export can not be done corretly
/// \li VAS_PICTURE_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_PICTURE_DEVICE_NOT_WRITABLE - when device is not writable
int vasPicture(QIODevice* __device, QGraphicsScene* __scene, QString __format, int __compression);
// -----------------------------------------------------------------------------

#endif
