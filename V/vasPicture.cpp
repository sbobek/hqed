/*
 *	   $Id: vasPicture.cpp,v 1.7 2010-01-08 19:47:51 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>

#if QT_VERSION >= 0x040300
     #include <QtSvg>
     #include <QSvgGenerator>
#endif

#include "vasPicture.h"
// -----------------------------------------------------------------------------

// #brief Exports the given scene to SVG file
//
// #param __device - the output device (File, Socket, Process, ...)
// #param __scene - the scene that should be mapped
// #return exit code:
// #li VAS_PICTURE_SUCCES - on succes
// #li VAS_PICTURE_UNKNOWN_DEVICE - when device is not specified
// #li VAS_PICTURE_DEVICE_NOT_WRITABLE - when device is not writable
int vasSVG(QIODevice* __device, QGraphicsScene* __scene)
{
     if(__device == NULL)
          return VAS_PICTURE_UNKNOWN_DEVICE;
     if(!__device->isWritable())
          return VAS_PICTURE_DEVICE_NOT_WRITABLE;

#if QT_VERSION >= 0x040300

     // Obszar sceny do zrzutu
     QRect srect;
    
     // Zapisanie wymiarow sceny
     srect.setX((int)__scene->sceneRect().x());
     srect.setY((int)__scene->sceneRect().y());
     srect.setWidth((int)__scene->sceneRect().width());
     srect.setHeight((int)__scene->sceneRect().height());
                         
     // Tworzenie pliku svg
     QPainter painter;
     QSvgGenerator* svggen = new QSvgGenerator;
     svggen->setOutputDevice(__device);
     svggen->setSize(QSize(srect.width(), srect.height()));
     
     painter.begin(svggen);
     //painter.setWindow(srect);
     __scene->render(&painter);
     painter.end();
     
#endif  

     return VAS_PICTURE_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Exports the given scene to Graphic file
//
// #param __device - the output device (File, Socket, Process, ...)
// #param __scene - the scene that should be mapped
// #param __format - the file format (BMP,JPG,PNG,PPM,XBM,XPM)
// #param __compression - the level of commpression
// #return exit code:
// #li VAS_PICTURE_SUCCES - on succes
// #li VAS_PICTURE_UNKNOWN_FORMAT - when given format is not supported
// #li VAS_PICTURE_NOT_SAVED - when export can not be done corretly
// #li VAS_PICTURE_UNKNOWN_DEVICE - when device is not specified
// #li VAS_PICTURE_DEVICE_NOT_WRITABLE - when device is not writable
int vasPicture(QIODevice* __device, QGraphicsScene* __scene, QString __format, int __compression)
{
     QStringList formats, hints, filters;
     formats << "BMP" << "JPG" << "PNG" << "PPM" << "XBM" << "XPM";
     const char* cFormats[] = {"BMP", "JPG", "PNG", "PPM", "XBM", "XPM"};
  
     int fi = formats.indexOf(__format.toUpper());
     if(fi == -1)
          return VAS_PICTURE_UNKNOWN_FORMAT;
     if(__device == NULL)
          return VAS_PICTURE_UNKNOWN_DEVICE;
     if(!__device->isWritable())
          return VAS_PICTURE_DEVICE_NOT_WRITABLE;
     
     // Obszar sceny do zrzutu
     QRect srect;
     
     // Poprawienie rozmiaru sceny, tak aby plik svg mial odpowiedni rozmiar
          
     // Zapisanie wymiarow sceny
     srect.setX((int)__scene->sceneRect().x());
     srect.setY((int)__scene->sceneRect().y());
     srect.setWidth((int)__scene->sceneRect().width());
     srect.setHeight((int)__scene->sceneRect().height());
                         
     // Tworzenie pliku graficznego
     QPainter painter;
     QPixmap* pixmap = new QPixmap(srect.width(), srect.height());
     
     painter.begin(pixmap);
     
     painter.setPen(QColor(Qt::black));
     painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
     painter.drawRect(pixmap->rect());
     
     __scene->render(&painter);
     
     painter.end();
     
     if(!pixmap->save(__device, cFormats[fi], __compression))
          return VAS_PICTURE_NOT_SAVED;
          
     return VAS_PICTURE_SUCCES;
}
// -----------------------------------------------------------------------------

