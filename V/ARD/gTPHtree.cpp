 /**
 *                                                                             *
 * @file gTPHtree.cpp                                                          *
 * Project name: HeKatE hqed                                                   *
 * $Id: gTPHtree.cpp,v 1.26.8.2 2011-09-01 19:32:10 hqeddoxy Exp $                     *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 02.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod obiektu bedacego zarzadzajacym    *
 * rysowaniem diagramu TPH                                                     *
 *                                                                             *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../C/widgets/hBaseScene.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"

#include "gTPHnode.h"
#include "gTPHtree.h"
//----------------------------------------------------------------------------

#define LEVEL_HEIGHT 20		// Odleglosc najwyzszym wezlem na danym pziomie a kolejnym poziomem
#define HORIZONTAL_MARGIN 10	// Odleglosc pomiedzy wezlami w poziomie
//----------------------------------------------------------------------------


// #brief Konstruktor klasy przyjmujacy rodzica
//
// #param XTT_Table* _p - Wska�nik do rodzica tabeli
// #return Brak zwracanych wartosci
gTPHtree::gTPHtree(ARD_PropertyList* _p) : fParent(_p)
{
     _display = NULL;
}
//----------------------------------------------------------------------------

// #brief Destruktor klasy
//
// #return Brak zwracanych wartosci
gTPHtree::~gTPHtree(void)
{
}
//----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* gTPHtree::display(void)
{
     return _display;
}
//----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void gTPHtree::setDisplay(hBaseScene* __display)
{
     _display = __display;
}
//----------------------------------------------------------------------------

// #brief Odnawia widok dla calego diagramu
//
// #return Brak zwracanych wartosci
void gTPHtree::refresh(void)
{
	setParents();
	
	int rootIndex = fParent->indexOfRoot();
	if(rootIndex > -1)
		createTree(fParent->at(rootIndex));
		
     if(_display != NULL)
     {
          // Adding elements to the scene
          for(int i=0;i<fParent->size();++i)
               if(!_display->items().contains(fParent->at(i)->graphicTPHnode()))
                    _display->addItem(fParent->at(i)->graphicTPHnode());

          // Scene resizing
          _display->AdaptSceneSize();
          _display->update();
     }
}
//----------------------------------------------------------------------------

// #brief Ustawia w kazdym wezle wskaznik do rodzica
//
// #return brak zwracanych wartosci
void gTPHtree::setParents(void)
{
	for(int i=0;i<fParent->size();++i)
		fParent->at(i)->graphicTPHnode()->setTPHparent(fParent->tphParent(fParent->at(i)));
}
//----------------------------------------------------------------------------

// #brief Ustawia zaleznosci odleglosciowe pomiedzy wezlami a ich rodzicami
//
// #param ARD_Property* _root - wezel poczatkowy
// #return brak zwracanych wartosci
void gTPHtree::createTree(ARD_Property* _root)
{
	QList<QList<ARD_Property*>*>* _properties;
	QList<QList<float>*>* _widths;
	QList<float>* _heights;
	
	_properties = new QList<QList<ARD_Property*>*>;
	_widths = new QList<QList<float>*>;
	_heights = new QList<float>;
	
	_properties->append(new QList<ARD_Property*>);
	_properties->at(0)->append(_root);
	_widths->append(new QList<float>);
	_widths->at(0)->append(_root->graphicTPHnode()->fieldArea().width());
	_heights->append(_root->graphicTPHnode()->fieldArea().height());
	
	getTreeGeometry(0, _properties, _widths, _heights);
	
	// Ustawienie root'a
	float root_xpos = (_widths->at(0)->at(0)/2)-(_root->graphicTPHnode()->fieldArea().width()/2);
	_properties->at(0)->at(0)->graphicTPHnode()->setPos(root_xpos, 0.);
	
	// Teraz wpisujemy do poszczegolnych wezlow pozycje na scenie
	for(int level=1;level<_properties->size();++level)
	{
		// Obliczanie sumy szerokosci na danym poziomie dla danego rodzica
		QString currentParent = _properties->at(level)->at(0)->TPHparent();	// Bierzacy rodzic
		int sindex = 0;											// Index startowy potomkow danego rodzica
		float nodeWidth = .0;										// Suma szerokosci potomkow danego rodzica
		for(int pc=0;pc<_properties->at(level)->size();++pc)
		{	
			nodeWidth += _widths->at(level)->at(pc);
			if((pc == (_properties->at(level)->size()-1)) || (_properties->at(level)->at(pc+1)->TPHparent() != currentParent))
			{
				float eindex = pc;
				/* if((pc == (_properties->at(level)->size()-1)) && (_properties->at(level)->at(pc)->TPHparent() == currentParent))
				{
					nodeWidth += _widths->at(level)->at(pc);
					eindex++;
				} */
					
				// Ustawianie wszystkich wezlow
				nodeWidth = (-nodeWidth)/2.;
				for(int psi=sindex;psi<=eindex;++psi)
				{
					float realHalfWidth = _properties->at(level)->at(psi)->graphicTPHnode()->fieldArea().width()/2;
					float artificialWidth = _widths->at(level)->at(psi);
					float halfArtificialWidth = artificialWidth/2.;
					float realHeight = _properties->at(level)->at(psi)->graphicTPHnode()->fieldArea().height();
					float maxHeight = _heights->at(level);
					
					_properties->at(level)->at(psi)->graphicTPHnode()->setHorizontalDistance(-(nodeWidth+halfArtificialWidth-realHalfWidth));
					_properties->at(level)->at(psi)->graphicTPHnode()->setVerticalDistance(-(LEVEL_HEIGHT+(maxHeight-realHeight)));
					
					nodeWidth += artificialWidth;
				}
				
				if(pc < (_properties->at(level)->size()-1))
					currentParent = _properties->at(level)->at(pc+1)->TPHparent();
				sindex = pc+1;
				nodeWidth = 0.;
			}
		}
	}
	
	for(int i=0;i<_properties->size();++i)
		delete _properties->at(i);
	for(int i=0;i<_widths->size();++i)
		delete _widths->at(i);
	delete _properties;
	delete _widths;
	delete _heights;
}
//----------------------------------------------------------------------------

// #brief Analizuje kolejne poziomy drzewa
//
// #param int currLevel - aktualny poziom w ktorym trzeba wyszukac wszystkich potomkow
// #return brak zwracanych wartosci
void gTPHtree::getTreeGeometry(int currLevel, QList<QList<ARD_Property*>*>* _properties, QList<QList<float>*>* _widths, QList<float>* _heights)
{
	// Wyszukiwanie potomkow wszystkich wlasciwosci na danym poziomie
	for(int i=0;i<_properties->at(currLevel)->size();++i)
	{
		// Wyszukanie potomkow
		QList<ARD_Property*>* childList = fParent->childList(_properties->at(currLevel)->at(i));
		
		// Gdy lisc
		if(childList->size() == 0)
		{
			delete childList;
			continue;
		}
		
		// Ewntualne powiekszanie list
		while(_properties->size() < (currLevel+2))
		{
			_properties->append(new QList<ARD_Property*>);
			_widths->append(new QList<float>);
			_heights->append(0.);
		}
		
		// Wpisanie znaleznionych potomkow i ich szerokosci i wyszukanie maksymalnej wysokosci
		for(int c=0;c<childList->size();++c)
		{
			_properties->at(currLevel+1)->append(childList->at(c));
			_widths->at(currLevel+1)->append(childList->at(c)->graphicTPHnode()->fieldArea().width()+HORIZONTAL_MARGIN);
			if(childList->at(c)->graphicTPHnode()->fieldArea().height() > _heights->at(currLevel+1))
				_heights->replace(currLevel+1, childList->at(c)->graphicTPHnode()->fieldArea().height());
		}
		
		delete childList;
	}
	
	// Gdy na danym poziomie nie ma juz potomkow
	if(_properties->size() < (currLevel+2))
		return;
		
	// Odpalenie funckji dla nizszych poziomow
	getTreeGeometry(currLevel+1, _properties, _widths, _heights);
	 	
	// Teraz nastepuje poprawianie tymczasowych szerokosci wezlow
	float width_sum = 0.;
	QString currentParent = _properties->at(currLevel+1)->at(0)->TPHparent();
	for(int i=0;i<_widths->at(currLevel+1)->size();++i)
	{
		width_sum += _widths->at(currLevel+1)->at(i);
		if((i == (_widths->at(currLevel+1)->size()-1)) || (_properties->at(currLevel+1)->at(i+1)->TPHparent() != currentParent))
		{
			// ---- Zapisanie wyliczonej szerokosci ----
			// Wyszukanie rodzica w liscie na wyzszym poziomie
			int pindex = -1;
			for(int l=0;l<_properties->at(currLevel)->size();++l)
				if(_properties->at(currLevel)->at(l)->id() == currentParent)
					pindex = l;
					
			// Gdy nie znajdziemy rodzica w liscie powyzej to straszny blad
			if(pindex == -1)
				QMessageBox::critical(NULL, "HQEd", "Can't find parent on the higher level.");
				
			// Zapisanie wyliczonej szerokosci, jezeli wieksza od oryginalnej
			if(width_sum > _widths->at(currLevel)->at(pindex))
				_widths->at(currLevel)->replace(pindex, width_sum);
			
			// Zapisanie nowych parametrow
			if(i < (_widths->at(currLevel+1)->size()-1))
				currentParent = _properties->at(currLevel+1)->at(i+1)->TPHparent();
			width_sum = 0.;
		}
	}
}
//----------------------------------------------------------------------------
