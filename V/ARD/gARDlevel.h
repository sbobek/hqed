 /**
 * \file	gARDlevel.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	02.01.2008 
 * \version	1.0
 * Comment:     Plik zawierajacy definicje obiektu bedacego zarzadzajacym postacia pojedynczego poziomu ARD
 * \brief	This file contains class definition that is responsible for managing of single level of ARD
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef GARDLEVELH
#define GARDLEVELH
//---------------------------------------------------------------------------

#define MARGIN_LENGTH 800			// Dlugosc linii marginesu
#define MARGINS_COUNT 2
#define TEXTS_COUNT 1
//---------------------------------------------------------------------------
#include <QObject>
class QGraphicsLineItem;
class QGraphicsTextItem;
class QGraphicsProxyWidget;
class QRectF;
class gARDbutton;
class ARD_Dependency;
class ARD_PropertyList;
class ARD_Property;
class ARD_Level;
class hBaseScene;
class gARDnode;
class gARDdependency;
//---------------------------------------------------------------------------


/**
* \class 	gARDlevel
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	02.01.2008
* \brief 	Class definition that is responsible for managing of single level of ARD
*/
class gARDlevel : public QObject
{
     Q_OBJECT

public:
     /// \brief Constructor of gARDlevel class
     gARDlevel(ARD_Level* _p, QObject *parent = 0);

     /// \brief Destructor of gARDlevel class
     virtual ~gARDlevel(void);

     /// ----------------- Methods that return values of class fields -----------------
	
     /// \brief Returns the ID of another node, and increases the old
     unsigned int nextNodeId(bool _increase = true);
	
     /// \brief Returns the next ID of dependency, and increases on the old
     unsigned int nextDependencyId(bool _increase = true);
	
     /// \brief Returns whether the level is collapsed and the unfolded
     bool expanded(void);
	
     /// \brief Returns a list of nodes
     QList<gARDnode*>* nodes(void);
	
     /// \brief Returns a list of dependencies
     QList<gARDdependency*>* dependencies(void);
	
     /// \brief Returns a pointer to the parent
     ARD_Level* parent(void);

     /// \brief Returns the level number of the given level
     int levelNo();

     /// \brief Getter of a display object
     ///
     /// \return A pointer to the displaye object
     hBaseScene* display(void);
	
     /// ----------------- Methods for setting the values ??of class fields -----------------
	
     /// \brief Sets whether the level is collapsed or the unfolded
     void setExpanded(bool);

     /// \brief Setter of a display object
     ///
     /// \param __display - a pointer to the display object
     /// \return no values return
     void setDisplay(hBaseScene* __display);
	
     /// -------------------------- Operational Methods ---------------------------
	
     /// \brief Clears the the contents of the container
     void flush(void);
	
     /// \brief Returns the index of node with the ID
     int indexOfNodeID(unsigned int);
	
     /// \brief Returns the index of dependencies of the parameters the same as the Input
     int indexOfDependencyInOut(ARD_Dependency*);
	
     /// \brief Returns ondex of dependencies of the ends
     int indexOfDependencyInOut(QString, QString);
	
     /// \brief Returns the index who has a parent node with the specified ID
     int indexOfParentID(QString);
	
     /// \brief Returns a list of dependencies with given ends
     QList<gARDdependency*>* indexOfinout(QString, QString);
	
     /// \brief Returns a pointer to a site who has a parent with the specified ID
     gARDnode* indexOfParentIDptr(QString);
	
     /// \brief Searches for nodes of roots of a tree, diagram
     QList<gARDnode*>* rootsNodes(void);
	
     /// \brief Returns a list of descendants of node
     QList<gARDnode*>* childsList(gARDnode* _parentNode, bool skipParentNode = true);
	
     /// \brief Recovers the entire diagram
     void refresh(void);
	
     /// \brief Removes the connection associated with the property
     void removeDependencies(ARD_Property* _input = NULL);
	
     /// \brief Deletes a connection to the corresponding parent
     void removeDependencies(ARD_Dependency*);
	
     /// \brief Refreshing connections
     void repaintDependencies(ARD_Property* _input = NULL);
	
     /// \brief Sets the pointer in each node to the parent
     void setParents(void);
	
     /// \brief Checks if the Input properties have a common parent
     int hasCommonParent(QList<gARDnode*>*, QList<gARDnode*>*);
	
     /// \brief Sets the dependence between the distances and between nodes and their parents
     void createTree(QList<gARDnode*>*);
	
     /// \brief Sets the dependencies between the distances between nodes and their parents
     void getTreeGeometry(int, QList<gARDnode*>*, QList<QList<gARDnode*>*>*, QList<QList<float>*>*, QList<float>*);
	
     /// \brief Arranges components level
     void placeObjects(void);
	
     /// \brief Returns the rectangular area occupied by the level
     QRectF levelArea(void);
	
     /// \brief Moves the stage area on a set value
     void moveLevelBy(float, float);

public slots:

     /// \brief Removes all the levels below the given level.
     /// This slot is by default connected to the delete button.
     void removeLevelsBelow();

     void toggleExpaded()
     { setExpanded( ! fExpanded);}

protected:


private:
     
     unsigned int fNextNodeID;                      ///< Next node identifier
     unsigned int fNextDependencyID;                ///< Next identifier of dependence
     ARD_Level* fParent;                            ///< Parent
     hBaseScene* _display;                          ///< The object to display a tree
     QList<gARDnode*>* fNodes;                      ///< List of nodes
     QList<gARDdependency*>* fDependencies;         ///< List of dependencies
     QGraphicsLineItem* fMargins[MARGINS_COUNT];    ///< Lines of area level
     QGraphicsTextItem* fTexts[TEXTS_COUNT];        ///< Level title
     gARDbutton* fExpander;                         ///< Button for winding and unwinding of levels
     gARDbutton* fDeleter;                          ///< Button that deletes all the level below the given level.
     bool fExpanded;                                ///< Specifies whether the level is expanded (true) or collapsed (false)
};
//---------------------------------------------------------------------------
#endif
