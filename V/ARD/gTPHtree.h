 /**
 * \file	gTPHtree.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	02.01.2008
 * \version	1.0
 * \brief	This file contains class definition that manages character of the TPH diagram
 * \note This file is a part of HQEd. It has been developed within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GTPHTREEH
#define GTPHTREEH
//---------------------------------------------------------------------------

class ARD_PropertyList;
class ARD_Property;
class hBaseScene;
class gTPHnode;
//---------------------------------------------------------------------------


/**
* \class 	gTPHtree
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	02.01.2008
* \brief 	Class definition that managers the character of TPH diagram
*/
class gTPHtree
{
private:

     ARD_PropertyList* fParent;    ///< List of nodes
     hBaseScene* _display;         ///< The object to display a tree

public:

     /// \brief Default constructor
     gTPHtree(ARD_PropertyList*);

     /// \brief Destructor
     ~gTPHtree(void);
	
     /// \brief Refreshes all the diagram
     ///
     /// \return no values return
     void refresh(void);
	
     /// \brief Finds each node parent
     ///
     /// \return no values return
	void setParents(void);
	
     /// \brief Creates the tree geometry (distance, rects, etc.)
     ///
     /// \param __root - pointer to the root node
     /// \return no values return
     void createTree(ARD_Property* __root);
	
     /// \brief Analise geometry of each tree level
     ///
     /// \return no values return
     void getTreeGeometry(int, QList<QList<ARD_Property*>*>*, QList<QList<float>*>*, QList<float>*);

     /// \brief Getter of a display object
     ///
     /// \return A pointer to the displaye object
     hBaseScene* display(void);

     /// \brief Setter of a display object
     ///
     /// \param __display - a pointer to the display object
     /// \return no values return
     void setDisplay(hBaseScene* __display);
};
//---------------------------------------------------------------------------
#endif
