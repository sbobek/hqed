/**
* \file     ardSplitEditorWire.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditorWire definition of
* a QGraphicsItem resembling a wire (or an arrow) that represents a Dependency.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/

#ifndef ARDSPLITEDITORWIRE_H
#define ARDSPLITEDITORWIRE_H
//-------------------------------------------------------------------------------------------

#include <QGraphicsLineItem>
#include <QSemaphore>
//-------------------------------------------------------------------------------------------

class ardSplitEditorNode;
class ARD_Dependency;
//-------------------------------------------------------------------------------------------


/**
* \class 	ardSplitEditorWire
* \author	Piotr Held
* \date 	15.03.2011
* \brief 	Class definition of a QGraphicsItem resembling a wire (or an arrow) that represents a Dependency.
*/
class ardSplitEditorWire : public QGraphicsLineItem
{
public:

     /// \brief Holds the Type of the graphics object.
     enum { Type = UserType + 4 };

     
     /// \brief Default constructor, accept ardSplitEditorNode*.
     ardSplitEditorWire(ardSplitEditorNode *startItem, ardSplitEditorNode *endItem,
                        QGraphicsItem *parent = 0, QGraphicsScene *scene = 0);

     /// \brief Overloaded type function.
     /// \return The value of the Type enum.
     int type() const
     { return Type; }

     /// \brief Overloaded function, returns the boundingRect() of the Wire.
     /// \return The boundingRect of the Wire.
     QRectF boundingRect() const;


     /// \brief Returns the shape of the Wire taking into account the arrow Head.
     /// \return The QPainterPath representing the shape of the Wire.  
     QPainterPath shape() const;


     /// \brief Returns a pointer to the Start Item.
     /// \return ardSplitEditorNode* - Pointer to the Start Item.
     ardSplitEditorNode *startItem() const
     { return myStartItem; }


     /// \brief Returns a pointer to the End Item.
     /// \return ardSplitEditorNode* - Pointer to the End Item.
     ardSplitEditorNode *endItem() const
     { return myEndItem; }

     /// \brief Updates the position of the line()
     void updatePosition();

protected:

     /// \brief Peforms a paint event.
     /// This method performs a paint and updates the line of this QGraphicsLineItem.
     /// \param painter - pointer to QPainter on which this should be painted.
     /// \param option, widget - not used parameters.
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option,
                QWidget *widget = 0);

private:

     ardSplitEditorNode *myStartItem;   ///< Pointer to the Node that is the beginning of the Wire.
     ardSplitEditorNode *myEndItem;     ///< Pointer to the Node that is the end of the Wire.
     QColor myColor;                    ///< Color of the brush used to paint the Wire.
     QPen myPen;                        ///< Pen used when Wire is not Seleceted.
     QPen mySelectedPen;                ///< Pen used when Wire is Selected.
     QPolygonF arrowHead;               ///< The Polygon that represents the Head of the Wire.
     qreal arrowSize;                   ///< Represents the size of the arrowHead - set ARD Font Height.
};
//-------------------------------------------------------------------------------------------

#endif // ARDSPLITEDITORWIRE_H
