 /**
 *                                                                             *
 * @file gARDlevel.cpp                                                         *
 * Project name: HeKatE hqed                                                   *
 * $Id: gARDlevel.cpp,v 1.27.4.5.2.5 2011-09-01 20:51:02 hqeddoxy Exp $              *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 02.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod obiektu bedacego zarzadzajacym    *
 * rysowaniem jednego z poziomow diagramu ARD                                  *
 *                                                                             *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QGraphicsLineItem>

#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Dependency.h"
#include "../../M/ARD/ARD_DependencyList.h"

#include "gARDnode.h"
#include "gARDlevel.h"
#include "gARDbutton.h"
#include "gARDlevelList.h"
#include "gARDdependency.h"
//----------------------------------------------------------------------------

#define TOP_BOTTOM_EDGE_WIDTH 25		// Margines z gory i z dolu
#define LEFT_RIGHT_EDGE_WIDTH 100		// Margines z lewej i z prawej
#define LEVEL_HEIGHT 10				// Odleglosc najwyzszym wezlem na danym pziomie a kolejnym poziomem
#define HORIZONTAL_MARGIN 20			// Odleglosc pomiedzy wezlami w poziomie
//----------------------------------------------------------------------------


// #brief Konstruktor klasy przyjmujacy rodzica
//
// #param XTT_Table* _p - Wska�nik do rodzica tabeli
// #return Brak zwracanych wartosci
gARDlevel::gARDlevel(ARD_Level* _p,QObject* parent) : QObject(parent),fParent(_p)
{
     _display = NULL;
	fNodes = new QList<gARDnode*>;
	fDependencies = new QList<gARDdependency*>;

	fNextNodeID = 1;
	fNextDependencyID = 1;
	
	QPen pen(Qt::SolidLine);
	pen.setWidth(1);
	pen.setColor(Qt::black);
	
	for(int i=0;i<MARGINS_COUNT;i++)
	{
		fMargins[i] = new QGraphicsLineItem(0., 0., MARGIN_LENGTH, 0.);
		fMargins[i]->setPen(pen);
	}
	
	for(int i=0;i<TEXTS_COUNT;i++)
	{
		fTexts[i] = new QGraphicsTextItem("");
		fTexts[i]->setFont(Settings->ardARDdiagramFont());
		fTexts[i]->setTextWidth(LEFT_RIGHT_EDGE_WIDTH);
	}
     // Setting up the Expander Button.
	fExpanded = true;
     fExpander = new gARDbutton(this);
     fExpander->setPictureHoverStr(QString(":/all_images/images/pa.png"));
     fExpander->setPictureHoverStrExp(QString(":/all_images/images/ma.png"));
     fExpander->setPictureNonHoverStr(QString(":/all_images/images/pu.png"));
     fExpander->setPictureNonHoverStrExp(QString(":/all_images/images/mu.png"));
     fExpander->updateCurrentPicStr();
     connect(fExpander,SIGNAL(clicked()),this,SLOT(toggleExpaded()));

     // Setting up the Delete Button.
     fDeleter = new gARDbutton(this);
     fDeleter->setPictureHoverStr(QString(":/all_images/images/DeleteHover.png"));
     fDeleter->setPictureHoverStrExp(QString(":/all_images/images/DeleteHover.png"));
     fDeleter->setPictureNonHoverStr(QString(":/all_images/images/Delete.png"));
     fDeleter->setPictureNonHoverStrExp(QString(":/all_images/images/Delete.png"));
     fDeleter->updateCurrentPicStr();
     connect(fDeleter,SIGNAL(clicked()),this,SLOT(removeLevelsBelow()));
}
//----------------------------------------------------------------------------

// #brief Destruktor klasy
//
// #return Brak zwracanych wartosci
gARDlevel::~gARDlevel(void)
{
	for(;fNodes->size()>0;)
		delete fNodes->takeAt(0);
	for(;fDependencies->size()>0;)
		delete fDependencies->takeAt(0);
		
	delete fNodes;
	delete fDependencies;
	
	for(int i=0;i<MARGINS_COUNT;i++)
		delete fMargins[i];
	for(int i=0;i<TEXTS_COUNT;i++)
		delete fTexts[i];
		
	delete fExpander;
     delete fDeleter;
}
//----------------------------------------------------------------------------

// #brief Oproznia zawartosc pojemnika
//
// #return Brak zwracanych wartosci
void gARDlevel::flush(void)
{
	for(;fNodes->size()>0;)
		delete fNodes->takeAt(0);
	for(;fDependencies->size()>0;)
		delete fDependencies->takeAt(0);
}
//----------------------------------------------------------------------------

// #brief Zwraca kolejny identyfikator wezla, i opconalnie zwieksza go o jeden
//
// #param Xbool _increase - okresla czy nalezy zwiekszyc wartosc kolejnego identyfikatora wezla
// #return brak zwracanych wartosci
unsigned int gARDlevel::nextNodeId(bool _increase)
{
	unsigned int res = fNextNodeID;
	
	if(_increase)
		fNextNodeID++;
		
	return res;
}
//----------------------------------------------------------------------------

// #brief Zwraca kolejny identyfikator zaleznosci, i opconalnie zwieksza go o jeden
//
// #param bool _increase - okresla czy nalezy zwiekszyc wartosc kolejnego identyfikatora zaleznosci
// #return brak zwracanych wartosci
unsigned int gARDlevel::nextDependencyId(bool _increase)
{
	unsigned int res = fNextDependencyID;
	
	if(_increase)
		fNextDependencyID++;
		
	return res;
}
//----------------------------------------------------------------------------

// #brief Zwraca czy poziom jest zwiniety czy rozwiniety
//
// #return zwraca czy poziom jest zwiniety czy rozwiniety
bool gARDlevel::expanded(void)
{
	return fExpanded;
}
//----------------------------------------------------------------------------

// #brief Ustawia czy poziom jest zwiniety czy rozwiniety
//
// #param bool _exp - nowa wartosc statusu
// #return brak zwracanych wartosci
void gARDlevel::setExpanded(bool _exp)
{
	fExpanded = _exp;
	refresh();
     ardLevelList->gLevelList()->alignLevels(0, true);
}
//----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* gARDlevel::display(void)
{
     return _display;
}
// -----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void gARDlevel::setDisplay(hBaseScene* __display)
{
     _display = __display;
}
//----------------------------------------------------------------------------

// #brief Zwraca liste wezlow
//
// #return wskaznik do listy wezlow
QList<gARDnode*>* gARDlevel::nodes(void)
{
	return fNodes;
}
//----------------------------------------------------------------------------

// #brief Zwraca liste zalzenosci
//
// #return wskaznik do listy zalzenosci
QList<gARDdependency*>* gARDlevel::dependencies(void)
{
	return fDependencies;
}
//----------------------------------------------------------------------------

// #brief Zwraca wskaznik do rodzica
//
// #return wskaznik do rodzica
ARD_Level* gARDlevel::parent(void)
{
	return fParent;
}
//----------------------------------------------------------------------------

// #brief Zwraca numer levelu
//
// #return numer levelu
int gARDlevel::levelNo()
{
     return fParent->level();
}
//----------------------------------------------------------------------------

// #brief Zwraca liste wezlow ktore nie zaleza od ktorych zaden inny nie jest juz zalezny, chyba ze on sam
//
// #return liste wezlow ktore nie zaleza od ktorych zaden inny nie jest juz zalezny, chyba ze on sam
QList<gARDnode*>* gARDlevel::rootsNodes(void)
{
	QList<gARDnode*>* res = new QList<gARDnode*>;
	
	for(int i=0;i<fNodes->size();++i)
		res->append(fNodes->at(i));
		
	// Usuwanie poszczeolnych wezlow
	for(int i=0;i<fParent->dependencies()->size();++i)
	{
		if((fParent->dependencies()->at(i)->independent() == NULL) || (fParent->dependencies()->at(i)->dependent() == NULL))
		{
			delete res;
			return NULL;
		}
			
		// Nieusuwamy wezlow ktore zaleza od siebie
		if(fParent->dependencies()->at(i)->independent() == fParent->dependencies()->at(i)->dependent())
			continue;
			
		// Wyszukanie w liscie wynikowej wezla ktory jako rodzica ma wlasnosc zalezna
		for(int j=res->size()-1;j>=0;--j)
			if(res->at(j)->parent()->id() == fParent->dependencies()->at(i)->independent()->id())
				res->removeAt(j);
	}
	
	// Gdy nie znajdziemy normalnego roota, to jako rooty wyszukujemy wezly ktore maja najwieksze rodziny
	if(res->size() == 0)
	{
		// Badanie rozmiarow rodzin poszczegolnych wezlow
		QList<int> famSize;
		int maxSize = 0;
		for(int i=0;i<fNodes->size();++i)
		{
			QList<ARD_Property*>* family = fParent->findFamily(fNodes->at(i)->parent()->id());
			famSize.append(family->size());
			if(maxSize < family->size())
				maxSize = family->size();
			delete family;
		}
		
		// Do rezultatu dodajemy te wezly ktore maja rodzine rowna maksymalnemu rozmiarowi
		for(int i=0;i<fNodes->size();++i)
			if(maxSize == famSize.at(i))
				res->append(fNodes->at(i));
	}
	
	return res;
}
//----------------------------------------------------------------------------

// #brief Zwraca liste potomkow danego wezla na jeden poziom glebiej
//
// #param gARDnode* _parentNode - wezel rodzic
// #param bool skipParentNode - okresla czy do listy mozna dodac wezel weyjsciowy, to znaczy taki ktory jest zalezny od samego siebie
// #return liste potomkow danego wezla na jeden poziom glebiej
QList<gARDnode*>* gARDlevel::childsList(gARDnode* _parentNode, bool skipParentNode)
{
	QList<gARDnode*>* res = new QList<gARDnode*>;
	
	for(int i=0;i<fNodes->size();++i)
		if((!res->contains(fNodes->at(i))) && (fNodes->at(i)->ardParents()->contains(_parentNode)) && ((!skipParentNode) || ((skipParentNode) && (fNodes->at(i) != _parentNode))))
			res->append(fNodes->at(i));
			
	return res;
}
//----------------------------------------------------------------------------

// #brief Sprawdza czy podane wlasnosci maja wspolnego rodzica
//
// #param QList<gARDnode*>* _first - pierwsza lista rodzicow
// #param QList<gARDnode*>* _second - druga lista rodzicow
// #return index pierwszego wspolnego rodzica z listy pierwszej, jezeli nie ma wspolnego to -1
int gARDlevel::hasCommonParent(QList<gARDnode*>* _first, QList<gARDnode*>* _second)
{
	for(int i=0;i<_first->size();++i)
		if(_second->contains(_first->at(i)))	
			return i;
		
	return -1;
}
//----------------------------------------------------------------------------

// #brief Zwraca index zaleznosci o parametrach takich samych jak wejsciowa
//
// #param ARD_Dependency* _dep - wskaznik wlasnosci wejsciowej
// #return index zaleznosci o podanych parametrach lub jezeli takiej nie ma -1
int gARDlevel::indexOfDependencyInOut(ARD_Dependency* _dep)
{
	if(_dep == NULL)
		return -1;
	if((_dep->independent() == NULL) || (_dep->dependent() == NULL))
		return -1;
		
	return indexOfDependencyInOut(_dep->independent()->id(), _dep->dependent()->id());
}
//----------------------------------------------------------------------------

// #brief Zwraca index zaleznosci o podancyh koncach
//
// #param QString _indep - identyfikator wlasnosci niezaleznej
// #param QString _dep - identyfikator wlasnosci zaleznej
// #return index zaleznosci o podanych koncach lub jezeli takiej nie ma -1
int gARDlevel::indexOfDependencyInOut(QString _indep, QString _dep)
{
	for(int i=0;i<fDependencies->size();++i)
		if((fDependencies->at(i)->parent()->independent() != NULL) && (fDependencies->at(i)->parent()->dependent() != NULL) &&
		(fDependencies->at(i)->parent()->independent()->id() == _indep) && (fDependencies->at(i)->parent()->dependent()->id() == _dep))
			return i;
			
	return -1;
}
//----------------------------------------------------------------------------

// #brief Zwraca index wezla o podanym id
//
// #param unsigned int _id - identyfikator wezla
// #return index wezla o podanym id, jezeli nie ma takiego to zwraca -1
int gARDlevel::indexOfNodeID(unsigned int _id)
{
	for(int i=0;i<fNodes->size();++i)
		if(fNodes->at(i)->id() == _id)
			return i;
			
	return -1;
}
//----------------------------------------------------------------------------

// #brief Zwraca index wezla ktory ma rodzica o podanym id
//
// #param QString _pID - identyfikator rodzica (ARD_Poperty)
// #return index wezla ktory ma rodzica o podanym id, jezeli nie ma takiego to zwraca -1
int gARDlevel::indexOfParentID(QString _pID)
{
	for(int i=0;i<fNodes->size();++i)
		if((fNodes->at(i)->parent() != NULL) && (fNodes->at(i)->parent()->id() == _pID))
			return i;
			
	return -1;
}
//----------------------------------------------------------------------------

// #brief Zwraca wskaznik do wezla ktory ma rodzica o podanym id
//
// #param QString _pID - identyfikator rodzica (ARD_Poperty)
// #return wskaznik do wezla ktory ma rodzica o podanym id, jezeli nie ma takiego to zwraca NULL
gARDnode* gARDlevel::indexOfParentIDptr(QString _pID)
{
	int res = indexOfParentID(_pID);
	if(res == -1)
		return NULL;
	return fNodes->at(res);
}
//----------------------------------------------------------------------------

// #brief Wyszukuje zaleznosci na podstawie zrodla i konca
//
// #param QString _src - identyfikator wlasciwosci wejscowej, jezeli "" to dowolna
// #param QString _dest - identyfikator wlasciwosci koncowej, jezeli "" to dowolna
// #return liste zaleznosci spelniajacych warunki
QList<gARDdependency*>* gARDlevel::indexOfinout(QString _src, QString _dest)
{
	QList<gARDdependency*>* res = new QList<gARDdependency*>;

	for(int i=0;i<fDependencies->size();++i)
		if(((_src == "") || ((fDependencies->at(i)->parent()->independent() != NULL) && (fDependencies->at(i)->parent()->independent()->id() == _src))) 
		&& 
		((_dest == "") || ((fDependencies->at(i)->parent()->dependent() != NULL) && (fDependencies->at(i)->parent()->dependent()->id() == _dest))))
			res->append(fDependencies->at(i));
			
	return res;
}
// -----------------------------------------------------------------------------

// #brief Rozmieszcza  elementy poziomu
//
// #return Brak zwracanych wartosci
void gARDlevel::placeObjects(void)
{
	setParents();
	QList<gARDnode*>* roots = rootsNodes();
	if(roots->size() == 0)
		return;
	createTree(roots);
}
//----------------------------------------------------------------------------

// #brief Odnawia widok dla calego diagramu
//
// #return Brak zwracanych wartosci
void gARDlevel::refresh(void)
{	
	// Ustawianie marginesow i tekstu
	QRectF area = levelArea();
	QFontMetrics fm(fTexts[0]->font());
	QString text = "Level " + QString::number(fParent->level());
	
	float textXpos = area.x();
	float expYpos = area.y() + 1.;
     float delXpos = area.x() + fExpander->boundingRect().width()+1;

	if(!fExpanded)
	{
		textXpos = area.x() + (MARGIN_LENGTH >> 1) - (fm.width(text) >> 1);
		expYpos = area.y() + (area.height()/2.) - (fExpander->boundingRect().height()/2.);
	}
	
	fMargins[0]->setPos(area.x(), area.y());
	fMargins[1]->setPos(area.x(), area.bottom());
	
	fTexts[0]->setPlainText(text);
	fTexts[0]->setPos(textXpos, area.y()+(area.height()/2)-(fm.height()/2));
	
	fExpander->setPos(area.x(), expYpos);
     fDeleter->setPos(delXpos,expYpos);

     if(_display == NULL)
          return;

	// Dodawanie/usuwanie elementow
	for(int i=0;i<fNodes->size();++i)
	{
		// Sprawdzanie czy element znajduje sie na scenie
          bool itemExists = _display->items().contains(fNodes->at(i));

		if((fExpanded) && (!itemExists))
               _display->addItem(fNodes->at(i));
		if((!fExpanded) && (itemExists))
               _display->removeItem(fNodes->at(i));
	}
		
	for(int i=0;i<MARGINS_COUNT;++i)
	{
		QPen linePen;
		linePen.setColor(Settings->ardMarginsColor());
		linePen.setStyle(Qt::SolidLine);
		linePen.setWidth(1);
		fMargins[i]->setPen(linePen);
          if(!(_display->items().contains(fMargins[i])))
               _display->addItem(fMargins[i]);
	}
	for(int i=0;i<TEXTS_COUNT;++i)
	{
		fTexts[i]->setDefaultTextColor(Settings->ardLevelLabelFontColor());
          if(!(_display->items().contains(fTexts[i])))
               _display->addItem(fTexts[i]);
	}
     if(!(_display->items().contains(fExpander)))
          _display->addItem(fExpander);
     if(!(_display->items().contains(fDeleter)))
          _display->addItem(fDeleter);
     _display->update();
}
//----------------------------------------------------------------------------

// #brief Odswieza uklad polaczen
//
// #param ARD_Property* _input - jezeli NULL to odswieza wszystkie polaczenia, jezeli nie to odswieza tylko te ktore lacza sie z dana wlasnoscia
// #return brak zwracanych wartosci
void gARDlevel::repaintDependencies(ARD_Property* _input)
{
	for(int i=0;i<fDependencies->size();++i)
		if((_input == NULL) || (fDependencies->at(i)->parent()->independent() == _input) || (fDependencies->at(i)->parent()->dependent() == _input))
			fDependencies->at(i)->repaint();
}
//----------------------------------------------------------------------------

// #brief Usuwa polaczen zwiazane z dana wlasnoscia
//
// #param ARD_Property* _input - jezeli NULL to usuwa wszystkie polaczenia, jezeli nie to usuwa tylko te ktore lacza sie z dana wlasnoscia
// #return brak zwracanych wartosci
void gARDlevel::removeDependencies(ARD_Property* _input)
{
	for(int i=fDependencies->size()-1;i>=0;--i)
		if((_input == NULL) || (fDependencies->at(i)->parent()->independent() == _input) || (fDependencies->at(i)->parent()->dependent() == _input))
			fDependencies->takeAt(i);
}
//----------------------------------------------------------------------------

// #brief Usuwa polaczen zwiazane z danym rodzicem
//
// #param ARD_Dependency* _parent - rodzic polaczenia
// #return brak zwracanych wartosci
void gARDlevel::removeDependencies(ARD_Dependency* _parent)
{
	if(_parent == NULL)
		return;
	
	for(int i=fDependencies->size()-1;i>=0;--i)
		if(fDependencies->at(i)->parent() == _parent)
			fDependencies->takeAt(i);
}
//----------------------------------------------------------------------------

// #brief Ustawia w kazdym wezle wskazniki do rodzicow
//
// #return brak zwracanych wartosci
void gARDlevel::setParents(void)
{
	// W tym drzewie przyjmujemy ze dzieckiem jest walsnosc niezalezna (independent) a rodzicem zalezna (dependent)

	// Kasowanie listy rodzicow dla kazdego wezla
	for(int i=0;i<fNodes->size();++i)
		fNodes->at(i)->ardParents()->clear();
		
	// Przeszukiwanie zaleznosci
	for(int d=0;d<fParent->dependencies()->size();++d)
	{
		if((fParent->dependencies()->at(d)->independent() == NULL) || (fParent->dependencies()->at(d)->dependent() == NULL))
			return;
			
		// Wyszukiwanie poczatkowego i koncowego wezla
		int childNodeIndex = indexOfParentID(fParent->dependencies()->at(d)->independent()->id());
		int parentNodeIndex = indexOfParentID(fParent->dependencies()->at(d)->dependent()->id());
		
		if((childNodeIndex == -1) || (parentNodeIndex == -1))
			continue;
			
		fNodes->at(childNodeIndex)->ardParents()->append(fNodes->at(parentNodeIndex));
		//QMessageBox::critical(NULL, "HQEd", "Dodano zaleznosc parent=" + fNodes->at(parentNodeIndex)->parent()->id() + ", child=" + fNodes->at(childNodeIndex)->parent()->id());
	}
	
	// Wyswietlenie listy rodzicow
	/* QString res = "Lista rodzicow dla poszczegolnych wezlow:";
	for(int i=0;i<fNodes->size();++i)
	{
		res += "\nWezel " + fNodes->at(i)->parent()->id() + ": ";
		for(int j=0;j<fNodes->at(i)->ardParents()->size();++j)
			res += fNodes->at(i)->ardParents()->at(j)->parent()->id() + ",";
	}
	QMessageBox::critical(NULL, "HQEd", res); */
}
//----------------------------------------------------------------------------

// #brief Ustawia zaleznosci odleglosciowe pomiedzy wezlami a ich rodzicami
//
// #param ARD_Property* _root - wezel poczatkowy
// #return brak zwracanych wartosci
void gARDlevel::createTree(QList<gARDnode*>* _roots)
{
	QList<QList<gARDnode*>*>* _nodes;
	QList<gARDnode*>* _allNodes;
	QList<QList<float>*>* _heights;
	QList<float>* _widths;
     
	_nodes = new QList<QList<gARDnode*>*>;
	_allNodes = new QList<gARDnode*>;
	_heights = new QList<QList<float>*>;
	_widths = new QList<float>;

	_nodes->append(new QList<gARDnode*>);
	for(int i=0;i<_roots->size();++i)
	{
		_allNodes->append(_roots->at(i));
		_nodes->at(0)->append(_roots->at(i));
	}

	_heights->append(new QList<float>);
	for(int i=0;i<_roots->size();++i)
		_heights->at(0)->append(_roots->at(i)->fieldArea().height());

	// Wyszukiwanie maksymalnej szerokosci wsrod root'ow
	float mw = _roots->at(0)->fieldArea().width();
	for(int i=1;i<_roots->size();++i)
		if(_roots->at(i)->fieldArea().width() > mw)
			mw = _roots->at(i)->fieldArea().width();
	_widths->append(mw);

	getTreeGeometry(0, _allNodes, _nodes, _heights, _widths);

	// Ustawienie root'ow
	float currentNodeX = 0.;
	float currentNodeY = 0.;
	for(int i=0;i<_roots->size();++i)
	{
		float root_ypos = (_heights->at(0)->at(0)/2)-(_roots->at(i)->fieldArea().height()/2);
		_nodes->at(0)->at(i)->setPos(currentNodeX, currentNodeY+root_ypos);
		currentNodeY += (LEVEL_HEIGHT+_roots->at(i)->fieldArea().height());
	}
	//currentNodeX -= (_widths->at(1)+HORIZONTAL_MARGIN);

	// Teraz wpisujemy do poszczegolnych wezlow pozycje na scenie
	for(int level=1;level<_nodes->size();++level)
	{	
		currentNodeX -= (_widths->at(level)+HORIZONTAL_MARGIN);

		// Obliczanie sumy szerokosci na danym poziomie dla danego rodzica
		gARDnode*  currentParent = _nodes->at(level)->at(0);							// Bierzacy rodzic
		int sindex = 0;														// Index startowy potomkow danego rodzica
		float nodeHeight = .0;													// Suma wysokosci potomkow danego rodzica
		for(int pc=0;pc<_nodes->at(level)->size();++pc)
		{	
			nodeHeight += _heights->at(level)->at(pc);
			if((pc == (_nodes->at(level)->size()-1)) || (hasCommonParent(currentParent->ardParents(), _nodes->at(level)->at(pc+1)->ardParents()) == -1))
			{
				int eindex = pc;

				// Wyszukanie jednego z rodzicow wystepujacych na bierzacym poziomie
				int commonParentIndex = hasCommonParent(_nodes->at(level)->at(eindex)->ardParents(), currentParent->ardParents());
				if(commonParentIndex == -1)
				{
					QString params = "";
					int ni = indexOfNodeID(currentParent->id());
					params = "\nError params:\nCurrent Level = " + QString::number(level) + "\ncurrent property: " + fNodes->at(ni)->parent()->id();
					params += "\nindexOfNode = " + QString::number(ni);
					QMessageBox::critical(NULL, "gARDlevel::createTree", "Can't find parent on the higher level." + params);
					return;
				}

				// Ustawianie wszystkich wezlow
                    // nowa wersja
                    float parentTop = _nodes->at(level)->at(eindex)->ardParents()->at(commonParentIndex)->scenePos().y();
				float parentHeight = _nodes->at(level)->at(eindex)->ardParents()->at(commonParentIndex)->fieldArea().height();
				float parentCenter = parentTop+(parentHeight/2.);
                    // -- end nowa wersja
				nodeHeight = (-nodeHeight)/2.;
				for(int psi=sindex;psi<=eindex;++psi)
				{
					float realHalfHeight = _nodes->at(level)->at(psi)->fieldArea().height()/2.;
					float artificialHeight = _heights->at(level)->at(psi);
					float halfArtificialHeight = artificialHeight/2.;
                         // stara wersja - powodowala bledy w niektorych przypadkach
					//float parentTop = _nodes->at(level)->at(psi)->ardParents()->at(commonParentIndex)->scenePos().y();
					//float parentHeight = _nodes->at(level)->at(psi)->ardParents()->at(commonParentIndex)->fieldArea().height();
					//float parentCenter = parentTop+(parentHeight/2.);
					_nodes->at(level)->at(psi)->setPos(currentNodeX, parentCenter+(nodeHeight+halfArtificialHeight-realHalfHeight));
                         
					nodeHeight += artificialHeight;
				}
                    
				if(pc < (_nodes->at(level)->size()-1))
					currentParent = _nodes->at(level)->at(pc+1);
				sindex = eindex+1;
				nodeHeight = 0.;
			}
		}
	}

	for(int i=0;i<_nodes->size();++i)
		delete _nodes->at(i);
	for(int i=0;i<_heights->size();++i)
		delete _heights->at(i);
	delete _nodes;
	delete _allNodes;
	delete _heights;
	delete _widths;
}
//----------------------------------------------------------------------------

// #brief Analizuje kolejne poziomy drzewa
//
// #param int currLevel - aktualny poziom w ktorym trzeba wyszukac wszystkich potomkow
// #param QList<gARDnode*>* _allNodes - lista wszystkich dodoanych wezlow
// #param QList<QList<gARDnode*>*>* _nodes - lista list wezlow z podzialem na poszczegolne poziomy
// #param QList<QList<float>*>* _heights - lista list szerokosci poszczegolnych poziomow i wezlow
// #param QList<float>* _widths - lista maksymalnych szerokosci poziomow
// #return brak zwracanych wartosci
void gARDlevel::getTreeGeometry(int currLevel, QList<gARDnode*>* _allNodes, QList<QList<gARDnode*>*>* _nodes, QList<QList<float>*>* _heights, QList<float>* _widths)
{
	// Wyszukiwanie potomkow wszystkich wlasciwosci na danym poziomie
	for(int i=0;i<_nodes->at(currLevel)->size();++i)
	{
		// Wyszukanie potomkow
		QList<gARDnode*>* childList = childsList(_nodes->at(currLevel)->at(i));
		
		// Wyswietlenie znalezionej listy potomkow
		/* QString strChilds = "Dla wezla " + _nodes->at(currLevel)->at(i)->parent()->id() + " znaleziono potomkow:\n";
		for(int i=0;i<childList->size();++i)
			strChilds += childList->at(i)->parent()->id() + ",";
		QMessageBox::critical(NULL, "HQEd", strChilds);
		*/
		
		// Gdy lisc
		if(childList->size() == 0)
		{
			delete childList;
			continue;
		}
		
		// Wpisanie znaleznionych potomkow i ich szerokosci i wyszukanie maksymalnej wysokosci
		for(int c=0;c<childList->size();++c)
		{
			if(_allNodes->contains(childList->at(c)))
				continue;
				
			// Sprawdzanie czy sa wczytani wszyscy rodzice, jezeli nie to musimy sprawdzic czy nie jest to jakis cykl
			bool contML = false;
			for(int parentIndex=0;parentIndex<childList->at(c)->ardParents()->size();++parentIndex)
			{
				// Moze istniec kilka sytuacji osobliwych ktore sygnalizuja zaklocenie w normalnej postci drzewa
				// Cykle i zaleznosci przechodnie i posrednie
				// Trzeba je wykryc dla jaknajlepszego rozmieszczenie elemntow na scenie
				// Jezeli nie wszyscy rodzice sa w liscie albo sa na ostatnim poziomie to mamy doczynienia
				// z taka dziwna sytuacja
				
				// Jezeli jezeli ktorys z rodzicow jest na bierzacym poziomie to kontynujemy bo ten element zostanie dodany pozniej
				if((_nodes->size() == (currLevel+2)) && (_nodes->at(currLevel+1)->contains(childList->at(c)->ardParents()->at(parentIndex))))
				{
					parentIndex = childList->at(c)->ardParents()->size();
					contML = true;
				}
				
				if(contML) continue;
				
				// Jezeli jakiegos rodzica nie ma jeszcze wsrod wezlow, to sprawdzamy czy, we wszystkich potomkach, naszego wezla on nie wystepuje
				// Jezeli wystepuje to sprawdzamy rozmiary ich rodzin, jezeli nasz aktualny ma wieksza to go dodajemy
				// Jezeli nie wystepuje to znaczy ze nie ma cyklu i dodamy go poniej jak sie pojawia wszyscy rodzice
				if(!_allNodes->contains(childList->at(c)->ardParents()->at(parentIndex)))
				{
					QList<ARD_Property*>* family = fParent->findFamily(childList->at(c)->parent()->id());
					if(!family->contains(childList->at(c)->ardParents()->at(parentIndex)->parent()))
					{	
						parentIndex = childList->at(c)->ardParents()->size();
						contML = true;
					}
					
					// Gdy rodzina zawiera rodzica - CYKL - dodajemy naszego aktualnego jezeli ma wieksza rodzina - jest wczesniej w drzewie
					if((!contML) && (family->contains(childList->at(c)->ardParents()->at(parentIndex)->parent())))
					{
						QList<ARD_Property*>* family2 = fParent->findFamily(childList->at(c)->ardParents()->at(parentIndex)->parent()->id());
						if(family2->size() > family->size())
						{
							parentIndex = childList->at(c)->ardParents()->size();
							contML = true;
						}
						
						delete family2;
					}
					
					
					delete family;
				}
			}
			
			if(contML)
			{
				//QString msg = "Dla rodzica " + _nodes->at(currLevel)->at(i)->parent()->id() + " nie dodano potomka " + childList->at(c)->parent()->id();
				//QMessageBox::critical(NULL, "HQEd", msg);
				continue;
			}
				
			// Ewntualne powiekszanie list
			while(_nodes->size() < (currLevel+2))
			{
				_nodes->append(new QList<gARDnode*>);
				_heights->append(new QList<float>);
				_widths->append(0.);
			}
				
			_allNodes->append(childList->at(c));
			_nodes->at(currLevel+1)->append(childList->at(c));
			_heights->at(currLevel+1)->append(childList->at(c)->fieldArea().height()+LEVEL_HEIGHT);
			if(childList->at(c)->fieldArea().width() > _widths->at(currLevel+1))
				_widths->replace(currLevel+1, childList->at(c)->fieldArea().width());
		}
		
		delete childList;
	}
	
	// Gdy na danym poziomie nie ma juz potomkow
	if(_nodes->size() < (currLevel+2))
		return;
		
	// Odpalenie funckji dla nizszych poziomow
	getTreeGeometry(currLevel+1, _allNodes, _nodes, _heights, _widths);
	
	/* QString t = "Utworzono drzewo na poziome " + QString::number(fParent->level()) + ":";
	for(int i=0;i<_nodes->size();++i)
	{
		t += "\nPoziom " + QString::number(i) + ": ";
		for(int j=0;j<_nodes->at(i)->size();++j)
			t += _nodes->at(i)->at(j)->parent()->id() + ", ";
	}
	QMessageBox::critical(NULL, "HQEd", t); */
		
	// Teraz nastepuje poprawianie tymczasowych wysokosci wezlow
	float height_sum = 0.;
	gARDnode* currentParent = _nodes->at(currLevel+1)->at(0);
		
	for(int i=0;i<_heights->at(currLevel+1)->size();++i)
	{
		height_sum += _heights->at(currLevel+1)->at(i);
		if((i == (_heights->at(currLevel+1)->size()-1)) || (hasCommonParent(currentParent->ardParents(), _nodes->at(currLevel+1)->at(i+1)->ardParents()) == -1))
		{
			// ---- Zapisanie wyliczonej szerokosci ----
			// Wyszukanie rodzica w liscie na wyzszym poziomie
			int pindex = hasCommonParent((*_nodes)[currLevel], currentParent->ardParents());
					
			// Gdy nie znajdziemy rodzica w liscie powyzej to straszny blad
			if(pindex == -1)
			{
				QString params = "";
				int ni = indexOfNodeID(currentParent->id());
				params = "\nError params:\nCurrent Level = " + QString::number(currLevel) + "\ncurrent parent: " + fNodes->at(ni)->parent()->id();
				params += "\nindexOfNode = " + QString::number(ni);
				QMessageBox::critical(NULL, "gARDlevel::getTreeGeometry", "Can't find parent on the higher level." + params);
				return;
			}
				
			// Zapisanie wyliczonej szerokosci, jezeli wieksza od oryginalnej
			if(height_sum > _heights->at(currLevel)->at(pindex))
				_heights->at(currLevel)->replace(pindex, height_sum);
			
			// Zapisanie nowych parametrow
			if(i < (_heights->at(currLevel+1)->size()-1))
				currentParent = _nodes->at(currLevel+1)->at(i+1);
			height_sum = 0.;
		}
	}
}
//----------------------------------------------------------------------------

// #brief Zwraca obszar zajmowany przez dany level
//
// #return obszar zajmowany przez dany level
QRectF gARDlevel::levelArea(void)
{	
	QRectF res(0., 0., 0., 0.);
	
	if(fNodes->size() == 0)
		return res;
	
	float minx = fNodes->at(0)->pos().x();
	float miny = fNodes->at(0)->pos().y();
	float maxx = fNodes->at(0)->pos().x()+fNodes->at(0)->boundingRect().width();
	float maxy = fNodes->at(0)->pos().y()+fNodes->at(0)->boundingRect().height();
	
	for(int i=1;i<fNodes->size();++i)
	{
		QRectF br = fNodes->at(i)->boundingRect();
		if(minx > fNodes->at(i)->pos().x())
			minx = fNodes->at(i)->pos().x();
		if(miny > fNodes->at(i)->pos().y())
			miny = fNodes->at(i)->pos().y();
		if(maxx < (fNodes->at(i)->pos().x()+br.width()))
			maxx = fNodes->at(i)->pos().x()+br.width();
		if(maxy < (fNodes->at(i)->pos().y()+br.height()))
			maxy = fNodes->at(i)->pos().y()+br.height();
	}
	
	for(int i=0;i<fDependencies->size();++i)
	{
		QRectF depArea = fDependencies->at(i)->area();
		if(!depArea.isValid())
			continue;
		
		if(minx > depArea.x())
			minx = depArea.x();
		if(miny > depArea.y())
			miny = depArea.y();
		if(maxx < (depArea.x()+depArea.width()))
			maxx = depArea.x()+depArea.width();
		if(maxy < (depArea.y()+depArea.height()))
			maxy = depArea.y()+depArea.height();
	}
	
	res.setX(minx-LEFT_RIGHT_EDGE_WIDTH);
	res.setY(miny-TOP_BOTTOM_EDGE_WIDTH);
	res.setWidth(maxx-minx+(2*LEFT_RIGHT_EDGE_WIDTH));
	res.setHeight(maxy-miny+(2*TOP_BOTTOM_EDGE_WIDTH));
	
	if(!fExpanded)
	{
		QFontMetrics fm(fTexts[0]->font());
		float dy = fm.height()+TOP_BOTTOM_EDGE_WIDTH;
		res.setHeight(dy);
	}
	
	return res;
}
//----------------------------------------------------------------------------

// #brief Zwraca obszar zajmowany przez dany level
//
// #return obszar zajmowany przez dany level
void gARDlevel::moveLevelBy(float dx, float dy)
{
	// Przesuwanie wezlow
	for(int i=0;i<fNodes->size();++i)
		fNodes->at(i)->moveBy(dx, dy);
		
	for(int i=0;i<fDependencies->size();++i)
		fDependencies->at(i)->moveBy(dx, dy);
		
	// Przesuwanie marginesow
	for(int i=0;i<MARGINS_COUNT;++i)
		fMargins[i]->moveBy(dx, dy);
		
	// Przesuwanie marginesow
	for(int i=0;i<TEXTS_COUNT;++i)
		fTexts[i]->moveBy(dx, dy);
	
	fExpander->moveBy(dx, dy);
     fDeleter->moveBy(dx, dy);
}
//----------------------------------------------------------------------------

//
// This slot deletes all the levels below the given one.
//
void gARDlevel::removeLevelsBelow()
{
     QMessageBox::StandardButton bttn = QMessageBox::warning(NULL,tr("Remove levels below"),\
                          tr("Are you sure you want to remove all of the levels below level %1?").arg(fParent->level()),\
                          QMessageBox::No | QMessageBox::Yes,QMessageBox::Yes);
     if (bttn == QMessageBox::Yes)
     {
          ardLevelList->removeLevelsBelow(fParent->level());
          setExpanded(true);
          if(_display)
               _display->update();
     }
     // Here sth can go bad, so if there are any problems creating things, check it here.
}
//----------------------------------------------------------------------------
