/**
* \file	ardSplitEditorWire.cpp
* \class  ardSplitEditorWire
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a QGraphicsItem resembling a wire (or an arrow)
* that represents a Dependency.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/

#include <QtGui>
#include <math.h>

#include "ardSplitEditorWire.h"
#include "ardSplitEditorNode.h"

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
//-------------------------------------------------------------------------------------------

const qreal Pi = 3.1413;
// This value is chosen through test. It is used to determine the angle of the arrow.
const qreal divisor = 2.5;
//-------------------------------------------------------------------------------------------

ardSplitEditorWire::ardSplitEditorWire(ardSplitEditorNode *startItem, ardSplitEditorNode *endItem, QGraphicsItem *parent, QGraphicsScene *scene)
     : QGraphicsLineItem(parent, scene)
{
     myStartItem = startItem;
     myEndItem = endItem;
     setFlag(QGraphicsItem::ItemIsSelectable, true);

     myColor = Settings->ardDependencyColor();;

     myPen.setWidth(1);
     myPen.setColor(Settings->ardDependencyColor());
     myPen.setStyle(Qt::SolidLine);

     mySelectedPen.setWidth(1);
     mySelectedPen.setColor(Settings->ardSelectedDependencyColor());
     mySelectedPen.setStyle(Qt::SolidLine);

     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);
     arrowSize = fontM.height();
}
//----------------------------------------------------------------------------------

QRectF ardSplitEditorWire::boundingRect() const
{
     qreal extra = (pen().width() + 20) / 2.0;
     return QRectF(line().p1(), QSizeF(line().p2().x() - line().p1().x(),
                                       line().p2().y() - line().p1().y()))
     .normalized()
     .adjusted(-extra, -extra, extra, extra);
}
//----------------------------------------------------------------------------------

QPainterPath ardSplitEditorWire::shape() const
{
     QPainterPath path;

     path = QGraphicsLineItem::shape();
     path.addPolygon(arrowHead);
     return path;
}
//----------------------------------------------------------------------------------

void ardSplitEditorWire::updatePosition()
{
     QLineF line(mapFromItem(myStartItem, 0, 0), mapFromItem(myEndItem, 0, 0));
     setLine(line);
}
//----------------------------------------------------------------------------------

void ardSplitEditorWire::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)
{
     /* Check for collisions, if the two item collide, the algorithm may not work.
       The collisions are checked by shape() which a pixel bigger than the boundingRect() for the
       ardSplitEditorNode
       */
     if (myStartItem->collidesWithItem(myEndItem,Qt::IntersectsItemShape))
          return;

     // Drawing the lines
#if QT_VERSION >= 0x040500
     QRectF myStartRect = myStartItem->mapRectToScene(myStartItem->boundingRect());
     QRectF myEndRect = myEndItem->mapRectToScene(myEndItem->boundingRect());
#else
     QRectF myStartRect = myStartItem->mapToScene(myStartItem->boundingRect()).boundingRect();
     QRectF myEndRect = myEndItem->mapToScene(myEndItem->boundingRect()).boundingRect();
#endif

     QLineF centerLine(myStartRect.center(), \
                       myEndRect.center());
     QPolygonF endPolygon(myEndRect);
     QPolygonF startPolygon(myStartRect);
     QPointF p1 = endPolygon.first();
     QPointF p2;
     QPointF intersectEndPoint;
     QPointF intersectStartPoint;
     QLineF polyLine;

     //Finding the end intersection point.
     for (int i = 1; i < 5; ++i)
     {
          p2 = endPolygon.at(i);
          polyLine = QLineF(p1, p2);
          QLineF::IntersectType intersectType =
                    polyLine.intersect(centerLine, &intersectEndPoint);
          if (intersectType == QLineF::BoundedIntersection)
               break;
          p1 = p2;
     }

     // Finding the start intersection point.
     p1 = startPolygon.first();
     for (int i = 1; i < 5; ++i)
     {
          p2 = startPolygon.at(i);
          polyLine = QLineF(p1, p2);
          QLineF::IntersectType intersectType =
                    polyLine.intersect(centerLine, &intersectStartPoint);
          if (intersectType == QLineF::BoundedIntersection)
               break;
          p1 = p2;
     }

     setLine(QLineF(intersectEndPoint, intersectStartPoint));

     // Turning the head of the arrow around
     double angle = ::acos(line().dx() / line().length());
     if (line().dy() >= 0)
          angle = (Pi * 2) - angle;

     QPointF arrowP1 = line().p1() + QPointF(sin(angle + Pi / divisor) * arrowSize,
                                             cos(angle + Pi / divisor) * arrowSize);
     QPointF arrowP2 = line().p1() + QPointF(sin(angle + Pi - Pi / divisor) * arrowSize,
                                             cos(angle + Pi - Pi / divisor) * arrowSize);

     arrowHead.clear();
     arrowHead << line().p1() << arrowP1 << arrowP2;

     // Setting the Pen and color
     if (isSelected())
          painter->setPen(mySelectedPen);
     else
          painter->setPen(myPen);

     painter->setBrush(myColor);

     // Performing the draw.
     painter->drawLine(line());
     painter->drawPolygon(arrowHead);

}
//-------------------------------------------------------------------------------------------
