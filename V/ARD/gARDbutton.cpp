/**
* \file	gARDbutton.cpp
* \class  gARDbutton
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class used to implement a standard button useable in QGraphicsView.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//---------------------------------------------------------------------------

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QAction>
#include <QActionGroup>
#include <QLabel>
#include <QMenu>
#include <QtGui>

#include <QGraphicsItem>

#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "gARDlevel.h"
#include "gARDbutton.h"
//---------------------------------------------------------------------------

#define BUTTON_WIDTH 20
#define BUTTON_HEIGHT 20
//---------------------------------------------------------------------------

gARDbutton::gARDbutton(gARDlevel* _gParent) :
#if QT_VERSION >= 0x040600
     QGraphicsObject(0)
#else
     QGraphicsItem(0)
#endif
{
     fGparent = _gParent;
     fPictureStr = ":/all_images/images/plainButton.png";
     fPictureHoverStr = fPictureStr;
     fPictureNonHoverStr = fPictureStr;
     fPictureHoverStrExp = fPictureStr;
     fPictureNonHoverStrExp = fPictureStr;
     hover = false;
     setZValue(1);
     setAcceptsHoverEvents(true);
}
//---------------------------------------------------------------------------

gARDbutton::~gARDbutton(void)
{
}
//---------------------------------------------------------------------------

gARDlevel* gARDbutton::gParent(void)
{
     return fGparent;
}
//--------------------------------------------------------------------------

void gARDbutton::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     QRectF target(0., 0., BUTTON_WIDTH, BUTTON_HEIGHT);
     QImage image(fPictureStr);
     QRectF source(image.rect());

     _p->drawImage(target, image, source);
}
//---------------------------------------------------------------------------

void gARDbutton::mousePressEvent(QGraphicsSceneMouseEvent* _event)
{
     if(_event->button() != Qt::LeftButton)
          return;

     emit clicked();
     if(fGparent->expanded())
          fPictureStr = fPictureHoverStrExp;
     if(!fGparent->expanded())
          fPictureStr = fPictureHoverStr;
     update();
     QGraphicsItem::mousePressEvent(_event);
}
//---------------------------------------------------------------------------

void gARDbutton::hoverEnterEvent(QGraphicsSceneHoverEvent* _event)
{
     hover = true;
     if(fGparent->expanded())
          fPictureStr = fPictureHoverStrExp;
     if(!fGparent->expanded())
          fPictureStr = fPictureHoverStr;
     QGraphicsItem::hoverEnterEvent(_event);
}
//---------------------------------------------------------------------------

void gARDbutton::hoverLeaveEvent(QGraphicsSceneHoverEvent* _event)
{
     hover = false;
     if(fGparent->expanded())
          fPictureStr = fPictureNonHoverStrExp;
     if(!fGparent->expanded())
          fPictureStr = fPictureNonHoverStr;

     QGraphicsItem::hoverLeaveEvent(_event);
}
//---------------------------------------------------------------------------

void gARDbutton::updateCurrentPicStr()
{
     if (hover)
     {
          if (fGparent->expanded())
               fPictureStr = fPictureHoverStrExp;
          else // Not expanded.
               fPictureStr = fPictureHoverStr;

     }
     else // not hover
     {
          if (fGparent->expanded())
               fPictureStr = fPictureNonHoverStrExp;
          else // Not expanded.
               fPictureStr = fPictureNonHoverStr;
     }
}
//---------------------------------------------------------------------------

QRectF gARDbutton::boundingRect() const
{
     return QRectF(0., 0., BUTTON_WIDTH, BUTTON_HEIGHT);
}
//---------------------------------------------------------------------------
