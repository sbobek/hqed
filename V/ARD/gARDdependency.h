 /**
 * \file	gARDdependency.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.05.2007 
 * \version	1.0
 * \brief	This file contains class definition that represents the relationship between the properties on the ARD diagram
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef GARDDEPENDENCYH
#define GARDDEPENDENCYH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class gARDwire;
class gARDlevel;
class ARD_Dependency;
//---------------------------------------------------------------------------


/**
* \class 	gARDdependency
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.05.2007 
* \brief 	Class definition that represents the relationship between the properties on the ARD diagram
*/
class gARDdependency
{
private:
    
     unsigned int fID; ///< Connection identifier
    
     unsigned int fEssentialWiresQuantity; ///< Specifies how many elements of line are currently required
     
     gARDwire* fWire[5]; ///< Array of pointers of connections between the tables
     
     ARD_Dependency* fParent; ///< Object which represents connection
	     
     gARDlevel* fGparent; ///< Parent of the graphical representation of the level
     
     float line_1_pos_3p; ///< Specifies the proportion of first horizontal distance with three partial combination
		
     float line_1_pos_5p; ///< Specifies the proportion of first horizontal distance with five partial combination
	     
     float line_2_pos; ///< Specifies the proportion of first vertical distance with five partial combination
		
     float line_5_pos; ///< Specifies the proportion of second horizontal distance with five partial combination
		
     QPointF startP;  ///< Starting point of the connection
     QPointF endP;    ///< Endpoint of the connection

public:

     /// \brief Constructor of gARDdependency class
     gARDdependency(unsigned int, ARD_Dependency*, gARDlevel*);

     /// \brief Destructor of gARDdependency class
     ~gARDdependency(void);
	
     /// \brief Returns the connection ID
     unsigned int id(void);
	
     /// \brief Returns a pointer to the parent
     ARD_Dependency* parent(void);
	 
     /// \brief Returns a pointer to the graphic parent
     gARDlevel* gParent(void);

     /// \brief Method which is responsible for connections drawing
     void repaint(void);

     /// \brief Method which sets all elements as selected
     void selectAll(void);

     /// \brief Clear selection
     void clearSelection(void);

     /// \brief This method returns the pointers to the lines
     gARDwire* wires(int);

     /// \brief The method executed when the element changed his position
     void posChange(int);
	
     /// \brief Returns the area occupied by the connection
     QRectF area(void);
	
     /// \brief Moves the elements by the given vector
     void moveBy(float, float);
};
//---------------------------------------------------------------------------
#endif
