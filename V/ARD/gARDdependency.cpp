 /**
 *                                                                             *
 * @file gARDdependency.cpp                                                       *
 * Project name: HeKatE hqed                                                   *
 * $Id: gARDdependency.cpp,v 1.26.8.2 2011-09-01 23:03:51 hqeddoxy Exp $                  *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 21.05.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod obiektu symbolizujacego           *
 * polaczenie miedzy tabelami                                                  *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_Dependency.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"
#include "gARDnode.h"
#include "gARDlevel.h"
#include "gARDwire.h"
#include "gARDdependency.h"
//---------------------------------------------------------------------------


// #brief Konstruktor klasy, przyjmujacy rodzica i alokujacy pamiec na linie, symbolizujace polaczenie
//
// #param XTT_Connections* _parent - Wska�nik do polaczenia rodzica
// #return Brak zwracanych wartosci
gARDdependency::gARDdependency(unsigned int _id, ARD_Dependency* _parent, gARDlevel* _gParent) : fParent(_parent), fGparent(_gParent)
{
	fID = _id;

     fEssentialWiresQuantity = 0;
     for(int i=0;i<5;i++)
          fWire[i] = new gARDwire(true, true, this, i, 0);
     for(int i=0;i<3;i++)
          MainWin->ard_modelScene->addItem(fWire[i]);

     line_1_pos_3p = 0.5;
	line_1_pos_5p = 10.;
     line_2_pos = 0.5;
     line_5_pos = 10.;
}
//---------------------------------------------------------------------------

// #brief Destruktor klasy
//
// #return Brak zwracanych wartosci
gARDdependency::~gARDdependency(void)
{
     for(int i=0;i<5;i++)
          delete fWire[i];
}
//---------------------------------------------------------------------------

// #brief Zwraca identyfikator polaczenia
//
// #return identyfikator polaczenia
unsigned int gARDdependency::id(void)
{
	return fID;
}
//---------------------------------------------------------------------------

// #brief Zwraca wskaznik do rodzica
//
// #return Wskaznik do rodzica
ARD_Dependency* gARDdependency::parent(void)
{
     return fParent;
}
//---------------------------------------------------------------------------

// #brief Zwraca wskaznik do rodzica graficznego
//
// #return Wskaznik do rodzica graficznego
gARDlevel* gARDdependency::gParent(void)
{
	return fGparent;
}
//---------------------------------------------------------------------------

// #brief Zwraca obszar zajmowany przez polaczenia
//
// #return obszar zajmowany przez polaczenia
QRectF gARDdependency::area(void)
{
	//if(fEssentialWiresQuantity == 0)
		//return QRectF(1., 1., -1., 1.);
		
	QRectF res(fWire[0]->pos().x(), fWire[0]->pos().y(), fWire[0]->boundingRect().width(), fWire[0]->boundingRect().height());
	for(int i=1;i<5;++i)
		res |= QRectF(fWire[i]->pos().x(), fWire[i]->pos().y(), fWire[i]->boundingRect().width(), fWire[i]->boundingRect().height());
		
	return res;
}
//---------------------------------------------------------------------------

// #brief Przesuwa wszystkie elementy o zadany wektor
//
// #param float dx - przesuniecie wzgledem OX
// #param float dy - przesuniecie wzgledem OY
// #return brak zwracanych wartosci
void gARDdependency::moveBy(float dx, float dy)
{
	for(int i=0;i<5;++i)
		fWire[i]->moveBy(dx, dy); 
		
	repaint();
}
//---------------------------------------------------------------------------

// #brief Metoda wybierajaca wszystkie elementy jako zaznaczone
//
// #return Brak zwracanych warto�ci
void gARDdependency::selectAll(void)
{
     for(int i=0;i<5;i++)
     {
		fWire[i]->setDependencyActive(true);
          fWire[i]->setZValue(6);
     }
     //MainWin->xtt_scene->setSelectedConnectionID(fParent->ConnId());
}
//---------------------------------------------------------------------------

// #brief Metoda kasujaca wszystkie elementy jako zaznaczone
//
// #return Brak zwracanych warto�ci
void gARDdependency::clearSelection(void)
{
     for(int i=0;i<5;i++)
     {
		fWire[i]->setDependencyActive(false);
          fWire[i]->setSelected(false);
          fWire[i]->setZValue(5);
     }
     //MainWin->xtt_scene->setSelectedConnectionID(-1);
}
//---------------------------------------------------------------------------

// #brief Metoda zwracajaca wskazniki do lini
//
// #param int _i - Index lini
// #return Wskaznik jezeli index jest poprawny, ianczej flase
gARDwire* gARDdependency::wires(int _i)
{
     if((_i < 0) || (_i >= 5))
          return NULL;

     return fWire[_i];
}
//---------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie zmiany polozenia jedej z lini
//
// #param int itemIndex - Index lini ktorej zmiana dotyczy
// #return Brak zwracanych warto�ci
void gARDdependency::posChange(int itemIndex)
{
     // Gdy pierwsza pionowa
     if(itemIndex == 1)
     {
          // Pobranie dlugosc pierwszej lini przed danym elementem
          float len = fWire[0]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu
		if(fEssentialWiresQuantity == 3)
			len = len/line_1_pos_3p;
		if(fEssentialWiresQuantity == 5)
			len = len/line_1_pos_5p;

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[1]->x() - fWire[0]->x();
          if(new_len == 0)
               new_len = 1;
			
		if(fEssentialWiresQuantity == 3)
			line_1_pos_3p = new_len/len;
		if(fEssentialWiresQuantity == 5)
			line_1_pos_5p = new_len/len;
			
          repaint();

          return;
     }

     // Gdy srodkowa pozioma
     if(itemIndex == 2)
     {
          // Pobranie dlugosc lini po danym elemencie
          float len = fWire[1]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu dla obu lini
          len /= line_2_pos;

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[2]->y()-fWire[1]->y();
          if(new_len == 0)
               new_len = 1;
          line_2_pos = new_len/len;
          repaint();

          return;
     }

     // Gdy druga pionowa
     if(itemIndex == 3)
     {
          // Pobranie dlugosc lini po danym elemencie
          float len = fWire[4]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu dla obu lini
          len /= line_5_pos;

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[3]->x()-fWire[4]->x();
          if(new_len == 0)
               new_len = 1;
          line_5_pos = new_len/len;
          repaint();

          return;
     }
}
//---------------------------------------------------------------------------

// #brief Metoda rysujaca polaczenie
//
// #return Brak zwracanych warto�ci
void gARDdependency::repaint(void)
{
     // Okreslenie wspolrzednych poczatku i konca lini
	if(fParent == NULL)
		return;
	if((fParent->independent() == NULL) || (fParent->dependent() == NULL))
		return;
		
	int indepIndex = fGparent->indexOfParentID(fParent->independent()->id());
	int depIndex = fGparent->indexOfParentID(fParent->dependent()->id());
	
	if((indepIndex == -1) || (depIndex == -1))
		return;
	
	QRectF indepArea = fGparent->nodes()->at(indepIndex)->sceneBoundingRect();
	QRectF depArea = fGparent->nodes()->at(depIndex)->sceneBoundingRect();
	
	startP.setX(indepArea.right());
	startP.setY(indepArea.center().y());
	endP.setX(depArea.x());
	endP.setY(depArea.center().y());

     // Ustawienie flagi czy bylo 5 elementow na scenie
     int ewq = fEssentialWiresQuantity;

     // Okreslenie ilosci potrzebnych elementow polaczenia
     fEssentialWiresQuantity = 5;
     if(startP.x() < (endP.x()-5))
          fEssentialWiresQuantity = 3;
	if(!fGparent->expanded())
		fEssentialWiresQuantity = 0;
	
	// Usuwanie elementow ze ceny
	for(int i=(int)fEssentialWiresQuantity;i<ewq;++i)
		MainWin->ard_modelScene->removeItem(fWire[i]);
		
	// Dodawanie elementow do ceny
	for(int i=ewq;i<(int)fEssentialWiresQuantity;++i)
		MainWin->ard_modelScene->addItem(fWire[i]);
	
	QString currLabel = "";

     // Ustawienie parameteow linii
     if(fEssentialWiresQuantity == 3)
     {
		float line_1_pos = line_1_pos_3p;
		
          float len1 = (endP.x()-startP.x())*line_1_pos;
          float len2 = endP.y()-startP.y();
          float len3 = (endP.x()-startP.x())*(1-line_1_pos);

		fWire[1]->setVerticality(true);
          fWire[1]->setLength(len2);
          fWire[1]->setMoveable(true);
          fWire[1]->setCurPos(startP.x()+len1, startP.y());
          fWire[1]->setPos(startP.x()+len1, startP.y());
		//fWire[1]->update();
		
		//fWire[2]->setLabel(currLabel);
		fWire[2]->setLabelVisible(false);
		fWire[2]->setLabelAlignment(1);
          fWire[2]->setVerticality(false);
          fWire[2]->setLength(len3);
          fWire[2]->setMoveable(false);
          fWire[2]->setCurPos(startP.x()+len1, startP.y()+len2);
          fWire[2]->setPos(startP.x()+len1, startP.y()+len2);
		//fWire[2]->update();
		
		//fWire[0]->setLabel(currLabel);
		fWire[0]->setLabelVisible(false);
		fWire[0]->setLabelAlignment(-1);
          fWire[0]->setVerticality(false);
          fWire[0]->setLength(len1);
          fWire[0]->setMoveable(false);
          fWire[0]->setCurPos(startP.x(), startP.y());
          fWire[0]->setPos(startP.x(), startP.y());
		//fWire[0]->update();
		
		/// Musimy jakies wartosci ustawic aby poziomy sie prawidlowo zwijaly
		fWire[3]->setLength(1.);
		fWire[3]->setCurPos(startP.x()+len1, startP.y());
          fWire[3]->setPos(startP.x()+len1, startP.y());
		fWire[4]->setLength(1.);
		fWire[4]->setCurPos(startP.x()+len1, startP.y()+len2);
          fWire[4]->setPos(startP.x()+len1, startP.y()+len2);
     }

     // Ustawienie parameteow linii
     if(fEssentialWiresQuantity == 5)
     {
          // Ustawianie dlugosci kolejnych lini
          float len1 = line_1_pos_5p;
          float len5 = -line_5_pos;

          float len2 = endP.y()-startP.y();
          float len4 = -(endP.y()-startP.y());
          
		
		if((endP.y()-startP.y()) == 0)
          {
               len2 = (indepArea.height()/2)+20;
               len2 *= line_2_pos;
               len4 = len2;
          }
          else
          {
               len2 *= line_2_pos;
               len4 *= (1-line_2_pos);
          }

		//fWire[0]->setLabel(currLabel);
		fWire[0]->setLabelVisible(false);
		fWire[0]->setLabelAlignment(-1);
          fWire[0]->setVerticality(false);
          fWire[0]->setLength(len1);
          fWire[0]->setMoveable(false);
          fWire[0]->setCurPos(startP.x(), startP.y());
          fWire[0]->setPos(startP.x(), startP.y());
		//fWire[0]->update();

          fWire[1]->setVerticality(true);
          fWire[1]->setLength(len2);
          fWire[1]->setMoveable(true);
          fWire[1]->setCurPos(startP.x()+len1, startP.y());
          fWire[1]->setPos(startP.x()+len1, startP.y());
		//fWire[1]->update();


		//fWire[4]->setLabel(currLabel);
		fWire[4]->setLabelVisible(false);
		fWire[4]->setLabelAlignment(1);
          fWire[4]->setVerticality(false);
          fWire[4]->setLength(len5);
          fWire[4]->setMoveable(false);
          fWire[4]->setCurPos(endP.x(), endP.y());
          fWire[4]->setPos(endP.x(), endP.y());
		//fWire[4]->update();

          fWire[3]->setVerticality(true);
          fWire[3]->setLength(len4);
          fWire[3]->setMoveable(true);
          fWire[3]->setCurPos(endP.x()+len5, endP.y());
          fWire[3]->setPos(endP.x()+len5, endP.y());
		//fWire[3]->update();

		//fWire[2]->setLabel(currLabel);
		fWire[2]->setLabelVisible(false);
		fWire[2]->setLabelAlignment(0);
          fWire[2]->setVerticality(false);
		fWire[2]->setLength(fWire[3]->x() - fWire[1]->x());
          fWire[2]->setMoveable(true);
          fWire[2]->setCurPos(fWire[1]->pos().x(), startP.y()+len2);
          fWire[2]->setPos(fWire[1]->pos().x(), startP.y()+len2);
		//fWire[2]->update();

          // Dodawanie elementow do sceny
          /* if(!is5)
               for(int i=3;i<5;i++)
                    MainWin->ard_modelScene->addItem(fWire[i]); */
     }
	
	// Uaktualnienie widoku sceny
	MainWin->ard_modelScene->update(fGparent->levelArea());
}
//---------------------------------------------------------------------------
