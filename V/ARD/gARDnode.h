 /**
 * \file	gARDnode.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	02.01.2008 
 * \version	1.0
 * Comment:     Plik zawierajacy definicje obiektu bedacego wezlem diagramu ARD
 * \brief	This file contains class definition that is a node of ARD diagram
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GARDNODEH
#define GARDNODEH
//---------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class QGraphicsItem;
class ARD_Property;
class gARDlevel;
class ardPropertyEditor;
//---------------------------------------------------------------------------


/**
* \class 	gARDnode
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	1.05.2009
* \brief 	Class definition that is a node of ARD diagram.
*/
#if QT_VERSION >= 0x040600
class gARDnode : public 	QGraphicsObject
#else
class gARDnode : public QObject, public QGraphicsItem
#endif
{
     Q_OBJECT

private:
     
     unsigned int fID; ///< Node identifier
     
     ARD_Property* fParent; ///< Equivalent in the model
     
     gARDlevel* fGparent; ///< Parent of graphical representation of the level
     
     QList<gARDnode*>* fARDparents; ///< Parents in the ARD diagram
     
     ardPropertyEditor* _attributeEditor; ///< Pointer to the current edit window
     
     bool beingEdited; ///< On whether the an object is being edited.

public:

     /// \brief Constructor receiving a parent
     gARDnode(unsigned int, ARD_Property*, gARDlevel*);

     /// \brief Destructor
     ~gARDnode(void);

     /// ----------------- Methods that return values of class fields -----------------

     /// \brief Returns the node identifier
     unsigned int id(void);

     /// \brief Returns a pointer to the parent
     ARD_Property* parent(void);

     /// \brief Returns a pointer to the graphic parent
     gARDlevel* gParent(void);

     /// \brief Returns a list of parents in the ARD diagram
     QList<gARDnode*>* ardParents(void);

     /// ----------------- Operational Methods -----------------

     /// \brief Returns the index node with set identifier
     int indexOfParent(unsigned int);

     /// \brief The method of drawing an object
     void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* widget=0);

     /// \brief Method called when a double click
     void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*);

     /// \brief Method called when the mouse clicks on an object
     void mousePressEvent(QGraphicsSceneMouseEvent*);

     /// \brief Method called when the button is released over an object
     void mouseReleaseEvent(QGraphicsSceneMouseEvent*);

     /// \brief Controls the context menu
     void contextMenuEvent(QGraphicsSceneContextMenuEvent * event);

     /// \brief Method that changes the value of beingEdited
     void setBeingEdited(bool);

     /// \brief Method that returns the value of beingEdited
     bool getBeingEdited(void) const;

     /// \brief Method for determining the square in which it is located an object
     QRectF boundingRect() const;

     /// \brief Method called when an object changes position
     QVariant itemChange(GraphicsItemChange, const QVariant&);

     /// \brief Returns the rectangular area for drawing and unsubscribing attributes
     QRectF fieldArea(void) const;

protected slots:

     /// \brief Run after you have finished editing the gARDnode
     void finishedEditing(void);
};
//---------------------------------------------------------------------------
#endif
