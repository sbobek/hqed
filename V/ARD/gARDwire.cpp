 /**
 *                                                                             *
 * @file gARDwire.cpp                                                          *
 * Project name: HeKatE hqed                                                   *
 * $Id: gARDwire.cpp,v 1.26.4.1.2.2 2011-09-01 20:29:02 hqeddoxy Exp $                     *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 07.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod obiektu symbolizujacego           *
 * czesc polaczenia miedzy zaleznosciami na diagramie ARD                      *
 *                                                                             *
 */

#include <QGraphicsScene>

#include "../../M/ARD/ARD_Dependency.h"
#include "../../M/ARD/ARD_DependencyList.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "gARDdependency.h"
#include "gARDwire.h"
//---------------------------------------------------------------------------


// #brief Konstruktor klasy, ustawiajacy wlasciwosci obiektu
//
// #param _isVertical - Okresla czy jest to linia pionowa (isVertical == true)
// #param _isMovable - Okresla czy mozna ja przesuwac na scenie
// #param _parent - Wska�nik do rodzica pojemnika przechowujacego ten obiekt
// #param index - Index w tablicy rodzica tego obiektu
// #return brak zwracanych wartosci
gARDwire::gARDwire(bool _isVertical, bool _isMovable, gARDdependency* _parent, int _index, int _length)
{
    fIsVertical = _isVertical;
    fIsMoveable = _isMovable;
    fParent = _parent;
    fIndex = _index;
    fLength = _length;
    setFlag(ItemIsSelectable);
    setMoveable(fIsMoveable);
#if QT_VERSION >= 0x040600
    setFlag(ItemSendsGeometryChanges);
#endif
    setCursor(Qt::CrossCursor);

    fLabel = "";
    fLabelVisible = false;
    fIsSelected = false;
    fLabelAlignment = -1;
    level = 0;

    setZValue(5);
}
//---------------------------------------------------------------------------

// #brief Ustawia wlasciwosc poruszania po scenie za pomoca myszki elementu
//
// #param _mov - Wartosc okreslajaca czy element moze sie poruszac
// #return Brak zwracanych warto�ci
void gARDwire::setMoveable(bool _mov)
{
     setFlag(ItemIsFocusable, _mov);
     setFlag(ItemIsMovable, _mov);
     fIsMoveable = _mov;
}
//---------------------------------------------------------------------------

// #brief Ustawia wartosci zmiennych x-owych i y-kowych ktrych nie moze zmienic obiekt w zaleznosci od typu
//
// #param float _x - Wartosc x
// #param float _y - Wartosc y
// #return Brak zwracanych warto�ci
void gARDwire::setCurPos(float _x, float _y)
{
     fPos.setX(_x);
     fPos.setY(_y);
}
//---------------------------------------------------------------------------

// #brief Metoda ustawiajaca dlugosc lini i zapamietujaca ja w toolTip
//
// #param float _l - Nowa dlugosc lini
// #return Brak zwracanych warto�ci
void gARDwire::setLength(float _l)
{
     setToolTip(QString::number(_l));
     fLength = _l;
}
//---------------------------------------------------------------------------

// #brief Metoda klasy GRect rysujaca kwadrat na ekranie
//
// #param QPainter* _p - Wska�nik do plutna rysowania
// #param const QStyleOptionGraphicsItem* - Wska�nik do stylu rysowania
// #param QWidget* widget - Wska�nik nieuzywany :)
// #return Brak zwracanych warto�ci
void gARDwire::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Ustalanie wspolrzednych
	float x = 0.;
	float y = 0.;
     float dx = 0.;
     float dy = 0.;

     if(!fIsVertical)
          dx = fLength;
     if(fIsVertical)
          dy = fLength;

     QLineF line(x, y, dx, dy);

     if(!MainWin->ModelBW())
	{
          _p->setPen(QPen(Settings->ardDependencyColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
		if(fIsSelected)
			_p->setPen(QPen(Settings->ardSelectedDependencyColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
	}
     if(MainWin->ModelBW())
	{
          _p->setPen(QPen(QColor(0, 0, 0, 255), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
		if(fIsSelected)
			_p->setPen(QPen(QColor(0, 0, 0, 255), 1, Qt::DotLine, Qt::SquareCap, Qt::RoundJoin));
	}
                                                                
     _p->drawLine(line);
	
	// Wyswietlenie etykiety
/* 	if(fLabelVisible)
	{
		QString labelForShow = fLabel;
		QFontMetrics fm(Settings->ardARDdiagramFont());
		_p->setFont(Settings->ardARDdiagramFont());
		_p->setPen(Settings->xttConnectionLabelColor());
		
		// Odciecie nadmiernej ilosci znakow w etykiecie
		bool showLabel;
		int labelLength = fm.width(labelForShow);
		for(int i=fLabel.size()-1;i>=0;--i)
		{
			showLabel = true;
			if(!(((fLength > .0) && ((labelLength) <= (fLength - 10))) || ((fLength <= .0) && ((-labelLength) >= (fLength + 10)))))
			{
				labelForShow = fLabel.left(i) + "...";
				labelLength = fm.width(labelForShow);
				showLabel = false;
			}
			else break;
		}
		
		double xp = (fLength>.0)?8:-8; 			// gdy fLabelAlignment == -1
		if(fLabelAlignment == 0)
			xp = (fLength/2.0) - ((double)(labelLength/2)); 
		if(fLabelAlignment == 1)
			xp = (fLength>.0)?fLength-8-(labelLength):-(labelLength)-8;
		
		// Warunek sprawdzajacy czy dlugosc linii polaczenia nie jest za krotka, jezeli tak to ukrywamy podpis
		//if(((fLength > .0) && ((labelLength) <= (fLength - 10))) || ((fLength <= .0) && ((-labelLength) >= (fLength + 10))))
		if(showLabel)
			_p->drawText((int)xp, -5, labelForShow);
	} */
	
     //_p->drawRect(boundingRect());
}
//---------------------------------------------------------------------------

// #brief Metoda klasy zwracajaca obszar prostokatny w ktorym sie znajduje obiekt
//
// #return Obszar prostokatny zawierajacy obiekt
QRectF gARDwire::boundingRect() const
{
     // Margines
     int margin = fLabelVisible?5:2;

     float dsx = -margin;
     float dsy = -margin;
     
     float dx = margin*2.0;
     float dy = margin*2.0;

     if(!fIsVertical)
     {
          if(fLength < 0)
		{
               dsx = fLength-margin;
			dx = -fLength+(2*margin);
		}
		if(fLength >= 0)
			dx = fLength+(2*margin);
     }
     if(fIsVertical)
     {
		if(fLength < 0)
		{
               dsy = fLength-margin;
			dy = -fLength+(2*margin);
		}
		if(fLength >= 0)
			dy = fLength+(2*margin);
     }

     return QRectF(dsx, dsy, dx, dy);
}
//---------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie zmiany polozenia obiektu
//
// #param GraphicsItemChange change - Zawiera wartosc elementu ktora ulegla zmianie
// #param const QVariant &value - Referencja do wartosci zmienionych
// #return Wartosc ktora ulela zmianie
QVariant gARDwire::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemSelectedChange) && (scene()) && (level == 0))
     {
		level = 1;
          bool isSel = value.toBool();
          if(isSel)
               fParent->selectAll();
          else
               fParent->clearSelection();
		fIsSelected = isSel;
		level = 0;

     }

     if((change == ItemPositionChange) && (scene()) && (level == 0))
     {
          level = 1;
          QPointF newPos = value.toPointF();

          // Gdy pionowa to moze zmienic tylko wartosc x
          if(fIsVertical)
               newPos.setY(fPos.y());

          // Gdy pozioma to moze zmienic tylko wartosc y
          else if(!fIsVertical)
               newPos.setX(fPos.x());

          // Wykonaie operacji zwiazanych ze zmiana pozycji tego elementu
          if(scene()->mouseGrabberItem() == this)
               fParent->posChange(fIndex);

          level = 0;
          return newPos;
     }
     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy GRect wywolywana w momencie klikniecia elementu myszka
//
// #param QGraphicsSceneMouseEvent* _e - Wskaznik do obiektu przechowujacego parametry zdarzenia
// #return Brak zwracanych wartosci
void gARDwire::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
	scene()->clearSelection();
     QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy GWire wywolywana w momencie dwukrotnego klikniecia na obiekcie, prostokacie
//
// #param QGraphicsSceneMouseEvent* _e - Wska�nik do informacji o zdarzeniu
// #return Brak zwracanych warto�ci
void gARDwire::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
     QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy ustawiajaca sposob wysrodkowania etykiety opisujacej polaczenie
//
// #param _align - Sposob wysrodkowania -1-do lewej, 0-do srodka, 1-do prawej
// #return Brak zwracanych warto�ci
void gARDwire::setLabelAlignment(int _align)
{
	if((_align != -1) && (_align != 0 ) && (_align != 1))
	{
		fLabelAlignment = -1;
		return;
	}
		
	fLabelAlignment = _align;
}
//---------------------------------------------------------------------------

void gARDwire::setDependencyActive(bool _stat)
{	
	fIsSelected = _stat;
}
//---------------------------------------------------------------------------
