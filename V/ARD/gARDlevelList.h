 /**
 * \file	gARDlevelList.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	06.01.2008
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu zarzadzajacym prezentacja listy poziomow ARD na scenie
 * \brief	This file contains class definition that managers presentation of a list of ARD levels on the scene
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GARDLEVELLISTH
#define GARDLEVELLISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
// -----------------------------------------------------------------------------

class ARD_Level;
class ARD_LevelList;
class ARD_Property;
class ARD_Dependency;
class ARD_Attribute;
class XTT_Attribute;
class hBaseScene;
// -----------------------------------------------------------------------------


/**
* \class 	gARDlevelList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	06.01.2008
* \brief 	Class definition that managers presentation of a list of ARD levels on the scene
*/
class gARDlevelList
{

private:

     ARD_LevelList* fParent;       ///< The model corresponding object
     hBaseScene* _display;         ///< The object to display a tree
	
public:

     /// \brief Default contructor
     gARDlevelList(void);
	
     /// \brief Constructor
     ///
     /// \param __parent - the list of level that must be visualizated
     gARDlevelList(ARD_LevelList* __parent);
	
     /// \brief Destructor
     ~gARDlevelList(void);

     /// \brief Getter of a parent object
     ///
     /// \return A pointer to the parent object
     ARD_LevelList* parent(void);

     /// \brief Getter of a display object
     ///
     /// \return A pointer to the displaye object
     hBaseScene* display(void);

     /// \brief Setter of a display object
     ///
     /// \param __display - a pointer to the display object
     /// \return no values return
     void setDisplay(hBaseScene* __display);
	
     /// \brief Fixes the areae for the level
     ///
     /// \param _fromLevel - starting level
     /// \param _down - indicates if the alignment should go up or down
     /// \return no values return
     void alignLevels(int _fromLevel, bool _down);
};
// -----------------------------------------------------------------------------
#endif
