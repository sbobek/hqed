 /**
 * \file	gARDwire.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	07.01.2008 
 * \version	1.0
 * Comment:     Plik zawierajacy definicje linii symbolizujacej czesc calego 
 * polaczenia miedzy zaleznosciami na diagramie ARD
 * \brief	This file contains class definition that represents a part of 
 * the whole connection between the dependencies on the ARD diagram
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GARDWIREH
#define GARDWIREH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class gARDdependency;
//---------------------------------------------------------------------------


/**
* \class 	gARDwire
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	07.01.2008
* \brief 	Class definition that represents a part of the whole connection between the dependencies on the ARD diagram.
*/
class gARDwire : public QGraphicsItem
{
private:
     
     bool fIsVertical; ///< Specifies whether the line is vertical
     
     bool fIsMoveable; ///< Specifies whether you can move the this item
     
     gARDdependency* fParent; ///< Pointer to parent element
     
     int fIndex; ///< Index element in the parent
     
     float fLength; ///< Line length
     
     bool fIsSelected; ///< Specifies whether the object is selected
     
     QPointF fPos; ///< Specifies the x and y coordinate. Depending on the type of variable can not change it
     
     int level; ///< Determines the level of nesting functions itemChange
	     
     QString fLabel; ///< Specify a label connection
	     
     bool fLabelVisible; ///< Specifies whether the label is to be displayed
	     
     int fLabelAlignment; ///< Specifies how the label centering -1 - to the left, 0 - center, 1 - to right

public:

     /// \brief Class constructor
     gARDwire(bool, bool, gARDdependency*, int, int);

     /// \brief A drawing element method
     void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* widget=0);

     /// \brief Method of determining the area of the element being
     QRectF boundingRect() const;

     /// \brief The method called when object properties change
     QVariant itemChange(GraphicsItemChange, const QVariant&);
	
     /// \brief Method called when the mouse clicks on an object
     void mousePressEvent(QGraphicsSceneMouseEvent*);

     /// \brief Method called when Double Click on object
     void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*);

     /// \brief The method of setting the value of fields
     void setVerticality(bool _v){ fIsVertical = _v; }
     void setLabelVisible(bool _v){ fLabelVisible = _v; }
     void setLabel(QString _l){ fLabel = _l; }
     void setIndex(int _i){ fIndex = _i; }
     void setLength(float);
     void setActive(bool _s){ fIsSelected = _s; }
     void setMoveable(bool);
     void setCurPos(float, float);
     void setLabelAlignment(int);
     void setDependencyActive(bool);

     // Methods that return values of the fields
	
     /// \brief Returns whether an object can be clicked
     bool selected(void){ return fIsSelected; }
	
     /// \brief Returns the content of a label
     QString label(void){ return fLabel; }
	
     /// \brief Returns whether the label is visible
     bool isLabelVisible(void){ return fLabelVisible; }
	
     /// \brief Returns the way the label centering
     int labelAlignment(void){ return fLabelAlignment; }
};
//---------------------------------------------------------------------------
#endif
