/**
* \file	ardSplitEditorLoop.cpp
* \class  ardSplitEditorLoop
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a QGraphicsItem resembling a loop that represents a
* self-dependent Dependency.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//----------------------------------------------------------------------------
#include <QColor>
#include <QPen>
#include <math.h>

#include "ardSplitEditorLoop.h"
#include "ardSplitEditorNode.h"

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
//----------------------------------------------------------------------------

const qreal Pi = 3.1413;
const qreal divisor = 2.5;
//----------------------------------------------------------------------------

ardSplitEditorLoop::ardSplitEditorLoop(ardSplitEditorNode* node) :
          QGraphicsPathItem(), parentNode(node)
{
     setFlag(QGraphicsItem::ItemIsSelectable, true);

     myColor = Settings->ardDependencyColor();

     myPen.setWidth(1);
     myPen.setColor(Settings->ardDependencyColor());
     myPen.setStyle(Qt::SolidLine);

     mySelectedPen.setWidth(1);
     mySelectedPen.setColor(Settings->ardSelectedDependencyColor());
     mySelectedPen.setStyle(Qt::SolidLine);

     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);
     arrowSize = fontM.height();
}
//----------------------------------------------------------------------------

QRectF ardSplitEditorLoop::boundingRect() const
{
     qreal extra = (pen().width() + 20) / 2.0;
     QRectF returned = shape().controlPointRect().normalized().adjusted(-extra,-extra,extra,extra);
     return returned;
}
//----------------------------------------------------------------------------

void ardSplitEditorLoop::paint(QPainter *painter, const QStyleOptionGraphicsItem*, QWidget*)

{
     QPainterPath tempPath;
     QRectF arcRect(parentNode->boundingRect().width() / 4.0 , 0.0,\
                    parentNode->boundingRect().width()/ 2.0, arrowSize + parentNode->verticalMargin());
     qreal beginningDegree = 30.0;
     // Defining the arc of the path.
     tempPath.moveTo(arcRect.center());
     tempPath.arcTo(arcRect, 180.0  + beginningDegree, - (180 + 2.0 * beginningDegree) );

     // Turning the head of the arrow around
     double angle = 2.0 * Pi - 2.0 * Pi * tempPath.angleAtPercent(1  -  beginningDegree / 360.0 ) / 360.0;

     QPointF headOfArrow = tempPath.pointAtPercent( 1.0 );
     QPointF arrowP1 = headOfArrow + QPointF(sin(angle + Pi / divisor) * arrowSize,
                                             cos(angle + Pi / divisor) * arrowSize);
     QPointF arrowP2 = headOfArrow + QPointF(sin(angle + Pi - Pi / divisor) * arrowSize,
                                             cos(angle + Pi - Pi / divisor) * arrowSize);

     arrowHead.clear();
     arrowHead << headOfArrow << arrowP1 << arrowP2;

#if QT_VERSION >= 0x040500
     arcRect = parentNode->mapRectToScene(arcRect);
#else
     arcRect = parentNode->mapToScene(arcRect).boundingRect();
#endif

     qreal height = arcRect.top() - parentNode->mapToScene(headOfArrow).y();

     // Adding the arrowHead to the tempPath
     tempPath.closeSubpath();
     tempPath.moveTo(headOfArrow);
     tempPath.addPolygon(arrowHead);

#if QT_VERSION >= 0x040500
     QRectF boundRect = parentNode->mapRectToScene(parentNode->boundingRect());
#else
     QRectF boundRect = parentNode->mapToScene(parentNode->boundingRect()).boundingRect();
#endif

     QPointF tranPoint = boundRect.topLeft() + QPointF(0.0, height);

#if QT_VERSION >= 0x040600
     tempPath.translate(tranPoint);
#else
     QTransform transform;
     transform.translate(tranPoint.x(),tranPoint.y());
     tempPath = transform.map(tempPath);
#endif
     setPath(tempPath);

     // Doing the actual drawing
     if (painter != NULL)
     {
          if (isSelected())
               painter->setPen(mySelectedPen);
          else
               painter->setPen(myPen);

          painter->setBrush(myColor);

          // Drawing arc
          int startAngle = - ( 16 * 360  * beginningDegree / 360.0);
          int spanAngle = ( 16 * 360  * (180 + 2.0 * beginningDegree) / 360.0);

          arcRect.moveTop(arcRect.top() + height);
          painter->drawArc(arcRect, startAngle, spanAngle);

          // Drawing arrowHead
          painter->setBrush(myColor);
          arrowHead.translate(tranPoint);
          painter->drawPolygon(arrowHead);
     }
}
//----------------------------------------------------------------------------
