 /**
 *                                                                             *
 * @file gTPHnode.cpp                                                          *
 * Project name: HeKatE hqed                                                   *
 * $Id: gTPHnode.cpp,v 1.27.4.1.2.1 2011-09-01 20:03:44 hqeddoxy Exp $                     *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 2.01.2008                                                             *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod klasy bedacej wezlem              *
 * w diagramie tph                                                             *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QAction>
#include <QActionGroup>
#include <QLabel>
#include <QMenu> 
#include <QtGui>

#include <QGraphicsItem>

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/XTT/XTT_Attribute.h"

#include "gTPHnode.h"
//---------------------------------------------------------------------------

#define HORIZONTALMARGIN 10.
#define VERTICALMARGIN 10.
//---------------------------------------------------------------------------


// #brief Konstruktor klasy
//
// #param ARD_Property* _parent - Wska�nik do wezla rodzica
// #return Brak zwracanych wartosci
gTPHnode::gTPHnode(ARD_Property* _parent) : QGraphicsItem(0)
{
    fParent = _parent;
    setFlag(ItemIsSelectable);
    setFlag(ItemIsFocusable);
    setFlag(ItemIsMovable);
#if QT_VERSION >= 0x040600
    setFlag(ItemSendsGeometryChanges);
#endif
    setZValue(1);
	
    fTPHparent = NULL;
}
//---------------------------------------------------------------------------

// #brief Zwraca odleglosc od rodzica w pionie
//
// #param ARD_Property* _parent - Wska�nik do wezla rodzica
// #return Zwraca odleglosc od rodzica w pionie
float gTPHnode::verticalDistance(void)
{
	return fVerticalDistance;
}
//---------------------------------------------------------------------------

// #brief Zwraca odleglosc od rodzica w poziomie
//
// #param ARD_Property* _parent - Wska�nik do wezla rodzica
// #return Zwraca odleglosc od rodzica w poziomie
float gTPHnode::horizontalDistance(void)
{
	return fHorizontalDistance;
}
//--------------------------------------------------------------------------

// #brief Ustawia odleglosc od rodzica w pionie
//
// #param float _newVal - nowa odleglosc
// #return Brak zwracanych warto�ci
void gTPHnode::setVerticalDistance(float _newVal)
{
	fVerticalDistance = _newVal;
	setPos(scenePos().x(), fTPHparent->graphicTPHnode()->scenePos().y()+fTPHparent->graphicTPHnode()->fieldArea().height() - fVerticalDistance);
}
//--------------------------------------------------------------------------

// #brief Ustawia odleglosc od rodzica w poziomie
//
// #param float _newVal - nowa odleglosc
// #return Brak zwracanych warto�ci
void gTPHnode::setHorizontalDistance(float _newVal)
{
	fHorizontalDistance = _newVal;
	setPos(fTPHparent->graphicTPHnode()->scenePos().x()+(fTPHparent->graphicTPHnode()->fieldArea().width()/2) - fHorizontalDistance, scenePos().y());
}
//--------------------------------------------------------------------------

// #brief Ustawia wskaznik na rodzica w diagramie TPH
//
// #param ARD_Property* _TPHparent - wskaznik do rodzica w diagramie TPH
// #return Brak zwracanych warto�ci
void gTPHnode::setTPHparent(ARD_Property* _TPHparent)
{
	fTPHparent = _TPHparent;
}
//--------------------------------------------------------------------------

// #brief Metoda klasy GRect rysujaca kwadrat na ekranie
//
// #param QPainter* _p - Wska�nik do plutna rysowania
// #param const QStyleOptionGraphicsItem* - Wska�nik do stylu rysowania
// #param QWidget* widget - Wska�nik nieuzywany
// #return Brak zwracanych warto�ci
void gTPHnode::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
	//_p->drawRect(boundingRect());
	
     // Rysowanie kwadratu
     _p->setBrush(Settings->ardTPHnodeColor());
	if(!fParent->canBeTransformated())
		_p->setBrush(Settings->ardTPHnotTransformatedNodeColor());
     if(isSelected())
          _p->setBrush(Settings->ardTPHselectedNodeColor());
     if(MainWin->ModelBW())
          _p->setBrush(QColor(255, 255, 255, 255));
		
	// Wyrysowanie ramki
	_p->setPen(Qt::NoPen);
	if(Settings->useFramedNodes())
	{
		QPen ppen;
		ppen.setWidth(1);
		ppen.setColor(Qt::black);
		ppen.setStyle(Qt::SolidLine);
		if(!fParent->canBeTransformated())
			ppen.setStyle(Qt::DashLine);
			
		_p->setPen(ppen);
	}
	
	// Narysowanie prostokata
	QRectF rect = fieldArea();
     _p->drawRect(rect);
	
	// Wypisanie nazw atrybutow
	QFont nodeFont = Settings->ardTPHFont();
	QFontMetrics fontM(nodeFont);
	
	_p->setPen(Qt::SolidLine);
	_p->setPen(Settings->ardTPHfontColor());
	_p->setFont(nodeFont);
	int fontHeight = fontM.height();
	float leftMargin = HORIZONTALMARGIN/2.;
	float topMargin = VERTICALMARGIN/2.;
	
	for(int i=0;i<fParent->size();++i)
		_p->drawText(QPointF(leftMargin, topMargin+(float)((i+1)*fontHeight)), fParent->at(i)->name());
	
	// Narysowanie linii do rodzica
	if(fTPHparent != NULL)
	{	
		if((QApplication::keyboardModifiers() != Qt::ControlModifier) || (scene()->mouseGrabberItem() == this))
		{
			fHorizontalDistance = fTPHparent->graphicTPHnode()->scenePos().x()+(fTPHparent->graphicTPHnode()->fieldArea().width()/2) - scenePos().x();
			fVerticalDistance = fTPHparent->graphicTPHnode()->scenePos().y()+fTPHparent->graphicTPHnode()->fieldArea().height() - scenePos().y();
		}
		_p->setPen(Settings->ardTPHbranchColor());
		_p->drawLine((int)rect.width()/2, 0, (int)fHorizontalDistance, (int)fVerticalDistance);
	}
}
//---------------------------------------------------------------------------

// #brief Metoda klasy gTPHnode wywolywana w momencie pojedynczego klikniecia na obiekcie, prostokacie
//
// #param QGraphicsSceneMouseEvent* _e - Wska�nik do informacji o zdarzeniu
// #return Brak zwracanych warto�ci
void gTPHnode::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
	scene()->clearSelection();
	setSelected(true);
     
     QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy gTPHnode wywolywana w momencie dwukrotnego klikniecia na obiekcie, prostokacie
//
// #param QGraphicsSceneMouseEvent* _e - Wska�nik do informacji o zdarzeniu
// #return Brak zwracanych warto�ci
void gTPHnode::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
	QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy zwracajaca obszar prostokatny w ktorym sie znajduje obiekt
//
// #return Obszar prostokatny zawierajacy obiekt
QRectF gTPHnode::boundingRect() const
{
	return fieldArea() | QRectF(0., 0., fHorizontalDistance, fVerticalDistance);
}
//---------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie zmiany polozenia obiektu
//
// #param GraphicsItemChange change - Zawiera wartosc elementu ktora ulegla zmianie
// #param const QVariant &value - Referencja do wartosci zmienionych
// #return Wartosc ktora ulela zmianie
QVariant gTPHnode::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemSelectedChange) && (scene()))
     {
     }
     
     if((change == ItemPositionChange) && (scene()))
     {
		// Jezeli bez kontrola to przenosimy tylko wezel
		if(QApplication::keyboardModifiers() != Qt::ControlModifier)
		{
			// Odswiezenie potomkow
			QList<ARD_Property*>* cl = ardPropertyList->childList(fParent);
			for(int i=0;i<cl->size();++i)
				cl->at(i)->graphicTPHnode()->update();
		}
		
		// Jezeli kontrol to przenosimy cala galaz
		if(QApplication::keyboardModifiers() == Qt::ControlModifier)
		{
			// Odswiezenie potomkow
			QList<ARD_Property*>* cl = ardPropertyList->childList(fParent);
			for(int i=0;i<cl->size();++i)
			{
				cl->at(i)->graphicTPHnode()->setVerticalDistance(cl->at(i)->graphicTPHnode()->verticalDistance());
				cl->at(i)->graphicTPHnode()->setHorizontalDistance(cl->at(i)->graphicTPHnode()->horizontalDistance());
				cl->at(i)->graphicTPHnode()->update();
			}
		}
     }
	
     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Metoda obliczajaca wielksoc pola w ktorym sa wypisywane atrybuty
//
// #return obszar prostoktny w ktorym sa wypisywane atrybuty
QRectF gTPHnode::fieldArea(void) const
{
	// Ustalanie rozmiarow wezla
	float x = 0.;
	float y = 0.;
	float width = 0.;
	float height = 0.;
	
	QFont nodeFont = Settings->ardTPHFont();
	QFontMetrics fontM(nodeFont);
	
	for(int i=0;i<fParent->size();++i)
	{
		int currStrWidth = fontM.width(fParent->at(i)->name());
		height += fontM.height();
		
		if(currStrWidth > width)
			width = currStrWidth;
	}
	
	width += HORIZONTALMARGIN;
	height += VERTICALMARGIN;
	
	return QRectF(x, y, width, height);
}
//---------------------------------------------------------------------------
