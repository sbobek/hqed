/**
 *                                                                             *
 * @file gARDnode.cpp                                                          *
 * Project name: HeKatE hqed                                                   *
 * $Id: gARDnode.cpp,v 1.27.4.14.2.7 2011-09-01 23:03:51 hqeddoxy Exp $        *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 2.01.2008                                                             *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod klasy bedacej wezlem              *
 * w diagramie ARD                                                             *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QAction>
#include <QActionGroup>
#include <QLabel>
#include <QMenu> 
#include <QtGui>

#include <QGraphicsItem>

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/XTT/XTT_Attribute.h"

#include "gARDlevel.h"
#include "gARDlevelList.h"
#include "gARDdependency.h"
#include "gARDnode.h"

#include "../../C/ARD/ardPropertyEditor.h"
#include "../../C/ARD/ardFinalizationEditor.h"
#include "../../C/ARD/ardSplitEditor.h"

//---------------------------------------------------------------------------

#define HORIZONTALMARGIN 10.
#define VERTICALMARGIN 10.
//---------------------------------------------------------------------------

// #brief Konstruktor klasy
//
// #param _parent - Wskanik do wezla rodzica
// #return Brak zwracanych wartosci
gARDnode::gARDnode(unsigned int _id, ARD_Property* _parent, gARDlevel* _gParent) :
     #if QT_VERSION >= 0x040600
     QGraphicsObject(0)
   #else
     QGraphicsItem(0)
   #endif

{
     fID = _id;
     fParent = _parent;
     fGparent = _gParent;
     _attributeEditor = NULL;
     beingEdited = false;
     setFlag(ItemIsSelectable);
     setFlag(ItemIsFocusable);
     setFlag(ItemIsMovable);
#if QT_VERSION >= 0x040600
     setFlag(ItemSendsGeometryChanges);
#endif
     setZValue(1);

     fARDparents = new QList<gARDnode*>;
     setToolTip(_parent->id());     
}
//---------------------------------------------------------------------------

// #brief Destruktor klasy
//
// #return brak zwracanych wartosci
gARDnode::~gARDnode(void)
{
     delete fARDparents;
}
//---------------------------------------------------------------------------

// #brief Zwraca identyfikator wezla
//
// #return identyfikator wezla
unsigned int gARDnode::id(void)
{
     return fID;
}
//---------------------------------------------------------------------------

// #brief Zwraca liste rodzicow w diagramie ARD
//
// #return zwraca liste rodzicow w diagramie ARD
QList<gARDnode*>* gARDnode::ardParents(void)
{
     return fARDparents;
}
//--------------------------------------------------------------------------

// #brief Zwraca index rodzica o zadanym identyfikatorze
//
// #param _pid - identyfikaotr wezla rodzica
// #return index rodzica o zadanym identyfikatorze, lub -1 gdy rodzic o podanym identyfikatorze nie istnieje
int gARDnode::indexOfParent(unsigned int _pid)
{
     for(int i=0;i<fARDparents->size();++i)
          if(fARDparents->at(i)->id() == _pid)
               return i;

     return -1;
}
//--------------------------------------------------------------------------

// #brief Zwraca wskaznik do rodzica
//
// #return zwraca wskaznik do rodzica
ARD_Property* gARDnode::parent(void)
{
	return fParent;
}
//--------------------------------------------------------------------------

// #brief Zwraca wskaznik do rodzica graficznego
//
// #return zwraca wskaznik do rodzica graficznego
gARDlevel* gARDnode::gParent(void)
{
	return fGparent;
}
//--------------------------------------------------------------------------

// #brief Metoda klasy GRect rysujaca kwadrat na ekranie
//
// #param _p - Wskanik do plutna rysowania
// #param styleOption - Wskanik do stylu rysowania
// #param widget - Wskanik nieuzywany
// #return Brak zwracanych wartoci
void gARDnode::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     //_p->drawRect(boundingRect());

     // Rysowanie kwadratu
     _p->setBrush(Settings->ardPropertyColor());
     if(!fParent->canBeTransformated())
          _p->setBrush(Settings->ardTPHnotTransformatedNodeColor());
     if(isSelected())
          _p->setBrush(Settings->ardSelectedPropertyColor());
     if(MainWin->ModelBW())
          _p->setBrush(QColor(255, 255, 255, 255));

     // Wyrysowanie ramki
     _p->setPen(Qt::NoPen);
     if(Settings->useFramedNodes())
     {
          QPen ppen;
          ppen.setWidth(1);
          ppen.setColor(Qt::black);
          ppen.setStyle(Qt::SolidLine);
          if(!fParent->canBeTransformated())
               ppen.setStyle(Qt::DashLine);
          _p->setPen(ppen);
     }

     // Narysowanie prostokata
     QRectF rect = fieldArea();
     _p->drawRect(rect);

     // Wypisanie nazw atrybutow
     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);

     _p->setPen(Qt::SolidLine);
     _p->setPen(Settings->ardPropertyFontColor());
     _p->setFont(nodeFont);
     int fontHeight = fontM.height();
     float leftMargin = HORIZONTALMARGIN/2.;
     float topMargin = VERTICALMARGIN/2.;

     for(int i=0;i<fParent->size();++i)
          _p->drawText(QPointF(leftMargin, topMargin+(float)((i+1)*fontHeight)), fParent->at(i)->name());
     //_p->drawText(QPointF(leftMargin, topMargin+(float)((fParent->size()+1)*fontHeight)), fParent->TPHparent());
}
//---------------------------------------------------------------------------

// #brief Metoda klasy gARDnode wywolywana w momencie pojedynczego klikniecia na obiekcie, prostokacie
//
// #param _e - Wskanik do informacji o zdarzeniu
// #return Brak zwracanych wartoci
void gARDnode::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
     scene()->clearSelection();
     setSelected(true);

     QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy gARDnode wywolywana w momencie zwolnienia przycisku myszki nad obiektem
//
// #param _e - Wskanik do informacji o zdarzeniu
// #return Brak zwracanych wartoci
void gARDnode::mouseReleaseEvent(QGraphicsSceneMouseEvent* _e)
{
     // Zaznaczenie zaleznosci
     if((Settings->ardNodeSelectPolicy() == SELECT_CONNECTIONS)
               ||
               ((_e->modifiers() == Qt::ControlModifier) && (Settings->ardNodeSelectPolicy() == SELECT_CONNECTIONS_WITH_CTRL)))
     {
          // Find all dependencies relates with selected node

          // If not defined graphical level parent
          if(fGparent == NULL)
               return;


          QList<gARDdependency*>* IN_connlist = fGparent->indexOfinout(fParent->id(), "");
          QList<gARDdependency*>* OUT_connlist = fGparent->indexOfinout("", fParent->id());

          for(int i=0;i<IN_connlist->size();++i)
               IN_connlist->at(i)->selectAll();
          for(int i=0;i<OUT_connlist->size();++i)
               OUT_connlist->at(i)->selectAll();

          delete IN_connlist;
          delete OUT_connlist;
     }
     if(fGparent != NULL)
     {
          int levelNumber = fGparent->levelNo();
          if (levelNumber > 0)
               levelNumber--;
          ardLevelList->gLevelList()->alignLevels(levelNumber, true);
     }

     QGraphicsItem::mouseReleaseEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Metoda klasy GRect wywolywana w momencie dwukrotnego klikniecia na obiekcie, prostokacie
//
// #param _e - Wskanik do informacji o zdarzeniu
// #return Brak zwracanych wartoci
void gARDnode::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
     if((_e->button() == Qt::LeftButton) && (beingEdited == false))
     {
          if (ardLevelList->endsWith(fGparent->parent())){
               beingEdited = true;
               _attributeEditor = new ardPropertyEditor(this);
               _attributeEditor->show();
          }
          else
               QMessageBox::about(MainWin,tr("Edit Error"),tr("You can edit only the last Level in the ARD Diagram."));
     }
     QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

void gARDnode::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
     if (ardLevelList->endsWith(fGparent->parent()))
     {
          QMenu menu;
          QAction *editAction = menu.addAction(tr("&Edit"));
          QAction *finalizeAction = menu.addAction(tr("&Finalize"));
          QAction *splitAction = menu.addAction(tr("&Split"));
          QAction *selectedAction = menu.exec(event->screenPos());
          if (selectedAction == finalizeAction)
          {
               if (fParent->size() == 1 && fParent->first()->type() == ARD_ATT_CONCEPTUAL)
               {
                    ardFinalizationEditor* editor = new ardFinalizationEditor(this);
                    editor->show();
               }
               else
                    QMessageBox::about(MainWin,tr("Finalization Error"),tr("You can only Finalize Simple Conceptual Properties in the ARD Diagram."));

          }
          else if (selectedAction == editAction)
          {
               if(beingEdited == false)
               {
                    beingEdited = true;
                    _attributeEditor = new ardPropertyEditor(this);
                    _attributeEditor->show();
               }
          }
          else if (selectedAction == splitAction )
          {
               if (fParent->size() > 1)
               {
                    ardSplitEditor* editor = new ardSplitEditor(this);
                    editor->show();
               }
               else
                    QMessageBox::about(MainWin,tr("Split Error"),tr("You can only Split Properties that have two or more Attributes."));
          }
     }
     // ...

}

// #brief Metoda klasy zwracajaca obszar prostokatny w ktorym sie znajduje obiekt
//
// #return Obszar prostokatny zawierajacy obiekt
QRectF gARDnode::boundingRect() const
{
     return fieldArea();
}
//---------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie zmiany polozenia obiektu
//
// #param change - Zawiera wartosc elementu ktora ulegla zmianie
// #param value - Referencja do wartosci zmienionych
// #return Wartosc ktora ulela zmianie
QVariant gARDnode::itemChange(GraphicsItemChange change, const QVariant &value)
{     
     if((change == ItemPositionChange) && (scene()))
     {
          fGparent->repaintDependencies(fParent);
     }

     if((change == ItemSelectedChange) && (scene()))
     {
          fGparent->repaintDependencies(fParent);
     }

     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Metoda obliczajaca wielksoc pola w ktorym sa wypisywane atrybuty
//
// #return obszar prostoktny w ktorym sa wypisywane atrybuty
QRectF gARDnode::fieldArea(void) const
{
     // Ustalanie rozmiarow wezla
     float x = 0.;
     float y = 0.;
     float width = 0.;
     float height = 0.;

     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);

     for(int i=0;i<fParent->size();++i)
     {
          int currStrWidth = fontM.width(fParent->at(i)->name());
          height += fontM.height();

          if(currStrWidth > width)
               width = currStrWidth;
     }

     width += HORIZONTALMARGIN;
     height += VERTICALMARGIN;

     return QRectF(x, y, width, height);
}
//---------------------------------------------------------------------------

// #brief Metoda ustalajca warto beingEdited
//
// #return bool _beingEdited - ustawia warto beingEdited na _beingEdited
void gARDnode::setBeingEdited(bool _beingEdited)
{
     beingEdited = _beingEdited;
}
//---------------------------------------------------------------------------

// #brief Metoda zwracajca warto beingEdited
//
// #return bool - warto beingEdited
bool gARDnode::getBeingEdited() const
{
     return beingEdited;
}
//---------------------------------------------------------------------------

// #brief Slot uruchamiany po skoczeniu edycji - ustawia beingEdited na false
//
// #return no values return
void gARDnode::finishedEditing()
{
     beingEdited = false;
}
//---------------------------------------------------------------------------
