/**
* \file	ardSplitEditorNode.cpp
* \class  ardSplitEditorNode
* \author Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a QGraphicsItem resembling node that represents
* a Property.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//---------------------------------------------------------------------------

#include <QMessageBox>
#include <QRectF>
#include <QColor>

#include "ardSplitEditorNode.h"
#include "ardSplitEditorWire.h"
#include "ardSplitEditorLoop.h"
#include "../../C/ARD/widgets/ardSplitEditorScene.h"

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Attribute.h"
//---------------------------------------------------------------------------

#define HORIZONTALMARGIN 10.
#define VERTICALMARGIN 10.
//---------------------------------------------------------------------------

ardSplitEditorNode::ardSplitEditorNode(ARD_Property* _parent) :
     #if QT_VERSION >= 0x040600
     QGraphicsObject(0)
   #else
     QGraphicsItem(0)
   #endif
{
     //IMPORTANT: fParent is not deleted by the destructor - must be manually deleted;
     if ( _parent == NULL)
          _parent = new ARD_Property;
     fParent = _parent;

     relation = FutureChild;
     setFlag(ItemIsSelectable);
     setFlag(ItemIsFocusable);
     setFlag(ItemIsMovable);

#if QT_VERSION >= 0x040600
     setFlag(ItemSendsScenePositionChanges);
#endif
}
//---------------------------------------------------------------------------

void ardSplitEditorNode::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Rysowanie kwadratu
     _p->setBrush(Settings->ardPropertyColor());
     if(!fParent->canBeTransformated())
          _p->setBrush(Settings->ardTPHnotTransformatedNodeColor());
     if(isSelected())
          _p->setBrush(Settings->ardSelectedPropertyColor());
     if(MainWin->ModelBW())
          _p->setBrush(QColor(255, 255, 255, 255));

     // Wyrysowanie ramki
     _p->setPen(Qt::NoPen);
     if(Settings->useFramedNodes())
     {
          QPen ppen;
          ppen.setWidth(1);
          ppen.setColor(Qt::black);
          ppen.setStyle(Qt::SolidLine);
          if(!fParent->canBeTransformated())
               ppen.setStyle(Qt::DashLine);
          _p->setPen(ppen);
     }

     // Narysowanie prostokata
     QRectF rect = fieldArea();
     _p->drawRect(rect);

     // Wypisanie nazw atrybutow
     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);

     _p->setPen(Qt::SolidLine);
     _p->setPen(Settings->ardPropertyFontColor());
     _p->setFont(nodeFont);
     int fontHeight = fontM.height();
     float leftMargin = HORIZONTALMARGIN/2.;
     float topMargin = VERTICALMARGIN/2.;

     if (relation == FutureChild)
          for(int i=0;i<fParent->size();++i)
               _p->drawText(QPointF(leftMargin, topMargin+(float)((i+1)*fontHeight)), fParent->at(i)->name());
     else
     {
          for(int i=0;i<fParent->size();++i)
               _p->drawText(QPointF(leftMargin, topMargin+(float)((i+2)*fontHeight)), fParent->at(i)->name());

          // Making the color of the pen visible.
          QRgb rgbcolor = _p->brush().color().rgb();
          _p->setPen(QColor(((rgbcolor/65536)% 256 + 128)%256,((rgbcolor/256)%256 + 128)%256,(rgbcolor%256 + 128)%256));

          if (relation == Independent)
          {
               _p->drawText(QPointF(leftMargin, topMargin+(float)(fontHeight)),QString('I'));
          }
          else if ( relation == Dependent)
          {
               _p->drawText(QPointF(leftMargin, topMargin+(float)(fontHeight)),QString('D'));
          }
     }
}
//---------------------------------------------------------------------------

QRectF ardSplitEditorNode::boundingRect() const
{
     return fieldArea();
}
//---------------------------------------------------------------------------

QVariant ardSplitEditorNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemPositionChange) && (scene()))
     {
          emit repaintDependencies(this);
     }

     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

void ardSplitEditorNode::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
     if ( event->modifiers() == Qt::ShiftModifier)
     {
          QList<QGraphicsItem*> itemList = scene()->selectedItems();
          if (itemList.count() == 1)
          {
               ardSplitEditorNode* lastSelected = \
                         qgraphicsitem_cast<ardSplitEditorNode*>(itemList.first());
               if (lastSelected != NULL) // this needs to be changed, and I need to include in the painting self-dependence;
               {
                    if ( lastSelected->getRelation() == Dependent )
                    {
                         QMessageBox::about(scene()->views().first(),\
                                            tr("Dependency Error"),\
                                            tr("Cannot make a Property that was Dependent to the Property being Split Independent from the Child Property."));
                         QGraphicsItem::mousePressEvent(event);
                    }
                    else if (relation == Independent)
                    {
                         QMessageBox::about(scene()->views().first(),\
                                            tr("Dependency Error"),\
                                            tr("Cannot make a Property that was Independent from the Property being Split Dependent to the Child Property."));
                         QGraphicsItem::mousePressEvent(event);
                    }
                    else
                    {
                         ardSplitEditorScene* ardScene = dynamic_cast<ardSplitEditorScene*>(scene());
                         // Checking if the dependency is already on scene.
                         if (ardScene->existDependency(lastSelected,this))
                              return ;
                         if (lastSelected == this)
                         {
                              ardSplitEditorLoop* newLoop = new ardSplitEditorLoop(this);
                              scene()->addItem(newLoop);
                              newLoop->update();
                         }
                         else
                         {
                              ardSplitEditorWire* newWire = new ardSplitEditorWire(lastSelected,this);
                              scene()->addItem(newWire);
                              newWire->update();
                              setSelected(false);
                         }
                    }
               }
          }
     }
     else
          QGraphicsItem::mousePressEvent(event);
}
//---------------------------------------------------------------------------

QRectF ardSplitEditorNode::fieldArea(void) const
{
     // Ustalanie rozmiarow wezla
     float x = 0.;
     float y = 0.;
     float width = 0.;
     float height = 0.;

     QFont nodeFont = Settings->ardARDdiagramFont();
     QFontMetrics fontM(nodeFont);

     for(int i=0;i<fParent->size();++i)
     {
          int currStrWidth = fontM.width(fParent->at(i)->name());
          height += fontM.height();

          if(currStrWidth > width)
               width = currStrWidth;
     }

     if (relation != FutureChild)
          height += fontM.height();

     width += HORIZONTALMARGIN;
     height += VERTICALMARGIN;

     qreal penWidth = 1;
     return QRectF(x- penWidth / 2 , y - penWidth / 2, width + penWidth, height + penWidth);
}
//---------------------------------------------------------------------------

QPainterPath ardSplitEditorNode::shape() const
{
     QPainterPath path;
     QRect r = fieldArea().toAlignedRect();

     r.adjust(-1,-1,1,1);

     path.addRect(QRectF(r));
     return path;
}
//---------------------------------------------------------------------------

qreal ardSplitEditorNode::verticalMargin()
{
     return VERTICALMARGIN;
}
//---------------------------------------------------------------------------

qreal ardSplitEditorNode::horizontalMargin()
{
     return HORIZONTALMARGIN;
}
//---------------------------------------------------------------------------
