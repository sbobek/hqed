 /**
 *                                                                             *
 * @file gARDlevelList.cpp                                                     *
 * Project name: HeKatE hqed                                                   *
 * $Id: gARDlevelList.cpp,v 1.26.8.2 2011-09-01 21:46:49 hqeddoxy Exp $                *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 06.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy gARDlevelList                   *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../C/MainWin_ui.h"
#include "../../C/widgets/hBaseScene.h"

#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/ARD/ARD_AttributeList.h"
#include "../../M/ARD/ARD_PropertyList.h"

#include "gARDlevel.h"
#include "gARDlevelList.h"
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
gARDlevelList::gARDlevelList(void)
{
	fParent = NULL;
     _display = NULL;
}
// -----------------------------------------------------------------------------

// #brief Konstruktor ustawiajacy rodzica
//
// #param ARD_LevelList* _parent - wzkaznik do rodzica
// #return brak zwracanych wartosci
gARDlevelList::gARDlevelList(ARD_LevelList* _parent) : fParent(_parent)
{
     _display = NULL;
}
// -----------------------------------------------------------------------------

// #brief Destruktor defaultowy klasy
//
// #return brak zwracanych wartosci
gARDlevelList::~gARDlevelList(void)
{
}
// -----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* gARDlevelList::display(void)
{
     return _display;
}
// -----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void gARDlevelList::setDisplay(hBaseScene* __display)
{
     _display = __display;
}
// -----------------------------------------------------------------------------

// #brief Wyrownuje na scenie obszary zajete przez wszystkie poziomy
//
// #param int _fromLevel - okresla poziom startowy - od ktorego zaczac
// #param bool _down - okresla czy od startowego nalezy isc w gore czy w dol
// #return brak zwracanych wartosci
void gARDlevelList::alignLevels(int _fromLevel, bool _down)
{
	if(fParent == NULL)
		return;
		
	int _fromLevelIndex = 0;
	for(int i=0;i<fParent->size();++i)
		if(fParent->at(i)->level() == _fromLevel)
			_fromLevelIndex = i;
	
	float y = 0.;
	float x = -MARGIN_LENGTH/2.;
		
	// Gdy idziemy w dol
	if(_down)
	{
		if(_fromLevelIndex < 0)
			return;
		if(_fromLevelIndex >= 1)
			y = fParent->at(_fromLevelIndex-1)->gLevel()->levelArea().bottom();
		
		for(int i=_fromLevelIndex;i<fParent->size();++i)
		{
			QRectF rect = fParent->at(i)->paint();
			float cy = rect.y();
			float dy = y-cy;
			
			float cx = rect.x();
			float dx = x-cx;
			
               /*QString res = "Parametry funkcji:\n_fromLevelIndex = " + QString::number(_fromLevelIndex);
			res += "\ny startowe = " + QString::number(y);
			res += "\ny poziomu = " + QString::number(cy);
			res += "\nh poziomu = " + QString::number(rect.height());
			res += "\nprzesuwanie poziomu " + QString::number(fParent->at(i)->level()) + " o " + QString::number(dx) + ", " + QString::number(dy);
               QMessageBox::critical(NULL, "gARDlevelList::alignLevels", res);*/
			
			fParent->at(i)->gLevel()->moveLevelBy(dx, dy);
			
			y += rect.height();
		}
	}
	
	// Gdy idziemy w gore
	if(!_down)
	{	
		if(_fromLevelIndex >= fParent->size())
			return;
		if(_fromLevelIndex < (fParent->size()-1))
			y = fParent->at(_fromLevelIndex+1)->gLevel()->levelArea().y();
			
		for(int i=_fromLevelIndex;i>=0;--i)
		{
			QRectF rect = fParent->at(i)->paint();
			float cy = rect.bottom();
			float dy = y-cy;
			
			float cx = rect.x();
			float dx = x-cx;
			
			fParent->at(i)->gLevel()->moveLevelBy(dx, dy);
			
			y -= rect.height();
		}
	}
}
// -----------------------------------------------------------------------------
