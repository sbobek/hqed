/**
* \file      gARDbutton.h
* \class     gARDbutton
* \author    Piotr Held
* \date      15.03.2011
* \version   1.0
* \brief     This file contains class gARDbutton definition which is a customizable
* button for use in the Qt View Framework.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef GARDBUTTON_H
#define GARDBUTTON_H
// -----------------------------------------------------------------------------

#include <QGraphicsItem>
// -----------------------------------------------------------------------------

class QGraphicsItem;
class gARDlevel;
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040600
class gARDbutton : public 	QGraphicsObject
#else
          class gARDbutton : public QObject, public QGraphicsItem
#endif
{
     Q_OBJECT
private:

     
     gARDlevel* fGparent; ///< Graphic representation of the ARD_Level.

     QString fPictureStr;               ///< Path to picture that is currently displayed.
     QString fPictureHoverStr;          ///< Path to picture that will be displayed when item has entered Hover Event.
     QString fPictureHoverStrExp;       ///< Path to picture that will be displayed when item has entered Hover Event and the expander is expanded*/
     QString fPictureNonHoverStr;       ///< Path to picture that will be displayed when item is not Hovered.
     QString fPictureNonHoverStrExp;    ///< Path to picture that will be displayed when item is not Hovered  and the expander is expanded
     
     bool hover; ///< /// This property holds whether mouse hovered the element or not.

public:

     /// \brief Constructor that accepts the ARD_Levels Graphic Parent.
     gARDbutton(gARDlevel*);

     /// \brief Destructor of gARDbutton class
     ~gARDbutton(void);

     // ----------------- Methods that return the value of the fields of this class -----------------

   
     /// \brief Returns a pointer to the graphic parent.
     /// \returns returns a pointer to the graphic parent.
     gARDlevel* gParent(void);

     // ----------------- Operation methods -----------------

     /// \brief Draws the button on scene.
     /// \param _p - pointer to the QPainter on which to perform the draw.
     /// \param styleOption - pointer to the style options.
     /// \param widget - Unused pointer.
     /// \returns No return value.
     void paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget* widget=0);


     /// \brief Method called when the mouse hovers over the Item.
     /// \param _event - pointer to the object holding the parameters of the event.
     /// \returns No return value.
     void hoverEnterEvent(QGraphicsSceneHoverEvent* _event);


     /// \brief Method called when the mouse stops hovering over the Item.
     /// \param _event - pointer to the object holding the parameters of the event.
     /// \returns No return value.
     void hoverLeaveEvent(QGraphicsSceneHoverEvent* _event);


     /// \brief Method called when the mouse is clicked over the Item.
     /// \param _event - pointer to the object holding the parameters of the event.
     /// \returns No return value.
     void mousePressEvent(QGraphicsSceneMouseEvent* _event);

  
     /// \brief Returns the rectangle containing the Item.
     /// \returns Rectangle containing the Item. 
     QRectF boundingRect() const;

     /// \brief Sets the fPictureNonHoverStr.
     /// \param _picStr - reference to string that will be assigned to fPictureNonHoverStr.
     void setPictureNonHoverStr(const QString& _picStr)
     { fPictureNonHoverStr = _picStr; }


     /// \brief Sets the fPictureNonHoverStrExp.
     /// \param _picStr - reference to string that will be assigned to fPictureNonHoverStrExp.
     void setPictureNonHoverStrExp(const QString& _picStr)
     { fPictureNonHoverStrExp = _picStr; }


     /// \brief Sets the fPictureHoverStr.
     /// \param _picStr - reference to string that will be assigned to fPictureHoverStr.
     void setPictureHoverStr(const QString& _picStr)
     { fPictureHoverStr = _picStr; }

     /// \brief Sets the fPictureHoverStrExp.
     /// \param _picStr - reference to string that will be assigned to fPictureHoverStrExp.
     void setPictureHoverStrExp(const QString& _picStr)
     { fPictureHoverStrExp = _picStr; }

     /// \brief Updates the fPictureStr to the apriopriate value.
     void updateCurrentPicStr();

signals:

     /// \brief Slot triggered when Item is clicked.
     void clicked();
};
// -----------------------------------------------------------------------------

#endif // GARDBUTTON_H
