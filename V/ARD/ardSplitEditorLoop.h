/**
* \file     ardSplitEditorLoop.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditorLoop definition of a
* QGraphicsItem resembling a loop that represents a self-dependent Dependency.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDSPLITEDITORLOOP_H
#define ARDSPLITEDITORLOOP_H
// -----------------------------------------------------------------------------

#include <QPen>
#include <QColor>
#include <QGraphicsPathItem>
// -----------------------------------------------------------------------------

class ardSplitEditorNode;
// -----------------------------------------------------------------------------


/**
* \class 	ardSplitEditorLoop
* \author	Piotr Held
* \date 	15.03.2011
* \brief 	Class definition of a QGraphicsItem resembling a loop that represents a self-dependent Dependency.
*/
class ardSplitEditorLoop : public QGraphicsPathItem
{
//     Q_OBJECT
public:
     
     /// \brief Holds the Type of the graphics object.   
     enum { Type = UserType + 5 };//that means Type == 2^10 + 5

 
     /// \brief Only constructor.
     /// \param node - the Node for which this Item will represent a self-dependent Dependency.
     explicit ardSplitEditorLoop(ardSplitEditorNode* node);


     /// \brief Overloaded type function.
     int type() const
         { return Type; }

     /// \brief Overloaded function, returns the boundingRect() of the Loop.
     /// \return The boundingRect of the Loop.     
     QRectF boundingRect() const;
     
     /// \brief Returns a pointer to the ardSplitEditorNode for which this Loop represents a self-deopendent Dependency.
     /// \return Pointer to tha ardSplitEditorNode for which this Loop represents a self-dependent Dependency.
     ardSplitEditorNode *node() const
         { return parentNode; }

protected:
     /// \brief Peforms a paint event.
     /// This method performs a paint and updates the paht of this QGraphicsPathItem.
     /// \param painter - pointer to QPainter on which this should be painted.
     /// \param option, widget - not used parameters.
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
     ardSplitEditorNode* parentNode;    ///< The node for which this Loop represents the self-dependent Dependency.
     QColor myColor;                    ///< The color of the Loop - set to black.
     QPen myPen;                        ///< The Pen of the Loop (when not selected).
     QPen mySelectedPen;                ///< The Pen of the Loop (when selected).
     qreal arrowSize;                   ///< The size of the Arrow of the Loop (set to the size of the text).
     QPolygonF arrowHead;               ///< Represents the Arrow Polygon.
};
// -----------------------------------------------------------------------------

#endif // ARDSPLITEDITORLOOP_H
