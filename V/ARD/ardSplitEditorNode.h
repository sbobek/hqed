/**
* \file     ardSplitEditorNode.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditorNode definition of a
* QGraphicsItem resembling a node that represents a Property.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDSPLITEDITORNODE_H
#define ARDSPLITEDITORNODE_H
// -----------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
// -----------------------------------------------------------------------------

class ARD_Property;
class ardSplitEditorWire;
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040600
class ardSplitEditorNode : public 	QGraphicsObject
#else
          class ardSplitEditorNode : public QObject, public QGraphicsItem
#endif
{
     Q_OBJECT
public:
     /// \brief Defines the types of Nodes that this class can represent.
     enum PropertyRelation{
          FutureChild, ///< Node represents a Property that will become a child of the Property being Split
          Dependent,///< Node represents a Property that was Dependent to the Property being split
          Independent///< Node represents a Property that was Dependent to the Property being split */
     };

     /// \brief Holds the Type of the graphics object.      
     enum { Type = UserType + 1 };

     /// \brief Overloaded type function.
     /// \return The value of the Type enum.
     int type() const
     { return Type; }
 
     /// \brief Constructor.
     /// \param _parent - pointer to the ARD_Property that is represented by this node.
     /// \warning - If _parent == NULL, there is a new ARD_Property created. That Property is not deleted anywhere.    
     explicit ardSplitEditorNode(ARD_Property *_parent = NULL);
    
     /// \brief Overloaded function, returns the boundingRect() of the ardSplitEditorNode.
     /// Function equivalent to fieldArea().
     /// \see fieldArea()
     /// \return The boundingRect of the ardSplitEditorNode.     
     QRectF boundingRect() const;

     /// \brief Manages item changes
     /// When the Node's position changes the Node sends a repaintDependencies(this) signal.
     /// \param change - the change that occured - sth happens when change == ItemPositionChange.
     /// \param value - no influence on this function.

     QVariant itemChange(GraphicsItemChange change, const QVariant &value);

     ///
     /// \brief Controls the mouse press event.
     /// If apriopriate requirements are met a new dependency is made. These requirements are:
     ///  - The Qt::ShifModifier must be pressed.
     ///  - There must be precisely one ardSplitEditorNode Selected when the event occurred.
     ///  - The Selected Node's relation must not be equal to Dependent
     ///  - this Node's relation must not be equal to Independent.    
     void mousePressEvent(QGraphicsSceneMouseEvent *event);


     /// \brief Returns the area containing the ardSplitEditorNode.
     /// \see boundingRect()
     /// \return QRectF that was calculated on the basis of the size that the Attribute names take.
     QRectF fieldArea(void) const;


     /// \brief Returns an enlarged area that contains the ardSplitEditorNode.
     /// \return QPainterPath that contains a rectangle that is slightly bigger than the fieldArea().
     /// \see fieldArea()
     QPainterPath shape() const;

     /// \brief Returns the Vertical Margin of the Node.
     /// \return The Vertical Margin.
     qreal verticalMargin();

     /// \brief Returns the Horizontal Margin of the Node.
     /// \return The Vertical Margin.
     qreal horizontalMargin();

     /// \brief Prepares the scene for a geometry change in the node.
     void prepareRectChange()
     { prepareGeometryChange(); }

     //------------Methods controling the properties of the node---------------//

     /// \brief Changes the ItemIsMovable flag to yes.
     /// \param yes - sets the ItemIsMovable flag to yes.
     void setMovable(bool yes)
     { setFlag(ItemIsMovable,yes); }


     /// \brief Sets the relation to rel.
     /// \param rel the PropertyRelation to which relation will be set to.
     /// \see getRelation()
     void setRelation(PropertyRelation rel)
     { relation = rel; }


     /// \brief Returns the ardSplitEditorNode relation.
     /// \return The properties relation.
     /// \see setRelation()

     PropertyRelation getRelation()
     { return relation; }

     /// \brief Returns the Property which this node represents.
     /// \return Pointer to the Property that this node represents.
     ARD_Property* parentProperty()
     { return fParent; }
signals:

     /// \brief Signal emitted when the nodes position changes.
     /// \param node - the pointer to the Node that changed its position, here always node == this.
     void repaintDependencies(ardSplitEditorNode* node);

protected:

     /// \brief Peforms a paint event.
     /// \param painter - pointer to QPainter on which this should be painted.
     /// \param option, widget - not used parameters.
     void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

private:
     ARD_Property* fParent;        ///< The pointer to the parent ARD_Property. warning NOT DELETED ANYWHERE.
     PropertyRelation relation;    ///< The holds the relation that the node has to the property being splitted.
};
// -----------------------------------------------------------------------------

#endif // ARDSPLITEDITORNODE_H
