 /**
 * \file	gTPHnode.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	02.01.2008 
 * \version	1.0
 * Comment:     Plik zawierajacy definicje obiektu bedacego wezlem diagramu TPH 
 * \brief	This file contains class definition that is a node of TPH diagram
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GTPHNODEH
#define GTPHNODEH
//---------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class QGraphicsItem;
class ARD_Property;
//---------------------------------------------------------------------------


/**
* \class 	gTPHnode
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	02.01.2008
* \brief 	Class definition that is a node of TPH diagram.
*/
class gTPHnode : public QGraphicsItem
{
private:
     
     ARD_Property* fParent; ///< Parent	
     
     ARD_Property* fTPHparent; ///< Parent in TPH diagram
	    
     float fVerticalDistance; ///< Vertical Distance from parent 
	     
     float fHorizontalDistance; ///< Horizontal Distance from parent 

public:

     /// \brief The constructor receiving a parent
     gTPHnode(ARD_Property*);
	
     /// ----------------- Methods that return values of class fields -----------------
	
     /// \brief Returns the vertical distance from a parent
     float verticalDistance(void);
	
     /// \brief Returns the horizontal distance from a parent
     float horizontalDistance(void);
	
     /// \brief Sets the pointer to the parent in the TPH diagram
     void setTPHparent(ARD_Property*);
	
     /// ----------------- Methods for setting the values of class fields -----------------
	
     /// \brief Sets the vertical distance from the parent
     void setVerticalDistance(float);
	
     /// \brief Sets the horizontal distance from the parent
     void setHorizontalDistance(float);

     /// ----------------- Operational Methods -----------------
	
     /// \brief The method of drawing an object
     void paint(QPainter*, const QStyleOptionGraphicsItem*, QWidget* widget=0);

     /// \brief Method called when a double click
     void mouseDoubleClickEvent(QGraphicsSceneMouseEvent*);
	
     /// \brief Method called when the mouse clicks on an object
     void mousePressEvent(QGraphicsSceneMouseEvent*);

     /// \brief Method for determining the square in which it is located an object
     QRectF boundingRect() const;

     /// \brief Method called when an object changes position
     QVariant itemChange(GraphicsItemChange, const QVariant&);
	
     /// \brief Returns the rectangular area for drawing and unsubscribing attributes
     QRectF fieldArea(void) const;
};
//---------------------------------------------------------------------------
#endif
