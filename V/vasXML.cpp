/*
 *	   $Id: vasXML.cpp,v 1.7.4.5.2.9 2011-09-29 09:18:04 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>

#if QT_VERSION >= 0x040300
#include <QXmlStreamWriter>
#endif

#include "../M/ARD/ARD_PropertyList.h"
#include "../M/ARD/ARD_AttributeList.h"
#include "../M/ARD/ARD_DependencyList.h"

#include "../M/hType.h"
#include "../M/hTypeList.h"
#include "../M/XTT/XTT_States.h"
#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_AttributeGroups.h"
#include "../M/XTT/XTT_ConnectionsList.h"
#include "../M/XTT/XTT_CallbackContainer.h"

#include "../C/MainWin_ui.h"
#include "../C/Settings_ui.h"

#include "../namespaces/ns_ard.h"
#include "../namespaces/ns_xtt.h"
#include "../namespaces/ns_hqed.h"

#include "vasXML.h"
// -----------------------------------------------------------------------------

// #brief Exports the given model (or its parts) to XML format
//
// #param __device - the output device (File, Socket, Process, ...)
// #param __formating - indicates of the output should be formated
// #param __parts - parts of the HML file.
// #param __rmsg - result message which contains na error message when exit code indicates na error result
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_HML_2_0(QIODevice* __device, bool __formating, QMap<QString, QString> __parts, QString& __rmsg)
{
    // Checking device accesibility
    if(__device == NULL)
    {
        __rmsg = "The given device is unknown";
        return VAS_XML_UNKNOWN_DEVICE;
    }
    if(!__device->isWritable())
    {
        __rmsg = "The given device is not writable";
        return VAS_XML_DEVICE_NOT_WRITABLE;
    }

    // Retriving model parts
    for(int i=0;i<__parts.keys().size();++i)
    {
        QString key = __parts.keys().at(i);
        QString val = __parts[key];
        QStringList args;

        // These commands require an argument which is a list of element ID
        if((key == "tpe") || (key == "tbl") || (key == "atr") || (key == "clb"))
        {
            if(val.isEmpty())
            {
                __rmsg = "Missing argument for \"" + key + "\" command";
                return VAS_XML_FORMAT_ERROR;
            }

            // Try to split the list of arguments into ids
            args = val.split(",");
        }

        // These commands do not require arguments
        if((key == "all") || (key == "tpes") || (key == "tbls") || (key == "atrs") || (key == "clbs"))
        {
            if(!val.isEmpty())
            {
                __rmsg = "Too many arguments for \"" + key + "\" command";
                return VAS_XML_FORMAT_ERROR;
            }
        }

        // Retrieving model part

        // When all model is required
        if(key == "all")
            return vas_out_HML_2_0(__device, __formating, 0);

        QXmlStreamWriter* outxml = new QXmlStreamWriter(__device);
        if(key == "tpes")
            typeList->out_HML_2_0(outxml);
        else if(key == "tbls")
            TableList->out_HML_2_0(outxml);
        else if(key == "atrs")
            hqed::out_ard_xtt_atts_HML_2_0(outxml, VAS_XML_MODEL_PARTS_XTT_ATTRS);
        else if(key == "clbs")
            CallbackContainer->usedToXML(outxml);
        else if(key == "tpe")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = typeList->indexOfID(id);
                if(idx == -1)
                {
                    delete outxml;
                    __rmsg = "Can't find type ID \"" + id + "\"";
                    return VAS_XML_FORMAT_ERROR;
                }
                typeList->at(idx)->out_HML_2_0(outxml);
            }
        }
        else if(key == "tbl")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = TableList->IndexOfID(id);
                if(idx == -1)
                {
                    delete outxml;
                    __rmsg = "Can't find table ID \"" + id + "\"";
                    return VAS_XML_FORMAT_ERROR;
                }
                TableList->Table(idx)->out_HML_2_0(outxml);
            }
        }
        else if(key == "clb")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                int idx = CallbackContainer->getByID(id);
                if(idx == -1)
                {
                    delete outxml;
                    __rmsg = "Can't find callback ID \"" + id + "\"";
                    return VAS_XML_FORMAT_ERROR;
                }
                CallbackContainer->get(idx)->toXML(outxml);
            }
        }
        else if(key == "atr")
        {
            for(int a=0;a<args.size();++a)
            {
                QString id = args.at(a).simplified();
                XTT_Attribute* attr = AttributeGroups->indexOfAttId(id);
                if(attr == NULL)
                {
                    delete outxml;
                    __rmsg = "Can't find attribute ID \"" + id + "\"";
                    return VAS_XML_FORMAT_ERROR;
                }
                attr->out_HML_2_0(outxml);
            }
        }
        delete outxml;
    }

    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Exports the given scene to SVG file
//
// #param __device - the output device (File, Socket, Process, ...)
// #param __formating - indicates of the output should be formated
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_HML_2_0(QIODevice* __device, bool __formating, int __parts)
{
    if(__device == NULL)
        return VAS_XML_UNKNOWN_DEVICE;
    if(!__device->isWritable())
        return VAS_XML_DEVICE_NOT_WRITABLE;

    if(__parts == 0)
        __parts = VAS_XML_MODEL_PARTS_ARD_PROPS | VAS_XML_MODEL_PARTS_ARD_TPH | VAS_XML_MODEL_PARTS_ARD_DPNDS | VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS | VAS_XML_MODEL_PARTS_XTT_TABLS | VAS_XML_MODEL_PARTS_XTT_STATS | VAS_XML_MODEL_PARTS_XTT_CLBS;

    // klasa QXmlStreamWriter zostala wprowadzona dopiero od Qt w wersji 4.3
#if QT_VERSION >= 0x040300

    QXmlStreamWriter* outxml = new QXmlStreamWriter(__device);
    outxml->setAutoFormatting(__formating);
    outxml->writeStartDocument();
    outxml->writeStartElement("hml");
    outxml->writeAttribute("version", "2.0");
    if((__parts & VAS_XML_MODEL_PARTS_XTT_TYPES) != 0)
        typeList->out_HML_2_0(outxml);
    hqed::out_ard_xtt_atts_HML_2_0(outxml, __parts);
    if((__parts & VAS_XML_MODEL_PARTS_ARD_PROPS) != 0)
        ardPropertyList->out_ard_properties_HML_2_0(outxml);
    if((__parts & VAS_XML_MODEL_PARTS_ARD_TPH) != 0)
        ardPropertyList->out_ard_tph_HML_2_0(outxml);
    if((__parts & VAS_XML_MODEL_PARTS_ARD_DPNDS) != 0)
        ardDependencyList->out_HML_2_0(outxml);
    if((__parts & VAS_XML_MODEL_PARTS_XTT_STATS) != 0)
        States->out_HML_2_0(outxml);
    if((__parts & VAS_XML_MODEL_PARTS_XTT_TABLS) != 0)
        TableList->out_HML_2_0(outxml);
    if((__parts & VAS_XML_MODEL_PARTS_XTT_CLBS) != 0)
        CallbackContainer->usedToXML(outxml);
    outxml->writeEndElement();
    outxml->writeEndDocument();
    delete outxml;
#endif

#if QT_VERSION < 0x040300

    QTextStream out(__device);

    // Wczytywanie danych
    QStringList forOutput, *ts, *ag, *tl, *st;
    forOutput.clear();

    ts = ((__parts & VAS_XML_MODEL_PARTS_XTT_TYPES) != 0)?typeList->out_HML_2_0() : new QStringList;
    ag = hqed::out_ard_xtt_atts_HML_2_0(__parts);
    st = ((__parts & VAS_XML_MODEL_PARTS_XTT_STATS) != 0)?States->out_HML_2_0() : new QStringList;
    tl = ((__parts & VAS_XML_MODEL_PARTS_XTT_TABLS) != 0)?TableList->out_HML_2_0() : new QStringList;

    forOutput << "<hml version=\"2.0\">";
    for(int i=0;i<ts->size();i++)
        forOutput.append("\t" + ts->at(i));
    for(int i=0;i<ag->size();i++)
        forOutput.append("\t" + ag->at(i));
    for(int i=0;i<st->size();i++)
        forOutput.append("\t" + st->at(i));
    for(int i=0;i<tl->size();i++)
        forOutput.append("\t" + tl->at(i));
    forOutput << "</hml>";

    for(int i=0;i<forOutput.size();i++)
        out << forOutput.at(i) + "\n";

    // Kasowanie wskaznikow do danych
    delete ts;
    delete ag;
    delete tl;
    delete st;

#endif

    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------

/// \brief Exports the given model to Drools 5.0 format.
/// \author Lukasz Lysik (llysik@gmail.com)
///
/// \param __deviceRuleFlow
/// \param __deviceDecTab
/// \param __deviceWorkspace
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_Drools_5_0(QIODevice* __deviceRuleFlow, QIODevice* __deviceDecTab, QIODevice* __deviceWorkspace)
{
    // klasa QXmlStreamWriter zostala wprowadzona dopiero od Qt w wersji 4.3
#if QT_VERSION >= 0x040300

    QXmlStreamWriter* outxml = new QXmlStreamWriter(__deviceRuleFlow);
    outxml->setAutoFormatting(Settings->xmlAutoformating());
    outxml->writeStartDocument();
    outxml->writeStartElement("process");
    outxml->writeAttribute("xmlns", "http://drools.org/drools-5.0/process");
    outxml->writeAttribute("xmlns:xs", "http://www.w3.org/2001/XMLSchema-instance");
    outxml->writeAttribute("xs:schemaLocation", "http://drools.org/drools-5.0/process drools-processes-5.0.xsd");
    outxml->writeAttribute("type", "RuleFlow");
    outxml->writeAttribute("name", "ruleflow");
    outxml->writeAttribute("id", "com.sample.ruleflow");
    outxml->writeAttribute("package-name", "com.sample");
    outxml->writeStartElement("header");
    outxml->writeEndElement();

    // write tables
    TableList->out_Drools_5_0(outxml);
    // write coxnnections

    outxml->writeEndElement();
    outxml->writeEndDocument();
    delete outxml;

    QTextStream* outcsv = new QTextStream(__deviceDecTab);
    TableList->out_Drools_5_0_DecTab(outcsv);
    delete outcsv;
#endif

    QTextStream* outjava = new QTextStream(__deviceWorkspace);
    AttributeGroups->out_Drools_Workspace(outjava);
    delete outjava;

    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Imports model from the XML format 
//
// #param __device - the input device (File, Socket, Process, ...)
// #param __msg - the output argument, where the XML reading informations are stored.
// #param __format - the output attribute that conatains the inforamtion about opened file format.
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
// #return exit code:
// #li VAS_XML_NOTHING_DONE - when any operations executed
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
// #li VAS_XML_FORMAT_ERROR - when XML format is not correct
// #li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
// #li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_XML(QIODevice* __device, QString& __msg, QString& __format, int __parts)
{
    // Parsowanie XML
    QDomDocument* domDocument = new QDomDocument;
    QString errorStr;
    int errorLine;
    int errorColumn;
    int result = VAS_XML_NOTHING_DONE;

    if(!domDocument->setContent(__device, true, &errorStr, &errorLine, &errorColumn))
    {
        __msg = QString("Parse error at line %1, column %2:\n%3").arg(errorLine).arg(errorColumn).arg(errorStr);
        delete domDocument;
        return VAS_XML_FORMAT_ERROR;
    }

    // Sprawdzenie czy typ pliku jest obslugiwany
    QDomElement root = domDocument->documentElement();
    if((root.tagName().toLower() != "xttml")  && (root.tagName().toLower() != "ardml") && (root.tagName().toLower() != "attml") && (root.tagName().toLower() != "hml"))
    {
        delete domDocument;
        return VAS_XML_FORMAT_NOT_SUPPORTED;
    }

    // We return the file format that will be read
    __format = root.tagName().toLower() + root.attribute("version", "0.0");

    // Gdy plik typu xttml
    if(root.tagName().toLower() == "xttml")
    {
        // Sprawdzenie wersji formatu
        QString version = root.attribute("version", "0.0");
        if(version != "2.0")
            result = VAS_XML_FORMAT_NOT_SUPPORTED;

        if(__parts == 0)
            __parts = VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS | VAS_XML_MODEL_PARTS_XTT_TABLS;

        if(version == "2.0")
        {
            MainWin->CreateNewXTTProject(false);
            result = vas_in_XTTML_2_0(root, __msg, __parts);
        }
        if(version != "2.0")
            result = VAS_XML_VERSION_NOT_SUPPORTED;
    }

    // Gdy plik typu attml
    if(root.tagName().toLower() == "attml")
    {
        // Sprawdzenie wersji formatu
        QString version = root.attribute("version", "0.0");
        if(version != "1.0")
            result = VAS_XML_FORMAT_NOT_SUPPORTED;

        if(__parts == 0)
            __parts = VAS_XML_MODEL_PARTS_XTT_ATTRS;

        if(version == "1.0")
            result = vas_in_XTTML_2_0(root, __msg, __parts);
        if(version != "1.0")
            result = VAS_XML_VERSION_NOT_SUPPORTED;
    }

    // Gdy plik typu ardml
    if(root.tagName().toLower() == "ardml")
    {
        // Sprawdzenie wersji formatu
        QString version = root.attribute("version", "0.0");
        if(version != "1.0")
            result = VAS_XML_FORMAT_NOT_SUPPORTED;

        if(__parts == 0)
            __parts = VAS_XML_MODEL_PARTS_ARD_PROPS | VAS_XML_MODEL_PARTS_ARD_TPH | VAS_XML_MODEL_PARTS_ARD_DPNDS;

        if(version == "1.0")
        {
            MainWin->CreateNewARDProject();
            result = vas_in_ARDML_1_0(root, __msg, __parts);
        }
        if(version != "1.0")
            result = VAS_XML_VERSION_NOT_SUPPORTED;
    }

    // Gdy plik typu hml
    if(root.tagName().toLower() == "hml")
    {
        // Sprawdzenie wersji formatu
        QString version = root.attribute("version", "0.0");

        if(version == "1.0")
        {
            MainWin->CreateNewARDProject();
            if(__parts == 0)
                __parts = VAS_XML_MODEL_PARTS_ARD_PROPS | VAS_XML_MODEL_PARTS_ARD_TPH | VAS_XML_MODEL_PARTS_ARD_DPNDS;
            result = vas_in_HML_1_0(root, __msg, __parts);
        }
        if(version == "2.0")
        {
            MainWin->CreateNewARDProject();
            MainWin->CreateNewXTTProject(false);
            if(__parts == 0)
                __parts = VAS_XML_MODEL_PARTS_ARD_PROPS | VAS_XML_MODEL_PARTS_ARD_TPH | VAS_XML_MODEL_PARTS_ARD_DPNDS | VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS | VAS_XML_MODEL_PARTS_XTT_TABLS | VAS_XML_MODEL_PARTS_XTT_STATS | VAS_XML_MODEL_PARTS_XTT_CLBS;
            result = vas_in_HML_2_0(root, __msg, __parts);
        }

        if((version != "1.0") && (version != "2.0"))
            result = VAS_XML_VERSION_NOT_SUPPORTED;
    }

    delete domDocument;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Imports model from the ARDML 1.0 format 
//
// #param __root - reference to the root element
// #param __msg - the output argument, where the XML format errors are stored.
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
// #li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
// #li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
// #li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_HML_1_0(QDomElement& __root, QString& __msg, int __parts)
{
    // Sprawdzenie naglowka i wersji formatu HML
    if((__root.tagName().toLower() != "hml") || (__root.attribute("version", "0.0") != "1.0"))
    {
        __msg = "Incorect HML version. Requested version is 1.0";
        return VAS_XML_FORMAT_ERROR;
    }

    bool res = true;

    // Reszta zakladamy ze jest ok
    QDomElement DOMats = __root.firstChildElement("attribute_set");
    QDomElement DOMprs = __root.firstChildElement("property_set");
    QDomElement DOMtph = __root.firstChildElement("tph");
    QDomElement DOMard = __root.firstChildElement("ard");

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_ATTRS) != 0) && (!DOMats.isNull()))
        res = res && ardAttributeList->fromHML_1_0(DOMats);

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_PROPS) != 0) && (!DOMats.isNull()))
        res = res && ardPropertyList->fromHML_1_0(DOMprs);

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_TPH) != 0) && (!DOMtph.isNull()))
        res = res && ardPropertyList->fromHML_1_0(DOMtph);

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_DPNDS) != 0) && (!DOMard.isNull()))
        res = res && ardDependencyList->fromHML_1_0(DOMard);

    if(!res)
    {
        ard::clear();
        return VAS_XML_FORMAT_ERROR;
    }

    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Imports model from the ARDML 1.0 format 
//
// #param __root - reference to the root element
// #param __msg - the output argument, where the XML format errors are stored.
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
// #li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
// #li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
// #li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_HML_2_0(QDomElement& __root, QString& __msg, int __parts)
{
    // Sprawdzenie naglowka i wersji formatu HML
    if((__root.tagName().toLower() != "hml") || (__root.attribute("version", "0.0") != "2.0"))
    {
        __msg = "Incorect HML version. Requested version is 2.0";
        return VAS_XML_FORMAT_ERROR;
    }

    // The result
    int result = VAS_XML_SUCCES;

    // Indicates if the file contains the ard and/or xtt data
    bool hasARD = false;
    bool hasXTT = false;

    QDomElement DOMprps = __root.firstChildElement("properties");
    QDomElement DOMard = __root.firstChildElement("ard");
    QDomElement DOMtph = __root.firstChildElement("tph");
    QDomElement DOMxtt = __root.firstChildElement("xtt");

    hasARD = ((!DOMprps.isNull()) && (!DOMard.isNull()) && (!DOMtph.isNull()));
    hasXTT = (!DOMxtt.isNull());
    if(hasARD)
        __msg += "HASARD";
    if(hasXTT)
        __msg += "HASXTT";

    if((__parts & VAS_XML_MODEL_PARTS_XTT_TYPES) != 0)
        if(typeList->in_HML_2_0(__root, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_XTT_ATTRS) != 0)
        if(AttributeGroups->in_HML_2_0(__root, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_ARD_PROPS) != 0)
        if(ardPropertyList->in_HML_2_0(DOMprps, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_ARD_TPH) != 0)
        if(ardPropertyList->in_HML_2_0(DOMtph, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_ARD_DPNDS) != 0)
        if(ardDependencyList->in_HML_2_0(DOMard, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_XTT_TABLS) != 0)
        if(TableList->in_HML_2_0(__root, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    /*if((__parts & VAS_XML_MODEL_PARTS_XTT_STATS) != 0)
          if(TableList->in_HML_2_0_states(__root, __msg) != 0)
               result = VAS_XML_XTT_SYNTAX_ERROR;*/

    if((__parts & VAS_XML_MODEL_PARTS_XTT_STATS) != 0)
        if(States->in_HML_2_0(__root, __msg) != 0)
            result = VAS_XML_XTT_SYNTAX_ERROR;

    if((__parts & VAS_XML_MODEL_PARTS_XTT_CLBS) != 0)
        if(CallbackContainer->in_XML(__root, __msg))
            result = VAS_XML_XTT_SYNTAX_ERROR;

    return result;
}
// -----------------------------------------------------------------------------

// #brief Imports model from the XTTML 2.0 format 
//
// #param __root - reference to the root element
// #param __msg - the output argument, where the XML format errors are stored.
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
// #li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
// #li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
// #li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_XTTML_2_0(QDomElement& __root, QString& __msg, int __parts)
{
    // restoring old form of operators
    QString preOpVals_any = XTT_Attribute::_any_operator_;
    QString preOpVals_nil = XTT_Attribute::_not_definded_operator_;
    XTT_Attribute::_not_definded_operator_ = "op:nd";
    XTT_Attribute::_any_operator_ = "op:any";

    // Sprawdzenie naglowka i wersji formatu XTTML
    if(((__root.tagName().toLower() != "attml") || (__root.attribute("version", "0.0") != "1.0")) &&
            ((__root.tagName().toLower() != "xttml") || (__root.attribute("version", "0.0") != "2.0")))
    {
        __msg = "Incorect XTTML version. Requested version is 2.0";
        return VAS_XML_FORMAT_ERROR;
    }

    bool res = true;

    // Reszta zakladamy ze jest ok
    QDomElement DOMgl = __root.firstChildElement("group_list");
    QDomElement DOMal = __root.firstChildElement("attribute_list");
    QDomElement DOMtl = __root.firstChildElement("xtt_list_table");
    QDomElement DOMcl = __root.firstChildElement("xtt_list_connections");

    if((__parts & VAS_XML_MODEL_PARTS_XTT_ATTRS) != 0)
    {
        if(!DOMgl.isNull())
            res = res && AttributeGroups->FromATTML_1_0(DOMgl);
        if(!DOMal.isNull())
            res = res && AttributeGroups->FromATTML_1_0(DOMal);
    }

    if((__parts & VAS_XML_MODEL_PARTS_XTT_TABLS) != 0)
    {
        if(!DOMtl.isNull())
            res = res && TableList->FromXTTML_2_0(DOMtl);
        if(!DOMcl.isNull())
            res = res && ConnectionsList->FromXTTML_2_0(DOMcl);
    }

    // restoring operators to current
    XTT_Attribute::_not_definded_operator_ = preOpVals_nil;
    XTT_Attribute::_any_operator_ = preOpVals_any;

    if(!res)
    {
        xtt::clear(false);
        return VAS_XML_FORMAT_ERROR;
    }
    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Imports model from the ARDML 1.0 format 
//
// #param __root - reference to the root element
// #param __msg - the output argument, where the XML format errors are stored.
// #param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
// #return exit code:
// #li VAS_XML_SUCCES - on succes
// #li VAS_XML_UNKNOWN_DEVICE - when device is not specified
// #li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
// #li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
// #li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
// #li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_ARDML_1_0(QDomElement& __root, QString& __msg, int __parts)
{
    // Sprawdzenie naglowka i wersji formatu ARDML
    if((__root.tagName().toLower() != "ardml") || (__root.attribute("version", "0.0") != "1.0"))
    {
        __msg = "Incorect ARDML version. Requested version is 1.0";
        return VAS_XML_FORMAT_ERROR;
    }

    bool res = true;

    // Reszta zakladamy ze jest ok
    QDomElement DOMprp = __root.firstChildElement("properties");
    QDomElement DOMtph = __root.firstChildElement("tph");
    QDomElement DOMard = __root.firstChildElement("ard");

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_PROPS) != 0) && (!DOMprp.isNull()))
        res = res && ardPropertyList->fromARDML_wv(DOMprp);

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_TPH) != 0) && (!DOMtph.isNull()))
        res = res && ardPropertyList->fromARDML_wv(DOMtph);

    if(((__parts & VAS_XML_MODEL_PARTS_ARD_DPNDS) != 0) && (!DOMard.isNull()))
        res = res && ardDependencyList->fromARDML_wv(DOMard);

    if(!res)
    {
        ard::clear();
        return VAS_XML_FORMAT_ERROR;
    }
    return VAS_XML_SUCCES;
}
// -----------------------------------------------------------------------------
