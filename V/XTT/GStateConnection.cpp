/*
 *	   $Id: GStateConnection.cpp,v 1.2 2010-01-08 19:47:55 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"

#include "GStateWire.h"
#include "GStateTable.h"
#include "GStateConnection.h"
//---------------------------------------------------------------------------

// #brief Constructor GStateConnection class.
GStateConnection::GStateConnection(MyGraphicsScene* _scene, XTT_State* _parent) : fParent(_parent), _parentScene(_scene)
{
     fEssentialWiresQuantity = 0;
     for(int i=0;i<5;i++)
          fWire[i] = new GStateWire(true, true, this, i, 0);
     for(int i=0;i<3;i++)
          _parentScene->addItem(fWire[i]);
     _amIvisible = true;

     line_1_pos_3p = 0.5;
	line_1_pos_5p = 0.5;
     line_2_pos = 0.5;
     line_5_pos = 0.5;
	
	// Ustawienia poczatkowe parametrow lokalizacji
	_leftTable = NULL;
     _rightTable = NULL;
	fsri = -1;
	fdri = -1;
	
	// Okresla podstawe odleglosci
	WIRE_SPACE_BASE_SIZE = (float)Settings->xttConnectionsDistance();
	wirespace_2 = .0;
	wirespace_4 = .0;

	// Wczytanie lokalizacji
	setLocalizationParameters();
}
//---------------------------------------------------------------------------

// #brief Destructor GStateConnection class.
GStateConnection::~GStateConnection(void)
{
     for(int i=0;i<5;i++)
     {
          _parentScene->removeItem(fWire[i]);
          delete fWire[i];
     }
}
//---------------------------------------------------------------------------

// #brief Function that makes the connection visible
//
// #return true on success, false when connection cannot be visible
bool GStateConnection::makeVisible(void)
{
     bool canBeVisible = true;
     /*if(fParent->trajectory()->empty())
          canBeVisible = false;*/
          
     if((canBeVisible) && (!_amIvisible))
     {
          for(int i=0;i<3;i++)
               _parentScene->addItem(fWire[i]);
          fEssentialWiresQuantity = 3;
     }
     
     if((!canBeVisible) && (_amIvisible))
     {
          for(int i=0;i<5;i++)
               _parentScene->removeItem(fWire[i]);
          fEssentialWiresQuantity = 0;
     }
     
     _amIvisible = canBeVisible;
     return _amIvisible;
}
//---------------------------------------------------------------------------

// #brief Function that returns if the state has defined trajecotry to the output
//
// #return if the state has defined trajecotry to the output
bool GStateConnection::hasTrajectory(void)
{
     if(fParent == NULL)
          return false;
          
     return fParent->hasTrajectory();
}
//---------------------------------------------------------------------------

// #brief Function reads localization parameters.
//
// #return No return value.
void GStateConnection::setLocalizationParameters(void)
{
     if(fParent == NULL)
          return;
          
     _leftTable = NULL;
     _rightTable = NULL;
     
     // Error handling
     if(fParent->parent() == NULL)
     {
          //QMessageBox::critical(NULL, "HQEd", "Undefined parent of the state \'" + fParent->id() +  "\'.", QMessageBox::Ok);
          return;
     }
     if(XTT_States::outputGroup() == NULL)
     {
          //QMessageBox::critical(NULL, "HQEd", "Undefined state output table.", QMessageBox::Ok);
          return;
     }
     
     // Saving parents
     _leftTable = fParent->parent()->uiTable();
     _rightTable = XTT_States::outputGroup()->uiTable();
     
     // Error handling
     int sri = _leftTable->stateIndex(fParent);
     int dri = _rightTable->stateIndex(fParent);
     if(sri == -1)
     {
          //QMessageBox::critical(NULL, "HQEd", "Incorrect state left parent.\nDoes the parent contains the state?", QMessageBox::Ok);
          return;
     }
     if(dri == -1)
     {
          //QMessageBox::critical(NULL, "HQEd", "Incorrect state right parent.\nDoes the parent contains the state?", QMessageBox::Ok);
          return;
     }
     
     // Saving indexes
     fsri = sri;
     fdri = dri;

	if(WIRE_SPACE_BASE_SIZE == .0)
		WIRE_SPACE_BASE_SIZE = 0.1;
		
	wirespace_2 = WIRE_SPACE_BASE_SIZE*(fsri+1);
	wirespace_4 = WIRE_SPACE_BASE_SIZE*(_rightTable->rowCount()-fdri);
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the parent.
//
// #return Pointer to the parent.
XTT_State* GStateConnection::Parent(void)
{
     return fParent;
}
//---------------------------------------------------------------------------

// #brief Function selects all connections.
//
// #return No return value.
void GStateConnection::SelectAll(void)
{
     for(int i=0;i<5;i++)
     {
          fWire[i]->setConectionActive(true);
          fWire[i]->setZValue(6);
     }
     fParent->selectTrajectory(true);
     //_parentScene->setSelectedConnectionID(fParent->ConnId());
}
//---------------------------------------------------------------------------

// #brief Function deselects connections.
//
// #return No return value.
void GStateConnection::ClearSelection(void)
{
     for(int i=0;i<5;i++)
     {
          fWire[i]->setConectionActive(false);
		fWire[i]->setSelected(false);
          fWire[i]->setZValue(5);
     }
     fParent->selectTrajectory(false);
     _parentScene->setSelectedConnectionID("");
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the line.
//
// #param _index Index of the line.
// #return Pointer to the line. When index is incorrect function returns -1.
GStateWire* GStateConnection::Wires(int _i)
{
     if((_i < 0) || (_i >= 5))
          return NULL;

     return fWire[_i];
}
//---------------------------------------------------------------------------

// #brief Function is called when line localization is changed.
//
// #param _index Index of the line.
// #return No return value.
void GStateConnection::PosChange(int itemIndex)
{
     // Gdy pierwsza pionowa
     if(itemIndex == 1)
     {
          // Pobranie dlugosc pierwszej lini przed danym elementem
          float len = fWire[0]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu
		if(fEssentialWiresQuantity == 3)
			len = (len-wirespace_2)/line_1_pos_3p;
		//if(fEssentialWiresQuantity == 5)
		//	len = (len-wirespace_2)/line_1_pos_5p;

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[1]->x() - fWire[0]->x();
          if(new_len == 0)
               new_len = 1;
			
		if(fEssentialWiresQuantity == 3)
			line_1_pos_3p = (new_len-wirespace_2)/len;
		if(fEssentialWiresQuantity == 5)
			line_1_pos_5p = (new_len-wirespace_2)/wirespace_2;
			
          RePaint();

          return;
     }

     // Gdy srodkowa pozioma
     if(itemIndex == 2)
     {
          // Pobranie dlugosc lini po danym elemencie
          float len = fWire[1]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu dla obu linii
          len /= line_2_pos;

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[2]->y()-fWire[1]->y();
          if(new_len == 0)
               new_len = 1;
          line_2_pos = new_len/len;
          RePaint();

          return;
     }

     // Gdy druga pionowa
     if(itemIndex == 3)
     {
          // Pobranie dlugosc lini po danym elemencie
          //float len = fWire[4]->toolTip().toFloat();

          // Przywrocenie wartosci calej szerokosci podzialu dla obu lini
          //len = (-len)/(line_5_pos+1);

          // Okreslenie aktualnej pozycji elementu
          float new_len = fWire[3]->x()-fWire[4]->x();
          if(new_len == 0)
               new_len = 1;
          //line_5_pos = new_len/len;
          line_5_pos = (-new_len-wirespace_4)/wirespace_4;
          RePaint();

          return;
     }
}
//---------------------------------------------------------------------------

// #brief Function draws connections between tables.
//
// #return No return value.
void GStateConnection::RePaint(void)
{
     if(fsri == -1)
          return;
          
     if(!makeVisible())
          return;
          
     if((_leftTable == NULL) || (_rightTable == NULL) || (fsri == -1) || (fdri == -1))
          return;

     // Okreslenie wspolrzednych poczatku i konca lini
     startP.setX(_leftTable->localization().x() + _leftTable->width());
     startP.setY(_leftTable->localization().y() + _leftTable->rowMiddleY(fsri));
     
     endP.setX(_rightTable->localization().x());
     endP.setY(_rightTable->localization().y() + _rightTable->rowMiddleY(fdri));

     // Ustawienie flagi czy bylo 5 elementow na scenie
     bool is5 = fEssentialWiresQuantity == 5;

     // Okreslenie ilosci potrzebnych elementow polaczenia
     fEssentialWiresQuantity = 5;
     if(startP.x() < (endP.x()-20))
     {
          fEssentialWiresQuantity = 3;
          if(is5)
          {
               // Usuwanie elementow ze ceny
               for(int i=3;i<5;i++)
                    _parentScene->removeItem(fWire[i]);
          }
     }
	
	// Zmienne pomagajace dobrac dlugosc linii
	
	/*
	// Okresla odleglosc pomiedzy liniami wychodzacymi z jednej tabeli
	float line_distance = 10;
	
	// Modyfikator dlugosci linii
	float line_len_coeff = ((float)fsri+1.)*line_distance;
	
	// Okresla kierunek dzialania modyfikatora dlugosci linii
	int llc_direction = 1;
	if((endP.y() - startP.y()) > 0)
		llc_direction = -1;*/
	
     // Ustawienie parameteow linii
     if(fEssentialWiresQuantity == 3)
     {
		float line_1_pos = line_1_pos_3p;
		
          float len1 = ((endP.x()-startP.x())*line_1_pos)+wirespace_2;//+(line_len_coeff*(float)llc_direction);
          float len2 = endP.y() - startP.y();
          float len3 = ((endP.x() - startP.x())*(1-line_1_pos))-wirespace_2;//-(line_len_coeff*(float)llc_direction);

		fWire[1]->setVerticality(true);
          fWire[1]->setLength(len2);
          fWire[1]->setMoveable(true);
          fWire[1]->setCurPos(startP.x()+len1, startP.y());
          fWire[1]->setPos(startP.x()+len1, startP.y());
		//fWire[1]->update();
		
		//fWire[2]->setLabel(fParent->Label());
		fWire[2]->setLabelVisible(true);
		fWire[2]->setLabelAlignment(1);
          fWire[2]->setVerticality(false);
          fWire[2]->setLength(len3);
          fWire[2]->setMoveable(false);
          fWire[2]->setCurPos(startP.x()+len1, startP.y()+len2);
          fWire[2]->setPos(startP.x()+len1, startP.y()+len2);
		//fWire[2]->update();
		
		//fWire[0]->setLabel(fParent->Label());
		fWire[0]->setLabelVisible(true);
		fWire[0]->setLabelAlignment(-1);
          fWire[0]->setVerticality(false);
          fWire[0]->setLength(len1);
          fWire[0]->setMoveable(false);
          fWire[0]->setCurPos(startP.x(), startP.y());
          fWire[0]->setPos(startP.x(), startP.y());
		//fWire[0]->update();
     }

     // Ustawienie parameteow linii
     if(fEssentialWiresQuantity == 5)
     {
          // Ustawianie dlugosci kolejnych lini
          float len1 = wirespace_2*line_1_pos_5p+wirespace_2;
          float len5 = -(wirespace_4*line_5_pos+wirespace_4);

          float len2 = endP.y()-startP.y();
          float len4 = -(endP.y()-startP.y());
          
          // Jezeli na tej samej wysokosci to musimy obejsc tabele
          if((endP.y()-startP.y()) == 0)
          {
               len2 = (float)_leftTable->height()*2.;
               len2 *= line_2_pos;
               len4 = len2;
          }
          else
          {
               len2 *= line_2_pos;
               len4 *= (1-line_2_pos);
          }

		//fWire[0]->setLabel(fParent->Label());
		fWire[0]->setLabelVisible(true);
		fWire[0]->setLabelAlignment(-1);
          fWire[0]->setVerticality(false);
          fWire[0]->setLength(len1);
          fWire[0]->setMoveable(false);
          fWire[0]->setCurPos(startP.x(), startP.y());
          fWire[0]->setPos(startP.x(), startP.y());
		//fWire[0]->update();

          fWire[1]->setVerticality(true);
          fWire[1]->setLength(len2);
          fWire[1]->setMoveable(true);
          fWire[1]->setCurPos(startP.x()+len1, startP.y());
          fWire[1]->setPos(startP.x()+len1, startP.y());
		//fWire[1]->update();


		//fWire[4]->setLabel(fParent->Label());
		fWire[4]->setLabelVisible(true);
		fWire[4]->setLabelAlignment(1);
          fWire[4]->setVerticality(false);
          fWire[4]->setLength(len5);
          fWire[4]->setMoveable(false);
          fWire[4]->setCurPos(endP.x(), endP.y());
          fWire[4]->setPos(endP.x(), endP.y());
		//fWire[4]->update();

          fWire[3]->setVerticality(true);
          fWire[3]->setLength(len4);
          fWire[3]->setMoveable(true);
          fWire[3]->setCurPos(endP.x()+len5, endP.y());
          fWire[3]->setPos(endP.x()+len5, endP.y());
		//fWire[3]->update();

		//fWire[2]->setLabel(fParent->Label());
		fWire[2]->setLabelVisible(true);
		fWire[2]->setLabelAlignment(0);
          fWire[2]->setVerticality(false);
		fWire[2]->setLength(fWire[3]->x() - fWire[1]->x());
          fWire[2]->setMoveable(true);
          fWire[2]->setCurPos(fWire[1]->pos().x(), startP.y()+len2);
          fWire[2]->setPos(fWire[1]->pos().x(), startP.y()+len2);
		//fWire[2]->update();

          // Dodawanie elementow do sceny
          if(!is5)
               for(int i=3;i<5;i++)
                    _parentScene->addItem(fWire[i]);
     }
	
	// Uaktualnienie widoku sceny
	_parentScene->update();
}
//---------------------------------------------------------------------------

// #brief Function that refreshes the view of the connection
//
// #return no values return
void GStateConnection::refreshAll(void)
{
     setLocalizationParameters();
     RePaint();
}
//---------------------------------------------------------------------------
