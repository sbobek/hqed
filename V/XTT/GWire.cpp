/*
 *	   $Id: GWire.cpp,v 1.31.4.1 2010-11-18 16:01:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QGraphicsScene>

#include "../../C/XTT/ConnectionEditor_ui.h"
#include "../../M/XTT/XTT_Connections.h"
#include "../../M/XTT/XTT_ConnectionsList.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "GConnection.h"
#include "GWire.h"
//---------------------------------------------------------------------------

// #brief Constructor GWire class.
//
// #param _isVertical Determines whether the line is vertical.
// #param _isMovalbe Detarmines whether the line can be moved.
// #param _parent Pointer to the parent of the object.
// #param _index Index of the parent of the element.
// #param _length Length of the line.
GWire::GWire(bool _isVertical, bool _isMovable, GConnection* _parent, int _index, int _length)
{
     fIsVertical = _isVertical;
     fIsMoveable = _isMovable;
     fParent = _parent;
     fIndex = _index;
     fLength = _length;
     setFlag(ItemIsSelectable);
     setMoveable(fIsMoveable);
#if QT_VERSION >= 0x040600
     setFlag(ItemSendsGeometryChanges);
#endif
     setCursor(Qt::CrossCursor);

	fLabel = "";
	fLabelVisible = false;
     fIsSelected = false;
	fLabelAlignment = -1;
     level = 0;

     setZValue(5);
}
//---------------------------------------------------------------------------

// #brief Function determines whether the object can be moved.
//
// #param _b Determines whether the object can be moved.
// #return No return values.
void GWire::setMoveable(bool _mov)
{
     setFlag(ItemIsFocusable, _mov);
     setFlag(ItemIsMovable, _mov);
     fIsMoveable = _mov;
}
//---------------------------------------------------------------------------

// #brief Function sets current position of the object.
//
// #param _x Coordinate x.
// #param _y Coordinate y.
// #return No return values.
void GWire::setCurPos(float _x, float _y)
{
     fPos.setX(_x);
     fPos.setY(_y);
}
//---------------------------------------------------------------------------

// #brief Function sets length of the line and saves it into tooltip.
//
// #param _l Lenth of the line.
// #return No return values.
void GWire::setLength(float _l)
{
     setToolTip(QString::number(_l));
     fLength = _l;
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GWire::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Ustalanie wspolrzednych
	float x = 0.;
	float y = 0.;
     float dx = 0.;
     float dy = 0.;

     if(!fIsVertical)
          dx = fLength;
     if(fIsVertical)
          dy = fLength;


     QLineF line(x, y, dx, dy);

     if(!MainWin->ModelBW())
     {
          if(!fParent->Parent()->IsCut())
               _p->setPen(QPen(Settings->xttConnectionColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
          else if(fParent->Parent()->IsCut())
               _p->setPen(QPen(Settings->xttCutConnectionColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
          if(fIsSelected)
               _p->setPen(QPen(Settings->xttSelectedConnectionColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
     }
     else if(MainWin->ModelBW())
     {
          if(!fParent->Parent()->IsCut())
               _p->setPen(QPen(QColor(0, 0, 0, 255), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
          else if(fParent->Parent()->IsCut())
               _p->setPen(QPen(QColor(0, 0, 0, 255), 1, Qt::DashLine, Qt::SquareCap, Qt::RoundJoin));
          if(fIsSelected)
               _p->setPen(QPen(QColor(0, 0, 0, 255), 1, Qt::DotLine, Qt::SquareCap, Qt::RoundJoin));
     }
                                                                
     _p->drawLine(line);
	
	// Wyswietlenie etykiety
	if(fLabelVisible)
	{
		QString labelForShow = fLabel;
		QFontMetrics fm(Settings->xttConnectionLabelFont());
		_p->setFont(Settings->xttConnectionLabelFont());
		_p->setPen(Settings->xttConnectionLabelColor());
		
		// Odciecie nadmiernej ilosci znako w etykiecie
		bool showLabel;
		int labelLength = fm.width(labelForShow);
		for(int i=fLabel.size()-1;i>=0;--i)
		{
			showLabel = true;
			if(!(((fLength > .0) && ((labelLength) <= (fLength - 10))) || ((fLength <= .0) && ((-labelLength) >= (fLength + 10)))))
			{
				labelForShow = fLabel.left(i) + "...";
				labelLength = fm.width(labelForShow);
				showLabel = false;
			}
			else break;
		}
		
		double xp = (fLength>.0)?8:-8; 			// gdy fLabelAlignment == -1
		if(fLabelAlignment == 0)
			xp = (fLength/2.0) - ((double)(labelLength/2)); 
		if(fLabelAlignment == 1)
			xp = (fLength>.0)?fLength-8-(labelLength):-(labelLength)-8;
		
		// Warunek sprawdzajacy czy dlugosc linii polaczenia nie jest za krotka, jezeli tak to ukrywamy podpis
		//if(((fLength > .0) && ((labelLength) <= (fLength - 10))) || ((fLength <= .0) && ((-labelLength) >= (fLength + 10))))
		if(showLabel)
			_p->drawText((int)xp, -5, labelForShow);
	}
	
     //_p->drawRect(boundingRect());
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GWire::boundingRect() const
{
     // Margines
     int margin = fLabelVisible?5:2;

     float dsx = -margin;
     float dsy = -margin;
     
     float dx = margin*2.0;
     float dy = margin*2.0;

     if(!fIsVertical)
     {
          if(fLength < 0)
               dsx = -dsx;
          dx = fLength-(dsx*2);
     }
     if(fIsVertical)
     {
          if(fLength < 0)
               dsy = -dsy;
          dy = fLength-(dsy*2);
     }

     return QRectF(dsx, dsy, dx, dy);
}
//---------------------------------------------------------------------------

// #brief Function is called when object connection is changed.
//
// #param change Object that is changed.
// #param value Reference to changed value.
// #return Changed value.
QVariant GWire::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemSelectedChange) && (scene()) && (level == 0))
     {
		level = 1;
          bool isSel = value.toBool();
          if(isSel)
               fParent->SelectAll();
          else
               fParent->ClearSelection();
		fIsSelected = isSel;
		level = 0;

     }

     if((change == ItemPositionChange) && (scene()) && (level == 0))
     {
          level = 1;
          QPointF newPos = value.toPointF();

          // Gdy pionowa to moze zmienic tylko wartosc x
          if(fIsVertical)
               newPos.setY(fPos.y());

          // Gdy pozioma to moze zmienic tylko wartosc y
          if(!fIsVertical)
               newPos.setX(fPos.x());

          // Wykonaie operacji zwiazanych ze zmiana pozycji tego elementu
          if(scene()->mouseGrabberItem() == this)
               fParent->PosChange(fIndex);

          level = 0;
          return newPos;
     }
     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse clicks on the object.
//
// #param event Pointer to event object.
// #return No return values.
void GWire::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
	scene()->clearSelection();
     QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse double clicks on the object.
//
// #param event Pointer to event object.
// #return No return values.
void GWire::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
     if(_e->button() != Qt::LeftButton)
          return;

     // Wyszukiwanie i edycja tabeli
     int index = ConnectionsList->IndexOfID(fParent->Parent()->ConnId());

     if(index == -1)
          return;

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(false, index);
     ConnEdit->show();
     
     QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function sets label alignment.
//
// #param _t Type of label alignment.
// #return No return values.
void GWire::setLabelAlignment(int _align)
{
	if((_align != -1) && (_align != 0 ) && (_align != 1))
	{
		fLabelAlignment = -1;
		return;
	}
		
	fLabelAlignment = _align;
}
//---------------------------------------------------------------------------

// #brief Function determines whether the connection is active.
//
// #param _b Determines whether the connection is active.
// #return No return values.
void GWire::setConectionActive(bool _stat)
{
	fIsSelected = _stat;
}
//---------------------------------------------------------------------------
