 /**
 * \file	GStateRect.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.11.2009
 * \version	1.0
 * \brief	This file contains class definition that is rectangular (equivalent to the table).
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GSTATERECTH
#define GSTATERECTH
//---------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------
 
class hSet;
class SetEditor_ui;
class GStateTable;
class QGraphicsItem;
class MyGraphicsScene;
//---------------------------------------------------------------------------
/**
* \class 	GStateRect
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.11.2009
* \brief 	Class definition that is rectangular (equivalent to the table).
*/
#if QT_VERSION >= 0x040600
class GStateRect : public 	QGraphicsObject
#else
class GStateRect : public QObject, public QGraphicsItem
#endif
{
     Q_OBJECT

private:

    GStateTable* fParent;	          ///< Pointer to the parent of the parent state table
    MyGraphicsScene* _parentScene;     ///< Pointer to the parent scene
    SetEditor_ui* _setEditor;          ///< Pointer to the set editor widget

    int frLevel;                       ///< Recursion level

    // Coordinates of last clicked cell
    int col;                           ///< Column of last clicked cell
    int row;                           ///< Row of last clicked cell
    hSet* set;                         ///< A set related with the cell

public:

    /// \brief Constructor GStateRect class.
    ///
    /// \param __scene - pointer to the parent scene
    /// \param _ptr - Pointer to the parent of the table.
    GStateRect(MyGraphicsScene* __scene, GStateTable* _ptr);

    /// \brief Destructor GStateRect class.
    ~GStateRect(void);

    /// \brief Function draws square on the screen.
    ///
    /// \param _p Pointer to the canvas.
    /// \param _sogi Pointer to the drawing style.
    /// \param widget Unused pointer.
    /// \return No return value.
    void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

    /// \brief Sets the semaphore of positioning locking
    ///
    /// \param __sepmaphore - value of the semaphore
    /// \return no values return
    void lockPositioning(bool __sepmaphore);

    /// \brief Function is called when mouse double click event is triggered.
    ///
    /// \param _e Pointer to the event object.
    /// \return No return value.
    void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e);

    /// \brief Function is called when mouse click event is triggered.
    ///
    /// \param _e Pointer to the event object.
    /// \return No return value.
    void mousePressEvent(QGraphicsSceneMouseEvent* _e);

    /// \brief Function is called when mouse button is released over the object.
    ///
    /// \param _e Pointer to the event object.
    /// \return No return value.
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* _e);

    /// \brief Function retrieves rectangular in which object is located.
    ///
    /// \return Rectangular in which object is located.
    QRectF boundingRect(void) const;

    /// \brief Function is called when location of the object is changed.
    ///
    /// \param change Changing value.
    /// \param _value Reference to changed values.
    /// \return Value that was changed.
    QVariant itemChange(GraphicsItemChange change, const QVariant& _value);
     
public slots:

    /// \brief Function that is triggered when user finishes the edit process
    ///
    /// \return no values return
    void onEditFinish(void);

    /// \brief Function that is triggered when user finishes the fulledit with ok button
    ///
    /// \return no values return
    void onFullEditFinish(void);
};
//---------------------------------------------------------------------------
#endif
