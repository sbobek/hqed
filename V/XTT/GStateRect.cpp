/*
 *	   $Id: GStateRect.cpp,v 1.2.4.2 2010-11-19 08:17:05 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QAction>
#include <QActionGroup>
#include <QLabel>
#include <QMenu> 
#include <QtGui>

#include <QGraphicsItem>

#include "../../namespaces/ns_hqed.h"

#include "../../M/hSet.h"
#include "../../M/hFunction.h"
#include "../../M/hMultipleValue.h"

#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Column.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../C/XTT/TableEditor_ui.h"
#include "../../C/XTT/CellEditor_ui.h"
#include "../../C/XTT/SetCreator_ui.h"
#include "../../C/XTT/SetEditor_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/StateGraphicsScene.h"

#include "GStateTable.h"
#include "GStateRect.h"
//---------------------------------------------------------------------------

// #brief Constructor GRect class.
// #param _ptr Pointer to the parent of the table.
GStateRect::GStateRect(MyGraphicsScene* __scene, GStateTable* _parent) :
#if QT_VERSION >= 0x040600
     QGraphicsObject(0),
#else
     QGraphicsItem(0),
#endif
     _parentScene(__scene)
{
    fParent = _parent;
    setFlag(ItemIsSelectable);
    setFlag(ItemIsFocusable);
    setFlag(ItemIsMovable);
#if QT_VERSION >= 0x040600
     setFlag(ItemSendsGeometryChanges);
#endif
    setZValue(1);
    frLevel = 0;

    _setEditor = new SetEditor_ui;

    _parentScene->setNewItem(this);
}
//---------------------------------------------------------------------------

// #brief Destructor GStateRect class.
GStateRect::~GStateRect(void)
{
     delete _setEditor;
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GStateRect::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Rysowanie kwadratu
     _p->setPen(Qt::NoPen);
     _p->setBrush(Settings->xttTableColor());
     if(isSelected())
          _p->setBrush(Settings->xttSelectedTableColor());
     if(MainWin->ModelBW())
          _p->setBrush(QColor(255, 255, 255, 255));
     _p->drawRect(0, 0, fParent->width(), fParent->height());
     
     // lines painting
     
     // horizontal
     int dx = 0, 
         dy = 0;
     _p->setPen(QPen(Settings->xttTableBorderColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
     for(int i=-1;i<fParent->rowCount();++i)
     {
          QLineF line(dx, dy, fParent->width(), dy);
          dy += fParent->rowHeight(i);
          _p->drawLine(line);
          
          if(i == (fParent->rowCount()-1))
          {
               QLineF line(dx, dy, fParent->width(), dy);
               _p->drawLine(line);
          }
     }
     
     // vertical
     dx = 0;
     dy = 0;
     for(int i=-1;i<fParent->columnCount();++i)
     {
          QLineF line(dx, dy, dx, fParent->height());
          dx += fParent->columnWidth(i);
          _p->drawLine(line);
          
          if(i == (fParent->columnCount()-1))
          {
               QLineF line(dx, dy, dx, fParent->height());
               _p->drawLine(line);
          }
     }
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse click event is triggered.
//
// #param _e Pointer to the event object.
// #return No return value.
void GStateRect::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
    QList<QGraphicsItem*> items = scene()->items();
    for(int i=0;i<items.size();++i)
        items.at(i)->setSelected(true);

    scene()->clearSelection();
    setSelected(true);

    QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse button is released over the object.
//
// #param _e Pointer to the event object.
// #return No return value.
void GStateRect::mouseReleaseEvent(QGraphicsSceneMouseEvent* _e)
{
    // Zaznaczenie zaleznosci
    if((Settings->xttTableSelectPolicy() == SELECT_CONNECTIONS)
    ||
    ((_e->modifiers() == Qt::ControlModifier) && (Settings->xttTableSelectPolicy() == SELECT_CONNECTIONS_WITH_CTRL)))
    {
        // Find all connections relates with selected table

        // If not defined table parent
        /*if(fParent == NULL)
            return;

        QList<XTT_Connections*>* IN_connlist = ConnectionsList->listOfInConnections(fParent->TableId());
        QList<XTT_Connections*>* OUT_connlist = ConnectionsList->listOfOutConnections(fParent->TableId());

        for(int i=0;i<IN_connlist->size();++i)
            IN_connlist->at(i)->gConnection()->SelectAll();
        for(int i=0;i<OUT_connlist->size();++i)
            OUT_connlist->at(i)->gConnection()->SelectAll();

        delete IN_connlist;
        delete OUT_connlist;*/
    }

    QGraphicsItem::mouseReleaseEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse double click event is triggered.
//
// #param _e Pointer to the event object.
// #return No return value.
void GStateRect::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
    if(_e->button() != Qt::LeftButton)
        return;

    // Sprawdzanie w ktorej komorce nastpilo klikniecie
    col = -2;
    row = -2;

    int width = 0;
    int height = 0;

    // Okreslanie kolumny
    for(int w=-1;w<fParent->columnCount();w++)
    {
        width += fParent->columnWidth(w);
        if(width >= _e->pos().x())
        {
            col = w;
            break;
        }
    }

    // Okreslanie wiersza
    for(int h=-1;h<fParent->rowCount();h++)
    {
        height += fParent->rowHeight(h);
        if(height >= _e->pos().y())
        {
            row = h;
            break;
        }
    }

    // Uruchmienie okienka edycji zawartosci komorki
    if(fParent == NULL)
        return;

    // Jezeli edytujemy komorke
    if((row > -1) && (col > -1))
    {
        // Parametry uruchomienia okienka
        set = fParent->setAt(row, col);
        if(set == NULL)
            return;

        XTT_Attribute* attr = fParent->column(col)->attribute();

        // trying to execute a fast edit mode
        if(Settings->xttTableEditMode() == XTT_TABLE_EDIT_MODE_SIMPLE)
        {
            if(SetCreator != NULL)
                delete SetCreator;
            SetCreator = new SetCreator_ui;
            if(SetCreator->setMode(attr->Type(), set, !attr->multiplicity()) == SET_WINDOW_MODE_OK_YOU_CAN_SHOW)      // if setCreator mode is incorrect for this cell - we must run full edition
            {
                QObject::connect(SetCreator, SIGNAL(okclicked()), this, SLOT(onEditFinish()));
                SetCreator->show();
                return;
            }
        }

        // Entering to the full edition mode
        if(_setEditor != NULL)
            delete _setEditor;
        _setEditor = new SetEditor_ui;
        QObject::connect(_setEditor, SIGNAL(okClicked()), this, SLOT(onFullEditFinish()));

        int smr = _setEditor->setMode(attr->Type(), set, 2, !attr->multiplicity(), false, NULL);
        if(smr == SET_WINDOW_MODE_FAIL)
            return;
        if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
            return;
        _setEditor->show();
    }

    // Jezeli edytujemy naglowek
    if((row == -1) && (col > -1))
    {
    }

    // Jezeli edytujemy etykiete
    if((row > -1) && (col == -1))
    {
        bool ok;
        QString result = QInputDialog::getText(NULL, "State label", "Enter a new state label:", QLineEdit::Normal, fParent->labelAt(row), &ok);
        result = result.simplified();
        result.replace("/", "");
        if(ok)
            fParent->setLabelAt(row, result);
    }

    //QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GStateRect::boundingRect() const
{
     return QRectF(0, 0, fParent->width(), fParent->height());
}
//---------------------------------------------------------------------------

// #brief Sets the semaphore of positioning locking
//
// #param __sepmaphore - value of the semaphore
// #return no values return
void GStateRect::lockPositioning(bool __sepmaphore)
{
     frLevel = 0;
     if(__sepmaphore)
          frLevel = 1;
}
//---------------------------------------------------------------------------

// #brief Function is called when location of the object is changed.
//
// #param _change Changing value.
// #param _value Reference to changed values.
// #return Value that was changed.
QVariant GStateRect::itemChange(GraphicsItemChange change, const QVariant &value)
{
    if((change == ItemSelectedChange) && (scene()) && (frLevel == 0))
    {
        /*frLevel = 1;

        bool isSel = value.toBool();
        if(isSel)
            _parentScene->setSelectedTableID(fParent->TableId());
        else
            _parentScene->setSelectedTableID("");

        frLevel = 0;*/
    }

    if((change == ItemPositionChange) && (scene()) && (frLevel == 0))
    {
        frLevel = 1;

        // Value jest nowa wartoscia polozenia
        QPointF newPos = value.toPointF();
        fParent->setLocalization(newPos);

        if(Settings->xttAutoSceneResize())
        {
            QRectF srect = scene()->sceneRect();
            QRectF brect = boundingRect();
            QRectF newItemRect = QRectF(newPos.x(), newPos.y(), brect.width(), brect.height());
            QRectF requiredRect = srect | newItemRect;

            if(requiredRect != srect)
                scene()->setSceneRect(requiredRect);
        }

        frLevel = 0;
    }
    return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user finishes the fulledit with ok button
//
// #return no values return
void GStateRect::onFullEditFinish(void)
{    
     hSet* result = _setEditor->result();
     set->flush();
     result->copyTo(set);
     
     onEditFinish();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user finishes the edit process
//
// #return no values return
void GStateRect::onEditFinish(void)
{
     XTT_State* state = NULL;
     if(fParent != NULL)
     {
          state = fParent->stateAt(row);
          if(state != NULL)
               state->cancelSimulation();
     }
     _parentScene->update();
}
//---------------------------------------------------------------------------
