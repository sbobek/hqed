/**
 * \file	GWire.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is line connection between tables.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GWireH
#define GWireH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class GConnection;
//---------------------------------------------------------------------------
/**
* \class 	GText
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.05.2007 
* \brief 	Class definition that is line connection between tables.
*/
class GWire : public QGraphicsItem
{
private:

	bool fIsVertical;	     ///< Determines whether the line is vertical.
	bool fIsMoveable;	     ///< Determines whether an element can be moved.

	GConnection* fParent;	///< Pointer to the parent of the element.

	int fIndex;			///< Index of the parent of the element.
	float fLength;		     ///< Length of the line.
	bool fIsSelected;	     ///< Determines whether the object was selected.
	QPointF fPos;		     ///< Coordinates of the object.
	int level;			///< Nesting level of the function itemChange.	
	QString fLabel;		///< Label of the connection.	
	bool fLabelVisible;	     ///< Determines whether the label should be visible.

	/// Determines the way of label alignment.
	/// \li -1 - to the left.
	/// \li 0 - center.
	/// \li 1 - to the right.
	int fLabelAlignment;

public:

	/// \brief Constructor GWire class.
	///
	/// \param _isVertical Determines whether the line is vertical.
	/// \param _isMovalbe Detarmines whether the line can be moved.
	/// \param _parent Pointer to the parent of the object.
	/// \param _index Index of the parent of the element.
	/// \param _length Length of the line.
	GWire(bool _isVertical, bool _isMovalbe, GConnection* _parent, int _index, int _length);

	/// \brief Function draws square on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function retrieves rectangular in which object is located.
	///
	/// \return Rectangular in which object is located.
	QRectF boundingRect() const;

	/// \brief Function is called when object connection is changed.
	///
	/// \param change Object that is changed.
	/// \param value Reference to changed value.
	/// \return Changed value.
	QVariant itemChange(GraphicsItemChange change, const QVariant& value);

	/// \brief Function is called when mouse clicks on the object.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void mousePressEvent(QGraphicsSceneMouseEvent* event);

	/// \brief Function is called when mouse double clicks on the object.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* event);

	/// \brief Function determines whether the line is verticaly.
	///
	/// \param _v Determines whether the line is verticaly.
	/// \return No return values.
	void setVerticality(bool _v){ fIsVertical = _v; }

	/// \brief Function determines whether the label is visible.
	///
	/// \param _v Determines whether the label is visible
	/// \return No return values.
	void setLabelVisible(bool _v){ fLabelVisible = _v; }

	/// \brief Function sets text of the label.
	///
	/// \param _l Text of the label.
	/// \return No return values.
	void setLabel(QString _l){ fLabel = _l; }

	/// \brief Function sets index of the parent of the element.
	///
	/// \param _i Index of the parent of the element.
	/// \return No return values.
	void setIndex(int _i){ fIndex = _i; }

	/// \brief Function sets length of the line and saves it into tooltip.
	///
	/// \param _l Lenth of the line.
	/// \return No return values.
	void setLength(float _l);

	/// \brief Function determines whether the object was selected.
	///
	/// \param _s Determines whether the object was selected.
	/// \return No return values.
	void setActive(bool _s){ fIsSelected = _s; }

	/// \brief Function determines whether the object can be moved.
	///
	/// \param _b Determines whether the object can be moved.
	/// \return No return values.
	void setMoveable(bool _b);

	/// \brief Function sets current position of the object.
	///
	/// \param _x Coordinate x.
	/// \param _y Coordinate y.
	/// \return No return values.
	void setCurPos(float _x, float _y);

	/// \brief Function sets label alignment.
	///
	/// \param _t Type of label alignment.
	/// \return No return values.
	void setLabelAlignment(int _t);

	/// \brief Function determines whether the connection is active.
	///
	/// \param _b Determines whether the connection is active.
	/// \return No return values.
	void setConectionActive(bool _b);

	/// Zwraca czy dany obiekt moze zostac klikniety
	/// \brief Function retrieves decision whether the object can be clicked.
	///
	/// \return Decision whether the object can be clicked.
	bool Selected(void){ return fIsSelected; }

	/// \brief Function retrieves text of the label.
	///
	/// \return Text of the label.
	QString Label(void){ return fLabel; }

	/// \brief Function retrieves decision whether the label is visible.
	///
	/// \return Decision whether the label is visible.
	bool isLabelVisible(void){ return fLabelVisible; }

	/// \brief Function retrieves type of label alignment.
	///
	/// \return Type of label alignment.
	int LabelAlignment(void){ return fLabelAlignment; }
};
//---------------------------------------------------------------------------
#endif
