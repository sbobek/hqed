/**
 * \file	GConnection.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is connection between tables.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef GConnectionH
#define GConnectionH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class GWire;
class MyGraphicsScene;
class XTT_Connections;
//---------------------------------------------------------------------------
/**
* \class 	GConncetion
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.05.2007 
* \brief 	Class definition that is connection between tables.
*/
class GConnection
{
private:

	float WIRE_SPACE_BASE_SIZE;				///< Base distance between connections.
	unsigned int fEssentialWiresQuantity;	///< Number of essential wires.
     bool _amIvisible;                       ///< Indicates if the connection is currently visible

	GWire* fWire[5];			     ///< Table of pointers to connections between tables.
	XTT_Connections* fParent;	     ///< Pointer to the parent connection.
     MyGraphicsScene* _parentScene;     ///< Pointer to the parent scene

	float line_1_pos_3p;	///< Proportion of the first horizontal line with 3 part division.
	float line_1_pos_5p;	///< Proportion of the first horizontal line with 5 part division.
	float line_2_pos;		///< Proportion of the first vertical line with 5 part division.
	float line_5_pos;		///< Proportion of the second vertical line with 5 part division.

	int fsti;	///< Source table index.	
	int fsri;	///< Source row index.	
	int fdti;	///< Destination table index.	
	int fdri;	///< Destination row index.

	QPointF startP, ///< Coordinates of the starting point.
		endP;		///< Coordinates of the ending point.

	float wirespace_2;	///< Distance between connections dependign on the row from which connections go out.	
	float wirespace_4;	///< Distance between connections dependign on the row from which connections go out.

public:

	/// \brief Constructor GConnection class.
     ///
     ///\param _scene - pointer to the scene where object will be displaed
     ///\param _parent - pointer to the parent connection
	GConnection(MyGraphicsScene* _scene, XTT_Connections* _parent);

	/// \brief Destructor GConnection class.
	~GConnection(void);

	/// \brief Function retrieves pointer to the parent.
	///
	/// \return Pointer to the parent.
	XTT_Connections* Parent(void);

	/// \brief Function draws connections between tables.
	///
	/// \return No return value.
	void RePaint(void);

	/// \brief Function selects all connections.
	///
	/// \return No return value.
	void SelectAll(void);

	/// \brief Function deselects connections.
	///
	/// \return No return value.
	void ClearSelection(void);

	/// \brief Function retrieves pointer to the line.
	///
	/// \param _index Index of the line.
	/// \return Pointer to the line. When index is incorrect function returns -1.
	GWire* Wires(int _index);

	/// \brief Function is called when line localization is changed.
	///
	/// \param _index Index of the line.
	/// \return No return value.
	void PosChange(int _index);

	/// \brief Function reads localization parameters.
	///
	/// \return No return value.
	void setLocalizationParameters(void);
     
     /// \brief Function that makes the connection visible
	///
	/// \return true on success, false when connection cannot be visible
     bool makeVisible(void);
};
//---------------------------------------------------------------------------
#endif
