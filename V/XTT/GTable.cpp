/*
 *	   $Id: GTable.cpp,v 1.32.4.1 2012-01-12 19:51:01 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"
#include "../../M/XTT/XTT_Table.h"
#include "GLine.h"
#include "GRect.h"
#include "GText.h"
#include "GNode.h"
#include "GTable.h"
//----------------------------------------------------------------------------

// #brief Constructor GTable class.
//
// #param __scene - Pointer to the parent scene
// #param _p Pointer to the parent of the table.
GTable::GTable(MyGraphicsScene* __scene, XTT_Table* _p) : fParent(_p), _parentScene(__scene)
{
     fHLineCount = 0;
     fVLineCount = 0;
     fTextCount = 0;
     fNodeCount = 0;
     fHLines = NULL;
     fVLines = NULL;
     fBase = NULL;
     fErrBase = NULL;
     fText = NULL;
     fNodes = NULL;

     _amIvisible = false;
     makeVisible();
}
//----------------------------------------------------------------------------

// #brief Destructor GTable class.
GTable::~GTable(void)
{
     removeAll();
}
//----------------------------------------------------------------------------

// #brief Function that makes the table visible
//
// #return true on success, false when table cannot be visible
bool GTable::makeVisible(void)
{
     bool canBeIvisible = true;
     if((fParent->TableType() == 0) && (!Settings->xttShowInitialTables()))
          canBeIvisible = false;
     
     // Checking if the table can be visible
     if((canBeIvisible) && (!_amIvisible))
     {
          fBase = new GRect(_parentScene, fParent); 
          fBase->setPos(fParent->Localization().x(), fParent->Localization().y());
          _parentScene->addItem(fBase);
          _parentScene->resetNewItem();
          _amIvisible = true;
          ReCreate();
          Update();
          RePosition();
     }
     
     if((!canBeIvisible) && (_amIvisible))
          removeAll();
          
     _amIvisible = canBeIvisible;
     return _amIvisible;
}
//----------------------------------------------------------------------------

// #brief Function finds table on the stage and scroll the view to show it completely.
//
// #return No return value.
void GTable::ensureVisible(void)
{
     if(!_amIvisible)
          return;
     
	if(fBase->scene() == NULL)
		return;
          
     fBase->update();    // Odswiezamy scene takze tutaj poniewaz moze to tylko zaznaczenie komorki
          
     if(!Settings->xttEnabledAutocentering())
          return;

	QRectF itemRect = fBase->boundingRect();	
	int xMargin = 0;
	int yMargin = 0;
	float itemWidth = (xMargin*2.)+itemRect.width();
	float itemHeight = (yMargin*2.)+itemRect.height();
	
	for(int s=0;s<fBase->scene()->views().count();++s)
	{
		QRectF sRect = fBase->scene()->views().at(s)->sceneRect();
		if((itemWidth < sRect.width()) && (itemHeight < sRect.height()))
		{
			fBase->scene()->views().at(s)->ensureVisible(fBase, xMargin, yMargin);
			//QMessageBox::question(NULL, "HQEd", "itemWidth=" + QString::number(itemWidth) + ", itemHeight=" + QString::number(itemHeight) + "\nsceneWidth=" + QString::number(sRect.width()) + ", sceneHeight=" + QString::number(sRect.height()), QMessageBox::Ok);
		}
	}
	
	fBase->update();
}
//----------------------------------------------------------------------------

// #brief Function adds new horizontal line.
//
// #return No return value.
void GTable::AddHorLine(void)
{
     GLine** tmp = new GLine*[fHLineCount+1];

     // Kopiowanie
     for(unsigned int i=0;i<fHLineCount;i++)
          tmp[i] = fHLines[i];

     // Usuniecie starej tablicy wskaznikow
     if(fHLineCount)
          delete [] fHLines;

     fHLines = tmp;

     fHLines[fHLineCount] = new GLine(fParent, false, fHLineCount);
     _parentScene->addItem(fHLines[fHLineCount]);
     fHLineCount++;
}
//----------------------------------------------------------------------------

// #brief Function removes last horizontal line.
//
// #return No return value.
void GTable::DeleteHorLine(void)
{    
     GLine** tmp = new GLine*[fHLineCount-1];

     // Kopiowanie
     for(unsigned int i=0;i<fHLineCount-1;i++)
          tmp[i] = fHLines[i];

     // Usuniecie starej tablicy wskaznikow
     if(fHLineCount)
     {
          delete fHLines[fHLineCount-1];
          delete [] fHLines;
     }

     fHLines = tmp;

     fHLineCount--;
}
//----------------------------------------------------------------------------

// #brief Function adds new vertical line.
//
// #return No return value.
void GTable::AddVerLine(void)
{
     GLine** tmp = new GLine*[fVLineCount+1];

     // Kopiowanie
     for(unsigned int i=0;i<fVLineCount;i++)
          tmp[i] = fVLines[i];

     // Usuniecie starej tablicy wskaznikow
     if(fVLineCount)
          delete [] fVLines;

     fVLines = tmp;

     fVLines[fVLineCount] = new GLine(fParent, true, fVLineCount);
     _parentScene->addItem(fVLines[fVLineCount]);
     fVLineCount++;
}
//----------------------------------------------------------------------------

// #brief Function removes last vertical line.
//
// #return No return value.
void GTable::DeleteVerLine(void)
{
     GLine** tmp = new GLine*[fVLineCount-1];

     // Kopiowanie
     for(unsigned int i=0;i<fVLineCount-1;i++)
          tmp[i] = fVLines[i];

     // Usuniecie starej tablicy wskaznikow
     if(fVLineCount)
     {
          delete fVLines[fVLineCount-1];
          delete [] fVLines;
     }

     fVLines = tmp;

     fVLineCount--;
}
//----------------------------------------------------------------------------

// #brief Function adds new text.
//
// #param _tt Type of the text that should be taken.
// #param _row Number of row from which text should be taken.
// #param _col Number of column from which text should be taken.
// #return No return value.
void GTable::AddText(unsigned int _tt, unsigned int _row, unsigned int _col)
{
     GText** tmp = new GText*[fTextCount+1];

     // Kopiowanie
     for(unsigned int i=0;i<fTextCount;i++)
          tmp[i] = fText[i];

     // Usuniecie starej tablicy wskaznikow
     if(fTextCount)
          delete [] fText;

     fText = tmp;

     fText[fTextCount] = new GText(fParent, _tt, _row, _col);
     _parentScene->addItem(fText[fTextCount]);
     fTextCount++;
}
//----------------------------------------------------------------------------

// #brief Function removes last text.
//
// #return No return value.
void GTable::DeleteText(void)
{
     GText** tmp = new GText*[fTextCount-1];

     // Kopiowanie
     for(unsigned int i=0;i<fTextCount-1;i++)
          tmp[i] = fText[i];

     // Usuniecie starej tablicy wskaznikow
     if(fTextCount)
     {
          delete fText[fTextCount-1];
          delete [] fText;
     }

     fText = tmp;

     fTextCount--;
}
//----------------------------------------------------------------------------

// #brief Function adds new node.
//
// #param _row Number of the row.
// #return No return value.
void GTable::AddNode(unsigned int _row)
{
     GNode** tmp = new GNode*[fNodeCount+1];

     // Kopiowanie
     for(unsigned int i=0;i<fNodeCount;i++)
          tmp[i] = fNodes[i];

     // Usuniecie starej tablicy wskaznikow
     if(fNodeCount)
          delete [] fNodes;

     fNodes = tmp;

     fNodes[fNodeCount] = new GNode(fParent, fParent->Row(_row), false);
     _parentScene->addItem(fNodes[fNodeCount]);
     fNodeCount++;
}
//----------------------------------------------------------------------------

// #brief Function removes last node.
//
// #return No return value.
void GTable::DeleteNode(void)
{
     GNode** tmp = new GNode*[fNodeCount-1];

     // Kopiowanie
     for(unsigned int i=0;i<fNodeCount-1;i++)
          tmp[i] = fNodes[i];

     // Usuniecie starej tablicy wskaznikow
     if(fNodeCount)
     {
          delete fNodes[fNodeCount-1];
          delete [] fNodes;
     }

     fNodes = tmp;

     fNodeCount--;
}
//----------------------------------------------------------------------------

// #brief Function creates list of components and set them.
//
// #return No return value.
void GTable::ReCreate(void)
{
     // Checking if the table can be visible
     if(!makeVisible())
          return;

     //QMessageBox::information(NULL, "HQEd", "myk1", QMessageBox::Ok);
     // Obliczanie liczby pionowych lini
     unsigned int vline_quantity = fParent->ColCount() + 1;

     // Poprawa ilosc w zaleznosci od granic pomiedzy kolejnymi kontekstami
     // Kiedy istnieje granica pomiedzy conditional i assert lub retract
     if((fParent->ContextSize(1) > 0) && ((fParent->ContextSize(2) > 0) || (fParent->ContextSize(3) > 0)))
          vline_quantity += 1;

     // Kiedy istnieje granica pomiedzy action i assert lub retract
     if((fParent->ContextSize(4) > 0) && ((fParent->ContextSize(2) > 0) || (fParent->ContextSize(3) > 0)))
          vline_quantity += 1;

     // Kiedy tylko wystepuja konteksty conditional i action
     if((fParent->ContextSize(1) > 0) && (fParent->ContextSize(2) == 0) && (fParent->ContextSize(3) == 0) && (fParent->ContextSize(4) > 0))
          vline_quantity += 1;

     // Obliczanie liczby poziomych lini
     unsigned int hline_quantity = fParent->RowCount() + 3;

     // Obliczanie ilsoc tekstow
     unsigned int text_quantity = fParent->RowCount() * fParent->ColCount() + fParent->ColCount() + 1;

     // Obliczanie liczby potrzebnych wezlow
     unsigned int nodes_quantity = fParent->RowCount();     // Dla tabel inicjalizacyjnych
     if(fParent->TableType() == 1)
          nodes_quantity = fParent->RowCount()*2+1;         // Dla normalnych tabel
     if(fParent->TableType() == 2)
          nodes_quantity = fParent->RowCount()+1;           // Dla tabel koncowych

     // Alokacja pamieci na wybrane elementy

     // Na linie poziome
     for(;hline_quantity < fHLineCount;)
          DeleteHorLine();
     for(;hline_quantity > fHLineCount;)
          AddHorLine();

     // Na linie pionowe
     for(;vline_quantity < fVLineCount;)
          DeleteVerLine();
     for(;vline_quantity > fVLineCount;)
          AddVerLine();

     // Na teksty
     for(;text_quantity < fTextCount;)
          DeleteText();
     for(;text_quantity > fTextCount;)
          AddText(0, 0, 0);

     // Na wezly
     for(;nodes_quantity < fNodeCount;)
          DeleteNode();
     for(;nodes_quantity > fNodeCount;)
          AddNode(0);
 
     // Tworzenie kwadratu na tlo tabeli
     //if(fBase)
          //delete fBase;
     //fBase = new GRect(fParent);
     //fBase->setPos(fParent->Localization().x(), fParent->Localization().y());
     //_parentScene->addItem(fBase);
     //QMessageBox::information(NULL, "HQEd", "myk2", QMessageBox::Ok);
}
//----------------------------------------------------------------------------

// #brief Function updates parameters.
//
// #return No return value.
void GTable::Update(void)
{
     // Checking if the table can be visible
     /*bool makevisprev = _amIvisible;
     bool makevis = makeVisible();
     if((!makevisprev) && (makevis))
          ReCreate();*/
     if(!makeVisible())
          return;

     //QMessageBox::information(NULL, "HQEd", "myk3", QMessageBox::Ok);
     // Ustawiamy parametry obiektow

     // Ustawianie lini poziomych
     for(unsigned int i=0;i<fHLineCount;i++)
          fHLines[i]->setVerticality(false);

     // Ustawianie lini pionowych
     for(unsigned int i=0;i<fVLineCount;i++)
          fVLines[i]->setVerticality(true);

     // Ustawianie parametrow tekstow

     // Teksty naglowkow
     for(int i=0;i<fParent->ColCount();i++)
     {
          fText[i]->setCol(i);
          fText[i]->setTextType(2);
     }

     // Teksty kmorek
     for(int i=0;i<fParent->ColCount();i++)
     {
          for(unsigned int j=0;j<fParent->RowCount();j++)
          {
               int index = fParent->ColCount()+i*fParent->RowCount()+j;
               fText[index]->setRow(j);
               fText[index]->setCol(i);
               fText[index]->setTextType(0);
          }
     }

     // Tekst tytulu, powinien byc ostatni
     fText[fTextCount-1]->setTextType(1);
     
     // Index wezla startowego
     unsigned int startIndex = 0;
     
     // Je�eli tabela istnieje i nie jest inicjalizacyjna to ustawiamy pierwszy wezel
     if(fParent->TableType() != 0)
     {
          // Je�eli tabela nie jest inicjalizacyjna to ustawiamy pierwszy wezel na HEADER
          fNodes[startIndex]->setRowParent(NULL);
          startIndex++;
     }

     // Ustawianie wezlow
     for(unsigned int i=startIndex;i<fParent->RowCount()+startIndex;++i)
     {
          fNodes[i]->setRowParent(fParent->Row(i));
          if(fParent->TableType() == 1)
               fNodes[fParent->RowCount()+i]->setRowParent(fParent->Row(i-startIndex));
     }
          
     //QMessageBox::information(NULL, "HQEd", "myk4", QMessageBox::Ok);
}
//----------------------------------------------------------------------------

// #brief Function sets elements once again.
//
// #return No return value.
void GTable::RePosition(void)
{
     // Checking if the table can be visible
     /*bool makevisprev = _amIvisible;
     bool makevis = makeVisible();
     if((!makevisprev) && (makevis))
     {
          ReCreate();
          Update();
     }*/
     if(!makeVisible())
          return;
          
     // Kolejne linie
     int next_x = fParent->Localization().x();
     int next_y = fParent->Localization().y();
	
	// Kwadrat bazowy
	fBase->setPos(next_x, next_y);

     // Rysowanie lini poziomych
     fHLines[0]->setPos(next_x, next_y);
     next_y += fParent->HeaderHeight();
     fHLines[1]->setPos(next_x, next_y-2);
     for(unsigned int i=2;i<fHLineCount;i++)
     {
          fHLines[i]->setPos(next_x, next_y);
          next_y += fParent->RowHeight(i-2);
     }

     // Rysowanie lini pionowych
     next_y = fParent->Localization().y();
     for(int i=0;i<=fParent->ColCount();i++)
     {
          fVLines[i]->setPos(next_x, next_y);
          next_x += fParent->ColWidth(i);
     }

     // Rysowanie lini specjalnych (podwojnych)
     int dc = fVLineCount - fParent->ColCount();

	// Okresla przesuniecie drugiej linii odzielajacej konteksty
	const int line_offset = 2;
	
     // Jezeli sa to dwie linie to trzeba je wstawic po kontekscie contidional i przed action
     if(dc == 3)
     {
          // Wstawianie po kontekscie conditional
          next_x = fParent->Localization().x();
          next_y = fParent->Localization().y();
          for(int i=0;i<fParent->ColCount();i++)
          {
               if(fParent->ColContext(i) != 1)
               {
                    fVLines[fVLineCount-2]->setPos(next_x+line_offset, next_y);
                    break;
               }
               next_x += fParent->ColWidth(i);
          }

          // Wstawianie przed kontekst action
          next_x = fParent->Localization().x() + fParent->Width();
          next_y = fParent->Localization().y();
          for(unsigned int i=fParent->ColCount()-1;((int)i)>=0;i--)
          {
               if(fParent->ColContext(i) != 4)
               {
                    fVLines[fVLineCount-1]->setPos(next_x+line_offset, next_y);
                    break;
               }
			next_x -= fParent->ColWidth(i);
          }
     }

     // Jezeli jest to tylko jedna linia nadmiarowa to trzeba ja wstawic po kontekscie
     // conditional, lub action
     if(dc == 2)
     {
          if(fParent->ContextSize(1) > 0)
          {
               // Wstawianie po kontekscie conditional
               next_x = fParent->Localization().x();
               next_y = fParent->Localization().y();
               for(int i=0;i<fParent->ColCount();i++)
               {
                    if(fParent->ColContext(i) != 1)
                    {
                         fVLines[fVLineCount-1]->setPos(next_x+line_offset, next_y);
                         break;
                    }
                    next_x += fParent->ColWidth(i);
               }
          }

          if((fParent->ContextSize(4) > 0) && (fParent->ContextSize(1) == 0))
          {
               // Wstawianie przed kontekst action
               next_x = fParent->Localization().x() + fParent->Width();;
               next_y = fParent->Localization().y();
               for(unsigned int i=fParent->ColCount()-1;((int)i)>=0;i--)
               {
                    if(fParent->ColContext(i) != 4)
                    {
                         fVLines[fVLineCount-1]->setPos(next_x+line_offset, next_y);
                         break;
                    }
                    next_x -= fParent->ColWidth(i-1);
               }
          }
     }

     // Wypisanie tekstow

     // Teksty naglowkow
     next_x = fParent->Localization().x();
     next_y = fParent->Localization().y();
     for(int i=0;i<fParent->ColCount();i++)
     {
          fText[i]->setPos(next_x, next_y);
          next_x += fParent->ColWidth(i);
     }

     // Teksty komorek
     next_x = fParent->Localization().x();
     for(int c=0;c<fParent->ColCount();c++)
     {
          next_y = fParent->Localization().y() + fParent->HeaderHeight();
          for(unsigned int r=0;r<fParent->RowCount();r++)
          {
               int index = fParent->ColCount() + c*fParent->RowCount() + r;
               fText[index]->setPos(next_x, next_y);

               next_y += fParent->RowHeight(r);
          }
          next_x += fParent->ColWidth(c);
     }

     // Tekst podpisu
     fText[fTextCount-1]->setPos(fParent->Localization().x(), fParent->Localization().y() + (int)fParent->Height());

     // Ustawianie wezlow
     next_x = fParent->Localization().x();
     next_y = fParent->Localization().y() + fParent->HeaderHeight();
     
     unsigned int startIndex = 0;
     if((fParent->TableType() != 0) && (fNodeCount > 0))
     {
          int y = fParent->Localization().y() + (fParent->HeaderHeight() >> 1);
          fNodes[startIndex]->setPos(next_x-8.5, y);
          fNodes[startIndex]->setPosition(next_x-8.5, y);
          fNodes[startIndex]->setIncome(true);
           // Zapisanie parametrow wezla do obiektu
          fNodes[startIndex]->setToolTip("true;" + fParent->TableId() + ";");
          startIndex++;
     }
     
     for(unsigned int i=startIndex;i<fParent->RowCount()+startIndex;++i)
     {
          int y = next_y + (fParent->RowHeight(i-startIndex)/2);

          // Gdy tabela jest typu initialize
          if(fParent->TableType() == 0)
          {
               fNodes[i]->setPos(next_x+(int)fParent->Width()+1, y);
               fNodes[i]->setPosition(next_x+(int)fParent->Width()+1, y);
               fNodes[i]->setIncome(false);
               // Zapisanie parametrow wezla do obiektu
               fNodes[i]->setToolTip("false;" + fParent->TableId() + ";" + fParent->Row(i-startIndex)->RowId());
          }

          // Gdy tabela jest typu middle
          if(fParent->TableType() == 1)
          {
               fNodes[i]->setPos(next_x-8.5, y);
               fNodes[i]->setPosition(next_x-8.5, y);
               fNodes[i]->setIncome(true);
                // Zapisanie parametrow wezla do obiektu
               fNodes[i]->setToolTip("true;" + fParent->TableId() + ";" + fParent->Row(i-startIndex)->RowId());

               fNodes[fParent->RowCount()+i]->setPos(next_x+(int)fParent->Width()+1, y);
               fNodes[fParent->RowCount()+i]->setPosition(next_x+(int)fParent->Width()+1, y);
               fNodes[fParent->RowCount()+i]->setIncome(false);
			
                // Zapisanie parametrow wezla do obiektu
               fNodes[fParent->RowCount()+i]->setToolTip("false;" + fParent->TableId() + ";" + fParent->Row(i-startIndex)->RowId());
          }
          
          // Gdy tabela jest typu halt
          if(fParent->TableType() == 2)
          {
               fNodes[i]->setPos(next_x-8.5, y);
               fNodes[i]->setPosition(next_x-8.5, y);
               fNodes[i]->setIncome(true);
               
                // Zapisanie parametrow wezla do obiektu
               fNodes[i]->setToolTip("true;" + fParent->TableId() + ";" + fParent->Row(i-startIndex)->RowId());
          }
               
          next_y += fParent->RowHeight(i-startIndex);
     }
	
	// Uaktualnienie widoku sceny
	_parentScene->update();

     //QMessageBox::information(NULL, "HQEd", "myk6", QMessageBox::Ok);
}
//----------------------------------------------------------------------------

// #brief Function select the table.
//
// #return No return value.
void GTable::setTableSelection(bool _sel)
{
     if(fBase == NULL)
          return;
          
     fBase->setSelected(_sel);
	fBase->update();
}
//----------------------------------------------------------------------------

// #brief Allow for select header cell
//
// #param int _colIndex - index of column in which the header should be selected
// #param bool _selStat - selection status
// #return No return value
void GTable::selectHeader(int _colIndex, bool _selStat)
{
	if((_colIndex < 0) || (_colIndex >= fParent->ColCount()) || (_colIndex >= (int)fTextCount))
		return;
		
	fText[_colIndex]->selectHeaderCell(_selStat);
}
//----------------------------------------------------------------------------

// #brief Allow for clear selection of all the elements that table consist of
//
// #return No return value
void GTable::unselectAllElements(void)
{
     if(fHLineCount)
     {
          for(unsigned int i=0;i<fHLineCount;i++)
               fHLines[i]->setSelected(false);
     }

     if(fVLineCount)
     {
          for(unsigned int i=0;i<fVLineCount;i++)
               fVLines[i]->setSelected(false);
     }

     if(fTextCount)
     {
          for(unsigned int i=0;i<fTextCount;i++)
               fText[i]->setSelected(false);
     }

     if(fNodeCount)
     {
          for(unsigned int i=0;i<fNodeCount;i++)
               fNodes[i]->setSelected(false);
     }

     if(fBase)
          fBase->setSelected(false);

     if(fErrBase)
          fErrBase->setSelected(false);
}
//----------------------------------------------------------------------------

// #brief Function that removes all the items from the scene
//
// #return No return value
void GTable::removeAll(void)
{
     if(fHLineCount)
     {
          for(unsigned int i=0;i<fHLineCount;i++)
          {
               _parentScene->removeItem(fHLines[i]);
               delete fHLines[i];
          }
          delete [] fHLines;
          fHLineCount = 0;
     }

     if(fVLineCount)
     {
          for(unsigned int i=0;i<fVLineCount;i++)
          {
               _parentScene->removeItem(fVLines[i]);
               delete fVLines[i];
          }
          delete [] fVLines;
          fVLineCount = 0;
     }

     if(fTextCount)
     {
          for(unsigned int i=0;i<fTextCount;i++)
          {
               _parentScene->removeItem(fText[i]);
               delete fText[i];
          }
          delete [] fText;
          fTextCount = 0;
     }

     if(fNodeCount)
     {
          for(unsigned int i=0;i<fNodeCount;i++)
          {
               _parentScene->removeItem(fNodes[i]);
               delete fNodes[i];
          }
          delete [] fNodes;
          fNodeCount = 0;
     }

     if(fBase)
     {
          _parentScene->removeItem(fBase);
          delete fBase;
     }
     fBase = NULL;

     if(fErrBase)
     {
          _parentScene->removeItem(fErrBase);
          delete fErrBase;
     }
     fErrBase = NULL;
}
//----------------------------------------------------------------------------
     
