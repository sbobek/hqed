 /**
 * \file	GRect.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	15.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is rectangular (equivalent to the table).
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GRectH
#define GRectH
//---------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------
class QGraphicsProxyWidget;
class XTT_Table;
class XTT_Cell;
class MyGraphicsScene;
class ExpressionEditor_ui;
class CellEditorAdapter;
class CellExpressionTextEditor;
//---------------------------------------------------------------------------

/**
* \class 	GRect
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	15.05.2007 
* \brief 	Class definition that is rectangular (equivalent to the table).
*/

class GRect : public QObject, public QGraphicsItem
{
    Q_OBJECT

private:

        void clickedRowColumn(QGraphicsSceneMouseEvent* _e, int &row, int &column);
        void updateFocus(QGraphicsSceneMouseEvent* _e);
        void openConditionalEditor(XTT_Cell *cell, int row, int col);

        XTT_Table* fParent;                     ///< Pointer to the parent of the table.
        MyGraphicsScene* _parentScene;          ///< Pointer to the parent scene
        ExpressionEditor_ui* _expr_editor;      ///< Pointer to the expresion editor window
        int frLevel;                            ///< Recursion level
        CellEditorAdapter* cellEditor;
        CellExpressionTextEditor* cellExpressionEditor;
        QGraphicsProxyWidget* editorWidget;

protected:
        void keyPressEvent(QKeyEvent *event);

public:

	/// \brief Constructor GRect class.
        ///
	/// \param __scene - pointer to the parent scene
	/// \param _ptr Pointer to the parent of the table.
        GRect(MyGraphicsScene* __scene, XTT_Table* _ptr, QObject *parent = 0);

	/// \brief Function draws square on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function is called when mouse double click event is triggered.
	///
	/// \param _e Pointer to the event object.
	/// \return No return value.
	void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e);

	/// \brief Function is called when mouse click event is triggered.
	///
	/// \param _e Pointer to the event object.
	/// \return No return value.
	void mousePressEvent(QGraphicsSceneMouseEvent* _e);

	/// \brief Function is called when mouse button is released over the object.
	///
	/// \param _e Pointer to the event object.
	/// \return No return value.
	void mouseReleaseEvent(QGraphicsSceneMouseEvent* _e);

	/// \brief Function retrieves rectangular in which object is located.
	///
	/// \return Rectangular in which object is located.
	QRectF boundingRect() const;

	/// \brief Function is called when location of the object is changed.
	///
	/// \param _change Changing value.
	/// \param _value Reference to changed values.
	/// \return Value that was changed.
        QVariant itemChange(GraphicsItemChange _change, const QVariant& _value);

private slots:
        void slotEditorCancelled();
        void slotEditorFinished();
};
//---------------------------------------------------------------------------
#endif
