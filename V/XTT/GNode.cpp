/*
 *	   $Id: GNode.cpp,v 1.31.4.1 2010-11-18 16:01:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../namespaces/ns_xtt.h"

#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Row.h"
#include "../../M/XTT/XTT_ConnectionsList.h"
#include "GLine.h"
#include "GRect.h"
#include "GText.h"
#include "GTable.h"
#include "GNode.h"
//---------------------------------------------------------------------------

// #brief Contructor GNode class.
//
// #param _t_parent Pointer to the parent of the table.
// #param _r_parent Pointer to the parent of the row.
// #param _income Determines whether connection is income.
GNode::GNode(XTT_Table* _t_parent, XTT_Row* _r_parent, bool _income)
{
     fTableParent = _t_parent;
     fRowParent = _r_parent;
     isIncome = _income;

     setCursor(Qt::CrossCursor);
     setFlag(ItemIsSelectable);
     setFlag(ItemIsFocusable);
     setFlag(ItemIsMovable);
#if QT_VERSION >= 0x040600
     setFlag(ItemSendsGeometryChanges);
#endif
     setZValue(7);
}
//---------------------------------------------------------------------------

// #brief Function draws object on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GNode::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
    // Rysowanie kwadratu
    _p->setPen(Qt::NoPen);
    _p->setBrush(Settings->xttTableBorderColor());
    QPointF points[3] = {
        QPointF(0.0, -5.0),
        QPointF(0.0, 5.0),
        QPointF(8.5, 0.0)
    };
    _p->drawPolygon(points, 3);

    // Rysowanie linii
    float x, y, dx, dy;
    x = .0;
    y = .0;
    dx = fPosition.x() - scenePos().x();
    if(isIncome)
        dx += 8.5;
    dy = fPosition.y() - scenePos().y();
    _p->setPen(Qt::SolidLine);
    _p->drawLine(QPointF(x, y), QPointF(dx, dy));
    //_p->drawRect(boundingRect());
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GNode::boundingRect() const
{
    float x, y, dx, dy;

    x = .0;
    y = -5.0;
    dx = (fPosition.x() - scenePos().x());
    dy = (fPosition.y() - scenePos().y());

    if(dx < .0)
    {
        x = 8.5;
        dx -= 8.5;
    }
    if(dx >= .0)
        dx += 8.5;

    if(dy < .0)
    {
        y = 5.;
        dy -= 10.;
    }
    if(dy >= .0)
        dy += 10.;

    return QRectF(x, y, dx, dy);
}
//---------------------------------------------------------------------------

// #brief Function is called when location of the object is changed.
//
// #param _change Changing value.
// #param _value Reference to changed values.
// #return Value that was changed.
QVariant GNode::itemChange(GraphicsItemChange change, const QVariant &value)
{
     /*if((change == ItemPositionChange) && (scene()))
     {
         scene()->update(boundingRect());
     }*/
     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse button is released on the scene.
//
// #param _gsme Pointer to the event object.
// #return No return value.
void GNode::mouseReleaseEvent(QGraphicsSceneMouseEvent* _gsme)
{
     if(_gsme->button() != Qt::LeftButton)
          return;

     QList<QGraphicsItem*> res = scene()->items(_gsme->scenePos());

     // Sprawdzanie wystapienie elementu wezla pod zwolnionym przyciskiem
     for(int i=0;i<res.size();i++)
     {
          // Sprawdzanie czy to nie jest ten sam element ktory wlasnie przenosimy
          if(res.at(i)->isSelected())
               continue;

          // Tworzenie listy parametrow
          QStringList params = res.at(i)->toolTip().split(";");

          // Jezeli lista zaiwera nie poprawna ilosc znakow pusta
          if(params.size() != 3)
               continue;

          // Tworzenie polaczenia
          QString stid = "";                     // ID tabeli z ktorej wychodzi to polaczenie
          QString srid = "";                     // ID wiersza z ktorego wychodzi to polaczenie
          QString dtid = "";                     // ID tabeli do ktorej przychodzi to polaczenie
          QString drid = "";                     // ID wiersza do ktorego przychodzi to polaczenie

          if(params.at(0) == "true")
          {
               stid = fTableParent->TableId();                     
               srid = fRowParent->RowId();
               dtid = params.at(1);
               drid = params.at(2);
          }
          else if(params.at(0) == "false")
          {
               dtid = fTableParent->TableId();
               if(fRowParent != NULL)
                    drid = fRowParent->RowId();
               stid = params.at(1);
               srid = params.at(2);
          }
          else if((params.at(0) != "false") && (params.at(0) != "true"))
               continue;
          if(params.at(0) == toolTip().split(";").at(0))
               continue;

          int conn_ind = -1;

          // Sprawdzanie czy dane polaczenie juz nie istnieje
          QList<int>* conn_list = ConnectionsList->IndexOfOutConnections(stid, srid);
          if(conn_list->size() > 0)
               conn_ind = conn_list->at(0);
          delete conn_list;
          if(conn_ind == -1)
          {
               ConnectionsList->Add();
               conn_ind = (int)ConnectionsList->Count()-1;
          }
		
          // Po to zeby nie wystapily bledy podczas odswiezenia rysunku
          ConnectionsList->Connection(conn_ind)->setSrcTable("");
          ConnectionsList->Connection(conn_ind)->setSrcRow("");
          ConnectionsList->Connection(conn_ind)->setDesTable("");
          ConnectionsList->Connection(conn_ind)->setDesRow("");

          // Wlasciew ustawianie wartosci
          ConnectionsList->Connection(conn_ind)->setSrcTable(stid);
          ConnectionsList->Connection(conn_ind)->setSrcRow(srid);
          ConnectionsList->Connection(conn_ind)->setDesTable(dtid);
          ConnectionsList->Connection(conn_ind)->setDesRow(drid);
		
		// Analiza wszystkich poalczaen tabel i zrodlowej i koncowej
		TableList->AnalyzeTableConnections(stid);
		TableList->AnalyzeTableConnections(dtid);
		
		// Ustawienie pozycji polaczenia
		ConnectionsList->Connection(conn_ind)->Reposition();
		ConnectionsList->Connection(conn_ind)->Repaint();
          xtt::fullVerification();
     }

     // Powrot na stare smieci :)
     setPos(fPosition);
     
     QGraphicsItem::mouseReleaseEvent(_gsme);
}
//---------------------------------------------------------------------------

// #brief Function sets position of the element.
//
// #param _x Coordinate x.
// #param _y Coordinate y.
// #return No return value.
void GNode::setPosition(float x, float y)
{
     fPosition.setX(x);
     fPosition.setY(y);
}
//---------------------------------------------------------------------------

