/**
 * \file	GTable.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	15.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is table drawing on the canvas.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GTableH
#define GTableH
//---------------------------------------------------------------------------

class XTT_Table;
class MyGraphicsScene;
class GLine;
class GRect;
class GText;
class GNode;
//---------------------------------------------------------------------------
/**
* \class 	GTable
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	15.05.2007 
* \brief 	Class definition that is table drawing on the canvas.
*/
class GTable
{
private:

     XTT_Table* fParent;	               ///< Pointer to the parent of the table.
     MyGraphicsScene* _parentScene;     ///< Pointer to the parent scene

     unsigned int fHLineCount;	///< Number of horizontal lines.
     unsigned int fVLineCount;	///< Number of vertical lines.
     unsigned int fTextCount;	///< Number of texts.
     unsigned int fNodeCount;	///< Number of nodes.

     GLine** fHLines;	///< Pointer to horizontal lines.
     GLine** fVLines;	///< Pointer to vertical lines.
     GRect* fBase;		///< Pointer to rectangle - background of the table.
     GRect* fErrBase;	///< Pointer to rectangle - background of the table with error.
     GText** fText;		///< Pointer to texts.
     GNode** fNodes;	///< Pointer to nodes.
     
     bool _amIvisible;   ///< Denotes if the table is currently visible

public:

	/// \brief Constructor GTable class.
	///
	/// \param __scene - Pointer to the parent scene
	/// \param _p Pointer to the parent of the table.
	GTable(MyGraphicsScene* __scene, XTT_Table* _p);

	/// \brief Destructor GTable class.
	~GTable(void);

	/// \brief Function retrieves number of horizontal lines.
	///
	/// \return Number of horizontal lines.
	unsigned int HorLineCount(void){ return fHLineCount; }

	/// \brief Function retrieves number of vertical lines.
	///
	/// \return Number of vertical lines.
	unsigned int VerLineCount(void){ return fVLineCount; }

	/// \brief Function retrieves number of texts.
	///
	/// \return Number of texts.
	unsigned int TextCount(void){ return fTextCount; }

	/// \brief Function adds new horizontal line.
	///
	/// \return No return value.
	void AddHorLine(void);

	/// \brief Function adds new vertical line.
	///
	/// \return No return value.
	void AddVerLine(void);

	/// \brief Function adds new text.
	///
	/// \param _tt Type of the text that should be taken.
	/// \param _row Number of row from which text should be taken.
	/// \param _col Number of column from which text should be taken.
	/// \return No return value.
	void AddText(unsigned int, unsigned int, unsigned int);

	/// \brief Function adds new node.
	///
	/// \param _row Number of the row.
	/// \return No return value.
	void AddNode(unsigned int _row);

	/// \brief Function removes last vertical line.
	///
	/// \return No return value.
	void DeleteVerLine(void);

	/// \brief Function removes last horizontal line.
	///
	/// \return No return value.
	void DeleteHorLine(void);

	/// \brief Function removes last text.
	///
	/// \return No return value.
	void DeleteText(void);

	/// \brief Function removes last node.
	///
	/// \return No return value.
	void DeleteNode(void);

	/// \brief Function creates list of components and set them.
	///
	/// \return No return value.
	void ReCreate(void);

	/// \brief Function updates parameters.
	///
	/// \return No return value.
	void Update(void);

	/// \brief Function sets elements once again.
	///
	/// \return No return value.
	void RePosition(void);

	/// \brief Function select the table.
	///
	/// \return No return value.
	void setTableSelection(bool);

        GRect* baseRect() { return fBase; }

	/// \brief Function finds table on the stage and scroll the view to show it completely.
	///
	/// \return No return value.
	void ensureVisible(void);
     
     /// \brief Allow for select header cell
	///
        /// \param _colIndex - index of column in which the header should be selected
        /// \param _selStat - selection status
	/// \return No return value
	void selectHeader(int _colIndex, bool _selStat);
     
     /// \brief Allow for clear selection of all the elements that table consist of
	///
	/// \return No return value
	void unselectAllElements(void);
     
     /// \brief Function that removes all the items from the scene
	///
	/// \return No return value
     void removeAll(void);
     
     /// \brief Function that makes the table visible
	///
	/// \return true on success, false when table cannot be visible
     bool makeVisible(void);
};
//---------------------------------------------------------------------------
#endif
