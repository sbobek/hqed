/*
 *	   $Id: GRect.cpp,v 1.34.4.2.2.1 2011-03-16 08:58:37 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <QAction>
#include <QActionGroup>
#include <QLabel>
#include <QMenu> 
#include <QtGui>

#include <QGraphicsItem>

#include "../../namespaces/ns_hqed.h"
#include "../../C/XTT/SetCreator_ui.h"
#include "../../C/XTT/ExpressionEditor_ui.h"

#include "../../M/hFunction.h"
#include "../../M/hMultipleValue.h"

#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Connections.h"
#include "../../M/XTT/XTT_ConnectionsList.h"
#include "../../C/XTT/TableEditor_ui.h"
#include "../../C/XTT/CellEditor_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"
#include "../../C/XTT/CellEditorAdapter.h"
#include "C/XTT/CellExpressionTextEditor.h"

#include "GConnection.h"
#include "GRect.h"
//---------------------------------------------------------------------------

// #brief Constructor GRect class.
// #param _ptr Pointer to the parent of the table.

GRect::GRect(MyGraphicsScene* __scene, XTT_Table* _parent, QObject *parent) : QObject(parent), QGraphicsItem(0), _parentScene(__scene)
{
    fParent = _parent;
    cellEditor = 0;
    editorWidget = 0;
    setFlag(ItemIsSelectable);
    setFlag(ItemIsFocusable);
    setFlag(ItemIsMovable);
#if QT_VERSION >= 0x040600
    setFlag(ItemSendsGeometryChanges);
#endif
    setZValue(1);
    _expr_editor = NULL;
    frLevel = 0;
	
    _parentScene->setNewItem(this);
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GRect::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Rysowanie kwadratu
     _p->setPen(Qt::NoPen);
     _p->setBrush(Settings->xttTableColor());
     if(isSelected())
          _p->setBrush(Settings->xttSelectedTableColor());
     if(MainWin->ModelBW())
          _p->setBrush(QColor(255, 255, 255, 255));
     _p->drawRect(0, 0, fParent->Width(), fParent->Height());
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse click event is triggered.
//
// #param _e Pointer to the event object.
// #return No return value.
void GRect::mousePressEvent(QGraphicsSceneMouseEvent* _e)
{
	QList<QGraphicsItem*> items = scene()->items();
	for(int i=0;i<items.size();++i)
		items.at(i)->setSelected(true);
	
 	scene()->clearSelection();
	setSelected(true);

    updateFocus(_e);

    QGraphicsItem::mousePressEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse button is released over the object.
//
// #param _e Pointer to the event object.
// #return No return value.
void GRect::mouseReleaseEvent(QGraphicsSceneMouseEvent* _e)
{
	// Zaznaczenie zaleznosci
	if((Settings->xttTableSelectPolicy() == SELECT_CONNECTIONS) 
	|| 
	((_e->modifiers() == Qt::ControlModifier) && (Settings->xttTableSelectPolicy() == SELECT_CONNECTIONS_WITH_CTRL)))
	{
		// Find all connections relates with selected table
		
		// If not defined table parent
		if(fParent == NULL)
			return;
			
		QList<XTT_Connections*>* IN_connlist = ConnectionsList->listOfInConnections(fParent->TableId());
		QList<XTT_Connections*>* OUT_connlist = ConnectionsList->listOfOutConnections(fParent->TableId());
		
		for(int i=0;i<IN_connlist->size();++i)
			IN_connlist->at(i)->gConnection()->SelectAll();
		for(int i=0;i<OUT_connlist->size();++i)
			OUT_connlist->at(i)->gConnection()->SelectAll();
		
		delete IN_connlist;
		delete OUT_connlist;
	}
     
     QGraphicsItem::mouseReleaseEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse double click event is triggered.
//
// #param _e Pointer to the event object.
// #return No return value.
void GRect::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* _e)
{
    if(_e->button() != Qt::LeftButton)
         return;

        int row, col;
        clickedRowColumn(_e, row, col);
	
	//QMessageBox::critical(NULL, "HQEd", "Event coordinates row = " + QString::number(row) + ", col = " + QString::number(col), QMessageBox::Ok);
	
	// Uruchmienie okienka edycji zawartosci komorki
	if(fParent == NULL)
		return;
		
	// Jezeli edytujemy komorke
	if(row > -1)
	{
	     // Parametry uruchomienia okienka
	     XTT_Attribute* attr = fParent->ContextAtribute(col);
	     XTT_Cell* cell = NULL;
	     if(row < (int)fParent->RowCount())
	          cell = fParent->Row(row)->Cell(col);

             if((attr == NULL) || (cell == NULL))
                 return;

             openConditionalEditor(cell, row, col);
	}
	
	// Jezeli edytujemy naglowek
	if(row == -2)
	{
		int tindex = TableList->IndexOfID(fParent->TableId());
		if(tindex == -1)
			return;
			
		delete TableEdit;
		TableEdit = new TableEditor_ui(false, tindex);
		TableEdit->show();
	}
     
     QGraphicsItem::mouseDoubleClickEvent(_e);
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GRect::boundingRect() const
{
     return QRectF(0, 0, fParent->Width(), fParent->Height());
}
//---------------------------------------------------------------------------

// #brief Function is called when location of the object is changed.
//
// #param _change Changing value.
// #param _value Reference to changed values.
// #return Value that was changed.
QVariant GRect::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemSelectedChange) && (scene()) && (frLevel == 0))
     {
          frLevel = 1;

          bool isSel = value.toBool();
          if(isSel)
               _parentScene->setSelectedTableID(fParent->TableId());
          else
               _parentScene->setSelectedTableID("");
			
		frLevel = 0;
     }

     if((change == ItemPositionChange) && (scene()) && (frLevel == 0))
     {
            frLevel = 1;
            // Value jest nowa wartoscia polozenia
            QPointF newPos = value.toPointF();
            fParent->setLocalization(newPos.toPoint());
		
            if(Settings->xttAutoSceneResize())
            {
                QRectF srect = scene()->sceneRect();
                QRectF brect = boundingRect();
                QRectF newItemRect = QRectF(newPos.x(), newPos.y(), brect.width(), brect.height());
                QRectF requiredRect = srect | newItemRect;

                if(requiredRect != srect)
                    scene()->setSceneRect(requiredRect);
            }
            frLevel = 0;
     }
     return QGraphicsItem::itemChange(change, value);
}
//---------------------------------------------------------------------------

//
void GRect::clickedRowColumn(QGraphicsSceneMouseEvent *_e, int &row, int &column)
{
    if(_e->button() != Qt::LeftButton)
         return;

       // Sprawdzanie w ktorej komorce nastpilo klikniecie
       column = -1;
       row = -1;

       unsigned int width = 0;
       unsigned int height = fParent->HeaderHeight();

       // Okreslanie kolumny
       for(int w=0;w<fParent->ColCount();w++)
       {
               width += fParent->ColWidth(w);
               if(width >= _e->pos().x())
               {
                       column = w;
                       break;
               }
       }

       // Sprawdzanie czy klikniecie nie nastapilo w naglowku
       if(height >= _e->pos().y())
               row = -2;

       // Okreslanie wiersza
       if(row != -2)
       {
               for(unsigned int h=0;h<fParent->RowCount();h++)
               {
                       height += fParent->RowHeight(h);
                       if(height >= _e->pos().y())
                       {
                               row = h;
                               break;
                       }
               }
       }
}

void GRect::updateFocus(QGraphicsSceneMouseEvent *_e)
{
    if(_e->button() != Qt::LeftButton)
    {
        return;
    }

    int row, col;
    clickedRowColumn(_e, row, col);

    MyGraphicsScene *parentScene = static_cast<MyGraphicsScene*>(scene());
    if(!parentScene)
        return;

    // XTT cell clicked
    if(row > -1)
    {
        if(!fParent)
            return;

        XTT_Attribute* attr = fParent->ContextAtribute(col);
        XTT_Cell* cell = NULL;
        if(row < (int)fParent->RowCount())
            cell = fParent->Row(row)->Cell(col);

        if((attr == NULL) || (cell == NULL))
            return;

        parentScene->setSelectedTableID(fParent->TableId());
        parentScene->setFocusTableID(fParent->TableId());
        parentScene->setFocusCellID(cell->CellId());
    }
}

void GRect::openConditionalEditor(XTT_Cell *cell, int row, int col)
{
    if(editorWidget) {
        delete editorWidget;
        editorWidget = 0;
    }

    // Calculate editor coordinates
    int e_x(0), e_y(fParent->HeaderHeight());
    for(int i = 0; i < row; i++) {
        e_y += fParent->RowHeight(i);
    }
    for(int i = 0; i < col; i++) {
        e_x += fParent->ColWidth(i);
    }

    //cellEditor = new CellEditorAdapter(cell, 0);
    //cellEditor->setGraphicsItem(this, e_x, e_y);
    //cellEditor->setMinimumSize(QSize(fParent->ColWidth(col), fParent->RowHeight(row) + 8));
    cellExpressionEditor = new CellExpressionTextEditor(cell, 0);
    cellExpressionEditor->setMinimumSize(QSize(fParent->ColWidth(col), fParent->RowHeight(row) + 8));
    cellExpressionEditor->setGraphicsItem(this, e_x, e_y);
    editorWidget = _parentScene->addWidget(cellExpressionEditor);
    editorWidget->setGeometry(QRectF(mapToScene(QPointF(e_x, e_y)), QSizeF(fParent->ColWidth(col), fParent->RowHeight(row) + 8)));
    editorWidget->setZValue(10);
    cellExpressionEditor->setFocus();
    cellExpressionEditor->adjustEditorSize();

    _parentScene->ignoreKeyboard = true;
    _parentScene->setFocusTableID(fParent->TableId());

    connect(cellExpressionEditor, SIGNAL(editingCancelled()), this, SLOT(slotEditorCancelled()));
    connect(cellExpressionEditor, SIGNAL(editingFinished()), this, SLOT(slotEditorFinished()));
}

void GRect::slotEditorCancelled()
{
    slotEditorFinished();

    setFocus();
    _parentScene->ignoreKeyboard = false;
}

void GRect::slotEditorFinished()
{
    if(editorWidget) {
        delete editorWidget;
        editorWidget = 0;
    }

    setFocus();
    _parentScene->ignoreKeyboard = false;
}

void GRect::keyPressEvent(QKeyEvent *event)
{
    if(!event->text().isEmpty())
    {
        XTT_Cell *focusCell = fParent->cell(_parentScene->FocusCellID());
        if(!focusCell)
            return;

        unsigned int row, col;
        if(!fParent->cellRowColumn(focusCell, row, col))
            QMessageBox::warning(0, "error", QString::number(row) + " " + QString::number(col));

        openConditionalEditor(focusCell, row, col);

        return;
    }

    QGraphicsItem::keyPressEvent(event);
}
