/**
 * \file	GSortButton.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	06.11.2009
 * \version	1.15
 * \brief	This file contains class definition that represents sorting buttom
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef GSORTBUTTONH
#define GSORTBUTTONH
//---------------------------------------------------------------------------

#include <QPainter>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

/**
* \class 	GSortButton
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	06.11.2009
* \brief 	Class definition that represents sorting buttom
*/
#if QT_VERSION >= 0x040600
class GSortButton : public 	QGraphicsObject
#else
class GSortButton : public QObject, public QGraphicsItem
#endif
{
     Q_OBJECT
     
private:

     bool _up;           ///< Indicates if the button should be draw as up or down button
     bool _hovered;      ///< Indicates if the mouse cursor is over the element
     double _edgelength; ///< The width of the triangle edge

public:

	/// \brief Contructor GSortButton class.
	///
	/// \param __parent - parent of the button
	GSortButton(bool __up, QGraphicsItem* __parent);

	/// \brief Function draws object on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter*, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function is called when mouse button is pressed on the item.
	///
	/// \param _gsme Pointer to the event object.
	/// \return No return value.
	void mousePressEvent(QGraphicsSceneMouseEvent* _gsme);
     
     /// \brief Function is called when mouse cursor enters the element area.
	///
	/// \param _gsme Pointer to the event object.
	/// \return No return value.
	void hoverEnterEvent(QGraphicsSceneHoverEvent* _gsme);
     
     /// \brief Function is called when mouse cursor leaves the element area.
	///
	/// \param _gsme Pointer to the event object.
	/// \return No return value.
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* _gsme);

	/// \brief Function retrieves rectangular in which object is located.
	///
	/// \return Rectangular in which object is located.
	QRectF boundingRect(void) const;
     
     /// \brief Function that returns the length of the button edge
	///
	/// \param __newlength - a new length of the button edge
	/// \return no values return
     void setEdgeLength(double __newlength);
     
     /// \brief Function that returns the length of the button edge
	///
	/// \return the length of the button edge
     double edgeLength(void);
     
signals:

     /// \brief Signal emited when user presses the item
     ///
     /// \return no values return
     void onMousePress(void);
};
//---------------------------------------------------------------------------
#endif
