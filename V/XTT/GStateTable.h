/**
 * \file	GStateTable.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.11.2009
 * \version	1.0
 * \brief	This file contains class definition that is state table drawing on the canvas.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GStateTableH
#define GStateTableH
//---------------------------------------------------------------------------

class hSet;
class XTT_State;
class XTT_Column;
class XTT_Attribute;
class XTT_Statesgroup;
class MyGraphicsScene;
class GStateRect;
class GStateText;
//---------------------------------------------------------------------------
/**
* \class 	GStateTable
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.11.2009
* \brief 	Class definition that is state table drawing on the canvas.
*/
class GStateTable
{
private:

    XTT_Statesgroup* fParent;           ///< Pointer to the parent of the table.
    MyGraphicsScene* _parentScene;     ///< Pointer to the parent scene

    int _nextidindex;                  ///< New id index
    int _tableType;                    ///< The type of the table 0-initial states, 1-final states
    unsigned int fTextCount;	          ///< Number of texts.

    GStateRect* fBase;		          ///< Pointer to rectangle - background of the table.
    GStateText** fText;		          ///< Pointer to texts.

    bool _amIvisible;                  ///< Denotes if the table is currently visible
    bool _amIoutput;                   ///< Indicates if the table is a output table

    // localization, positions itd...
    QPointF _localization;             ///< X,Y localization coordiantes
    int _headerHeight;                 ///< Height of the header
    int _labelWidth;                   ///< Holds the width of the label column
    QList<int> _rowHeights;            ///< List of row heights
    QList<XTT_Column*> _columns;       ///< List of columns parameters

    /// \brief Calculates the quantity of needed texts objects
    ///
    /// \return quantity of needed texts objects
    int textQuantity(void);

    /// \brief returns the number of texts in each type. at(0) - the number of texsts that has type 0 itd
    ///
    /// \return the number of texts in each type
    QList<int>* texts(void);

    /// \brief Function that returns an index of the first text in the texts array from the given text section
    ///
    /// \param __sectionindex - index of the section
    /// \li 0 - cell contents
    /// \li 1 - table title
    /// \li 2 - normal column header
    /// \li 3 - row labels
    /// \li 4 - header of row labels column
    /// \return the index of the first text object from the given section
    int textStartIndex(int __sectionindex);

    /// \brief returns the list of the texts positions.
    ///
    /// \return list of the texts positions.
    QList<qreal>* textsPositions(void);
     
public:

    /// \brief Constructor GTable class.
    ///
    /// \param __scene - Pointer to the parent scene
    /// \param _p Pointer to the parent of the table.
    GStateTable(MyGraphicsScene* __scene, XTT_Statesgroup* _p);

    /// \brief Destructor GTable class.
    ~GStateTable(void);

    /// \brief Function that clears all the data
    ///
    /// \return no values return
    void clear(void);

    /// \brief Function that sets the parent of this object
    ///
    /// \param __parent - pointer to the new parent
    /// \return no values return
    void setParent(XTT_Statesgroup* __parent);

    /// \brief Function that returns the point of table localization
    ///
    /// \return the reference to the point of table localization
    QPointF localization(void);

    /// \brief Function that returns a pointer to the column object
    ///
    /// \param __colindex - index of the column
    /// \return pointer to the column, or NULL if the index os incorrect
    XTT_Column* column(int __colindex);

    /// \brief Function that returns a width of the column
    ///
    /// \param __rowindex - index of the column
    /// \return a width of the column
    int columnWidth(int __colindex);

    /// \brief Function that returns a height of the row
    ///
    /// \param __rowindex - index of the row
    /// \return a height of the row
    int rowHeight(int __rowindex);

    /// \brief Function that returns the poisition of the given state
    ///
    /// \param __state - the pointer to the state
    /// \return the offset from the first element of the container
    int stateIndex(XTT_State* __state);

    /// \brief Function that returns a pointer to the state related with the given row
    ///
    /// \param __row - index of the row
    /// \return pointer to the state in the given row
    XTT_State* stateAt(int __row);

    /// \brief Function that returns a pointer to the set in the given cell
    ///
    /// \param __row - index of the row
    /// \param __col - index of the col
    /// \return pointer to the set in the given cell
    hSet* setAt(int __row, int __col);

    /// \brief Function that returns a label of the state at the given position
    ///
    /// \param __row - index of the row
    /// \return label of the state at the given position
    QString labelAt(int __row);

    /// \brief Function that sets the label of the state at the given position
    ///
    /// \param __row - the row index
    /// \param __newlabel - new label of the state at the given position
    /// \return no values return
    void setLabelAt(int __row, QString __newlabel);

    /// \brief Function that sets the width of the column
    ///
    /// \param __colindex - index of the column
    /// \param __newwidth - new width of the col
    /// \return no values return
    void setColumnWidth(int __colindex, int __newwidth);

    /// \brief Function that sets the height of the row
    ///
    /// \param __rowindex - index of the row
    /// \param __newheight - new height of the row
    /// \return no values return
    void setRowHeight(int __rowindex, int __newheight);

    /// \brief Function that sets the new localization of the table
    ///
    /// \param __newpos - point that contains two coordinates of the localization
    /// \return no values return
    void setLocalization(QPointF __newpos);

    /// \brief Function that returns the number of column
    ///
    /// \return the number of column
    int columnCount(void);

    /// \brief Function that returns the number of rows
    ///
    /// \return the number of rows
    int rowCount(void);

    /// \brief Function that returns the width of the table
    ///
    /// \return the width of the table
    int width(void);

    /// \brief Function that returns the height of the table
    ///
    /// \return the height of the table
    int height(void);

    /// \brief Function that returns a shape of the graphical representation of the item in scene coordinates
    ///
    /// \return a shape of the graphical representation of the item in scene coordinates
    QRectF shape(void);

    /// \brief Function that returns the Y coordinate of the row __row
    ///
    /// \param __row - index of row, if -1 return Y coordinate of the table
    /// \return the Y coordinate of the row __row
    qreal rowY(int __row = -1);

    /// \brief Function that returns the Y coordinate of the middle of the row __row
    ///
    /// \param __row - index of row, if -1 return Y coordinate of the header
    /// \return the Y coordinate of the middle the row __row
    qreal rowMiddleY(int __row = -1);

    /// \brief Function that returns the height of the table header
    ///
    /// \return the height of the table header
    int headerHeight(void);

    /// \brief Function that returns the type of the table
    ///
    /// \return the type of the table
    int tableType(void);

    /// \brief Function that returns the title of the table
    ///
    /// \return the title of the table as string
    QString tableTitle(void);

    /// \brief Function that returns the index of the colum that is related with the given attribute
    ///
    /// \param __att - pointer to the given attribute
    /// \return index of the column, if attribute is not exists returns -1
    int indexOfAttribute(XTT_Attribute* __att);

    /// \brief Function that returns the index of the colum that is related with the given attribute
    ///
    /// \param __id - id of the attribute
    /// \return index of the column, if attribute is not exists returns -1
    int indexOfAttribute(QString __id);

    /// \brief Function retrieves number of texts.
    ///
    /// \return Number of texts.
    unsigned int TextCount(void){ return fTextCount; }

    /// \brief Function adds new text.
    ///
    /// \param __id - new id of the text
    /// \return No return value.
    void AddText(QString __id);

    /// \brief Function removes last text.
    ///
    /// \return No return value.
    void DeleteText(void);

    /// \brief Function that refreshes all the data
    ///
    /// \return No return value.
    void refreshAll(void);

    /// \brief Function creates list of components and set them.
    ///
    /// \return No return value.
    void ReCreate(void);

    /// \brief Function updates parameters.
    ///
    /// \return No return value.
    void Update(void);

    /// \brief Function sets elements once again.
    ///
    /// \return No return value.
    void RePosition(void);

    /// \brief Function that selects the cell in the state table
    ///
    /// \param  __state - pointer to the state
    /// \param  __attr - pointer to the attribute in the given state
    /// \param  __selected - indicates the state of selection
    /// \return No return value.
    void selectCell(XTT_State* __state, XTT_Attribute* __attr, bool __selected);

    /// \brief Function that selects the cell in the state table
    ///
    /// \param  __state - pointer to the state
    /// \param  __attid - id of the attribute
    /// \param  __selected - indicates the state of selection
    /// \return No return value.
    void selectCell(XTT_State* __state, QString __attid, bool __selected);

    /// \brief Function that selects the state in the table if it exists
    ///
    /// \param  __state - pointer to the state to be selected
    /// \param  __selectionstatus - indicates the status of state selection
    /// \return No return value.
    void selectState(XTT_State* __state, bool __selectionstatus);

    /// \brief Function select the table.
    ///
    /// \return No return value.
    void setTableSelection(bool);

    /// \brief Function finds table on the stage and scroll the view to show it completely.
    ///
    /// \return No return value.
    void ensureVisible(void);

    /// \brief Allow for select header cell
    ///
    /// \param int _colIndex - index of column in which the header should be selected
    /// \param bool _selStat - selection status
    /// \return No return value
    void selectHeader(int _colIndex, bool _selStat);

    /// \brief Allow for select single row
    ///
    /// \param __rowIndex - index of the row that must be selected
    /// \param __selected - the status of selection
    /// \return No return value
    void selectRow(int __rowIndex, bool __selected = true);

    /// \brief Allow for clear selection of all the elements that table consist of
    ///
    /// \return No return value
    void unselectAllElements(void);

    /// \brief Function that removes all the items from the scene
    ///
    /// \return No return value
    void removeAll(void);

    /// \brief Function that makes the table visible
    ///
    /// \return true on success, false when table cannot be visible
    bool makeVisible(void);

    /// \brief Function that returns if the table is a output table
    ///
    /// \return if the table is a output table
    bool isOutput(void);
};
//---------------------------------------------------------------------------
#endif
