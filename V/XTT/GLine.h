 /**
 * \file	GLine.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	15.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is a line (edge of the table).
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef GLineH
#define GLineH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class XTT_Table;
//---------------------------------------------------------------------------
/**
* \class 	GLine
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	15.05.2007 
* \brief 	Class definition that is a line (edge of the table).
*/
class GLine : public QGraphicsItem
{
private:

	XTT_Table* fParent;	///< Pointer to the parent table.
	bool fIsVertical;	///< Determines whether the line is vertical.

public:

	/// \brief Contructor GLine class.
	///
	/// \param _parent Pointer to the parent table.
	/// \param _isVert Determines whether the line is vertical.
	/// \param _sizeIndex Index of the row/column to which line is assigned.
	GLine(XTT_Table* _parent, bool _isVert, int _sizeIndex);

	/// \brief Function draws square on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function retrieves rectangular in which object is located.
	///
	/// \return Rectangular in which object is located.
	QRectF boundingRect() const;

	/// \brief Function sets decision whether the object is vertical.
	///
	/// \param _v Decision whether the objec is vertical.
	/// \return No return value.
	void setVerticality(bool _v){ fIsVertical = _v; }
};
//---------------------------------------------------------------------------
#endif
