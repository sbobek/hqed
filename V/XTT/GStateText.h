/**
 * \file	GStateText.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.11.2009
 * \version	1.0
 * \brief	This file contains class definition that is text drawing on the canvas.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GSTATETEXTH
#define GSTATETEXTH
//---------------------------------------------------------------------------

#include <QObject>
#include <QGraphicsItem>
#include <QActionGroup>
#include <QPainter>
#include <QString>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//--------------------------------------------------------------------------

class hSet;
class XTT_Attribute;
class GStateTable;
class hMenuAction;
class SetEditor_ui;
//---------------------------------------------------------------------------
/**
* \class 	GStateText
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.11.2009
* \brief 	Class definition that is text drawing on the canvas.
*/
#if QT_VERSION >= 0x040600
class GStateText : public 	QGraphicsObject
#else
class GStateText : public QObject, public QGraphicsItem
#endif
{
	Q_OBJECT

private:

     int _row;                     ///< Index of row in the table
     int _col;                     ///< Index of col in the table
     QString _id;                  ///< Id of the item
     GStateTable* fParent;	     ///< Pointer to the parent of the table
     hSet* _set;                   ///< Pointer to the initial value of attribute fTextType = 0, fTextType = 3
     QString _text;                ///< Displayed text when fTextType = 1
     XTT_Attribute* _attribute;    ///< Pointer to the attribute fTextType = 2
     
     SetEditor_ui* _setEditor;          ///< Pointer to the set editor widget

	/// Okresla czy komorka ma zostac zaznaczona
	bool isSelected;	          ///< Determines whether cell should be selected.
     bool fIsHeaderSelected;	     ///< Determines selection of the header cell
     
     /// Okresla co nalezy pobrac z tabeli, jaki text
     /// 0 - zawartosc komorki okreslonej przez fRow, fCol
     /// 1 - Title
     /// 2 - Header okreslony przez fCol
	/// Determines what type of text should be taken.
	/// \li 0 - content of the cell.
	/// \li 1 - Title.
	/// \li 2 - Header.	
     /// \li 3 - Row label
     /// \li 4 - Header of the label column
     int fTextType;
     
	/// Polecenia menu kontekstowego
	QAction* aFullEditCell;		     ///< Pointer to the class provides an abstract user interface action.
	QAction* aChangeRowLabel;          ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aLoadState;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aEidtTable;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aCellEditor;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aFastEditCell;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aDeleteTable;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAddNewRow;	          ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAddCopy;	          ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aDeleteRow;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aDeleteCol;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aStateSchemaEditor;	///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAttributeEditor;	///< Pointer to the class provides an abstract user interface action.
	
     /// \brief Function that deallocate the memory
	///
	/// \return No return value.
     void clearActions(void);

     /// \brief Function creates menu actions.
     ///
     /// \return No return value.
     void createActions(void);
	
protected:

	/// \brief Function is called when mouse right button is pressed.
	///
        /// \param contextMenuEvent Pointer to event object.
	/// \return No return values.
	void contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent);
	
	/// \brief Function is called mouse is hanging over the cell.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
	
	/// \brief Function is called when mouse is leavin the cell.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
     
     /// \brief Function is called when user cliks twice on the item
	///
	/// \param __event - Pointer to event object.
	/// \return No return values.
     void mouseDoubleClickEvent(QGraphicsSceneMouseEvent* __event);
     
     /// \brief Function is called when user presess mouse button on the item
	///
	/// \param __event - Pointer to event object.
	/// \return No return values.
     void mousePressEvent(QGraphicsSceneMouseEvent* __event);
     
     /// \brief Function that returns a command separator
	///
	/// \return command separator
     QString cmdsep(void);
     
     /// \brief Function is called when location of the object is changed.
     ///
     /// \param _change Changing value.
     /// \param _value Reference to changed values.
     /// \return Value that was changed.
     QVariant itemChange(GraphicsItemChange change, const QVariant &value);

public:

     /// \brief Constructor GStateText class.
	///
	/// \param __id - id of the item
	/// \param __parent - pointer to the parent
	GStateText(QString __id, GStateTable* __parent);

     /// \brief Destructor of GStateText class.
     ~GStateText(void);

	/// \brief Function draws square on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function retrieves rectangular in which object is located.
	///
     /// \return Rectangular in which object is located.
	QRectF boundingRect() const;

	/// \brief Function sets type of the text.
	///
     /// \param _tt - new identifier of the type of the text
	/// \return No return value.
	void setTextType(int _tt);
     
     /// \brief Function sets id of the text.
	///
     /// \param __id - new identifier of the text
	/// \return No return value.
	void setId(QString __id);
     
     /// \brief Function sets the pointer to the parent
	///
     /// \param __parent - pointer to the parent
	/// \return No return value.
	void setParent(GStateTable* __parent);
     
     /// \brief Function sets the pointer to the attribute
	///
     /// \param __xttattribute - pointer to the attribute
	/// \return No return value.
	void setAttribute(XTT_Attribute* __xttattribute);
     
     /// \brief Function sets the text
	///
     /// \param __text - new text
	/// \return No return value.
	void setText(QString __text);
     
     /// \brief Function sets the pointer to the set of values
	///
     /// \param __set - pointer to the set of values
	/// \return No return value.
	void setSet(hSet* __set);
     
     /// \brief Function sets the row index
	///
     /// \param __row - new row index
	/// \return No return value.
	void setRow(int __row);
     
     /// \brief Function sets the column index
	///
     /// \param __col - new column index
	/// \return No return value.
	void setCol(int __col);
     
     /// \brief Allow for select header cell
	///
        /// \param _selStat - selection status
	/// \return No return value
	void selectHeaderCell(bool _selStat);
     
     /// \brief Allow for select cell
	///
        /// \param _selStat - selection status
	/// \return No return value
	void selectCell(bool _selStat);
     
     /// \brief Return status of header selection
	///
	/// \return True is header is selected, otherwise false
	bool isHeaderSelected(void);
     
     /// \brief Function that creates the string that conatins the name of command with parameters
     ///
     /// \param __cmd - the name of the command
     /// \param __params - type of params
     /// \li t - tableid
     /// \li r - row
     /// \li c - column
     /// \return string that conatins the name of command with parameters
     QString params(QString __cmd, QString __params);
     
public slots:

     /// \brief Function that is triggered when user chooses full edit action from the context menu
	///
	/// \return no values return
     void onFullEdit(void);
     
     /// \brief Function that is triggered when user closes the set editor with the OK button
	///
	/// \return no values return
     void onFinishFullEdit(void);
     
     /// \brief Function that is triggered when user closes the set creator with the OK button
	///
	/// \return no values return
     void onFinishSimpleEdit(void);
     
     /// \brief Function that is triggered when user closes the set editor/creator with the OK button
	///
	/// \return no values return
     void onFinishEdit(void);
     
     /// \brief Function that is triggered when user chooses label edit action from the context menu
	///
	/// \return no values return
     void onLabelEdit(void);
};
//---------------------------------------------------------------------------
#endif
