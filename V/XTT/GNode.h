/**
 * \file	GNode.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	20.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is junction of connection.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef GNodeH
#define GNodeH
//---------------------------------------------------------------------------

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//---------------------------------------------------------------------------

class XTT_Row;
class XTT_Table;
//---------------------------------------------------------------------------
/**
* \class 	GNode
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	20.05.2007 
* \brief 	Class definition that is junction of connection.
*/
class GNode : public QGraphicsItem
{
private:

     XTT_Row* fRowParent;	///< Pointer to the parent of the row.

     XTT_Table* fTableParent;	///< Pointer to the parent of the table.

     QPointF fPosition;	///< Location of the junction.

     bool isIncome;	///< Determines whether connection is income.

public:

    /// \brief Contructor GNode class.
    ///
    /// \param _t_parent Pointer to the parent of the table.
    /// \param _r_parent Pointer to the parent of the row.
    /// \param _income Determines whether connection is income.
    GNode(XTT_Table* _t_parent, XTT_Row* _r_parent, bool _income);

    /// \brief Function draws object on the screen.
    ///
    /// \param _p Pointer to the canvas.
    /// \param _sogi Pointer to the drawing style.
    /// \param widget Unused pointer.
    /// \return No return value.
    void paint(QPainter*, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

    /// \brief Function is called when mouse button is released on the scene.
    ///
    /// \param _gsme Pointer to the event object.
    /// \return No return value.
    void mouseReleaseEvent(QGraphicsSceneMouseEvent* _gsme);

    /// \brief Function retrieves rectangular in which object is located.
    ///
    /// \return Rectangular in which object is located.
    QRectF boundingRect() const;

    /// \brief Function is called when location of the object is changed.
    ///
    /// \param _change Changing value.
    /// \param _value Reference to changed values.
    /// \return Value that was changed.
    QVariant itemChange(GraphicsItemChange change, const QVariant& _value);

    /// \brief Function sets pointer to the parent of the row.
    ///
    /// \return No return value.
    void setRowParent(XTT_Row* _r){ fRowParent = _r; }

    /// \brief Function sets position of the element.
    ///
    /// \param _x Coordinate x.
    /// \param _y Coordinate y.
    /// \return No return value.
    void setPosition(float _x, float _y);

    /// \brief Function sets decision whether connection is income.
    ///
    /// \param _i Determines whether connection is income.
    /// \return No return value.
    void setIncome(bool _i){ isIncome = _i; }

};
//---------------------------------------------------------------------------
#endif
