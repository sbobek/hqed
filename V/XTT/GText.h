/**
 * \file	GText.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	15.05.2007 
 * \version	1.15
 * \brief	This file contains class definition that is text drawing on the canvas.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GTextH
#define GTextH
//---------------------------------------------------------------------------

#include <QObject>
#include <QGraphicsItem>
#include <QActionGroup>
#include <QPainter>
#include <QGraphicsSceneMouseEvent>
#include <QStyleOptionGraphicsItem>
//--------------------------------------------------------------------------

class XTT_Table;
class hMenuAction;
class GSortButton;
//---------------------------------------------------------------------------
/**
* \class 	GText
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	15.05.2007 
* \brief 	Class definition that is text drawing on the canvas.
*/
#if QT_VERSION >= 0x040600
class GText : public 	QGraphicsObject
#else
class GText : public QObject, public QGraphicsItem
#endif
{
	Q_OBJECT

private:

     /// Tabela rodzic
     XTT_Table* fParent;	///< Pointer to the parent of the table.

     /// Wiersz
     unsigned int fRow;	///< Number of the row.

     /// Kolumna
     unsigned int fCol;	///< Number of the column.

     /// Okresla co nalezy pobrac z tabeli, jaki text
     /// 0 - zawartosc komorki okreslonej przez fRow, fCol
     /// 1 - Title
     /// 2 - Header okreslony przez fCol
	/// Determines what type of text should be taken.
	/// \li 0 - content of the cell.
	/// \li 1 - Title.
	/// \li 2 - Header.	
     /// \li 3 - Row label
     unsigned int fTextType;
	
	/// Okresla czy komorka ma zostac zaznaczona
	bool isSelected;	///< Determines whether cell should be selected.
     
     bool fIsHeaderSelected;		     ///< Determines selection of the header cell
     
     GSortButton* _sortButtons[2];      ///< Two pointer to the sort buttons
     
	/// Polecenia menu kontekstowego
     QActionGroup* agSortActionGroup;   ///< Pointer to the group of sort action
	hMenuAction* aEidtTable;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aCellEditor;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aFullEditCell;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aFastEditCell;		///< Pointer to the class provides an abstract user interface action.
        hMenuAction* aNewEditCell;		///< Pointer to the class provides an new abstract user interface action.
	hMenuAction* aDeleteTable;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aInsertRowBefore;	///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aInsertRowAfter;	     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAddNewRow;	          ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aCopyRowBefore;	     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aCopyRowAfter;	     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAddCopy;	          ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aDeleteRow;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aDeleteCol;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aMoveRowDown;		///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aMoveRowUp;		     ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aSortCriterionMin;    ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aSortCriterionMax;    ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aSortCriterionPow;    ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aSortCriterionAlp;    ///< Pointer to the class provides an abstract user interface action.
	hMenuAction* aAttributeEditor;	///< Pointer to the class provides an abstract user interface action.
	
	/// \brief Function creates menu actions.
	///
	/// \return No return value.
	void createActions(void);
     
     /// \brief Function that returns a command separator
	///
	/// \return command separator
     QString cmdsep(void);
     
     /// \brief Function that creates the string that conatins the name of command with parameters
	///
     /// \param __cmd - the name of the command
     /// \param __params - type of params
     /// \li t - tableid
     /// \li r - row
     /// \li c - column
	/// \return string that conatins the name of command with parameters
     QString params(QString __cmd, QString __params);
     
     /// \brief Function that initializes the SortButtons elements
     ///
     /// \return no values return
     void initSortButtons(void);
     
     /// \brief Function that sets the position of the SortButtons elements
     ///
     /// \return no values return
     void posSortButtons(void);
	
protected:

	/// \brief Function is called when mouse right button is pressed.
	///
        /// \param contextMenuEvent Pointer to event object.
	/// \return No return values.
	void contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent);
	
	/// \brief Function is called mouse is hanging over the cell.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
	
	/// \brief Function is called when mouse is leavin the cell.
	///
	/// \param event Pointer to event object.
	/// \return No return values.
	void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);

public:

	/// \brief Constructor GText class.
	///
	/// \param _parent Pointer to the parent of the table.
	/// \param _tt Index of the table.
	/// \param _row Index of the row of the cell.
	/// \param _col Index of the column of the cell.
	GText(XTT_Table* _parent, unsigned int _tt, unsigned int _row, unsigned int _col);
     
     /// \brief Destructor of GText class.
     ~GText(void);

	/// \brief Function draws square on the screen.
	///
	/// \param _p Pointer to the canvas.
	/// \param _sogi Pointer to the drawing style.
	/// \param widget Unused pointer.
	/// \return No return value.
	void paint(QPainter* _p, const QStyleOptionGraphicsItem* _sogi, QWidget* widget=0);

	/// \brief Function retrieves rectangular in which object is located.
	///
	/// \return Rectangular in which object is located.
	QRectF boundingRect() const;

	/// \brief Function sets number of the row.
	///
	/// \return No return value.
	void setRow(unsigned int _r){ fRow = _r; }

	/// \brief Function sets number of the column.
	///
	/// \return No return value.
	void setCol(unsigned int _c){ fCol = _c; }

	/// \brief Function sets type of the text.
	///
	/// \return No return value.
	void setTextType(unsigned int _tt);
     
     /// \brief Allow for select header cell
	///
        /// \param _selStat - selection status
	/// \return No return value
	void selectHeaderCell(bool _selStat);
     
     /// \brief Return status of header selection
	///
	/// \return True is header is selected, otherwise false
	bool isHeaderSelected(void);
     
public slots:

     /// \brief Performs ascending sort in the cell
	///
	/// \return Performs ascending sort in the cell
     void ascendingSort(void);
     
     /// \brief Performs descending sort in the cell
	///
	/// \return Performs descending sort in the cell
     void descendingSort(void);
};
//---------------------------------------------------------------------------
#endif
