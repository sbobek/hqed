/*
 *	   $Id: GStateText.cpp,v 1.2.4.5 2011-06-06 15:33:34 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QFont>
#include <QFontMetrics>

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hDomain.h"
#include "../../M/hTypeList.h"
#include "../../M/hFunction.h"
#include "../../M/hExpression.h"
#include "../../M/hFunctionList.h"
#include "../../M/hMultipleValue.h"
#include "../../M/hFunctionArgument.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Column.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/XTT/SetEditor_ui.h"
#include "../../C/XTT/widgets/StateGraphicsScene.h"
#include "../../C/widgets/hMenuAction.h"
#include "../../C/devPluginInterface/devPluginController.h"
#include "../../C/devPluginInterface/devPlugin.h"
#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "GStateTable.h"
#include "GStateText.h"
//---------------------------------------------------------------------------

// #brief Constructor GStateText class.
//
// #param __id - id of the item
// #param __parent - pointer to the parent
GStateText::GStateText(QString /*__id*/, GStateTable* __parent) :
#if QT_VERSION >= 0x040600
     QGraphicsObject(0)
#else
     QObject(0),
     QGraphicsItem(0)
#endif
{
     setZValue(3);
     
     //setFlag(ItemIsSelectable);
     fParent = __parent;
     _attribute = NULL;
     _set = NULL;
     _text = "";
     fTextType = -1;
	isSelected = false;
	fIsHeaderSelected = false;

     aFullEditCell = NULL;
     aChangeRowLabel = NULL;
     aFastEditCell = NULL;
     aLoadState = NULL;
     aDeleteTable = NULL;
     aAddNewRow = NULL;
     aAddCopy = NULL;
     aDeleteRow = NULL;
     aDeleteCol = NULL;
     aStateSchemaEditor = NULL;
     aAttributeEditor = NULL;

     _setEditor = new SetEditor_ui;
     
	setAcceptsHoverEvents(true);
}
//---------------------------------------------------------------------------

// #brief Destructor of GStateText class.
GStateText::~GStateText(void)
{
     clearActions();
     delete _setEditor;
}
//---------------------------------------------------------------------------

// #brief Function sets type of the text.
//
// #return No return value.
void GStateText::setTextType(int _tt)
{
     fTextType = _tt;
     update();
}
//---------------------------------------------------------------------------

// #brief Function sets id of the text.
//
// #param __id - new identifier of the text
// #return No return value.
void GStateText::setId(QString __id)
{
     _id = __id;
}
//---------------------------------------------------------------------------

// #brief Function sets the pointer to the parent
//
// #param __parent - pointer to the parent
// #return No return value.
void GStateText::setParent(GStateTable* __parent)
{
     fParent = __parent;
     update();
}
//---------------------------------------------------------------------------

// #brief Function sets the pointer to the attribute
//
// #param __xttattribute - pointer to the attribute
// #return No return value.
void GStateText::setAttribute(XTT_Attribute* __xttattribute)
{
     _attribute = __xttattribute;
     update();
}
//---------------------------------------------------------------------------

// #brief Function sets the text
//
// #param __text - new text
// #return No return value.
void GStateText::setText(QString __text)
{
     _text = __text;
     update();
}
//---------------------------------------------------------------------------

// #brief Function sets the pointer to the set of values
//
// #param __set - pointer to the set of values
// #return No return value.
void GStateText::setSet(hSet* __set)
{
     _set = __set;
     update();
}
//---------------------------------------------------------------------------

// #brief Function sets the row index
//
// #param __row - new row index
// #return No return value.
void GStateText::setRow(int __row)
{
     _row = __row;
}
//---------------------------------------------------------------------------

// #brief Function sets the column index
//
// #param __col - new column index
// #return No return value.
void GStateText::setCol(int __col)
{
     _col = __col;
}
//---------------------------------------------------------------------------

// #brief Function is called mouse is hanging over the cell.
//
// #param event Pointer to event object.
// #return No return values.
void GStateText::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
	if(scene() == NULL)
		return;

	isSelected = true;
    if((Settings->xttEnabledAutocentering()) && (scene()->mouseGrabberItem() != this))
        ensureVisible(boundingRect(), 0, 0);
          
	QGraphicsItem::hoverEnterEvent(event);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse is leavin the cell.
//
// #param event Pointer to event object.
// #return No return values.
void GStateText::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
	isSelected = false;
	fIsHeaderSelected = false;
	QGraphicsItem::hoverLeaveEvent(event);
}
//---------------------------------------------------------------------------

// #brief Allow for select header cell
//
// #param bool _selStat - selection status
// #return No return value
void GStateText::selectHeaderCell(bool _selStat)
{
	fIsHeaderSelected = _selStat;
	if(_selStat)
		ensureVisible(boundingRect(), 0, 0);
	update();
}
//---------------------------------------------------------------------------

// #brief Allow for select cell
//
// #param bool _selStat - selection status
// #return No return value
void GStateText::selectCell(bool _selStat)
{
     isSelected = _selStat;
     setSelected(isSelected);
	update();
}
//---------------------------------------------------------------------------

// #brief Return status of header selection
//
// #return True is header is selected, otherwise false
bool GStateText::isHeaderSelected(void)
{
	return fIsHeaderSelected;
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GStateText::boundingRect() const
{
	// Ustalanie rozmiarow tekstu
	int tWidth = 0;
	int tHeight = 0;
	
	// Jezeli zawartosc komorki
	if(fTextType == 0)
	{
		tWidth = fParent->columnWidth(_col);
		tHeight = fParent->rowHeight(_row);
	}
	
	// Jezeli tytul tabeli
	if(fTextType == 1)
	{
		// Sprawdzanie wymiarow czcionki
		QFontMetrics fm(Settings->xttTableTitleFont());
		tWidth = fm.width("State: " + fParent->tableTitle());
		tHeight = fm.height();
	}
	
	// Jezeli tresc naglowka
	if(fTextType == 2)
	{
		tWidth = fParent->columnWidth(_col);
		tHeight = fParent->headerHeight();
	}
     
     // Jezeli tresc etykiety
	if(fTextType == 3)
	{
		tWidth = fParent->columnWidth(-1);
		tHeight = fParent->rowHeight(_row);
	}
     
     // Jezeli tresc naglowka etykiety
	if(fTextType == 4)
	{
		tWidth = fParent->columnWidth(-1);
		tHeight = fParent->rowHeight(-1);
	}
	
     return QRectF(-5, 0, tWidth, tHeight);
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GStateText::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     QString text = "";
     QRect rect;
     int flags = Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextWordWrap;
	const int horizontal_text_margin = 10;
	const int vertical_text_margin = 4;
     const int field_hor_margin = 1;
     const int field_ver_margin = 1;
     
     if(fTextType == 0)
     {
          if((_row >= fParent->rowCount()) || (_col >= fParent->columnCount()))
               return;
          text = ":= " + _set->toString();
		setToolTip("");
		_p->setFont(Settings->xttTableFont());
		_p->setPen(Settings->xttTableFontColor());
		
		// Sprawdzanie wymiarow czcionki
		QFontMetrics fm(Settings->xttTableFont());
		int text_widht = fParent->columnWidth(_col);
		int text_height = fParent->rowHeight(_row);
		
		// Ewentualne powiekszenie rozmiarow
		if(Settings->xttCellAutosize())
		{
               int totalwidth = fm.width(text)+horizontal_text_margin+field_hor_margin;
               int totalheight = fm.height()+vertical_text_margin+field_ver_margin;
			if(text_widht < totalwidth)
			{
				text_widht = totalwidth;
				fParent->setColumnWidth(_col, text_widht);
			}
			if(text_height < totalheight)
			{
				text_height = totalheight;
				fParent->setRowHeight(_row, text_height);
			}
		}
		
		rect.setRect(field_hor_margin, field_ver_margin, text_widht-(field_hor_margin<<1), text_height-(field_ver_margin<<1));
     }
     else if(fTextType == 1)
     {
          text = "State: " + _text;
          
          flags = Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap;
		_p->setFont(Settings->xttTableTitleFont());
		_p->setPen(Settings->xtTableTitleFontColor());
		
		// Sprawdzanie wymiarow czcionki
		QFontMetrics fm(Settings->xttTableTitleFont());
		int text_widht = fm.width(text);
		int text_height = fm.height();
		
		rect.setRect(0, 0, text_widht, text_height);
     }
     else if(fTextType == 2)
     {
          QString aname = "";
          if(_attribute != NULL)
          {
               QString (XTT_Attribute::*reffunc)(void) = Settings->attsReferenceByAcronyms() == true ? &XTT_Attribute::Acronym : &XTT_Attribute::Name;
               aname = AttributeGroups->CreatePath(_attribute) + (_attribute->*reffunc)();
          }
          text = "(->) " + aname;
          
		_p->setFont(Settings->xttTableHeaderFont());
		_p->setPen(Settings->xttTableHeaderFontColor());
		
		// Sprawdzanie wymiarow czcionki
		QFontMetrics fm(Settings->xttTableHeaderFont());
		int text_widht = fParent->columnWidth(_col);
		int text_height = fParent->headerHeight();
		
		// Ewentualne powiekszenie rozmiarow
		if(Settings->xttCellAutosize())
		{
               int totalwidth = fm.width(text)+horizontal_text_margin+field_hor_margin;
               int totalheight = fm.height()+vertical_text_margin+field_ver_margin;
			if(text_widht < totalwidth)
			{
				text_widht = totalwidth;
				fParent->setColumnWidth(_col, text_widht);
			}
			if(text_height < totalheight)
			{
				text_height = totalheight;
				fParent->setRowHeight(-1, text_height);
			}
		}
		
		rect.setRect(field_hor_margin, field_ver_margin, text_widht-(field_hor_margin<<1), text_height-(field_ver_margin<<1));
     }
     else if((fTextType == 3) || (fTextType == 4))
     {
          text = _text;
          
		_p->setFont(Settings->xttTableHeaderFont());
		_p->setPen(Settings->xttTableHeaderFontColor());
		
		// Sprawdzanie wymiarow czcionki
		QFontMetrics fm(Settings->xttTableHeaderFont());
		int text_widht = fParent->columnWidth(_col);
		int text_height = fParent->headerHeight();
		
		// Ewentualne powiekszenie rozmiarow
		if(Settings->xttCellAutosize())
		{
               int totalwidth = fm.width(text)+horizontal_text_margin+field_hor_margin;
               int totalheight = fm.height()+vertical_text_margin+field_ver_margin;
			if(text_widht < totalwidth)
			{
				text_widht = totalwidth;
				fParent->setColumnWidth(_col, text_widht);
			}
			if(text_height < totalheight)
			{
				text_height = totalheight;
				fParent->setRowHeight(_row, text_height);
			}
		}
		
		rect.setRect(field_hor_margin, field_ver_margin, text_widht-(field_hor_margin<<1), text_height-(field_ver_margin<<1));
     }
     
	// Jezeli niepoprawna skladnia to podswietlamy komorke
	/*if((fTextType == 0) && (!fParent->Row(fRow)->Cell(fCol)->isCorrectSyntax()) && (Settings->xttHighlightErrorCell()))
		_p->fillRect(rect, QBrush(Settings->xttErrorCellColor(), MainWin->ModelBW()?Qt::Dense6Pattern:Qt::SolidPattern));*/
     
     // Jezeli komorka jest zaznaczona, lub jezeli jest to naglowek i jest zaznaczony
	if(((fTextType == 0) && (isSelected)) || ((fTextType == 2) && (fIsHeaderSelected)))
		_p->fillRect(rect, QBrush(Settings->xttHoveredCellColor(), MainWin->ModelBW()?Qt::Dense7Pattern:Qt::SolidPattern));

     _p->drawText(rect, flags, text);
}
//---------------------------------------------------------------------------

// #brief Function is called when location of the object is changed.
//
// #param _change Changing value.
// #param _value Reference to changed values.
// #return Value that was changed.
QVariant GStateText::itemChange(GraphicsItemChange change, const QVariant &value)
{
     if((change == ItemSelectedChange) && (scene()) && (fTextType == 0))
     {
		//frLevel = 1;
          bool isSel = value.toBool();
		isSelected = isSel;
		//frLevel = 0;
     }
     return QGraphicsItem::itemChange(change, value);
}

//---------------------------------------------------------------------------

// #brief Function that deallocate the memory
//
// #return No return value.
void GStateText::clearActions(void)
{
     if(aFullEditCell != NULL)
          delete aFullEditCell;
     if(aChangeRowLabel != NULL)
          delete aChangeRowLabel;
     if(aFastEditCell != NULL)
          delete aFastEditCell;
     if(aLoadState != NULL)
          delete aLoadState;
     if(aDeleteTable != NULL)
          delete aDeleteTable;
     if(aAddNewRow != NULL)
          delete aAddNewRow;
     if(aAddCopy != NULL)
          delete aAddCopy;
     if(aDeleteRow != NULL)
          delete aDeleteRow;
     if(aDeleteCol != NULL)
          delete aDeleteCol;
     if(aStateSchemaEditor != NULL)
          delete aStateSchemaEditor;
     if(aAttributeEditor != NULL)
          delete aAttributeEditor;
}
//---------------------------------------------------------------------------

// Metoda tworzaca polenia menu
// #brief Function creates menu actions.
//
// #return No return value.
void GStateText::createActions(void)
{
     clearActions();

	aFullEditCell = new QAction(QIcon(":/all_images/images/Edit1.png"), QObject::tr("Full edit..."), NULL);
     aFullEditCell->setStatusTip(QObject::tr("Allows edit this cell content"));
     connect(aFullEditCell, SIGNAL(triggered()), this, SLOT(onFullEdit()));
     
     aChangeRowLabel = new QAction(QObject::tr("Change label..."), NULL);
     aChangeRowLabel->setStatusTip(QObject::tr("Allows to change state label"));
     connect(aChangeRowLabel, SIGNAL(triggered()), this, SLOT(onLabelEdit()));
     
     aFastEditCell = new hMenuAction("", QObject::tr("Simple edit..."), NULL);
     aFastEditCell->setStatusTip(QObject::tr("Allows edit this cell content"));
     connect(aFastEditCell, SIGNAL(setCreatorConfirmed()), this, SLOT(onFinishSimpleEdit()));
     
     aLoadState = new hMenuAction(params("loadstate", "tr"), QObject::tr("Load state"), NULL);
     aLoadState->setStatusTip(QObject::tr("Loads the current state into xtt model"));
	
	aDeleteTable = new hMenuAction(params("deletestategroup", "t"), QIcon(":/all_images/images/DelTabele.png"), QObject::tr("Delete states"), NULL);
     aDeleteTable->setStatusTip(QObject::tr("Allows to remove all the states from the given group"));
     
     aAddNewRow = new hMenuAction(params("stateaddnewrow", "t"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Add new state"), NULL);
     aAddNewRow->setStatusTip(QObject::tr("Adds a new row at the end of the table"));
     
     aAddCopy = new hMenuAction(params("addcopyofstate", "tr"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Add copy of this state"), NULL);
     aAddCopy->setStatusTip(QObject::tr("Adds a copy of this state"));
	
	aDeleteRow = new hMenuAction(params("deletestate", "tr"), QIcon(":/all_images/images/DelRow.png"), QObject::tr("Delete state"), NULL);
     aDeleteRow->setStatusTip(QObject::tr("Allows remove this row from this table"));
	
	aDeleteCol = new hMenuAction(params("stateinputdeletecol", "t"), QIcon(":/all_images/images/DelCol.png"), QObject::tr("Delete col"), NULL);
     aDeleteCol->setStatusTip(QObject::tr("Allows remove this attribute from all the states in the group"));

	aStateSchemaEditor = new hMenuAction(params("stateeditschema", "t"), QObject::tr("Edit group schema..."), NULL);
     aStateSchemaEditor->setStatusTip(QObject::tr("Allows to edit the schema of the group of states"));
     
     aAttributeEditor = new hMenuAction(params("stateeditattribute", ""), QIcon(":/all_images/images/Edit1.png"), QObject::tr("Edit attribute"), NULL);
     aAttributeEditor->setStatusTip(QObject::tr("Allows to edit attribute related with this column"));
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse right button is pressed.
//
// #param event Pointer to event object.
// #return No return values.
void GStateText::contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent)
{
     if((fParent == NULL) || (fParent->isOutput()) || (fTextType == 1))
		return;
		
	createActions();
     
     // --------------------------------
     bool isMultivalued = false;   // indicates if the second argument of the cell function is a set defined as explicite values, not as expression or attribute
     bool isOrderedDomain = false; // indicates if the type of contraints of the attribute is a domain
     bool canExplodeDomain = false;// indicates if the domain is not too large to explode it
     bool singleRequired = true;   // indcates if the 2nd argument must be single valued
     bool isDefinedAsSet = false;  // indcates if the 2nd argument is defined as set
     
     int textType = fTextType;     // It changes during function executing - so it must be stored in a local variable
     
     QString rangeMenuTitle = "Add range";
     QString singleReqCmd = "true";
     
     // checking if the second arg of the cell function is a explicite defined set
     XTT_Attribute* attr = NULL;
     hSet* currset = NULL;

     if(((textType == 0) || (textType == 2)) && (fParent->rowCount() > 0))
     {
          attr = _attribute;
          if((attr != NULL) && (attr->Type()->typeClass() == TYPE_CLASS_DOMAIN) && (attr->Type()->domain()->ordered()))
               isOrderedDomain = true;
               
          hqed::clipboardXttAttribute()->push(attr);
          if(textType == 0)
          {
               currset = _set;
               hqed::clipboardSet()->push(currset);
               hqed::clipboardType()->push(attr->Type());
               isDefinedAsSet = true;
               
               if(attr->multiplicity())
               {
                    singleRequired = false;
                    isMultivalued = true;  
                    singleReqCmd = "false";
               }
               canExplodeDomain = attr->Type()->domain()->canBeExpanded();
          }
     }
     aFastEditCell->setAction("setcreator:"+singleReqCmd);
     // --------------------------------
	
	QMenu mainMenu(NULL);
     QMenu remsetitm("Remove set item");
     QMenu mCellContent("Change cell content");
     QMenu mRowMenu("Row");
     QMenu mColMenu("Column");
     QMenu mSimMenu("Start simulation from this state");
	
	if(textType == 3)
     {
          mainMenu.addAction(aLoadState);
          mainMenu.setDefaultAction(aLoadState);    
          mainMenu.addSeparator();
          
          // retrieving the list of plugins that supports simulation
          QList<devPlugin*>* pls = devPC->pluginsFilter(DEV_PLUGIN_SUPPORTED_MODULES_S);
          for(int i=0;i<pls->size();++i)
          {
               QMenu* newpluginsim = new QMenu(pls->at(i)->name());
               hMenuAction* ddisim = new hMenuAction(params("startdevpluginsimulation", "tr") + cmdsep() + pls->at(i)->name() + cmdsep() + "ddi", "ddi", NULL);
               hMenuAction* tdisim = new hMenuAction(params("startdevpluginsimulation", "tr") + cmdsep() + pls->at(i)->name() + cmdsep() + "gdi", "gdi", NULL);
               hMenuAction* gdisim = new hMenuAction(params("startdevpluginsimulation", "tr") + cmdsep() + pls->at(i)->name() + cmdsep() + "tdi", "tdi", NULL);
               newpluginsim->addAction(ddisim);
               newpluginsim->addAction(tdisim);
               newpluginsim->addAction(gdisim);
               QObject::connect(newpluginsim, SIGNAL(destroyed()), ddisim, SLOT(deleteLater()));
               QObject::connect(newpluginsim, SIGNAL(destroyed()), tdisim, SLOT(deleteLater()));
               QObject::connect(newpluginsim, SIGNAL(destroyed()), gdisim, SLOT(deleteLater()));
               QObject::connect(&mSimMenu, SIGNAL(destroyed()), newpluginsim, SLOT(deleteLater()));
               mSimMenu.addMenu(newpluginsim);
          }
          delete pls;
          
          mSimMenu.addSeparator();
          mSimMenu.addAction(new hMenuAction(params("starthqedsimulation", "tr"), QIcon(":/all_images/images/execute_small.png"), "HQEd simulation", NULL));
          
          mainMenu.addMenu(&mSimMenu);
          mainMenu.addSeparator();
          
          mainMenu.addAction(aChangeRowLabel);    
          mainMenu.addSeparator();
     }

	if(textType == 0)
     {
		mainMenu.addAction(aFullEditCell);
           if((canExplodeDomain) && (isDefinedAsSet))
               mainMenu.addAction(aFastEditCell);
           mainMenu.setDefaultAction(aFastEditCell);    
     }
	//mainMenu.addAction(aCellEditor);
     //mainMenu.addAction(aEidtTable);
	
     if(textType == 0)
     {
          // Creating mCellContent operator
          mainMenu.addSeparator();
          
          // Creating edit set menu
          if((canExplodeDomain) && ((isDefinedAsSet)))
          {
               mCellContent.addAction(aFastEditCell);
          }
          // Creating range menu
          if((isMultivalued) && (isOrderedDomain) && (canExplodeDomain))
          {
               QMenu* ranges = attr->Type()->domain()->createRangeMenu(rangeMenuTitle, "addmultiplevaluerange");
               QObject::connect(&mCellContent, SIGNAL(destroyed()), ranges, SLOT(deleteLater()));
               mCellContent.addMenu(ranges);
          }
          // Creating remove set item menu
          if((isDefinedAsSet) && (currset->size() > 1))
          {
               QStringList itms = currset->stringItems();
               for(int i=0;i<itms.size();++i)
                    remsetitm.addAction(new hMenuAction("setremoveitem:"+QString::number(i), "Item: "+itms.at(i), NULL));
               mCellContent.addMenu(&remsetitm);
          }
          
          if(!mCellContent.actions().empty())
               mainMenu.addMenu(&mCellContent);
     }
     
	mainMenu.addSeparator();
     mRowMenu.addAction(aAddNewRow);
     if((textType == 0) || (textType == 3))
	{
          mRowMenu.addAction(aAddCopy);
          if(fParent->rowCount() > 1)
          {
               mRowMenu.addSeparator();
               mRowMenu.addAction(aDeleteRow);
          }
	}
     mainMenu.addMenu(&mRowMenu);
          
     if(((textType == 0) || (textType == 2)) && (fParent->columnCount() > 1))
     {
          mColMenu.addAction(aDeleteCol);
          mainMenu.addMenu(&mColMenu);
     }
	
	mainMenu.addSeparator();
	mainMenu.addAction(aDeleteTable);
     
     if((textType == 0) || (textType == 2))
     {
          mainMenu.addSeparator();
          mainMenu.addAction(aAttributeEditor);
          mainMenu.addAction(aStateSchemaEditor);
     }
          
     // Setting the default action
     if((textType == 0) && (canExplodeDomain) && (isDefinedAsSet) && (Settings->xttTableEditMode() == XTT_TABLE_EDIT_MODE_SIMPLE))
          mainMenu.setDefaultAction(aFastEditCell);
     if((textType == 0) && ((!canExplodeDomain) || (!isDefinedAsSet) || (Settings->xttTableEditMode() == XTT_TABLE_EDIT_MODE_FULL)))
          mainMenu.setDefaultAction(aFullEditCell);
	if(textType == 2)
          mainMenu.setDefaultAction(aEidtTable);

     // Executing menu
	mainMenu.exec(contextMenuEvent->screenPos());

     // cleaning and refreshing
     if((textType == 0) || (textType == 2))
     {
          if(textType == 0)
          {
               hqed::clipboardSet()->pop();
               hqed::clipboardType()->pop();
          }
          hqed::clipboardXttAttribute()->pop();
     }
}
//---------------------------------------------------------------------------

// #brief Function is called when user presess mouse button on the item
//
// #param __event - Pointer to event object.
// #return No return values.
void GStateText::mousePressEvent(QGraphicsSceneMouseEvent* __event)
{
     if(fTextType == 3)
          fParent->selectRow(_row);
     
     QGraphicsItem::mousePressEvent(__event);
}
//---------------------------------------------------------------------------

// #brief Function is called when user cliks twice on the item
//
// #param __event - Pointer to event object.
// #return No return values.
void GStateText::mouseDoubleClickEvent(QGraphicsSceneMouseEvent* __event)
{
     QGraphicsItem::mouseDoubleClickEvent(__event);
}
//---------------------------------------------------------------------------

// #brief Function that creates the string that conatins the name of command with parameters
//
// #param __cmd - the name of the command
// #param __params - type of params
// #li t - tableid
// #li r - row
// #li c - column
// #return string that conatins the name of command with parameters
QString GStateText::params(QString __cmd, QString __params)
{
     QString result = __cmd;
     
     if(__params.contains("t", Qt::CaseInsensitive))
          result += cmdsep() + fParent->tableTitle();
     if(__params.contains("r", Qt::CaseInsensitive))
          result += cmdsep() + QString::number(_row);
     if(__params.contains("c", Qt::CaseInsensitive))
          result += cmdsep() + QString::number(_col);
          
     return result;
}
//---------------------------------------------------------------------------

// #brief Function that returns a command separator
//
// #return command separator
QString GStateText::cmdsep(void)
{
     return ":";
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user chooses full edit action from the context menu
//
// #return no values return
void GStateText::onFullEdit(void)
{
     // Entering to the full edition mode
     if(_setEditor != NULL)
          delete _setEditor;
     _setEditor = new SetEditor_ui;

     QObject::connect(_setEditor, SIGNAL(okClicked()), this, SLOT(onFinishFullEdit()));
     int smr = _setEditor->setMode(_attribute->Type(), _set, 4, !_attribute->multiplicity(), false, NULL);

     if(smr == SET_WINDOW_MODE_FAIL)
          return;
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
     _setEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user closes the set editor with the OK button
//
// #return no values return
void GStateText::onFinishFullEdit(void)
{
     hSet* result = _setEditor->result();
     _set->flush();
     result->copyTo(_set);
     onFinishEdit();
     
     delete result;
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user closes the set creator with the OK button
//
// #return no values return
void GStateText::onFinishSimpleEdit(void)
{
     onFinishEdit();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user closes the set editor/creator with the OK button
//
// #return no values return
void GStateText::onFinishEdit(void)
{
     // clearing state trajectory
     XTT_State* state = fParent->stateAt(_row);
     if(state != NULL)
          state->cancelSimulation();
          
     scene()->update();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when user chooses label edit action from the context menu
//
// #return no values return
void GStateText::onLabelEdit(void)
{
     bool ok;
     QString result = QInputDialog::getText(NULL, "State label", "Enter a new state label:", QLineEdit::Normal, fParent->labelAt(_row), &ok);
     result = result.simplified();
     result.replace("/", "");
     if(ok)
          fParent->setLabelAt(_row, result);
}
//---------------------------------------------------------------------------
