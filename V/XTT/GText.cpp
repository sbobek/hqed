/*
 *	   $Id: GText.cpp,v 1.41.4.10 2012-01-12 19:51:01 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QFont>
#include <QFontMetrics>

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hDomain.h"
#include "../../M/hTypeList.h"
#include "../../M/hFunction.h"
#include "../../M/hExpression.h"
#include "../../M/hFunctionList.h"
#include "../../M/hMultipleValue.h"
#include "../../M/hFunctionArgument.h"
#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Column.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../C/Settings_ui.h"
#include "../../C/MainWin_ui.h"
#include "../../C/widgets/hMenuAction.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"
#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "GTable.h"
#include "GSortButton.h"
#include "GText.h"
//---------------------------------------------------------------------------

// #brief Constructor GText class.
//
// #param _parent Pointer to the parent of the table.
// #param _tt Index of the table.
// #param _row Index of the row of the cell.
// #param _col Index of the column of the cell.
GText::GText(XTT_Table* _parent, unsigned int _tt, unsigned int _row, unsigned int _col) :
    #if QT_VERSION >= 0x040600
    QGraphicsObject(0)
  #else
    QObject(0),
    QGraphicsItem(0)
  #endif
{
    setZValue(3);
    fParent = _parent;
    fRow = _row;
    fCol = _col;
    isSelected = false;
    fIsHeaderSelected = false;
    initSortButtons();
    setTextType(_tt);
    agSortActionGroup = new QActionGroup(this);

    setAcceptsHoverEvents(true);
}
//---------------------------------------------------------------------------

// #brief Destructor of GText class.
GText::~GText(void)
{
    delete _sortButtons[0];
    delete _sortButtons[1];
    delete agSortActionGroup;
}
//---------------------------------------------------------------------------

// #brief Function that initializes the SortButtons elements
//
// #return no values return
void GText::initSortButtons(void)
{
    _sortButtons[0] = new GSortButton(true, this);
    _sortButtons[1] = new GSortButton(false, this);

    _sortButtons[0]->setToolTip("Ascending sort");
    _sortButtons[1]->setToolTip("Descending sort");
    connect(_sortButtons[0], SIGNAL(onMousePress()), this, SLOT(ascendingSort()));
    connect(_sortButtons[1], SIGNAL(onMousePress()), this, SLOT(descendingSort()));

    posSortButtons();
}
//---------------------------------------------------------------------------

// #brief Function that sets the position of the SortButtons elements
//
// #return no values return
void GText::posSortButtons(void)
{
    if(fTextType != 2)
        return;

    QRectF rect = boundingRect();
    double itemleft = rect.x() + 12.;
    double rectheight = rect.height();
    double margin3 = rectheight - _sortButtons[0]->edgeLength() - _sortButtons[1]->edgeLength();
    double margin = margin3/3.;
    double top1 = rect.y() + margin;
    double top2 = rect.y() + 2*margin + _sortButtons[0]->edgeLength();

    _sortButtons[0]->setPos(itemleft, top1);
    _sortButtons[1]->setPos(itemleft, top2);
}
//---------------------------------------------------------------------------

// #brief Performs ascending sort in the cell
//
// #return no values return
void GText::ascendingSort(void)
{
    QString msg;
    if(!fParent->sortRules(fCol, XTT_COLUMN_SORT_ASCENDING, msg))
        QMessageBox::critical(NULL, "HQEd", "Sorting error:\n" + msg, QMessageBox::Ok);

    xtt::fullVerification();
}
//---------------------------------------------------------------------------

// #brief Performs descending sort in the cell
//
// #return Performs descending sort in the cell
void GText::descendingSort(void)
{
    QString msg;
    if(!fParent->sortRules(fCol, XTT_COLUMN_SORT_DESCENDING, msg))
        QMessageBox::critical(NULL, "HQEd", "Sorting error:\n" + msg, QMessageBox::Ok);

    xtt::fullVerification();
}
//---------------------------------------------------------------------------

// #brief Function sets type of the text.
//
// #return No return value.
void GText::setTextType(unsigned int _tt)
{
    _sortButtons[0]->setVisible(_tt == 2);
    _sortButtons[1]->setVisible(_tt == 2);
    fTextType = _tt;
}
//---------------------------------------------------------------------------

// #brief Function is called mouse is hanging over the cell.
//
// #param event Pointer to event object.
// #return No return values.
void GText::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    if(scene() == NULL)
        return;

    isSelected = true;
    if((Settings->xttEnabledAutocentering()) && (scene()->mouseGrabberItem() != this))
        ensureVisible(boundingRect(), 0, 0);

    QGraphicsItem::hoverEnterEvent(event);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse is leaving the cell.
//
// #param event Pointer to event object.
// #return No return values.
void GText::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    if(fTextType == 0)
        fParent->Row(fRow)->Cell(fCol)->setSelected(false);

    isSelected = false;
    fIsHeaderSelected = false;
    QGraphicsItem::hoverLeaveEvent(event);
}
//---------------------------------------------------------------------------

// #brief Allow to select header cell
//
// #param bool _selStat - selection status
// #return No return value
void GText::selectHeaderCell(bool _selStat)
{
    fIsHeaderSelected = _selStat;
    if(_selStat)
        ensureVisible(boundingRect(), 0, 0);
    update();
}
//---------------------------------------------------------------------------

// #brief Return status of header selection
//
// #return True is header is selected, otherwise false
bool GText::isHeaderSelected(void)
{
    return fIsHeaderSelected;
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GText::boundingRect() const
{
    // Ustalanie rozmiarow tekstu
    int tWidth = 0;
    int tHeight = 0;

    // Jezeli zawartosc komorki
    if(fTextType == 0)
    {
        tWidth = fParent->ColWidth(fCol);
        tHeight = fParent->RowHeight(fRow);
    }

    // Jezeli tytul tabeli
    if(fTextType == 1)
    {
        // Sprawdzanie wymiarow czcionki
        QFontMetrics fm(Settings->xttTableTitleFont());
        tWidth = fm.width("Table id: " + fParent->TableId() + " - " + fParent->Title());
        tHeight = fm.height();
    }

    // Jezeli tresc naglowka
    if(fTextType == 2)
    {
        tWidth = fParent->ColWidth(fCol);
        tHeight = fParent->HeaderHeight();
    }

    return QRectF(-5, 0, tWidth, tHeight);
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GText::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
    QString text = "";
    QRectF rect;
    int flags = Qt::AlignHCenter | Qt::AlignVCenter | Qt::TextWordWrap;
    const int horizontal_text_margin = 10;
    const int vertical_text_margin = 4;
    const int field_hor_margin = 1;
    const int field_ver_margin = 1;

    if(fTextType == 0)
    {
        if((fRow >= fParent->RowCount()) || (fCol >= (unsigned int)fParent->ColCount()))
            return;
        text = fParent->Row(fRow)->Cell(fCol)->Content()->toString();
        setToolTip("");
        if(Settings->xttShowErrorInToolTip())
            setToolTip(fParent->Row(fRow)->Cell(fCol)->CellError(false));
        _p->setFont(Settings->xttTableFont());
        _p->setPen(Settings->xttTableFontColor());

        // Sprawdzanie wymiarow czcionki
        QFontMetrics fm(Settings->xttTableFont());
        int text_widht = fParent->ColWidth(fCol);
        int text_height = fParent->RowHeight(fRow);

        // Ewentualne powiekszenie rozmiarow
        if(Settings->xttCellAutosize())
        {
            int totalwidth = fm.width(text)+horizontal_text_margin+field_hor_margin;
            int totalheight = fm.height()+vertical_text_margin+field_ver_margin;
            if(text_widht < totalwidth)
            {
                text_widht = totalwidth;
                fParent->setColWidth(fCol, text_widht);
                fParent->setLocalization(fParent->Localization());
            }
            if(text_height < totalheight)
            {
                text_height = totalheight;
                fParent->setRowHeight(fRow, text_height);
                fParent->setLocalization(fParent->Localization());
            }
        }

        rect.setRect(field_hor_margin, field_ver_margin, text_widht-(field_hor_margin<<1), text_height-(field_ver_margin<<1));
    }
    else if(fTextType == 1)
    {
        text = "Table id: " + fParent->TableId() + " - " + fParent->Title();

        flags = Qt::AlignLeft | Qt::AlignVCenter | Qt::TextWordWrap;
        _p->setFont(Settings->xttTableTitleFont());
        _p->setPen(Settings->xtTableTitleFontColor());

        // Sprawdzanie wymiarow czcionki
        QFontMetrics fm(Settings->xttTableTitleFont());
        int text_widht = fm.width(text);
        int text_height = fm.height();

        rect.setRect(0, 0, text_widht, text_height);
    }
    else if(fTextType == 2)
    {
        int context = fParent->ColContext(fCol);

        QString aname = "";
        if(context != 5)
        {
            QString (XTT_Attribute::*reffunc)(void) = Settings->attsReferenceByAcronyms() == true ? &XTT_Attribute::Acronym : &XTT_Attribute::Name;
            aname = AttributeGroups->CreatePath(fParent->ContextAtribute(fCol)) + (fParent->ContextAtribute(fCol)->*reffunc)();
        }
        if(aname == "#NAN")
        {
            QMessageBox::critical(NULL, "HQEd", "Missing pointer.", QMessageBox::Ok);
            return;
        }
        // Ustalanie kontekstu i dodanie znacznika kontekstu
        QStringList ctx_smbl;
        ctx_smbl << "(?) " << "(+) " << "(-) " << "(->) " << "*";
        aname = ctx_smbl.at(context-1) + aname;
        text = aname;

        _p->setFont(Settings->xttTableHeaderFont());
        _p->setPen(Settings->xttTableHeaderFontColor());

        // Sprawdzanie wymiarow czcionki
        QFontMetrics fm(Settings->xttTableHeaderFont());
        int text_widht = fParent->ColWidth(fCol);
        int text_height = fParent->HeaderHeight();
        int btnwdth = (int)_sortButtons[0]->edgeLength();

        // Ewentualne powiekszenie rozmiarow
        if(Settings->xttCellAutosize())
        {
            int totalwidth = fm.width(text)+horizontal_text_margin+field_hor_margin+btnwdth;
            int totalheight = fm.height()+vertical_text_margin+field_ver_margin;
            if(text_widht < totalwidth)
            {
                text_widht = totalwidth;
                fParent->setColWidth(fCol, text_widht);
                fParent->setLocalization(fParent->Localization());
            }
            if(text_height < totalheight)
            {
                text_height = totalheight;
                fParent->setHeaderHeight(text_height);
                fParent->setLocalization(fParent->Localization());
            }
        }

        rect.setRect(field_hor_margin, field_ver_margin, text_widht+btnwdth-(field_hor_margin<<1)-1, text_height-(field_ver_margin<<1)-1.5);
        posSortButtons();
        rect.setRect(rect.x(), rect.y(), rect.width() - 5, rect.height());
    }

    // Jezeli niepoprawna skladnia to podswietlamy komorke
    if((fTextType == 0) && (!fParent->Row(fRow)->Cell(fCol)->isCorrectSyntax()) && (Settings->xttHighlightErrorCell()))
        _p->fillRect(rect, QBrush(Settings->xttErrorCellColor(), MainWin->ModelBW()?Qt::Dense6Pattern:Qt::SolidPattern));

    // Jezeli komorka nie jest zaznaczona i powinna przyjac drugi kolor zaznaczenia
    if((fTextType == 0) && ((fRow%2) == 0) && ((!isSelected) || (!fParent->Row(fRow)->Cell(fCol)->isSelected())))
    {
        _p->fillRect(rect, QBrush(Settings->xttTable2Color(), MainWin->ModelBW()?Qt::Dense7Pattern:Qt::SolidPattern));
        if(fParent->ColContext(fCol) == 4)
        {
            QPen currentPen = _p->pen();
            _p->setPen(QPen(Settings->xttTableBorderColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
            _p->drawLine(rect.x()+1, rect.y(), rect.x()+1, rect.y()+rect.height());
            _p->setPen(currentPen);
        }
    }

    // Jezeli jest to naglowek, to powinien miec odpowiedni kolor
    if(fTextType == 2)
    {
        _p->fillRect(rect, QBrush(Settings->xttTableHeaderColor(), Qt::SolidPattern));
    }

    // Jezeli komorka jest zaznaczona
    if(((fTextType == 0) && ((isSelected) || (fParent->Row(fRow)->Cell(fCol)->isSelected()))))
        _p->fillRect(rect, QBrush(Settings->xttHoveredCellColor(), MainWin->ModelBW()?Qt::Dense7Pattern:Qt::SolidPattern));

    if(fParent->RowCount() == 0 || fParent->ColCount() == 0)
    {
        _p->drawText(rect, flags, text);
        return;
    }

    if(fParent->Row(fRow)->Cell(fCol)->CellId() == static_cast<MyGraphicsScene*>(scene())->FocusCellID() &&
       fParent->TableId() == static_cast<MyGraphicsScene*>(scene())->FocusTableID() && fTextType == 0)
    {
        _p->setPen(QPen(QBrush(Qt::SolidPattern), 3));
        _p->drawRect(rect);
    }

    // Dostosuj rozmiar prostokata, w ktorym umieszczony zostanie tekst w przypadku gdy jest to tekst naglowka
    if(fTextType == 2)
        rect.setRect(rect.x(), rect.y(), rect.width()+5, rect.height());

    _p->drawText(rect, flags, text);
}
//---------------------------------------------------------------------------

// Metoda tworzaca polenia menu
// #brief Function creates menu actions.
//
// #return No return value.
void GText::createActions(void)
{
    aEidtTable = new hMenuAction(params("edittable", "t"), QIcon(":/all_images/images/Edit1.png"), QObject::tr("Table schema..."), NULL);
    aEidtTable->setStatusTip(QObject::tr("Allows edit this table"));

    aCellEditor = new hMenuAction(params("celleditor", "t"), QIcon(":/all_images/images/Edit2.png"), QObject::tr("Cell editor..."), NULL);
    aCellEditor->setStatusTip(QObject::tr("Show cell editor"));

    aNewEditCell = new hMenuAction(params("neweditor", "trc"), QIcon(":/all_images/images/Edit1.png"), QObject::tr("New editor..."), NULL);
    aNewEditCell->setStatusTip(QObject::tr("Allows edit this cell content"));

    aFullEditCell = new hMenuAction(params("expressioneditor", "trc"), QIcon(":/all_images/images/Edit1.png"), QObject::tr("Full edit..."), NULL);
    aFullEditCell->setStatusTip(QObject::tr("Allows edit this cell content"));

    aFastEditCell = new hMenuAction("", QObject::tr("Simple edit..."), NULL);
    aFastEditCell->setStatusTip(QObject::tr("Allows edit this cell content"));

    aDeleteTable = new hMenuAction(params("deletetable", "t"), QIcon(":/all_images/images/DelTabele.png"), QObject::tr("Delete table"), NULL);
    aDeleteTable->setStatusTip(QObject::tr("Allows remove this table"));

    aInsertRowBefore = new hMenuAction(params("insertrowbefore", "tr"), QIcon(":/all_images/images/AddRowBefore.png"), QObject::tr("Insert row before"), NULL);
    aInsertRowBefore->setStatusTip(QObject::tr("Allows add new row before this row"));

    aInsertRowAfter = new hMenuAction(params("insertrowafter", "tr"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Insert row after"), NULL);
    aInsertRowAfter->setStatusTip(QObject::tr("Allows add new row after this row"));

    aAddNewRow = new hMenuAction(params("addnewrow", "t"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Add new row"), NULL);
    aAddNewRow->setStatusTip(QObject::tr("Adds a new row at the end of the table"));

    aCopyRowBefore = new hMenuAction(params("copyrowbefore", "tr"), QIcon(":/all_images/images/AddRowBefore.png"), QObject::tr("Copy row before"), NULL);
    aCopyRowBefore->setStatusTip(QObject::tr("Allows to copy the current row and insert this copy before the current row"));

    aCopyRowAfter = new hMenuAction(params("copyrowafter", "tr"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Copy row after"), NULL);
    aCopyRowAfter->setStatusTip(QObject::tr("Allows to copy the current row and insert this copy after the current row"));

    aAddCopy = new hMenuAction(params("addcopyofrow", "tr"), QIcon(":/all_images/images/AddRowAfter.png"), QObject::tr("Add copy of this row"), NULL);
    aAddCopy->setStatusTip(QObject::tr("Adds a copy of this row at the end of the table"));

    aMoveRowUp = new hMenuAction(params("moverowup", "tr"), QIcon(":/all_images/images/RowMoveUp.png"), QObject::tr("Move row up"), NULL);
    aMoveRowUp->setStatusTip(QObject::tr("Allows move this row one position up"));

    aMoveRowDown = new hMenuAction(params("moverowdown", "tr"), QIcon(":/all_images/images/RowMoveDown.png"), QObject::tr("Move row down"), NULL);
    aMoveRowDown->setStatusTip(QObject::tr("Allows move this row one position down"));

    aDeleteRow = new hMenuAction(params("deleterow", "tr"), QIcon(":/all_images/images/DelRow.png"), QObject::tr("Delete row"), NULL);
    aDeleteRow->setStatusTip(QObject::tr("Allows remove this row from this table"));

    aDeleteCol = new hMenuAction(params("deletecol", "tc"), QIcon(":/all_images/images/DelCol.png"), QObject::tr("Delete col"), NULL);
    aDeleteCol->setStatusTip(QObject::tr("Allows remove this column from this table"));

    aSortCriterionMin = new hMenuAction(params("changetablesortcriterion", "tc") + cmdsep() + QString::number(XTT_COLUMN_SORT_MIN_VALUE), "Minimum value", NULL);
    aSortCriterionMin->setStatusTip(QObject::tr("Set the sorting criterion as minimum value"));
    aSortCriterionMin->setCheckable(true);
    aSortCriterionMin->setChecked(fParent->column(fCol)->sortCriterion() == XTT_COLUMN_SORT_MIN_VALUE);
    agSortActionGroup->addAction(aSortCriterionMin);

    aSortCriterionMax = new hMenuAction(params("changetablesortcriterion", "tc") + cmdsep() + QString::number(XTT_COLUMN_SORT_MAX_VALUE), "Maximum value", NULL);
    aSortCriterionMax->setStatusTip(QObject::tr("Set the sorting criterion as minimum value"));
    aSortCriterionMax->setCheckable(true);
    aSortCriterionMax->setChecked(fParent->column(fCol)->sortCriterion() == XTT_COLUMN_SORT_MAX_VALUE);
    agSortActionGroup->addAction(aSortCriterionMax);

    aSortCriterionPow = new hMenuAction(params("changetablesortcriterion", "tc") + cmdsep() + QString::number(XTT_COLUMN_SORT_POWER), "Set power", NULL);
    aSortCriterionPow->setStatusTip(QObject::tr("Set the sorting criterion as set power"));
    aSortCriterionPow->setCheckable(true);
    aSortCriterionPow->setChecked(fParent->column(fCol)->sortCriterion() == XTT_COLUMN_SORT_POWER);
    agSortActionGroup->addAction(aSortCriterionPow);

    aSortCriterionAlp = new hMenuAction(params("changetablesortcriterion", "tc") + cmdsep() + QString::number(XTT_COLUMN_SORT_ALPHABETICAL), "Alphabetical", NULL);
    aSortCriterionAlp->setStatusTip(QObject::tr("Set the sorting criterion as alphabetical"));
    aSortCriterionAlp->setCheckable(true);
    aSortCriterionAlp->setChecked(fParent->column(fCol)->sortCriterion() == XTT_COLUMN_SORT_ALPHABETICAL);
    agSortActionGroup->addAction(aSortCriterionAlp);

    aAttributeEditor = new hMenuAction(params("editattribute", "tc"), QIcon(":/all_images/images/Edit1.png"), QObject::tr("Edit attribute"), NULL);
    aAttributeEditor->setStatusTip(QObject::tr("Allows to edit attribute related with this column"));
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse right button is pressed.
//
// #param event Pointer to event object.
// #return No return values.
void GText::contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent)
{
    if(fTextType == 1)
        return;

    int context = fParent->ColContext(fCol);
    createActions();

    // --------------------------------
    bool isMultivalued = false;   // indicates if the second argument of the cell function is a set defined as explicite values, not as expression or attribute
    bool isOrderedDomain = false; // indicates if the type of contraints of the attribute is a domain
    bool canExplodeDomain = false;// indicates if the domain is not too large to explode it
    bool singleRequired = true;   // indcates if the 2nd argument must be single valued
    bool isDefinedAsSet = false;  // indcates if the 2nd argument is defined as set

    QString rangeMenuTitle = "Add range";
    QString singleReqCmd = "true";

    // checking if the second arg of the cell function is a explicite defined set
    XTT_Attribute* attr = NULL;
    XTT_Cell* cell = NULL;
    hExpression* cellexpr = NULL;
    hFunction* cellfunc = NULL;
    hFunctionArgument* cellfuncarg2 = NULL;
    hMultipleValue* cellfuncarg2value = NULL;
    if(((fTextType == 0) || (fTextType == 2)) && (fParent->RowCount() > 0))
    {
        cell = fParent->Row(fRow)->Cell(fCol);
        attr = fParent->ContextAtribute(fCol);
        if((attr != NULL) && (attr->Type()->typeClass() == TYPE_CLASS_DOMAIN) && (attr->Type()->domain()->ordered()))
            isOrderedDomain = true;

        if(fTextType == 0)
        {
            if(cell != NULL)
                cellexpr = cell->Content()->expressionField();
            if(cellexpr != NULL)
                cellfunc = cellexpr->function();
            if((cellfunc != NULL) && (cellfunc->argCount() == 2))
                cellfuncarg2 = cellfunc->argument(1);
            if(cellfuncarg2 != NULL)
            {
                cellfuncarg2value = cellfuncarg2->value();
                hqed::clipboardSet()->push(cellfuncarg2value->valueField());
                hqed::clipboardType()->push(attr->Type());
                hqed::clipboardMultipleValue()->push(cellfuncarg2value);

                if((cellfuncarg2->value()->type() == VALUE) && (cellfuncarg2->value()->valueField()->hasOnlyConstantValues()))
                    isDefinedAsSet = true;

                if(cellfuncarg2->valueMultiplicity() == FUNCTION_TYPE__MULTI_VALUED)
                {
                    singleRequired = false;
                    isMultivalued = true;
                    singleReqCmd = "false";
                    if(cellfuncarg2->value()->type() != VALUE)
                        rangeMenuTitle = "Set as range";
                }
            }
            canExplodeDomain = attr->Type()->domain()->canBeExpanded();
        }
    }
    aFastEditCell->setAction("setcreator:"+singleReqCmd);
    // --------------------------------

    QMenu mainMenu(NULL);
    QMenu mCellOperator("Change cell operator");
    QMenu mCellContent("Change cell content");
    QMenu remsetitm("Remove set item");
    QMenu mRowMenu("Row");
    QMenu mColMenu("Column");
    QMenu mSortCriterionMenu("Sort criterion");

    if(fTextType == 0)
    {
        mainMenu.addAction(aNewEditCell);
        mainMenu.addAction(aFullEditCell);
        if((canExplodeDomain) && (isDefinedAsSet))
            mainMenu.addAction(aFastEditCell);
        //mainMenu.setDefaultAction(aFastEditCell);
    }
    mainMenu.addAction(aCellEditor);
    mainMenu.addAction(aEidtTable);

    if(fTextType == 0)
    {
        // Creating mCellOperator operator
        // Reading compatible operators
        int mlt = fParent->ContextAtribute(fCol)->multiplicity() == true ? FUNCTION_TYPE__MULTI_VALUED : FUNCTION_TYPE__SINGLE_VALUED;
        hType* reqt = context==1?typeList->at(hpTypesInfo.iopti(BOOLEAN_BASED)):typeList->specialType(VOID_BASED);
        QList<hFunction*> opers = funcList->findFunctions(reqt, mlt, false);

        // Creating mCellContent operator
        mainMenu.addSeparator();
        for(int i=0;i<opers.size();++i)
        {
            QString cmd = params("changecelloperator", "trc") + cmdsep() + opers.at(i)->internalRepresentation();
            hMenuAction* newAction = new hMenuAction(cmd, opers.at(i)->userRepresentation(), NULL);
            mCellOperator.addAction(newAction);
        }
        mainMenu.addMenu(&mCellOperator);

        // Creating edit set menu
        if((canExplodeDomain) && ((isDefinedAsSet)))
        {
            mCellContent.addAction(aFastEditCell);
        }
        // Creating range menu
        if((isMultivalued) && (isOrderedDomain) && (canExplodeDomain))
        {
            QMenu* ranges = attr->Type()->domain()->createRangeMenu(rangeMenuTitle, "addmultiplevaluerange");
            QObject::connect(&mCellContent, SIGNAL(destroyed()), ranges, SLOT(deleteLater()));
            mCellContent.addMenu(ranges);
        }
        // Creating remove set item menu
        if(isDefinedAsSet)
        {
            QStringList itms = cellfuncarg2->value()->valueField()->stringItems();
            for(int i=0;i<itms.size();++i)
                remsetitm.addAction(new hMenuAction("setremoveitem:"+QString::number(i), "Item: "+itms.at(i), NULL));
            mCellContent.addMenu(&remsetitm);
        }

        mCellContent.addSeparator();
        QString cmd = params("resetcellcontent", "trc");
        hMenuAction* newAction = new hMenuAction(cmd, "Reset content", NULL);
        mCellContent.addAction(newAction);

        mainMenu.addMenu(&mCellContent);
    }

    mainMenu.addSeparator();
    mRowMenu.addAction(aAddNewRow);
    if(fTextType == 0)
    {
        mRowMenu.addAction(aInsertRowBefore);
        mRowMenu.addAction(aInsertRowAfter);

        mRowMenu.addSeparator();
        mRowMenu.addAction(aCopyRowBefore);
        mRowMenu.addAction(aCopyRowAfter);
        mRowMenu.addAction(aAddCopy);

        mRowMenu.addSeparator();
        mRowMenu.addAction(aMoveRowUp);
        mRowMenu.addAction(aMoveRowDown);

        mRowMenu.addSeparator();
        if(fParent->RowCount() > 1)
            mRowMenu.addAction(aDeleteRow);
    }
    mainMenu.addMenu(&mRowMenu);

    if(fParent->ColCount() > 1)
    {
        mColMenu.addAction(aDeleteCol);
        mainMenu.addMenu(&mColMenu);
    }

    mainMenu.addSeparator();
    if(isOrderedDomain)
    {
        mSortCriterionMenu.addAction(aSortCriterionMin);
        mSortCriterionMenu.addAction(aSortCriterionMax);
    }
    mSortCriterionMenu.addAction(aSortCriterionPow);
    mSortCriterionMenu.addAction(aSortCriterionAlp);
    mainMenu.addMenu(&mSortCriterionMenu);

    mainMenu.addSeparator();
    mainMenu.addAction(aDeleteTable);
    mainMenu.addSeparator();
    if(context != 5)
        mainMenu.addAction(aAttributeEditor);

    // Setting the default action
    if((fTextType == 0) && (canExplodeDomain) && (isDefinedAsSet) && (Settings->xttTableEditMode() == XTT_TABLE_EDIT_MODE_SIMPLE))
        mainMenu.setDefaultAction(aFastEditCell);
    if((fTextType == 0) && ((!canExplodeDomain) || (!isDefinedAsSet) || (Settings->xttTableEditMode() == XTT_TABLE_EDIT_MODE_FULL)))
        mainMenu.setDefaultAction(aFullEditCell);
    if(fTextType == 2)
        mainMenu.setDefaultAction(aEidtTable);

    // Executing menu
    if(mainMenu.exec(contextMenuEvent->screenPos()) == aDeleteTable)
        return;

    // cleaning and refreshing
    if(cellfuncarg2 != NULL)
    {
        hqed::clipboardSet()->pop();
        hqed::clipboardType()->pop();
        hqed::clipboardMultipleValue()->pop();
    }
    fParent->GraphicTable()->RePosition();

    foreach(QAction* action, mCellOperator.actions())
         delete action;

    // These are destroyed together with mainMenu
    /*delete aEidtTable;
 delete aCellEditor;
 delete aFullEditCell;
 delete aDeleteTable;
 delete aInsertRowBefore;
 delete aInsertRowAfter;
 delete aDeleteRow;
 delete aDeleteCol;
 delete aMoveRowDown;
 delete aMoveRowUp;*/
}
//---------------------------------------------------------------------------

// #brief Function that returns a command separator
//
// #return command separator
QString GText::cmdsep(void)
{
    return ":";
}
//---------------------------------------------------------------------------

// #brief Function that creates the string that conatins the name of command with parameters
//
// #param __cmd - the name of the command
// #param __params - type of params
// #li t - tableid
// #li r - row
// #li c - column
// #return string that conatins the name of command with parameters
QString GText::params(QString __cmd, QString __params)
{
    QString result = __cmd;

    if(__params.contains("t", Qt::CaseInsensitive))
        result += cmdsep() + fParent->TableId();
    if(__params.contains("r", Qt::CaseInsensitive))
        result += cmdsep() + QString::number(fRow);
    if(__params.contains("c", Qt::CaseInsensitive))
        result += cmdsep() + QString::number(fCol);

    return result;
}
//---------------------------------------------------------------------------
