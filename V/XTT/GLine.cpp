/*
 *	   $Id: GLine.cpp,v 1.30 2010-01-08 19:47:55 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../M/XTT/XTT_Table.h"
#include "../../C/Settings_ui.h"
#include "GLine.h"
//---------------------------------------------------------------------------

// #brief Contructor GLine class.
//
// #param _parent Pointer to the parent table.
// #param _isVert Determines whether the line is vertical.
// #param _sizeIndex Index of the row/column to which line is assigned.
GLine::GLine(XTT_Table* _parent, bool _isVert, int)
{
     setZValue(2);
     
     fParent = _parent;
     fIsVertical = _isVert;
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GLine::boundingRect() const
{
     int dx = 3;
     int dy = 3;

     if(!fIsVertical)
          dx = fParent->Width();
     if(fIsVertical)
          dy = fParent->Height();

     return QRectF(-1, -1, dx+1, dy+1);
}
//---------------------------------------------------------------------------

// #brief Function draws square on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GLine::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     // Ustalanie wspolrzednych
     int dx = 0;
     int dy = 0;

     if(!fIsVertical)
          dx = fParent->Width();
     if(fIsVertical)
          dy = fParent->Height();

     QLineF line(0, 0, dx, dy);
     _p->setPen(QPen(Settings->xttTableBorderColor(), 1, Qt::SolidLine, Qt::SquareCap, Qt::RoundJoin));
     _p->drawLine(line);
}
//---------------------------------------------------------------------------
