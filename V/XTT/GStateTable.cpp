/*
 *	   $Id: GStateTable.cpp,v 1.1.8.2 2011-09-01 20:03:44 hqeddoxy Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/StateGraphicsScene.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_Column.h"
#include "../../M/XTT/XTT_Attribute.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "GStateRect.h"
#include "GStateText.h"
#include "GStateTable.h"
//----------------------------------------------------------------------------

// #brief Constructor GStateTable class.
//
// #param __scene - Pointer to the parent scene
// #param _p Pointer to the parent of the table.
GStateTable::GStateTable(MyGraphicsScene* __scene, XTT_Statesgroup* _p) : _parentScene(__scene)
{
     _headerHeight = Settings->xttDefaultRowHeight();
     _labelWidth = Settings->xttDefaultColumnWidth();
     
     _nextidindex = 0;
     fTextCount = 0;
     _tableType = 0;
     fBase = NULL;
     fText = NULL;
     _localization = QPointF(0., 0.);

     _amIvisible = false;
     _amIoutput = false;
     setParent(_p);
}
//----------------------------------------------------------------------------

// #brief Destructor GStateTable class.
GStateTable::~GStateTable(void)
{
     clear();
}
//----------------------------------------------------------------------------

// #brief Function that clears all the data
//
// #return no values return
void GStateTable::clear(void)
{
     unselectAllElements();
     removeAll();
     _rowHeights.clear();
     
     while(!_columns.empty())
          delete _columns.takeFirst();
}
//----------------------------------------------------------------------------

// #brief Function that sets the parent of this object
//
// #param __parent - pointer to the new parent
// #return no values return
void GStateTable::setParent(XTT_Statesgroup* __parent)
{
     if(__parent == NULL)
          return;
          
     fParent = __parent;
     _amIoutput = fParent->isOutputGroup();
     refreshAll();
}
//----------------------------------------------------------------------------

// #brief Function that refreshes all the data
//
// #return No return value.
void GStateTable::refreshAll(void)
{
     clear();

     QList<XTT_Attribute*>* atts;

     if(_amIoutput)
          atts = fParent->outputAttributes();
     else
          atts = fParent->inputAttributes();

     int rowcnt = fParent->size();
     int colcnt = atts->size();

     for(int i=0;i<rowcnt;++i)
          _rowHeights.append(Settings->xttDefaultRowHeight());
     for(int i=0;i<colcnt;++i)
     {
          XTT_Column* newcol = new XTT_Column;
          newcol->setAttribute(atts->at(i));
          _columns.append(newcol);
     }
     delete atts;

     ReCreate();
     Update();
     RePosition();
     
     _amIvisible = true;
}
//----------------------------------------------------------------------------

// #brief Function that sets the new localization of the table
//
// #param __newpos - point that contains two coordinates of the localization
// #return no values return
void GStateTable::setLocalization(QPointF __newpos)
{
     _localization = __newpos;
     
     RePosition();
     _parentScene->update();
}
//----------------------------------------------------------------------------

// #brief Function that returns the point of table localization
//
// #return the reference to the point of table localization
QPointF GStateTable::localization(void)
{
     return _localization;
}
//----------------------------------------------------------------------------

// #brief Function that returns a pointer to the column object
//
// #param __colindex - index of the column
// #return pointer to the column, or NULL if the index os incorrect
XTT_Column* GStateTable::column(int __colindex)
{
     if((__colindex < 0) || (__colindex >= _columns.size()))
          return NULL;
          
     return _columns.at(__colindex);
}
//----------------------------------------------------------------------------

// #brief Function that returns the number of column
//
// #return the number of column
int GStateTable::columnCount(void)
{
     return _columns.size();
}
//----------------------------------------------------------------------------

// #brief Function that returns the number of rows
//
// #return the number of rows
int GStateTable::rowCount(void)
{
     return _rowHeights.size();
}
//----------------------------------------------------------------------------

// #brief Function that returns a width of the column
//
// #param __rowindex - index of the column
// #return a width of the column
int GStateTable::columnWidth(int __colindex)
{    
     if((__colindex < -1) || (__colindex >= _columns.size()))
          return 0;
     if((_amIoutput) && (__colindex == -1))
          return 0;
     if((!_amIoutput) && (__colindex == -1))
          return _labelWidth;
          
     return _columns.at(__colindex)->width();
}
//----------------------------------------------------------------------------

// #brief Function that sets the width of the column
//
// #param __colindex - index of the column
// #param __newwidth - new width of the col
// #return no values return
void GStateTable::setColumnWidth(int __colindex, int __newwidth)
{
     if((__colindex < -1) || (__colindex >= _columns.size()))
          return;
          
     if(__colindex == -1)
          _labelWidth = __newwidth;
     if(__colindex > -1)
          _columns.at(__colindex)->setWidth(__newwidth);
     
     RePosition();
     _parentScene->update();
}
//----------------------------------------------------------------------------

// #brief Function that returns the width of the table
//
// #return the width of the table
int GStateTable::width(void)
{
     int result = 0;
     for(int i=-1;i<_columns.size();++i)
          result += columnWidth(i);
     
     return result;
}
//----------------------------------------------------------------------------

// #brief Function that returns a height of the row
//
// #param __rowindex - index of the row
// #return a height of the row
int GStateTable::rowHeight(int __rowindex)
{
     if((__rowindex < -1) || (__rowindex >= _rowHeights.size()))
          return 0;
     
     if(__rowindex == -1)
          return headerHeight();
          
     return _rowHeights.at(__rowindex);
}
//----------------------------------------------------------------------------

// #brief Function that sets the height of the row
//
// #param __rowindex - index of the row
// #param __newheight - new height of the row
// #return no values return
void GStateTable::setRowHeight(int __rowindex, int __newheight)
{
     if((__rowindex < -1) || (__rowindex >= _rowHeights.size()))
          return;
     
     if(__rowindex == -1)
          _headerHeight = __newheight;
     if(__rowindex > -1)
          _rowHeights[__rowindex] = __newheight;
     
     RePosition();
     _parentScene->update();
}
//----------------------------------------------------------------------------

// #brief Function that returns the height of the table
//
// #return the height of the table
int GStateTable::height(void)
{
     int result = 0;
     for(int i=-1;i<_rowHeights.size();++i)
          result += rowHeight(i);
     
     return result;
}
//----------------------------------------------------------------------------

// #brief Function that returns the Y coordinate of the row __row
//
// #param __row - index of row, if -1 return Y coordinate of the table
// #return the Y coordinate of the row __row
qreal GStateTable::rowY(int __row)
{
     qreal result = 0.;
     for(int i=-1;i<__row;++i)
          result += rowHeight(i);
          
     return result;
}
//----------------------------------------------------------------------------

// #brief Function that returns the Y coordinate of the middle of the row __row
//
// #param __row - index of row, if -1 return Y coordinate of the header
// #return the Y coordinate of the middle the row __row
qreal GStateTable::rowMiddleY(int __row)
{
     return rowY(__row) + (rowHeight(__row) / 2.);
}
//----------------------------------------------------------------------------

// #brief Function that returns a shape of the graphical representation of the item in scene coordinates
//
// #return a shape of the graphical representation of the item in scene coordinates
QRectF GStateTable::shape(void)
{
     return fBase->sceneBoundingRect().normalized();
}
//----------------------------------------------------------------------------

// #brief Function that returns the height of the table header
//
// #return the height of the table header
int GStateTable::headerHeight(void)
{
     return _headerHeight;
}
//----------------------------------------------------------------------------

// #brief Function that returns the type of the table
//
// #return the type of the table
int GStateTable::tableType(void)
{
     return _tableType;
}
//----------------------------------------------------------------------------

// #brief Function that returns the title of the table
//
// #return the title of the table as string
QString GStateTable::tableTitle(void)
{
     if(fParent == NULL)
          return "";
          
      return fParent->baseName();
}
//----------------------------------------------------------------------------

// #brief Function that returns the poisition of the given state
//
// #param __state - the pointer to the state
// #return the offset from the first element of the container
int GStateTable::stateIndex(XTT_State* __state)
{
     if(fParent == NULL)
          return -1;
          
     return fParent->stateIndex(__state);
}
//----------------------------------------------------------------------------

// #brief Function that returns a pointer to the state related with the given row
//
// #param __row - index of the row
// #return pointer to the state in the given row
XTT_State* GStateTable::stateAt(int __row)
{
     if((__row < 0) || (__row >= _rowHeights.size()))
          return NULL;
     if(fParent == NULL)
          return NULL;
          
     return fParent->stateAt(__row);
}
//----------------------------------------------------------------------------

// #brief Function that returns a pointer to the set in the given cell
//
// #param __row - index of the row
// #param __col - index of the col
// #return pointer to the set in the given cell
hSet* GStateTable::setAt(int __row, int __col)
{
     if((__row < 0) || (__row >= _rowHeights.size()))
          return NULL;
     if((__col < 0) || (__col >= _columns.size()))
          return NULL;
     if(fParent == NULL)
          return NULL;
          
     XTT_State* state = fParent->stateAt(__row);
     if(state == NULL)
          return NULL;
          
     QList<XTT_Attribute*>* inputatts = state->inputAttributes();
     QList<XTT_Attribute*>* outputatts = state->outputAttributes();
     
     if((!_amIoutput) && (!inputatts->contains(_columns.at(__col)->attribute())))
          return NULL;
     if((_amIoutput) && (!outputatts->contains(_columns.at(__col)->attribute())))
          return NULL;
          
     if(_amIoutput)
          state->outputSetAt(_columns.at(__col)->attribute());
          
     delete inputatts;
     delete outputatts;
          
     return state->inputSetAt(_columns.at(__col)->attribute());
}
//----------------------------------------------------------------------------

// #brief Function that returns a label of the state at the given position
//
// #param __row - index of the row
// #return label of the state at the given position
QString GStateTable::labelAt(int __row)
{
     if((__row < 0) || (__row >= _rowHeights.size()))
          return "";
     if(fParent == NULL)
          return "";
          
     return fParent->labelAt(__row);
}
//----------------------------------------------------------------------------

// #brief Function that sets the label of the state at the given position
//
// #param __row - the row index
// #param __newlabel - new label of the state at the given position
// #return no values return
void GStateTable::setLabelAt(int __row, QString __newlabel)
{
     fParent->setLabelAt(__row, __newlabel);
     
     Update();
     RePosition();
     ((MyGraphicsScene*)_parentScene)->AdaptSceneSize();
     _parentScene->update();
}
//----------------------------------------------------------------------------

// #brief Function that returns the index of the colum that is related with the given attribute
//
// #param __att - pointer to the given attribute
// #return index of the column, if attribute is not exists returns -1
int GStateTable::indexOfAttribute(XTT_Attribute* __att)
{
     for(int i=0;i<_columns.size();++i)
          if(_columns.at(i)->attribute() == __att)
               return i;
     return -1;
}
//----------------------------------------------------------------------------

// #brief Function that returns the index of the colum that is related with the given attribute
//
// #param __id - id of the attribute
// #return index of the column, if attribute is not exists returns -1
int GStateTable::indexOfAttribute(QString __id)
{
     for(int i=0;i<_columns.size();++i)
          if(_columns.at(i)->attribute()->Id() == __id)
               return i;
     return -1;
}
//----------------------------------------------------------------------------

// #brief Function that makes the table visible
//
// #return true on success, false when table cannot be visible
bool GStateTable::makeVisible(void)
{
     bool canBeIvisible = true;
     if((tableType() == 0) && (!Settings->xttShowInitialTables()))
          canBeIvisible = false;
     
     // Checking if the table can be visible
     if((canBeIvisible) && (!_amIvisible))
     {
          fBase = new GStateRect(_parentScene, this); 
          fBase->setPos(localization().x(), localization().y());
          _parentScene->addItem(fBase);
          _parentScene->resetNewItem();
          _amIvisible = true;
          ReCreate();
          Update();
          RePosition();
     }
     
     if((!canBeIvisible) && (_amIvisible))
          removeAll();
          
     _amIvisible = canBeIvisible;
     return _amIvisible;
}
//----------------------------------------------------------------------------

// #brief Function that returns if the table is a output table
//
// #return if the table is a output table
bool GStateTable::isOutput(void)
{
     return _amIoutput;
}
//----------------------------------------------------------------------------

// #brief Function finds table on the stage and scroll the view to show it completely.
//
// #return No return value.
void GStateTable::ensureVisible(void)
{
    if(!_amIvisible)
        return;
     
	if(fBase->scene() == NULL)
		return;
          
    fBase->update();    // Odswiezamy scene takze tutaj poniewaz moze to tylko zaznaczenie komorki
          
    if(!Settings->xttEnabledAutocentering())
          return;

	QRectF itemRect = fBase->boundingRect();	
	int xMargin = 0;
	int yMargin = 0;
	float itemWidth = (xMargin*2.)+itemRect.width();
	float itemHeight = (yMargin*2.)+itemRect.height();
	
	for(int s=0;s<fBase->scene()->views().count();++s)
	{
		QRectF sRect = fBase->scene()->views().at(s)->sceneRect();
		if((itemWidth < sRect.width()) && (itemHeight < sRect.height()))
		{
			fBase->scene()->views().at(s)->ensureVisible(fBase, xMargin, yMargin);
			//QMessageBox::question(NULL, "HQEd", "itemWidth=" + QString::number(itemWidth) + ", itemHeight=" + QString::number(itemHeight) + "\nsceneWidth=" + QString::number(sRect.width()) + ", sceneHeight=" + QString::number(sRect.height()), QMessageBox::Ok);
		}
	}
	
	fBase->update();
}
//----------------------------------------------------------------------------

// #brief Function adds new text.
//
// #param _tt Type of the text that should be taken.
// #param _row Number of row from which text should be taken.
// #param _col Number of column from which text should be taken.
// #return No return value.
void GStateTable::AddText(QString __id)
{
     GStateText** tmp = new GStateText*[fTextCount+1];

     // Kopiowanie
     for(unsigned int i=0;i<fTextCount;i++)
          tmp[i] = fText[i];

     // Usuniecie starej tablicy wskaznikow
     if(fTextCount)
          delete [] fText;

     fText = tmp;

     fText[fTextCount] = new GStateText(__id, this);
     _parentScene->addItem(fText[fTextCount]);
     fTextCount++;
}
//----------------------------------------------------------------------------

// #brief Function removes last text.
//
// #return No return value.
void GStateTable::DeleteText(void)
{
     GStateText** tmp = new GStateText*[fTextCount-1];

     // Kopiowanie
     for(unsigned int i=0;i<fTextCount-1;i++)
          tmp[i] = fText[i];

     // Usuniecie starej tablicy wskaznikow
     if(fTextCount)
     {
          delete fText[fTextCount-1];
          delete [] fText;
     }

     fText = tmp;

     fTextCount--;
}
//----------------------------------------------------------------------------

// #brief returns the number of texts in each type. at(0) - the number of texsts that has type 0 itd
//
// #return the number of texts in each type
QList<int>* GStateTable::texts(void)
{
     QList<int>* result = new QList<int>;
     
     result->append(rowCount()*columnCount());     // 0 - cell contents
     result->append(1);                            // 1 - table title
     result->append(columnCount());                // 2 - normal column header
     if(!_amIoutput)
     {
          result->append(rowCount());              // 3 - row labels
          result->append(1);                       // 4 - header of row labels column
     }
     
     return result;
}
//----------------------------------------------------------------------------

// #brief Function that returns an index of the first text in the texts array from the given text section
//
// #param __sectionindex - index of the section
// #li 0 - cell contents
// #li 1 - table title
// #li 2 - normal column header
// #li 3 - row labels
// #li 4 - header of row labels column
// #return the index of the first text object from the given section
int GStateTable::textStartIndex(int __sectionindex)
{
     int result = 0;
     QList<int>* txts = texts();
     
     int lastsection = ((__sectionindex >= 0) && (__sectionindex < txts->size())) == true ? __sectionindex : txts->size();
     for(int i=0;i<lastsection;++i)
          result += txts->at(i);
          
     delete txts;
          
     return result;
}
//----------------------------------------------------------------------------

// #brief returns the list of the texts positions.
//
// #return list of the texts positions.
QList<qreal>* GStateTable::textsPositions(void)
{
     //QList<int>* t = texts();
     qreal  cx = localization().x(), 
            cy = localization().y();
     
     QList<qreal>* result = new QList<qreal>;
     
     // content cells
     for(int r=0;r<rowCount();++r)
     {
          cy += rowHeight(r-1);
          cx = localization().x();
          for(int c=0;c<columnCount();++c)
          {
               cx += columnWidth(c-1);
               result->append(cx);
               result->append(cy);
          }
     }
     
     // table title
     result->append(localization().x());
     result->append(localization().y() + height() + 1);
     
     // normal column header
     cx = localization().x();
     cy = localization().y();
     for(int c=0;c<columnCount();++c)
     {
          cx += columnWidth(c-1);
          result->append(cx);
          result->append(cy);
     }
     
     // The ouput table does not display labeled rows
     if(!_amIoutput)
     {
          // row labels
          cx = localization().x();
          cy = localization().y();
          for(int r=0;r<rowCount();++r)
          {
               cy += rowHeight(r-1);
               result->append(cx);
               result->append(cy);
          }
          
          // header of row labels column
          result->append(localization().x());
          result->append(localization().y());
     }
     
     return result;
}
//----------------------------------------------------------------------------

// #brief Calculates the quantity of needed texts objects
//
// #param quantity of needed texts objects
int GStateTable::textQuantity(void)
{
     int result = 0;
     
     QList<int>* t = texts();
     for(int i=0;i<t->size();++i)
          result += t->at(i);
     delete t;
     
     return result;
}
//----------------------------------------------------------------------------

// #brief Function creates list of components and set them.
//
// #return No return value.
void GStateTable::ReCreate(void)
{
     // Obliczanie ilsoc tekstow
     unsigned int text_quantity = (unsigned int)textQuantity();

     // Na teksty
     for(;text_quantity < fTextCount;)
          DeleteText();
     for(;text_quantity > fTextCount;)
          AddText("gst" + QString::number(_nextidindex++));
          
     if(fBase == NULL)
          fBase = new GStateRect(_parentScene, this); 
     //fBase->setPos(localization().x(), localization().y());
     _parentScene->addItem(fBase);
     _parentScene->resetNewItem();
}
//----------------------------------------------------------------------------

// #brief Function updates parameters.
//
// #return No return value.
void GStateTable::Update(void)
{
     // Ustawianie parametrow tekstow
     QList<int>* textsnums = texts();
     int type = 0;
     int count = 0;
     
     // Teksty kmorek
     for(int i=0;i<rowCount();i++)
     {
          XTT_State* crstate = fParent->stateAt(i);
          QList<XTT_Attribute*>* inputattributes = crstate->inputAttributes();
          QList<XTT_Attribute*>* outputattributes = crstate->outputAttributes();
          for(int j=0;j<columnCount();j++,count++)
          {
               bool attExists = _amIoutput == false ? inputattributes->contains(column(j)->attribute()) : outputattributes->contains(column(j)->attribute());
               fText[count]->setRow(i);
               fText[count]->setCol(j);
               if(attExists)
               {
                    fText[count]->setTextType(type);
                    fText[count]->setAttribute(column(j)->attribute());
                    if(!_amIoutput)
                         fText[count]->setSet(crstate->inputSetAt(column(j)->attribute()));
                    if(_amIoutput)
                         fText[count]->setSet(crstate->outputSetAt(column(j)->attribute()));
               }
               if(!attExists)
               {
                    fText[count]->setTextType(4);
                    fText[count]->setAttribute(NULL);
                    fText[count]->setText("---");
               }
          }
          delete inputattributes;
          delete outputattributes;
     }
     type++;        // == 1
     
     // Table title
     fText[count]->setTextType(type);
     fText[count]->setText(fParent->baseName());
     count++;
     type++;        // == 2
     
     // normal column header
     for(int i=0;i<textsnums->at(type);++i)
     {
          fText[count+i]->setCol(i);
          fText[count+i]->setTextType(type);
          fText[count+i]->setAttribute(column(i)->attribute());
     }
     count += textsnums->at(type);
     type++;        // == 3
     
     if(!_amIoutput) {     // If the output table - we finish here
          // row labels
          for(int i=0;i<textsnums->at(type);++i)
          {
               fText[count+i]->setRow(i);
               fText[count+i]->setCol(-1);
               fText[count+i]->setTextType(type);
               fText[count+i]->setText(fParent->labelAt(i));
          }
          count += textsnums->at(type);
          type++;        // == 4

          // header of row labels column
          for(int i=0;i<textsnums->at(type);++i)
          {
               fText[count+i]->setRow(-1);
               fText[count+i]->setCol(-1);
               fText[count+i]->setTextType(type);
               fText[count+i]->setText("Label");
          }
          count += textsnums->at(type);
          type++;        // == 5
     }
     
     delete textsnums;
}
//----------------------------------------------------------------------------

// #brief Function sets elements once again.
//
// #return No return value.
void GStateTable::RePosition(void)
{ 
     // Kwadrat bazowy
     fBase->lockPositioning(true);
	fBase->setPos(localization().x(), localization().y());
     fBase->update();
     fBase->lockPositioning(false);
     
     QList<qreal>* poss = textsPositions();
     for(unsigned int i=0;i<fTextCount;++i)
     {
          int index = i*2;
          fText[i]->setPos(poss->at(index), poss->at(index+1));
          fText[i]->update();
     }
     delete poss;
     
     if(fParent != NULL)
          fParent->localizationRefresh();

     // Uaktualnienie widoku sceny
	_parentScene->update();
}
//----------------------------------------------------------------------------

// #brief Function select the table.
//
// #return No return value.
void GStateTable::setTableSelection(bool _sel)
{
     if(fBase == NULL)
          return;
          
     fBase->setSelected(_sel);
	fBase->update();
}
//----------------------------------------------------------------------------

// #brief Allow for select header cell
//
// #param int _colIndex - index of column in which the header should be selected
// #param bool _selStat - selection status
// #return No return value
void GStateTable::selectHeader(int _colIndex, bool _selStat)
{
	if((_colIndex < 0) || (_colIndex >= columnCount()) || (_colIndex >= (int)fTextCount))
		return;
		
	fText[textStartIndex(2) + _colIndex]->selectHeaderCell(_selStat);
}
//----------------------------------------------------------------------------

// #brief Allow for select single row
//
// #param __rowIndex - index of the row that must be selected
// #return No return value
void GStateTable::selectRow(int __rowIndex, bool __selected)
{
     int colCount = columnCount();
     int offset = (colCount * __rowIndex) + textStartIndex(0);
     
     unselectAllElements();
     for(int i=0;i<colCount;++i)
     {
          fText[offset+i]->selectCell(__selected);
          fText[offset+i]->update();
     }
}
//----------------------------------------------------------------------------

// #brief Function that selects the state in the table if it exists
//
// #param  __state - pointer to the state to be selected
// #param  __selectionstatus - indicates the status of state selection
// #return No return value.
void GStateTable::selectState(XTT_State* __state, bool __selectionstatus)
{
     int si = stateIndex(__state);
     if(si > -1)
          selectRow(si, __selectionstatus);
}
//----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __state - pointer to the state 
// #param  __attr - pointer to the attribute in the given state
// #param  __selected - indicates the state of selection
// #return No return value.
void GStateTable::selectCell(XTT_State* __state, XTT_Attribute* __attr, bool __selected)
{
     if((__attr == NULL) && (__state == NULL))
          return;

     int rowIndex = stateIndex(__state);
     int colIndex = indexOfAttribute(__attr);
     if((rowIndex == -1) || (colIndex == -1))
          return;
     
     int colCount = columnCount();
     int offset = (colCount*rowIndex) + textStartIndex(0) + colIndex;
     
     unselectAllElements();
     fText[offset]->selectCell(__selected);
     fText[offset]->update();
}
//----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __state - pointer to the state
// #param  __attid - id of the attribute
// #param  __selected - indicates the state of selection
// #return No return value.
void GStateTable::selectCell(XTT_State* __state, QString __attid, bool __selected)
{
     int colIndex = indexOfAttribute(__attid);
     if(colIndex == -1)
          return;
          
     selectCell(__state, column(colIndex)->attribute(), __selected);
}
//----------------------------------------------------------------------------

// #brief Allow for clear selection of all the elements that table consist of
//
// #return No return value
void GStateTable::unselectAllElements(void)
{
     if(fTextCount)
     {
          for(unsigned int i=0;i<fTextCount;i++)
               fText[i]->selectCell(false);
     }

     if(fBase)
          fBase->setSelected(false);
}
//----------------------------------------------------------------------------

// #brief Function that removes all the items from the scene
//
// #return No return value
void GStateTable::removeAll(void)
{
     if(fBase)
     {
          _parentScene->removeItem(fBase);
          delete fBase;
     }
     fBase = NULL;

     if(fTextCount)
     {
          for(unsigned int i=0;i<fTextCount;i++)
          {
               _parentScene->removeItem(fText[i]);
               delete fText[i];
          }
          delete [] fText;
          fTextCount = 0;
     }
     
     // updating view
     _parentScene->update();
}
//----------------------------------------------------------------------------
     
