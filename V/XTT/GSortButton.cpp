/*
 *	   $Id: GSortButton.cpp,v 1.1.4.1.2.1 2011-09-01 19:32:10 hqeddoxy Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../C/Settings_ui.h"
#include "GSortButton.h"
//---------------------------------------------------------------------------

// #brief Contructor GNode class.
//
// #param _t_parent Pointer to the parent of the table.
// #param _r_parent Pointer to the parent of the row.
// #param _income Determines whether connection is income.
GSortButton::GSortButton(bool __up, QGraphicsItem* __parent) :
#if QT_VERSION >= 0x040600
     QGraphicsObject(__parent)
#else
     QGraphicsItem(__parent)
#endif
{
     _up = __up;
     _hovered = false;
     _edgelength = 6.0;
     
     setCursor(Qt::PointingHandCursor);
     setAcceptsHoverEvents(true);
}
//---------------------------------------------------------------------------

// #brief Function draws object on the screen.
//
// #param _p Pointer to the canvas.
// #param _sogi Pointer to the drawing style.
// #param widget Unused pointer.
// #return No return value.
void GSortButton::paint(QPainter* _p, const QStyleOptionGraphicsItem*, QWidget*)
{
     int shift = _up == true ? 3 : 0;
     QColor color = _hovered == true ? QColor(255, 0, 0) : Settings->xttTableBorderColor();
     
     // Rysowanie kwadratu
     _p->setPen(Qt::NoPen);
     _p->setBrush(color);
     
     double _halfedgelength = _edgelength/2.;
     QPointF points[6] = {
     
                    // Down button coordinates
                    QPointF(-_halfedgelength, 0.),
                    QPointF(_halfedgelength, 0.),
                    QPointF(0., _edgelength),
                         
                     // Up button coordinates
                    QPointF(-_halfedgelength, _edgelength),
                    QPointF(_halfedgelength, _edgelength),
                    QPointF(0., 0.)
               };
               
     _p->drawPolygon(points+shift, 3);
}
//---------------------------------------------------------------------------

// #brief Function retrieves rectangular in which object is located.
//
// #return Rectangular in which object is located.
QRectF GSortButton::boundingRect() const
{
	float x, y, dx, dy;
     double _halfedgelength = _edgelength/2.;
     
     x = -_halfedgelength;
     y = 0.;
     dx = _edgelength;
     dy = _edgelength;
	
     return QRectF(x, y, dx, dy);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse button is pressed on the item.
//
// #param _gsme Pointer to the event object.
// #return No return value.
void GSortButton::mousePressEvent(QGraphicsSceneMouseEvent* _gsme)
{
     emit onMousePress();
     QGraphicsItem::mousePressEvent(_gsme);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse cursor enters the element area.
//
// #param _gsme Pointer to the event object.
// #return No return value.
void GSortButton::hoverEnterEvent(QGraphicsSceneHoverEvent* _gsme)
{
     _hovered = true;
     update();
     QGraphicsItem::hoverEnterEvent(_gsme);
}
//---------------------------------------------------------------------------

// #brief Function is called when mouse cursor leaves the element area.
//
// #param _gsme Pointer to the event object.
// #return No return value.
void GSortButton::hoverLeaveEvent(QGraphicsSceneHoverEvent* _gsme)
{
     _hovered = false;
     update();
     QGraphicsItem::hoverLeaveEvent(_gsme);
}
//---------------------------------------------------------------------------

// #brief Function that returns the length of the button edge
//
// #param __newlength - a new length of the button edge
// #return no values return
void GSortButton::setEdgeLength(double __newlength)
{
     if(__newlength <= 0.)
          return;
     
     _edgelength = __newlength;
     update();
}
//---------------------------------------------------------------------------

// #brief Function that returns the length of the button edge
//
// #return the length of the button edge
double GSortButton::edgeLength(void)
{
     return _edgelength;
}
//---------------------------------------------------------------------------

