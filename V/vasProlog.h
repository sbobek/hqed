 /**
 * \file	vasProlog.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	30.04.2009 
 * \version	1.0
 * \brief	This file contains declarations of functions that maps the model to SWI Prolog language
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef VAS_PROLOG
#define VAS_PROLOG
// -----------------------------------------------------------------------------

#include <QIODevice>
// -----------------------------------------------------------------------------

#define VAS_HMR_MODEL_PARTS_XTT_TYPES 0x00000001       ///< XTT Types
#define VAS_HMR_MODEL_PARTS_XTT_ATTRS 0x00000002       ///< XTT Attrributes
#define VAS_HMR_MODEL_PARTS_XTT_SCHMS 0x00000004       ///< XTT Tables schemas
#define VAS_HMR_MODEL_PARTS_XTT_RULES 0x00000008       ///< XTT rules
#define VAS_HMR_MODEL_PARTS_XTT_STATS 0x00000010       ///< XTT States
#define VAS_HMR_MODEL_PARTS_XTT_CLBCS 0x00000020       ///< XTT Callbacks

#define VAS_SWI_PROLOG_SUCCES 0
#define VAS_SWI_PROLOG_UNKNOWN_DEVICE 1
#define VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE 2
#define VAS_SWI_PROLOG_ERROR 3
// -----------------------------------------------------------------------------

/// \brief Exports the given model to SWI Prolog language format
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __msg - result message which contains na error message when exit code indicates na error result
/// \param __parts - map of the required parts
/// \param __prefix - this argument contains string that is addes before generated code
/// \param __postfix - this argument contains string that is addes after generated code
/// \param __protocolversion - indicates if the model should be formated according to protocol format
/// \return exit code:
/// \li VAS_SWI_PROLOG_SUCCES - on succes
/// \li VAS_SWI_PROLOG_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE - when device is not writable
/// \li VAS_SWI_PROLOG_ERROR - when during the generation error occurs
int vas_out_SWI_Prolog(QIODevice* __device, QString& __msg, QMap<QString, QString> __parts, QString __prefix = "", QString __postfix = "", bool __protocolversion = false);

/// \brief Exports the given model to SWI Prolog language format
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __msg - reference to the variable where the result (info, errors) messages are stored
/// \param __parts - The parts of the file to generation, if 0 the all parts are used
/// \param __prefix - this argument contains string that is addes before generated code
/// \param __postfix - this argument contains string that is addes after generated code
/// \param __protocolversion - indicates if the model should be formated according to protocol format
/// \return exit code:
/// \li VAS_SWI_PROLOG_SUCCES - on succes
/// \li VAS_SWI_PROLOG_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE - when device is not writable
/// \li VAS_SWI_PROLOG_ERROR - when during the generation error occurs
int vas_out_SWI_Prolog(QIODevice* __device, QString& __msg, int __parts = 0, QString __prefix = "", QString __postfix = "", bool __protocolversion = false);
// -----------------------------------------------------------------------------

#endif
