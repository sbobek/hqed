 /**
 * \file	vasXML.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	30.04.2009 
 * \version	1.0
 * \brief	This file contains declarations of functions that maps the model to/from XML
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef VAS_XML
#define VAS_XML
// -----------------------------------------------------------------------------

#include <QIODevice>
#include <QGraphicsScene>
#include <QDomDocument>
// -----------------------------------------------------------------------------

#define VAS_XML_NOTHING_DONE -1
#define VAS_XML_SUCCES 0
#define VAS_XML_UNKNOWN_DEVICE 1
#define VAS_XML_DEVICE_NOT_WRITABLE 2
#define VAS_XML_DEVICE_NOT_READABLE 3
#define VAS_XML_FORMAT_ERROR 4
#define VAS_XML_FORMAT_NOT_SUPPORTED 5
#define VAS_XML_VERSION_NOT_SUPPORTED 6
#define VAS_XML_XTT_SYNTAX_ERROR 7

#define VAS_XML_MODEL_PARTS_XTT_TYPES 0x00000001       ///< XTT Types
#define VAS_XML_MODEL_PARTS_XTT_ATTRS 0x00000002       ///< XTT Attrributes
#define VAS_XML_MODEL_PARTS_XTT_TABLS 0x00000004       ///< XTT Tables and Links
#define VAS_XML_MODEL_PARTS_XTT_STATS 0x00000008       ///< XTT States
#define VAS_XML_MODEL_PARTS_XTT_CLBS  0x00000160       ///< XTT Callbacks
#define VAS_XML_MODEL_PARTS_ARD_PROPS 0x00000010       ///< ARD Prperities
#define VAS_XML_MODEL_PARTS_ARD_TPH   0x00000020       ///< ARD TPH
#define VAS_XML_MODEL_PARTS_ARD_DPNDS 0x00000040       ///< ARD Dependencies
#define VAS_XML_MODEL_PARTS_ARD_ATTRS 0x00000080       ///< ARD Attrributes - HML 1.0
// -----------------------------------------------------------------------------

/// \brief Exports the given model to XML format
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __formating - indicates of the output should be formated
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_HML_2_0(QIODevice* __device, bool __formating, int __parts);

/// \brief Exports the given model (or its parts) to XML format
///
/// \param __device - the output device (File, Socket, Process, ...)
/// \param __formating - indicates of the output should be formated
/// \param __parts - parts of the HML file.
/// \param __rmsg - result message which contains na error message when exit code indicates na error result
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_HML_2_0(QIODevice* __device, bool __formating, QMap<QString, QString> __parts, QString& __rmsg);

/// \brief Exports the given model to Drools 5.0 format.
/// \author Lukasz Lysik (llysik@gmail.com)
///
/// \param __deviceRuleFlow
/// \param __deviceDecTab
/// \param __deviceWorkspace
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_XML_DEVICE_NOT_WRITABLE - when device is not writable
int vas_out_Drools_5_0(QIODevice* __deviceRuleFlow, QIODevice* __deviceDecTab, QIODevice* __deviceWorkspace);

/// \brief Imports model from the XML format 
///
/// \param __device - the input device (File, Socket, Process, ...)
/// \param __msg - the output argument, where the XML format errors are stored.
/// \param __format - the output attribute that conatains the inforamtion about opened file format.
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
/// \return exit code:
/// \li VAS_XML_NOTHING_DONE - when any operations executed
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_UNKNOWN_DEVICE - when device is not specified
/// \li VAS_XML_DEVICE_NOT_READABLE - when device is not readable
/// \li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
/// \li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
/// \li VAS_XML_VERSION_NOT_SUPPORTED - when XML version is not suported
/// \li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_XML(QIODevice* __device, QString& __msg, QString& __format, int __parts = 0);

/// \brief Imports model from the ARDML 1.0 format 
///
/// \param __root - reference to the root element
/// \param __msg - the output argument, where the XML format errors are stored.
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
/// \li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
/// \li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_HML_1_0(QDomElement& __root, QString& __msg, int __parts);

/// \brief Imports model from the ARDML 1.0 format 
///
/// \param __root - reference to the root element
/// \param __msg - the output argument, where the XML format errors are stored.
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
/// \li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
/// \li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_HML_2_0(QDomElement& __root, QString& __msg, int __parts);

/// \brief Imports model from the XTTML 2.0 format 
///
/// \param __root - reference to the root element
/// \param __msg - the output argument, where the XML format errors are stored.
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
/// \li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
/// \li VAS_XML_XTT_SYNTAX_ERROR - when XTT error
int vas_in_XTTML_2_0(QDomElement& __root, QString& __msg, int __parts);

/// \brief Imports model from the ARDML 1.0 format 
///
/// \param __root - reference to the root element
/// \param __msg - the output argument, where the XML format errors are stored.
/// \param __parts - parts of the HML file. This value is a combination of VAS_XML_MODEL_PARTS_*. The default value is zero - the function makes a decision which parts should be readed. Otherwise the required parts are read.
/// \return exit code:
/// \li VAS_XML_SUCCES - on succes
/// \li VAS_XML_FORMAT_ERROR - when XML format is not correct. The error message is stored int the __msg argument.
/// \li VAS_XML_FORMAT_NOT_SUPPORTED - when XML format is not suported
/// \li VAS_XML_XTT_SYNTAX_ERROR - when ARD error
int vas_in_ARDML_1_0(QDomElement& __root, QString& __msg, int __parts);
// -----------------------------------------------------------------------------

#endif
