/*
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project, 
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


========================================================
======================= FOR WHAT =======================
========================================================

     This application is the one of the first attempts at practical
realization "HeKatE" project. This project is a new approach for Generalized
Rule-based Programing (GREP), which uses ARD and XTT approaches for modeling
applications logical layer.

     ARD is the highest level of modeling, where variables and theirs domains
and also relations betwen them can be defined. In ARD model we started with
the highest and the most general modeling level, where we define wthat is the
input and what we want to know on the output.  On the following, less general
ARD modeling levels, model becomes more and more detailed.  When model is
detailed enough, XTT diagram based on ARD is generated.

     XTT modeling consist in creating eXtendet Table Trees. As ARD modeling
defines logical relations betwen attributes, the rests of programming process
must be realized on this level.  When XTT model is done, the PROLOG code is
generated. This is the modeling lowest layer. Both, ARD and XTT models are
translated to PROLOG, can be executed as application.

What is the advantage of this project?

Because this application was written in c++ language with the help Qt
library, the source code can be compiled on the each platform, and operating
system. The prolog code can be generated and runed also on the each platform.
These two issues provide programming universal laguage independent from the
hardware and software.



========================================================
========== BUILDING AND INSTALING APPLICATION ==========
========================================================

If you have the code from cvs, or in your hqed directory exists file "hqed.pro",
you can use this script.

WARNING:
You must install Qt 4.3 library for correct compilation.

- First type "qmake" in order to create a makefile.
- Then you can complie the sorce code - type "make".
