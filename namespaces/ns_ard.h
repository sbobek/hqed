 /**
 * \file	ns_ard.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	30.04.2009 
 * \version	1.0
 * \brief	This file contains hqed namespace declarations and functions belong to this namespace
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef NS_ARDH
#define NS_ARDH
// -----------------------------------------------------------------------------

#include <QString>
#include <QStringList>
// -----------------------------------------------------------------------------

namespace ard
{
     /// \brief Function that removes all the XTT items
     ///
     /// \return No return value.
     void clear(void);

     /// \brief Function starts a new ARD project
     ///
     /// \return No return value.
     void startNew(void);

     /// \brief returns the id of default ARD attribute type
     ///
     /// \return returns the id of default ARD attribute type
     QString defaultArdAttributeTypeId(void);

     /// \brief returns the name of default ARD attribute type
     ///
     /// \return returns the name of default ARD attribute type
     QString defaultArdAttributeTypeName(void);

     /// \brief Generates an XML based description of default ARD attribute type
     ///
     /// \return an XML based description of default ARD attribute type
     QString xmlDefaultArdAttributeType(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No return value.
     void xmlDefaultArdAttributeType(QXmlStreamWriter* _xmldoc);
     #endif
}
// -----------------------------------------------------------------------------

#endif
