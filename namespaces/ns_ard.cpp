/*
 *	   $Id: ns_ard.cpp,v 1.6.4.2 2011-06-06 15:33:34 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QDate>

#include "../M/hType.h"

#include "../M/ARD/ARD_Level.h"
#include "../M/ARD/ARD_LevelList.h"
#include "../M/ARD/ARD_PropertyList.h"
#include "../M/ARD/ARD_AttributeList.h"
#include "../M/ARD/ARD_DependencyList.h"

#include "ns_ard.h"
// -----------------------------------------------------------------------------

// #brief Function that removes all the XTT items
//
// #return No return value.
void ard::clear(void)
{
     ardLevelList->flush();
     ardDependencyList->flush();
     ardPropertyList->flush();
     ardAttributeList->flush();
}
// -----------------------------------------------------------------------------

// #Function starts a new ARD project
//
// #return No return value.
void ard::startNew(void)
{
     clear();
     ardLevelList->add()->addProperty(ardPropertyList->add());
     ardPropertyList->refreshDisplay();
     ardLevelList->refreshDisplay();
}
// -----------------------------------------------------------------------------

// #brief returns the id of default ARD attribute type
//
// #return returns the id of default ARD attribute type
QString ard::defaultArdAttributeTypeId(void)
{
     return "tpe_ard_default";
}
// -----------------------------------------------------------------------------

// #brief returns the name of default ARD attribute type
//
// #return returns the name of default ARD attribute type
QString ard::defaultArdAttributeTypeName(void)
{
     return "ard_default_type";
}
// -----------------------------------------------------------------------------

// #brief Generates an XML based description of default ARD attribute type
//
// #return an XML based description of default ARD attribute type
QString ard::xmlDefaultArdAttributeType(void)
{
     hPrimitiveTypesInfo ptnfo;
     QString basetypename = ptnfo.ptn.at(ptnfo.iopti(SYMBOLIC_BASED));
     QString res;

     res += "<type ";
     res += "id=\"" + defaultArdAttributeTypeId() + "\" ";
     res += "name=\"" + defaultArdAttributeTypeName() + "\" ";
     res += "base=\"" + basetypename + "\">";
     res += "<domain>";
          res += "<value ";
          res += "is=\"none\"";
          res += "/>";
     res += "</domain>";
     res += "</type>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
/// \brief Function saves all data in hml 2.0 format.
///
/// \param _xmldoc Pointer to document.
/// \return No retur value.
void ard::xmlDefaultArdAttributeType(QXmlStreamWriter* _xmldoc)
{
     hPrimitiveTypesInfo ptnfo;
     QString basetypename = ptnfo.ptn.at(ptnfo.iopti(SYMBOLIC_BASED));

     _xmldoc->writeStartElement("type");
     _xmldoc->writeAttribute("id", defaultArdAttributeTypeId());
     _xmldoc->writeAttribute("name", defaultArdAttributeTypeName());
     _xmldoc->writeAttribute("base", basetypename);
          _xmldoc->writeStartElement("domain");
               _xmldoc->writeStartElement("value");
               _xmldoc->writeAttribute("is", "none");
               _xmldoc->writeEndElement();
          _xmldoc->writeEndElement();
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------
