/*
 *	  $Id: ns_hqed.cpp,v 1.24.4.2.2.6 2011-09-29 09:18:04 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QStack>

#include "../V/vasXML.h"

#include "../C/XTT/widgets/ErrorsListWidget.h"
#include "../C/XTT/widgets/ExecuteMessagesListWidget.h"
#include "../C/XTT/widgets/MyGraphicsView.h"
#include "../C/XTT/widgets/MyGraphicsScene.h"

#include "../M/ARD/ARD_Level.h"
#include "../M/ARD/ARD_LevelList.h"
#include "../M/ARD/ARD_Dependency.h"
#include "../M/ARD/ARD_DependencyList.h"
#include "../M/ARD/ARD_AttributeList.h"
#include "../M/ARD/ARD_Attribute.h"
#include "../M/ARD/ARD_Property.h"
#include "../M/ARD/ARD_PropertyList.h"
#include "../M/ARD/ARD_DependencyList.h"

#include "../M/hSet.h"
#include "../M/hType.h"
#include "../M/hModel.h"
#include "../M/hValue.h"
#include "../M/hSetItem.h"
#include "../M/hTypeList.h"
#include "../M/hModelList.h"
#include "../M/hMultipleValue.h"
#include "../M/hEcmaScript.h"

#include "../M/XTT/XTT_ConnectionsList.h"
#include "../M/XTT/XTT_Connections.h"
#include "../M/XTT/XTT_Table.h"
#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_AttributeGroups.h"
#include "../M/XTT/XTT_AttributeList.h"
#include "../M/XTT/XTT_Attribute.h"
#include "../M/XTT/XTT_Cell.h"
#include "../M/XTT/XTT_Row.h"
#include "../M/XTT/XTT_States.h"

#include "../C/MainWin_ui.h"
#include "../C/Settings_ui.h"
#include "ns_xtt.h"
#include "ns_hqed.h"
// -----------------------------------------------------------------------------
// Clipboard variables
QStack<hSet*>* __clipboardSet;
QStack<hType*>* __clipboardType;
QStack<hDomain*>* __clipboardDomain;
QStack<XTT_Attribute*>* __attributeDomain;
QStack<hMultipleValue*>* __clipboardMultipleValue;

hModelList* __modelList;
// -----------------------------------------------------------------------------

// #brief creates the models conatiner
//
// #return no values return
void hqed::createModelList(void)
{
    __modelList = new hModelList;
}
// -----------------------------------------------------------------------------

// #brief Returns the pointer to the current model list
//
// #return the pointer to the current model list
hModelList* hqed::modelList(void)
{
    return __modelList;
}
// -----------------------------------------------------------------------------

// #brief Search for phrases in the text
//
// #param QStringList* _phrases - list of searched phrases
// #param QString _text - text for phrases search
// #param Qt::CaseSensitivity _cs - specify search CaseSensitivity
// #param int _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
// #return It return a list of tables which satisfied the search criteria
bool hqed::searchForPhrases(QStringList* _phrases, QString _text, Qt::CaseSensitivity _cs, int _phrasesPolicy)
{
    bool res = (_phrasesPolicy==CONJUNCTION_POLICY)?true:false;

    for(int p=0;p<_phrases->size();++p)
    {
        bool ex = _text.contains(_phrases->at(p), _cs);

        if((_phrasesPolicy == CONJUNCTION_POLICY) && (!ex))
            return false;
        if((_phrasesPolicy == DISJUNCTION_POLICY) && (ex))
            return true;
    }

    return res;
}
// -----------------------------------------------------------------------------

// #brief Sets up the local settings for application
//
// #return No returns value
void hqed::setLocalSettings(void)
{
    // Zastosowanie ustawien lokalnych
    QLocale::setDefault(QLocale::c());
}
// -----------------------------------------------------------------------------

// #brief returns the local settings
//
// #return The local settings
QLocale hqed::localSettings(void)
{
    return QLocale::c();
}
// -----------------------------------------------------------------------------

// #brief Functions that set the widget on screen center
//
// #param QWidget* __widget - the pointer to the widget that should be centered on the screen
// #return No returns value
void hqed::centerWindow(QWidget* __widget)
{
    QRect wg = __widget->geometry();
    QRect dg = QApplication::desktop()->screenGeometry(QApplication::desktop()->primaryScreen());

    int x = (dg.width() >> 1) - (wg.width() >> 1);
    int y = (dg.height() >> 1) - (wg.height() >> 1);
    __widget->move(x, y);
}
// -----------------------------------------------------------------------------

// #brief Generates XTT schema
//
// #return return 0- if ok, 1- if o the level are non atomic properties
int hqed::generateXTT(void)
{
    //AttributeGroups->Flush();
    MainWin->xtt_scene->setSelectedConnectionID("");
    MainWin->xtt_scene->setSelectedTableID("");
    MainWin->xtt_runModelMessagesDockWindowMessagesList->deleteAll();
    MainWin->xtt_errorsList->deleteAll();
    MainWin->setXTT_Changed(true);

    // clear XTT model
    ConnectionsList->Flush();
    TableList->Flush();

    // Searching for the more detailed ARD level
    if(ardLevelList->size() == 0)
        return XTT_GEN_ERROR_NO_LEVELS_DEFINED;

    ARD_Level* lowestLevel = ardLevelList->at(ardLevelList->size()-1);

    // Checking the level finalization
    for(int p=0;p<lowestLevel->properties()->size();++p)
        if(lowestLevel->properties()->at(p)->canBeTransformated())
            return XTT_GEN_ERROR_NO_FINALIZED_LEVEL;

    QList<XTT_Attribute*>* inputAttrList = new QList<XTT_Attribute*>;		// List of input attributes
    QList<XTT_Attribute*>* outputAttrList = new QList<XTT_Attribute*>;		// List of output attributes

    // Creating XTT attributes
    int destinationAttributeGroup = 0;
    for(int p=0;p<lowestLevel->properties()->size();++p)
    {
        ARD_Attribute* curARDattr = lowestLevel->properties()->at(p)->at(0);
        if(curARDattr->type() == ARD_ATT_CONCEPTUAL)
            continue;

        XTT_Attribute* xttEq = curARDattr->xttEquivalent();
        XTT_Attribute* newXTTattr = NULL;

        // If the attribute exists
        if((xttEq != NULL) && (AttributeGroups->FindAttribute(xttEq->Id()) != NULL))
        {
            if(xttEq->Class() == XTT_ATT_CLASS_RO)       // If attrbiute is defined as input then we adds it to the list - it is needed for initailize table creating
                inputAttrList->append(xttEq);
            if(xttEq->Class() == XTT_ATT_CLASS_WO)       // If attrbiute is defined as output then we adds it to the list
                outputAttrList->append(xttEq);
            newXTTattr = xttEq;
            //continue;
        }
        else if((xttEq == NULL) || (AttributeGroups->FindAttribute(xttEq->Id()) == NULL))
        {
            // Creating XTTattribute
            newXTTattr = AttributeGroups->Group(destinationAttributeGroup)->Add();
            curARDattr->setXTTequivalent(newXTTattr);

            // Setting-up properties of new attributes
            newXTTattr->setName(curARDattr->name());
            newXTTattr->setAcronym(AttributeGroups->createAcronym(curARDattr->name(), destinationAttributeGroup, destinationAttributeGroup, AttributeGroups->Group(destinationAttributeGroup)->Count()-1));
            newXTTattr->setDescription(curARDattr->description());
            newXTTattr->setType(typeList->at(hpTypesInfo.iopti(INTEGER_BASED)));								// Setting integer type
            //newXTTattr->setDefaultValue(XTT_Attribute::_not_definded_operator_);
            newXTTattr->setValue(new hMultipleValue(NOT_DEFINED, newXTTattr->Type()));
            newXTTattr->setMultiplicity(false);
        }

        // Creating list of input attributes
        // Searching all the dependencies that incoming to the current property
        QList<ARD_Dependency*>* dl1 = lowestLevel->indexOfinout(NULL, lowestLevel->properties()->at(p));

        // If there is no dependencies then the property contains the input attribute
        if(dl1->empty())
        {
            inputAttrList->append(lowestLevel->properties()->at(p)->at(0)->xttEquivalent());
            newXTTattr->setClass(XTT_ATT_CLASS_RO);
        }
        delete dl1;

        // Creating list of output attributes
        // Searching all the dependencies that incoming to the current property
        QList<ARD_Dependency*>* dl2 = lowestLevel->indexOfinout(lowestLevel->properties()->at(p), NULL);

        // If there is no dependencies then the property contains the input attribute
        if(dl2->empty())
        {
            outputAttrList->append(lowestLevel->properties()->at(p)->at(0)->xttEquivalent());
            newXTTattr->setClass(XTT_ATT_CLASS_WO);
        }
        delete dl2;
    }

    AttributeGroups->Group(destinationAttributeGroup)->sort();

    // The list of dependencies on lowest level
    QList<ARD_Dependency*>* tmp_dep_list = new QList<ARD_Dependency*>;
    for(int d=0;d<lowestLevel->dependencies()->size();++d)
        tmp_dep_list->append(lowestLevel->dependencies()->at(d));

    // Generating XTT
    for(int d=0;d<lowestLevel->dependencies()->size();)
    {
        ARD_Property* F = lowestLevel->dependencies()->at(d)->independent();
        ARD_Property* T = lowestLevel->dependencies()->at(d)->dependent();
        lowestLevel->dependencies()->removeAt(d);

        if(F == T)
            continue;

        // Creating Ft i Tf
        QList<ARD_Dependency*>* dFt = lowestLevel->indexOfinout(NULL, T);
        lowestLevel->removeDependencies(dFt);

        QList<ARD_Dependency*>* dTf = lowestLevel->indexOfDependentOfOnly(F);
        lowestLevel->removeDependencies(dTf);

        QList<ARD_Property*>* Ft = new QList<ARD_Property*>;
        QList<ARD_Property*>* Tf = new QList<ARD_Property*>;
        for(int i=0;i<dFt->size();++i)
            Ft->append(dFt->at(i)->independent());
        for(int i=0;i<dTf->size();++i)
            Tf->append(dTf->at(i)->dependent());

        // The list of attributes in new rule
        QList<XTT_Attribute*>* conditionalPart = new QList<XTT_Attribute*>;
        QList<XTT_Attribute*>* actionPart = new QList<XTT_Attribute*>;

        // First case
        if((Ft->isEmpty()) && (Tf->isEmpty()))
        {
            conditionalPart->append(F->at(0)->xttEquivalent());
            actionPart->append(T->at(0)->xttEquivalent());
            TableList->addTableTemplate(conditionalPart, actionPart);
        }

        // Second case
        if((!Ft->isEmpty()) && (Tf->isEmpty()))
        {
            conditionalPart->append(F->at(0)->xttEquivalent());

            for(int i=0;i<Ft->size();++i)
                conditionalPart->append(Ft->at(i)->at(0)->xttEquivalent());

            actionPart->append(T->at(0)->xttEquivalent());
            TableList->addTableTemplate(conditionalPart, actionPart);
        }

        // Third case
        if((Ft->isEmpty()) && (!Tf->isEmpty()))
        {
            conditionalPart->append(F->at(0)->xttEquivalent());
            actionPart->append(T->at(0)->xttEquivalent());
            for(int i=0;i<Tf->size();++i)
                actionPart->append(Tf->at(i)->at(0)->xttEquivalent());
            TableList->addTableTemplate(conditionalPart, actionPart);
        }

        // Fourth case
        if((!Ft->isEmpty()) && (!Tf->isEmpty()))
        {
            conditionalPart->append(F->at(0)->xttEquivalent());
            for(int i=0;i<Ft->size();++i)
                conditionalPart->append(Ft->at(i)->at(0)->xttEquivalent());
            actionPart->append(T->at(0)->xttEquivalent());
            TableList->addTableTemplate(conditionalPart, actionPart);

            conditionalPart->clear();
            actionPart->clear();
            conditionalPart->append(F->at(0)->xttEquivalent());
            for(int i=0;i<Tf->size();++i)
                actionPart->append(Tf->at(i)->at(0)->xttEquivalent());
            TableList->addTableTemplate(conditionalPart, actionPart);
        }

        // Free memory
        delete conditionalPart;
        delete actionPart;
        delete dFt;
        delete dTf;
        delete Ft;
        delete Tf;
    }

    // Recovering dependencies on lowest level
    for(int d=0;d<tmp_dep_list->size();++d)
        lowestLevel->dependencies()->append(tmp_dep_list->at(d));

    // Defining variables
    bool xttOptimalization = Settings->optimizeXTT();
    bool createInitialTables = Settings->createInitialTables();
    bool allowDuplicateConnections = Settings->allowDuplicateConnection();

    // ------ OPTIMIZE XTT ------
    if(xttOptimalization)
        TableList->optimizeXTT();

    // Creating connections beetwen tables
    for(int d=0;d<lowestLevel->dependencies()->size();++d)
    {
        // Searchnig for attributes related with current dependencie
        ARD_Property* p_indep = lowestLevel->dependencies()->at(d)->independent();
        ARD_Property* p_dep = lowestLevel->dependencies()->at(d)->dependent();
        ARD_Attribute* aa_indep = p_indep->at(0);
        ARD_Attribute* aa_dep = p_dep->at(0);
        XTT_Attribute* xa_indep = aa_indep->xttEquivalent();
        XTT_Attribute* xa_dep = aa_dep->xttEquivalent();

        /*
   The schema:
   ... | xa_indep, ... -> xa_indep, ... | xa_dep, ...
                       ^ this connection must be created
  */

        // The result of searching the tables
        QList<XTT_Table*>* tl_1;
        QList<XTT_Table*>* tl_2;
        QList<XTT_Table*>* tl_3;

        // Searchnig for table that has xa_indep in decision ctx
        bool fs_ctss[4] = {false, false, false, true};
        tl_1 = TableList->findTables(fs_ctss, xa_indep);

        // That is mean that the xa_indep is the input attribute. If the stettings do not allow to create initial table, then continue the main loop
        if((tl_1->empty()) && (!createInitialTables))
        {
            delete tl_1;
            continue;
        }

        // Searchnig for table that has xa_indep in cond ctx and xa_dep in decision ctx
        bool ss_ctss[4] = {true, false, false, false};
        bool ts_ctss[4] = {false, false, false, true};
        tl_2 = TableList->findTables(ss_ctss, xa_indep);
        tl_3 = TableList->findTables(tl_2, ts_ctss, xa_dep);

        // Creating connections
        for(int t1=0;t1<tl_1->size();++t1)
        {
            for(int t3=0;t3<tl_3->size();++t3)
            {
                if((tl_1->at(t1)->RowCount() == 0) || (!ConnectionsList->ExistsOutConnections(tl_1->at(t1)->TableId(), tl_1->at(t1)->Row(tl_1->at(t1)->RowCount()-1)->RowId())))
                    tl_1->at(t1)->AddRow();
                QString stid = tl_1->at(t1)->TableId();
                QString srid = tl_1->at(t1)->Row(tl_1->at(t1)->RowCount()-1)->RowId();
                QString dtid = tl_3->at(t3)->TableId();
                //if(tl_3->at(t3)->RowCount() == 0)
                //     tl_3->at(t3)->AddRow();
                QString drid = ""; //tl_3->at(t3)->Row(0)->RowId();

                // Searching for the connections to check if exist with the same parameters
                QList<XTT_Connections*>* cl = ConnectionsList->listOfConnections(stid, srid, dtid, drid);

                // The conncetion is created when the connection is not exists or the settings allow to create the duplicate
                if(((!cl->empty()) && (!allowDuplicateConnections)) || (cl->empty()))
                {
                    XTT_Connections* newConn = ConnectionsList->Add();	// Creating the new connection

                    // Setting up the connection parameters
                    newConn->setSrcTable(stid);
                    newConn->setSrcRow(srid);
                    newConn->setDesTable(dtid);
                    newConn->setDesRow(drid);

                    // Refresh the graphical representation
                    newConn->Reposition();
                    newConn->Repaint();
                }
            }
        }

        // Free alocated memory
        delete tl_1;
        delete tl_2;
        delete tl_3;
    }

    // Checking for halt tables
    for(unsigned int t=0;t<TableList->Count();++t)
    {
        QList<XTT_Connections*>* cl = TableList->Table(t)->outConnectionsList();
        if(cl->empty())
            TableList->Table(t)->setTableType(2);
        delete cl;
    }

    // Creating initializaed tables
    if(createInitialTables)
    {
        // Searching the tables that contain at least one of the input atrribute in their conditional context
        for(unsigned int t=0;t<TableList->Count();++t)
        {
            XTT_Table* currTab = TableList->Table(t);
            QList<XTT_Attribute*>* ta = currTab->ContextAtributes(1);				// The list of the attributes in the conditional context in current table
            QList<XTT_Attribute*> li = listIntersection(*ta, *inputAttrList);		// The intersection of the list

            // If the list is not empty, the initialize table is created
            if(!li.empty())
            {
                QList<XTT_Attribute*>* conAttr = new QList<XTT_Attribute*>;
                QList<XTT_Attribute*>* decAttr = &li;
                XTT_Table* initTab = TableList->createTable(conAttr, decAttr);
                initTab->setTableType(0);						     // Set as fact table
                initTab->setTitle(Settings->xttInitialTableDefaultName());	// Set the table title
                TableList->Add(initTab);							     // Adding new table to the TableList

                // Creating the connection
                initTab->AddRow();
                if(currTab->RowCount() == 0)
                    currTab->AddRow();
                QString stid = initTab->TableId();
                QString srid = initTab->Row(initTab->RowCount()-1)->RowId();
                QString dtid = currTab->TableId();
                QString drid = currTab->Row(0)->RowId();

                XTT_Connections* newConn = ConnectionsList->Add();	// Creating the new connection

                // Setting up the connection parameters
                newConn->setSrcTable(stid);
                newConn->setSrcRow(srid);
                newConn->setDesTable(dtid);
                newConn->setDesRow(drid);

                // Refresh the graphical representation
                newConn->Reposition();
                newConn->Repaint();
            }
            delete ta;
        }
    }

    // Free alocated memory
    delete inputAttrList;
    delete outputAttrList;

    MainWin->xtt_scene->resetNewItem();
    xtt::fullVerification();
    xtt::logicalFullVerification();
    TableList->RefreshAll();					// Redraw all tables

    return XTT_GEN_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Get value from xttml paramter line.
//
// #param line Parameters line.
// #param val_name Name of the value.
// #return Searched value.
QString hqed::XML_ReadValue(QString line, QString val_name)
{
    QString res = "#NAN#";
    val_name = " " + val_name;

    if(!line.contains(val_name, Qt::CaseInsensitive))
        return res;

    int val_index = line.indexOf(val_name, Qt::CaseInsensitive);
    int f_index = line.indexOf("\"", val_index, Qt::CaseInsensitive);
    int s_index = line.indexOf("\"", f_index+1, Qt::CaseInsensitive);

    if((f_index == -1) || (s_index == -1))
        return res;

    res = line.mid(f_index+1, s_index-f_index-1);

    return res;
}
// -----------------------------------------------------------------------------

// #brief Remove space at the begin and end of the string.
//
// #param str Input string.
// #return Changed string.
QString hqed::RemoveSpaceAtEnds(QString str)
{
    while((str.length() > 0) && (str[0] == ' '))
    {
        str = str.remove(0, 1);
    }

    while((str.length() > 0) && (str[str.length()-1] == ' '))
    {
        str = str.remove(str.length()-1, 1);
    }

    return str;
}
// -----------------------------------------------------------------------------

// #brief mcwp is an abbreviation of Make Compatible With PROLOG. The function converts given string into string that is compatible with PROLOG.
//
// #param __string - string to conversion
// #return string that is compatible with PROLOG
QString hqed::mcwp(QString __string)
{
    if(__string.isEmpty())
        return __string;

    bool addApostrophe = false;
    QString apostrophe = "";

    addApostrophe = addApostrophe || __string.contains(" ");
    addApostrophe = addApostrophe || __string.contains("{");
    addApostrophe = addApostrophe || __string.contains("}");
    addApostrophe = addApostrophe || __string.contains(".");
    addApostrophe = addApostrophe || __string.contains(" ");
    addApostrophe = addApostrophe || __string.data()[0].isUpper();
    addApostrophe = addApostrophe || __string.data()[0].isDigit();
    addApostrophe = addApostrophe || __string.data()[0].isSymbol();
    addApostrophe = addApostrophe || (__string.at(0) == '_');

    bool isNumber;
    __string.toDouble(&isNumber);
    addApostrophe = addApostrophe && (!isNumber);

    if(addApostrophe)
        apostrophe = "\'";

    QString result = apostrophe + __string + apostrophe;
    return result;
}
// -----------------------------------------------------------------------------

// #brief returns the list of characters that cannot be used as starting character in the ARD and XTT attribute name
//
// #return the list of characters that cannot be used as starting character in the ARD and XTT attribute name
QString hqed::attributeNameNotAllowedStartingCharactersList(void)
{
     return "0123456789!@#$%^&*()-_+={}[];\':\"\\/.,<>";
}
// -----------------------------------------------------------------------------

// #brief checks the name of the ARD or XTT attrbiute
//
// #param __name - name of the attribute to be checked
// #return exit code
// #li 0 - if the given name is correct
// #li 1 - if the name is empty
// #li 2 - if the name starts with incorrect character
int hqed::checkAttributeName(QString __name)
{
     __name = __name.trimmed();
     if(__name.isEmpty())
          return 1;

     QString notallowedstartingcharacters = attributeNameNotAllowedStartingCharactersList();
     for(int i=0;i<notallowedstartingcharacters.size();++i)
          if(__name.startsWith(notallowedstartingcharacters.at(i)))
               return 2;
     return 0;
}
// -----------------------------------------------------------------------------

// #brief returns the version of the hqed
//
// #return the version of the hqed as string
QString hqed::hqedVersion(void)
{
    return "M6_10_1";
}
// -----------------------------------------------------------------------------

// #brief returns the full name of the editor
//
// #return the full name of the editor
QString hqed::hqedName(void)
{
    return "HeKatE Qt Editor ver. " + hqedVersion();
}
// -----------------------------------------------------------------------------

// #brief Returns the error indicator, that is necessary to message parse
//
// #param __messageIndicator - denotes if the new or end message indicator should be returned
// #return the error indicator as string
QString hqed::errorIndicator(int __messageIndicator)
{
    QString result = "";
    if(__messageIndicator == NEW_MESSAGE_INDICATOR)
        result = "__NEW_MESSAGE__";
    if(__messageIndicator == END_MESSAGE_INDICATOR)
        result = "__END_MESSAGE__";

    return result;
}
// -----------------------------------------------------------------------------

// #brief Returns the separator that is used to separate the parts of the messages
//
// #return the message separator as string
QString hqed::messageSeparator(void)
{
    return "|";
}
// -----------------------------------------------------------------------------

// #brief Returns the name of the logic server HeaRT
//
// #returnthe name of the logic server HeaRT as string
QString hqed::logicServerName(void)
{
    return "HeaRT";
}
// -----------------------------------------------------------------------------

// #brief Concatenates two strings with given separator.
//
// #param __str1 - first string
// #param __sep - strings separator
// #param __str2 - second separator
// #param __allowDuplSep - indicates if the the separator can be double: when the __str1 ends with __sep 
// #return the concatenated string
QString hqed::concatenate(QString __str1, QString __sep, QString __str2, bool __allowDuplSep)
{
    QString sep = ((!__str1.isEmpty()) && (!__str2.isEmpty()) && ((!__str1.endsWith(__sep)) || (__allowDuplSep)))?__sep:"";
    return __str1 + sep + __str2;
}
// -----------------------------------------------------------------------------

// #brief Creates the error message using given parameters
//
// #param _tid Table id.
// #param _cid Cell id.
// #param _err Error message.
// #param _msgType Type of message
// #li 0 - error.
// #li 1 - warning.
// #li 2 - information.
// #param _tc Origin of added element.
// #li 0 - Added element comes from cell.
// #li 1 - Added element comes from connection.
// #li 2 - Added element comes from type or attribute
// #li 3 - Added element comes from table
// #li 4 - Added element comes from table header
// #li 5 - Added element comes from row
// #li 6 - Added element comes from stategroup
// #li 7 - Added element comes from state
// #li 8 - Added element comes from single cell of the state
// #return the error string that can be parsed by widget
QString hqed::createErrorString(QString _tid, QString _cid, QString _err, int _msgType, int _tc)
{
    QString sep = messageSeparator();
    QString result = errorIndicator(NEW_MESSAGE_INDICATOR) + _tid;
    result += sep + _cid;
    result += sep + QString::number(_msgType);
    result += sep + QString::number(_tc);
    result += sep + _err + errorIndicator(END_MESSAGE_INDICATOR);

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that retrieves selected message parts. The message format is compatible with createErrorString() result
//
// #param __message - the message that contain the required parts
// #param __parts - indicates which parts should be retrieved
// #param __separator - allows to define the separator beetween retrieved parts
// #return string that contains the required parts separated by __separator
QString hqed::retrieveMessageParts(QString __message, int __parts, QString __separator)
{
    /*QString result = errorIndicator(NEW_MESSAGE_INDICATOR) + QString::number(_tid);
     result += "|" + QString::number(_cid);
     result += "|" + QString::number(_msgType);
     result += "|" + QString::number(_tc);
     result += "|" + QString::number(_err) + errorIndicator(END_MESSAGE_INDICATOR);*/

    int allParts[] = {MESSAGE_PART_TABLE_ID,
                      MESSAGE_PART_CELL_ID,
                      MESSAGE_PART_MSGTYPE,
                      MESSAGE_PART_MORIGIN,
                      MESSAGE_PART_MESSAGE};

    QString sep = messageSeparator();
    int seplen = sep.length();
    QStringList __strParts;

    int pos = __message.indexOf(hqed::errorIndicator(NEW_MESSAGE_INDICATOR));
    if(pos == -1)       // it means that the message has other format, so we return the whole content
        return __message;

    __message = __message.remove(0, pos+hqed::errorIndicator(NEW_MESSAGE_INDICATOR).size());

    pos = __message.indexOf(sep);
    __strParts.append(__message.left(pos));
    __message = __message.remove(0, pos+seplen);

    pos = __message.indexOf(sep);
    __strParts.append(__message.left(pos));
    __message = __message.remove(0, pos+seplen);

    pos = __message.indexOf(sep);
    __strParts.append(__message.left(pos));
    __message = __message.remove(0, pos+seplen);

    pos = __message.indexOf(sep);
    __strParts.append(__message.left(pos));
    __message = __message.remove(0, pos+seplen);

    pos = __message.indexOf(hqed::errorIndicator(END_MESSAGE_INDICATOR));
    __strParts.append(__message.left(pos));

    int pcount = 0;
    QString result = "";
    for(int i=0;i<__strParts.size();++i)
    {
        if((__parts & allParts[i]) == 0)
            continue;
        if(pcount > 0)
            result += __separator;
        pcount++;
        result += __strParts.at(i);
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that shows all the messages that contains the __messages argument
//
// #param __messages - the messages. The string has compatible format with createErrorString function.
// #param __msgsTypes - the filter that allow for showing only eequested type(s) of messages
// #return no values returns
void hqed::showMessages(QString __messages, int __msgsTypes)
{
    int pos = __messages.indexOf(hqed::errorIndicator(NEW_MESSAGE_INDICATOR));
    while(pos > -1)
    {
        QString smt = hqed::retrieveMessageParts(__messages, MESSAGE_PART_MSGTYPE);
        QString msg = hqed::retrieveMessageParts(__messages, MESSAGE_PART_MESSAGE);

        int _mt = smt.toInt();
        if((_mt == 0) && ((__msgsTypes & MESSAGE_TYPE_ERROR) != 0))
            QMessageBox::critical(NULL, "HQEd", msg, QMessageBox::Ok);
        if((_mt == 1) && ((__msgsTypes & MESSAGE_TYPE_WARNING) != 0))
            QMessageBox::warning(NULL, "HQEd", msg, QMessageBox::Ok);
        if((_mt == 2) && ((__msgsTypes & MESSAGE_TYPE_INFORMATION) != 0))
            QMessageBox::information(NULL, "HQEd", msg, QMessageBox::Ok);

        pos = __messages.indexOf(hqed::errorIndicator(END_MESSAGE_INDICATOR));
        __messages = __messages.remove(0, pos+hqed::errorIndicator(END_MESSAGE_INDICATOR).size());
        pos = __messages.indexOf(hqed::errorIndicator(NEW_MESSAGE_INDICATOR));
    }
}
// -----------------------------------------------------------------------------

// #brief Function that creates the information about current element localization
//
// #param __xmlElement - the reference to the element
// #return string that contains the information about element localization
QString hqed::createDOMlocalization(QDomElement const & __xmlElement)
{
    QString result = "";
    result += "\nTag name: " + __xmlElement.tagName();
    result += "\nLine number: " + QString::number(__xmlElement.lineNumber());
    result += "\nColumn number: " + QString::number(__xmlElement.columnNumber());
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hqed::sendEventMessage(QString* __errmsgs, int __event, ...)
{
    bool result = true;
    va_list argsptr;

    if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        result = ardAttributeList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardPropertyList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardDependencyList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardLevelList->eventMessage(__errmsgs, __event, attr) && result;
        result = AttributeGroups->eventMessage(__errmsgs, __event, attr) && result;
        result = ConnectionsList->eventMessage(__errmsgs, __event, attr) && result;
        result = TableList->eventMessage(__errmsgs, __event, attr) && result;
        result = States->eventMessage(__errmsgs, __event, attr) && result;
        result = OutputStates->eventMessage(__errmsgs, __event, attr) && result;

        va_end(argsptr);
    }

    if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        result = ardAttributeList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardPropertyList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardDependencyList->eventMessage(__errmsgs, __event, attr) && result;
        result = ardLevelList->eventMessage(__errmsgs, __event, attr) && result;
        result = AttributeGroups->eventMessage(__errmsgs, __event, attr) && result;
        result = ConnectionsList->eventMessage(__errmsgs, __event, attr) && result;
        result = TableList->eventMessage(__errmsgs, __event, attr) && result;
        result = States->eventMessage(__errmsgs, __event, attr) && result;
        result = OutputStates->eventMessage(__errmsgs, __event, attr) && result;

        va_end(argsptr);
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function process given string with respect to system variables. It replaces founded variables with their current values
//
// #param __tmpl - the event identifier
// #return the string with the current system variables
QString hqed::processAppVariables(QString __tmpl)
{
    __tmpl = __tmpl.replace("%appdir%", appRootPath(), Qt::CaseInsensitive);
    __tmpl = __tmpl.replace("%trsvarname%", hEcmaScript::scriptTranslationVariable(), Qt::CaseInsensitive);
    return __tmpl;
}
// -----------------------------------------------------------------------------

// #brief Equivalent of cp -rf but doesnt copy executable files
//
// #return list of files that function was unable to copy
void copyPath(QString src, QString dst,QStringList &failedList)
{
    QDir dir(src);
    if (! dir.exists())
        return;

    foreach (QString d, dir.entryList(QDir::Dirs | QDir::NoDotAndDotDot))
    {
        QString dst_path = dst + QDir::separator() + d;
        dir.mkpath(dst_path);
        copyPath(src+ QDir::separator() + d, dst_path,failedList);
    }

    foreach (QString f, dir.entryList(QDir::Files))
    {
        QString sourcePath = src + QDir::separator() + f;
        QFileInfo sourcePathInfo(sourcePath);

        if(!sourcePathInfo.isExecutable() || sourcePathInfo.isDir())
                if(!QFile::copy(sourcePath, dst + QDir::separator() + f))
                failedList << dst + QDir::separator() + f;
    }
}
// -----------------------------------------------------------------------------

// #brief Function that returns the root path of the application
//
// #return the root path of the application as string
QString hqed::appRootPath(void)
{
#ifdef Q_OS_LINUX
    QString apppath = "/usr/share/hqed";
#elif defined(Q_OS_WIN32)
    QString apppath = QCoreApplication::applicationDirPath();
#else
#error "We don't support that version yet..."
#endif


    QString homePath = QDir::homePath();
    QDir mockApplicationPath = QDir(homePath).filePath(".hqed");
    mockApplicationPath.makeAbsolute();

    // if ~/.hqed or C://Users//User1/.home doesn't exist...
    static bool MockFolerCheckAlreadyRun = false;
    if(!MockFolerCheckAlreadyRun && !mockApplicationPath.exists())
    {
        if(!mockApplicationPath.mkpath("."))
        {
            QMessageBox::critical(NULL, "FATAL ERROR", "Failed to create .hqed directory in " + homePath);
            exit(EXIT_FAILURE);
        }
        // ... copy files from /user/share/hqed to it
        QStringList failedCopyList;
        copyPath(apppath, mockApplicationPath.absolutePath(),failedCopyList);
        if(failedCopyList.size() > 0)
        {
            QMessageBox::critical(NULL, "FATAL ERROR", "Failed to copy hqed files to" + mockApplicationPath.path());
            exit(EXIT_FAILURE);
        }
        MockFolerCheckAlreadyRun = true;
    }

    return mockApplicationPath.path();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the layout properities
//
// #param __layout - pointer to the layout
// #return no values return
void hqed::setupLayout(QGridLayout* __layout)
{
    if(__layout == NULL)
        return;

    __layout->setSpacing(2);
    __layout->setMargin(2);
}
// -----------------------------------------------------------------------------

// #brief Function that parses the protocol version 1 messages
//
// #param __message - received raw protocol message
// #param rcode - result code of the function
// #param rmsg - the message about error if occur
// #return the list of protocol elements
QStringList hqed::parseProtocolV1Message(QString __message, int& rcode, QString& rmsg)
{
    bool qoutation = false;       // indicates if the current char is quoted
    bool lastsep = false;         // true means that the last non white character was separator
    bool lastbc = false;          // true means that the last non white character was bracket close

    int bracketbalance = 0;       // the balance of brackets

    QChar __qo = '\'';
    QChar __qc = '\'';
    QChar __bo = '[';
    QChar __bc = ']';
    QChar __sep = ',';

    QString current;
    QStringList result;

    __message = __message.trimmed();                            // Removing white spaces from the start and end
    //qDebug() << "Parsing: " << __message;
    if(__message.isEmpty())
    {
        rmsg = "Parsing message: empty message.";
        rcode = PROTOCOL_PARSING_RESULT_ERROR_FORMAT;
        return result;
    }
    if(__message[0] != __bo)                                    // When no opening bracket on the start
    {
        rmsg = "Parsing message: opening bracket not found.";
        rcode = PROTOCOL_PARSING_RESULT_ERROR_FORMAT;
        return result;
    }

    // Parsing message
    for(int i=0;i<__message.length();++i)
    {
        if((__message[i] == __qo) && (!qoutation))              // When quotation opens - we do not read it
        {
            qoutation = true;
            if(bracketbalance == 1)                             // The nested quoatation marks will be parsed later
                continue;
        }
        else if((__message[i] == __qc) && (qoutation))          // When quotation closes - we do not read it
        {
            qoutation = false;
            if(bracketbalance == 1)                             // The nested quoatation marks will be parsed later
                continue;
        }

        if((!qoutation) && (__message[i].isSpace()))           // When not quoted white char - we do not read it
            continue;


        if((!qoutation) && (__message[i] == __bo))             // When not quoted opening bracket
            bracketbalance++;
        if((!qoutation) && (__message[i] == __bc))             // When not quoted closing bracket
            bracketbalance--;
        if(bracketbalance < 0)
        {
            rmsg = "Parsing message: missplaced bracket in \'" + __message + "\' at character " + QString::number(i);
            rcode = PROTOCOL_PARSING_RESULT_ERROR_FORMAT;
            return result;
        }
        if((!qoutation) && (__message[i] == __bo) && (bracketbalance == 1))        // When opening bracket
            continue;

        // When end reading of single argument
        if((!qoutation) &&
                (((__message[i] == __sep) && (bracketbalance == 1)) ||
                 ((__message[i] == __bc) && (bracketbalance == 0))))
        {
            if(!current.isEmpty())
                result << current;
            current = "";
            continue;
        }

        if((!qoutation) && (__message[i] == __sep) && (bracketbalance == 1))       // When not quoted separator - we do not read it
        {
            lastsep = true;
            lastbc = false;
            continue;
        }

        current += QString(__message[i]);
    }

    // Checking variables state after parsing
    if(qoutation)
    {
        rmsg = "Parsing message: missing qotation closing character.";
        rcode = PROTOCOL_PARSING_RESULT_ERROR_FORMAT;
        return result;
    }
    if(bracketbalance != 0)
    {
        rmsg = "Parsing message: missing bracket.";
        rcode = PROTOCOL_PARSING_RESULT_ERROR_FORMAT;
        return result;
    }

    rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __parts - indicates which parts of the model should be exported
// #return List of all data in hml 2.0 format.
QStringList* hqed::out_ard_xtt_atts_HML_2_0(int __parts)
{
    QStringList* res = new QStringList;
    res->clear();

    __parts = __parts & (VAS_XML_MODEL_PARTS_ARD_ATTRS | VAS_XML_MODEL_PARTS_XTT_ATTRS);
    if(__parts == 0)
        return res;

    *res << "<attributes>";
    if((__parts & VAS_XML_MODEL_PARTS_ARD_ATTRS) != 0)
    {
        QStringList* tmp = ardAttributeList->out_HML_2_0();
        for(int j=0;j<tmp->size();++j)
            *res << "\t" + tmp->at(j);
        delete tmp;
    }
    if((__parts & VAS_XML_MODEL_PARTS_XTT_ATTRS) != 0)
    {
        QStringList* tmp = AttributeGroups->out_HML_2_0();
        for(int j=0;j<tmp->size();++j)
            *res << "\t" + tmp->at(j);
        delete tmp;
    }
    *res << "</attributes>";

    return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __parts - indicates which parts of the model should be exported
// #return No retur value.
void hqed::out_ard_xtt_atts_HML_2_0(QXmlStreamWriter* _xmldoc, int __parts)
{
    __parts = __parts & (VAS_XML_MODEL_PARTS_ARD_ATTRS | VAS_XML_MODEL_PARTS_XTT_ATTRS);
    if(__parts == 0)
        return;

    _xmldoc->writeStartElement("attributes");
    if((__parts & VAS_XML_MODEL_PARTS_ARD_ATTRS) != 0)
        ardAttributeList->out_HML_2_0(_xmldoc);
    if((__parts & VAS_XML_MODEL_PARTS_XTT_ATTRS) != 0)
        AttributeGroups->out_HML_2_0(_xmldoc);
    _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function that initializes clipboard
//
// #return no values return
void hqed::initClipboard(void)
{
    __clipboardSet = new QStack<hSet*>;
    __clipboardType = new QStack<hType*>;
    __clipboardDomain = new QStack<hDomain*>;
    __attributeDomain = new QStack<XTT_Attribute*>;
    __clipboardMultipleValue = new QStack<hMultipleValue*>;
}
// -----------------------------------------------------------------------------

// #brief Function that initializes clipboard
//
// #return no values return
void hqed::destroyClipboard(void)
{
    delete __clipboardSet;
    delete __clipboardType;
    delete __clipboardDomain;
    delete __attributeDomain;
    delete __clipboardMultipleValue;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the set clipboard
//
// #return the pointer to the set clipboard
QStack<hSet*>* hqed::clipboardSet(void)
{
    return __clipboardSet;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the type clipboard
//
// #return the pointer to the type clipboard
QStack<hType*>* hqed::clipboardType(void)
{
    return __clipboardType;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the domain clipboard
//
// #return the pointer to the domain clipboard
QStack<hDomain*>* hqed::clipboardDomain(void)
{
    return __clipboardDomain;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the attributes clipboard
//
// #return the pointer to the attributes clipboard
QStack<XTT_Attribute*>* hqed::clipboardXttAttribute(void)
{
    return __attributeDomain;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the multiple value clipboard
//
// #return the pointer to the multiple value clipboard
QStack<hMultipleValue*>* hqed::clipboardMultipleValue(void)
{
    return __clipboardMultipleValue;
}
// -----------------------------------------------------------------------------
