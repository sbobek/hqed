/**
 * \file	ns_hqed.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.0
 * \brief	This file contains hqed namespace declarations and functions belong to this namespace
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef NS_HQEDH
#define NS_HQEDH
// -----------------------------------------------------------------------------

#include <QString>
#include <QStringList>
#include <QDomDocument>
#include <QHostAddress>
#include <QIODevice>
#include <QLocale>
#include <QGridLayout>
#include <QStack>
#if QT_VERSION >= 0x040300
#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

// Type of search
#define CONJUNCTION_POLICY 0
#define DISJUNCTION_POLICY 1

// Type of error
#define SYNTAX_ERROR 0
#define LOGIC_ERROR 1

// generateXTT return messages
#define XTT_GEN_SUCCESS 0
#define XTT_GEN_ERROR_NO_LEVELS_DEFINED 1
#define XTT_GEN_ERROR_NO_FINALIZED_LEVEL 2

// MessageIndicators
#define NEW_MESSAGE_INDICATOR 0
#define END_MESSAGE_INDICATOR 1

// MessageParts
#define MESSAGE_PART_TABLE_ID 0x00000001
#define MESSAGE_PART_CELL_ID  0x00000002
#define MESSAGE_PART_MSGTYPE  0x00000004
#define MESSAGE_PART_MORIGIN  0x00000008
#define MESSAGE_PART_MESSAGE  0x00000010

// Protocol message parsing results
#define PROTOCOL_PARSING_RESULT_SUCCESS               0
#define PROTOCOL_PARSING_RESULT_ERROR_FORMAT          0x00000100

// Window Mode Types
#define SET_WINDOW_MODE_FAIL               0x00000001       // mode setup failed
#define SET_WINDOW_MODE_OK_YOU_CAN_SHOW    0x00000002       // mode setup success, you can show another dialog
#define SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW 0x00000004       // mode setup success, you cannot show another dialog. The action is takes automatically

// MessageTypes
#define MESSAGE_TYPE_ERROR       0x00000001
#define MESSAGE_TYPE_WARNING     0x00000002
#define MESSAGE_TYPE_INFORMATION 0x00000004

// Event signals
#define XTT_SIGNAL_ATTRIBUTE_CHANGED 1       // QString* __errmsgs, int __event, XTT_Attribute* __attptr
#define XTT_SIGNAL_ATTRIBUTE_REMOVED 2       // QString* __errmsgs, int __event, XTT_Attribute* __attptr
//---------------------------------------------------------------------------

class hSet;
class hType;
class hDomain;
class hModelList;
class hMultipleValue;
class XTT_Attribute;
//---------------------------------------------------------------------------

namespace hqed
{
     /// \brief creates the models conatiner
     ///
     /// \return no values return
     void createModelList(void);

     /// \brief Returns the pointer to the current model list
     ///
     /// \return the pointer to the current model list
     hModelList* modelList(void);

     /// \brief Search for phrases in the text
     ///
     /// \param *_phrases - list of searched phrases
     /// \param _text - text for phrases search
     /// \param _cs - specify search CaseSensitivity
     /// \param _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
     /// \return It return a list of tables which satisfied the search criteria
     bool searchForPhrases(QStringList* _phrases, QString _text, Qt::CaseSensitivity _cs, int _phrasesPolicy);

     /// \brief Compares content of two lists
     ///
     /// \param _first - the first list
     /// \param _second - the second list
     /// \return returns true if lists have the same content (order is not important)
     template <class T>
     bool compareListContents(QList<T>& _first, QList<T>& _second)
     {
          if(_first.size() != _second.size())
               return false;

          for(int i=0;i<_first.size();++i)
               if(!_second.contains(_first.at(i)))
                    return false;

          return true;
     }

     /// \brief Merges two lists
     ///
     /// \param _first - the first list
     /// \param _second - the second list
     /// \return returns list where first are elements from _first list and later that elements from second which are not in the first
     template <class T>
     QList<T> mergeListContents(QList<T>& _first, QList<T>& _second)
     {
          QList<T> res = _first;

          for(int i=0;i<_second.size();++i)
               if(!res.contains(_second.at(i)))
                    res.append(_second.at(i));

          return res;
     }

     /// \brief Inverts the order of items in the list
     ///
     /// \param _list - list to be inverted
     /// \return returns list where items are in the opposite order w.r.t. input list
     template <class T>
     void invertList(QList<T>* _list)
     {
          int lsize = _list->size();
          int starti = 0;
          int endi = (lsize >> 1);

          for(int i=starti;i<endi;++i)
               _list->swap(i,lsize-1-i);
     }

     /// \brief Get list intersection
     ///
     /// \param _first - the first list
     /// \param _second - the second list
     /// \return returns list where are the common elements from the both lists
     template <class T>
     QList<T> listIntersection(const QList<T>& _first, const QList<T>& _second)
     {
          QList<T> res;

          for(int i=0;i<_first.size();++i)
               if(_second.contains(_first.at(i)))
                    res.append(_first.at(i));

          return res;
     }

     /// \brief Get list substraction
     ///
     /// \param _minued - minued list
     /// \param _subtrahend - the _subtrahend list
     /// \return returns list where are the elements that are not in the _subtrahend list
     template <class T>
     QList<T> listSubtraction(const QList<T>& _minued, const QList<T>& _subtrahend)
     {
          QList<T> res;

          for(int i=0;i<_minued.size();++i)
               if(!_subtrahend.contains(_minued.at(i)))
                    res.append(_minued.at(i));

          return res;
     }

     /// \brief Get information about the first list contains all the elements from the second list
     ///
     /// \param _list - the first list
     /// \param _sub - the list about which we want to know if it is a subset the first one
     /// \return returns true if the first list contains all the elements from the second list
     template <class T>
     bool isSublist(QList<T> _list, QList<T> _sub)
     {
          for(int i=0;i<_sub.size();++i)
               if(!_list.contains(_sub.at(i)))
                    return false;

          return true;
     }

     /// \brief Search for unique key name for the map container
     ///
     /// \param __map - pointer to the map container
     /// \param __kBaseName - the base name of the key
     /// \return returns the new unique key
     template <class DataType>
     QString findNextKey(QMap<QString, DataType>* __map, QString __kBaseName = "__map_key_")
     {
          int index = 0;
          QString new_key;

          do
          {
               new_key = __kBaseName + QString::number(index++);
          }
          while(__map->contains(new_key));

          return new_key;
     }

     /// \brief Generates XTT schema
     ///
     /// \return return 0- if ok, 1- if o the level are non atomic properties
     int generateXTT(void);

     /// \brief Sets up the local settings for application
     ///
     /// \return No returns value
     void setLocalSettings(void);

     /// \brief returns the local settings
     ///
     /// \return The local settings
     QLocale localSettings(void);

     /// \brief Functions that set the widget on screen center
     ///
     /// \param *__widget - the pointer to the widget that should be centered on the screen
     /// \return No returns value
     void centerWindow(QWidget* __widget);

     /// \brief Get value from xttml paramter line.
     ///
     /// \param line Parameters line.
     /// \param val_name Name of the value.
     /// \return Searched value.
     QString XML_ReadValue(QString line, QString val_name);

     /// \brief Remove space at the begin and end of the string.
     ///
     /// \param str Input string.
     /// \return Changed string.
     QString RemoveSpaceAtEnds(QString str);

     /// \brief mcwp is an abbreviation of Make Compatible With PROLOG. The function converts given string into string that is compatible with PROLOG.
     ///
     /// \param __string - string to conversion
     /// \return string that is compatible with PROLOG
     QString mcwp(QString __string);

     /// \brief returns the list of characters that cannot be used as starting character in the ARD and XTT attribute name
     ///
     /// \return the list of characters that cannot be used as starting character in the ARD and XTT attribute name
     QString attributeNameNotAllowedStartingCharactersList(void);

     /// \brief checks the name of the ARD or XTT attrbiute
     ///
     /// \param __name - name of the attribute to be checked
     /// \return exit code
     /// \li 0 - if the given name is correct
     /// \li 1 - if the name is empty
     /// \li 2 - if the name starts with incorrect character
     int checkAttributeName(QString __name);
     
     /// \brief returns the version of the hqed
     ///
     /// \return the version of the hqed as string
     QString hqedVersion(void);

     /// \brief returns the full name of the editor
     ///
     /// \return the full name of the editor
     QString hqedName(void);

     /// \brief Returns the error indicator, that is necessary to message parse
     ///
     /// \param __messageIndicator - denotes if the new or end message indicator should be returned
     /// \return the error indicator as string
     QString errorIndicator(int __messageIndicator);

     /// \brief Returns the separator that is used to separate the parts of the messages
     ///
     /// \return the message separator as string
     QString messageSeparator(void);

     /// \brief Returns the name of the logic server HeaRT
     ///
     /// \return the name of the logic server HeaRT as string
     QString logicServerName(void);

     /// \brief Concatenates two strings with given separator.
     ///
     /// \param __str1 - first string
     /// \param __sep - strings separator
     /// \param __str2 - second separator
     /// \param __allowDuplSep - indicates if the the separator can be double: when the __str1 ends with __sep
     /// \return the concatenated string
     QString concatenate(QString __str1, QString __sep, QString __str2, bool __allowDuplSep = false);

     /// \brief Creates the error message using given parameters
     ///
     /// \param _tid Table id.
     /// \param _cid Cell id.
     /// \param _err Error message.
     /// \param _msgType Type of message
     /// \li 0 - error.
     /// \li 1 - warning.
     /// \li 2 - information.
     /// \param _tc Origin of added element.
     /// \li 0 - Added element comes from cell.
     /// \li 1 - Added element comes from connection.
     /// \li 2 - Added element comes from type
     /// \return the error string that can be parsed by widget
     QString createErrorString(QString _tid, QString _cid, QString _err, int _msgType = 0, int _tc = 0);

     /// \brief Function that retrieves selected message parts. The message format is compatible with createErrorString() result
     ///
     /// \param __message - the message that contain the required parts
     /// \param __parts - indicates which parts should be retrieved
     /// \param __separator - allows to define the separator beetween retrieved parts
     /// \return string that contains the required parts separated by __separator
     QString retrieveMessageParts(QString __message, int __parts = MESSAGE_PART_MESSAGE, QString __separator = "|");

     /// \brief Function that shows all the messages that contains the __messages argument
     ///
     /// \param __messages - the messages. The string has compatible format with createErrorString function.
     /// \param __msgsTypes - the filter that allow for showing only eequested type(s) of messages
     /// \return no values returns
     void showMessages(QString __messages, int __msgsTypes = MESSAGE_TYPE_ERROR | MESSAGE_TYPE_WARNING | MESSAGE_TYPE_INFORMATION);

     /// \brief Function that creates the information about current element localization
     ///
     /// \param __xmlElement - the reference to the element
     /// \return string that contains the information about element localization
     QString createDOMlocalization(QDomElement const & __xmlElement);

     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool sendEventMessage(QString* __errmsgs, int __event, ...);

     /// \brief Function process given string with respect to system variables. It replaces founded variables with their current values
     ///
     /// \param __tmpl - the event identifier
     /// \return the string with the current system variables
     QString processAppVariables(QString __tmpl);

     /// \brief Function that returns the root path of the application
     ///
     //// \return the root path of the application as string
     QString appRootPath(void);

     /// \brief Function that sets the layout properities
     ///
     /// \param __layout - pointer to the layout
     /// \return no values return
     void setupLayout(QGridLayout* __layout);

     /// \brief Function that parses the protocol version 1 messages
     ///
     /// \param __message - received raw protocol message
     /// \param rcode - result code of the function
     /// \param rmsg - the message about error if occur
     /// \return the list of protocol elements
     QStringList parseProtocolV1Message(QString __message, int& rcode, QString& rmsg);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \param __parts - indicates which parts of the model should be exported
     /// \return List of all data in hml 2.0 format.
     QStringList* out_ard_xtt_atts_HML_2_0(int __parts);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \param __parts - indicates which parts of the model should be exported
     /// \return No retur value.
     void out_ard_xtt_atts_HML_2_0(QXmlStreamWriter* _xmldoc, int __parts);
     #endif

     // ------------------ CLIPBOARD FUNCTIONS ------------------

     /// \brief Function that initializes clipboard
     ///
     /// \return no values return
     void initClipboard(void);

     /// \brief Function that initializes clipboard
     ///
     /// \return no values return
     void destroyClipboard(void);

     /// \brief Function that returns the pointer to the set clipboard
     ///
     /// \return the pointer to the set clipboard
     QStack<hSet*>* clipboardSet(void);

     /// \brief Function that returns the pointer to the type clipboard
     ///
     /// \return the pointer to the type clipboard
     QStack<hType*>* clipboardType(void);

     /// \brief Function that returns the pointer to the domain clipboard
     ///
     /// \return the pointer to the domain clipboard
     QStack<hDomain*>* clipboardDomain(void);

     /// \brief Function that returns the pointer to the attributes clipboard
     ///
     /// \return the pointer to the attributes clipboard
     QStack<XTT_Attribute*>* clipboardXttAttribute(void);

     /// \brief Function that returns the pointer to the multiple value clipboard
     ///
     /// \return the pointer to the multiple value clipboard
     QStack<hMultipleValue*>* clipboardMultipleValue(void);
}
// -----------------------------------------------------------------------------

#endif
