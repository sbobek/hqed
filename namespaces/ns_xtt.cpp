/**
 * \file	ns_xtt.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	29.12.2008 
 * \version	1.0
 * \brief	This file contains definitions of functions from xtt namespace.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QDate>

#include "../C/XTT/widgets/MyGraphicsView.h"
#include "../C/XTT/widgets/MyGraphicsScene.h"
#include "../C/XTT/widgets/StateGraphicsScene.h"
#include "../C/XTT/widgets/ErrorsListWidget.h"
#include "../C/XTT/widgets/ExecuteMessagesListWidget.h"
#include "../C/devPluginInterface/devPlugin.h"
#include "../C/devPluginInterface/devPluginController.h"

#include "../V/XTT/GTable.h"

#include "../M/hType.h"
#include "../M/hTypeList.h"
#include "../M/hFunctionList.h"
#include "../M/hMultipleValue.h"

#include "../M/XTT/XTT_Row.h"
#include "../M/XTT/XTT_Cell.h"
#include "../M/XTT/XTT_Table.h"
#include "../M/XTT/XTT_State.h"
#include "../M/XTT/XTT_States.h"
#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_Attribute.h"
#include "../M/XTT/XTT_Statesgroup.h"

#include "../M/XTT/XTT_Connections.h"
#include "../M/XTT/XTT_AttributeList.h"
#include "../M/XTT/XTT_ConnectionsList.h"
#include "../M/XTT/XTT_AttributeGroups.h"

#include "../C/About_ui.h"
#include "../C/MainWin_ui.h"
#include "../C/Settings_ui.h"

#include "ns_hqed.h"
#include "ns_xtt.h"
// -----------------------------------------------------------------------------

// #brief Function that returns a pointer to the scene where XTT model is displayed
//
// #return pointer to the scene where XTT model is displayed
MyGraphicsScene* xtt::modelScene(void)
{
    //return MainWin->xtt_state_scene;
    return MainWin->xtt_scene;
}
// -----------------------------------------------------------------------------

// #brief Function that returns a pointer to the scene where XTT states are displayed
//
// #return pointer to the scene where XTT states are displayed
MyGraphicsScene* xtt::stateScene(void)
{
    return MainWin->xtt_state_scene;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the table objects is correct on the logical level
//
// #param __table - pointer to verified table
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalTableVerification(XTT_Table* __table)
{
    QString em;
    return logicalTableVerification(__table, em, true);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the table objects are correct on the logical level
//
// #param __table - pointer to verified table
// #param __error_message - the reference to the error message(s)
// #param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalTableVerification(XTT_Table* __table, QString& _error_message, bool __display_messages)
{
    if(__table == NULL)
    {
        QMessageBox::critical(NULL, "HQEd", "xtt::logicalTableVerification: incorrect table pointer.\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
        return DEV_PLUGIN_RESULT_CODE_ERROR;
    }

    return logicalTableVerification(__table->Title(), _error_message, __display_messages);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the table objects is correct on the logical level
//
// #param __schema - title of verified table
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalTableVerification(QString __schema)
{
    QString em;
    return logicalTableVerification(__schema, em, true);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the table objects are correct on the logical level
//
// #param __schema - title of verified table
// #param __error_message - the reference to the error message(s)
// #param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalTableVerification(QString __schema, QString& _error_message, bool __display_messages)
{
    // Checking if the given table is a normal table
    QString tid = "";
    XTT_Table* tbl = TableList->_IndexOfTitle(__schema);
    if(tbl != NULL)
    {
        if(tbl->TableType() != 1)
            return DEV_PLUGIN_RESULT_CODE_SUCCESS;
        tid = tbl->TableId();
    }

    int result = devPC->logicalTableVerification(__schema, _error_message);

    if(__display_messages)
    {
        MainWin->xtt_errorsList->deleteFilter(LOGIC_ERROR, tid);
        MainWin->xtt_errorsList->parseMessages(_error_message, LOGIC_ERROR);
    }
    if(Settings->xttShowErrorMessages())
        hqed::showMessages(_error_message);

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the model is correct on the logical level
//
// #param __error_message - the reference to the error message(s)
// #param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalFullVerification(QString& _error_message, bool __display_messages)
{
    int result = devPC->logicalFullVerification(_error_message);

    if(__display_messages)
    {
        MainWin->xtt_errorsList->deleteFilter(LOGIC_ERROR);
        MainWin->xtt_errorsList->parseMessages(_error_message, LOGIC_ERROR);
    }
    if(Settings->xttShowErrorMessages())
        hqed::showMessages(_error_message);

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the model is correct on the logical level
//
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int xtt::logicalFullVerification(void)
{
    QString em;
    return logicalFullVerification(em, true);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the model is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool xtt::modelVerification(QString& _error_message)
{
    bool res = true;

    res = States->statesVerification(_error_message) && res;
    res = AttributeGroups->attributesVerification(_error_message) && res;
    res = TableList->tablesVerification(_error_message) && res;

    _error_message = _error_message.replace("\n\n", "\n");
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the types objects are correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool xtt::typesVerification(QString& _error_message)
{
    bool res = typeList->verification(_error_message);
    _error_message = _error_message.replace("\n\n", "\n");
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns result of system verification
//
// #param __error_message - the reference to the error message(s)
// #param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
// #return true if yes otherwise false
bool xtt::fullVerification(QString& _error_message, bool __display_messages)
{
    bool result = true;

    QString model_messages = "",
    types_messages = "";

    if(!modelVerification(model_messages))
        result = false;

    if(!typesVerification(types_messages))
        result = false;

    // removing error messages if the __display_messages is true because after verification the new messages will be added
    if(__display_messages)
        MainWin->xtt_errorsList->deleteFilter(SYNTAX_ERROR);
    // Adding to the error widget
    if(__display_messages)
        MainWin->xtt_errorsList->parseMessages(model_messages);
    if(__display_messages)
        MainWin->xtt_errorsList->parseMessages(types_messages);

    if(!types_messages.isEmpty())
        _error_message += "Types errors:" + types_messages;
    if(!model_messages.isEmpty())
        _error_message += "Model errors:" + model_messages;

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns result of system verification
//
// #return true if yes otherwise false
bool xtt::fullVerification(void)
{
    QString msgs;
    bool result = fullVerification(msgs);

    if(Settings->xttShowErrorMessages())
        hqed::showMessages(msgs);

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that removes all the XTT items
//
// #param __keepAttributes - indicates if the attributes must be also removed
// #return No return value.
void xtt::clear(bool __keepAttributes)
{
    OutputStates->flush();
    States->flush();
    ConnectionsList->Flush();
    TableList->Flush();
    if(!__keepAttributes)
        AttributeGroups->Flush();

    funcList->flush();
    typeList->flush();

    typeList->createBaseTypes();
    funcList->fill();
}
// -----------------------------------------------------------------------------

// #brief Finds list of tables connected to the table.
//
// #param _initalTable Pointer to current init table.
// #param _tables Pointer to list of tabels id that have at least one parent.
// #param _initsList Pointer to list of pointer to parent table list.
// #param _currentTableID Current table id.
// #return No return value.
void xtt::findInitialTables(XTT_Table* _initalTable, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, QString _currentTableID)
{
    // Sprawdzamy czy bierzaca tabela jest juz na liscie
    int ti = _tables->indexOf(_currentTableID);

    // Kiedy tabela wystepuje na liscie juz zdefiniowanych rodzicow
    if(ti > -1)
    {
        // Jezeli tabela i rodzic juz wystepuja to prawdopodobnie jest to polaczenie zapetlajace, wiec powracamy
        if(_initsList->at(ti)->indexOf(_initalTable) > -1)
            return;
        _initsList->at(ti)->append(_initalTable);
    }

    // Kiedy tabela nie wystepuje na liscie juz zdefiniowanych rodzicow
    if(ti == -1)
    {
        ti = _tables->size();
        _tables->append(_currentTableID);
        QList<XTT_Table*>* newList = new QList<XTT_Table*>;
        newList->append(_initalTable);
        _initsList->append(newList);
    }

    // Wyszukiwanie wszystkich polaczen wychodzacych z bierzacej tabeli
    QList<XTT_Connections*>* outConnList = ConnectionsList->listOfOutConnections(_currentTableID);

    // Dla kazdego polaczenia wyszukujemy liste tabel i przechodzimy do nich
    for(int i=0;i<outConnList->size();i++)
        findInitialTables(_initalTable, _tables, _initsList, outConnList->at(i)->DesTable());

    delete outConnList;
}
// -----------------------------------------------------------------------------

// #brief Traces the model execution.
//
// #param __messages - Reference to the messages string.
// #return the list of row ids that can be executed.
QList<XTT_Row*>* xtt::trace(QString& __messages)
{	
    QList<XTT_Row*>* result = new QList<XTT_Row*>;

    QString error;
    QStringList tableTypes;
    tableTypes << "fact" << "middle" << "halt";

    // Sprawdzanie czy mozna cos uruchomic
    if(TableList->Count() == 0)
        return result;

    // Wyszukanie wszystkich tabel ktore nie maja polaczen
    QList<XTT_Table*>* fact_tabele_list = new QList<XTT_Table*>;
    QList<XTT_Table*>* halt_tabele_list = new QList<XTT_Table*>;
    for(unsigned int i=0;i<TableList->Count();i++)
    {
        QList<XTT_Connections*>* cilist = TableList->Table(i)->inConnectionsList();
        QList<XTT_Connections*>* colist = TableList->Table(i)->outConnectionsList();

        // Jezeli tabela nie ma poloczen przychodzacych, to uwazamy ja za inicjalizacyjna
        if(((Settings->autoTableTypeRecognition()) && (cilist->size() == 0)) || ((!Settings->autoTableTypeRecognition()) && (TableList->Table(i)->TableType() == 0)))
        {
            fact_tabele_list->append(TableList->Table(i));
            int tt = TableList->Table(i)->TableType();
            if((false) && (tt != 0) && (Settings->messageAboutAmbiguousTableType()))
            {
                QString wrng = "Table \'" + TableList->Table(i)->Title() + "\' seems to be fact table, but it is defined as a " + tableTypes.at(tt) + " table.";
                __messages += hqed::createErrorString(TableList->Table(i)->TableId(), "", wrng, 1, 3);
            }
        }

        // Jezeli tabela nie ma poloczen przychodzacych, to uwazamy ja za inicjalizacyjna
        if((cilist->size() > 0) && (((Settings->autoTableTypeRecognition()) && (colist->size() == 0)) || ((!Settings->autoTableTypeRecognition()) && (TableList->Table(i)->TableType() == 2))))
        {
            halt_tabele_list->append(TableList->Table(i));
            int tt = TableList->Table(i)->TableType();
            if((false) && (tt != 2) && (Settings->messageAboutAmbiguousTableType()))
            {
                QString wrng = "Table \'" + TableList->Table(i)->Title() + "\' seems to be halt table, but it is defined as a " + tableTypes.at(tt) + " table.";
                __messages += hqed::createErrorString(TableList->Table(i)->TableId(), "", wrng, 1, 3);
            }
        }

        delete colist;
        delete cilist;
    }

    // Sprawdzenie czy sa tabele startowe
    if(fact_tabele_list->size() == 0)
    {
        error = "There is no initialize table.";
        __messages += hqed::createErrorString("", "", error, 1, 2);
        return result;
    }

    // Spawdzenie czy tabele startowe maja przynajmniej jeden wiersz
    for(int i=0;i<fact_tabele_list->size();i++)
    {
        if(fact_tabele_list->at(i)->RowCount() == 0)
        {
            error = "Initial table \'" + fact_tabele_list->at(i)->Title() + "\' hasn't rows. Initial table must have at least one row.";
            __messages += hqed::createErrorString(fact_tabele_list->at(i)->TableId(), "", error, 1, 3);
            return result;
        }
    }

    // Tworzenie list rodzicow (tabel inicjalizacyjnych)
    QList<QString>* _tables = new QList<QString>;				// Lista identyfikatorow tabel
    QList<QList<XTT_Table*>*>* _initsList = new QList<QList<XTT_Table*>*>;	// Lista list tabel bedacych rodzicami danej tabeli, kolejnosc list jest taka sama jak kolejnosc tabel w zmiennej _tables

    for(int i=0;i<fact_tabele_list->size();++i)
        findInitialTables(fact_tabele_list->at(i), _tables, _initsList, fact_tabele_list->at(i)->TableId());

    // Parametry startowe uruchomienia
    int currR = 0;												// Wiersz startowy
    bool* initializeTableFired = new bool[fact_tabele_list->size()];		// Lista wartosci oznaczajaca czy dana tabela inicjalizacyjna byla uruchomiona
    for(int i=0;i<fact_tabele_list->size();++i)
        initializeTableFired[i] = false;

    // Uruchomienie modelu
    for(int t=0;t<fact_tabele_list->size();++t)		// Uruchamianie kolejnych tabel inicjalizacyjnych
        if(!initializeTableFired[t])				// Uruchamiana jest tylko tabela ktora jeszcze nie byla uruchamiana
            if(traceRow(fact_tabele_list, fact_tabele_list->at(t), initializeTableFired, _tables, _initsList, TableList->IndexOfID(fact_tabele_list->at(t)->TableId()), currR, -1, -1, NULL, result, __messages) == -1)
                break;

    // Kasowanie dynamicznie zaalokowanej pamieci
    delete fact_tabele_list;
    delete halt_tabele_list;
    delete _tables;
    for(int i=0;i<_initsList->size();++i)
        delete _initsList->at(i);
    delete _initsList;
    delete initializeTableFired;

    return result;
}
// -----------------------------------------------------------------------------

// #brief start tracing from the row.
//
// #param fact_tabele_list - Pointer to list of initializing tables.
// #param _initalTable - Pointer to current initializing table.
// #param _initials_tables_status - Pointer to status describing activity of the table.
// #param _tables - Pointer to list of table - order by parent list.
// #param _initsList - Pointer to parants of tables.
// #param table_index - Table index that is the begin of analizys.
// #param row_index - Row index that is the begin of analizys.
// #param final_table_index - Index of the final table, -1 go to the final index.
// #param final_row_index - Index of the final row, -1 go to the final row.
// #param _lastConn - Pointer to the last connection that leads to this table.
// #param __rowList - Reference to the pointer to the list of visited rows.
// #param __messages - Reference to the messages string.
// #return 1 - everything is OK, 0 - error, -1 - ambiguity (connections from init table missed tabels waiting for connections), 2 - function reached the row of destination.
int xtt::traceRow(QList<XTT_Table*>* fact_tabele_list, XTT_Table* _initalTable, bool* _initials_tables_status, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, int table_index, int row_index, int final_table_index, int final_row_index, XTT_Connections* /*_lastConn*/, QList<XTT_Row*>*& __rowList, QString& __messages)
{    
    // Zaznaczenie ze dana tabela inicjalizacyjna zostala uruchomiona
    _initials_tables_status[fact_tabele_list->indexOf(_initalTable)] = true;

    // Sprawdzanie czy wchodzimy do tabeli koncowej ponizej wiersza koncowego, jezeli tak to konczymy
    if((table_index == final_table_index) && (row_index >= final_row_index))
        return 2;

    // Przechowuje bledy
    QString error;
    XTT_Table* currT = TableList->Table(table_index);

    // !!!!!!!! Sprawdzamy czy tabela do ktorej wchodzimy ma wszystkich rodzicow uruchomionych !!!!!!!!
    // jezeli nie, to musimy wszystkich uruchomic
    int tio = _tables->indexOf(currT->TableId());	// Index tabeli w zmiennej _tables wyszukiwany aby wiedziec gdzie jest lista jej rodzicow
    int listSize = _initsList->at(tio)->size();		// Liczba rodzicow tabeli do ktorej wchodzimy
    for(int i=0;i<listSize;++i)					// Sprawdzamy kazdego rodzica czy byl uruchamiany
        if(!_initials_tables_status[fact_tabele_list->indexOf(_initsList->at(tio)->at(i))])
            if(traceRow(fact_tabele_list, _initsList->at(tio)->at(i), _initials_tables_status, _tables, _initsList, TableList->IndexOfID(_initsList->at(tio)->at(i)->TableId()), 0, table_index, row_index, NULL, __rowList, __messages) == -1)
                return -1;

    // Analiza kolejnych wierszy
    for(unsigned int current_row=row_index;current_row<TableList->Table(table_index)->RowCount();++current_row)
    {
        // Sprawdzanie czy do wiersza koncowego, jezeli tak to go nie urachamiamy tylko zwracamy prawde
        if((table_index == final_table_index) && ((int)current_row == final_row_index))
            return 2;

        XTT_Row* currR = TableList->Table(table_index)->Row(current_row);
        if(__rowList->indexOf(currR) > -1)       // kiedy dany row jest juz na liscie to znaczy ze byl analizowany jak rowniez wszystko co po nim nastepuje
            return 1;

        // Jezeli nie ma wiersz to go dodajemy
        __rowList->append(currR);

        // Wyszukanie kolejnego polaczenia do odpalenia
        QList<XTT_Connections*>* allConnections = currR->OutConnection();
        QList<XTT_Connections*> toFired = *allConnections;						// Lista polaczen do odpalenia
        delete allConnections;

        for(int c=0;c<toFired.size();++c)
        {
            XTT_Connections* currC = toFired.at(c);
            QString ntid = currC->DesTable();
            QString nrid = currC->DesRow();

            // Wyszukiwanie indexow nastepnego polaczenia
            int ntind = TableList->IndexOfID(ntid);
            if(ntind == -1)
            {
                error = "Error out connection in table ID=\"" + TableList->Table(table_index)->TableId() + "\" row_index=\"" + QString::number(current_row) + ". Destination table not exists.";
                __messages += hqed::createErrorString(TableList->Table(table_index)->TableId(), currR->RowId(), error, 0, 5);
                return 0;
            }

            // Wyszukiwanie indexu wiersza
            int nrind = -1;
            for(unsigned int i=0;i<TableList->Table(ntind)->RowCount();i++)
                if(TableList->Table(ntind)->Row(i)->RowId() == nrid)
                {
                nrind = i;
                break;
            }
            if((nrind == -1) && (!nrid.isEmpty()))
            {
                error = "Error out connection in table ID=\"" + TableList->Table(table_index)->TableId() + "\" row_index=\"" + QString::number(current_row) + "\". Destination row not exists.";
                __messages += hqed::createErrorString(TableList->Table(table_index)->TableId(), currR->RowId(), error, 0, 5);
                return 0;
            }
            if((nrind == -1) && (nrid.isEmpty()))
                nrind = 0;

            // Jezeli wszystko ok to uruchamiamy nastepny wiersz
            traceRow(fact_tabele_list, _initalTable, _initials_tables_status, _tables, _initsList, ntind, nrind, final_table_index, final_row_index, currC, __rowList, __messages);
        }
    }

    return 1;
}
// -----------------------------------------------------------------------------

// #brief Exectues the xtt model.
//
// #param _initstate - Pointer to the initial state, from which the simulation has started
// #return No return value.
void xtt::run(XTT_State* _initstate)
{
    MainWin->xtt_runModelMessagesDockWindowMessagesList->deleteAll();

    QString error;
    QStringList tableTypes;
    tableTypes << "fact" << "middle" << "halt";

    // Sprawdzanie czy mozna cos uruchomic
    if(TableList->Count() == 0)
    {
        MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Model is not executable.", 2);
        QMessageBox::warning(NULL, "HQEd", "Model is not executable.");
        return;
    }

    // Wyszukanie wszystkich tabel ktore nie maja polaczen
    QList<XTT_Table*>* fact_tabele_list = new QList<XTT_Table*>;
    QList<XTT_Table*>* halt_tabele_list = new QList<XTT_Table*>;
    for(unsigned int i=0;i<TableList->Count();i++)
    {
        QList<XTT_Connections*>* cilist = TableList->Table(i)->inConnectionsList();
        QList<XTT_Connections*>* colist = TableList->Table(i)->outConnectionsList();

        // Jezeli tabela nie ma poloczen przychodzacych, to uwazamy ja za inicjalizacyjna
        if(((Settings->autoTableTypeRecognition()) && (cilist->size() == 0)) || ((!Settings->autoTableTypeRecognition()) && (TableList->Table(i)->TableType() == 0)))
        {
            fact_tabele_list->append(TableList->Table(i));
            int tt = TableList->Table(i)->TableType();
            if((false) && (tt != 0) && (Settings->messageAboutAmbiguousTableType()))
            {
                QString wrng = "Table \'" + TableList->Table(i)->Title() + "\' seems to be fact table, but it is defined as a " + tableTypes.at(tt) + " table.";
                //QMessageBox::warning(NULL, "XTT_Editor", wrng);
                MainWin->xtt_runModelMessagesDockWindowMessagesList->add(TableList->Table(i)->TableId(), "", "", wrng, 1);
            }
        }

        // Jezeli tabela nie ma poloczen przychodzacych, to uwazamy ja za inicjalizacyjna
        if((cilist->size() > 0) && (((Settings->autoTableTypeRecognition()) && (colist->size() == 0)) || ((!Settings->autoTableTypeRecognition()) && (TableList->Table(i)->TableType() == 2))))
        {
            halt_tabele_list->append(TableList->Table(i));
            int tt = TableList->Table(i)->TableType();
            if((false) && (tt != 2) && (Settings->messageAboutAmbiguousTableType()))
            {
                QString wrng = "Table \'" + TableList->Table(i)->Title() + "\' seems to be halt table, but it is defined as a " + tableTypes.at(tt) + " table.";
                //QMessageBox::warning(NULL, "XTT_Editor", wrng);
                MainWin->xtt_runModelMessagesDockWindowMessagesList->add(TableList->Table(i)->TableId(), "", "", wrng, 1);
            }
        }

        // Sprawdzanie niedostepnosci wierszy
        int fcri = TableList->Table(i)->firstConnectionRowIndex();
        for(int x=0;x<fcri;x++)
            MainWin->xtt_runModelMessagesDockWindowMessagesList->add(TableList->Table(i)->TableId(), TableList->Table(i)->Row(x)->RowId(), "", "inaccessible row.", 1);

        delete colist;
        delete cilist;
    }

    // Sprawdzenie czy sa tabele startowe
    if(fact_tabele_list->size() == 0)
    {
        error = "There is no initialize table.";
        MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", error, 2);
        QMessageBox::warning(NULL, "HQEd", error);
        return;
    }

    // Spawdzenie czy tabele startowe maja przynajmniej jeden wiersz
    for(int i=0;i<fact_tabele_list->size();i++)
    {
        if(fact_tabele_list->at(i)->RowCount() == 0)
        {
            error = "Initial table \'" + fact_tabele_list->at(i)->Title() + "\' hasn't rows. Initial table must have at least one row.";
            MainWin->xtt_runModelMessagesDockWindowMessagesList->add(fact_tabele_list->at(i)->TableId(), "", "", error, 0);
            QMessageBox::warning(NULL, "HQEd", error);
            return;
        }
    }

    // Zmiana kursora aplikacji i dorzucenie okna dokowalnego
    QApplication::setOverrideCursor(Qt::WaitCursor);

    // Tworzenie list rodzicow (tabel inicjalizacyjnych)
    QList<QString>* _tables = new QList<QString>;				// Lista identyfikatorow tabel
    QList<QList<XTT_Table*>*>* _initsList = new QList<QList<XTT_Table*>*>;	// Lista list tabel bedacych rodzicami danej tabeli, kolejnosc list jest taka sama jak kolejnosc tabel w zmiennej _tables

    for(int i=0;i<fact_tabele_list->size();++i)
        findInitialTables(fact_tabele_list->at(i), _tables, _initsList, fact_tabele_list->at(i)->TableId());

    // Wyswietlenie mapy w okienku dokowalnym
    if(Settings->createMapMovement())
    {
        MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "------------------------ Initialize tables list ------------------------", 2);
        for(int t=0;t<_tables->size();++t)
        {
            int ti = TableList->IndexOfID(_tables->at(t));
            if(ti == -1)
            {
                error = "Creating initialize list error.";
                MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", error, 0);
                QMessageBox::warning(NULL, "HQEd", error);
            }

            if(ti > -1)
            {
                QString line = ""; //"Table " + TableList->Table(ti)->Title() + ": ";
                for(int p=0;p<_initsList->at(t)->size();++p)
                    line += _initsList->at(t)->at(p)->Title() + ", ";
                MainWin->xtt_runModelMessagesDockWindowMessagesList->add(_tables->at(t), "", "", line, 2);
            }
        }
        MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "-----------------------------------------------------------------", 2);
    }

    // Parametry startowe uruchomienia
    int currR = 0;												// Wiersz startowy
    bool* initializeTableFired = new bool[fact_tabele_list->size()];		// Lista wartosci oznaczajaca czy dana tabela inicjalizacyjna byla uruchomiona
    for(int i=0;i<fact_tabele_list->size();++i)
        initializeTableFired[i] = false;

    // Uruchomienie modelu
    for(int t=0;t<fact_tabele_list->size();++t)		// Uruchamianie kolejnych tabel inicjalizacyjnych
        if(!initializeTableFired[t])				// Uruchamiana jest tylko tabela ktora jeszcze nie byla uruchamiana
            if(ExecuteRow(fact_tabele_list, fact_tabele_list->at(t), initializeTableFired, _tables, _initsList, TableList->IndexOfID(fact_tabele_list->at(t)->TableId()), currR, -1, -1, NULL, _initstate) == -1)
                break;

    QApplication::restoreOverrideCursor();

    if(_initstate != NULL)
        OutputStates->refreshStatesUI();

    // Kasowanie dynamicznie zaalokowanej pamieci
    delete fact_tabele_list;
    delete halt_tabele_list;
    delete _tables;
    for(int i=0;i<_initsList->size();++i)
        delete _initsList->at(i);
    delete _initsList;

    if(Settings->beepOnFinish())
        QApplication::beep();
}
// -----------------------------------------------------------------------------

// #brief Execute row.
//
// #param fact_tabele_list Pointer to list of initializing tables.
// #param _initalTable Pointer to current initializing table.
// #param _initials_tables_status Pointer to status describing activity of the table.
// #param _tables Pointer to list of table - order by parent list.
// #param _initsList Pointer to parants of tables.
// #param table_index Table index that is the begin of analizys.
// #param row_index Row index that is the begin of analizys.
// #param final_table_index Index of the final table, -1 go to the final index.
// #param final_row_index Index of the final row, -1 go to the final row.
// #param _lastConn Pointer to the last connection that leads to this table.
// #param _initstate - Pointer to the initial state, from which the simulation has started
// #return 1 - everything is OK, 0 - error, -1 - ambiguity (connections from init table missed tabels waiting for connections), 2 - function reached the row of destination.
int xtt::ExecuteRow(QList<XTT_Table*>* fact_tabele_list, XTT_Table* _initalTable, bool* _initials_tables_status, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, int table_index, int row_index, int final_table_index, int final_row_index, XTT_Connections* /*_lastConn*/, XTT_State* _initstate)
{
    // Zaznaczenie ze dana tabela inicjalizacyjna zostala uruchomiona
    _initials_tables_status[fact_tabele_list->indexOf(_initalTable)] = true;

    // Sprawdzanie czy wchodzimy do tabeli koncowej ponizej wiersza koncowego, jezeli tak to konczymy
    if((table_index == final_table_index) && (row_index >= final_row_index))
        return 2;

    // Przechowuje bledy
    QString error;
    QString strResults[3] = {"error", "false", "true"};
    XTT_Table* currT = TableList->Table(table_index);

    // !!!!!!!! Sprawdzamy czy tabela do ktorej wchodzimy ma wszystkich rodzicow uruchomionych !!!!!!!!
    // jezeli nie, to musimy wszystkich uruchomic
    int tio = _tables->indexOf(currT->TableId());	// Index tabeli w zmiennej _tables wyszukiwany aby wiedziec gdzie jest lista jej rodzicow
    int listSize = _initsList->at(tio)->size();		// Liczba rodzicow tabeli do ktorej wchodzimy
    for(int i=0;i<listSize;++i)					// Sprawdzamy kazdego rodzica czy byl uruchamiany
        if(!_initials_tables_status[fact_tabele_list->indexOf(_initsList->at(tio)->at(i))])
            if(ExecuteRow(fact_tabele_list, _initsList->at(tio)->at(i), _initials_tables_status, _tables, _initsList, TableList->IndexOfID(_initsList->at(tio)->at(i)->TableId()), 0, table_index, row_index, NULL, _initstate) == -1)
                return -1;

    // Analiza kolejnych wierszy
    for(unsigned int current_row=row_index;current_row<TableList->Table(table_index)->RowCount();current_row++)
    {
        // Sprawdzanie czy do wiersza koncowego, jezeli tak to go nie urachamiamy tylko zwracamy prawde
        if((table_index == final_table_index) && ((int)current_row == final_row_index))
            return 2;

        bool continue_main_loop = false;
        XTT_Row* currR = TableList->Table(table_index)->Row(current_row);

        // Uruchomienie analizy
        for(unsigned int c=0;c<currR->Length();c++)
        {
            XTT_Attribute* ctxattr = currT->ContextAtribute(c);

            // Trzeba tutaj zmienic kursor, poniewaz w momencie proby zapisania blednego wyrazenia
            // kursor bedzie dalej zajety
            QApplication::restoreOverrideCursor();
            int res = currR->Cell(c)->Execute();
            QApplication::setOverrideCursor(Qt::WaitCursor);

            // Jezeli sporzadzamy raport w postaci logu uruchomienia
            if(Settings->showRunCellStatus())
            {
                int msgType = res==-1?0:2;
                QString message = "executing cell(" + currT->Title() + "," + QString::number(current_row+1) + "," + QString::number(c+1) + ") - return code: " + strResults[1+res];
                if(res == -1)
                    message += ": " + currR->Cell(c)->CellError(false);
                MainWin->xtt_runModelMessagesDockWindowMessagesList->add(currT->TableId(), currR->RowId(), currR->Cell(c)->CellId(), message, msgType);
            }

            // Jezeli kontekst decyzyjny to robimy update stanu inicjalizujacego
            if((_initstate != NULL) && (currT->ColContext(c) == 4))
            {
                if((c == (currR->Length()-1)) || (currT->ColContext(c+1) == 5))
                    _initstate->trajectory()->append(currR);
                if(ctxattr->Class() == XTT_ATT_CLASS_WO)     // update wartosci atrybutu
                {
                    hMultipleValueResult mvr = ctxattr->Value()->value();
                    _initstate->setOutputValueAt(ctxattr, mvr.toSet(true));
                }
            }

            // Jezeli false w kontekscie conditional
            if((res == 0) && (currT->ColContext(c) == 1))
            {
                c = currR->Length();
                continue_main_loop = true;
            }
            if(res == -1)
            {
                QApplication::restoreOverrideCursor();
                error = "Error cell content ID=\"" + TableList->Table(table_index)->TableId() + "\" row_index=\"" + QString::number(current_row) + "\" col_index=\"" + QString::number(c) + "\".\nError maessage: " + currR->Cell(c)->CellError();

                // Kiedy trzeba pokazac informacje
                if(Settings->xttShowErrorCellMessage())
                    QMessageBox::critical(NULL, "HQEd", error);

                TableList->Table(table_index)->GraphicTable()->setTableSelection(true);
                return 0;
            }
        }

        if(continue_main_loop)
            continue;

        // Wyszukanie kolejnego polaczenia do odpalenia
        QList<XTT_Connections*>* allConnections = currR->OutConnection();
        QList<XTT_Connections*> toFired = *allConnections;						// Lista polaczen do odpalenia
        delete allConnections;

        /* This commented part concern on multi-out-connections that goes out from one row */
        /* It is still heree if return to this idea */

        /*QList<XTT_Connections*>* notLabeledConnections = currR->OutConnection("");	// Lista polaczen nie etykietowanych wychodzacych z tego wiersza
		QList<XTT_Connections*>* labeledConnections = NULL;						// Lista polaczen etykietowanych wychodzacych z tego wiersza
		
		// Jezeli ostanie polaczenie nie bylo NULL, to sprawdzamy czy mialo jakas etykiete
		if((_lastConn != NULL) && (_lastConn->Label() != ""))
			labeledConnections = currR->OutConnection(_lastConn->Label());
			
		// Jezeli ostanie polaczenie nie bylo NULL oraz nie mialo etykiety, to wyszukujemy wszystkie polaczenia etykietowane
		if((_lastConn != NULL) && (_lastConn->Label() == ""))
			labeledConnections = currR->OutLabeledConnection();
			
		// Jezeli ostanie polaczenie bylo NULL, to z wiersza moze wyjsc takze polaczenie etykietowane
		if(_lastConn == NULL)
			labeledConnections = currR->OutLabeledConnection();
			
		// === Tworzenie listy polaczen do odpalenia ===

		// Gdy wiekszy priorytet maja polaczenia nieetykietowane
		if(Settings->xttConnectionPriority() == USE_NOT_LABELED_FIRST)
		{	
			// Graniczne wartosci iteracji petli
			int _start1 = 0;
			int _start2 = 0;
			int _end1 = (notLabeledConnections->size() == 0)? 0:1;
			int _end2 = ((labeledConnections == NULL) || (labeledConnections->size() == 0))? 0:1;
			
			// poprawa ganicznych wartosci w zaleznsoci od opcji
			if(Settings->xttAmbiguousConnectionPolicy() == USE_ALL)
				_end1 = notLabeledConnections->size();

			// Kopiowanie wskaznikow do polaczen
			for(int i=_start1;i<_end1;++i)
				toFired.append(notLabeledConnections->at(i));
				
			// Kopiowanie polaczen z drugiej listy
			if((Settings->xttAmbiguousConnectionPolicy() == USE_ALL)		// Gdy uzywamy wszystkich polaczen 
			|| 
			(toFired.size() == 0) && (labeledConnections != NULL))			// Albo kiedy nie ma polaczen nieeykietowanych a sa etykietowane
			{
				if(Settings->xttAmbiguousConnectionPolicy() == USE_ALL)
					_end2 = labeledConnections->size();
					
				// Kopiowanie wskaznikow do polaczen
				for(int i=_start2;i<_end2;++i)
					toFired.append(labeledConnections->at(i));
			}
		}
		
		// Gdy wiekszy priorytet maja polaczenia etykietowane
		if(Settings->xttConnectionPriority() == USE_LABELED_FIRST)
		{	
			// Graniczne wartosci iteracji petli
			int _start1 = 0;
			int _start2 = 0;
			int _end1 = ((labeledConnections == NULL) || (labeledConnections->size() == 0))? 0:1;
			int _end2 = (notLabeledConnections->size() == 0)? 0:1;
			
			// poprawa ganicznych wartosci w zaleznsoci od opcji
			if((Settings->xttAmbiguousConnectionPolicy() == USE_ALL) && (labeledConnections != NULL))
				_end1 = labeledConnections->size();

			// Kopiowanie wskaznikow do polaczen
			for(int i=_start1;i<_end1;++i)
				toFired.append(labeledConnections->at(i));
				
			// Kopiowanie polaczen z drugiej listy
			if((Settings->xttAmbiguousConnectionPolicy() == USE_ALL)		// Gdy uzywamy wszystkich polaczen 
			|| 
			(toFired.size() == 0))									// Albo kiedy nie ma polaczen eykietowanych a sa nieetykietowane
			{
				if(Settings->xttAmbiguousConnectionPolicy() == USE_ALL)
					_end2 = notLabeledConnections->size();
					
				// Kopiowanie wskaznikow do polaczen
				for(int i=_start2;i<_end2;++i)
					toFired.append(notLabeledConnections->at(i));
			}
		}
		
		delete notLabeledConnections;
		delete labeledConnections;*/

        for(int c=0;c<toFired.size();++c)
        {
            XTT_Connections* currC = toFired.at(c);
            QString ntid = currC->DesTable();
            QString nrid = currC->DesRow();

            // Wyszukiwanie indexow nastepnego polaczenia
            int ntind = TableList->IndexOfID(ntid);
            if(ntind == -1)
            {
                QApplication::restoreOverrideCursor();
                error = "Error out connection in table ID=\"" + TableList->Table(table_index)->TableId() + "\" row_index=\"" + QString::number(current_row) + ". Destination table not exists.";

                // Kiedy trzeba pokazac informacje
                if(Settings->xttShowErrorCellMessage())
                    QMessageBox::critical(NULL, "HQEd", error);

                TableList->Table(table_index)->GraphicTable()->setTableSelection(true);
                return 0;
            }

            // Wyszukiwanie indexu wiersza
            int nrind = -1;
            for(unsigned int i=0;i<TableList->Table(ntind)->RowCount();i++)
                if(TableList->Table(ntind)->Row(i)->RowId() == nrid)
                {
                nrind = i;
                break;
            }
            if((nrind == -1) && (!nrid.isEmpty()))
            {
                QApplication::restoreOverrideCursor();
                error = "Error out connection in table ID=\"" + TableList->Table(table_index)->TableId() + "\" row_index=\"" + QString::number(current_row) + "\". Destination row not exists.";

                // Kiedy trzeba pokazac informacje
                if(Settings->xttShowErrorCellMessage())
                    QMessageBox::critical(NULL, "HQEd", error);

                TableList->Table(table_index)->GraphicTable()->setTableSelection(true);
                return 0;
            }
            if((nrind == -1) && (nrid.isEmpty()))
                nrind = 0;

            // Jezeli wszystko ok to uruchamiamy nastepny wiersz
            int execRes = ExecuteRow(fact_tabele_list, _initalTable, _initials_tables_status, _tables, _initsList, ntind, nrind, final_table_index, final_row_index, currC, _initstate);
            if(execRes != 1)
                return execRes;
            if(currC->IsCut())
                return 1;
        }
    }

    // Jezeli mamy aktywne ograniczenia, czyli -> (final_table_index != -1) && (final_row_index != -1)
    // to to wywolanie funckji moze sie skonczyc w tym miejscu tylko wtedy kiedy table_index
    // jest tabela poprzedajaca final_table_index. Mozemy to sprawdzic sprawdzajac listy rodzicow
    // jezeli tabele final_table_index i table_index maja chociaz jednego wspolnego rodzica
    // to nastapilo ominiecie synchronizacji. Dzieje sie tak poniewaz przy poprawnej synchronizacji
    // jezeli tabele maja wspolnego rodzica to wtedy final_table_index = table_index, a ten przypadek
    // jest dobrze rozpatrony i w kazdym wypadku konczy sie wczesniej
    if((final_table_index != -1) && (final_row_index != -1))
    {
        XTT_Table* final_table = TableList->Table(final_table_index);
        XTT_Table* current_table = TableList->Table(table_index);
        int fti = _tables->indexOf(final_table->TableId());
        int cti = _tables->indexOf(current_table->TableId());

        // Sprawdzanie rodzicow
        for(int i=0;i<_initsList->at(fti)->size();++i)
            if(_initsList->at(cti)->indexOf(_initsList->at(fti)->at(i)) > -1)
            {
            error = "Arrested connection - synchronization error: this row wasn't been executed.";
            MainWin->xtt_runModelMessagesDockWindowMessagesList->add(final_table->TableId(), final_table->Row(final_row_index)->RowId(), "", error, 0);
            return -1;
        }
    }

    return 1;
}
// -----------------------------------------------------------------------------
