 /**
 * \file	ns_xtt.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	29.12.2008 
 * \version	1.0
 * \brief	This file contains hqed namespace declarations and functions belong to this namespace
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef NS_XTTH
#define NS_XTTH
// -----------------------------------------------------------------------------

#include <QString>
#include <QStringList>
// -----------------------------------------------------------------------------

class XTT_State;
class XTT_Table;
class XTT_Connections;
class MyGraphicsScene;
// -----------------------------------------------------------------------------

#include "../M/XTT/XTT_Table.h"
#include "../M/XTT/XTT_Connections.h"
// -----------------------------------------------------------------------------

namespace xtt
{
     /// \brief Function that returns a pointer to the scene where XTT model is displayed
     ///
     /// \return pointer to the scene where XTT model is displayed
     MyGraphicsScene* modelScene(void);
     
     /// \brief Function that returns a pointer to the scene where XTT states are displayed
     ///
     /// \return pointer to the scene where XTT states are displayed
     MyGraphicsScene* stateScene(void);
     
     /// \brief Function that returns if the model is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool modelVerification(QString& _error_message);
     
     /// \brief Function that returns if the types objects are correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool typesVerification(QString& _error_message);
     
     /// \brief Function that returns if the table objects is correct on the logical level
     ///
     /// \param __table - pointer to verified table
     /// \param __error_message - the reference to the error message(s)
     /// \param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalTableVerification(XTT_Table* __table, QString& _error_message, bool __display_messages = true);
     
     /// \brief Function that returns if the table objects is correct on the logical level
     ///
     /// \param __schema - title of verified table
     /// \param __error_message - the reference to the error message(s)
     /// \param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalTableVerification(QString __schema, QString& _error_message, bool __display_messages = true);
     
     /// \brief Function that returns if the table objects is correct on the logical level
     ///
     /// \param __table - pointer to verified table
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalTableVerification(XTT_Table* __table);
     
     /// \brief Function that returns if the table objects is correct on the logical level
     ///
     /// \param __schema - title of verified table
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalTableVerification(QString __schema);
     
     /// \brief Function that returns if the model is correct on the logical level
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalFullVerification(QString& _error_message, bool __display_messages = true);
     
     /// \brief Function that returns if the model is correct on the logical level
     ///
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalFullVerification(void);
     
     /// \brief Function that returns result of system verification
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \param __display_messages - denotes if the errors that appears during the verification must be displayed in the errorWidget in HQed
     /// \return true if yes otherwise false
     bool fullVerification(QString& _error_message, bool __display_messages = true);
     
     /// \brief Function that returns result of system verification
     ///
     /// \return true if yes otherwise false
     bool fullVerification(void);
     
     /// \brief Function that removes all the XTT items
     ///
     /// \param __keepAttributes - indicates if the attributes must be also removed
     /// \return No return value.
     void clear(bool __keepAttributes);
     
     /// \brief Traces the model execution.
     ///
     /// \param __messages - Reference to the messages string.
     /// \return the list of pointers to rows can be executed.
     QList<XTT_Row*>* trace(QString& __messages);
     
     /// \brief start tracing from the row.
	///
	/// \param fact_tabele_list - Pointer to list of initializing tables.
	/// \param _initalTable - Pointer to current initializing table.
	/// \param _initials_tables_status - Pointer to status describing activity of the table.
	/// \param _tables - Pointer to list of table - order by parent list.
	/// \param _initsList - Pointer to parants of tables.
	/// \param table_index - Table index that is the begin of analizys.
	/// \param row_index - Row index that is the begin of analizys.
	/// \param final_table_index - Index of the final table, -1 go to the final index.
	/// \param final_row_index - Index of the final row, -1 go to the final row.
	/// \param _lastConn - Pointer to the last connection that leads to this table.
	/// \param __rowList - Reference to the pointer to the list of visited rows.
	/// \param __messages - Reference to the messages string.
	/// \return 1 - everything is OK, 0 - error, -1 - ambiguity (connections from init table missed tabels waiting for connections), 2 - function reached the row of destination.
     int traceRow(QList<XTT_Table*>* fact_tabele_list, XTT_Table* _initalTable, bool* _initials_tables_status, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, int table_index, int row_index, int final_table_index, int final_row_index, XTT_Connections* _lastConn, QList<XTT_Row*>*& __rowList, QString& __messages);
     
     /// \brief Exectues the xtt model.
     ///
     /// \param _initstate Pointer to the initial state, from which the simulation has started
     /// \return No return value.
     void run(XTT_State* _initstate = NULL);
     
     /// \brief Execute row.
	///
	/// \param fact_tabele_list Pointer to list of initializing tables.
	/// \param _initalTable Pointer to current initializing table.
	/// \param _initials_tables_status Pointer to status describing activity of the table.
	/// \param _tables Pointer to list of table - order by parent list.
	/// \param _initsList Pointer to parants of tables.
	/// \param table_index Table index that is the begin of analizys.
	/// \param row_index Row index that is the begin of analizys.
	/// \param final_table_index Index of the final table, -1 go to the final index.
	/// \param final_row_index Index of the final row, -1 go to the final row.
	/// \param _lastConn Pointer to the last connection that leads to this table.
	/// \param _initstate Pointer to the initial state, from which the simulation has started
	/// \return 1 - everything is OK, 0 - error, -1 - ambiguity (connections from init table missed tabels waiting for connections), 2 - function reached the row of destination.
     int ExecuteRow(QList<XTT_Table*>* fact_tabele_list, XTT_Table* _initalTable, bool* _initials_tables_status, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, int table_index, int row_index, int final_table_index, int final_row_index, XTT_Connections* _lastConn, XTT_State* _initstate);
     
     /// \brief Finde list of tables connected to the table.
	///
	/// \param _initalTable Pointer to current init table.
	/// \param _tables Pointer to list of tabels id that have at least one parent.
	/// \param _initsList Pointer to list of pointer to parent table list.
	/// \param _currentTableID Current table id.
	/// \return No return value.
	void findInitialTables(XTT_Table* _initalTable, QList<QString>* _tables, QList<QList<XTT_Table*>*>* _initsList, QString _currentTableID);
}
// -----------------------------------------------------------------------------

#endif
