 /**
 * \file	hExpression.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	10.09.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the HeKatE expression type
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HEXPRESSIONH
#define HEXPRESSIONH

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QList>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define H_EXPRESSION_SUCCES                           0
#define H_EXPRESSION_ERROR_OTHER_OBJECT_ERROR        -1
#define H_EXPRESSION_ERROR_NOT_DEFINED_FUNCTION      -2
#define H_EXPRESSION_ERROR_NOT_DEFINED_REQUIRED_TYPE -3
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hValue;
class hSetItem;
class hFunction;
class hFunctionList;
class hMultipleValue;
class hFunctionArgument;
class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class 	hExpression
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	10.09.2008 
* \brief 	Class definition that represents the HeKatE expression type
*/
class hExpression
{
private:
     
     bool _memberOfDomain;                   ///< Denotes if the result of the expression is a member of the domain
     hType* _requiredType;                   ///< The required type of result of the function
     hFunction* _function;                   ///< Pointer to the function object
     
     QString _lastError;                     ///< The last error message
     
     /// \brief The function that prepare the function to call by setting the arguments, required type, ...
     ///
     /// \return the result of function call
     int prepareFunction(void);

public:

     /// \brief Default constructor hFunction class.
     hExpression(void);
     
     /// \brief Constructor hFunction class.
     ///
     /// \param *__required_type - poiter to the object type that is required
     hExpression(hType* __required_type);
     
     /// \brief Default destructor hFunction class.
     ~hExpression(void);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     hExpression* copyTo(hExpression* __destinationItem = NULL);
     
     /// \brief Calls the given function with given arguments
     ///
     /// \return the result of function call
     hMultipleValue* calculate(void);
     
     /// \brief Function that returns the string that is the user representation of the expression
     ///
     /// \return the string that is the user representation of the expression
     QString toString(void);

     /// \brief Function gets all data in drools5 format.
     ///
     /// \return the expression in drools5 format
     QString toDrools5(void);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     // -----------------------
     
     /// \brief Function that returns the pointer to the function object
     ///
     /// \return returns the pointer to the function object
     hFunction* function(void);
     
     /// \brief Function if the result of this expression is a member of domain
     ///
     /// \return if the result of this expression is a member of domain (true), otherwise false
     bool isMemberOfDomain(void);
     
     /// \brief Function that returns the last error message
     ///
     /// \return returns the last error message as string
     QString lastError(void);
     
     /// \brief Function that returns the pointer to the required type
     ///
     /// \return the pointer to the required type
     hType* requiredType(void);
     
     // -----------------------
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_content - last error message
     /// \return no return value
     void setLastError(QString __error_content);
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_code - code of the error
     /// \param __error_desc - the additional error description
     /// \return returns the given error code
     int setLastError(int __error_code, QString __error_desc = "");
     
     /// \brief Function that sets the pointer to the function object
     ///
     /// \param *__function - pointer to the function
     /// \return no value return
     void setFunction(hFunction* __function);
     
     /// \brief Function that sets the pointer to the required type
     ///
     /// \param *__req_type - pointer to the required type
     /// \return true on succes otherwise false
     bool setRequiredType(hType* __req_type);
     
     /// \brief Function that sets the argument count
     ///
     /// \param __arg_count
     /// \return true if success otherwise false
     bool setArgumentCount(int __arg_count);
     
     /// \brief Function that sets the status of being a member of domain
     ///
     /// \param __mod - new value of the status
     /// \return no value return
     void setAsMemberOfDomain(bool __mod);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that returns if the expression objest definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool expressionVerification(QString& __error_message);

     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
     /// \param __cellExpression - denotes if this object represents the whole cell expression
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression = false);
};
// -----------------------------------------------------------------------------
#endif
