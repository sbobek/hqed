 /**
 * \file	hFunction.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	9.09.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the HeKatE function abstract type
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HFUNCTION
#define HFUNCTION

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QList>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

// The function errors codes
#define FUNCTION_ERROR_STRING                  "__FUNC_ERROR__: "
#define FUNCTION_ERROR_SUCCESS                 0
#define FUNCTION_ERROR_UNDEFINED_PARAMS       -1
#define FUNCTION_ERROR_ARG_CNT_OUT_OF_RANGE   -2
#define FUNCTION_ERROR_INCONSISTENT_TYPES     -3
#define FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE -4
#define FUNCTION_ERROR_INCONSISTENT_ARG_TYPES -5

// -----------------------------------------------------------------------------

// Each argument should have defined type. The type consist of two parts:
// 1. Type definition
// 2. Mulitiplicity definition, that denotes if the argument can has only single or also multiple value from given type
// For example:
// Integer argument that can has more than one value i defined as: FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__MULTI_VALUED
// -----------------------------------------------------------------------------

// The result types
#define FUNCTION_RESULT_TYPE_NOT_DEFINED  0x00000000
#define FUNCTION_TYPE__SINGLE_VALUED      0x10000000
#define FUNCTION_TYPE__MULTI_VALUED       0x20000000

// The values of types that each function can return
#define FUNCTION_TYPE__NOT_DEFINED 0x00000000
#define FUNCTION_TYPE__BOOLEAN     0x00000001
#define FUNCTION_TYPE__INTEGER     0x00000002
#define FUNCTION_TYPE__NUMERIC     0x00000004
#define FUNCTION_TYPE__UOSYMBL     0x00000008
#define FUNCTION_TYPE__OSYMBL      0x00000010
#define FUNCTION_TYPE__VOID        0x00000020          // This type is used in decision context only
#define FUNCTION_TYPE__ACTION      0x00000040          // This type is used in decision context only
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hFunctionList;
class hMultipleValue;
class hFunctionArgument;
class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class 	hFunction
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	9.09.2008 
* \brief 	Class definition that represents the HeKatE function abstract type
*/
class hFunction
{
protected:

     bool _verifyResult;                ///< Denotes if the result of the calculation should be verified with _requiredType constraints
     hType* _requiredType;              ///< The pointer to the required type
     int _resultType;                   ///< Denotes if the result that is returned by this function is single or multiple valued
     int _returnedTypes;                ///< The value that denotes what types can be returned by function
     int _minArgCount;                  ///< The minimal number of arguments
     int _maxArgCount;                  ///< The maximal numbero of arguments. When less than zero then field should be ignored - no maximal number of attributes.
     int _additionalArgsType;           ///< The type of attributes that their type is not defined in Constructor

     QList<hFunctionArgument*>* _args;  ///< Pointer to the list of arguments
     QList<int> _argTypesConf;          ///< The configuration of argument types
     QList<int> _argMltplConf;          ///< The configuration of argument multiplicity
     QStringList _argNames;             ///< The list of arguments names
     
     QString _xmlRepresentation;        ///< The XML representation of the function
     QString _prologRepresentation;     ///< The PROLOG representation of the function
     QString _drools5Representation;    ///< The Drools5 representation of the function
     QString _userRepresentation;       ///< The user representation of the function
     QString _internalRepresentation;   ///< The internal representation of the function
     QString _operatorRepresentation;   ///< The operator representation of the function
     QString _description;              ///< The description of the function
     
     QString _lastError;                ///< The value of the last error

     bool _isOperator;                  ///< True if function represents operator
     bool _operatorLeftAssociative;     ///< True if when function represents operator that is left-associative (if applicable)
     int _operatorPrecedence;           ///< Operator precedence (if applicable)
     
     /// \brief Function that sets up the internal parameters of the function
     ///
     /// \return no value returns
     virtual void setupFunction(void) = 0;
     
public:

     /// \brief Constructor hFunction class.
     hFunction(void);
     
     /// \brief Destructor hFunction class.
     virtual ~hFunction(void);
     
     // -----------------
     
     /// \brief Function that sets the value of the required type.
     ///
     /// \param __required_type - the pointer to the required type
     /// \return the result of compatybility this function with required type
     virtual int setRequiredType(hType* __required_type);
     
     /// \brief Function that appends the current object to the list of functions
     ///
     /// \param __flist - the destination container
     /// \return the result of registration as integer
     virtual int registerFunction(hFunctionList* __flist);
     
     /// \brief Function that creates the new instance of the function object
     ///
     /// \param __destination - poinyter to the destination object
     /// \return pointer to the new instance of function object
     virtual hFunction* copyTo(hFunction* __destination = NULL);
     
     /// \brief Function that sets the argument for the function
     ///
     /// \param __arg_index - the index of argument
     /// \param hFunctionArgument* __arg - the pointer to the argument
     /// \return success code if all is ok otherwise error code
     virtual int setArgument(int __arg_index, hFunctionArgument* __arg);
     
     /// \brief Function that sets the number of arguments
     ///
     /// \param __arg_count - the number of the arguments
     /// \return the result of compatybility this function with the number of arguments
     int setArgCount(int __arg_count);
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_content - last error message
     /// \return no return value
     void setLastError(QString __error_content);
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_code - code of the error
     /// \param __error_desc - the additional error description
     /// \return returns the given error code
     int setLastError(int __error_code, QString __error_desc = "");
     
     /// \brief Function that sets the status of calculation result verification
     ///
     /// \param __rvs - the new value of the status
     /// \return no value returns
     void setResultVerificationStatus(bool __rvs);
     
     /// \brief Function that refreshes the names of the arguments
     ///
     /// \return no value returns
     void refreshArgumentsNames(void);

     // -----------------
     
     /// \brief Function that performs the compatible types lists for each argument
     ///
     /// \return no value return
     void refreshArgumentCompatibleTypesLists(void);
     
     /// \brief Function that returns the minimal value of the arguments
     ///
     /// \return the minimal value of the arguments
     int minArgCount(void);
     
     /// \brief Function that returns the maximal value of the arguments
     ///
     /// \return the maximal value of the arguments
     int maxArgCount(void);
     
     /// \brief Function that returns the current number of arguments
     ///
     /// \return the current number of arguments
     int argCount(void);
     
     /// \brief Function that returns the function characterisitics (result type and mulitiplicity)
     ///
     /// \return the function characterisitics (result type and mulitiplicity) as integer
     int functionCharacterisitics(void);
     
     /// \brief Function that returns the value that denotes the type of the result
     ///
     /// \return the value that denotes the type of the result
     int resultType(void);
     
     /// \brief Function that returns the value that denotes what types can be returned by function
     ///
     /// \return the value is a integer type. If less than zero then error.
     int returnedTypes(void);
     
     /// \brief Function that returns the value (without multiplicity) that indicates which types the given argument can have
     ///
     /// \param __argument - the argument index
     /// \return value (without multiplicity) that indicates which types the given argument can have as integer
     int argTypeConf(int __argument);

     /// \brief Function that returns the number of items in argument type configuration list
     ///
     /// \return number of items in argument type configuration list.
     int argTypeConfCount();
     
     /// \brief Function that returns the multiplicity which argument can have
     ///
     /// \param __argument - the argument index
     /// \return multiplicity which argument can have as integer
     int argMltplConf(int __argument);
     
     /// \brief Function that returns the pointer to the required type returned by the function
     ///
     /// \return the pointer to the required type returned by the function
     hType* requiredType(void);
     
     /// \brief Function that return the status of calculation result verification
     ///
     /// \return status of calculation result verification as boolean value
     bool resultVerificationStatus(void);
     
     /// \brief Function that returns the XML representation of the function
     ///
     /// \return the XML representation of the function as string
     QString xmlRepresentation(void);
     
     /// \brief Function that returns the PROLOG representation of the function
     ///
     /// \return the PROLOG representation of the function as string
     QString prologRepresentation(void);
     
     /// \brief Function that returns the user representation of the function
     ///
     /// \return the user representation of the function as string
     QString userRepresentation(void);
     
     /// \brief Function that returns the internal representation of the function
     ///
     /// \return the internal representation of the function as string
     QString internalRepresentation(void);

     /// \brief Function that returns operator representation of the function
     ///
     /// \return operator representation of the function
     QString operatorRepresentation(void);
     
     /// \brief Function that returns the description of the function object
     ///
     /// \return the description of the function object as string
     QString description(void);
     
     /// \brief Function that returns the name of the argument
     ///
     /// \param __arg_index - index of the argument
     /// \return the name of the argument as string
     QString argumentName(int __arg_index);
     
     /// \brief Function that returns the pointer to the argument object
     ///
     /// \param __arg_index - index of the argument
     /// \return the pointer to the argument object
     hFunctionArgument* argument(int __arg_index);
     
     /// \brief Function that returns the last error message
     ///
     /// \return the last error message as string
     QString lastError(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     virtual bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that returns if the function is the root function in the cell
     ///
     /// \return true if yes otherwise false
     bool isCellFunction(void);
     
     /// \brief Function that returns if the function object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     virtual bool functionVerification(QString& __error_message);
     
     /// \brief Function that returns the result of the fnction caculation. This is pure virtual function.
     ///
     /// \return the hMultipleValue object as the result of the caculation.
     virtual hMultipleValue* calculate(void) = 0;

     /// \brief Function that returns if function represents operator
     ///
     /// \return true if the function represents operator
     bool isOperator(void);

     /// \brief Function that returns if operator is left-associative
     ///
     /// \return true if function represents operator that is left-associative
     bool isOperatorLeftAssociative(void);

     /// \brief Function that returns operator precedence
     ///
     /// \return operator precedence
     int operatorPrecedence(void);
     
     /// \brief Function that returns the string that is the user representation of the functions inculing arguments
     ///
     /// \return the string that is the user representation of the functions inculing arguments
     virtual QString toString(void) = 0;
     
     /// \brief Function that maps the function to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this function as string.
     virtual QString toProlog(int __option = -1) = 0;

     /// \brief Function that maps the function to the prolog code.
     ///
     /// \return The prolog code that represents this function as string.
     virtual QString toDrools5(void) = 0;
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	virtual QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	virtual void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
     /// \param __cellExpression - denotes if this object represents the whole cell expression
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression = false);
     
     // -----------------
     
     /// \brief Function that returns the specify function representation of the given type
     ///
     /// \param __type - the type object
     /// \return the specify function representation of the given type
     static int typeFunctionRepresentation(hType* __type);
};
// -----------------------------------------------------------------------------
#endif
