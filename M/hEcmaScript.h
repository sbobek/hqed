#ifndef HECMASCRIPT_H
#define HECMASCRIPT_H

#include <QtCore/QString>
#if QT_VERSION >= 0x040300
     #include <QtScript/QScriptEngine>
#endif

/**
* \class 	hEcmaScript
* \author	Michał Lenart mrart@student.agh.edu.pl
* \brief 	Class contains useful static functions for ECMAScript presentation plugins.
*/
class hEcmaScript
{
public:
     /// \brief Function that returns the name of the script translation value
     ///
     /// \return the name of the script translation value
     static QString scriptTranslationVariable(void);

     /// \brief Function parse value using input function in presentation ECMAScript.
     ///
     /// \param &__scriptContent - Content of ECMASCript with input function.
     /// \param &__inValue - Value to parse.
     /// \param &__outValue - This variable will be set to parsed value.
     /// \param &__resultCode - This variable will be set to non-zero value in case of error.
     /// \return Returns true when no error occurred, false otherwise.
     static bool parseInput(const QString& __scriptContent, const QString& __inValue, QString& __outValue, int& __resultCode);

     /// \brief Function parse value using output function in presentation ECMAScript.
     ///
     /// \param &__scriptContent - Content of ECMASCript with output function.
     /// \param &__inValue - Value to parse.
     /// \param &__outValue - This variable will be set to parsed value.
     /// \param &__resultCode - This variable will be set to non-zero value in case of error.
     /// \return Returns true when no error occurred, false otherwise.
     static bool parseOutput(const QString& __scriptContent, const QString& __inValue, QString& __outValue, int& __resultCode);

     /// \brief This function checks if script given as parameter contains input function.
     ///
     /// \return Returns true if given script contains input function, false otherwise.
     static bool inputFunctionExists(const QString& __scriptContent);

     /// \brief This function checks if script given as parameter contains output function.
     ///
     /// \return Returns true if given script contains output function, false otherwise.
     static bool outputFunctionExists(const QString& __scriptContent);

private:
     class hCheckResultException {
     private:
          int _code;
          QString _message;
     public:
          hCheckResultException(int __code, QString __message) {
               _code = __code;
               _message = __message;
          }

          const int getCode() { return _code; }
          const QString getMessage() { return _message; }
     };

     static bool parse(QString __parseFunctionName, const QString& __scriptcontent, const QString& __inValue, QString& __outValue, int& __ok);
     static void checkResult(const QScriptEngine& __engine, const QScriptValue& __result);
     static bool functionExists(QString __parseFunctionName, const QString &__scriptContent);
};

#endif // HECMASCRIPT_H
