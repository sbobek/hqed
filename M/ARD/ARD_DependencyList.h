 /**
 * \file	ARD_DependencyList.h  
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	26.12.2007
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu bedacego pojemnikiem na zaleznosci ARD
 * \brief	This file contains class definition that is the container for ARD dependencies
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

 
#ifndef ARD_DEPENDENCYLISTH
#define ARD_DEPENDENCYLISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QList>
#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class ARD_Property;
class ARD_Attribute;
class ARD_Dependency;
class XTT_Attribute;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_DependencyList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	26.12.2007
* \brief 	Class definition that is the container for ARD dependencies.
*/
class ARD_DependencyList : public QList<ARD_Dependency*>
{

private:
          
     bool fChanged; ///< Specifies the status if content of the container was changed
     
     void findParents(QString, QList<ARD_Property*>*); ///< Searches for the parents for the value, function is private because of the recursion and danger
                                                       ///< during inability to call up
     
public:

     /// \brief Default constructor
     ARD_DependencyList(void);
     
     /// \brief Destructor
     ~ARD_DependencyList(void);
     
     /// ----------------- Methods for setting the values of class fields -----------------
     
     /// \brief Sets the change status on setpoint
     void setChanged(bool);
     
     /// ----------------- Operational Methods -----------------
     
     /// \brief Clears Container
     void flush(void);
     
     /// \brief Adds a new dependence
     ARD_Dependency* add(void);
     
     /// \brief Adds a new dependence
     void add(ARD_Dependency*);
     
     /// \brief Removes selected dependence
     void remove(int);
     
     /// \brief Searches for dependence on the ID
     int indexOfID(const QString&) const;
     
     /// \brief Functions that finds and returns the unique id
     ///
     /// \return the unique id as string
     QString findUniqID(void);
     
     /// \brief Searches and returns a list of dependencies of indicated values of source and destiny
     QList<ARD_Dependency*>* indexOfinout(ARD_Property*, ARD_Property*);
     
     /// \brief Searches and returns a list of dependencies of indicated values of source and destiny
     QList<ARD_Dependency*>* indexOfinout(QString, QString);
     
     /// \brief Returns a list of connections between the leaves of the tree
     QList<ARD_Dependency*>* leafsDependencies(void);

     /// \brief Searches for family od the value
     QList<ARD_Property*>* findFamily(QString);
     
     /// \brief Verify the relationship between the two properties
     bool checkDependence(QString _id, QString _dep, QList<ARD_Property*>* _checkedProperites = NULL);
     
     /// \brief Returns whether the dependencies were modified
     bool changed(void);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Loads data from a file in ARDML format in temporary version
     bool fromARDML_wv(QStringList*);
     
     /// \brief Loads data from a file in ARDML format in temporary version
     bool fromARDML_wv(QDomElement&);
     
     /// \brief Loads data from a file in HTML format in version 1.0
     bool fromHML_1_0(QDomElement&);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \return List of all data in hml 2.0 format.
     QStringList* out_HML_2_0(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No retur value.
     void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
     ///
     /// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
     int in_HML_2_0(QDomElement& __root, QString& __msg);
};
// -----------------------------------------------------------------------------

extern ARD_DependencyList* ardDependencyList;
// -----------------------------------------------------------------------------
#endif
