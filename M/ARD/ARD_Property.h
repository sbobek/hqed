/**
 * \file	ARD_Property.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	23.12.2007
 * \version	1.0
 * \brief	This file contains class ARD_Property definition which is a single object of ARD property
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ARD_PROPERTYH
#define ARD_PROPERTYH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
#if QT_VERSION >= 0x040300
#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class ARD_Attribute;
class ARD_DependencyList;
class gTPHnode;
class gARDnode;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_Property
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	23.12.2007
* \brief 	Class ARD_Property definition which is a single object of ARD property.
*/
class ARD_Property : public QList<ARD_Attribute*>
{
private:

    QString fID; ///< Object identifier

    QString fTPHparent; ///< Parent ID in TPH diagram. If it equals zero it means that the parent is root

    bool fChanged; ///< Specifies whether the object was changed

    gTPHnode* fGraphicTPHnode; ///< Object that is a graphical representation of properties in TPH diagram

    /// \brief Sets ID value
    ///
    /// \return no values return
    void setID(QString);

    /// \brief Sets value of parent in TPH diagram
    ///
    /// \return no values return
    void setTPHparent(QString);

public:

    /// \brief Default constructor of ARD_Property class
    ARD_Property(void);

    /// \brief Constructor of ARD_Property class which taking ID value as paramater
    ARD_Property(QString);

    /// \brief Destructor of ARD_Property class
    ~ARD_Property(void);

    // ----------------- Methods that return values of class fields -----------------

    /// \brief Returns the object identifier
    QString id(void);

    /// \brief Returns parents ID in TPH tree
    QString TPHparent(void) const;

    /// \brief Returns true if a given object was changed
    bool changed(void);

    /// \brief Returns pointer to an object which graphically represents the specific object
    gTPHnode* graphicTPHnode(void);

    // ----------------- Methods for setting the values of class fields -----------------

    /// \brief Sets value of the status of the object changes
    ///
    /// \return no values return
    void setChanged(bool);

    // --------------------------- Operational Methods ---------------------------

    /// \brief Specifies whether the object could be further transformed
    bool canBeTransformated(void);

    /// \brief Adds a new attribute to the object
    bool add(const QString&);

    /// \brief Adds a new attribute  to the object
    bool add(ARD_Attribute*);

    /// \brief Adds a list of attributes to property;
    bool add(const QList<ARD_Attribute*> & _attList);

    /// \brief Adds a new attribute to the object
    bool addByName(const QString&);

    /// \brief Remove attribute from the object
    bool subtract(const QString&);

    /// \brief Remove attribute from the object
    bool subtract(const ARD_Attribute*);

    /// \brief Removes all the Attributes specified by list.
    ///
    /// \param _removeList - list of items to be removed.
    /// \return true - if all of the Attributes _removeList were in this property and _removeList wasn't empty.
    bool subtract(const QList<ARD_Attribute*>& _removeList);

    /// \brief Sets the Attribute List of this Property to _attList
    ///
    /// \param _attList - list of attributes to be set in this Property.
    /// \return true if succeded to set the attribute list to _attList, otherwise false.
    bool setAttList(const QList<ARD_Attribute*> & _attList);

    /// \brief Searches for attributes based on the name
    int indexOfName(QString);

    /// \brief Searches for attributes based on the ID
    int indexOfID(QString);

    /// \brief Sorts attributes by name
    void sort(void);

    /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
    ///
    /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
    /// \param __event - the event identifier
    /// \param ... - the list of event params
    /// \return true on succes otherwise false
    bool eventMessage(QString* __errmsgs, int __event, ...);

    /// \brief Function gets all data in hml 2.0 format.
    ///
    /// \return List of all data in hml 2.0 format.
    QStringList* out_ard_property_HML_2_0(void);

#if QT_VERSION >= 0x040300
    /// \brief Function saves all data in hml 2.0 format.
    ///
    /// \param _xmldoc Pointer to document.
    /// \return No retur value.
    void out_ard_property_HML_2_0(QXmlStreamWriter* _xmldoc);
#endif

    /// \brief Function gets all data in hml 2.0 format.
    ///
    /// \return List of all data in hml 2.0 format.
    QStringList* out_ard_tph_HML_2_0(void);

#if QT_VERSION >= 0x040300
    /// \brief Function saves all data in hml 2.0 format.
    ///
    /// \param _xmldoc Pointer to document.
    /// \return No retur value.
    void out_ard_tph_HML_2_0(QXmlStreamWriter* _xmldoc);
#endif

    // Remove an attribute by functions removeAt(int)!!!

    /// \brief Class must have access to the parent setter
    friend class ARD_PropertyList;

    // ----------------- Static Methods -----------------

    /// \brief Static function that returns the prefix of the id.
    ///
    /// \return the prefix of the id as string.
    static QString idPrefix(void);
};
// -----------------------------------------------------------------------------
#endif
