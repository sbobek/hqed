 /**
 *                                                                             *
 * @file ARD_AttributeList.cpp                                                 *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_AttributeList.cpp,v 1.28.8.3 2011-08-31 02:42:44 hqeddoxy Exp $    *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 22.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_AttributeList               *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_hqed.h"

#include "../../C/MainWin_ui.h"
#include "../XTT/XTT_Attribute.h"
#include "../XTT/XTT_AttributeGroups.h"
#include "ARD_Property.h"
#include "ARD_PropertyList.h"
#include "ARD_Attribute.h"
#include "ARD_AttributeList.h"
// -----------------------------------------------------------------------------

ARD_AttributeList* ardAttributeList;
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_AttributeList::ARD_AttributeList(void) : QList<ARD_Attribute*>()
{
     fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Destruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_AttributeList::~ARD_AttributeList(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
}
// -----------------------------------------------------------------------------

// #brief Oproznia pojemnik
//
// #return wskaznik do nowo utworznonego atrybutu
void ARD_AttributeList::flush(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
          
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut
//
// #return wskaznik do nowo utworznonego atrybutu
ARD_Attribute* ARD_AttributeList::add(void)
{
     // Tworzenie nowego obiektu
     ARD_Attribute* tmp = new ARD_Attribute(findUniqID());
     add(tmp);
     
     return tmp;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut o podanym adresie
//
// #param ARD_Attribute* _new - adres nowego atrybutu do dodania
// #return brak zwracanych wartosci
void ARD_AttributeList::add(ARD_Attribute* _new)
{
     // Sprawdzamy czy taki atrybut juz nie istnieje
     if(contains(_new))
          return;
          
     append(_new);
}
// -----------------------------------------------------------------------------

// #brief Usuwa wybrany atrybut
//
// #param int _index - index atrybutu do usuniecia
// #return brak zwracanych wartosci
void ARD_AttributeList::remove(int _index)
{
     // Sprawdzanie poprawnosci indexu
     if((_index < 0) || (_index >= size()))
          return;
          
     // Usuwanie z obiektow typu property
     QList<ARD_Property*>* pl = ardPropertyList->listOfAttributeID(at(_index)->id());
     for(int i=0;i<pl->size();++i)
          pl->at(i)->removeAt(pl->at(i)->indexOfID(at(_index)->id()));
     delete pl;
     
     // Usywanie obiektu
     delete takeAt(_index);
}
// -----------------------------------------------------------------------------


// #brief Wyszukuje atrybuty na podstawie nazwy
//
// #param QString _name - nazwa atrybutu
// #return index atrybutu o podanej nazwie, jezeli nie ma takiego atrybutu to -1
int ARD_AttributeList::indexOfName(QString _name)
{
     for(int i=0;i<size();++i)
          if(at(i)->name() == _name)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje atrybuty na podstawie identyfikatora
//
// #param unsigned int _id - wartosc identyfikatora
// #return index atrybutu o podanym identyfikatorze, jezeli nie ma takiego atrybutu to -1
int ARD_AttributeList::indexOfID(QString _id)
{
     for(int i=0;i<size();++i)
          if(at(i)->id() == _id)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Functions that finds and returns the unique id
//
// #return the unique id as string
QString ARD_AttributeList::findUniqID(void)
{
     int candidate = 1;
     QString strCandidate = "";
     
     do
     {
          strCandidate = ARD_Attribute::idPrefix() + QString::number(candidate);
          candidate++;
     }
     while(indexOfID(strCandidate) > -1);
     
     return strCandidate;
}
// -----------------------------------------------------------------------------

// #brief Functions that creates the ARD attributes using XTT attributes
//
// #return true on succes, false on error
bool ARD_AttributeList::xttAtts2ardAtts(void)
{
     QList<XTT_Attribute*>* xtt_atts = AttributeGroups->Attributes();
     
     for(int i=0;i<xtt_atts->size();++i)
     {     
          if(xtt_atts->at(i) == NULL)
               return false;
               
          if(indexOfID(xtt_atts->at(i)->Id()) > -1)
               continue;
               
          ARD_Attribute* newAtt = add();
          
          if(newAtt == NULL)
               return false;
          
          newAtt->setId(xtt_atts->at(i)->Id());
          newAtt->setName(xtt_atts->at(i)->Name());
          newAtt->setDescription(xtt_atts->at(i)->Description());
          newAtt->setXTTequivalent(xtt_atts->at(i));
     }
     
     delete xtt_atts;
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Sortuje atrybuty rosnaco wg nazwy
//
// #return brak zwraanych wartosci
void ARD_AttributeList::sort(void)
{
     for(int i=0;i<size();++i)
          for(int j=0;j<size()-i-1;++j)
               if(at(j)->name() > at(j+1)->name())
                    swap(j, j+1);
                    
     // Zapisanie statusu zmiany
     fChanged = true;
}
// -----------------------------------------------------------------------------
 
// #brief Zwraca czy atrybuty byly modyfikowane
//
// #return true jezeli jakikolwiek element pojemnika byl modyfikowany, inaczej false
bool ARD_AttributeList::changed(void)
{
     if(fChanged)
          return true;
          
     for(int i=0;i<size();++i)
          if(at(i)->changed())
               return true;
     
     return false;
}
// -----------------------------------------------------------------------------

// #brief Ustawia status zmiany na zadana wartosc
//
// #param bool _c - wartosc statusu
// #return @returns brak zwraanych wartosci
void ARD_AttributeList::setChanged(bool _c)
{
     fChanged = _c;
     
     for(int i=0;i<size();++i)
          at(i)->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_AttributeList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Wcztuje dane z pliku w formacie HML w wersji 1.0
//
// #param QDomElement& root - referenacja do dokumentu HML
// #return true jezeli dane sa ok, ianczej false
bool ARD_AttributeList::fromHML_1_0(QDomElement& root)
{     
     // Sprawdzenie wystapineia odpowiedniego naglowka
     if(root.tagName().toLower() != "attribute_set")
          return false;
          
     bool res = true;
     fChanged = true;
     
     // Przechowuje wskaznik do aktualnego atrybutu
     ARD_Attribute* currentAttribute = NULL;
     
     // Wczytyawnie kolejnych atrybutow
     QDomElement DOMchild = root.firstChildElement("att");
     while(!DOMchild.isNull())
     {
          QString name = DOMchild.attribute("name", "#NAN#");
          
          // Usuwanie spacji z wczytanych wartosci
          for(;name.contains(" ");)
               name = name.remove(name.indexOf(" "), 1);
          
          // Jezeli nie podamy identyfikatora to blad
          if((name == "") || (name == "#NAN#"))
          {
               QString error = "Missing \"attribute\" parameter \"name\" at line " + QString::number(DOMchild.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          
          // Wyszukiwanie atrybutu w liscie atrybutuow
          int aindex = indexOfName(name);
          
          // Jezeli podany atrybut juz istnieje
          if(aindex > -1)
          {
               QString error = "Attribute \"" + name + "\" is already exists. Error at line " + QString::number(DOMchild.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          
          // Jezeli podany atrybut jeszcze nie istnieje
          if(aindex == -1)
          {
               currentAttribute = add();
               currentAttribute->setName(name);
               currentAttribute = NULL;
          }
     
          // read the next attribute atg
          DOMchild = DOMchild.nextSiblingElement("att");
     }
     
     setChanged(false);
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_AttributeList::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     for(int i=0;i<size();++i)
     {
          if(at(i)->xttEquivalent() != NULL)
               continue;

          QStringList* tmp = at(i)->out_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_AttributeList::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     for(int i=0;i<size();++i)
          if(at(i)->xttEquivalent() == NULL)
               at(i)->out_HML_2_0(_xmldoc);
}
#endif
// -----------------------------------------------------------------------------
