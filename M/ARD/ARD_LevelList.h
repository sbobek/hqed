/**
 * \file	ARD_LevelList.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	04.01.2008
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu bedacego pojemnikiem na obiekty bedace kolejnymi poziomami ARD
 * \brief       This file contains class definition which is a container for objects that are successive levels of ARD
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ARD_LEVELLISTH
#define ARD_LEVELLISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
// -----------------------------------------------------------------------------

class hBaseScene;
class ARD_Level;
class ARD_Property;
class ARD_Dependency;
class ARD_Attribute;
class XTT_Attribute;
class gARDlevelList;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_LevelList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	04.01.2008
* \brief 	Class definition which is a container for objects that are successive levels of ARD.
*/
class ARD_LevelList : public QList<ARD_Level*>
{

private:

    bool fChanged; ///< Specifies the status which indicate that the contents of container was changed

    QString fError; ///< The contents of the last occur error

    gARDlevelList* fGlevelList; ///< Object which is a graphical representation of levels

public:

    /// \brief Default constructor of ARD_LevelList class
    ARD_LevelList(void);

    /// \brief Destructor of ARD_LevelList class
    ~ARD_LevelList(void);

    /// \brief Checks whether level is the last of the ARD_LevelList.
    ///
    /// \param level - pointer to level that will be queried.
    /// \return true if level is last level of ARD_LevelList, otherwise false.
    bool endsWith(const ARD_Level* level) const { return !isEmpty() && last() == level; }

    // ----------------- Methods for setting the values of class fields -----------------

    /// \brief Sets status of change
    void setChanged(bool);

    /// \brief Setter of a display object
    ///
    /// \param __display - a pointer to the display object
    /// \return no values return
    void setDisplay(hBaseScene* __display);

    // ----------------- Methods that return values of class fields -----------------

    /// \brief Getter of a display object
    ///
    /// \return A pointer to the displaye object
    hBaseScene* display(void);

    /// \brief Returns status of change of container
    bool changed(void);

    /// \brief Returns pointer to the object which is a graphically represantation of levels
    gARDlevelList* gLevelList(void);

    /// \brief Returns the message of last occurred error
    QString error(void);

    // ----------------- Operational Methods -----------------

    /// \brief Flushes the container
    void flush(void);

    /// \brief Adds new level
    ARD_Level* add(void);

    /// \brief Adds new level
    void add(ARD_Level*);

    void remove(int);

    /// \brief Deletes levels below _index
    ///
    /// \param _index - index of the level below which all the levels will be deleted
    /// \return no return value
    void removeLevelsBelow(int);

    /// \brief Restores the levels
    void buildLevels(int);

    /// \brief Creates first level
    void createBaseLevel(void);

    /// \brief Refreshes the ARD display
    ///
    /// \return no values return
    void refreshDisplay(void);

    /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
    ///
    /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
    /// \param __event - the event identifier
    /// \param ... - the list of event params
    /// \return true on succes otherwise false
    bool eventMessage(QString* __errmsgs, int __event, ...);
};
// -----------------------------------------------------------------------------

extern ARD_LevelList* ardLevelList;
// -----------------------------------------------------------------------------
#endif
