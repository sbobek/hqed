 /**
 * \file	ARD_PropertyList.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	25.12.2007 
 * \version	1.0
 * \brief	This file contains class definition which is a container of ARD objects
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ARD_PROPERTYLISTH
#define ARD_PROPERTYLISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QList>
#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define PROPERTY_IS_TPH_LEAF 0
#define PROPERTY_IS_NOT_TPH_LEAF 1
#define PROPERTY_IS_NOT_EXIST 2
// -----------------------------------------------------------------------------

class hBaseScene;
class ARD_Property;
class ARD_Attribute;
class XTT_Attribute;

class gTPHtree;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_PropertyList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	25.12.2007
* \brief 	Class definition which is a container of ARD objects.
*/
class ARD_PropertyList : public QList<ARD_Property*>
{

private:
     
     bool fChanged; ///< Specifies the status which indicates that the contents of the container was changed
     
     gTPHtree* TPHdiagram; ///< Variable responsible for TPH diagram managing
     QString fError; ///< The contents of the last occur error
     
public:
   
     /// \brief Default constructor of ARD_ProperyList class.
     ARD_PropertyList(void);
       
     /// \brief Destructor of ARD_PropertyList class
     ~ARD_PropertyList(void);
     
     // ----------------- Metody ustawiajace wartosci pol klasy -----------------
     
     /// \brief Sets status of change
     ///
     /// \return no values return
     void setChanged(bool);

     /// \brief Setter of a display object
     ///
     /// \param __display - a pointer to the display object
     /// \return no values return
     void setDisplay(hBaseScene* __display);
     
     // ----------------- Metody zwracajace wartosci pol klasy -----------------
       
     /// \brief Returns pointer to object which draws TPH diagram
     gTPHtree* tphTree(void) const;
      
     /// \brief Returns last error message
     QString error(void);

     /// \brief Getter of a display object
     ///
     /// \return A pointer to the displaye object
     hBaseScene* display(void);
     
     // ----------------- Metody operacyjne -----------------
            
     /// \brief Flushes the container
     ///
     /// \return no values return
     void flush(void);
     
     /// \brief Adds new attribute
     ARD_Property* add(void);
     
     /// \brief Adds new attribute
     ///
     /// \return no values return
     void add(ARD_Property*);
     
     /// \brief Removes the given Property
     ///
     /// \return no values return
     void remove(int);

     /// \brief Removes children of given Property
     ///
     /// \return no values return
     void removeChildren (ARD_Property* prop);

     /// \brief Updates the parents for the given property
     ///
     /// \param _changedProperty - is the pointer to the property that was changed, his parents need to be updated
     /// \param _newAttributes - list of the new attributes for the property
     /// \return true if there is a finalisation among the parents of the _changedProperty, otherwise false.
     bool updatePropertyWithParents(ARD_Property* _changedProperty, const QList<ARD_Attribute*>& _newAttributes);
         
     /// \brief Searches for objects that contains an attribute with the specified name
     QList<ARD_Property*>* listOfAttributeName(QString);
         
     /// \brief Searches for objects that contains an attribute with the specified id
     QList<ARD_Property*>* listOfAttributeID(QString);
       
     /// \brief Searches for list of specified object children
     QList<ARD_Property*>* childList(ARD_Property*);

     /// \brief Searches for all of the parents of _property in the TPH diagram
     /// \attention Result of function must be deleted;
     /// \param _property - property for which we want to find parents.
     QList<ARD_Property*>* parentList(ARD_Property* _property) const;

     /// \brief Returns a list of attributes that are in all of the parents (ancestors) of this property.
     /// \attention The return QList<ARD_Attribute*>* must be deleted.
     /// \return list of attributes in parents of this.
     QList<ARD_Attribute*>* attributesInParentsList(ARD_Property* _property) const;
   
     /// \brief Returns pointer to parent of specified property in TPH diagram
     ARD_Property* tphParent( ARD_Property*);

     /// \brief Function that returns the TPH diagram root for the given _property.
     ///
     /// \param _property - property for which we are searching for a root.
     /// \return a pointer to the root property in the TPH diagram if such exists, otherwise NULL.     
     ARD_Property* tphRoot( ARD_Property* _property);

     /// \brief Searches for objects in list of TPH diagram
     QList<ARD_Property*>* listOfNotTransformatedProperties(QString _attName = "");
          
     /// Returns list of TPH diagram leaves
     QList<ARD_Property*>* listOfLeafs(void);
     
     /// \brief Functions that finds and returns the unique id
     ///
     /// \return the unique id as string
     QString findUniqID(void);
        
     /// \brief Searches for objects based on ID
     int indexOfID(QString);
        
     /// \brief Searches for the number of objects that are declared as roots in the TPH diagram
     int rootsCount(void);
        
     /// \brief Searches for object that is a root in TPH diagram
     int indexOfRoot(void);
         
     // \brief Returns true if attributes was changed
     bool changed(void);

     /// \brief Refreshes the TPH display
     ///
     /// \return no values return
     void refreshDisplay(void);
     
     /// Ustawia wartosc identyfikatora obiektu
     bool setPropertyID(ARD_Property* _property, QString _id);
        
     /// \brief Sets the value of the object parent in the TPH diagram
     bool setTPHparent(ARD_Property* _property, ARD_Property* _parent);
       
     /// \brief Checks if the objects is a leaf in the TPH diagram 
     int isLeaf(ARD_Property*);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
       
     /// \brief Reads data from the file in ARDML format in a temporary version
     bool fromARDML_wv(QStringList*);
       
     /// \brief Reads data from the file in ARDML format in a temporary version
     bool fromARDML_wv(QDomElement&);
         
     /// \brief Reads data from the file in HML 1.0 format
     bool fromHML_1_0(QDomElement&);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \return List of all data in hml 2.0 format.
     QStringList* out_ard_properties_HML_2_0(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No return value.
     void out_ard_properties_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \return List of all data in hml 2.0 format.
     QStringList* out_ard_tph_HML_2_0(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No return value.
     void out_ard_tph_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
     ///
     /// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
     int in_HML_2_0(QDomElement& __root, QString& __msg);
};
// -----------------------------------------------------------------------------

extern ARD_PropertyList* ardPropertyList;
// -----------------------------------------------------------------------------
#endif
