 /**
 *                                                                             *
 * @file ARD_Level.cpp                                                         *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_Level.cpp,v 1.29.4.1.2.2 2011-08-31 02:53:53 hqeddoxy Exp $                    *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 03.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje metod obiektu bedacego modelem          *
 * poziomu diagramu ARD                                                        *
 *                                                                             *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include "../../C/MainWin_ui.h"
#include "../../C/widgets/hBaseScene.h"

#include "../../namespaces/ns_hqed.h"

#include "ARD_Attribute.h"
#include "ARD_AttributeList.h"
#include "ARD_Property.h"
#include "ARD_PropertyList.h"
#include "ARD_Dependency.h"
#include "ARD_DependencyList.h"

#include "../../V/ARD/gARDnode.h"
#include "../../V/ARD/gARDlevel.h"
#include "../../V/ARD/gARDdependency.h"
#include "ARD_Level.h"
//----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_Level::ARD_Level(void)
{
	fProperties = new QList<ARD_Property*>;
	fDependencies = new QList<ARD_Dependency*>;
	fLevel = -1;
	fGlevel = new gARDlevel(this);
}
//----------------------------------------------------------------------------

// #brief Konstruktor ustawiajacy wartosc levelu
//
// #param int _level - numer levelu
// #return Brak zwracanych wartosci
ARD_Level::ARD_Level(int _level)
{
	fProperties = new QList<ARD_Property*>;
	fDependencies = new QList<ARD_Dependency*>;
	fLevel = _level;
	fGlevel = new gARDlevel(this);
}
//----------------------------------------------------------------------------

// #brief Destruktor
//
// #return Brak zwracanych wartosci
ARD_Level::~ARD_Level(void)
{
	delete fProperties;
	delete fDependencies;
	delete fGlevel;
}
//----------------------------------------------------------------------------

// #brief Oproznia pojemnik
//
// #return wskaznik do nowo utworznonego atrybutu
void ARD_Level::flush(void)
{
	fGlevel->flush();
	fProperties->clear();
	fDependencies->clear();
	fLevel = -1;
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Zwraca czy obiekty byly modyfikowane
//
// #return true jezeli jakikolwiek element pojemnika byl modyfikowany, inaczej false
bool ARD_Level::changed(void)
{
	return fChanged;
}
// -----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void ARD_Level::setDisplay(hBaseScene* __display)
{
     fGlevel->setDisplay(__display);
}
// -----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* ARD_Level::display(void)
{
     return fGlevel->display();
}
// -----------------------------------------------------------------------------

// #brief Zwraca numer levelu
//
// #return Zwraca numer levelu
int ARD_Level::level(void)
{
	return fLevel;
}
//----------------------------------------------------------------------------

// #brief Zwraca wskaznik do listy wlasnosci na tym poziomie
//
// #return Zwraca wskaznik do listy wlasnosci na tym poziomie
QList<ARD_Property*>* ARD_Level::properties(void)
{
	return fProperties;
}
//----------------------------------------------------------------------------

// #brief Zwraca wskaznik do zaleznosci wlasnosci na tym poziomie
//
// #return Zwraca wskaznik do zaleznosci wlasnosci na tym poziomie
QList<ARD_Dependency*>* ARD_Level::dependencies(void)
{
	return fDependencies;
}
//----------------------------------------------------------------------------

// #brief Zwraca wskaznik do obiektu bedacego grafizna reprezentacja tego poziomu
//
// #return wskaznik do obiektu bedacego grafizna reprezentacja tego poziomu
gARDlevel* ARD_Level::gLevel(void)
{
	return fGlevel;
}
//----------------------------------------------------------------------------

// #brief Ustawia status zmiany na zadana wartosc
//
// #param bool _c - wartosc statusu
// #return brak zwraanych wartosci
void ARD_Level::setChanged(bool _c)
{
	fChanged = _c;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc levelu
//
// #param int _level - nowy numer levelu
// #return Brak zwracanych wartosci
void ARD_Level::setLevel(int _level)
{
	fLevel = _level;
	fChanged = true;
}
//----------------------------------------------------------------------------

// #brief Wyszukuje obiekty na podstawie identyfikatora
//
// #param _id - wartosc identyfikatora
// #return index obiektu o podanym identyfikatorze, jezeli nie ma takiego obiektu to -1
int ARD_Level::indexOfDepID(QString _id)
{
	for(int i=0;i<fDependencies->size();++i)
		if(fDependencies->at(i)->id() == _id)
			return i;
			
	return -1;
}
// -----------------------------------------------------------------------------

QList<ARD_Dependency*>* ARD_Level::indexOfDependentOfOnly(ARD_Property* F)
{
	QList<ARD_Dependency*>* res = new QList<ARD_Dependency*>;

	for(int i=0;i<fProperties->size();++i)
	{
		if(fProperties->at(i) == F)
			continue;

		QList<ARD_Dependency*>* t = indexOfinout(NULL, fProperties->at(i));
		if((t->size() == 1) && (t->at(0)->independent() == F))
			res->append(t->at(0));
               
		delete t;
	}
	
	return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje zaleznosci na podstawie zrodla i konca
//
// #param ARD_Dependency* _src - wskaznik do wlasciwosci wejscowej, jezeli NULL to dowolna
// #param ARD_Dependency* _dest - wskaznik do wlasciwosci koncowej, jezeli NULL to dowolna
// #return liste zaleznosci spelniajacych warunki
QList<ARD_Dependency*>* ARD_Level::indexOfinout(ARD_Property* _src, ARD_Property* _dest)
{
	// Usalanie wartosci identyfiktorow
	QString src = "";
	QString dest = "";
	
	// Poprawa wartosci id's
	if(_src != NULL)
		src = _src->id();
	
	if(_dest != NULL)
		dest = _dest->id();
		
	return indexOfinout(src, dest);
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje zaleznosci na podstawie zrodla i konca
//
// #param QString _src - identyfikator wlasciwosci wejscowej, jezeli "" to dowolna
// #param QString _dest - identyfikator wlasciwosci koncowej, jezeli "" to dowolna
// #return liste zaleznosci spelniajacych warunki
QList<ARD_Dependency*>* ARD_Level::indexOfinout(QString _src, QString _dest)
{
	QList<ARD_Dependency*>* res = new QList<ARD_Dependency*>;

	for(int i=0;i<fDependencies->size();++i)
		if(((_src == "") || ((fDependencies->at(i)->independent() != NULL) && (fDependencies->at(i)->independent()->id() == _src))) 
		&& 
		((_dest == "") || ((fDependencies->at(i)->dependent() != NULL) && (fDependencies->at(i)->dependent()->id() == _dest))))
			res->append(fDependencies->at(i));
			
	return res;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowa zaleznosc do listy
//
// #param ARD_Dependency* _newDep - wskaznik do nowej zaleznsoci
// #return true jezeli jeszcze taka zaleznosc nie istnieje i jest poprawna, inaczej false
bool ARD_Level::addDependency(ARD_Dependency* _newDep)
{
	// Sprawdzanie czy nie NULL
	if(_newDep == NULL)
		return false;
	
	// Sprawdzanie czy nie istneije juz ten obiekt
	if(fDependencies->contains(_newDep))
		return false;
		
	// Sprawdzanie czy nie istneije taki sam obiekt
	QList<ARD_Dependency*>* dl = indexOfinout(_newDep->independent(), _newDep->dependent());
	int dl_size = dl->size();
	delete dl;
	if(dl_size > 0)
		return false;
		
	// Sprawdzanie czy istnieja wszystkie zaleznosci na danym poziomie
	if(indexOfPrpID(_newDep->independent()->id()) == -1)
		return false;
	if(indexOfPrpID(_newDep->dependent()->id()) == -1)
		return false;
		
	// Dodanie nowej zaleznosci do obiektu graficznego
	if(fGlevel->indexOfDependencyInOut(_newDep) == -1)
		fGlevel->dependencies()->append(new gARDdependency(fGlevel->nextDependencyId(), _newDep, fGlevel));
		
	// Teraz mozemy dodac w koncu
	fDependencies->append(_newDep);
	
	fChanged = true;
	return true;
}
// -----------------------------------------------------------------------------

// #brief Usuwa zaleznosc
//
// #param ARD_Dependency* _depPtr - wskaznik do zaleznsoci ktora chcemy usunac
// #return true jezeli ok inaczej false
bool ARD_Level::removeDependency(ARD_Dependency* _depPtr)
{
	fChanged = true;
	return removeDependency(fDependencies->indexOf(_depPtr));
}
// -----------------------------------------------------------------------------

// #brief Usuwa zaleznosc o podanym indexie
//
// #param int _index - index zaleznsoci ktora chcemy usunac
// #return true jezeli ok inaczej false
bool ARD_Level::removeDependency(int _index)
{
	// Sprawdzenie poprawnosci indexu
	if((_index < 0) || (_index >= fDependencies->size()))
		return false;
		
	fGlevel->removeDependencies(fDependencies->at(_index));
	fDependencies->removeAt(_index);
	
	fChanged = true;
	return true;
}
// -----------------------------------------------------------------------------

bool ARD_Level::removeDependencies(QList<ARD_Dependency*>* _depList)
{
	for(int d=0;d<_depList->size();++d)
		if(fDependencies->contains(_depList->at(d)))
			fDependencies->removeAt(fDependencies->indexOf(_depList->at(d)));
	
	return true;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty zawierajace arybut o podanej nazie
//
// #param QString _name - nazwa atrybutu
// #return liste obiektow zawierajace arybut o podanej nazwie
QList<ARD_Property*>* ARD_Level::listOfAttributeName(QString _name)
{
	QList<ARD_Property*>* res = new QList<ARD_Property*>;

	for(int i=0;i<fProperties->size();++i)
		if(fProperties->at(i)->indexOfName(_name) > -1)
			res->append(fProperties->at(i));
			
	return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty zawierajace arybut o podanym id
//
// #param _id - identyfiaktor atrybutu
// #return liste obiektow zawierajace arybut o podanym identyfikatorze
QList<ARD_Property*>* ARD_Level::listOfAttributeID(QString _id)
{
	QList<ARD_Property*>* res = new QList<ARD_Property*>;

	for(int i=0;i<fProperties->size();++i)
		if(fProperties->at(i)->indexOfID(_id) > -1)
			res->append(fProperties->at(i));
			
	return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty na podstawie identyfikatora
//
// #param _id - wartosc identyfikatora
// #return index obiektu o podanym identyfikatorze, jezeli nie ma takiego obiektu to -1
int ARD_Level::indexOfPrpID(QString _id)
{
	for(int i=0;i<fProperties->size();++i)
		if(fProperties->at(i)->id() == _id)
			return i;
			
	return -1;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje wszystkie wezly od ktorych jest zalezny wezel wejsciowy
//
// #param QString _id - identyfikator wlasciwosci (ARD_Property) wejscowej
// #return liste wezlow od ktorych dana wlasnosc zalezy bezposrednio lub posrednio lacznie z nia sama, jezeli NULL to blad
QList<ARD_Property*>* ARD_Level::findFamily(QString _id)
{
	if(ardPropertyList->indexOfID(_id) == -1)
		return NULL;

	QList<ARD_Property*>* _family = new QList<ARD_Property*>;
	findParents(_id, _family);
	return _family;
}
// -----------------------------------------------------------------------------

// #brief Funckja rekurencyjna wyszukujaca rodzicow danej wlasnosci
//
// #param QString _id - identyfikator wlasciwosci (ARD_Property) wejscowej
// #param QList<ARD_Property*>* _currentFamily - dotychczas utworzona rodzina
// #return brak zwracanych wartosci
void ARD_Level::findParents(QString _id, QList<ARD_Property*>* _currentFamily)
{
	// Wyszukiwanie listy wezlow od ktturych _id jest bezposrednio zalezny
	QList<ARD_Dependency*>* directDep = indexOfinout("", _id);
	
	// Dodawanie do listy rodziny wyszukanych zaleznosci
	for(int i=0;i<directDep->size();++i)
		if(!_currentFamily->contains(directDep->at(i)->independent()))
		{
			_currentFamily->append(directDep->at(i)->independent());
			findParents(directDep->at(i)->independent()->id(), _currentFamily);
		}
		
	delete directDep;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowa wlasnosc
//
// #param ARD_Property* _newProperty - wskaznik do nowej wlasnosci
// #return true jezeli jeszcze taka wlasnosc nie istnieje i jest poprawna, inaczej false
bool ARD_Level::addProperty(ARD_Property* _newProperty)
{
	// Sprawdzanie czy nie NULL
	if(_newProperty == NULL)
		return false;
	
	// Sprawdzanie czy nie istneije juz ten obiekt
	if(fProperties->contains(_newProperty))
		return false;
		
	// Sprawdzanie czy nie istneije taki sam obiekt
	if(indexOfPrpID(_newProperty->id()) > -1)
		return false;
		
	// Dodanie nowej wlasnosci do obiektu graficznego
	if(fGlevel->indexOfParentID(_newProperty->id()) == -1)
	{
		fGlevel->nodes()->append(new gARDnode(fGlevel->nextNodeId(), _newProperty, fGlevel));
		//fGlevel->setParents();
	}
		
	// Teraz mozemy dodac w koncu
	fProperties->append(_newProperty);
	
	fChanged = true;
	return true;
}
// -----------------------------------------------------------------------------

bool ARD_Level::addProperties(QList<ARD_Property *> &_propList)
{
     ARD_Property* tmp;
     bool ok = true;
     foreach(tmp,_propList)
     {
          ok =  ok && addProperty(tmp);
     }
     return ok;
}

// #brief Usuwa wlasnosc
//
// #param ARD_Property* _propPtr - wskaznik do wlasnosci ktora chcemy usunac
// #return true jezeli ok inaczej false
bool ARD_Level::removeProperty(ARD_Property* _propPtr)
{
	fChanged = true;
	return removeProperty(fProperties->indexOf(_propPtr));
}
// -----------------------------------------------------------------------------

// #brief Usuwa wlasnosc o podanym indexie
//
// #param int _index - index wlasnosci ktora chcemy usunac
// #return true jezeli ok inaczej false
bool ARD_Level::removeProperty(int _index)
{
	// Sprawdzenie poprawnosci indexu
	if((_index < 0) || (_index >= fProperties->size()))
		return false;
	
	// Usuwanie zaleznosci zwizanych z dana wlasnoscia
	QList<ARD_Dependency*>* cl1 = indexOfinout(fProperties->at(_index)->id(), "");	// Zaleznosci wychodzace
	QList<ARD_Dependency*>* cl2 = indexOfinout("", fProperties->at(_index)->id());	// Zaeznosci przychodzace
	
	// Laczenie list
	for(int i=0;i<cl2->size();++i)
		if(!cl1->contains(cl2->at(i)))
			cl1->append(cl2->at(i));
	
	// Usuwanie zaleznosci z obiektu graficznego
	fGlevel->removeDependencies(fProperties->at(_index));
	
	// Usuwanie elementow
	for(int i=0;i<cl1->size();++i)
		if(fDependencies->contains(cl1->at(i)))
			fDependencies->removeAt(fDependencies->indexOf(cl1->at(i)));
	delete cl1;
	delete cl2;
	
	// Usuwanie z obiektu graficznego
	int nodeIndex = fGlevel->indexOfParentID(fProperties->at(_index)->id());
	if(nodeIndex > -1)
		delete fGlevel->nodes()->takeAt(nodeIndex);
	
	fProperties->removeAt(_index);
	
	fChanged = true;
	return true;
}
// -----------------------------------------------------------------------------

// #brief Uaktualnia poziom na podstawie podanego
//
// #param ARD_Level* _srcLevel - poziom wzorcowy
// #param bool _moreGeneral - okresla czy poziom ma byc bardziej szczegolowy (false), czy bardziej ogolny (true)
// #return true jezeli uda sie utworzyc poziom, inaczej false
bool ARD_Level::update(ARD_Level* _srcLevel, bool _moreGeneral)
{
	// Jezeli uaktualniamy na bardziej ogolny poziom
	if(_moreGeneral)
	{
		// ========== Ustawianie wlasnosci ==========
		
		// To bedze lista wlasciwosci na tym poziomie
		QList<ARD_Property*>* parents = new QList<ARD_Property*>;
		
		// Do listy rodzicow dodajemy tylko tych ktorzy maja wszystkich potomkow na tym poziomie
		for(int p=0;p<_srcLevel->properties()->size();++p)
		{
			ARD_Property* parent = ardPropertyList->tphParent(_srcLevel->properties()->at(p));
			
			if(parent == NULL)
				return false;
				
			// Wyszukiwanie wszystkich potomkow danego rodzica
			QList<ARD_Property*>* parentChilds = ardPropertyList->childList(parent);
			
			// Sprawdzanie czy wszyscy potomkowie sa na tym poziomie
			bool addParent = true;
			for(int c=0;c<parentChilds->size();++c)
			{
				// Jezeli na tym poziomie nie ma potomka
				if(!_srcLevel->properties()->contains(parentChilds->at(c)))
				{
					c = parentChilds->size();
					addParent = false;
				}
			}
			
			// Jezeli wszyscy potomkowie sa na tym poziomie to dodajemy do listy dozwolonych rodzicow
			if((addParent) && (!parents->contains(parent)))
				parents->append(parent);
			
			// Jezeli nie wszyscy potomkowie istnieja to jako rodzica dodajemy aktualnie analizowane dziecko
			if((!addParent) && (!parents->contains(_srcLevel->properties()->at(p))))
				parents->append(_srcLevel->properties()->at(p));
				
			delete parentChilds;
		}
		
		// Dodawanie do aktualnej listy wlasnosci nie istniejacych
		for(int i=0;i<parents->size();++i)
			if(!fProperties->contains(parents->at(i)))
				addProperty(parents->at(i));
				
		// Usuwanie z listy wlasnosci juz nie istneijacych
		for(int i=fProperties->size()-1;i>=0;--i)
			if(!parents->contains(fProperties->at(i)))
				removeProperty(i);
				
		// ========== Ustawianie zaleznosci ==========
		
		// To bedze lista zaleznosci na tym poziomie
		QList<ARD_Dependency*>* deps = new QList<ARD_Dependency*>;
		
		// Do listy rodzicow dodajemy tylko tych ktorzy maja wszystkich potomkow na tym poziomie
		for(int d=0;d<_srcLevel->dependencies()->size();++d)
		{
			ARD_Property *indep, *dep;		// Docelowe parametry nowej zaleznosci
			
			ARD_Property* indepParent = ardPropertyList->tphParent(_srcLevel->dependencies()->at(d)->independent());
			ARD_Property* depParent = ardPropertyList->tphParent(_srcLevel->dependencies()->at(d)->dependent());
			
			indep = _srcLevel->dependencies()->at(d)->independent();
			if(parents->contains(indepParent))
				indep = indepParent;
				
			dep = _srcLevel->dependencies()->at(d)->dependent();
			if(parents->contains(depParent))
				dep = depParent;
			
			if((indep == dep) && ((indep == indepParent) || (dep == depParent)))
				continue;
			
			ARD_Dependency* newDep = ardDependencyList->add();
			newDep->setIndependent(indep);
			newDep->setDependent(dep);
			deps->append(newDep);
		}
		
		// Dodawanie do aktualnej listy zaleznosci nie istniejacych
		for(int i=0;i<deps->size();++i)
			if(!fDependencies->contains(deps->at(i)))
				addDependency(deps->at(i));
				
		// Usuwanie z listy zaleznosci juz nie istneijacych
		for(int i=fDependencies->size()-1;i>=0;--i)
			if(!deps->contains(fDependencies->at(i)))
				removeDependency(i);
	}
	
	// Rozmieszczenie obiektow na scenie
	fGlevel->placeObjects();
	
	fChanged = true;
	return true;
}
// -----------------------------------------------------------------------------

// #brief Odswieza widok graficzny
//
// #return zwraca obszar zajmowany przez obiekty danego levelu
QRectF ARD_Level::paint(void)
{
	fGlevel->refresh();
	return fGlevel->levelArea();
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_Level::eventMessage(QString* /*__errmsgs*/, int /*__event*/, ...)
{
     /*
     va_list argsptr;
     va_start(argsptr, __event);
     va_arg(argsptr, typ);
     va_end(argsptr);
     */
     
     // bool res = true;
     
     /*if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
          return res;*/
          
          
     return true;
}
// -----------------------------------------------------------------------------
