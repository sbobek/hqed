 /**
 *                                                                             *
 * @file ARD_DependencyList.cpp                                                *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_DependencyList.cpp,v 1.28.4.1.2.3 2011-08-31 02:42:44 hqeddoxy Exp $           *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 26.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_DependencyList              *
 *                                                                             *
 */

#include <QInputDialog>
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_hqed.h"
#include "../XTT/XTT_Attribute.h"
#include "ARD_Property.h"
#include "ARD_PropertyList.h"
#include "ARD_Dependency.h"
#include "ARD_PropertyList.h"
#include "ARD_DependencyList.h"
// -----------------------------------------------------------------------------

ARD_DependencyList* ardDependencyList;
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_DependencyList::ARD_DependencyList(void) : QList<ARD_Dependency*>()
{
     fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Destruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_DependencyList::~ARD_DependencyList(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
}
// -----------------------------------------------------------------------------

// #brief Oproznia pojemnik
//
// #return wskaznik do nowo utworznonego atrybutu
void ARD_DependencyList::flush(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
     
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy obiekt
//
// #return wskaznik do nowego obiektu
ARD_Dependency* ARD_DependencyList::add(void)
{
     // Tworzenie nowego obiektu
     ARD_Dependency* tmp = new ARD_Dependency(findUniqID());
     add(tmp);
     
     return tmp;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut o podanym adresie
//
// #param ARD_Dependency* _new - adres nowego obiektu do dodania
// #return brak zwracanych wartosci
void ARD_DependencyList::add(ARD_Dependency* _new)
{
     // Sprawdzamy czy taki atrybut juz nie istnieje
     if(contains(_new))
          return;
          
     append(_new);
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Usuwa wybrana zaleznosc
//
// #param int _index - index obiektu do usuniecia
// #return brak zwracanych wartosci
void ARD_DependencyList::remove(int _index)
{
     // Sprawdzanie poprawnosci indexu
     if((_index < 0) || (_index >= size()))
          return;
     
     // Usuwanie obiektu
     delete takeAt(_index);
     
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty na podstawie identyfikatora
//
// #param _id - wartosc identyfikatora
// #return index obiektu o podanym identyfikatorze, jezeli nie ma takiego obiektu to -1
int ARD_DependencyList::indexOfID(const QString& _id) const
{
     for(int i=0;i<size();++i)
          if(at(i)->id() == _id)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Functions that finds and returns the unique id
//
// #return the unique id as string
QString ARD_DependencyList::findUniqID(void)
{
     int candidate = 1;
     QString strCandidate = "";
     
     do
     {
          strCandidate = ARD_Property::idPrefix() + QString::number(candidate);
          candidate++;
     }
     while(indexOfID(strCandidate) > -1);
     
     return strCandidate;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje zaleznosci na podstawie zrodla i konca
//
// #param ARD_Dependency* _src - wskaznik do wlasciwosci wejscowej, jezeli NULL to dowolna
// #param ARD_Dependency* _dest - wskaznik do wlasciwosci koncowej, jezeli NULL to dowolna
// #return liste zaleznosci spelniajacych warunki
QList<ARD_Dependency*>* ARD_DependencyList::indexOfinout(ARD_Property* _src, ARD_Property* _dest)
{
     // Ustalanie wartosci identyfiktorow
     QString src = "";
     QString dest = "";
     
     // Poprawa wartosci id's
     if(_src != NULL)
          src = _src->id();
     
     if(_dest != NULL)
          dest = _dest->id();
          
     return indexOfinout(src, dest);
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje zaleznosci na podstawie zrodla i konca
//
// #param QString _src - identyfikator wlasciwosci wejscowej, jezeli "" to dowolna
// #param QString _dest - identyfikator wlasciwosci koncowej, jezeli "" to dowolna
// #return liste zaleznosci spelniajacych warunki
QList<ARD_Dependency*>* ARD_DependencyList::indexOfinout(QString _src, QString _dest)
{
     QList<ARD_Dependency*>* res = new QList<ARD_Dependency*>;

     for(int i=0;i<size();++i)
          if(((_src == "") || ((at(i)->independent() != NULL) && (at(i)->independent()->id() == _src))) 
          && 
          ((_dest == "") || ((at(i)->dependent() != NULL) && (at(i)->dependent()->id() == _dest))))
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Zwraca liste zaleznosci miedzy samymi lisciami drzew
//
// #return liste zaleznosci miedzy samymi lisciami drzew
QList<ARD_Dependency*>* ARD_DependencyList::leafsDependencies(void)
{
     QList<ARD_Dependency*>* res = new QList<ARD_Dependency*>;
     for(int i=0;i<size();++i)
          if(((at(i)->independent() != NULL) && (ardPropertyList->isLeaf(at(i)->independent()) == PROPERTY_IS_TPH_LEAF)) && 
            (((at(i)->dependent() != NULL) && (ardPropertyList->isLeaf(at(i)->dependent()) == PROPERTY_IS_TPH_LEAF))))
               res->append(at(i));
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje wszystkie wezly od ktorych jest zalezny wezel wejsciowy
//
// #param QString _id - identyfikator wlasciwosci (ARD_Property) wejscowej
// #return liste wezlow od ktorych dana wlasnosc zalezy bezposrednio lub posrednio lacznie z nia sama, jezeli NULL to blad
QList<ARD_Property*>* ARD_DependencyList::findFamily(QString _id)
{
     if(ardPropertyList->indexOfID(_id) == -1)
          return NULL;

     QList<ARD_Property*>* _family = new QList<ARD_Property*>;
     findParents(_id, _family);
     return _family;
}
// -----------------------------------------------------------------------------

// #brief Funckja rekurencyjna wyszukujaca rodzicow danej wlasnosci
//
// #param QString _id - identyfikator wlasciwosci (ARD_Property) wejscowej
// #param QList<ARD_Property*>* _currentFamily - dotychczas utworzona rodzina
// #return brak zwracanych wartosci
void ARD_DependencyList::findParents(QString _id, QList<ARD_Property*>* _currentFamily)
{
     // Wyszukiwanie listy wezlow od ktturych _id jest bezposrednio zalezny
     QList<ARD_Dependency*>* directDep = indexOfinout("", _id);
     
     // Dodawanie do listy rodziny wyszukanych zaleznosci
     for(int i=0;i<directDep->size();++i)
          if(!_currentFamily->contains(directDep->at(i)->independent()))
          {
               _currentFamily->append(directDep->at(i)->independent());
               findParents(directDep->at(i)->independent()->id(), _currentFamily);
          }
          
     delete directDep;
}
// -----------------------------------------------------------------------------

// #brief Funckja rekurencyjna sprawdzajaca czy zaleznosc _id zalezy posrednio lub bezposrednio od _dep
//
// #param QString _id - identyfikator wlasciwosci (ARD_Property) ktorej zlaeznosc chcemy sprawdzic
// #param QString _dep - identyfikator wlasciwosci docelowej
// #return true jezeli _id zalezy od _dep inaczej false
bool ARD_DependencyList::checkDependence(QString _id, QString _dep, QList<ARD_Property*>* _checkedProperites)
{
     bool delList = false;
     if(_checkedProperites == NULL)
     {
          if(ardPropertyList->indexOfID(_dep) == -1)
               return false;
               
          _checkedProperites = new QList<ARD_Property*>;
          delList = true;
     }
     if(ardPropertyList->indexOfID(_id) == -1)
          return false;
     
     // Wyszukiwanie listy wezlow od ktturych _id jest bezposrednio zalezny
     QList<ARD_Dependency*>* directDep = indexOfinout("", _id);
     
     // Dodawanie do listy rodziny wyszukanych zaleznosci
     for(int i=0;i<directDep->size();++i)
     {
          if(!_checkedProperites->contains(directDep->at(i)->independent()))
          {
               _checkedProperites->append(directDep->at(i)->independent());
               if(directDep->at(i)->independent()->id() == _dep)
                    return true;
               if(checkDependence(directDep->at(i)->independent()->id(), _dep, _checkedProperites))
                    return true;
          }
     }
          
     delete directDep;
     if(delList)
          delete _checkedProperites;
     
     return false;
}
// -----------------------------------------------------------------------------
 
// #brief Zwraca czy obiekty byly modyfikowane
//
// #return true jezeli jakikolwiek element pojemnika byl modyfikowany, inaczej false
bool ARD_DependencyList::changed(void)
{
     if(fChanged)
          return true;
          
     for(int i=0;i<size();++i)
          if(at(i)->changed())
               return true;
     
     return false;
}
// -----------------------------------------------------------------------------

// #brief Ustawia status zmiany na zadana wartosc
//
// #param bool _c - wartosc statusu
// #return brak zwraanych wartosci
void ARD_DependencyList::setChanged(bool _c)
{
     fChanged = _c;
     
     for(int i=0;i<size();++i)
          at(i)->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_DependencyList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku ARDML w formacie tymczasowym
//
// #param QStringList* list - Wskanik do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_DependencyList::fromARDML_wv(QStringList* list)      
{
     bool res = true;

     // Usuwanie spacji i tabulatorow wierszach
     for(int i=0;i<list->size();i++)
     {
          QString line = list->at(i);
          while(line.contains("\t"))
          {
               int index = line.indexOf("\t");
               line = line.remove(index, 1);
          }
          list->replace(i, line);
     }

     // Zakladamy ze pierwszy wiersz jest naglowkiem
     if(!list->at(0).contains("<ard>", Qt::CaseInsensitive))
          return false;
     if(!list->contains("</ard>", Qt::CaseInsensitive))
          return false;

     // Reszta zakladamy ze jest ok

     // Spis tagow
     QStringList tags;
     tags.clear();
     tags << "<ard>" << "</ard>" << "<dep";

     // Sluza do odpowiedniego kopiowania lini
     bool copy = false;
     QStringList buf;
     buf.clear();
     
     // Wskazniki do aktualnie wczytywanych obiektow
     QString currentSection = "";

     for(int line=0;line<list->size();line++)
     {
          //QMessageBox::warning(NULL, "Wczytywanie zaleznosci", list->at(line));
          // Sprawdzanie wystapienia tagu
          int action = -1;
          for(int tag=0;tag<tags.size();tag++)
               if(list->at(line).contains(tags.at(tag), Qt::CaseInsensitive))
                    action = tag;

          // Kopiowanie lini danych                               
          if(copy)
               buf.append(list->at(line));

          if(action == -1)
               continue;
               
          //QMessageBox::critical(NULL, "HQEd", "Action = " + QString::number(action), QMessageBox::Ok);

          QString l = list->at(line);
          l = l.remove(l.indexOf(tags.at(action), Qt::CaseInsensitive), tags.at(action).length());
          l = l.mid(0, l.indexOf("<", Qt::CaseInsensitive));


          // Wczytywanie parametrow w zaleznosci od wykrytego tagu
          
          // "<ard>"
          if(action == 0)
               currentSection = "ard";
               
          // "</ard>"
          if(action == 1)
               currentSection = "";
          
          // "<dep"
          if(action == 2)
          {
               if(currentSection != "ard")
               {
                    QString error = "Misplaced \"dep\" tag.";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               QString dep = hqed::XML_ReadValue(list->at(line), "dependent");
               QString indep = hqed::XML_ReadValue(list->at(line), "independent");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;dep.contains(" ");)
                    dep = dep.remove(dep.indexOf(" "), 1);
               for(;indep.contains(" ");)
                    indep = indep.remove(indep.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((dep == "") || (dep == "#NAN#"))
               {
                    QString error = "Missing \"dependency\" parameter \"dependent\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if((indep == "") || (indep == "#NAN#"))
               {
                    QString error = "Missing \"dependency\" parameter \"independent\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Teraz wyszukujemy obiekty property wedlug identyfikatorow
               int depIndex = ardPropertyList->indexOfID(dep);
               int indepIndex = ardPropertyList->indexOfID(indep);
               
               // Jezeli podany identyfikator nie istnieje
               if(depIndex == -1)
               {     
                    //QString error = "Undeclared dependency tag name <b>" + dep + "</b>.\nError at line: " + list->at(line) + ".";
                    QString error = "Undeclared dependency tag name " + dep + ".\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if(indepIndex == -1)
               {     
                    QString error = "Undeclared dependency tag name " + indep + ".\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Zaleznosci mozemy w pliku zapisac tyko dla property ktore sa lisciami w diagramie TPH
               // Sprawdzanie czy podane zaleznosci to liscie w diagramie TPH
               if(ardPropertyList->isLeaf(ardPropertyList->at(indepIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
               {
                    QString error = "Property, which has tag name equal " + ardPropertyList->at(indepIndex)->id() + ", is not a TPH tree leaf.\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if(ardPropertyList->isLeaf(ardPropertyList->at(depIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
               {
                    QString error = "Property, which has tag name equal " + ardPropertyList->at(depIndex)->id() + ", is not a TPH tree leaf.\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Ustawiamy wskazniki
               ARD_Dependency* newDep = add();
               newDep->setIndependent(ardPropertyList->at(indepIndex));
               newDep->setDependent(ardPropertyList->at(depIndex));
          }
     }

      
     // Sprawdzenie dzialania funckji sprawdzajacej zalznosc
     /* for(int i=0;i<10;i++)
     {
          bool ok;
          QString ids = QInputDialog::getText(NULL, "HQEd", "Property id:", QLineEdit::Normal, QDir::home().dirName(), &ok);
          if (ok && !ids.isEmpty())
          {
               QStringList l = ids.split(";");
               bool dep = checkDependence(l.at(0), l.at(1));
               if(dep)
                    QMessageBox::critical(NULL, "HQEd", "true", QMessageBox::Ok);
               if(!dep)
                    QMessageBox::critical(NULL, "HQEd", "false", QMessageBox::Ok);
               
          }
     } */
     // Odswiezenie widoku
     // delete fGTable;
     // fGTable = new GTable(this);
     // fGTable->ReCreate();
     // fGTable->Update();
     // fGTable->RePosition();

     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku ARDML w formacie tymczasowym
//
// #param QDomElement& root - Referencja do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_DependencyList::fromARDML_wv(QDomElement& root)      
{
     if(root.tagName().toLower() != "ard")
          return false;

     bool res = true;

     QDomElement child = root.firstChildElement("dep");
     while(!child.isNull())
     {
          QString dep = child.attribute("dependent", "#NAN#");
          QString indep = child.attribute("independent", "#NAN#");
          
          // Usuwanie spacji z wczytanych wartosci
          for(;dep.contains(" ");)
               dep = dep.remove(dep.indexOf(" "), 1);
          for(;indep.contains(" ");)
               indep = indep.remove(indep.indexOf(" "), 1);
          
          // Jezeli nie podamy identyfikatora to blad
          if((dep == "") || (dep == "#NAN#"))
          {
               QString error = "Missing \"dependency\" parameter \"dependent\" at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          if((indep == "") || (indep == "#NAN#"))
          {
               QString error = "Missing \"dependency\" parameter \"independent\" at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          
          // Teraz wyszukujemy obiekty property wedlug identyfikatorow
          int depIndex = ardPropertyList->indexOfID(dep);
          int indepIndex = ardPropertyList->indexOfID(indep);
          
          // Jezeli podany identyfikator nie istnieje
          if(depIndex == -1)
          {     
               //QString error = "Undeclared dependency tag name <b>" + dep + "</b>.\nError at line: " + list->at(line) + ".";
               QString error = "Undeclared dependency tag name " + dep + ".\nError at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          if(indepIndex == -1)
          {     
               QString error = "Undeclared dependency tag name " + indep + ".\nError at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          
          // Zaleznosci mozemy w pliku zapisac tyko dla property ktore sa lisciami w diagramie TPH
          // Sprawdzanie czy podane zaleznosci to liscie w diagramie TPH
          if(ardPropertyList->isLeaf(ardPropertyList->at(indepIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
          {
               QString error = "Property, which has tag name equal " + ardPropertyList->at(indepIndex)->id() + ", is not a TPH tree leaf.\nError at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          if(ardPropertyList->isLeaf(ardPropertyList->at(depIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
          {
               QString error = "Property, which has tag name equal " + ardPropertyList->at(depIndex)->id() + ", is not a TPH tree leaf.\nError at line " + QString::number(child.lineNumber()) + ".";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          
          // Ustawiamy wskazniki
          ARD_Dependency* newDep = add();
          newDep->setIndependent(ardPropertyList->at(indepIndex));
          newDep->setDependent(ardPropertyList->at(depIndex));
          
          // Wczytanie kolejnej zaleznosci
          child = child.nextSiblingElement("dep");
     }

      
     // Sprawdzenie dzialania funckji sprawdzajacej zalznosc
     /* for(int i=0;i<10;i++)
     {
          bool ok;
          QString ids = QInputDialog::getText(NULL, "HQEd", "Property id:", QLineEdit::Normal, QDir::home().dirName(), &ok);
          if (ok && !ids.isEmpty())
          {
               QStringList l = ids.split(";");
               bool dep = checkDependence(l.at(0), l.at(1));
               if(dep)
                    QMessageBox::critical(NULL, "HQEd", "true", QMessageBox::Ok);
               if(!dep)
                    QMessageBox::critical(NULL, "HQEd", "false", QMessageBox::Ok);
               
          }
     } */

     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku HML w formacie 1.0
//
// #param QDomElement& root - Referencja do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_DependencyList::fromHML_1_0(QDomElement& root)
{
     // Until format ARDMLtv is the same as HML 1.0
     return fromARDML_wv(root);
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_DependencyList::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     if(size() == 0)
          return res;

     *res << "<ard>";
     for(int i=0;i<size();++i)
     {
          // zaleznosci zapisujemy tylko dla lisci drzewa tph
          if((ardPropertyList->isLeaf(at(i)->independent()) == PROPERTY_IS_NOT_TPH_LEAF) ||
             (ardPropertyList->isLeaf(at(i)->dependent()) == PROPERTY_IS_NOT_TPH_LEAF))
               continue;

          QStringList* tmp = at(i)->out_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }
     *res << "</ard>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_DependencyList::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     if(size() == 0)
          return;

     _xmldoc->writeStartElement("ard");
     for(int i=0;i<size();++i)
     {
          // zaleznosci zapisujemy tylko dla lisci drzewa tph
          if((ardPropertyList->isLeaf(at(i)->independent()) == PROPERTY_IS_TPH_LEAF) &&
             (ardPropertyList->isLeaf(at(i)->dependent()) == PROPERTY_IS_TPH_LEAF))
                    at(i)->out_HML_2_0(_xmldoc);
     }
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int ARD_DependencyList::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     if(__root.tagName().toLower() != "ard")
          return 0;

     bool res = true;

     QString elementName = "dep";
     QDomElement child = __root.firstChildElement(elementName);
     while(!child.isNull())
     {
          QString dep = child.attribute("dependent", "#NAN#");
          QString indep = child.attribute("independent", "#NAN#");
          
          // Usuwanie spacji z wczytanych wartosci
          for(;dep.contains(" ");)
               dep = dep.remove(dep.indexOf(" "), 1);
          for(;indep.contains(" ");)
               indep = indep.remove(indep.indexOf(" "), 1);
          
          // Jezeli nie podamy identyfikatora to blad
          if((dep == "") || (dep == "#NAN#"))
          {
               __msg += "\n" + hqed::createErrorString("", "", "Missing \'dependent\' parameter in \'" + elementName + "\' element.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          if((indep == "") || (indep == "#NAN#"))
          {    
               __msg += "\n" + hqed::createErrorString("", "", "Missing \'independent\' parameter in \'" + elementName + "\' element.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          
          // Teraz wyszukujemy obiekty property wedlug identyfikatorow
          int depIndex = ardPropertyList->indexOfID(dep);
          int indepIndex = ardPropertyList->indexOfID(indep);
          
          // Jezeli podany identyfikator nie istnieje
          if(depIndex == -1)
          {     
               __msg += "\n" + hqed::createErrorString("", "", "Incorrect dependency reference \'dependent\' = \'" + dep + "\'.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          if(indepIndex == -1)
          {    
               __msg += "\n" + hqed::createErrorString("", "", "Incorrect dependency reference \'independent\' = \'" + indep + "\'.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          
          // Zaleznosci mozemy w pliku zapisac tyko dla property ktore sa lisciami w diagramie TPH
          // Sprawdzanie czy podane zaleznosci to liscie w diagramie TPH
          if(ardPropertyList->isLeaf(ardPropertyList->at(indepIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Property \'" + ardPropertyList->at(indepIndex)->id() + "\' is not a TPH tree leaf.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          if(ardPropertyList->isLeaf(ardPropertyList->at(depIndex)) == PROPERTY_IS_NOT_TPH_LEAF)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Property \'" + ardPropertyList->at(depIndex)->id() + "\' is not a TPH tree leaf.\nThe object will be omited." + hqed::createDOMlocalization(child), 0, 2);
               res = 1;
               child = child.nextSiblingElement(elementName);
               continue;
          }
          
          // Ustawiamy wskazniki
          ARD_Dependency* newDep = add();
          newDep->setIndependent(ardPropertyList->at(indepIndex));
          newDep->setDependent(ardPropertyList->at(depIndex));
          
          // Wczytanie kolejnej zaleznosci
          child = child.nextSiblingElement(elementName);
     }

     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------
