 /**
 *                                                                             *
 * @file ARD_Attribute.cpp                                                     *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_Attribute.cpp,v 1.32.4.2.2.2 2011-08-31 02:08:22 hqeddoxy Exp $                *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 21.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_Attribute                   *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_ard.h"
#include "../../namespaces/ns_hqed.h"
#include "../../C/MainWin_ui.h"
#include "../XTT/XTT_Attribute.h"
#include "ARD_Attribute.h"
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_Attribute::ARD_Attribute(void)
{
	fID = idPrefix();
	fName = "";
	fAttrDesc = "";
	fXTTequivalent = NULL;
	fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #param QString _id - Identyfikator atrybutu
// #return Brak zwracanych wartosci
ARD_Attribute::ARD_Attribute(QString _id)
{
	fID = _id;
	fName = "";
	fAttrDesc = "";
	fXTTequivalent = NULL;
	fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #param QString _id - Identyfikator atrybutu
// #param QString _name - nazwa atrybutu
// #return Brak zwracanych wartosci
ARD_Attribute::ARD_Attribute(QString _id, QString _name)
{
	fID = _id;
	fName = _name;
	fAttrDesc = "";
	fXTTequivalent = NULL;
	fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Zwraca typ atrybutu: ARD_ATT_CONCEPTUAL, ARD_ATT_PHYSICAL
//
// #return Zwraca typ atrybutu: ARD_ATT_CONCEPTUAL, ARD_ATT_PHYSICAL, lub -1 gdy nie mozna okreslic typu
int ARD_Attribute::type(void)
{
	int res = -1;
	
	if((fName.length() > 0) && (fName.data()[0].isUpper()))
		return ARD_ATT_CONCEPTUAL;
	if((fName.length() > 0) && (fName.data()[0].isLower()))
		return ARD_ATT_PHYSICAL;
		
	return res;
}
// -----------------------------------------------------------------------------

// #brief Zwraca ID atrybutu
//
// #return Zwraca ID atrybutu, jezeli 0 to id nie jest okreslone
QString ARD_Attribute::id(void)
{
	return fID;
}
// -----------------------------------------------------------------------------


// #brief Zwraca nazwe atrybutu
//
// #return Zwraca nazwe atrybutu, jezeli ciag pusty to nazwa nie jest okresona
const QString& ARD_Attribute::name(void) const
{
	return fName;
}
// -----------------------------------------------------------------------------

// #brief Zwraca opis atrybutu
//
// #return Zwraca opis atrybutu
QString ARD_Attribute::description(void)
{
	return fAttrDesc;
}
// -----------------------------------------------------------------------------

// #brief Zwraca wskaznik do obiektu XTT_Attribute, ktory jest tym samym atrybutem tylko dobrze zdefiniowanym
//
// #return Zwraca wskaznik do obiektu XTT_Attribute, ktory jest tym samym atrybutem tylko dobrze zdefiniowanym, jezeli NULL to nie ma odpowiednika
XTT_Attribute* ARD_Attribute::xttEquivalent(void)
{
	return fXTTequivalent;
}
// -----------------------------------------------------------------------------

// #brief Zwraca czy dany atrybut byl zmieniany
//
// #return Zwraca czy dany atrybut byl zmieniany
bool ARD_Attribute::changed(void)
{
	return fChanged;
}
// -----------------------------------------------------------------------------

// #brief Ustawia nazwe atrybutu
//
// #param QString _newName - nowa nazwa atrybutu
// #return Zwraca true jezeli nazwa jest poprawana, inaczej false
bool ARD_Attribute::setName(QString _newName)
{
        // Sprawdzanie nowej nazwy
	if((_newName.length() == 0) || (!_newName[0].isLetter()))
		return false;
		
	fName = _newName;
	
	// Zapisanie statusu zmiany
	fChanged = true;
	
	return true;
}
// -----------------------------------------------------------------------------

// #brief Ustawia i atrybutu
void ARD_Attribute::setId(QString __id)
{
     fID = __id;
}
// -----------------------------------------------------------------------------

// #brief Sets attributes description
//
// #param QString _newDesc - new description
// #return No return value
void ARD_Attribute::setDescription(QString _newDesc)
{
	fAttrDesc = _newDesc;
	
	// Zapisanie statusu zmiany
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Sets status of attribute change
//
// #param bool _c - new status
// #return No return value
void ARD_Attribute::setChanged(bool _c)
{
	fChanged = _c;
}
// -----------------------------------------------------------------------------


// #brief Ustawia ekwiwalnet atrybutu
//
// #param XTT_Attribute* _newEq - wskaznik do nowego ekwiwalentu
// #return brak zwracanych wartosci
void ARD_Attribute::setXTTequivalent(XTT_Attribute* _newEq)
{
	fXTTequivalent = _newEq;
	
	// Zapisanie statusu zmiany
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_Attribute::eventMessage(QString* /*__errmsgs*/, int __event, ...)
{
     va_list argsptr;
          
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          /*va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          
          va_end(argsptr);*/
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          if(fXTTequivalent == attr)
               fXTTequivalent = NULL;
          
          va_end(argsptr);
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_Attribute::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     QString init_line = "<attr id=\"" + id() + "\"";
     init_line += " type=\"" + ard::defaultArdAttributeTypeId() + "\"";
     init_line += " name=\"" + name() + "\"";
     init_line += ">";
     *res << init_line;

     if(!fAttrDesc.isEmpty())
          *res << "\t<desc>" + description() + "</desc>";

     *res << "</attr>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_Attribute::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     _xmldoc->writeStartElement("attr");
     _xmldoc->writeAttribute("id", id());
     _xmldoc->writeAttribute("name", name());
     _xmldoc->writeAttribute("type", ard::defaultArdAttributeTypeId());
     if(!fAttrDesc.isEmpty())
          _xmldoc->writeTextElement("desc", description());
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString ARD_Attribute::idPrefix(void)
{
     return "att_ard_";
}
// -----------------------------------------------------------------------------
