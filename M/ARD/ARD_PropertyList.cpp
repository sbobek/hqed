 /**
 *                                                                             *
 * @file ARD_PropertyList.cpp                                                  *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_PropertyList.cpp,v 1.28.4.5.2.5 2011-08-31 19:25:02 hqeddoxy Exp $             *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 25.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_PropertyList                *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../C/widgets/hBaseScene.h"
#include "../../V/ARD/gTPHtree.h"
#include "../../namespaces/ns_hqed.h"
#include "../XTT/XTT_Attribute.h"
#include "ARD_Property.h"
#include "ARD_Attribute.h"
#include "ARD_AttributeList.h"
#include "ARD_PropertyList.h"
// -----------------------------------------------------------------------------

ARD_PropertyList* ardPropertyList;
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_PropertyList::ARD_PropertyList(void) : QList<ARD_Property*>()
{
     fChanged = false;
     TPHdiagram = new gTPHtree(this);
}
// -----------------------------------------------------------------------------

// #brief Destruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_PropertyList::~ARD_PropertyList(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
     delete TPHdiagram;
}
// -----------------------------------------------------------------------------

gTPHtree* ARD_PropertyList::tphTree(void) const
{
     return TPHdiagram;
}
// -----------------------------------------------------------------------------

// #brief Zwraca tresc ostatniego bledu
//
// #return tresc ostatniego bledu
QString ARD_PropertyList::error(void)
{
     return fError;
}
// -----------------------------------------------------------------------------

// #brief Oproznia pojemnik
//
// #return wskaznik do nowo utworznonego atrybutu
void ARD_PropertyList::flush(void)
{
     while(size() > 0)
     {
          delete takeAt(0);
     }
          
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Refreshes the TPH display
//
// #return no values return
void ARD_PropertyList::refreshDisplay(void)
{
     TPHdiagram->refresh();
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy obiekt
//
// #return wskaznik do nowo utworzonego obiektu
ARD_Property* ARD_PropertyList::add(void)
{
     QString _newID = findUniqID();
     
     // Tworzenie nowego obiektu
     ARD_Property* tmp = new ARD_Property(_newID);
     add(tmp);
     
     fChanged = true;
     return tmp;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut o podanym adresie
//
// #param ARD_Property* _new - adres nowego obiektu do dodania
// #return brak zwracanych wartosci
void ARD_PropertyList::add(ARD_Property* _new)
{
     // Sprawdzamy czy taki atrybut juz nie istnieje
     if(contains(_new))
          return;
          
     fChanged = true;
     append(_new);
}
// -----------------------------------------------------------------------------

// #brief Deletes the given Property
//
// #param int _index - index obiektu do usuniecia
// #return brak zwracanych wartosci
void ARD_PropertyList::remove(int _index)
{
     // Sprawdzanie poprawnosci indexu
     if((_index < 0) || (_index >= size()))
          return;
          
     ARD_Property* forRemove = at(_index);
          
     // Usuwanie potomkow
     QList<ARD_Property*>* cl = childList(forRemove);
     for(int i=0;i<cl->size();++i)
          remove(indexOf(cl->at(i)));
     delete cl;
     
     // Usuwanie obiektu
     delete takeAt(indexOf(forRemove));
     
     fChanged = true;
}
// -----------------------------------------------------------------------------


void ARD_PropertyList::removeChildren(ARD_Property *prop)
{
     QList<ARD_Property*>* cl = childList(prop);
     for(int i=0;i<cl->size();++i)
          remove(indexOf(cl->at(i)));
     delete cl;
}
// -----------------------------------------------------------------------------

bool ARD_PropertyList::updatePropertyWithParents(ARD_Property *_changedProperty, const QList<ARD_Attribute *> &_newAttributes)
{
     if (_changedProperty == NULL)
          return false;
     QList<ARD_Attribute*>* _oldAttributes = dynamic_cast<QList<ARD_Attribute*>*> (_changedProperty);
     QList<ARD_Attribute*> _missing = hqed::listSubtraction(_newAttributes,*_oldAttributes);//all of these will be added
     QList<ARD_Attribute*> _obsolete = hqed::listSubtraction(*_oldAttributes,_newAttributes); // all of these will be deleted

     ARD_Property* tmpProp = _changedProperty;

     _changedProperty->setAttList(_newAttributes);
     tmpProp =  ardPropertyList->tphParent(tmpProp);

     while( tmpProp != NULL )
     {
          if (tmpProp->size() == 1 && (tmpProp->first()->type() == ARD_ATT_CONCEPTUAL))
          {
               return true;
          }
          else
          {
               tmpProp->add(_missing);
               tmpProp->subtract(_obsolete);
               tmpProp =  ardPropertyList->tphParent(tmpProp);

          }             
     }    
     return false;
}

// #brief Wyszukuje obiekty zawierajace arybut o podanej nazie
//
// #param QString _name - nazwa atrybutu
// #return liste obiektow zawierajace arybut o podanej nazwie
QList<ARD_Property*>* ARD_PropertyList::listOfAttributeName(QString _name)
{
     QList<ARD_Property*>* res = new QList<ARD_Property*>;

     for(int i=0;i<size();++i)
          if(at(i)->indexOfName(_name) > -1)
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty zawierajace arybut o podanym id
//
// #param _id - identyfiaktor atrybutu
// #return liste obiektow zawierajace arybut o podanym identyfikatorze
QList<ARD_Property*>* ARD_PropertyList::listOfAttributeID(QString _id)
{
     QList<ARD_Property*>* res = new QList<ARD_Property*>;

     for(int i=0;i<size();++i)
          if(at(i)->indexOfID(_id) > -1)
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Functions that finds and returns the unique id
//
// #return the unique id as string
QString ARD_PropertyList::findUniqID(void)
{
     int candidate = 1;
     QString strCandidate = "";
     
     do
     {
          strCandidate = ARD_Property::idPrefix() + QString::number(candidate);
          candidate++;
     }
     while(indexOfID(strCandidate) > -1);
     
     return strCandidate;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty bedace potomkami danego obiektu
//
// #param ARD_Property* _parent - wskaznik do obiektu ktorego potomkow chcemy wyszukac
// #return liste obiektow bedacych potomkami danego obiektu
QList<ARD_Property*>* ARD_PropertyList::childList(ARD_Property* _parent)
{
     QList<ARD_Property*>* res = new QList<ARD_Property*>;
     
     if(!contains(_parent))
          return res;

     for(int i=0;i<size();++i)
          if(at(i)->TPHparent() == _parent->id())
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

QList<ARD_Property*> * ARD_PropertyList::parentList(ARD_Property *_property) const
{
     QList<ARD_Property*>* parentList = new QList<ARD_Property*>;
     if (_property == NULL)
          return parentList;
     ARD_Property* tmp = ardPropertyList->tphParent(_property);
     while(tmp != NULL)
     {
          parentList->append(tmp);
          tmp = ardPropertyList->tphParent(_property);
     }
     return parentList;
}
// -----------------------------------------------------------------------------

QList<ARD_Attribute*>* ARD_PropertyList::attributesInParentsList(ARD_Property* _property) const
{
     QList<ARD_Attribute*>* ret = new QList<ARD_Attribute*>;
     if (_property == NULL)
          return ret;
     ARD_Property* tmp = ardPropertyList->tphParent(_property);
     ARD_Attribute* tmpAtt;
     QList<ARD_Attribute*>* tmpAttList;
     while (tmp != NULL)
     {
          tmpAttList = dynamic_cast<QList<ARD_Attribute*>*>(tmp);
          foreach(tmpAtt,*tmpAttList)
          {
               if (ret->contains(tmpAtt) == false)
                    ret->append(tmpAtt);
          }
          tmp = ardPropertyList->tphParent(tmp);
     }
     return ret;
}

// #brief Wyszukuje obiekt tkroy jest rodziem danego w diagramie TPH
//
// #param ARD_Property* _ptr - wskaznik do obiektu ktorego rodzica chcemy poznac
// #return wskaznik do rodzica, NULL jezeli nie ma rodzica
ARD_Property* ARD_PropertyList::tphParent(ARD_Property* _ptr)
{
     // Sprawdzanie czy przypadkiem wskaznik nie jest NULL
     if(_ptr == NULL)
     {
          fError = "Input pointer is not allocated";
          return NULL;
     }
     
     // Sprawdzanie czy wlasnosc jest w pojemniku
     if(!contains(_ptr))
     {
          fError = "Property " + _ptr->id() + " is not in list.";
          return NULL;
     }
          
     // Sprawdzanie czy dany obiekt ma ustalonego rodzica
     if(_ptr->TPHparent() == "")
     {
          fError = "Property " + _ptr->id() + " has not parent defined.";
          return NULL;
     }
     
     // Sprawdzanie czy dany obiekt nie jest juz ROOT'em
     if(_ptr->TPHparent() == "root")
     {
          fError = "Property " + _ptr->id() + " is root.";
          return NULL;
     }
          
     QString pID = _ptr->TPHparent();
     int pindex = indexOfID(pID);
     
     // Sprawdzanie czy dany rodzic istnieje
     if(pindex == -1)
     {
          fError = "Property " + _ptr->id() + " parent is not in the list.";
          return NULL;
     }
     
     return at(pindex);
}
// -----------------------------------------------------------------------------

ARD_Property* ARD_PropertyList::tphRoot(ARD_Property *_property)
{
     if (_property == NULL)
          return NULL;

     ARD_Property* tmp = _property;

     /* Because tphParent returns NULL at every possible problem, so if we don't get to the root
        we'll get a NULL pointer at tmp. */
     while (tmp != NULL && tmp->TPHparent() != "root")
          tmp = tphParent(tmp);
     return tmp;

}

// #brief Wyszukuje obiekty ktore sa lisciami na drzewie TPH
//
// #param QString _attName - opcjonalna nawa atrybutu, jezeli nie podamy to zwraca wszystkie obikety liscie
// #return liste obiektow - lisci diagramu TPH
QList<ARD_Property*>* ARD_PropertyList::listOfNotTransformatedProperties(QString _attName)
{
     QList<ARD_Property*>* res = new QList<ARD_Property*>;

     for(int i=0;i<size();++i)
          if((!at(i)->canBeTransformated()) && ((_attName == "") || (_attName == at(i)->at(0)->name())))
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje liscie drzewa TPH
//
// #return liste lisci drzewa TPH
QList<ARD_Property*>* ARD_PropertyList::listOfLeafs(void)
{
     QList<ARD_Property*>* res = new QList<ARD_Property*>;
     for(int i=0;i<size();++i)
          if(isLeaf(at(i)) == PROPERTY_IS_TPH_LEAF)
               res->append(at(i));
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje obiekty na podstawie identyfikatora
//
// #param unsigned int _id - wartosc identyfikatora
// #return index obiektu o podanym identyfikatorze, jezeli nie ma takiego obiektu to -1
int ARD_PropertyList::indexOfID(QString _id)
{
     for(int i=0;i<size();++i)
          if(at(i)->id() == _id)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje liczbe obiketow zadeklarowanych jako root'y w diagramie tph
//
// #return liczbe obiketow zadeklarowanych jako root'y w diagramie tph
int ARD_PropertyList::rootsCount(void)
{
     int res = 0;
     
     for(int i=0;i<size();++i)
          if(at(i)->TPHparent().toLower() == "root")
               res++;
               
     return res;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje index korzenia diagramu TPH
//
// #return index obiektu w liscie, jezeli nie ma to -1
int ARD_PropertyList::indexOfRoot(void)
{
     for(int i=0;i<size();++i)
          if(at(i)->TPHparent().toLower() == "root")
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* ARD_PropertyList::display(void)
{
     return TPHdiagram->display();
}
// -----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void ARD_PropertyList::setDisplay(hBaseScene* __display)
{
     TPHdiagram->setDisplay(__display);
}
// -----------------------------------------------------------------------------
 
// #brief Zwraca czy obiekty byly modyfikowane
//
// #return true jezeli jakikolwiek element pojemnika byl modyfikowany, inaczej false
bool ARD_PropertyList::changed(void)
{
     if(fChanged)
          return true;
          
     for(int i=0;i<size();++i)
          if(at(i)->changed())
               return true;
     
     return false;
}
// -----------------------------------------------------------------------------

// #brief Ustawia status zmiany na zadana wartosc
//
// #param bool _c - wartosc statusu
// #return brak zwracanych wartosci
void ARD_PropertyList::setChanged(bool _c)
{
     fChanged = _c;
     
     for(int i=0;i<size();++i)
          at(i)->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc identyfikatora obiektu
//
// #param ARD_Property* _property - obiekt ktorego identyfikator chcemy zmienic
// #param QString _id - wartosc identyfikatora
// #return true jezeli wszystko ok, inaczej false
bool ARD_PropertyList::setPropertyID(ARD_Property* _property, QString _id)
{
     // Jezeli nie ma takiej wlasnosci w pojemniku
     if(!contains(_property))
          return false;
          
     // Jezeli id jest bledne
     if(_id == "")
          return false;
          
     // Jezeli wskaznik jest NULL
     if(_property == NULL)
          return false;
     
     // Jezeli identyfikator juz istnieje
     if(indexOfID(_id) > -1)
          return false;
          
     _property->setID(_id);
          
     fChanged = true;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc rodzica obiektu w diagramie tph
//
// #param ARD_Property* _property - obiekt ktorego rodzica chcemy zmienic
// #param ARD_Property* _parent - rodzic dla danego obiektu
// #return true jezeli wszystko ok, inaczej false
bool ARD_PropertyList::setTPHparent(ARD_Property* _property, ARD_Property* _parent)
{
     // Jezeli nie ma takiej wlasnosci w pojemniku
     if(!contains(_property))
          return false;
          
     // Jezeli nie istnieje rodzic
     if((_parent != NULL) && (!contains(_parent)))
          return false;
          
     // Jezeli wskaznik jest NULL
     if(_property == NULL)
          return false;
     
     // Jezeli _parent jest NULL to znaczy ze chcemy jako rodzica ustawic ROOT
     if((_parent == NULL) && (indexOfRoot() > -1))
          return false;
     
     // Jezeli wybrany obiekt chcemy ustawic jako root
     if(_parent == NULL)
          _property->setTPHparent("root");
          
     // Jezeli wybrany obiekt chcemy ustawic jako potomka obiektu podanego jako _parent
     if(_parent != NULL)
          _property->setTPHparent(_parent->id());
          
     fChanged = true;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Metoda sprawdzajaca czy dany element jest lisciem w diagramie TPH
//
// #param ARD_Property* _property - Wska�nik do obiektu ktorego checemy sprawdzic
// #return PROPERTY_IS_TPH_LEAF jezeli jest lisciem, PROPERTY_IS_NOT_TPH_LEAF gdy nie jest lisciem, PROPERTY_IS_NOT_EXIST gdy nie ma takiej wlsciwosci
int ARD_PropertyList::isLeaf(ARD_Property* _property)
{
     // Gdy nie ma takiego adresu w pojemnku
     if(!contains(_property))
          return PROPERTY_IS_NOT_EXIST;
          
     // Sprawdzanie czy nie istnieje wlasciwosc ktora ma ten element jako rodzica
     for(int i=0;i<size();++i)
          if(at(i)->TPHparent() == _property->id())
               return PROPERTY_IS_NOT_TPH_LEAF;
          
     return PROPERTY_IS_TPH_LEAF;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_PropertyList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku ARDML w formacie tymczasowym
//
// #param QStringList* list - Wska�nik do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_PropertyList::fromARDML_wv(QStringList* list)      
{
     bool res = true;
     fChanged = true;

     // Usuwanie spacji i tabulatorow wierszach
     for(int i=0;i<list->size();i++)
     {
          QString line = list->at(i);
          while(line.contains("\t"))
          {
               int index = line.indexOf("\t");
               line = line.remove(index, 1);
          }
          list->replace(i, line);
     }

     // Zakladamy ze pierwszy wiersz jest naglowkiem
     if(!list->at(0).contains("<properties>", Qt::CaseInsensitive))
          return false;
     if(!list->contains("</properties>", Qt::CaseInsensitive))
          return false;

     // Reszta zakladamy ze jest ok

     // Spis tagow
     QStringList tags;
     tags.clear();
     tags << "<properties>" << "</properties>" << "<property" << "<att" << "</property>";
     tags << "<tph>" << "</tph>" << "<hist";

     // Sluza do odpowiedniego kopiowania lini
     bool copy = false;
     QStringList buf;
     buf.clear();
     
     // Wskazniki do aktualnie wczytywanych obiektow
     ARD_Property* currentProperty = NULL;
     ARD_Attribute* currentAttribute = NULL;
     QString currentSection = "";

     for(int line=0;line<list->size();line++)
     {
          //QMessageBox::warning(NULL, "tworzenie tabeli", list->at(line));
          // Sprawdzanie wystapienia tagu
          int action = -1;
          for(int tag=0;tag<tags.size();tag++)
               if(list->at(line).contains(tags.at(tag), Qt::CaseInsensitive))
                    action = tag;

          // Kopiowanie lini danych                               
          if(copy)
               buf.append(list->at(line));

          if(action == -1)
               continue;
               
          //QMessageBox::critical(NULL, "HQEd", "Action = " + QString::number(action), QMessageBox::Ok);

          QString l = list->at(line);
          l = l.remove(l.indexOf(tags.at(action), Qt::CaseInsensitive), tags.at(action).length());
          l = l.mid(0, l.indexOf("<", Qt::CaseInsensitive));


          // Wczytywanie parametrow w zaleznosci od wykrytego tagu
          
          // "<properties>"
          if(action == 0)
               currentSection = "properties";
               
          // "</properties>"
          if(action == 1)
               currentSection = "";
          
          // "<property"
          if(action == 2)
          {
               if(currentSection != "properties")
               {
                    QString error = "Misplaced \"property\" tag.";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               QString pid = hqed::XML_ReadValue(list->at(line), "pid");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;pid.contains(" ");)
                    pid = pid.remove(pid.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((pid == "") || (pid == "#NAN#"))
               {
                    QString error = "Missing \"property\" parameter \"pid\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli podany identyfikator juz istnieje
               if(indexOfID(pid) > -1)
               {     
                    QString error = "Tag name \"" + pid + "\" already exists.\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli ok to ustawiamy wartosci
               currentProperty = add();
               setPropertyID(currentProperty, pid);
          }
          
          // "<att"
          if(action == 3)
          {
               QString name = hqed::XML_ReadValue(list->at(line), "name");
               QString desc = hqed::XML_ReadValue(list->at(line), "description");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;name.contains(" ");)
                    name = name.remove(name.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((name == "") || (name == "#NAN#"))
               {
                    QString error = "Missing \"attribute\" parameter \"name\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Wyszukiwanie atrybutu w liscie atrybutuow
               int aindex = ardAttributeList->indexOfName(name);
               
               // Jezeli podany atrybut juz istnieje
               if(aindex > -1)
                    currentAttribute = ardAttributeList->at(aindex);
               
               // Jezeli podany atrybut jeszcze nie istnieje
               if(aindex == -1)
               {
                    currentAttribute = ardAttributeList->add();
                    currentAttribute->setName(name);
               }
               
               if(currentProperty == NULL)
               {
                    QString error = "Misplaced \"attribute\" tag.";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Dodanie atrybutu do bierzacej wlasciwosci
               if(!currentProperty->add(currentAttribute))
               {
                    QString error = "The attribute " + currentAttribute->name() + " is already exists in this property.\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
          }  

          // "</property>"
          if(action == 4)
          {
               currentProperty = NULL;
          }
          
          // "</tph>"
          if(action == 5)
               currentSection = "tph";
               
          // "</tph>"
          if(action == 6)
               currentSection = "";
          
          // "<hist"
          if(action == 7)
          {
               if(currentSection != "tph")
               {
                    QString error = "Misplaced \"hist\" tag.";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               QString parent = hqed::XML_ReadValue(list->at(line), "parent");
               QString child = hqed::XML_ReadValue(list->at(line), "child");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;parent.contains(" ");)
                    parent = parent.remove(parent.indexOf(" "), 1);
               for(;child.contains(" ");)
                    child = child.remove(child.indexOf(" "), 1);
               
               // Jezeli nie podamy rodzica to blad
               if((parent == "") || (parent == "#NAN#"))
               {
                    QString error = "Missing \"attribute\" parameter \"parent\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli nie potomka to blad
               if((child == "") || (child == "#NAN#"))
               {
                    QString error = "Missing \"attribute\" parameter \"child\" at line:\n" + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie czy istnieja wlasnosci o wczytanych identyfikatorach
               if((parent.toLower() != "root") && (indexOfID(parent) == -1))
               {
                    QString error = "Undeclared parent property \"" + parent + "\".\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if(indexOfID(child) == -1)
               {
                    QString error = "Undeclared child property \"" + child + "\".\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli tu doszlismy to znaczy ze wszystko jest ok
               // a wiec mozemy zaczac ustawiac zaleznosci
               
               // Wyszukiwanie obiektu
               int childIndex = indexOfID(child);
               
               // Ustalanie rodzica
               ARD_Property* parentPtr = NULL;
               if(parent.toLower() != "root")
                    parentPtr = at(indexOfID(parent));
               
               if(!setTPHparent(at(childIndex), parentPtr))
               {
                    QString error = "Parent definition error for property \"" + child + "\".\nError at line: " + list->at(line) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
          }
     }
     
     int rootIndex = indexOfRoot();
     if(rootIndex == -1)
     {
          QString error = "Root of TPH tree not defined.";
          QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
          return false;
     }

     TPHdiagram->setParents();
     TPHdiagram->createTree(at(rootIndex));
     // Odswiezenie widoku
     // delete fGTable;
     // fGTable = new GTable(this);
     // fGTable->ReCreate();
     // fGTable->Update();
     // fGTable->RePosition();
     
     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku ARDML w formacie tymczasowym
//
// #param QDomElement& root - Referencja do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_PropertyList::fromARDML_wv(QDomElement& root)      
{
     if((root.tagName().toLower() != "properties") && (root.tagName().toLower() != "tph"))
          return false;
          
     bool res = true;
     fChanged = true;
     
     if(root.tagName().toLower() == "properties")
     {
          // Wskazniki do aktualnie wczytywanych obiektow
          ARD_Property* currentProperty = NULL;
          ARD_Attribute* currentAttribute = NULL;
     
          QDomElement child = root.firstChildElement("property");
          while(!child.isNull())
          {
               // Wczytywanie parametrow w zaleznosci od wykrytego tagu
               QString pid = child.attribute("pid", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;pid.contains(" ");)
                    pid = pid.remove(pid.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((pid == "") || (pid == "#NAN#"))
               {
                    QString error = "Missing \"property\" parameter \"pid\" at line " + QString::number(child.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli podany identyfikator juz istnieje
               if(indexOfID(pid) > -1)
               {     
                    QString error = "Tag name \"" + pid + "\" already exists.\nError at line " + QString::number(child.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli ok to ustawiamy wartosci
               currentProperty = add();
               setPropertyID(currentProperty, pid);
                    
               
               // "<att" - wczytywanie atrybutow
               QDomElement DOMatt = child.firstChildElement("att");
               while(!DOMatt.isNull())
               {
                    QString name = DOMatt.attribute("name", "#NAN#");
                    QString desc = DOMatt.attribute("description", "#NAN#");
                    
                    // Usuwanie spacji z wczytanych wartosci
                    for(;name.contains(" ");)
                         name = name.remove(name.indexOf(" "), 1);
                    
                    // Jezeli nie podamy identyfikatora to blad
                    if((name == "") || (name == "#NAN#"))
                    {
                         QString error = "Missing \"attribute\" parameter \"name\" at line " + QString::number(DOMatt.lineNumber()) + ".";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Wyszukiwanie atrybutu w liscie atrybutuow
                    int aindex = ardAttributeList->indexOfName(name);
                    
                    // Jezeli podany atrybut juz istnieje
                    if(aindex > -1)
                         currentAttribute = ardAttributeList->at(aindex);
                    
                    // Jezeli podany atrybut jeszcze nie istnieje
                    if(aindex == -1)
                    {
                         currentAttribute = ardAttributeList->add();
                         currentAttribute->setName(name);
                    }
                    
                    if(currentProperty == NULL)
                    {
                         QString error = "Misplaced \"attribute\" tag.";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Dodanie atrybutu do bierzacej wlasciwosci
                    if(!currentProperty->add(currentAttribute))
                    {
                         QString error = "The attribute " + currentAttribute->name() + " is already exists in this property.\nError at line " + QString::number(DOMatt.lineNumber()) + ".";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Wczytanie kolejnego atrybutu
                    DOMatt = DOMatt.nextSiblingElement("att");
               }  
               
               // Wczytywanie kolejnego elementu
               child = child.nextSiblingElement("property");
          }
     }
     
     if(root.tagName().toLower() == "tph")
     {
          // "<hist"
          QDomElement DOMchild = root.firstChildElement("hist");
          while(!DOMchild.isNull())
          {
               QString parent = DOMchild.attribute("parent", "#NAN#");
               QString child = DOMchild.attribute("child", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;parent.contains(" ");)
                    parent = parent.remove(parent.indexOf(" "), 1);
               for(;child.contains(" ");)
                    child = child.remove(child.indexOf(" "), 1);
               
               // Jezeli nie podamy rodzica to blad
               if((parent == "") || (parent == "#NAN#"))
               {
                    QString error = "Missing \"attribute\" parameter \"parent\" at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli nie potomka to blad
               if((child == "") || (child == "#NAN#"))
               {
                    QString error = "Missing \"attribute\" parameter \"child\" at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie czy istnieja wlasnosci o wczytanych identyfikatorach
               if((parent.toLower() != "root") && (indexOfID(parent) == -1))
               {
                    QString error = "Undeclared parent property \"" + parent + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if(indexOfID(child) == -1)
               {
                    QString error = "Undeclared child property \"" + child + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli tu doszlismy to znaczy ze wszystko jest ok
               // a wiec mozemy zaczac ustawiac zaleznosci
               
               // Wyszukiwanie obiektu
               int childIndex = indexOfID(child);
               
               // Ustalanie rodzica
               ARD_Property* parentPtr = NULL;
               if(parent.toLower() != "root")
                    parentPtr = at(indexOfID(parent));
               
               if(!setTPHparent(at(childIndex), parentPtr))
               {
                    QString error = "Parent definition error for property \"" + child + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Wczytywanie kolejnego elementu
               DOMchild = DOMchild.nextSiblingElement("hist");
          }
          
          int rootIndex = indexOfRoot();
          if(rootIndex == -1)
          {
               QString error = "Root of TPH tree not defined.";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }

          TPHdiagram->setParents();
          TPHdiagram->createTree(at(rootIndex));
     }
     // Odswiezenie widoku
     // delete fGTable;
     // fGTable = new GTable(this);
     // fGTable->ReCreate();
     // fGTable->Update();
     // fGTable->RePosition();
     
     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Metoda wczytujaca dane o walsciwosciach z pliku ARDML (HML) w formacie 1.0
//
// #param QDomElement& root - Referencja do sekcji dotyczacej informacji o wlasciwosciach
// #return true jezeli dane sa ok, ianczej false
bool ARD_PropertyList::fromHML_1_0(QDomElement& root)
{
     if((root.tagName().toLower() != "property_set") && (root.tagName().toLower() != "tph"))
          return false;
          
     bool res = true;
     fChanged = true;
     
     if(root.tagName().toLower() == "property_set")
     {
          // Wskazniki do aktualnie wczytywanych obiektow
          ARD_Property* currentProperty = NULL;
          ARD_Attribute* currentAttribute = NULL;
     
          QDomElement DOMchild = root.firstChildElement("property");
          while(!DOMchild.isNull())
          {
               // Wczytywanie parametrow w zaleznosci od wykrytego tagu
               QString pid = DOMchild.attribute("pid", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;pid.contains(" ");)
                    pid = pid.remove(pid.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((pid == "") || (pid == "#NAN#"))
               {
                    QString error = "Missing \"property\" parameter \"pid\" at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli podany identyfikator juz istnieje
               if(indexOfID(pid) > -1)
               {     
                    QString error = "Property name tag \"" + pid + "\" already exists.\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli ok to ustawiamy wartosci
               currentProperty = add();
               setPropertyID(currentProperty, pid);
               setTPHparent(currentProperty, NULL);
                    
               
               // "<attref" - wczytywanie atrybutow
               QDomElement DOMatt = DOMchild.firstChildElement("attref");
               while(!DOMatt.isNull())
               {
                    QString name = DOMatt.attribute("name", "#NAN#");
                    
                    // Usuwanie spacji z wczytanych wartosci
                    for(;name.contains(" ");)
                         name = name.remove(name.indexOf(" "), 1);
                    
                    // Jezeli nie podamy identyfikatora to blad
                    if((name == "") || (name == "#NAN#"))
                    {
                         QString error = "Missing \"attref\" parameter \"name\" at line " + QString::number(DOMatt.lineNumber()) + ".";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Wyszukiwanie atrybutu w liscie atrybutuow
                    int aindex = ardAttributeList->indexOfName(name);
                    
                    // Jezeli podany atrybut juz istnieje
                    if(aindex > -1)
                         currentAttribute = ardAttributeList->at(aindex);
                    
                    // Jezeli podany atrybut nie istnieje to error
                    if(aindex == -1)
                    {
                         QString error = "Undefined reference to attribute \"" + name + "\". Error at line " + QString::number(DOMatt.lineNumber()) + ".";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    if(currentProperty == NULL)
                    {
                         QString error = "Misplaced \"attribute\" tag.";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Dodanie atrybutu do bierzacej wlasciwosci
                    if(!currentProperty->add(currentAttribute))
                    {
                         QString error = "The attribute " + currentAttribute->name() + " is already exists in this property.\nError at line " + QString::number(DOMatt.lineNumber()) + ".";
                         QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                         return false;
                    }
                    
                    // Pointer reset
                    currentAttribute = NULL;
                    
                    // Read next element
                    DOMatt = DOMatt.nextSiblingElement("attref");
               }  
               
               // Pointer reset
               currentProperty = NULL;
               
               // Read next element
               DOMchild = DOMchild.nextSiblingElement("property");
          }
     }
     
     if(root.tagName().toLower() == "tph")
     {
          // "<hist"
          QDomElement DOMchild = root.firstChildElement("trans");
          while(!DOMchild.isNull())
          {
               QString parent = DOMchild.attribute("src", "#NAN#");
               QString child = DOMchild.attribute("dst", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;parent.contains(" ");)
                    parent = parent.remove(parent.indexOf(" "), 1);
               for(;child.contains(" ");)
                    child = child.remove(child.indexOf(" "), 1);
               
               // Jezeli nie podamy rodzica to blad
               if((parent == "") || (parent == "#NAN#"))
               {
                    QString error = "Missing \"trans\" parameter \"src\" at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli nie potomka to blad
               if((child == "") || (child == "#NAN#"))
               {
                    QString error = "Missing \"trans\" parameter \"dst\" at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie czy istnieja wlasnosci o wczytanych identyfikatorach
               if((parent.toLower() != "root") && (indexOfID(parent) == -1))
               {
                    QString error = "Undeclared source property \"" + parent + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               if(indexOfID(child) == -1)
               {
                    QString error = "Undeclared destination property \"" + child + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli tu doszlismy to znaczy ze wszystko jest ok
               // a wiec mozemy zaczac ustawiac zaleznosci
               
               // Wyszukiwanie obiektu
               int childIndex = indexOfID(child);
               
               // Ustalanie rodzica
               ARD_Property* parentPtr = NULL;
               if(parent.toLower() != "root")
                    parentPtr = at(indexOfID(parent));
               
               if(!setTPHparent(at(childIndex), parentPtr))
               {
                    QString error = "Source definition error for property \"" + child + "\".\nError at line " + QString::number(DOMchild.lineNumber()) + ".";
                    QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
                    return false;
               }
               
               // Wczytywanie kolejnego elementu
               DOMchild = DOMchild.nextSiblingElement("trans");
          }
          
          int rootIndex = indexOfRoot();
          if(rootIndex == -1)
          {
               QString error = "Root of TPH tree not defined.";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }
          if(rootsCount() != 1)
          {
               QString error = "There is more than one TPH root defined.";
               QMessageBox::critical(NULL, "HQEd", error, QMessageBox::Ok);
               return false;
          }

          TPHdiagram->setParents();
          TPHdiagram->createTree(at(rootIndex));
     }
     // Odswiezenie widoku
     // delete fGTable;
     // fGTable = new GTable(this);
     // fGTable->ReCreate();
     // fGTable->Update();
     // fGTable->RePosition();
     
     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_PropertyList::out_ard_properties_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     if(size() == 0)
          return res;

     *res << "<properties>";
     for(int i=0;i<size();++i)
     {
          QStringList* tmp = at(i)->out_ard_property_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }
     *res << "</properties>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_PropertyList::out_ard_properties_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     if(size() == 0)
          return;

     _xmldoc->writeStartElement("properties");
     for(int i=0;i<size();++i)
          at(i)->out_ard_property_HML_2_0(_xmldoc);
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_PropertyList::out_ard_tph_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     if(size() == 0)
          return res;

     *res << "<tph>";
     for(int i=0;i<size();++i)
     {
          QStringList* tmp = at(i)->out_ard_tph_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }
     *res << "</tph>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_PropertyList::out_ard_tph_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     if(size() == 0)
          return;

     _xmldoc->writeStartElement("tph");
     for(int i=0;i<size();++i)
          at(i)->out_ard_tph_HML_2_0(_xmldoc);
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int ARD_PropertyList::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     if((__root.tagName().toLower() != "properties") && (__root.tagName().toLower() != "tph"))
          return 0;
          
     int res = 0;
     fChanged = true;

     if(__root.tagName().toLower() == "properties")
     {
          ardAttributeList->flush();              // removing all attrbiutes
          ardAttributeList->xttAtts2ardAtts();    // creating ARD attributes using XTT attributes
     
          // Wskazniki do aktualnie wczytywanych obiektow
          ARD_Property* currentProperty = NULL;
          ARD_Attribute* currentAttribute = NULL;
     
          QString elementName = "property";
          
          QDomElement DOMchild = __root.firstChildElement(elementName);
          while(!DOMchild.isNull())
          {
               // Wczytywanie parametrow w zaleznosci od wykrytego tagu
               QString pid = DOMchild.attribute("id", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;pid.contains(" ");)
                    pid = pid.remove(pid.indexOf(" "), 1);
               
               // Jezeli nie podamy identyfikatora to blad
               if((pid == "") || (pid == "#NAN#"))
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Missing \'" + elementName + "\' parameter \'id\'.\nObject will not be read." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Jezeli podany identyfikator juz istnieje
               if(indexOfID(pid) > -1)
               {     
                    __msg += "\n" + hqed::createErrorString("", "", "Property identifier \'" + pid + "\' already exists.\nObject will not be read." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Jezeli ok to ustawiamy wartosci
               currentProperty = add();
               setPropertyID(currentProperty, pid);
               setTPHparent(currentProperty, NULL);
                    
               
               // "<attref" - wczytywanie atrybutow
               QString elementName = "attref";
               QDomElement DOMatt = DOMchild.firstChildElement(elementName);
               while(!DOMatt.isNull())
               {
                    QString name = DOMatt.attribute("ref", "#NAN#");
                    
                    // Usuwanie spacji z wczytanych wartosci
                    for(;name.contains(" ");)
                         name = name.remove(name.indexOf(" "), 1);
                    
                    // Jezeli nie podamy identyfikatora to blad
                    if((name == "") || (name == "#NAN#"))
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Missing attribute reference \'" + elementName + "\' parameter \'ref\' in property \'" + pid + "\'.\nThe reference will be omited." + hqed::createDOMlocalization(DOMatt), 0, 2);
                         res = 1;
                         DOMatt = DOMatt.nextSiblingElement(elementName);
                         continue;
                    }
                    
                    // Wyszukiwanie atrybutu w liscie atrybutuow
                    int aindex = ardAttributeList->indexOfID(name);
                    
                    // Jezeli podany atrybut juz istnieje
                    if(aindex > -1)
                         currentAttribute = ardAttributeList->at(aindex);
                    
                    // Jezeli podany atrybut nie istnieje to error
                    if(aindex == -1)
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Undefined reference \'" + name + "\' to attribute in property \'" + pid + "\'.\nThe reference will be omited." + hqed::createDOMlocalization(DOMatt), 0, 2);
                         res = 1;
                         DOMatt = DOMatt.nextSiblingElement(elementName);
                         continue;
                    }
                    
                    if(currentProperty == NULL)
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Misplaced \'" + elementName + "\' tag." + hqed::createDOMlocalization(DOMatt), 0, 2);
                         res = 1;
                         DOMatt = DOMatt.nextSiblingElement(elementName);
                         continue;
                    }
                    
                    // Dodanie atrybutu do bierzacej wlasciwosci
                    if(!currentProperty->add(currentAttribute))
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "The attribute \'" + currentAttribute->name() + "\' is already exists in property \'" + pid + "\'." + hqed::createDOMlocalization(DOMatt), 0, 2);
                         res = 1;
                         DOMatt = DOMatt.nextSiblingElement(elementName);
                         continue;
                    }
                    
                    // Pointer reset
                    currentAttribute = NULL;
                    
                    // Read next element
                    DOMatt = DOMatt.nextSiblingElement(elementName);
               }  
               
               // Pointer reset
               currentProperty = NULL;
               
               // Read next element
               DOMchild = DOMchild.nextSiblingElement("property");
          }
     }
     
     if(__root.tagName().toLower() == "tph")
     {
          QString elementName = "hist";

          QDomElement DOMchild = __root.firstChildElement(elementName);
          while(!DOMchild.isNull())
          {
               QString parent = DOMchild.attribute("src", "#NAN#");
               QString child = DOMchild.attribute("dst", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;parent.contains(" ");)
                    parent = parent.remove(parent.indexOf(" "), 1);
               for(;child.contains(" ");)
                    child = child.remove(child.indexOf(" "), 1);
               
               // Jezeli nie podamy rodzica to blad
               if((parent == "") || (parent == "#NAN#"))
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Missing \'" + elementName + "\' parameter \'src\'." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Jezeli nie potomka to blad
               if((child == "") || (child == "#NAN#"))
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Missing \'" + elementName + "\' parameter \'dst\'." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Sprawdzanie czy istnieja wlasnosci o wczytanych identyfikatorach
               if((parent.toLower() != "root") && (indexOfID(parent) == -1))
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Incorrect property reference \'src\' in \'" + elementName + "\' object." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               if(indexOfID(child) == -1)
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Incorrect property reference \'dst\' in \'" + elementName + "\' object." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Jezeli tu doszlismy to znaczy ze wszystko jest ok
               // a wiec mozemy zaczac ustawiac zaleznosci
               
               // Wyszukiwanie obiektu
               int childIndex = indexOfID(child);
               
               // Ustalanie rodzica
               ARD_Property* parentPtr = at(indexOfID(parent));
               
               if(parentPtr == NULL)
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Incorrect \'src\' property reference: \'" + parent + "\'.\nThe object will be omited." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               if(!setTPHparent(at(childIndex), parentPtr))
               {    
                    __msg += "\n" + hqed::createErrorString("", "", "Source definition error for property \'" + child + "\'." + hqed::createDOMlocalization(DOMchild), 0, 2);
                    res = 1;
                    DOMchild = DOMchild.nextSiblingElement(elementName);
                    continue;
               }
               
               // Wczytywanie kolejnego elementu
               DOMchild = DOMchild.nextSiblingElement(elementName);
          }
          
          int rootIndex = indexOfRoot();

          if(rootIndex == -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Root of TPH tree not defined.", 0, 2);
               return 1;
          }
          if(rootsCount() != 1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "There is more than one TPH root defined.", 0, 2);
               return 1;
          }
          TPHdiagram->setParents();
          TPHdiagram->createTree(at(rootIndex));
     }
     
     fChanged = false;
     return res;
}
// -----------------------------------------------------------------------------

