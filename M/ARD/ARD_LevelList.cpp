 /**
 *                                                                             *
 * @file ARD_LevelList.cpp                                                     *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_LevelList.cpp,v 1.31.4.2.2.2 2011-08-31 17:30:55 hqeddoxy Exp $    *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 04.01.2008                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_LevelList                   *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../C/MainWin_ui.h"
#include "../../C/ARD/widgets/ardGraphicsScene.h"
#include "../../V/ARD/gARDlevel.h"
#include "../../V/ARD/gARDlevelList.h"
#include "../../V/ARD/gTPHtree.h"
#include "../../V/ARD/gTPHnode.h"


#include "../../namespaces/ns_hqed.h"

#include "ARD_Level.h"
#include "ARD_Property.h"
#include "ARD_Attribute.h"
#include "ARD_AttributeList.h"
#include "ARD_PropertyList.h"
#include "ARD_DependencyList.h"

#include "ARD_LevelList.h"
// -----------------------------------------------------------------------------

ARD_LevelList* ardLevelList;
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_LevelList::ARD_LevelList(void) : QList<ARD_Level*>()
{
	fChanged = false;
	fGlevelList = new gARDlevelList(this);
}
// -----------------------------------------------------------------------------

// #brief Destruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_LevelList::~ARD_LevelList(void)
{
	while(size() > 0)
	{
		delete takeAt(0);
	}
	delete fGlevelList;
}
// -----------------------------------------------------------------------------

// #brief Zwraca tresc ostatniego bledu
//
// #return tresc ostatniego bledu
QString ARD_LevelList::error(void)
{
	return fError;
}
// -----------------------------------------------------------------------------

// #brief Zwraca wskaznik do obiektu bedacego graficzna reprezentacja poziomow
//
// #return wskaznik do obiektu bedacego graficzna reprezentacja poziomow
gARDlevelList* ARD_LevelList::gLevelList(void)
{
	return fGlevelList;
}
// -----------------------------------------------------------------------------

// #brief Oproznia pojemnik
//
// #return no return value
void ARD_LevelList::flush(void)
{
	while(size() > 0)
	{
		delete takeAt(0);
	}
		
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Refreshes the ARD display
///
// #return no values return
void ARD_LevelList::refreshDisplay(void)
{
     fGlevelList->alignLevels(size()-1, true);
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy obiekt
//
// #return wskaznik do nowo utworzonego obiektu
ARD_Level* ARD_LevelList::add(void)
{
	// Tworzenie nowego obiektu
	ARD_Level* tmp = new ARD_Level(size());
     tmp->gLevel()->setDisplay(fGlevelList->display());
	add(tmp);
	
	fChanged = true;
	return tmp;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut o podanym adresie
//
// #param ARD_Property* _new - adres nowego obiektu do dodania
// #return brak zwracanych wartosci
void ARD_LevelList::add(ARD_Level* _new)
{
	// Sprawdzamy czy taki atrybut juz nie istnieje
	if(contains(_new))
		return;

     _new->gLevel()->setDisplay(fGlevelList->display());
	append(_new);
	
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Usuwa wybrany atrybut
//
// #param int _index - index obiektu do usuniecia
// #return brak zwracanych wartosci
void ARD_LevelList::remove(int _index)
{
     // Sprawdzanie poprawnosci indexu
     if((_index < 0) || (_index >= size()))
          return;

     // Usuwanie obiektu
     delete takeAt(_index);

     // Przenumerowanie leveli
     for(int i=0;i<size();++i)
          at(i)->setLevel(i);

     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Deletes levels below _index
//
// #param int _index - index of the level below which all the levels will be deleted
// #return no return value
void ARD_LevelList::removeLevelsBelow(int _index)
{
     _index++;

     // Sprawdzanie poprawnosci indexu
     if((_index < 1) || (_index >= size()))
		return;

     // Deleting obsolete Properties
     for (QList<ARD_Level*>::iterator i = this->end(); this->begin()+_index + 1 != i;i--)
          delete takeLast();
     ARD_Level* lastRemoved = takeLast();
     ARD_Property* tmp;
     QList<ARD_Property*>* listObsoleteProperties = new QList<ARD_Property*>;

     *listObsoleteProperties = hqed::listSubtraction(*(lastRemoved->properties()),*(last()->properties()));

     foreach(tmp, *listObsoleteProperties)
     {
          ardPropertyList->remove(ardPropertyList->indexOfID(tmp->id()));
          //tmp->TPHparent(); //check for the tph parents and not deallocated memory

     }

     //Deleting obsolete children of the properties
     *listObsoleteProperties = hqed::listIntersection(*(last()->properties()),*(lastRemoved->properties()));

     foreach(tmp,*listObsoleteProperties)
     {
          ardPropertyList->removeChildren(tmp);
     }

     delete listObsoleteProperties;
     delete lastRemoved;

     ardPropertyList->tphTree()->refresh();

	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Setter of a display object
//
// #param __display - a pointer to the display object
// #return no values return
void ARD_LevelList::setDisplay(hBaseScene* __display)
{
     fGlevelList->setDisplay(__display);
     for(int i=0;i<size();++i)
          at(i)->setDisplay(__display);
}
// -----------------------------------------------------------------------------

// #brief Getter of a display object
//
// #return A pointer to the displaye object
hBaseScene* ARD_LevelList::display(void)
{
     return fGlevelList->display();
}
// -----------------------------------------------------------------------------
 
// #brief Zwraca czy obiekty byly modyfikowane
//
// #return true jezeli jakikolwiek element pojemnika byl modyfikowany, inaczej false
bool ARD_LevelList::changed(void)
{
	if(fChanged)
		return true;
		
	for(int i=0;i<size();++i)
		if(at(i)->changed())
			return true;
	
	return false;
}
// -----------------------------------------------------------------------------

// #brief Ustawia status zmiany na zadana wartosc
//
// #param bool _c - wartosc statusu
// #return brak zwraanych wartosci
void ARD_LevelList::setChanged(bool _c)
{
	fChanged = _c;
	
	for(int i=0;i<size();++i)
		at(i)->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Uaktualnia, tworzy nowe poziomy na podstawie poziomu zrodlowego
//
// #param int _srcLevel - numer poziomu zrodlowego
// #return brak zwraanych wartosci
void ARD_LevelList::buildLevels(int _srcLevel)
{
	// Budowanie poziomow mniej szczegolowych
	int currentLevel = _srcLevel;
	bool ok;
	
	do
	{
		ARD_Level* _newLevel = NULL;
		if(currentLevel == 0)
		{
			_newLevel = new ARD_Level(size());
               _newLevel->gLevel()->setDisplay(fGlevelList->display());
			insert(0, _newLevel);
		}
		if(currentLevel > 0)
		{
			currentLevel--;
			_newLevel = at(currentLevel);
		}

		ok = _newLevel->update(at(currentLevel+1), true);

		// Jezeli nie udalo sie utworzyc to usuwamy wszystkie powyzsze levele
		if(!ok)
		{
			for(int i=currentLevel;i>=0;--i)
				remove(i);
		}
	}
	while(ok);
	
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Tworzy pierwszy bazowy poziom
//
// #return brak zwraanych wartosci
void ARD_LevelList::createBaseLevel(void)
{
	flush();
	ARD_Level* firstLevel = add();
	QList<ARD_Dependency*>* leafsDeps = ardDependencyList->leafsDependencies();
	QList<ARD_Property*>* leafs = ardPropertyList->listOfLeafs();

	for(int i=0;i<leafs->size();++i)
		firstLevel->addProperty(leafs->at(i));
	for(int i=0;i<leafsDeps->size();++i)
		firstLevel->addDependency(leafsDeps->at(i));

	firstLevel->gLevel()->placeObjects();
	buildLevels(0);
	gLevelList()->alignLevels(0, true);

	delete leafs;
	delete leafsDeps;

	/* // Pokazanie rezultatu wlasnosci
	QString res = "Utworzono " + QString::number(ardLevelList->size()) + " poziomow:";
	for(int l=0;l<ardLevelList->size();++l)
	{
		res = res + "\nPoziom " + QString::number(ardLevelList->at(l)->level()) + " zawiera: ";
		for(int p=0;p<ardLevelList->at(l)->properties()->size();++p)
			res = res + ardLevelList->at(l)->properties()->at(p)->id() + ", ";
	}
	
	
	// Pokazanie rezultatu zaleznosci
	res += "\n\nZaleznosci";
	for(int l=0;l<ardLevelList->size();++l)
	{
		res = res + "\nPoziom " + QString::number(ardLevelList->at(l)->level()) + " zawiera: ";
		for(int d=0;d<ardLevelList->at(l)->dependencies()->size();++d)
			res = res + "(" + ardLevelList->at(l)->dependencies()->at(d)->independent()->id() + "," + ardLevelList->at(l)->dependencies()->at(d)->dependent()->id() + "), ";
	}
	
	QMessageBox::information(this, "HQEd", res);  */
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_LevelList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int a=0;a<size();++a)
               res = at(a)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------
