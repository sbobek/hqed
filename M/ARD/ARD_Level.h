/**
 * \file	ARD_Level.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	03.01.2008
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu bedacego pojemnikiem na zaleznosci i wlasciwosci na danym poziomie projektowania.
 * \brief	This file contains class definition which is a container for dependencies and properties at a given level of design.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
*/

#ifndef ARD_LEVELH
#define ARD_LEVELH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
// -----------------------------------------------------------------------------

class hBaseScene;
class ARD_Property;
class ARD_PropertyList;
class ARD_Dependency;
class ARD_DependencyList;

class gARDlevel;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_Level
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	03.01.2008
* \brief 	Class definition which is a container for dependencies and properties at a given level of design.
*/
class ARD_Level
{
private:

    int fLevel; ///< Level Number

    QList<ARD_Property*>* fProperties; ///< Property list

    QList<ARD_Dependency*>* fDependencies; ///< Dependency list

    bool fChanged; ///< Specifies the status or contents of the container was changed

    gARDlevel* fGlevel; ///< Graphical representation of the level

    void findParents(QString, QList<ARD_Property*>*); ///< Searches for the parents for the value, function is private because of the recursion and danger
    ///< during inability to call up

public:

    /// \brief Default Constructor
    ARD_Level(void);

    /// \brief Constructor that sets value level
    ARD_Level(int);

    /// \brief Destructor
    ~ARD_Level(void);

    // ---------------- Methods that return class fields values ----------------

    /// \brief Returns whether the dependencies were modified
    bool changed(void);

    /// \brief Returns the number of Level
    int level(void);

    /// \brief Getter of a display object
    ///
    /// \return A pointer to the displaye object
    hBaseScene* display(void);

    /// \brief Returns a pointer to the list of properties at this level
    QList<ARD_Property*>* properties(void);

    /// \brief Return a const reference to the properties of this level.
    const QList<ARD_Property*> & properties() const
    { return *fProperties; }

    /// \brief Returns a pointer to dependencies of properties propierties on the level
    QList<ARD_Dependency*>* dependencies(void);

    /// \brief Returns a const reference to the dependencies of this level.
    const QList<ARD_Dependency*> & dependencies() const
    { return *fDependencies; }

    /// \brief Returns a pointer to the object which is  graphical representation of the level
    gARDlevel* gLevel(void);

    // ---------------- Methods for setting class fields values ----------------

    /// \brief Sets the number of Level
    void setLevel(int);

    /// \brief Sets the change status on the setpoint
    void setChanged(bool);

    /// \brief Setter of a display object
    ///
    /// \param __display - a pointer to the display object
    /// \return no values return
    void setDisplay(hBaseScene* __display);

    // -------------- Operational class methods for dependency --------------

    /// \brief Clears container
    void flush(void);

    /// \brief Searches and returns a list of dependencies which depends only on it
    QList<ARD_Dependency*>* indexOfDependentOfOnly(ARD_Property*);

    /// \brief Searches and returns a list of dependencies of specified source values and destination
    QList<ARD_Dependency*>* indexOfinout(ARD_Property*, ARD_Property*);

    /// \brief Searches and returns a list of dependencies of specified source values and destination
    QList<ARD_Dependency*>* indexOfinout(QString, QString);

    /// \brief Searches dependency on the ID
    int indexOfDepID(QString);

    /// \brief Adds a new dependence
    bool addDependency(ARD_Dependency*);

    /// \brief Removes dependence
    bool removeDependency(ARD_Dependency*);

    /// \brief Removes dependence of specified index
    bool removeDependency(int);

    /// \brief Usuwa zaleznosci pode w liscie???
    bool removeDependencies(QList<ARD_Dependency*>*);

    // --------------- Operational class methods for dependencies ---------------

    /// \brief Searches objects that contain an attribute specified name
    QList<ARD_Property*>* listOfAttributeName(QString);

    /// \brief Searches objects containing the attribute with the specified id
    QList<ARD_Property*>* listOfAttributeID(QString);

    /// \brief Searches objects by ID
    int indexOfPrpID(QString);

    /// \brief Adds new property
    bool addProperty(ARD_Property*);

    /// \brief Adds the _propList to properties
    /// \param _propList - list of properties that will be added. If it is empty the function returns true.ssss
    /// \return true if all properties exist, are not null and are in ardPropertyList, otherwise false.
    /// When false the function adds what it can.
    bool addProperties(QList<ARD_Property*>& _propList);

    /// \brief Removes property
    bool removeProperty(ARD_Property*);

    /// \brief Removes property of specified index
    bool removeProperty(int);

    /// \brief Updating the level based on a given
    bool update(ARD_Level*, bool);

    /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
    ///
    /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
    /// \param __event - the event identifier
    /// \param ... - the list of event params
    /// \return true on succes otherwise false
    bool eventMessage(QString* __errmsgs, int __event, ...);

    // ----------------------- Operational class methods -----------------------

    /// \brief Draw a level on the stage
    QRectF paint(void);

    /// \brief Searches family for the property
    QList<ARD_Property*>* findFamily(QString);
};
// -----------------------------------------------------------------------------
#endif
