 /**
 *                                                                             *
 * @file ARD_Dependency.cpp                                                    *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_Dependency.cpp,v 1.28.4.1.2.2 2011-08-31 02:42:44 hqeddoxy Exp $               *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 26.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_Dependency                  *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_hqed.h"

#include "../../C/MainWin_ui.h"
#include "../XTT/XTT_Attribute.h"
#include "ARD_Property.h"
#include "ARD_Dependency.h"
#include "ARD_Attribute.h"
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_Dependency::ARD_Dependency(void)
{
	fID = "";
	fIndependent = NULL;
	fDependent = NULL;
	fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #param unsigned int _id - Identyfikator atrybutu
// #return Brak zwracanych wartosci
ARD_Dependency::ARD_Dependency(QString _id)
{
	fID = _id;
	fIndependent = NULL;
	fDependent = NULL;
	fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Zwraca identyfikator obiektu
//
// #return Zwraca identyfikator obiektu
QString ARD_Dependency::id(void)
{
	return fID;
}
// -----------------------------------------------------------------------------

// #brief Zwraca status zmiany obiektu
//
// #return Zwraca status zmiany obiektu
bool ARD_Dependency::changed(void)
{
	return fChanged;
}
// -----------------------------------------------------------------------------

// #brief Zwraca wskaznik do obiektu niezaleznego
//
// #return Zwraca wskaznik do obiektu niezaleznego
ARD_Property* ARD_Dependency::independent(void)
{
	return fIndependent;
}
// -----------------------------------------------------------------------------


// #brief Zwraca wskaznik do obiektu zaleznego
//
// #return Zwraca wskaznik do obiektu zaleznego
ARD_Property* ARD_Dependency::dependent(void)
{
	return fDependent;
}
// -----------------------------------------------------------------------------

// #brief Sets the id of the object
//
// #param __id - id of the object
// #return no values returns
void ARD_Dependency::setID(QString __id)
{
     fID = __id;
}
// -----------------------------------------------------------------------------

// #brief Ustawia status zmiany obiektu
//
// #param unsigned int _id - nowa wartosc statusu
// #return Brak zwracanych wartosci
void ARD_Dependency::setChanged(bool _c)
{
	fChanged = _c;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wskaznik do obiektu niezaleznego
//
// #param ARD_Property* _ptr - wskaznik do obiektu
// #return Brak zwracanych wartosci
bool ARD_Dependency::setIndependent(ARD_Property* _ptr)
{
	if(_ptr == NULL)
		return false;
		
	fIndependent = _ptr;
	return true;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wskaznik do obiektu zaleznego
//
// #param ARD_Property* _ptr - wskaznik do obiektu
// #return Brak zwracanych wartosci
bool ARD_Dependency::setDependent(ARD_Property* _ptr)
{
	if(_ptr == NULL)
		return false;
		
	fDependent = _ptr;
	return true;
}
// -----------------------------------------------------------------------------


// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString ARD_Dependency::idPrefix(void)
{
     return "dep_";
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_Dependency::eventMessage(QString* /*__errmsgs*/, int /*__event*/, ...)
{
     /*
     va_list argsptr;
     va_start(argsptr, __event);
     va_arg(argsptr, typ);
     va_end(argsptr);
     */
     
     // bool res = true;
     
     /*if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
          return res;*/
          
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_Dependency::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     if((fIndependent == NULL) || (fDependent == NULL))
          return res;

     QString init_line = "<dep independent=\"" + fIndependent->id() + "\" dependent=\"" + fDependent->id() + "\"/>";
     *res << init_line;

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_Dependency::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     if((fIndependent == NULL) || (fDependent == NULL))
          return;

     _xmldoc->writeEmptyElement("dep");
     _xmldoc->writeAttribute("independent", fIndependent->id());
     _xmldoc->writeAttribute("dependent", fDependent->id());
}
#endif
// -----------------------------------------------------------------------------
