 /**
 * \file	ARD_Attribute.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.12.2007
 * \version	1.0
 * Comment:     Plik zwiera definicje obiektu bedacego atrybutem, zmienna w
 * diagramch ARD, jezeli w pliku ARD atrybut jest zdefiniowany w sposob
 * dokladny przy pomocy ATTML, to w tej klasie przechowujemy tylko nazwe
 * natomiast reszte danych w klasie XTT_ATTRIBUTE
 * \brief	This file contains class definition that represents an ARD attribute
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ARD_ATTRIBUTEH
#define ARD_ATTRIBUTEH

// -----------------------------------------------------------------------------

#define ARD_ATT_CONCEPTUAL 0
#define ARD_ATT_PHYSICAL   1
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QDomDocument>
#include <QStringList>
#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class XTT_Attribute;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_Attribute
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.12.2007
* \brief 	Class definition that represents an ARD attribute
*/
class ARD_Attribute
{
	
private:
	
	QString fID; ///< Attribute identifier
		
	QString fName; ///< Attribute Name:
	               /// - if it begins with a capital letter that means it is a conceptual attribute
	               /// - if it begins with a small letter it means that it is a physical attribute
		
	QString fAttrDesc; ///< Attribute Description
		
	XTT_Attribute* fXTTequivalent; ///< Contains a pointer to an object XTT_Attribute, which is the same attribute only a well-defined, if NULL is not equivalent
		
	bool fChanged; ///< Specifies whether the attribute was edited
	
public:

	/// \brief Default constructor
	ARD_Attribute(void);
	
	/// \brief Constructor taking the value of ID
	ARD_Attribute(QString);
	
	/// \brief Constructor taking the value of the ID and the name of the attribute
	ARD_Attribute(QString, QString);
	
        // ----------------- Methods that return class fields values-----------------
	
	/// \brief Returns the attribute type: ARD_ATT_CONCEPTUAL, ARD_ATT_PHYSICAL
	int type(void);
	
	/// \brief Returns the ID of the attribute
	QString id(void);
	
	/// \brief Returns the name of the attribute
        const QString& name(void) const;
	
	/// \brief Returns the description of the attribute
	QString description(void);
	
	/// \brief Returns a pointer to an object XTT_Attribute, which is the same attribute only a well-defined
	XTT_Attribute* xttEquivalent(void);
	
	/// \brief Returns whether an attribute was changed
	bool changed(void);
	
        // ----------------- Methods that sets class fields values -----------------
	
	/// \brief Sets the name attribute
	bool setName(QString);
	
	/// \brief Sets the description attribute
	void setDescription(QString);
     
        /// \brief Function that sets the ID of the attribute
        ///
        /// \return no values return
	void setId(QString);
	
	/// \brief Sets the change status of the atrribute
	void setChanged(bool);
	
	/// \brief Sets the equivalent of the attribute
	void setXTTequivalent(XTT_Attribute*);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \return List of all data in hml 2.0 format.
     QStringList* out_HML_2_0(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No retur value.
     void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     // ----------------- Static Methods -----------------
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
// -----------------------------------------------------------------------------
#endif
