/**
 *                                                                             *
 * @file ARD_Property.cpp                                                      *
 * Project name: HeKatE hqed                                                   *
 * $Id: ARD_Property.cpp,v 1.29.4.6.2.4 2011-08-31 17:47:07 hqeddoxy Exp $     *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 23.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawiera definicje metod klasy ARD_Property                    *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../C/ARD/widgets/ardGraphicsScene.h"

#include "../../namespaces/ns_hqed.h"
#include "../../V/ARD/gARDnode.h"
#include "../../V/ARD/gTPHnode.h"

#include "../XTT/XTT_Attribute.h"
#include "ARD_DependencyList.h"
#include "ARD_Attribute.h"
#include "ARD_AttributeList.h"
#include "ARD_Property.h"
#include "ARD_PropertyList.h"
// -----------------------------------------------------------------------------


// #brief Konstruktor defaultowy klasy
//
// #return Brak zwracanych wartosci
ARD_Property::ARD_Property(void)
{
	fID = "";
	fTPHparent = "root";
	fChanged = false;
	fGraphicTPHnode = new gTPHnode(this);
}
// -----------------------------------------------------------------------------

// #brief Konstruktor defaultowy klasy
//
// #param QString _id - Identyfikator obiektu
// #return Brak zwracanych wartosci
ARD_Property::ARD_Property(QString _id)
{
	fID = _id;
	fTPHparent = "root";
	fChanged = false;
	fGraphicTPHnode = new gTPHnode(this);
}
// -----------------------------------------------------------------------------

// #brief Destruktor klasy
//
// #return Brak zwracanych wartosci
ARD_Property::~ARD_Property(void)
{
	delete fGraphicTPHnode;
}
// -----------------------------------------------------------------------------

// #brief Zwraca identyfikator obiektu
//
// #return Zwraca identyfikator obiektu, jezeli zero to id jest niezdefiniowane
QString ARD_Property::id(void)
{
	return fID;
}
// -----------------------------------------------------------------------------

// #brief Zwraca identyfikator rodzica w diagramie TPH
//
// #return Zwaraca identyfikator rodzica w diagramie TPH, jezeli "root" to ROOT
QString ARD_Property::TPHparent(void) const
{
	return fTPHparent;
}
// -----------------------------------------------------------------------------

// #brief Zwraca czy dany obiekt byl modyfikowany
//
// #return Zwraca czy dany obiekt byl modyfikowany
bool ARD_Property::changed(void)
{
	return fChanged;
}
// -----------------------------------------------------------------------------

// #brief Zwraca obiekt ktory graficznie reprezentuje dany obiekt na diagramie TPH
//
// #return Zwraca obiekt ktory graficznie reprezentuje dany obiekt na diagramie TPH
gTPHnode* ARD_Property::graphicTPHnode(void)
{
	return fGraphicTPHnode;
}
// -----------------------------------------------------------------------------

// #brief Zwraca czy dany obiekt moze byc poddany dalszej transformacji (finalizacji, podzialowi)
//
// #return Zwraca true jezeli tak, inaczej false
bool ARD_Property::canBeTransformated(void)
{
	if((size() == 1) && (at(0)->type() == ARD_ATT_PHYSICAL))
		return false;

	return true;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc statusu zmiany obiektu
//
// #param bool _val - nowa wartosc statusu
// #return Brak zwracanych wartosci
void ARD_Property::setChanged(bool _val)
{
	fChanged = _val;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc identyfikatora
//
// #param QString _parent - identyfikator obiektu
// #return brak zwracanych wartosci
void ARD_Property::setID(QString _id)
{
	fID = _id;
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Ustawia wartosc identyfikatora rodzica w diagramie TPH
//
// #param QString _parent - identyfikator rodzica w diagramie TPH
// #return brak zwracanych wartosci
void ARD_Property::setTPHparent(QString _parent)
{
	fTPHparent = _parent;
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut do obiektu
//
// #param _newAttrID - identyfikator atrybutu, musi istniec w liscie atrybutow
// #return true jezeli uda sie dodac, jezeli nie ma atryburu o podanym ID, lub jezeli juz atrybut jest w obiekcie to false
bool ARD_Property::add(const QString &_newAttrID)
{
	// Wyszukiwanie w liscie atrybutow
	int index = ardAttributeList->indexOfID(_newAttrID);
	if(index == -1)
		return false;
	return add(ardAttributeList->at(index));
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut do obiektu
//
// #param ARD_Attribute* _newAttrPtr - wskaznik do atrybutu, musi istniec w liscie atrybutow
// #return true jezeli uda sie dodac, jezeli nie ma atryburu o podanym adresie, lub jezeli juz atrybut jest w obiekcie to false
bool ARD_Property::add(ARD_Attribute* _newAttrPtr)
{
	// Sprawdzanie czy juz nie ma w obiekcie
	if(contains(_newAttrPtr))
		return false;
	
	// Jezeli wskaznik na nic nie wskazuje
	if(_newAttrPtr == NULL)
		return false;

	// Sprawdzanie czy istnieje w liscie atrybutow
	if(!ardAttributeList->contains(_newAttrPtr))
		return false;

	// Dodanie obiektu
	append(_newAttrPtr);	
	
	// Zapisanie statusu zmiany
	fChanged = true;
	
	return true;
}
// -----------------------------------------------------------------------------

bool ARD_Property::add(const QList<ARD_Attribute *> &_attList)
{
     bool flag = true;
     ARD_Attribute* tmp;
     foreach(tmp,_attList)
          flag = flag && add(tmp);
     return flag;
}
// -----------------------------------------------------------------------------

// #brief Dodaje nowy atrybut do obiektu
//
// #param const QString &_name - nazwa atrybutu, musi istniec w liscie atrybutow
// #return true jezeli uda sie dodac, jezeli nie ma atryburu o podanej nazwie, lub jezeli juz atrybut jest w obiekcie to false
bool ARD_Property::addByName(const QString &_name)
{
     // Wyszukiwanie w liscie atrybutow
     int index = ardAttributeList->indexOfName(_name);
     if(index == -1)
          return false;
     return add(ardAttributeList->at(index));
}
// -----------------------------------------------------------------------------

// #brief Usuwa atrybut z obiektu
//
// #param QString name - nazwa atrybutu, musi być w tej własności
// #return true jezeli uda sie usunąć, jezeli nie ma atryburu o podanej nazwie to false
bool ARD_Property::subtract(const QString &name )
{
     int index = indexOfName(name);
     if (index == -1)
          return false;
     removeAt(index);
     fChanged = true;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Usuwa atrybut z obiektu
//
// #param ARD_Attribute* _attr - wskaznik na atrybut, musi być w tej niezerowy
// #return true jezeli uda sie usunąć, jezeli nie ma atryburu o podanym adresie, lub adres to NULL to false
bool ARD_Property::subtract(const ARD_Attribute *_attr)
{
     if ( _attr == NULL )
          return false;
     return subtract(_attr->name());
}
// -----------------------------------------------------------------------------

bool ARD_Property::subtract(const QList<ARD_Attribute *> &_removeList)
{
     ARD_Attribute* tmp;
     bool ok = true;
     if (_removeList.empty())
          return false;
     foreach (tmp,_removeList)
     {
          ok = subtract(tmp) && ok;
     }
     return ok;
}
// -----------------------------------------------------------------------------

bool ARD_Property::setAttList(const QList<ARD_Attribute *> & _attList)
{
     if (! isEmpty() )
     {
          QList<ARD_Attribute*> _tmpList = *(dynamic_cast<QList<ARD_Attribute*>* >(this));
          clear();
          if (add(_attList))
          {
               fChanged = true;
               return true;
          }
          else
          {
               clear();
               if (add(_tmpList) == false)
               {
                    fChanged = true;
                    return true;
               }
               else
               {
                    fChanged = false;
                    return false;
               }
          }
     }
     else
     {
          return add(_attList);
     }
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje atrybuty na podstawie nazwy
//
// #param QString _name - nazwa atrybutu
// #return index atrybutu o podanej nazwie, jezeli nie ma takiego atrybutu to -1
int ARD_Property::indexOfName(QString _name)
{
	for(int i=0;i<size();++i)
		if(at(i)->name() == _name)
			return i;

	return -1;
}
// -----------------------------------------------------------------------------

// #brief Wyszukuje atrybuty na podstawie identyfikatora
//
// #param unsigned int _id - wartosc identyfikatora
// #return index atrybutu o podanym identyfikatorze, jezeli nie ma takiego atrybutu to -1
int ARD_Property::indexOfID(QString _id)
{
	for(int i=0;i<size();++i)
		if(at(i)->id() == _id)
			return i;

	return -1;
}
// -----------------------------------------------------------------------------

// #brief Sortuje atrybuty rosnaco wg nazwy
//
// #return brak zwraanych wartosci
void ARD_Property::sort(void)
{
	for(int i=0;i<size();++i)
		for(int j=0;j<size()-i-1;++j)
			if(at(j)->name() > at(j+1)->name())
				swap(j, j+1);

	// Zapisanie statusu zmiany
	fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString ARD_Property::idPrefix(void)
{
     return "prp_";
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool ARD_Property::eventMessage(QString* /*__errmsgs*/, int /*__event*/, ...)
{
     /*
     va_list argsptr;
     va_start(argsptr, __event);
     va_arg(argsptr, typ);
     va_end(argsptr);
     */
     
     // bool res = true;
     
     /*if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
          return res;*/


     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_Property::out_ard_property_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     QString init_line = "<property id=\"" + id() + "\"";
     init_line += ">";
     *res << init_line;

     for(int i=0;i<size();++i)
          *res << "\t<attref ref=\"" + at(i)->id() + "\"/>";

     *res << "</property>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_Property::out_ard_property_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     _xmldoc->writeStartElement("property");
     _xmldoc->writeAttribute("id", id());
          for(int i=0;i<size();++i)
          {
               _xmldoc->writeEmptyElement("attref");
               _xmldoc->writeAttribute("ref", at(i)->id());
          }
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* ARD_Property::out_ard_tph_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     if(fTPHparent.toLower().trimmed() == "root")
          return res;

     QString init_line = "<hist src=\"" + id() + "\" dst=\"" + fTPHparent + "\"/>";
     *res << init_line;

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void ARD_Property::out_ard_tph_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     if(fTPHparent.toLower().trimmed() == "root")
          return;

     _xmldoc->writeEmptyElement("hist");
     _xmldoc->writeAttribute("src", fTPHparent);
     _xmldoc->writeAttribute("dst", id());
}
#endif
// -----------------------------------------------------------------------------

