 /**
 * \file	ARD_AttributeList.h 
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	22.12.2007
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu bedacego pojemnikiem na atrybuty ARD
 * \brief	This file contains class definition which is a container for ARD attributes
 * \note This file is a part of HQEd. It has been developed within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ARD_ATTRIBUTELISTH
#define ARD_ATTRIBUTELISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QDomDocument>
#include <QStringList>
#include <QList>
#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class ARD_Attribute;
class XTT_Attribute;
// -----------------------------------------------------------------------------



/**
* \class 	ARD_AttributeList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	22.12.2007
* \brief 	Class definition which is a container for ARD attributes
*/
class ARD_AttributeList : public QList<ARD_Attribute*>
{

private:
     
     /// Specifies the status when content of the container was changed 
     bool fChanged;
     
public:

     /// \brief Default constructor
     ARD_AttributeList(void);
     
     /// \brief Destructor
     ~ARD_AttributeList(void);
     
     // ----------------- Methods for setting the values of class fields -----------------
     
     /// Sets the change status on the setpoint
     void setChanged(bool);
     
     // ----------------- Operational Methods -----------------
     
     /// \brief Clears container
     void flush(void);
     
     /// \brief Adds new attribute
     ARD_Attribute* add(void);
     
     /// \brief Adds new attribute
     void add(ARD_Attribute*);
     
     /// \brief Removes the selected attribute
     void remove(int);
     
     /// \brief Searches for attributes based on the name
     int indexOfName(QString);
     
     /// Searches for attributes based on the ID
     int indexOfID(QString);
     
     /// \brief Functions that finds and returns the unique id
     ///
     /// \return the unique id as string
     QString findUniqID(void);
     
     /// \brief Functions that creates the ARD attributes using XTT attributes
     ///
     /// \return true on succes, false on error
     bool xttAtts2ardAtts(void);
     
     /// \brief Sorts for attributes based on the name
     void sort(void);
     
     /// \brief Returns whether the attributes were modified
     bool changed(void);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Loads data from a file in HTML format in version 1.0
     bool fromHML_1_0(QDomElement&);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \return List of all data in hml 2.0 format.
     QStringList* out_HML_2_0(void);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No retur value.
     void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
};
// -----------------------------------------------------------------------------

extern ARD_AttributeList* ardAttributeList;
// -----------------------------------------------------------------------------
#endif
