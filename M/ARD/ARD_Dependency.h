 /**
 * \file	ARD_Dependency.h 
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	26.12.2007
 * \version	1.0
 * Comment:     Plik zawiera definicje obiektu bedacego zaleznoscia pomiedzy 
 * wlasciwosciami w diagramie ARD.
 * \brief	This file contains class definition that is the relationship 
 * between the properties in the TPH diagram
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

 
#ifndef ARD_DEPENDENCYH
#define ARD_DEPENDENCYH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class ARD_Property;
// -----------------------------------------------------------------------------


/**
* \class 	ARD_Dependency
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	26.12.2007
* \brief 	Class definition which is the relationship between the properties in the TPH diagram
*/
class ARD_Dependency
{
	
private:

	QString fID; ///< Dependency identifier
		
	ARD_Property* fIndependent; ///< Pointer to independent object
		
	ARD_Property* fDependent; ///< Pointer to dependent object	
	
	bool fChanged; ///< Specifies whether the dependency was edited
	
public:

	/// \brief Default constructor
	ARD_Dependency(void);
	
	/// \brief Constructor receiving the ID value
	ARD_Dependency(QString);
	
	/// ----------------- Methods that return class fields values -----------------
	
	/// \brief Returns the object identifier
	QString id(void);
	
	/// \brief Returns the change status of the object
	bool changed(void);
	
	/// \brief Returns a pointer to independent object 
	ARD_Property* independent(void);
	
	/// \brief Returns a pointer to dependent object 
	ARD_Property* dependent(void);
	
	/// ----------------- Methods for setting the values of class fields -----------------
	
	/// \brief Sets the change status of the object
	void setChanged(bool);
     
        /// \brief Sets the id of the object
        ///
        /// \param __id - id of the object
        /// \return no values returns
	void setID(QString __id);
	
	/// \brief Sets the pointer to independent object 
	bool setIndependent(ARD_Property*);
	
        /// \brief Sets the pointer to dependent object
	bool setDependent(ARD_Property*);

        /// \brief Sets the pointer to dependent and independent object 
        bool setSelfDependent(ARD_Property* _property)
        { return setIndependent(_property) && setDependent(_property); }

        /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
        /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
        /// \param __event - the event identifier
        /// \param ... - the list of event params
	/// \return true on succes otherwise false
        bool eventMessage(QString* __errmsgs, int __event, ...);

        /// \brief Function gets all data in hml 2.0 format.
        ///
        /// \return List of all data in hml 2.0 format.
        QStringList* out_HML_2_0(void);

        #if QT_VERSION >= 0x040300
        /// \brief Function saves all data in hml 2.0 format.
        ///
        /// \param _xmldoc Pointer to document.
        /// \return No retur value.
        void out_HML_2_0(QXmlStreamWriter* _xmldoc);
        #endif
     
        // ----------------- Static Methods -----------------
     
        /// \brief Static function that returns the prefix of the id.
        ///
        /// \return the prefix of the id as string.
        static QString idPrefix(void);
	
};
// -----------------------------------------------------------------------------
#endif
