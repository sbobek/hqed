/*
 *	   $Id: hDomain.cpp,v 1.18.4.1 2010-11-10 02:02:44 llk Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../namespaces/ns_hqed.h"
#include "../C/Settings_ui.h"
#include "../C/widgets/hMenuAction.h"

#include "hType.h"
#include "hValue.h"
#include "hSetItem.h"
#include "hValueResult.h"
#include "hDomain.h"
// -----------------------------------------------------------------------------

// #brief Constructor hSet class.
hDomain::hDomain(void)
{
     _parentType = NULL;
     _orderd = false;
}
// -----------------------------------------------------------------------------

// #brief Constructor hDomain class.
//
// #param hType* __parentType - poiter to the type of data that the set can contain
hDomain::hDomain(hType* __parentType)
{
     _orderd = false;
     setParentType(__parentType);
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hSet* hDomain::copyTo(hSet* /*__destinationItem*/)
{
     hDomain* res = NULL;
     if(res == NULL)
          res = new hDomain;

     recreateNumbering();
     res->setOrdered(_orderd);
     res->_numbering = _numbering;
     hSet::copyTo(res);
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param hDomain* __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hDomain* hDomain::copyToDomain(hDomain* __destinationItem)
{
     hDomain* res = __destinationItem;
     if(res == NULL)
          res = new hDomain;

     recreateNumbering();
     res->setOrdered(_orderd);
     res->_numbering = _numbering;
     hSet::copyTo(res);
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that return the number on the given position
//
// #param __index - the index of value
// #return number on the given position
int hDomain::numberEquivalent(int __index)
{
     if((__index < 0) || (__index >= _numbering.size()) || (!ordered()))
          return __index+1;
          
     return _numbering.at(__index);
}
// -----------------------------------------------------------------------------

// #brief Function that return the index of the item that is equivalent to numeric __value
//
// #param __value - the numeric equivalent of item
// #return index of the item that is equivalent to numeric __value
int hDomain::symbolicEquivalent(int __value)
{
     int index = -1;
     if(ordered())
          index = _numbering.indexOf(__value);
     if((!ordered()) || (_numbering.size() == 0))
          index = __value-1;
          
     return index;
}
// -----------------------------------------------------------------------------

// #brief The function that updates the setItems w.r.t. numbering, it renumber all items again only if the type is apropriate.
//
// #return on succes, otherwise false
bool hDomain::updateItemsNumbering(void)
{
     // Checking correctness of the type and domain
     if(_parentType == NULL)
     {
          setLastError("DOMAIN ERROR: Not defined parent type.");
          return false;
     }
          
     if(_parentType->typeClass() != TYPE_CLASS_DOMAIN)
     {
          setLastError("DOMAIN ERROR: Set constraints are not defined as domain.");
          return false;
     }
     
     if((_parentType->baseType() != SYMBOLIC_BASED) || (!ordered()))
     {
          setLastError("DOMAIN ERROR: Numeric representation is availible only for symbolic and ordered domains.");
          return false;
     }
     
     // Renumbering items
     for(int i=0;i<size();++i)
     {
          ValueDefinitionType dt = at(i)->valuePtr()->definitionType();
          
          at(i)->valuePtr()->setDefinitionType(NUM);
          at(i)->valuePtr()->setValue(QString::number(numberEquivalent(i)));
          
          at(i)->valuePtr()->setDefinitionType(dt);
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief The function that updates the numbering using setItems, it creates new number list only if the type is apropriate.
//
// #return on succes, otherwise false
bool hDomain::updateNumbering(void)
{
     // Checking correctness of the type and domain
     if(_parentType == NULL)
     {
          setLastError("DOMAIN ERROR: Not defined parent type.");
          return false;
     }
          
     if(_parentType->typeClass() != TYPE_CLASS_DOMAIN)
     {
          setLastError("DOMAIN ERROR: Set constraints are not defined as domain.");
          return false;
     }
     
     if((_parentType->baseType() != SYMBOLIC_BASED) || (!ordered()))
     {
          setLastError("DOMAIN ERROR: Numeric representation is availible only for symbolic and ordered domains.");
          return false;
     }
     
     // Creating new numbering list
     _numbering.clear();
     for(int i=0;i<size();++i)
     {
          ValueDefinitionType dt = at(i)->valuePtr()->definitionType();
          at(i)->valuePtr()->setDefinitionType(NUM);
          
          hValueResult hvr = at(i)->valuePtr()->value();
          if(!(bool)hvr)
          {
               setLastError("DOMAIN ERROR: " + (QString)hvr);
               return false;
          }
          _numbering.append(((QString)hvr).toInt());
          
          at(i)->valuePtr()->setDefinitionType(dt);
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief The function that creates numbering of the items, or updates it
//
// #return on succes, otherwise false
bool hDomain::recreateNumbering(void)
{
     bool result = updateItemsNumbering();
     result = updateNumbering() && result;
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the parent type of the set
//
// #param hType* __pt - poiter to the type
// #return true on succes, otherwise false
bool hDomain::setParentType(hType* __pt)
{
     if(__pt != NULL)
     {
          if((__pt->baseType() == INTEGER_BASED) || (__pt->baseType() == NUMERIC_BASED))
               _orderd = true;
     }

     return hSet::setParentType(__pt);
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the value to the set
//
// #param *__value - pointer to the value that should be added
// #return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
int hDomain::add(hValue* __value)
{
     recreateNumbering();
     int result = hSet::add(__value);
     updateNumbering();
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the item to the set
//
// #param *__item - item that should be added
// #return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
int hDomain::add(hSetItem* __item)
{
     recreateNumbering();
     int result = hSet::add(__item);
     updateNumbering();
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the item to the set
//
// #param __index - item position
// #param *__item - item that should be inserted
// #return no values return
void hDomain::insertItem(int __index, hSetItem* __item)
{
     //recreateNumbering();
     hSet::insertItem(__index, __item);
     updateNumbering();
}
// -----------------------------------------------------------------------------

// #brief The functions that removes the item with given index from the set
//
// #param __index - idenx of item that should be deleted
// #return true if __index is correct otherwise false
bool hDomain::deleteItem(int __index)
{
     //recreateNumbering();
     bool result = hSet::deleteItem(__index);
     if(__index < _numbering.size())
          _numbering.removeAt(__index);
     updateNumbering();
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief The virtual function that return the index of the first item that contains given value
//
// #param QString __value - the value as string
// #return index of the first item that contains given value
int hDomain::contain(QString __value)
{
     if(_parentType == NULL)
          return setLastError(H_SET_ERROR_NOT_DEFINED_TYPE);
          
     if(_parentType->typeClass() == TYPE_CLASS_DOMAIN)
     {
          if(_parentType->baseType() == SYMBOLIC_BASED)
          {
               for(int i=0;i<size();++i)
               {
                    if(at(i)->type() == SET_ITEM_TYPE_RANGE)
                         return setLastError(H_SET_ERROR_INCORRECT_DOMAIN_ITEM, "\nError item: " + at(i)->toString());
                         
                    hValueResult hvr = at(i)->value();
                    if(!(bool)hvr)
                         return setLastError(H_SET_OTHER_OBJECT_ERROR, hvr.error());
                         
                    if((QString)hvr == __value)
                         return i;
               }
          }
          else
               return hSet::contain(__value);
     }
     
     return -1;
}
// -----------------------------------------------------------------------------

// #brief The functions that checks if the item is in domain
//
// #param hSetItem* __item - pointer to the item
// #return H_SET_TRUE_VALUE if item is in set otherwise H_SET_FALSE_VALUE. On error returns error code.
int hDomain::contain(hSetItem* __item)
{
     return hSet::contain(__item);
}
// -----------------------------------------------------------------------------

// #brief The functions that checks if the set __item is a sub set of this
//
// #param hSet* __item - pointer to the set
// #return H_SET_TRUE_VALUE if item is a sub set of this set otherwise H_SET_FALSE_VALUE. On error returns error code.
int hDomain::contain(hSet* __item)
{
     return hSet::contain(__item);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string list representation of the items
//
// #return The string list representation of the items
QStringList hDomain::stringItems(void)
{
     QStringList res;
     for(int i=0;i<size();++i)
          res << toString(i);
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the domain can be expanded in to single values
//
// #return true on yes, false on no
bool hDomain::canBeExpanded(void)
{
     return ((quint64)Settings->largeDomainSize() >= power());
}
// -----------------------------------------------------------------------------

// #brief Function that returns if it is possible to create menu that defines ranges
//
// #param __singleRequirement - indicates if the external dialog requires single value
// #return true on yes, false on no
bool hDomain::canCreateMenu(bool __singleRequirement)
{
     if(__singleRequirement)
          return false;
          
     return (canBeExpanded() && ordered());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the set of single values of domain
//
// #return the set of single values of domain, or NULL on error
hSet* hDomain::expand(void)
{
     if(_parentType == NULL)
          return NULL;
     
     hSet* result = new hSet(_parentType);
     hSet* sortedSet = copyTo();
     sortedSet->simplify();
     sortedSet->sort(H_SET_SORT_FROM_VALUE, H_SET_SORT_ASCENDING);
     
     for(int i=0;i<sortedSet->size();++i)
     {
          if(sortedSet->at(i)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
               result->add(sortedSet->at(i)->copyTo());
          if(sortedSet->at(i)->type() == SET_ITEM_TYPE_RANGE)
          {
               double curr, to;
               QString strcurr;
               hValueResult hvrc = sortedSet->at(i)->from();
               hValueResult hvrt = sortedSet->at(i)->to();
               
               if(!((bool)hvrc) || (!(bool)hvrt))
                    return NULL;
                    
               _parentType->mapToNumeric((QString)hvrc, curr, false);
               _parentType->mapToNumeric((QString)hvrt, to, false);
               strcurr = QString::number(curr);
               
               while(curr <= to)
               {
                    result->add(new hValue(strcurr));
                    _parentType->typeNextValue(strcurr, strcurr, false);
                    _parentType->mapToNumeric(strcurr, curr, false);
                    //QMessageBox::critical(NULL, "Error localization", strcurr, QMessageBox::Ok);
               }
          }
     }
     delete sortedSet;
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the menu that allows for selecting ranges
//
// #param __title - title of the menu
// #param __cmd - command that is executed after action selecting
// #return pointer to the menu
QMenu* hDomain::createRangeMenu(QString __title, QString __cmd)
{
     if((_parentType == NULL) || (size() == 0))
          return NULL;
     
     QList<QStringList*>* sil = new QList<QStringList*>;    // the list of ranges
     hSet* expanded = expand();                             // creating exploded domain
     
     if(expanded->isEmpty())
          return NULL;
     
     double next, curr;
     QString nextValue, currentValue;     

     // init
     sil->append(new QStringList);
     hValueResult hvrc = expanded->at(0)->value();
     if(!(bool)hvrc)
          return NULL;
     nextValue = (QString)hvrc;

     for(int i=0;i<expanded->size();++i)
     {
          // Calculating new value
          if(i > 0)
               _parentType->typeNextValue(nextValue, nextValue, false);
          
          // Checking if the next value is equal to the required
          hValueResult hvrc = expanded->at(i)->value();
          if(!(bool)hvrc)
               return NULL;
          currentValue = (QString)hvrc;
          
          // mapping to numeric representation
          _parentType->mapToNumeric(currentValue, curr, false);
          _parentType->mapToNumeric(nextValue, next, false);
          
          // comaprison of values
          
          // if next value is not equal then we create a new range
          if(next != curr)
          {
               sil->append(new QStringList);
               nextValue = currentValue;
          }

          if(!sil->last()->contains(currentValue))
               sil->last()->append(currentValue);
     }
     delete expanded;

     // Creating menu
     QMenu* mainM = new QMenu(__title);
     for(int i=0;i<sil->size();++i)
     {
          // to create a reange we need at least two values
          if(sil->at(i)->size() <= 1)
               continue;
          
          for(int j=0;j<sil->at(i)->size()-1;++j)
          {
               QMenu* subM = new QMenu("From: " + sil->at(i)->at(j));
               QObject::connect(mainM, SIGNAL(destroyed()), subM, SLOT(deleteLater()));
               
               mainM->addMenu(subM);
               for(int k=j+1;k<sil->at(i)->size();++k)
               {
                    QString completecmd = __cmd + ":" + sil->at(i)->at(j) + ":" + sil->at(i)->at(k);
                    hMenuAction* action = new hMenuAction(completecmd, "To: " + sil->at(i)->at(k), NULL);
                    QObject::connect(subM, SIGNAL(destroyed()), action, SLOT(deleteLater()));
                    subM->addAction(action);
               }
          }
     }
     
     for(int i=0;i<sil->size();++i)
          delete sil->at(i);
     delete sil;

     return mainM;
}
// -----------------------------------------------------------------------------

// #brief The functions swaps two items
//
// #param __index1 - index of the first item
// #param __index2 - index of the second item
// #return no values return
void hDomain::swapItems(int __index1, int __index2)
{
     recreateNumbering();
     
     // Checking correctness of the type and domain
     if((_parentType != NULL) &&
        (_parentType->typeClass() == TYPE_CLASS_DOMAIN) &&
        (_parentType->baseType() == SYMBOLIC_BASED) && (ordered()))
        
     {
          _numbering.swap(__index1, __index2);
          hSet::swapItems(__index1, __index2);
     }
     else
     {
          hSet::swapItems(__index1, __index2);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the set item
//
// #param __index - index of the item
// #return The string representation of the set item
QString hDomain::toString(int __index)
{
     if((__index < 0) || (__index >= size()))
          return "";
     
     if(_parentType == NULL)
     {
          setLastError(H_SET_ERROR_NOT_DEFINED_TYPE);
          return lastError();
     }
     
     if((_parentType->baseType() == SYMBOLIC_BASED) && (ordered()))
          return at(__index)->toString() + "/" + QString::number(numberEquivalent(__index));
          
     return at(__index)->toString();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the set
//
// #return The string representation of the set
QString hDomain::toString(void)
{
     QString res = "";
     
     if(_parentType == NULL)
     {
          setLastError(H_SET_ERROR_NOT_DEFINED_TYPE);
          return lastError();
     }
     
     if((_parentType->baseType() == SYMBOLIC_BASED) && (ordered()))
     {
          if(size() > 0)
               res = toString(0);
          
          for(int i=1;i<size();++i)
               res = res + " U " + toString(i);
               
          if(isEmpty())
               res = "{}";
     }
     else
          res = hSet::toString();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief The function that return the numeric equivalent of the __value. This function can be used only with ordered symbolic domains.
//
// #param __value - the value as string
// #param __exists - the boolean value where the status of exit is stored
// #return numeric equivalent of the __value.
int hDomain::toNumericEquivalent(QString __value, bool& __exists)
{
     __exists = false;
    
     int index = contain(__value);
     if(index == -1)
          return -1;
    
     __exists = true;
    
     return numberEquivalent(index);
}
// -----------------------------------------------------------------------------

// #brief The function that return the String equivalent of the __value. This function can be used only with ordered symbolic domains.
//
// #param __value - the value as integer
// #return string value
hValueResult hDomain::fromNumericEquivalent(int __value)
{
     int index = symbolicEquivalent(__value);
     
     if((index < 0) || (index >= size()))
          return hValueResult("The numeric value \'" + QString::number(__value) + "\' is out of range.", false);
          
     return at(index)->value();
}
// -----------------------------------------------------------------------------

// #brief The function that return the next value (not index) w.r.t. __ref_value. The function can be used only with SYMBOLIC BASED domains
//
// #param __ref_value - the reference value as integer
// #param __exists - reference to the boolean value, that holds the result of function. If true then returned value is correct, otherwise is incorrect
// #return the next value w.r.t. __ref_value
int hDomain::nextNumericValue(int __ref_value, bool& __exists)
{
     __exists = false;
     
     if((_numbering.size() == 0) && (__ref_value >= 1) && (__ref_value < size()))
     {
          return __ref_value+1;
     }
          
     int result = __ref_value;
     for(int i=0;i<_numbering.size();++i)
          if((((result > _numbering.at(i)) && (_numbering.at(i) > __ref_value))) || 
             ((_numbering.at(i) > __ref_value) && (result == __ref_value)))
             {
               result = _numbering.at(i);
               __exists = true;
             }
               
     return result;
}
// -----------------------------------------------------------------------------

// #brief The function that return the previous value (not index) w.r.t. __ref_value. The function can be used only with SYMBOLIC BASED domains
//
// #param __ref_value - the reference value as integer
// #param __exists - reference to the boolean value, that holds the result of function. If true then returned value is correct, otherwise is incorrect
// #return the previous value w.r.t. __ref_value
int hDomain::prevNumericValue(int __ref_value, bool& __exists)
{
     __exists = false;
     
     if((_numbering.size() == 0) && (__ref_value > 1) && (__ref_value <= size()))
     {
          return __ref_value-1;
     }
          
     int result = __ref_value;
     for(int i=0;i<_numbering.size();++i)
          if((((result < _numbering.at(i)) && (_numbering.at(i) < __ref_value))) || 
             ((_numbering.at(i) < __ref_value) && (result == __ref_value)))
             {
               result = _numbering.at(i);
               __exists = true;
             }
               
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the maximum numeric value
//
// #return the maximum numeric value
int hDomain::maxNumericValue(void)
{
     if(_numbering.isEmpty())
          return 0;
          
     int result = _numbering.at(0);
     for(int i=1;i<_numbering.size();++i)
          if(result < _numbering.at(i))
               result = _numbering.at(i);
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the type object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hDomain::setVerification(QString& __error_message)
{
     // Numbering recreation
     recreateNumbering();

     // Checking correctness of the type and domain
     if(_parentType == NULL)
     {
          __error_message += hqed::createErrorString("", "", "Not defined parent type.", 0, 2);
          return false;
     }
          
     if((_parentType->typeClass() != TYPE_CLASS_DOMAIN) || (_parentType->baseType() != SYMBOLIC_BASED))
          return true;
     
     // Checking if the set does not contain the same symbolic values
     QStringList smblVals;
     for(int i=0;i<size();++i)
     {
          hValueResult hvr = at(i)->value();
          if(!(bool)hvr)
          {
               __error_message += "\n" + hqed::createErrorString("", "", hvr.error(), 0, 2);
               return false;
          }
          smblVals << (QString)hvr;
     }
     for(int s=0;s<smblVals.size();++s)
     {
          if(smblVals.indexOf(smblVals.at(s)) != smblVals.lastIndexOf(smblVals.at(s)))
          {
               __error_message += "\n" + hqed::createErrorString("", "", "The domain of the type \'" + _parentType->name() + "\' conatins two or more symbolic values equal to: \'" + smblVals.at(s) + "\'.", 0, 2);
               return false;
          }
     }
     
     // We dont check numbering when domain is not ordered
     if(!ordered())
          return true;
     
     // checking if the set does not contain the same numeric values
     for(int s=0;s<_numbering.size();++s)
     {
          if(_numbering.indexOf(_numbering.at(s)) != _numbering.lastIndexOf(_numbering.at(s)))
          {
               __error_message += "\n" + hqed::createErrorString("", "", "The domain of the type \'" + _parentType->name() + "\' conatins two or more numeric values equal to: \'" + QString::number(_numbering.at(s)) + "\'", 0, 2);
               return false;
          }
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the domain to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this domain as string.
QString hDomain::toProlog(int /*__option*/)
{
     QString result = "";
     bool saveNumbering = recreateNumbering();
     
     result += "[" + hSet::toProlog(saveNumbering?1:-1) + "]";
     
     if(saveNumbering)
          result += ",\n       ordered: yes";
          
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* hDomain::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();

     recreateNumbering();
     QString ord = "no";
     if(_orderd)
          ord = "yes";
          
     QString init_line = "<domain";
     if(parentType()->baseType() == SYMBOLIC_BASED)
          init_line += " ordered=\"" + ord + "\"";
     init_line += ">";

     *res << init_line;
     
     // Zapis elementow
     for(int a=0;a<size();++a)
     {
          QStringList* al = at(a)->out_HML_2_0(1);
          for(int l=0;l<al->size();l++)
               *res << "\t" + al->at(l);
          delete al;
     }
     
     *res << "</domain>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void hDomain::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     bool saveNumbering = recreateNumbering();
     QString ord = "no";
     if(_orderd)
          ord = "yes";
          
     _xmldoc->writeStartElement("domain");
     if(parentType()->baseType() == SYMBOLIC_BASED)
          _xmldoc->writeAttribute("ordered", ord);

     // Zapis elementow
     for(int a=0;a<size();++a)
          at(a)->out_HML_2_0(_xmldoc, saveNumbering?1:0);
     
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hDomain::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "domain";
     QString childElementName = "value";
     QString att1 = "ordered", att1def = "yes";
     
     if(_parentType == NULL)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Critical error: udefined parent type of domain." + hqed::createDOMlocalization(__root), 0, 2);
          return 1;
     }
     
     if(__root.tagName().toLower() != elementName)
          return 0;
          
     // Reading type data
     QString dordered = __root.attribute(att1, att1def);
     
     // Checking for errors
     if((dordered.toLower() != "no") && (dordered.toLower() != "yes"))
     {
          __msg += "\n" + hqed::createErrorString("", "", "Incorrect value of the domain's \'" + att1 + "\' parameter in \'" + _parentType->name() + "\' type.\nRead value: " + dordered + ".\nThe value will be set to \'" + att1def + "\'." + hqed::createDOMlocalization(__root), 1, 2);
          dordered = att1;
          result++;
     }
     _orderd = (dordered.toLower() == "yes");
     
     // Reading values
     _numbering.clear();
     bool useNumbering = ((_parentType->baseType() == SYMBOLIC_BASED) && (ordered()));
     QDomElement DOMvalue = __root.firstChildElement(childElementName);
     while(!DOMvalue.isNull())
     {
          // Checking for numbering
          bool hasNum = DOMvalue.hasAttribute("num");
          useNumbering = useNumbering && hasNum;
          if((hasNum) && (useNumbering))
          {
               bool convStat;
               QString num = DOMvalue.attribute("num", "");
               int inum = num.toInt(&convStat);
               if(!convStat)
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Incorrect value of the \'num\' parameter in domain of \'" + _parentType->name() + "\' type.\nRead value: " + num + ".\nNumbering will be set as default (1,2,3,...)" + hqed::createDOMlocalization(DOMvalue), 1, 2);
                    useNumbering = false;
                    result++;
               }
               if((convStat) && (_numbering.indexOf(inum) > -1))
               {
                    __msg += "\n" + hqed::createErrorString("", "", "Numeric equivalent \'" + num + "\' already exists.\nError from domain of type \'" + _parentType->name() + "\'.\nNumbering will be set as default (1,2,3,...)" + hqed::createDOMlocalization(DOMvalue), 1, 2);
                    useNumbering = false;
                    result++;
               }
               _numbering.append(inum);
          }
          
          if(((DOMvalue.hasAttribute("is")) && (!DOMvalue.hasAttribute("from")) && (!DOMvalue.hasAttribute("to"))) ||
             ((!DOMvalue.hasAttribute("is")) && (DOMvalue.hasAttribute("from")) && (DOMvalue.hasAttribute("to"))))
          {
               hSetItem* si = new hSetItem;
               si->setParent(this);
               add(si);
               result += si->in_HML_2_0(DOMvalue, __msg, NULL);
          }
          else
          {
               QString errorstr = "Incorrect set of attributes.";
               errorstr += "\nThe attribute \'is\' cannot be used with \'from\' and \'to\' simultaneously.";
               errorstr += "\nThe attribute \'from\' must be used together with \'to\'.";
               errorstr += hqed::createDOMlocalization(DOMvalue);
               __msg += "\n" + hqed::createErrorString("", "", errorstr, 0, 2);
               result++;
          }
          
          DOMvalue = DOMvalue.nextSiblingElement(childElementName);
     }
     
     recreateNumbering();
     
     return result;
}
// -----------------------------------------------------------------------------
