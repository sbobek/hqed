 /**
 * \file	hFunctionList.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	10.09.2008 
 * \version	1.0
 * \brief	This file contains class definition that is a container of object that represents the HeKatE function
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HFUNCLISTH
#define HFUNCLISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define FUNC_LIST_SUCCES               0
#define FUNC_LIST_ERROR__OBJECT_EXIST -1
#define FUNC_LIST_ERROR__NULL_POINTER -2

// Types of representations
#define FUNC_REPRESENTATION_INTERNAL 0
#define FUNC_REPRESENTATION_XML      1
#define FUNC_REPRESENTATION_PROLOG   2
#define FUNC_REPRESENTATION_USER     3
// -----------------------------------------------------------------------------

class hFunction;
class hType;
// -----------------------------------------------------------------------------
/**
* \class 	hFunctionList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	10.09.2008 
* \brief 	Class definition that is a container of object that represents the HeKatE function
*/
class hFunctionList : public QList<hFunction*>
{
private:     
   
     QString _lastError;                ///< The value of the last error
     
public:

     /// \brief Constructor hFunction class.
     hFunctionList(void);
     
     /// \brief Destructor hFunction class.
     ~hFunctionList(void);
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_content - last error message
     /// \return no return value
     void setLastError(QString __error_content);
     
     /// \brief Function that sets the last error
     ///
     /// \param __error_code - code of the error
     /// \param __error_desc - the additional error description
     /// \return returns the given error code
     int setLastError(int __error_code, QString __error_desc = "");
     
     // -----------------
     
     /// \brief Fills the list with the new instances of object of each function
     ///
     /// \return no value return
     void fill(void);
     
     /// \brief Function that removes all the items from the set
	///
	/// \return no value return.
     void flush(void);
     
     /// \brief Function that appends the object
     ///
     /// \param *__new_function - poiter to the new object
     /// \return the result succes if such object does not exists, otherwise error
     int registerFunction(hFunction* __new_function);
     
     /// \brief Function that search for function that has the given internal name
     ///
     /// \param __representation - the name of the function (unique)
     /// \param __representationType - the type of the function representation
     /// \return index in the list if the function exists, otherwise -1
     int findFunction(QString __representation, int __representationType = FUNC_REPRESENTATION_INTERNAL);
     
     /// \brief Function that search for function that are compatible with given parameters
     ///
     /// \param *__returned_type - the type that is returned by the function
     /// \param multiplicity - denotes if the result should be single or multiple
     /// \param __create_copies - denotes if the function should create new instance of the function object
     /// \return the list of pointers to the function object that are compatible with given parameters
     QList<hFunction*> findFunctions(hType* __returned_type, int __multiplicity, bool __create_copies = false,
                                     const QList<int>& __arg_types = QList<int>());

     /// \brief Function that returns all defined operators
     ///
     /// \param __create_copies - denotes if the function should create new instance of the function object
     /// \return the list of pointers to the operators
     QList<hFunction*> findOperators(bool __create_copies = false);

     
     // -----------------
     
     /// \brief Function that returns the last error message
     ///
     /// \return the last error message as string
     QString lastError(void);
     
     /// \brief Function that search for function that has given internal or user representation
     ///
     /// \param __representation - the representation of the function
     /// \param __internal - denotes if given representation is internal or user friendly
     /// \return the index of the function. If the function does not exists return -1
     int indexOf(QString __representation, bool __internal = true);
};
// -----------------------------------------------------------------------------

extern hFunctionList* funcList;
// -----------------------------------------------------------------------------
#endif
