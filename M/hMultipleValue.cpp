/*
 *	   $Id: hMultipleValue.cpp,v 1.19.4.3.2.2 2011-06-13 17:02:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>
#include <iostream>

#include "../C/Settings_ui.h"
#include "../namespaces/ns_hqed.h"

#include "XTT/XTT_Attribute.h"
#include "XTT/XTT_AttributeGroups.h"

#include "hSet.h"
#include "hType.h"
#include "hValue.h"
#include "hSetItem.h"
#include "hFunction.h"
#include "hExpression.h"
#include "hFunctionList.h"
#include "hMultipleValue.h"
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
hMultipleValue::hMultipleValue(void)
{
     _singleRequired = false;
     _valueType = NULL;
     _type = NOT_DEFINED;
     _value = hSet::makeNotDefinedValue(_valueType);
     _attribute = NULL;
     _expression = new hExpression;
}
// -----------------------------------------------------------------------------

// #brief Copying constructor
//
// #param hMultipleValue* __source - the source object
hMultipleValue::hMultipleValue(hMultipleValue* __source)
{
     _singleRequired = __source->_singleRequired;
     _valueType = __source->_valueType;
     _type = __source->_type;
     _value = __source->_value->copyTo();
     _attribute = __source->_attribute;
     _expression = __source->_expression->copyTo();
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
//
// #param int __type - the type of value definition
hMultipleValue::hMultipleValue(ValueType __type)
{    
     _singleRequired = false;
     _type = __type;
     _valueType = NULL;
     if(_type == ANY_VALUE)
          _value = hSet::makeAnyValue(_valueType);
     else
          _value = hSet::makeNotDefinedValue(_valueType);
     _attribute = NULL;
     _expression = new hExpression;
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
//
// #param int __type - the type of value definition
// #param hType* __valueType - the pointer to the type object that is a parent type of the value
hMultipleValue::hMultipleValue(ValueType __type, hType* __valueType)
{
     _singleRequired = false;
     _type = __type;
     _valueType = NULL;
     if(_type == ANY_VALUE)
          _value = hSet::makeAnyValue(_valueType);
     else
          _value = hSet::makeNotDefinedValue(_valueType);
     _attribute = NULL;
     _expression = new hExpression;
     setParentType(__valueType);
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
//
// #param *__valueType - pointer to the value type
// #param *__value - initial value
// #param __sinlge_required - determinres if the value shoud be single or multiple, default is true
hMultipleValue::hMultipleValue(hType* __valueType, hValue* __value, bool __sinlge_required)
{    
     if(__value == NULL)
          return;
          
     if(__value->type() == NOT_DEFINED)
     {
          _type = NOT_DEFINED;
          _value = hSet::makeNotDefinedValue(__valueType);
          return;
     }
     if(__value->type() == ANY_VALUE)
     {
          _type = ANY_VALUE;
          _value = hSet::makeAnyValue(__valueType);
          return;
     }

     hSet* set_value = hValue::toSet(__valueType, __value);
     
     _type = VALUE;
     _valueType = __valueType;
          
     _singleRequired = __sinlge_required;
     _attribute = NULL;
     _expression = new hExpression;
     _value = set_value;
     
     if(__valueType != NULL)
          setParentType(__valueType);
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
//
// #param hSet* __value - initial value
hMultipleValue::hMultipleValue(hSet* __value)
{
     _type = VALUE;
     _valueType = NULL;
          
     _singleRequired = false;
     _attribute = NULL;
     _expression = new hExpression;
     _value = __value;
     if((__value != NULL) && (__value->parentType() != NULL))
          setParentType(__value->parentType());
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValue class.
//
// #param XTT_Attribute* __xtt_attribute - attribute reference
hMultipleValue::hMultipleValue(XTT_Attribute* __xtt_attribute)
{
     _singleRequired = !__xtt_attribute->multiplicity();
     _type = ATTRIBUTE;
     _value = hSet::makeNotDefinedValue(_valueType);
     _attribute = __xtt_attribute;
     _expression = new hExpression;
     setParentType(__xtt_attribute->Type());
}
// -----------------------------------------------------------------------------

// #brief Destrucotr hMultipleValue class.
hMultipleValue::~hMultipleValue(void)
{    
     if(_expression != NULL)
          delete _expression;
     if(_value != NULL)
          delete _value;
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param hMultipleValue* __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hMultipleValue* hMultipleValue::copyTo(hMultipleValue* __destinationItem)
{
     hMultipleValue* res = __destinationItem;
     if(res == NULL)
          res = new hMultipleValue;
          
     res->_singleRequired = _singleRequired;
     res->_valueType = _valueType;
     res->_type = _type;
     res->_value->flush();
     _value->copyTo(res->_value);
     res->_attribute = _attribute;
     _expression->copyTo(res->_expression);
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the type of the value
//
// #param int __type - new value of type
// #return No value return
void hMultipleValue::setType(ValueType __type)
{
     if((__type != NOT_DEFINED) &&
        (__type != ANY_VALUE) &&
        (__type != VALUE) &&
        (__type != ATTRIBUTE) &&
        (__type != EXPRESSION))
          return;
          
     _type = __type;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the value
//
// #param hSet* __value - the value set
// #return No value return
void hMultipleValue::setValue(hSet* __value)
{
     setType(VALUE);
     if((_value != __value) && (_value != NULL))  // FIXME if necessary ofcourse
     {
          delete _value;
          _value = NULL;
     }
          
     _value = __value;
     setParentType(_valueType);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the attribute pointer
//
// #param *__attribute - the pointer to the attribute
// #return No value return
void hMultipleValue::setAttribute(XTT_Attribute* __attr)
{
     setType(ATTRIBUTE);
     _attribute = __attr;
     if(__attr != NULL)
          _singleRequired = __attr->multiplicity();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the message of the last error
//
// #param __msg - the content of the last error message
// #return No value returs
void hMultipleValue::setLastError(QString __msg)
{
     _lastError = __msg;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the message of the last error
//
// #param __error_code - the code of the error
// #param __additional_msg - the additional message of the error
// #return the error code
int hMultipleValue::setLastError(int __error_code, QString __additional_msg)
{
     setLastError(__additional_msg);
     return __error_code;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the poiter to the expression object
//
// #param hExpression* __expression - pointer to the new expression object
// #return No value return
void hMultipleValue::setExpression(hExpression* __expression)
{
     setType(EXPRESSION);
     if((_expression != __expression) && (_expression != NULL))  // FIXME if necessary ofcourse
     {
          delete _expression;
          _expression = NULL;
     }
     
     _expression = __expression;
     setParentType(_valueType);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the requirement of single value
//
// #param bool __sr_val - new value of requirement
// #return No value return
void hMultipleValue::setSingleRequirement(bool __sr_val)
{
     _singleRequired = __sr_val;
     if(__sr_val)
          makeItSingle();
     //if(__sr_val)
     //     _type = MULTIPLE_VALUE_TYPE_VALUE;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the object have only single values
//
// #return true if yes, false if no
bool hMultipleValue::isSingle(void)
{
     if(_type == VALUE)       // Checking if the set has only one value
          return _value->isSingleValue();
     if(_type == ATTRIBUTE)   // Checking attribute multiplicity
          return !_attribute->multiplicity();
     if(_type == EXPRESSION)  // Checking if the function cannot return single value
     {
          if(_expression->function() == NULL)
               return false;
               
          int fi = funcList->findFunction(_expression->function()->internalRepresentation());
          if(fi == -1)
               return false;
               
          if(((funcList->at(fi)->returnedTypes() & FUNCTION_TYPE__SINGLE_VALUED) != 0) && ((funcList->at(fi)->returnedTypes() & FUNCTION_TYPE__MULTI_VALUED) == 0))
               return true;
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that transforms the current object that contains a set of values to an object that contains a single value
//
// #return true on success otherwise false
bool hMultipleValue::makeItSingle(void)
{
     if(isSingle())
          return true;
     
     if(_type == VALUE)
     {
          _singleRequired = true;
          return _value->makeItSingle();
     }
     if((_type == ATTRIBUTE) || (_type == EXPRESSION))
     {
          setType(VALUE);
          setValue(hSet::makeAnyValue(parentType()));
          _singleRequired = true;
          return true;
     }
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hMultipleValue::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     if(_valueType == __old)
          _valueType = __new;
          
     res = _value->updateTypePointer(__old, __new) && res;
     res = _expression->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hMultipleValue::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* att_ptr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          // Parent type update
          if((_type == ATTRIBUTE) && (_attribute == att_ptr))
          {
               if(att_ptr == NULL)
                    _type = NOT_DEFINED;
               if(att_ptr != NULL)
               {
                    _valueType = att_ptr->Type();
                    _singleRequired = !att_ptr->multiplicity();
               }
          }
          
          // Sending message further
          res = _value->eventMessage(__errmsgs, __event, att_ptr) && res;
          res = _expression->eventMessage(__errmsgs, __event, att_ptr) && res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* att_ptr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          // Parent type update
          if((_type == ATTRIBUTE) && (_attribute == att_ptr))
          {
               _type = NOT_DEFINED;
               _attribute = NULL;
          }
          
          // Sending message further
          res = _value->eventMessage(__errmsgs, __event, att_ptr) && res;
          res = _expression->eventMessage(__errmsgs, __event, att_ptr) && res;
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hMultipleValue::isAVRused(void)
{
     if(_type == VALUE)
          return _value->isAVRused();
     if(_type == ATTRIBUTE)
          return true;
     if(_type == EXPRESSION)
          return _expression->isAVRused();
        
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hMultipleValue::isExpressionUsed(void)
{
     if(_type == VALUE)
          return _value->isExpressionUsed();
     if(_type == ATTRIBUTE)
          return false;
     if(_type == EXPRESSION)
          return true;
        
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the multiple value object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hMultipleValue::multipleValueVerification(QString& __error_message)
{
     bool res = true;

     if(_valueType == NULL)
     {
          __error_message += "\nThe complex value " + toString() + " has not defined type.";
          return false;
     }

     // When fixed set
     if(_type == VALUE)
     {
          if(_value == NULL)
          {
               __error_message += "\nCritical error: Error from complex value. The value is unalocated.";
               res = false;
          }
          
          QString setErrs;
          if(((_value != NULL)) && (!_value->setVerification(setErrs)))
          {
               __error_message += "\nError of complex value " + toString() + " set definition: " + setErrs;
               res = false;
          }
          
          if(((_value != NULL)) && (_valueType->isSetCorrect(_value, false /* ??? FIXME ??? */) != H_TYPE_ERROR_NO_ERROR))
          {
               __error_message += "\nError from complex value " + toString() + " set definition: " + _valueType->lastError();
               res = false;
          }
     }

     // When attribute
     if(_type == ATTRIBUTE)
          res = _attribute->attributeVerification(__error_message) && res;

     // When expression
     if(_type == EXPRESSION)
          res = _expression->expressionVerification(__error_message) && res;

     // If the avr is not used we can calcualte the value of the object
     if((!isAVRused()) && 
        ((_type != VALUE) || ((bool)res))     // to not repeat the case for fixed set
       )
     {
          hMultipleValueResult res = value();
          if(_valueType->isSetCorrect(res.toSet(false), true) != H_TYPE_ERROR_NO_ERROR)      // true bo avr is not used
          {
               __error_message += "\nError from complex value " + toString() + ": " + _valueType->lastError();
               res = false;
          }
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the parent type of the value
//
// #param hType* __parentType - pointer to the value type
// #return true if all is ok otherwise false
bool hMultipleValue::setParentType(hType* __parentType)
{
     if(__parentType == NULL)
          return false;

     bool res = true;

     _valueType = __parentType;
     res = _value->setParentType(__parentType) && res;
     res = _expression->setRequiredType(__parentType) && res; 
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value type
//
// #return Return the pointer to the type of value
hType* hMultipleValue::parentType(void)
{
     return _valueType;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the message of the last error
//
// #return Return the message of the last error
QString hMultipleValue::lastError(void)
{
     return _lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of this object as string
//
// #return The current value of this object as string
hMultipleValueResult hMultipleValue::value(void)
{
     hMultipleValueResult res;
     hMultipleValue* cacl_result;
     
     switch(_type)
     {
          case NOT_DEFINED:
               res = hMultipleValueResult(hSet::makeNotDefinedValue(_valueType));
               break;
               
          case ANY_VALUE:
               res = hMultipleValueResult(hSet::makeAnyValue(_valueType));
               break;
               
          case VALUE:
               res = hMultipleValueResult(_value->toVals());
               break;
               
          case ATTRIBUTE:
               res = _attribute->Value()->value();
               break;
               
          case EXPRESSION:
               cacl_result = _expression->calculate();
               if(cacl_result == NULL)
                    res = hMultipleValueResult(_expression->lastError());
               else if(cacl_result != NULL)
               {
                    res = cacl_result->value();
                    delete cacl_result;
               }
               break;
               
          default:
               res = hMultipleValueResult(hSet::makeNotDefinedValue(_valueType));
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that tries to match the current value to format that is required for this type
//
// #param __allowTrunc - denotes if the digiths can be cut
// #return The result code
int hMultipleValue::toCurrentNumericFormat(bool __allowTrunc)
{
     if(type() == VALUE)
          return _value->toCurrentNumericFormat(__allowTrunc);
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the form of the value as value, attribute name, expression form as string
//
// #param __forceSetParentheses - add set delimiters ("{}") to string representation even if set contains only one item (applies only when
//  type of the stored value is VALUE)
// #return The form of the value as value, attribute name, expression form as string
QString hMultipleValue::toString(bool __forceSetParentheses)
{
     QString res = "";
     QString (XTT_Attribute::*reffunc)(void) = Settings->attsReferenceByAcronyms() == true ? &XTT_Attribute::Acronym : &XTT_Attribute::Name;
     
     switch(_type)
     {
          case NOT_DEFINED:
               res = XTT_Attribute::_not_definded_operator_;
               break;
               
          case ANY_VALUE:
               res = XTT_Attribute::_any_operator_;
               break;
               
          case VALUE:
               res = _value->toString(__forceSetParentheses);
               break;
               
          case ATTRIBUTE:
               res = AttributeGroups->CreatePath(_attribute) + (_attribute->*reffunc)();
               break;
               
          case EXPRESSION:
               res = _expression->toString();
               break;
               
          default:
               res = "unsupported type of value";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the form of the value as value, attribute name, expression form in drools5 format
//
// #return The form of the value as value, attribute name, expression form in drools5 format
QString hMultipleValue::toDrools5(void)
{
     if(_type == ATTRIBUTE)
          return "w.get" + _attribute->CapitalizedName() + "()";

     else if(_type == VALUE)
          return _value->toDrools5();

     else if(_type == EXPRESSION)
          return _expression->toDrools5();

     else if(_type == NOT_DEFINED)
          return XTT_Attribute::_not_definded_operator_;

     else if(_type == ANY_VALUE)
          return XTT_Attribute::_any_operator_;

     return "";
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string list representation of the items
//
// #return The string list representation of the items
QStringList hMultipleValue::stringItems(void)
{
     if(_type == VALUE)
          return _value->stringItems();
     
     return QStringList() << toString();
}
// -----------------------------------------------------------------------------

// #brief Function that maps the object to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents object as string.
QString hMultipleValue::toProlog(int /*__option*/)
{
     QString res = "";
     switch(_type)
     {
          case NOT_DEFINED:
               res = hqed::mcwp(XTT_Attribute::_not_definded_operator_);
               break;
               
          case ANY_VALUE:
               res = hqed::mcwp(XTT_Attribute::_any_operator_);
               break;
               
          case VALUE:
               res = _value->toProlog();
               break;
               
          case ATTRIBUTE:
               res = hqed::mcwp(_attribute->Path());
               break;
               
          case EXPRESSION:
               res = _expression->toProlog();
               break;
               
          default:
               res = "unsupported type of value";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the attribute field
//
// #return the string representation of the attribute field
QString hMultipleValue::attributeFieldToString(void)
{
     if(_attribute == NULL)
          return "Attribute is not selected";
 
     QString res = AttributeGroups->CreatePath(_attribute) + _attribute->Name();
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of value types as string representation
//
// #return Return the list of value types as string representation
QStringList hMultipleValue::stringTypes(void)
{
     return QStringList() << XTT_Attribute::_not_definded_operator_ << XTT_Attribute::_any_operator_ << "value" << "attref" << "expression";
}
// -----------------------------------------------------------------------------

// #brief Function that returns the message of the last error
//
// #param __type - the type of the value
// #param __strValue - the candidate for the value given as string
// #param __check_contraints - denotes if the value must be siutable with type domain
// #return Return the poniter to the created value, if error returns NULL
hMultipleValue* hMultipleValue::makeSingleValue(hType* __type, QString __strValue, bool __check_contraints)
{
     if(__type == NULL)
          return NULL;
          
     hSet* hs = new hSet(__type);
     hSetItem* hsi = new hSetItem(hs, SET_ITEM_TYPE_SINGLE_VALUE, new hValue(__strValue), new hValue, false);
     hs->add(hsi);
     
     if(__type->isItemCorrect(hsi, __check_contraints) != H_TYPE_ERROR_NO_ERROR)
          return NULL;
          
     hMultipleValue* result = new hMultipleValue(hs);
     result->setSingleRequirement(true);
          
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hMultipleValue::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(__mode == 4)
     {
          hMultipleValueResult set = value();
          QStringList* tmp = set.toSet(false)->out_HML_2_0(__mode);
          for(int i=0;i<tmp->size();++i)
               *res << tmp->at(i);
          
          delete tmp;
          return res;
     }
     
     if(_type == NOT_DEFINED)
     {
          hSet* s = hSet::makeNotDefinedValue(_valueType);
          QStringList* tmp = s->out_HML_2_0(__mode);
          for(int i=0;i<tmp->size();++i)
               *res << tmp->at(i);
          delete tmp;
          delete s;
     }
     if(_type == ANY_VALUE)
     {
          hSet* s = hSet::makeAnyValue(_valueType);
          QStringList* tmp = s->out_HML_2_0(__mode);
          for(int i=0;i<tmp->size();++i)
               *res << tmp->at(i);
          delete tmp;
          delete s;
     }
     if(_type == VALUE)
     {
          QStringList* tmp = _value->out_HML_2_0(__mode);
          for(int i=0;i<tmp->size();++i)
               *res << tmp->at(i);
          delete tmp;
     }
     if(_type == ATTRIBUTE)
          *res << "<attref ref=\"" + _attribute->Id() + "\"/>";
     if(_type == EXPRESSION)
     {
          QStringList* expr = _expression->out_HML_2_0(3);
          for(int i=0;i<expr->size();++i)
               *res << expr->at(i);
          delete expr;
     }

     return res;
}

// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void hMultipleValue::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(__mode == 4)
     {
          hMultipleValueResult res = value();
          res.toSet(false)->out_HML_2_0(_xmldoc, __mode);
          return;
     }
     
     if(_type == NOT_DEFINED)
     {
          hSet* s = hSet::makeNotDefinedValue(_valueType);
          s->out_HML_2_0(_xmldoc, __mode);
          delete s;
     }
     if(_type == ANY_VALUE)
     {
          hSet* s = hSet::makeAnyValue(_valueType);
          s->out_HML_2_0(_xmldoc, __mode);
          delete s;
     }
     if(_type == VALUE)
          _value->out_HML_2_0(_xmldoc, __mode);
     if(_type == ATTRIBUTE)
     {
          _xmldoc->writeEmptyElement("attref");
          _xmldoc->writeAttribute("ref", _attribute->Id());
     }
     if(_type == EXPRESSION)
          _expression->out_HML_2_0(_xmldoc, 3);
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #param __cellExpression - denotes if this object represents the whole cell expression
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hMultipleValue::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression)
{
     int result = 0;
     QStringList possibleTags = QStringList() << "set" << "attref" << "eval" << "expr" << "value";
     
     int tagIndex = possibleTags.indexOf(__root.tagName().toLower());
     if(tagIndex == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined or misplaced tag \'" + __root.tagName() + "\'.\nAllowed tag names are: " + possibleTags.join(", ") + "\nThe object will not be read." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
          return result;
     }
     
     // When set
     if(tagIndex == 0)
     {
          setType(VALUE);
          _value->clear();       
          result += _value->in_HML_2_0(__root, __msg, __attr);
     }
     
     // When attref
     if(tagIndex == 1)
     {
          setType(ATTRIBUTE);
          
          QDomElement DOMattref = __root;
          QString aref = DOMattref.attribute("ref", "");
          if(aref == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined attribute reference \'" + aref + "\'.\nError from \'" + DOMattref.tagName() + "\' element.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
               result++;
               return result;
          }
          
          XTT_Attribute* attr = AttributeGroups->FindAttribute(aref);
          if(attr == NULL)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Attribute reference error. Undefined attribute identifier \'" + aref + "\'.\nError from \'" + DOMattref.tagName() + "\' element.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
               result++;
               return result;
          }
          
          if((attr->multiplicity()) || (__cellExpression))
               _attribute = attr;
          if((!attr->multiplicity()) && (!__cellExpression))
          {
               setType(VALUE);
               _value->clear();
               
               hValue* val = new hValue(attr);
               hSetItem* si = new hSetItem(_value, SET_ITEM_TYPE_SINGLE_VALUE, val, new hValue, false);
               _value->add(si);
          }
     }
     
     // When eval
     if(tagIndex == 2)
     {
          setType(EXPRESSION);
          result += _expression->in_HML_2_0(__root, __msg, __attr);
     }
     
     // When expr
     if(tagIndex == 3)
     {
          setType(VALUE);
          _value->clear();
          
          hValue* val = new hValue(EXPRESSION);
          hSetItem* si = new hSetItem(_value, SET_ITEM_TYPE_SINGLE_VALUE, val, new hValue, false);
          _value->add(si);
          
          result += val->expressionField()->in_HML_2_0(__root, __msg, __attr);
     }
     
     // When value
     if(tagIndex == 4)
     {
          setType(VALUE);
          _value->clear();
          
          hValue* val = new hValue(VALUE);
          hSetItem* si = new hSetItem(_value, SET_ITEM_TYPE_SINGLE_VALUE, val, new hValue, false);
          _value->add(si);
          
          result += si->in_HML_2_0(__root, __msg, __attr);
     }
     _singleRequired = isSingle();
     
     return result;
}
// -----------------------------------------------------------------------------
