/*
 *	   $Id: hMultipleValueResult.cpp,v 1.11 2010-01-08 19:47:35 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "hSet.h"
#include "hMultipleValueResult.h"
// -----------------------------------------------------------------------------

// #brief Constructor hValueResult class.
hMultipleValueResult::hMultipleValueResult(void)
{
     _succes = true;
     _error = "";
     _result = NULL;
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValueResult class.
//
// #param __result - the result set
hMultipleValueResult::hMultipleValueResult(hSet* __result)
{
     _succes = true;
     _result = __result;
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValueResult class.
//
// #param QString __msg - the message as string. When __succes is true then __msg is stored in _result if false in _error
hMultipleValueResult::hMultipleValueResult(QString __error_msg)
{
     _error = __error_msg;
     _result = NULL;
     _succes = false;
}
// -----------------------------------------------------------------------------

// #brief Constructor hMultipleValueResult class.
//
// #param bool __succes - denotes if the result of calculation finished successfully
hMultipleValueResult::hMultipleValueResult(bool __succes)
{
     _error = "";
     _result = NULL;
     _succes = __succes;
}
// -----------------------------------------------------------------------------

// #brief Destructor hMultipleValueResult class.
hMultipleValueResult::~hMultipleValueResult(void)
{
     if(_result != NULL)
          delete _result;
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param *__dest - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hMultipleValueResult* hMultipleValueResult::copyTo(hMultipleValueResult* __dest)
{
     hMultipleValueResult* res = __dest;
     if(res == NULL)
          res = new hMultipleValueResult;
          
     res->_succes = _succes;
     res->_error = _error;
     res->_result = _result->copyTo();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Overloaded assignment operator
//
// #param & __right - the reference to the right object that is a source of data
// #return the current object
hMultipleValueResult& hMultipleValueResult::operator=(hMultipleValueResult __right)
{
     _result = NULL;
     if(__right._result != NULL)
          _result = __right._result->copyTo();
          
     _error = __right._error;
     _succes = __right._succes;
     
     return *this;
}
// -----------------------------------------------------------------------------

// #brief To boolean conversion method
//
// #return the boolean representation of object
hMultipleValueResult::operator bool(void)
{
     return _succes;
}
// -----------------------------------------------------------------------------

// #brief To QString conversion method
//
// #return the QString representation of object. The value of the string depends on _succes field value. When true returns _result otherwise _error field
hMultipleValueResult::operator QString(void)
{
     return _error;
}
// -----------------------------------------------------------------------------

// #brief To hSet* conversion method
//
// #param __make_copy - denotes if the copy of the set must be created
// #return the hSet* representation of result.
hSet* hMultipleValueResult::toSet(bool __make_copy)
{
     if(_result == NULL)
          return new hSet;
          
     hSet* result = _result;
     if(__make_copy)
          result = result->copyTo();
          
     return result;
}
// -----------------------------------------------------------------------------

