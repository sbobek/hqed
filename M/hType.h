 /**
 * \file	hType.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008 
 * \version	1.15
 * \brief	This file contains class definition that represents the type of the XTT attribute.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HTYPEH
#define HTYPEH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QMenu>
#include <QFile>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

// The identifiers of the type class
#define NO_TYPE_CLASS     0
#define TYPE_CLASS_DOMAIN 1
#define TYPE_CLASS_GROUP  2

// The identifiers of presentation methods
#define TYPE_PRESENTATION_ORIGIN 0
#define TYPE_PRESENTATION_ECMA   1
#define TYPE_PRESENTATION_DEVPC  2

// Identifiers of the primitive types
#define BOOLEAN_BASED  0
#define INTEGER_BASED  1
#define NUMERIC_BASED  2
#define SYMBOLIC_BASED 3
#define VOID_BASED     4
#define ACTION_BASED   5

// The range of numeric types
#define NUMERIC_INFIMUM INT_MIN
#define NUMERIC_SUPREMUM INT_MAX
#define BOOLEAN_FALSE 0
#define BOOLEAN_TRUE 1

// Errors
#define H_TYPE_ERROR_NO_ERROR                  0
#define H_TYPE_ERROR_TYPE_UNORDERED           -1
#define H_TYPE_ERROR_TYPE_INCONSISTENT        -2
#define H_TYPE_ERROR_VALUE_OUT_OF_RANGE       -3
#define H_TYPE_ERROR_INCORRECT_NUMBER_FORMAT  -4
#define H_TYPE_ERROR_INCORRECT_VALUE_TYPE     -5
#define H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT -6
#define H_TYPE_ERROR_INCOORECT_EXTREME_VALUES -7
#define H_TYPE_ERROR_PREVIOUS_VALUE_NOT_FOUND -8
#define H_TYPE_ERROR_NULL_VALUE               -9
#define H_TYPE_ERROR_ANY_VALUE                -10
#define H_TYPE_ERROR_OTHER_OBJECT_ERROR       -11
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hValue;
class hDomain;
class hSetItem;
class hTypeList;
class hMultipleValue;
// -----------------------------------------------------------------------------

/**
* \class 	hType
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that represents the type of the XTT attribute.
*/
class hType
{

private:

     /// The class of the type (the values are defined in define section)
     /// \li NO_TYPE_CLASS - no constraints defined
     /// \li TYPE_CLASS_DOMAIN - constraints defined as domain
     /// \li TYPE_CLASS_GROUP - constraints defined with the help of group (n/a at now)
     int _class;
     
     QString _id;                       ///< Identifier of type
     QString _name;                     ///< The name of the type
     QString _description;              ///< The desription of the type
     QString _lastError;                ///< The last error message
     
     int _baseType;                     ///< Define the primitive base type of this type
     int _length;                       ///< The length of the numeric value (digiths count without decimal symbol)
     int _scale;                        ///< In case of numeric - the number of digiths after decimal symbol
     
     hDomain* _domain;                  ///< The poiter to the domain object
     hTypeList* _typeList;              ///< Parent list of this type
     
     // Presentation parameters
     int _presentationSourceID;         ///< Identifier of the presentation source: TYPE_PRESENTATION_ORIGIN or TYPE_PRESENTATION_ECMA or TYPE_PRESENTATION_DEVPC
     QString  _presentationSourceName;  ///< The name of the source: in case of plugin it is plugin name, in case of script it is file name
     QString  _presentationSourceType;  ///< This parameter is important only in case of plugin-based presentation and it indicates the name of the presentation service

public:

     /// \brief Constructor hType class.
     ///
     /// \param __typeList - pointer to the type list in which this type is created
     hType(hTypeList* __typeList);
     
     /// \brief Constructor hType class.
     ///
     /// \param __typeList - pointer to the type list in which this type is created
     /// \param __class - identifer of the type class
     /// \param __type - identifer of the primitive type
     hType(hTypeList* __typeList, int __class, int __type);
     
     /// \brief Destructor hType class.
     virtual ~hType(void);
     
     /// \brief Function sets the object properties to default
     ///
     /// \param __typeList - pointer to the type list in which this type is created
     /// \return no values return
     virtual void initObject(hTypeList* __typeList);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     virtual hType* copyTo(hType* __destinationItem = NULL);
     
     /// \brief Function that returns the pointer to the domain
	///
	/// \return The pointer to the domain object
     hDomain* domain(void) { return _domain; }

     /// \brief Function that returns the type identifier
	///
	/// \return The type identifier as string
     virtual QString id(void){ return _id; }
     
     /// \brief Function that returns the description
	///
	/// \return The description of the type as string
     QString description(void){ return _description; }
     
     /// \brief Function that returns the id of the presentation source
	///
	/// \return the id of the presentation source
     int presentationSourceId(void);
     
     /// \brief Function that returns the name of the presentation source
	///
	/// \return the name of the presentation source
     QString presentationSourceName(void);
     
     /// \brief Function that returns the type of the presentation source
	///
	/// \return the type of the presentation source
     QString presentationSourceType(void);
     
     /// \brief Function that returns the name of the type. If the type is not constrained the name is "" the name generated automatically
	///
	/// \return The name of the type as string.
     QString name(void);
     
     /// \brief Function that sets the ID of the type
	///
     /// \param __id - new id of the type
	/// \return No value return
     void setID(QString __id);
     
     /// \brief Function that sets the name of the type
	///
     /// \param __name - new name of the type
	/// \return No value return
     void setName(QString __name);
     
     /// \brief Function that sets the description of the type
	///
     /// \param __description - new description of the type
	/// \return No value return
     void setDescription(QString __description);
     
     /// \brief Function that sets the id of the presentation source
	///
     /// \param __psrcid - new id of the presentation source
	/// \return No value return
     void setPresentationSourceId(int __psrcid);
     
     /// \brief Function that sets the name of the presentation source
	///
     /// \param __psrcname - new name of the presentation source
	/// \return No value return
     void setPresentationSourceName(QString __psrcname);
     
     /// \brief Function that sets the type of the presentation source
	///
     /// \param __psrctype - new type of the presentation source
	/// \return No value return
     void setPresentationSourceType(QString __psrctype);
     
     /// \brief Function that returns the information about type
	///
     /// \param __html - denotes if the html tag should be used
	/// \return the information about type as string
     QString typeInfo(bool __html = false);
     
     /// \brief Function that maps the type to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this type as string.
     virtual QString toProlog(int __option = -1);
     
     /// \brief Function that returns the last error message
	///
	/// \return The last error message as string
     QString lastError(void){ return _lastError; }
     
     /// \brief Function that returns the class of this type
	///
	/// \return The class of this type as integer
     int typeClass(void){ return _class; }
     
     /// \brief Function that returns the identifier of the primitive type
	///
	/// \return The identifier of the primitive type. The identifiers are defined above
     int baseType(void){ return _baseType; }
     
     /// \brief Function that returns the allowed length of the numeric value
	///
	/// \return The allowed length of the numeric value
     int length(void){ return _length; }
     
     /// \brief Function that returns the number of digithis after decimal symbol
	///
	/// \return The number of digithis after decimal symbol
     int scale(void){ return _scale; }
     
     /// \brief Function that returns the number of digithis after decimal symbol.
	///
	/// \return The number of digithis after decimal symbol as integer.
     int allowedDecimalLength(void);
     
     /// \brief Function that returns the number of digithis before decimal symbol
	///
	/// \return The number of digithis before decimal symbol
     int decimalSize(void);

     /// \brief Function that returns if the type identifier is correct
     ///
     /// \param __ct - The candidate type identifier
     /// \return true if the identifier is correct
     bool isTypeCorrect(int __ct);
     
     /// \brief Function that returns if the class of the type is correct
	///
     /// \param __cc - The candidate class identifier
	/// \return true if the identifier is correct
     bool isClassCorrect(int __cc);
     
     /// \brief Function that checks if the set is correct
     ///
     /// \param __set - pointer to the set object
     /// \param __checkConstraints - denotes if value should be compatible with constraints
     /// \return 0 on success otherwise error code
     int isSetCorrect(hSet* __set, bool __checkConstraints);
     
     /// \brief Function that checks if the values are correct
     ///
     /// \param __type - type of the item
     /// \param  __from - from value
     /// \param __to - to value
     /// \param __checkConstraints - denotes if value should be compatible with constraints
     /// \return 0 on success otherwise error code
     int isItemCorrect(int __type, hValue* __from, hValue* __to, bool __checkConstraints);
     
     /// \brief Function that checks if the item is correct
     ///
     /// \param __item - pointer to the item
     /// \param __checkConstraints - denotes if value should be compatible with constraints
     /// \return 0 on success otherwise error code
     int isItemCorrect(hSetItem* __item, bool __checkConstraints);
     
     /// \brief Function that tries to setup the baseType of this object
	///
     /// \param __nt - The candidate type identifier
	/// \return true if the identifier is correct
     bool setType(int __nt);
     
     /// \brief Function that tries to setup the class of this object
	///
     /// \param __nc - The candidate class identifier
	/// \return true if the identifier is correct
     bool setClass(int __nc);
     
     /// \brief Function that returns the number of important digiths before the decimal symbol
     ///
     /// \param __value - The value as string
     /// \return if the result is non negative - the number of digiths otherwise error code
     int decimalCount(QString __value);
     
     /// \brief Function that returns the number of digiths after the decimal symbol
     ///
     /// \param __value - The value as string
     /// \return if the result is non negative - the number of digiths otherwise error code
     int factorialCount(QString __value);
     
     /// \brief Function that returns the numeric representation of the value (only in case of ordres domains). In case of integer and nueric types the result is equal to __value
     ///
     /// \param __value - The value as string
     /// \param __numValue - The numeric representation of the value. In case of integer and nueric types the result is equal to __value
     /// \param __check_constraints - if true, the value should satisfy the type constraints
     /// \return mapping result code.
     int mapToNumeric(QString __value, double& __numValue, bool __check_constraints = true);
     
     /// \brief Function that returns the string representation of the numeric value (only in case of ordres domains). In case of integer and nueric types the result is equal to __num_value
     ///
     /// \param __num_value - The numeric representation of the value. In case of integer and nueric types the result is equal to __value
     /// \param __value - The reference to value as string
     /// \param __check_constraints - if true, the value should satisfy the type constraints
     /// \return mapping result code.
     int mapFromNumeric(QString __num_value, QString& __value, bool __check_constraints = true);

     /// \brief Function that translates the reference value with the help of plugin
     ///
     /// \param __refvalue - input value to translation
     /// \param __ok - reference to the value that contains the exit code: H_TYPE_ERROR_NO_ERROR on succes or other on error
     /// \param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
     /// \return translated value, or error if __ok != H_TYPE_ERROR_NO_ERROR
     QString translate(QString __refvalue, int& __ok, bool __isInput);
     
     /// \brief Function that translates the given value using defined plugin
	///
     /// \param __value - value to be translated
     /// \param __ok - indicates if the returned value is a result of a translation or error message
	/// \return translared value or error message
     QString translatePlugin(QString __value, int& __ok);
     
     /// \brief Function that returns previous value that is compatible with type
     ///
     /// \param __ref_value - the reference value
     /// \param __result - the symbolic representation of previous value
     /// \param __asNumeric - denotes if the result should be returned as numeric representation
     /// \return result code.
     int typePreviousValue(QString __ref_value, QString& __result, bool __asNumeric = false);
     
     /// \brief Function that returns next value that is compatible with type
     ///
     /// \param __ref_value - the reference value
     /// \param __result - the symbolic representation of next value
     /// \param __asNumeric - denotes if the result should be returned as numeric representation
     /// \return result code.
     int typeNextValue(QString __ref_value, QString& __result, bool __asNumeric = false);
     
     /// \brief Function that returns the default value that is compatible with type
     ///
     /// \return pointer to the value.
     hMultipleValue* typeDefaultValue(void);
     
     /// \brief Function that checks if the numeric value that is given as string has correct format according to _length and _scale fields
     ///
     /// \param __value - The value as string
     /// \return result code
     int numericValidator(QString __value);
     
     /// \brief Function that checks if the numeric value that is given as double has correct format according to _length and _scale fields
     ///
     /// \param __value - The value as double
     /// \return result code
     int numericValidator(double __value);
     
     /// \brief Function that checks if the numeric value that is given as integer has correct format according to _length and _scale fields
     ///
     /// \param __value - The value as integer
     /// \return result code
     int numericValidator(int __value);
     
     /// \brief Function that tries to match the current value to format that is required for this type
     ///
     /// \param __value - the given value
     /// \param allowTrunc - denotes if the digiths can be cut
     /// \return The result code
     int toCurrentNumericFormat(QString& __value, bool allowTrunc = false);
     
     /// \brief Function that returns the old identifier of type, that have been used in xttml and attml
     ///
     /// \return The old identifier of type, that have been used in xttml and attml
     int fromHekateType(void);
     
     /// \brief Function that returns the old identifier of contraints type, that have been used in xttml and attml
     ///
     /// \return The old identifier of contraints type, that have been used in xttml and attml
     int toOldXttContraintsType(void);
     
     /// \brief Function that sets the last error message
	///
     /// \param __lastError - the content of the error message
	/// \return No return value.
     void setLastError(QString __lastError);
     
     /// \brief Function that sets the last error message
	///
     /// \param __errorId - the error ID (defined in '#define' section)
     /// \param __errorMsg - addiotional error message
	/// \return The given error code.
     int setLastError(int __errorId, QString __errorMsg = "");
     
     /// \brief Function that sets the length field
	///
     /// \param __length - the new value of the field
	/// \return No return value.
     void setLength(int __length);
     
     /// \brief Function that sets the scale field
	///
     /// \param __scale - the new value of the field
	/// \return No return value.
     void setScale(int __scale);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that returns if the type object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool typeVerification(QString& __error_message);
     
     // -------------- Static members: -------------- 
     
     /// \brief Function that defines the default prefix of the type
	///
	/// \return The default prefix of the type as string (current "tpe_")
     static QString idPrefix(void);
     
     /// \brief Function that returns the list of the names of the primitive types
	///
	/// \return The list of the names of the primitive types
     static QStringList primitiveTypes(void);
     
     /// \brief Function that returns the name of the primitive type
	///
     /// \param __typeIdentifier - identifer of the primitive type
	/// \return The name of the primitive type as string
     static QString primitiveType(int __typeIdentifier);
     
     /// \brief Function that returns the list of identifiers of primitive types
	///
	/// \return The list of identifiers of primitive types
     static int* primitiveTypesIndentifiers(void);
     
     /// \brief Function that returns the count of primitive types
	///
	/// \return The count of primitive types
     static int primitiveTypesCount(void);
     
     /// \brief Function that returns the list of the names of the constraints types
	///
	/// \return The list of the names of the constraints types
     static QStringList constraintsNames(void);
     
     /// \brief Function that returns the list of the presentation sources
	///
	/// \return The list of the presentation sources
     static QStringList presentationSources(void);
     
     /// \brief Function that returns the count of constraints types
	///
	/// \return The count of constraints types
     static int constraintsTypesCount(void);
     
     /// \brief Function that returns the list of identifiers of constraints types
	///
	/// \return The list of identifiers of constraints types
     static int* constraintsIndentifiers(void);
     
     /// \brief Function that returns the old identifier of type, that have been used in xttml and attml
	///
     /// \param __baseType - the base type identifier
	/// \return The old identifier of type, that have been used in xttml and attml
     static int fromHekateType(int __baseType);
     
     /// \brief Function that translates the given value using given script content
	///
     /// \param __scriptcontent - content of the translation script
     /// \param __value - value to be translated
     /// \param __ok - indicates if the returned value is a result of a translation or error message
     /// \param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
    /// \return translared value or error message
     static QString translateScript(QString __scriptcontent, QString __value, int& __ok, bool __isInput);
     
     /// \brief Function that translates the given value using script from the given file
	///
     /// \param __scriptfile - file that contains the script
     /// \param __value - value to be translated
     /// \param __ok - indicates if the returned value is a result of a translation or error message
     /// \param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
	/// \return translared value or error message
     static QString translateScript(QFile& __scriptfile, QString __value, int& __ok, bool __isInput);

     bool translateScriptAvailable(bool __checkInput);

     /// \brief Function gets all data in hml 2.0 format.
	///
	/// \return List of all data in hml 2.0 format.
	virtual QStringList* out_HML_2_0(void);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
	/// \return No retur value.
	virtual void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     QString GetJavaType();
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
        virtual int in_HML_2_0(QDomElement& __root, QString& __msg);
};
// -----------------------------------------------------------------------------

/**
* \class 	hPrimitiveTypesInfo
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that contains the informations about primitive types
*/
class hPrimitiveTypesInfo
{

public:

     int ptc;               ///< Count of primitive types
     int cc;                ///< Constraints types count
     int* ptids;            ///< Identifiers of primitive types 
     int* cids;             ///< Identifiers of constraints 
     QStringList ptn;       ///< Names of primitive types
     QStringList cn;        ///< Names of constraints
     
     /// \brief Constructor hPrimitiveTypesInfo class.
     hPrimitiveTypesInfo(void)
     {
          ptc = hType::primitiveTypesCount();
          ptids = hType::primitiveTypesIndentifiers();
          ptn = hType::primitiveTypes();
          cc = hType::constraintsTypesCount();
          cids = hType::constraintsIndentifiers();
          cn = hType::constraintsNames();
     }
     // -----------------------------------------------------------------------------
     
     /// \brief Destructor hPrimitiveTypesInfo class.
     ~hPrimitiveTypesInfo(void)
     {
          delete [] ptids;
          delete [] cids;
     }
     // -----------------------------------------------------------------------------
     
     /// \brief Function that returns the index of primitive type identifiers
     ///
     /// \param __pti - given primitive type identifier
     /// \return index of primitive type identifier, if the identifier is not correct returns -1
     int iopti(int __pti)
     {
          for(int i=0;i<ptc;++i)
               if(ptids[i] == __pti)
                    return i; 
          return -1;
     }
     
     /// \brief Function that returns the index of constraint identifiers
     ///
     /// \param __ci - given constraints identifier
     /// \return index of constraint identifier, if the identifier is not correct returns -1
     int ioci(int __ci)
     {
          for(int i=0;i<cc;++i)
               if(cids[i] == __ci)
                    return i; 
          return -1;
     }
};
// -----------------------------------------------------------------------------

extern hPrimitiveTypesInfo hpTypesInfo; ///< Object that contains the informations about primitive types
// -----------------------------------------------------------------------------
#endif
