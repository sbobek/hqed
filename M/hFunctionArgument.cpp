/*
 *	   $Id: hFunctionArgument.cpp,v 1.16.4.2 2010-12-19 23:36:35 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../namespaces/ns_hqed.h"

#include "XTT/XTT_Attribute.h"

#include "hSet.h"
#include "hType.h"
#include "hTypeList.h"
#include "hDomain.h"
#include "hFunction.h"
#include "hFunctionList.h"
#include "hMultipleValue.h"
#include "hFunctionArgument.h"
// -----------------------------------------------------------------------------

// #brief Constructor hFunction class.
hFunctionArgument::hFunctionArgument(void)
{
     _argType = 0;                          
     _argName = "";     
     _isEditable = true;
     _isVisible = true;
     _argValue = new hMultipleValue;                
     _compatibleTypes = new QList<hType*>;
     _argValueDeleteLater = true;
}
// -----------------------------------------------------------------------------

// #brief Destructor hFunctionArgument class.
hFunctionArgument::~hFunctionArgument(void)
{
     delete _compatibleTypes;
     if(_argValueDeleteLater)
          delete _argValue;
}
// -----------------------------------------------------------------------------

// #brief Function that copies the current object to the other
//
// #param hFunctionArgument* __destination - the pointer to the destination object
// #return the pointer to the object that is a copy of current object
hFunctionArgument* hFunctionArgument::copyTo(hFunctionArgument* __destination)
{
     hFunctionArgument* dest = __destination;
     if(dest == NULL)
          dest = new hFunctionArgument;
          
     dest->_argType = _argType;                         
     dest->_argName = _argName;                      
     _argValue->copyTo(dest->_argValue);                       
     dest->_argValueDeleteLater = true;
     dest->_isEditable = _isEditable;
     dest->_isVisible = _isVisible;
     
     dest->_compatibleTypes->clear();
     for(int i=0;i<_compatibleTypes->size();++i)
          dest->_compatibleTypes->append(_compatibleTypes->at(i));        
     
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the last error message
//
// #return the last error message as string
QString hFunctionArgument::lastError(void)
{
     return _lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the argument can be modified by the user in expression editor window
//
// #return returns true if the argument can be modified by the user in expression editor window
bool hFunctionArgument::isEditable(void)
{
     return _isEditable;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the argument should be visible in string representation
//
// #return returns true if the argument should be visible in string representation
bool hFunctionArgument::isVisible(void)
{    
     return _isVisible;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the type of the argument
//
// #return the type of the argument as integer
int hFunctionArgument::type(void)
{
     return _argType;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the argument
//
// #return the name of the argument as string
QString hFunctionArgument::name(void)
{
     return _argName;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of the argument
//
// #return the value of the argument that is represented as hSet object
hMultipleValue* hFunctionArgument::value(void)
{
     return _argValue;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the argument value multiplicity
//
// #return the argument value multiplicity
int hFunctionArgument::valueMultiplicity(void)
{
     if(((_argType & FUNCTION_TYPE__SINGLE_VALUED) != 0) && ((_argType & FUNCTION_TYPE__MULTI_VALUED) == 0))
          return FUNCTION_TYPE__SINGLE_VALUED;
          
     if((_argType & FUNCTION_TYPE__MULTI_VALUED) != 0)
          return FUNCTION_TYPE__MULTI_VALUED;

     return FUNCTION_RESULT_TYPE_NOT_DEFINED;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of compatible types
//
// #return the value of the list of compatible types
QList<hType*>* hFunctionArgument::compatibleTypes(void)
{
     return _compatibleTypes;
}
// -----------------------------------------------------------------------------

// #brief Function that changes the type of the argument
//
// #param int __new_type - new value that is the new type of the argument
// #return no value return
void hFunctionArgument::setType(int __new_type)
{
     _argType = __new_type;
     _argValue->setSingleRequirement((__new_type & FUNCTION_TYPE__MULTI_VALUED) == 0);
}
// -----------------------------------------------------------------------------

// #brief Function that changes the parent type of the value
//
// #param hType* __parent_type - pointer to the new parent type
// #param bool __checkConstraints - denotes if the value of the argument should be compatible with new parent type constraints
// #return true on succes, otherwise false
bool hFunctionArgument::setValueParentType(hType* __parent_type, bool __checkConstraints)
{
     if(__parent_type == NULL)
     {
          setLastError("Not defined value parent type.");
          return false;
     }
     
     // Checking if the given type is compatible
     int typeIndex = -1;
     for(int i=0;i<_compatibleTypes->size();++i)
          if(_compatibleTypes->at(i)->name() == __parent_type->name())
          {
               typeIndex = i;
               break;
          }
     if(typeIndex == -1)
     {
          setLastError("Incomaptible type " + __parent_type->name());
          return false;
     }
      
     // Checking if the current argument value is compatible with new parentType
     hType* currentType = _argValue->parentType();          // Saving the old argument type
     _argValue->setParentType(__parent_type);               // Setting up the new type to correct calculation of value in the next line
     if(__parent_type->isSetCorrect(_argValue->value().toSet(false), __checkConstraints) != H_TYPE_ERROR_NO_ERROR)
     {
          _argValue->setParentType(currentType);            // If value is incorrect with the type then old type is restored
          setLastError("Argument value: " + _argValue->toString() + " is not compatible with type " + __parent_type->name() + "\nType error message: " + __parent_type->lastError());
          return false;
     }

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that changes the name of the argument
//
// #param QString __new_name - string that is the new name of the argument
// #return no value return
void hFunctionArgument::setName(QString __new_name)
{
     _argName = __new_name;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param QString __last_error - 
// #return no value return
void hFunctionArgument::setLastError(QString __last_error)
{
     _lastError = __last_error;
}
// -----------------------------------------------------------------------------

// #brief Function that changes the value of the argument
//
// #param hSet* __new_value - pointer to the set object that is the new value of the argument
// #return no value return
void hFunctionArgument::setValue(hMultipleValue* __new_value, bool __copy_obj)
{
     if(_argValue != NULL)
     {
          delete _argValue;
          _argValue = NULL;
     }
          
     _argValueDeleteLater = __copy_obj;
     if(!__copy_obj)
          _argValue = __new_value;
     if(__copy_obj)
          _argValue = __new_value->copyTo();
}
// -----------------------------------------------------------------------------

// #brief Function that changes the value of the field _isEditable
//
// #param _e - new value of field _isEditable (see description)
// #return no value return
void hFunctionArgument::setEditable(bool _e)
{
     _isEditable = _e;
}
// -----------------------------------------------------------------------------

// #brief Function that changes the value of the field _isVisible
//
// #param _v - new value of field _isVisible (see description)
// #return no value return
void hFunctionArgument::setVisible(bool _v)
{
     _isVisible = _v;
}
// -----------------------------------------------------------------------------

// #brief Function that searches for types that are compatible with current argument type
//
// #param QList<hType*>* __src_types_list - pointer to the list of types
// #return no value return
void hFunctionArgument::findCompatibleTypes(QList<hType*>* __src_types_list)
{
     int new_type_index = -1;
     QString value_parent_type_name = "##NAN";
     
     if(_argValue->parentType() != NULL)
          value_parent_type_name = _argValue->parentType()->name();

     _compatibleTypes->clear();
     for(int i=0;i<__src_types_list->size();++i)
     {   
          int typeRepresentation = hFunction::typeFunctionRepresentation(__src_types_list->at(i));
          if((_argType & typeRepresentation) != 0)
          {         
               if(__src_types_list->at(i)->name() == value_parent_type_name)         // Checking for new addres of current argument type
                    new_type_index = _compatibleTypes->size();
                  
               _compatibleTypes->append(__src_types_list->at(i));
          }
     }

     if((new_type_index == -1) && (_compatibleTypes->size() > 0))
          _argValue->setParentType(_compatibleTypes->at(0));
      
     if(new_type_index > -1)
          _argValue->setParentType(_compatibleTypes->at(new_type_index));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the index of current type of argument
//
// #return index of current type of argument as integer. In case of error returns -1
int hFunctionArgument::currentTypeIndex(void)
{
     return _compatibleTypes->indexOf(_argValue->parentType());
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hFunctionArgument::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     res = _argValue->updateTypePointer(__old, __new) && res;
     for(int i=0;i<_compatibleTypes->size();++i)
     {
          if(_compatibleTypes->at(i) == __old)
               _compatibleTypes->replace(i, __new);
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hFunctionArgument::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
          
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = _argValue->eventMessage(__errmsgs, __event, attr) && res;
     
          _argType = hFunction::typeFunctionRepresentation(_argValue->parentType());
          int arg_mult = _argValue->singleRequirement()==true? FUNCTION_TYPE__SINGLE_VALUED : FUNCTION_TYPE__MULTI_VALUED;
          _argType = arg_mult | _argType;
          findCompatibleTypes(typeList);
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = _argValue->eventMessage(__errmsgs, __event, attr) && res;
     
          _argType = hFunction::typeFunctionRepresentation(_argValue->parentType());
          int arg_mult = _argValue->singleRequirement()==true? FUNCTION_TYPE__SINGLE_VALUED : FUNCTION_TYPE__MULTI_VALUED;
          _argType = arg_mult | _argType;
          findCompatibleTypes(typeList);
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hFunctionArgument::isAVRused(void)
{
     return _argValue->isAVRused();
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hFunctionArgument::isExpressionUsed(void)
{
     return _argValue->isExpressionUsed();
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function argument object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hFunctionArgument::functionArgumentVerification(QString& __error_message)
{
     return _argValue->multipleValueVerification(__error_message);
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hFunctionArgument::out_HML_2_0(int __mode)
{
     return _argValue->out_HML_2_0(__mode);
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void hFunctionArgument::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     _argValue->out_HML_2_0(_xmldoc, __mode);
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #param __cellExpression - denotes if this object represents the whole cell expression
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hFunctionArgument::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression)
{    
     int result = 0;
     
     int arg_mult = _argType & (FUNCTION_TYPE__SINGLE_VALUED | FUNCTION_TYPE__MULTI_VALUED);
     int arg_type = _argType & ((~FUNCTION_TYPE__SINGLE_VALUED) & (~FUNCTION_TYPE__MULTI_VALUED));
     if(__attr != NULL)
     {
          arg_mult = __attr->multiplicity()?FUNCTION_TYPE__MULTI_VALUED:FUNCTION_TYPE__SINGLE_VALUED;
          arg_type = hFunction::typeFunctionRepresentation(__attr->Type());
          
          hType* ptp = __attr->Type();
          if((__attr->Type() != NULL) && (!__cellExpression)/*&& ((__root.tagName().toLower() == "expr") || (__root.tagName().toLower() == "eval"))*/)
          {        
               int bt = __attr->Type()->baseType();
               if(bt != SYMBOLIC_BASED)
                    ptp = typeList->at(hpTypesInfo.iopti(bt));
          }
          _argValue->setParentType(ptp);
     }
     
     result += _argValue->in_HML_2_0(__root, __msg, __attr, __cellExpression);
     arg_mult = _argValue->singleRequirement()==true?FUNCTION_TYPE__SINGLE_VALUED:FUNCTION_TYPE__MULTI_VALUED|FUNCTION_TYPE__SINGLE_VALUED;
     
     _argType = arg_mult | arg_type;
     findCompatibleTypes(typeList);
     
     return result;
}
// -----------------------------------------------------------------------------
