 /**
 * \file	hSetItem.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008 
 * \version	1.15
 * \brief	This file contains class definition that represents the atomic item of set of values
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HSETITEM
#define HSETITEM

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#include "hValueResult.h"
// -----------------------------------------------------------------------------

#define SET_ITEM_TYPE_NOT_DEFINED  0
#define SET_ITEM_TYPE_SINGLE_VALUE 1
#define SET_ITEM_TYPE_RANGE        2

#define SET_ITEM_SUCCESS_VALUE 0
#define SET_ITEM_FALSE_VALUE 0
#define SET_ITEM_TRUE_VALUE  1
#define SET_ITEM_ANY_VALUE   2
#define SET_ITEM_ND_VALUE    3

#define SET_ITEM_ERROR_PARENT_SET_NOT_DEFINED   -1
#define SET_ITEM_ERROR_NOT_ACCESIBLE_TYPE       -2
#define SET_ITEM_ERROR_INCONSISTENT_INPUT_TYPE  -3
#define SET_ITEM_ERROR_NOT_DEFINED_ITEM_TYPE    -4
#define SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE   -5
#define SET_ITEM_ERROR_OTHER_OBJECT_ERROR       -6
#define SET_ITEM_ERROR_INCORRECT_NUMBER_FORMAT  -7
#define SET_ITEM_ERROR_INCOORECT_EXTREME_VALUES -8
#define SET_ITEM_ERROR_INCORRECT_NUMERIC_FORMAT -9
#define SET_ITEM_ERROR_INCORRECT_VALUE_TYPE     -10
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hValue;
class XTT_Attribute;
// -----------------------------------------------------------------------------
/**
* \class 	hSetItem
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that represents the atomic item of set of values
*/
class hSetItem
{
private:
     
     /// The type of the item:
     /// \li SET_ITEM_TYPE_NOT_DEFINED - the type of the item is not defined
     /// \li SET_ITEM_TYPE_SINGLE_VALUE - contains only one value
     /// \li SET_ITEM_TYPE_RANGE - is the single range if form <from, to> (for only discrete domain)
     int _type;
     
     hSet* _parentSet;              ///< The pointer to the set that this object belongs
     hValue* _value1;              ///< The value of the item for single value type or the "from" value for the range
     hValue* _value2;              ///< Important only in case of _type == SET_ITEM_TYPE_RANGE as the "to" value
     
     QString _lastError;           ///< The last error message
     
protected:

     /// \brief Function that sets the left value of the range
     ///
     /// \param __from - the left value of the range
     /// \param __copyObject - defines if the object should be created as new, or the pointer can be used
     /// \return No return value.
     void setFrom(hValue* __from, bool __copyObject = false);
     
     /// \brief Function that sets the right value of the range
     ///
     /// \param __to - the right value of the range
     /// \param __copyObject - defines if the object should be created as new, or the pointer can be used
     /// \return No return value.
     void setTo(hValue* __to, bool __copyObject = false);
     
public:
     
     /// \brief Constructor hSetItem class.
     hSetItem(void);
     
     /// \brief Constructor hSetItem class.
     ///
     /// \param __parent - the pointer to the parent set
     /// \param __type - defines the type of the item as range or single value
     /// \param __from - defines the first value of the range, or single value
     /// \param  __to - defines the second value of the range, in case of _type == SET_ITEM_TYPE_RANGE is unimportant
     /// \param __copyObject - defines if the object should be created as new, or the pointer can be used
     hSetItem(hSet* __parent, int __type, hValue* __from, hValue* __to, bool __copyObject = false);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param __destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     hSetItem* copyTo(hSetItem* __destinationItem = NULL);
     
     /// \brief Destructor hSetItem class.
     ~hSetItem(void);
     
     /// \brief Function that returns the value of the item
     ///
     /// \return The string representation of the value
     hValueResult value(void);
     
     /// \brief Function that returns the left value of the range
     ///
     /// \return The string representation of the left value of the range
     hValueResult from(void);
     
     /// \brief Function that returns the right value of the range
     ///
     /// \return The string representation of the right value of the range
     hValueResult to(void);
     
     /// \brief Function that returns the value of the item as string representation
     ///
     /// \return The string representation of the value
     QString strValue(void);
     
     /// \brief Function that returns the value of the item as drools5 representation
     ///
     /// \return The drools5 representation of the value
     QString drls5Value(void);

     /// \brief Function that returns the value of the item as PROLOG representation
     ///
     /// \param __option - options of the output
     /// \return The PROLOG representation of the value
     QString prlgValue(int __option = -1);
     
     /// \brief Function that returns the left value of the range as string representation
     ///
     /// \return The string representation of the left value of the range
     QString strFrom(void);

     /// \brief Function that returns the left value of the range as drools5 representation
     ///
     /// \return The drools5 representation of the left value of the range
     QString drls5From(void);
     
     /// \brief Function that returns the left value of the range as PROLOG representation
     ///
     /// \param __option - options of the output
     /// \return The PROLOG representation of the left value of the range
     QString prlgFrom(int __option = -1);
     
     /// \brief Function that returns the right value of the range as string representation
     ///
     /// \return The string representation of the right value of the range
     QString strTo(void);

     /// \brief Function that returns the right value of the range as drools5 representation
     ///
     /// \return The drools5 representation of the right value of the range
     QString drls5To(void);
     
     /// \brief Function that returns the right value of the range as PROLOG representation
     ///
     /// \param __option - options of the output
     /// \return The PROLOG representation of the right value of the range
     QString prlgTo(int __option = -1);

     /// \brief Function that tries to match the current value to format that is required for this type
     ///
     /// \param __allowTrunc - denotes if the digiths can be cut
     /// \return The result code
     int toCurrentNumericFormat(bool __allowTrunc = false);
     
     /// \brief Function that returns the string representation of the item
     ///
     /// \return The string representation of the item
     QString toString(void);

     /// \brief Function that returns the drools5 representation of the item
     ///
     /// \return The drools5 representation of the item
     QString toDrools5(void);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function that returns the string representation of the values of the item
     ///
     /// \return The string representation of the values of the item
     QString toVals(void);
     
     /// \brief Function that returns the string representation of the value. The function tries to find the values that has the symbolic representation
     ///
     /// \return The string representation of the value
     QString toSymbolicFormat(QString __value);

     /// \brief Function that returns the drools5 representation of the value. The function tries to find the values that has the symbolic representation
     ///
     /// \return The drools5 representation of the value
     QString toDrools5Format(QString __value);
     
     /// \brief Function that returns the PROLOG representation of the value. The function tries to find the values that has the symbolic representation
     ///
     /// \return The PROLOG representation of the value
     QString toPrlgFormat(QString __value);
     
     /// \brief Function that returns the pointer to the object that represents the left value of the range
     ///
     /// \return The pointer to the object that represents the left value of the range
     hValue* fromPtr(void){ return _value1; }
     
     /// \brief Function that returns the pointer to the object that represents the right value of the range
     ///
     /// \return The pointer to the object that represents the right value of the range
     hValue* toPtr(void){ return _value2; }
     
     /// \brief Function that returns the pointer to the object that represents the single value
     ///
     /// \return The pointer to the object that represents the single value
     hValue* valuePtr(void){ return _value1; }
     
     /// \brief Function that returns the pointer to the parent container
     ///
     /// \return The pointer to the parent container
     hSet* parent(void){ return _parentSet; }
     
     /// \brief Function that returns the identifier of the item type
     ///
     /// \return The identifier of the item type (see description of the "_type" field)
     int type(void){ return _type; }
     
     /// \brief Function that transforms the current object that contains a set of values to an object that contains a single value
     ///
     /// \return true on success otherwise false
     bool makeItSingle(void);
     
     /// \brief Function that sets the pointer to the parent container
     ///
     /// \param __ps - the parent set of this item
     /// \return No return value.
     void setParent(hSet* __ps);
     
     /// \brief Function that sets the pointer to the parent type of the parent set. The function updates all the expressions _requiredType fields
     ///
     /// \param __pt - poiter to the new parent set parent type
     /// \return No return value.
     void setParentType(hType* __pt);
     
     /// \brief Function that sets the new type of item
     ///
     /// \param __type - new type of item
     /// \return No return value.
     void setType(int __type);
     
     /// \brief Function that sets the range value
     ///
     /// \param __from - the left value
     /// \param __to - the right value
     /// \param __copyObject - defines if the object should be created as new, or the pointer can be used
     /// \return true if the given range is correct, otherwise false.
     bool setRange(hValue* __from, hValue* __to, bool __copyObject = false);
     
     /// \brief Function that sets value of the item (in case of single value type)
     ///
     /// \param __value - new value of the item
     /// \param __copyObject - defines if the object should be created as new, or the pointer can be used
     /// \return true if the given range is correct, otherwise false.
     bool setValue(hValue* __value, bool __copyObject = false);
     
     /// \brief Function that sets the last error message
	///
     /// \param __lastError - the content of the error message
	/// \return No return value.
     void setLastError(QString __lastError);
     
     /// \brief Function that sets the last error message
	///
     /// \param __errorId - the error ID (defined in #define section)
     /// \param __errorMsg - the additional error message
	/// \return The given error code.
     int setLastError(int __errorId, QString __errorMsg = "");
     
     /// \brief Function that returns the last error message
	///
	/// \return The last error message as string
     QString lastError(void){ return _lastError; }
     
     /// \brief Function that check if the current item includes given value
     ///
     /// \param __value - value as string
     /// \return 1 if this item include given value, if no 0 otherwise error code
     int contains(QString __value);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that check if the type of the item is correct. If not the functions tries to fix it
     ///
     /// \return SET_ITEM_SUCCESS_VALUE i all is ok otherwise error code
     int typeVerification(void);
     
     /// \brief Function that calculates the result of substraction
     ///
     /// \param __subtrahend - poiter to the substrahend
     /// \return list of items that are the result of substraction
     QList<hSetItem*>* sub(hSetItem* __subtrahend);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - default - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \param ... - the list of others params.
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0, ...);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - default - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \param ... - the list of others params.
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0, ...);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr);
     
     /// Declaradion of friendship
     friend class XTT_Attribute;
     friend class XTT_AttributeGroups;
};
// -----------------------------------------------------------------------------
#endif
