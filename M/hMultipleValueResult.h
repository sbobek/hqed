 /**
 * \file	hMultipleValueResult.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	23.10.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the result of the multiple value calculation
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HMULTIPLEVALUERESULTH
#define HMULTIPLEVALUERESULTH
// -----------------------------------------------------------------------------

class hSet;
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

/**
* \class 	hMultipleValueResult
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	16.10.2008 
* \brief 	Class definition that represents the result of the multiple value calculation
*/
class hMultipleValueResult
{
private:

     bool _succes;                 ///< Describes if the calculation finished successfully
     hSet* _result;                ///< Contains the result of the calculation
     QString _error;               ///< Contains the error message
     
public:

     /// \brief Constructor hMultipleValueResult class.
     hMultipleValueResult(void);
     
     /// \brief Constructor hMultipleValueResult class.
     ///
     /// \param __result - the result as string
     hMultipleValueResult(hSet* __result);
     
     /// \brief Constructor hMultipleValueResult class.
     ///
     /// \param __msg - the message as string. When __succes is true then __msg is stored in _result if false in _error
     hMultipleValueResult(QString __error_msg);
     
     /// \brief Constructor hMultipleValueResult class.
     ///
     /// \param __succes - denotes if the result of calculation finished successfully
     hMultipleValueResult(bool __succes);
     
     /// \brief Destructor hMultipleValueResult class.
     ~hMultipleValueResult(void);
     
     /// \brief Function that returns the value of _succes field
     ///
     /// \return the value of _succes field
     bool succes(void) { return _succes; }
     
     /// \brief Function that returns the value of _result field
     ///
     /// \return the value of _result field
     hSet* result(void) { return _result; }
     
     /// \brief Function that returns the value of _error field
     ///
     /// \return the value of _error field
     QString error(void) { return _error; }
     
     /// \brief Overloaded assignment operator
     ///
     /// \param __right - the reference to the right object that is a source of data
     /// \return the current object
     hMultipleValueResult& operator=(hMultipleValueResult __right);
     
     /// \brief To boolean conversion method
     ///
     /// \return the boolean representation of object
     operator bool(void);
     
     /// \brief To QString conversion method
     ///
     /// \return the QString representation of error.
     operator QString(void);
     
     /// \brief To hSet* conversion method
     ///
     /// \param __make_copy - denotes if the copy of the set must be created
     /// \return the hSet* representation of result.
     hSet* toSet(bool __make_copy);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__dest - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     hMultipleValueResult* copyTo(hMultipleValueResult* __dest = NULL);
};
// -----------------------------------------------------------------------------
#endif
