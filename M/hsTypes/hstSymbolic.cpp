/*
 *	   $Id: hstSymbolic.cpp,v 1.7.4.1 2010-05-21 08:04:21 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_hqed.h"

#include "hstSymbolic.h"
// -----------------------------------------------------------------------------

// #brief Constructor hstSymbolic class.
hstSymbolic::hstSymbolic(hTypeList* __typeList)
: hType(__typeList)
{
}
// -----------------------------------------------------------------------------

// #brief Constructor hstSymbolic class.
//
// #param int __class - identifer of the type class
// #param int __type - identifer of the primitive type
hstSymbolic::hstSymbolic(hTypeList* __typeList, int __class, int __type) 
: hType(__typeList, __class, __type)
{
}
// -----------------------------------------------------------------------------

// #brief Function that returns the type identifier
//
// #return The type identifier as string
QString hstSymbolic::id(void)
{
     return name();
}
// -----------------------------------------------------------------------------

// #brief Function that maps this type object to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this domain as string.
QString hstSymbolic::toProlog(int /*__option*/)
{
     return "";
}
// -----------------------------------------------------------------------------

// #brief Function saves all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* hstSymbolic::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void hstSymbolic::out_HML_2_0(QXmlStreamWriter* /*_xmldoc*/)
{
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hstSymbolic::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "type";
     
     if(__root.tagName() != elementName)
          return 0;
          
     // Reading type data
     QString tid = __root.attribute("id", "");
     if(tid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined identifier of the \'" + name() + "\' type.\nLine number: " + QString::number(__root.lineNumber()) + "\nColumn number: " + QString::number(__root.columnNumber()), 0, 2);
          result++;
          return result;
     }
     setID(tid);
          
     return result;
}
// -----------------------------------------------------------------------------
