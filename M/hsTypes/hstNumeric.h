 /**
 * \file	hstNumeric.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	01.05.2009 
 * \version	1.0
 * \brief	This file contains class definition that represents the special numeric type
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HHSTNUMERIC
#define HHSTNUMERIC
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif

#include "../hType.h"
// -----------------------------------------------------------------------------

/**
* \class 	hstNumeric
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	1.05.2009
* \brief 	Class definition that represents the numeric type.
*/
class hstNumeric : public hType
{
public:
     
     /// \brief Constructor hstNumeric class.
     hstNumeric(hTypeList* __typeList);
     
     /// \brief Constructor hstNumeric class.
     ///
     /// \param __typeList - pointer to the type list
     /// \param __class - identifer of the type class
     /// \param __type - identifer of the primitive type
     hstNumeric(hTypeList* __typeList, int __class, int __type);
     
     /// \brief Function that returns the type identifier
	///
	/// \return The type identifier as string
     QString id(void);

     /// \brief Function that maps this type object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     virtual QString toProlog(int __option = -1);
     
     /// \brief Function saves all data in hml 2.0 format.
	///
	/// \return List of all data in hml 2.0 format.
	virtual QStringList* out_HML_2_0(void);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
	/// \return No retur value.
	virtual void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	virtual int in_HML_2_0(QDomElement& __root, QString& __msg);
};
// -----------------------------------------------------------------------------
#endif
