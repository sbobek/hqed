/*
 *	   $Id: hstInteger.cpp,v 1.8 2010-01-08 19:47:49 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../namespaces/ns_hqed.h"

#include "../hValue.h"
#include "../hDomain.h"
#include "../hSetItem.h"
#include "../hTypeList.h"

#include "hstInteger.h"
// -----------------------------------------------------------------------------

// #brief Constructor hstInteger class.
hstInteger::hstInteger(hTypeList* __typeList)
: hType(__typeList)
{
}
// -----------------------------------------------------------------------------

// #brief Constructor hstInteger class.
//
// #param int __class - identifer of the type class
// #param int __type - identifer of the primitive type
hstInteger::hstInteger(hTypeList* __typeList, int __class, int __type) 
: hType(__typeList, __class, __type)
{
}
// -----------------------------------------------------------------------------

// #brief Destructor hstInteger class.
hstInteger::~hstInteger(void)
{
     if(_stDomain != NULL)
          delete _stDomain;
}
// -----------------------------------------------------------------------------

// #brief Function sets the object properties to default
//
// #param __typeList - pointer to the type list in which this type is created
// #return no values return
void hstInteger::initObject(hTypeList* __typeList)
{
     _stDomain = NULL;
     int index = hpTypesInfo.iopti(NUMERIC_BASED);
     if(index == -1)
          return;

     _stDomain = new hDomain(__typeList->at(index));
     _stDomain->add(new hSetItem(_stDomain, SET_ITEM_TYPE_RANGE, new hValue(QString::number(NUMERIC_INFIMUM)), new hValue(QString::number(NUMERIC_SUPREMUM))));
}
// -----------------------------------------------------------------------------

// #brief Function that maps this type object to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this domain as string.
QString hstInteger::toProlog(int /*__option*/)
{
     QString lresult = "";
     QString lbasic = "numeric";
     QString llength = "";
     QString lordered = "";
     QString ldesc = "";
     
     llength += ",\n       length: " + QString::number(QString::number(NUMERIC_SUPREMUM).length());
     llength += ",\n       scale: " + QString::number(0);
     
     if(!description().isEmpty())
          ldesc = ",\n       desc: " + hqed::mcwp(description());
     
     lresult +=    "xtype [name: " + hqed::mcwp(name());
     lresult += ",\n       base: " + lbasic;
     lresult += llength;
     lresult += ldesc;
     lresult += ",\n       domain: " + _stDomain->toProlog();
     lresult += "\n      ].";
     
     return lresult;
}
// -----------------------------------------------------------------------------

// #brief Function saves all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* hstInteger::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();
     
     QString baseTypeName = "numeric";
     
     QString init_line = "<type id=\"" + id() + "\" name=\"" + name() + "\" base=\"" + baseTypeName + "\"";
     init_line += " length=\"" + QString::number(QString::number(NUMERIC_SUPREMUM).length()) + "\" scale=\"0\"";
     init_line += ">";
     *res << init_line;
     
     if(!description().isEmpty())
          *res << "\t<desc>" + description() + "</desc>";
     
     QStringList* al = _stDomain->out_HML_2_0();
     for(int l=0;l<al->size();l++)
          *res << "\t" + al->at(l);
     delete al;
     
     *res << "</type>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void hstInteger::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     QString baseTypeName = "numeric";

     _xmldoc->writeStartElement("type");
          _xmldoc->writeAttribute("id", id());
          _xmldoc->writeAttribute("name", name());
          _xmldoc->writeAttribute("base", baseTypeName);
          _xmldoc->writeAttribute("length", QString::number(QString::number(NUMERIC_SUPREMUM).length()));
          _xmldoc->writeAttribute("scale", QString::number(0));
     
          if(!description().isEmpty())
               _xmldoc->writeTextElement("desc", description());
          _stDomain->out_HML_2_0(_xmldoc);
     
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hstInteger::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "type";
     
     if(__root.tagName() != elementName)
          return 0;
          
     // Reading type data
     QString tid = __root.attribute("id", "");
     if(tid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined identifier of the \'" + name() + "\' type.\nLine number: " + QString::number(__root.lineNumber()) + "\nColumn number: " + QString::number(__root.columnNumber()), 0, 2);
          result++;
          return result;
     }
     setID(tid);
          
     return result;
}
// -----------------------------------------------------------------------------
