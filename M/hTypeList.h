/**
 * \file	hTypeList.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008
 * \version	1.15
 * \brief	This file contains class definition that is a container of object that represents attribute types
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HTYPELISTH
#define HTYPELISTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define SPECIAL_TYPES_COUNT 2
//---------------------------------------------------------------------------

class hType;
// -----------------------------------------------------------------------------
/**
* \class 	hTypeList
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that is a container of object that represents attribute types
*/
class hTypeList : public QList<hType*>
{

private:

    hType* specialTypes[SPECIAL_TYPES_COUNT];    ///< Pointers to the special types

    /// \brief Function that renews the identifiers of the special types. It is necessary for compatybility with types that can be read from XML file
    ///
    /// \return No value return
    void renewSTids(void);

    /// \brief Function that resets the identifiers of the special types. It is necessary for compatybility with types that can be read from XML file
    ///
    /// \return No value return
    void resetSTids(void);

public:

    /// \brief Constructor hTypeList class.
    hTypeList(void);

    /// \brief Destructor hTypeList class.
    ~hTypeList(void);

    /// \brief Function that create the base types such as bool, iteger,...
    ///
    /// \return No value return
    void createBaseTypes(void);

    /// \brief Function that returns the index to the type that has given ID
    ///
    /// \param __id - ID of type
    /// \return returns the index to the type that has given ID as iteger if not found return -1
    int indexOfID(QString __id);

    /// \brief Function that returns the pointer to the type that has given ID
    ///
    /// \param __id - ID of type
    /// \return returns the pointer to the type that has given ID as iteger if not found return NULL
    hType* _indexOfID(QString __id);

    /// \brief Function that returns the index to the type that has given name
    ///
    /// \param __name - name of type
    /// \return returns the index to the type that has given name as iteger if not found return -1
    int indexOfName(QString __name);

    /// \brief Function that returns the pointer to the type that has given name
    ///
    /// \param __name - name of type
    /// \return returns the pointer to the type that has given name as iteger if not found return NULL
    hType* _indexOfName(QString __name);

    /// \brief Function that returns the ID that is not used yet
    ///
    /// \return returns the ID that is not used yet as string
    QString searchNewId(void);

    /// \brief Function that creates the new type
    ///
    /// \param __oldHekateType - the identifier of old HeKatE type
    /// \return pointer to the new type
    hType* createNewType(int __oldHekateType);

    /// \brief Function that creates the new type
    ///
    /// \param __class - the class of contraints for new type
    /// \param __type - the identifier of base type
    /// \return pointer to the new type
    hType* createNewType(int __class, int __type);

    /// \brief Function that adds new item to the list
    ///
    /// \param __new_item - poiter to the new item
    /// \return no value return.
    void add(hType* __new_item);

    /// \brief Function that removes the given type
    ///
    /// \param __index - the type index
    /// \return true on succes otherwise false
    bool remove(int __index);

    /// \brief Function that updates int all the types the pointer to the new type
    ///
    /// \param __old - the value of the old pointer
    /// \param __new - the value of the new pointer
    /// \return true on succes otherwise false
    bool updateTypePointer(hType* __old, hType* __new);

    /// \brief Function that returns if the types objects are correct
    ///
    /// \param __error_message - the reference to the error message(s)
    /// \return true if yes otherwise false
    bool verification(QString& __error_message);

    /// \brief Function that removes all the items from the set
    ///
    /// \return no value return.
    void flush(void);

    /// \brief Function that creates the list of type names that are in the list
    ///
    /// \param __primitives - denotes if the list should contains the names of prmimitive types
    /// \return The list of names
    QStringList names(bool __primitives);

    /// \brief Function that returns the pointer to the one of the special type VOID, ACTION
    ///
    /// \param __type - the type identifier
    /// \return The pointer to the apropriate special type.
    hType* specialType(int __type);

    /// \brief Function that maps the object to the prolog code.
    ///
    /// \param __option - options of the output
    /// \return The prolog code that represents this object as string.
    QString toProlog(int __option = -1);

    /// \brief Function gets all data in hml 2.0 format.
    ///
    /// \return List of all data in hml 2.0 format.
    QStringList* out_HML_2_0(void);

#if QT_VERSION >= 0x040300
    /// \brief Function saves all data in hml 2.0 format.
    ///
    /// \param _xmldoc Pointer to document.
    /// \return No retur value.
    void out_HML_2_0(QXmlStreamWriter* _xmldoc);
#endif

    /// \brief Function loads all data from hml 2.0 format.
    ///
    /// \param __root - reference to the parent element
    /// \param __msg - reference to the error messages
    /// \return the result code:
    /// \li 0 - on success
    /// \li 1 - on error
    int in_HML_2_0(QDomElement& __root, QString& __msg);

    /// \brief Function that creates uniqe type identifier
    ///
    /// \return uniqe type identifier as string
    QString createUniqeTypeIdentifier(void);

};
// -----------------------------------------------------------------------------

extern hTypeList* typeList;
// -----------------------------------------------------------------------------
#endif
