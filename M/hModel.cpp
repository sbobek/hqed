/*
 *	  $Id: hModel.cpp,v 1.9.8.2 2011-09-29 09:18:04 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../C/ARD/widgets/ardGraphicsScene.h"
#include "../C/XTT/widgets/MyGraphicsScene.h"
#include "../C/XTT/widgets/StateGraphicsScene.h"
#include "../C/Settings_ui.h"

#include "../V/ARD/gARDlevel.h"
#include "../V/ARD/gARDlevelList.h"

#include "ARD/ARD_Level.h"
#include "ARD/ARD_LevelList.h"
#include "ARD/ARD_AttributeList.h"
#include "ARD/ARD_PropertyList.h"
#include "ARD/ARD_DependencyList.h"

#include "hTypeList.h"
#include "hFunctionList.h"
#include "XTT/XTT_States.h"
#include "XTT/XTT_TableList.h"
#include "XTT/XTT_AttributeGroups.h"
#include "XTT/XTT_ConnectionsList.h"

#include "hModel.h"
// -----------------------------------------------------------------------------

// #brief Default construcotr of the class
hModel::hModel(void)
{
    _ardLevelList = new ARD_LevelList;
    _ardAttributeList = new ARD_AttributeList;
    _ardPropertyList = new ARD_PropertyList;
    _ardDependencyList = new ARD_DependencyList;
    _ardFileName = "New project";
    _isARDFile = false;

    _xttStates = new XTT_States;
    _xttOutputStates = new XTT_States;
    _xttAttributeGroups = new XTT_AttributeGroups;
    _xttTableList = new XTT_TableList;
    _xttConnectionsList = new XTT_ConnectionsList;
    _xttTypeList = new hTypeList;
    _xttTypeList->createBaseTypes();
    _xttFileName = "New project";
    _isXTTFile = false;

    _dockWidgetsVisibleStatus[0] = Settings->ardTPHdiagramDockWidget();
    _dockWidgetsVisibleStatus[5] = Settings->ardLocalizatorDockWidget();
    _dockWidgetsVisibleStatus[6] = Settings->tphLocalizatorDockWidget();

    _dockWidgetsVisibleStatus[1] = Settings->xttSearchDockWidget();
    _dockWidgetsVisibleStatus[2] = Settings->xttErrorDockWidget();
    _dockWidgetsVisibleStatus[3] = Settings->xttExecuteDockWidget();
    _dockWidgetsVisibleStatus[4] = Settings->xttLocalizatorDockWidget();
    _dockWidgetsVisibleStatus[7] = Settings->xttPluginEventListenerDockWidget();
    _dockWidgetsVisibleStatus[8] = Settings->xttStatesDockWidget();

    _appMode = XTT_MODE;
    _modelID = "unknown";

    // ARD
    _ard_modelScene = new ardGraphicsScene();
    _ard_modelScene->setItemIndexMethod(QGraphicsScene::NoIndex);
    _ard_modelScene->setSceneRect(-200, -200, 400, 400);
    _ardCurrentARDscene = _ard_modelScene;
    _ard_thpScene = new ardGraphicsScene();

    // Setting the displays for the object
    _ardLevelList->setDisplay(_ard_modelScene);
    _ardPropertyList->setDisplay(_ard_thpScene);

    // XTT
    _xtt_scene = new MyGraphicsScene();
    _xtt_scene->setItemIndexMethod(QGraphicsScene::NoIndex);
    _xtt_scene->setSceneRect(-200, -200, 400, 400);

    _xtt_state_scene = new StateGraphicsScene();
    _xtt_state_scene->setItemIndexMethod(QGraphicsScene::NoIndex);

    setChanged(false);

    _menuButton = new QAction(_ardFileName, NULL);
    _menuButton->setCheckable(true);
    connect(_menuButton, SIGNAL(triggered()), this, SLOT(onActivate()));
}
// -----------------------------------------------------------------------------

// #brief Destructor of the class
hModel::~hModel(void)
{
    delete _ardLevelList;
    delete _ardDependencyList;
    delete _ardPropertyList;
    delete _ardAttributeList;

    delete _xttStates;
    delete _xttOutputStates;
    delete _xttConnectionsList;
    delete _xttTableList;
    delete _xttAttributeGroups;
    delete _xttTypeList;

    delete _ard_modelScene;
    delete _ard_thpScene;
    delete _xtt_scene;
    delete _menuButton;
    delete _xtt_state_scene;
}
// -----------------------------------------------------------------------------

// #brief function that clears the model
//
// #return no values return
void hModel::flush(void)
{
    _ardLevelList->flush();
    _ardDependencyList->flush();
    _ardPropertyList->flush();
    _ardAttributeList->flush();

    _xttOutputStates->flush();
    _xttStates->flush();
    _xttConnectionsList->Flush();
    _xttTableList->Flush();
    _xttAttributeGroups->Flush();
    _xttTypeList->flush();
}
// -----------------------------------------------------------------------------

// #brief function that infroms all the necessary object about model
//
// #return no values return
void hModel::registerModel(void)
{
    if(!MainWin->menuWindow->actions().contains(_menuButton))
        MainWin->menuWindow->addAction(_menuButton);
}
// -----------------------------------------------------------------------------

// #brief Set the change status for XTT model
//
// #param __c - the status value
// #return no values return
void hModel::setXttChanged(bool __c)
{
    _xttStates->setChanged(__c);
    _xttTableList->setChanged(__c);
    _xttOutputStates->setChanged(__c);
    _xttConnectionsList->setChanged(__c);
    _xttAttributeGroups->setChanged(__c);
}
// -----------------------------------------------------------------------------

// #brief Set the change status for ARD model
//
// #param __c - the status value
// #return no values return
void hModel::setArdChanged(bool __c)
{
    _ardAttributeList->setChanged(__c);
    _ardPropertyList->setChanged(__c);
    _ardDependencyList->setChanged(__c);
}
// -----------------------------------------------------------------------------

// #brief Set the change status for whole model
//
// #param __c - the status value
// #return no values return
void hModel::setChanged(bool __c)
{
    setArdChanged(__c);
    setXttChanged(__c);
}
// -----------------------------------------------------------------------------

// #brief Sets the application mode
//
// #param __newMode - mode identifier ARD_MODE or XTT_MODE
// #return no values return
void hModel::setAppMode(int __newMode)
{
    if((__newMode != ARD_MODE) && (__newMode != XTT_MODE))
        return;
    _appMode = __newMode;
}
// -----------------------------------------------------------------------------

// #brief Sets the status of visibility of DockedWindows
//
// #param __index - window index
// #param __status - new status
// #return no values return
void hModel::setDockWidgetsVisibleStatus(int __index, bool __status)
{
    if((__index < 0) || (__index >= DOCKED_WIDGETS))
        return;
    _dockWidgetsVisibleStatus[__index] = __status;
}
// -----------------------------------------------------------------------------

// #brief Sets the name of ARD model file name
//
// #param __filename - new file name
// #return no values return
void hModel::setARDfileName(QString __filename)
{
    _ardFileName = __filename;
    _menuButton->setText(modelTitle());
}
// -----------------------------------------------------------------------------

// #brief Sets the name of XTT model file name
//
// #param __filename - new file name
// #return no values return
void hModel::setXTTfileName(QString __filename)
{
    _xttFileName = __filename;
    _menuButton->setText(modelTitle());
}
// -----------------------------------------------------------------------------

// #brief Sets isARDfile flag
//
// #param __v - new value of the flag
// #return no values return
void hModel::setARDisFile(bool __v)
{
    _isARDFile = __v;
}
// -----------------------------------------------------------------------------

// #brief Sets isXTTfile flag
//
// #param __v - new value of the flag
// #return no values return
void hModel::setXTTisFile(bool __v)
{
    _isXTTFile = __v;
}
// -----------------------------------------------------------------------------

// #brief Sets the model ID
//
// #param __newID - ID of the model
// #return no values return
void hModel::setModelID(QString __newID)
{
    _modelID = __newID;
}
// -----------------------------------------------------------------------------

// #brief return the status of change of XTT model
//
// #return the status of change of XTT model
bool hModel::xttChanged(void)
{
    return _xttAttributeGroups->Changed() || _xttConnectionsList->Changed() || _xttTableList->Changed() || States->changed();
}
// -----------------------------------------------------------------------------

// #brief return the status of change of ARD model
//
// #return the status of change of ARD model
bool hModel::ardChanged(void)
{
    return _ardAttributeList->changed() || _ardPropertyList->changed() || _ardDependencyList->changed();
}
// -----------------------------------------------------------------------------

// #brief return the status of change of whole model
//
// #return the status of change of whole model
bool hModel::changed(void)
{
    return ardChanged() || xttChanged();
}
// -----------------------------------------------------------------------------

// #brief returns sth visibility status of the docked window
//
// #param __index - index of the docked window
// #return true - if window is visible, otherwise false
bool hModel::dockWidgetsVisibleStatus(int __index)
{
    if((__index < 0) || (__index >= DOCKED_WIDGETS))
        return false;
    return _dockWidgetsVisibleStatus[__index];
}
// -----------------------------------------------------------------------------

// #brief returns the application mode
//
// #return ARD_MODE or XTT_MODE
int hModel::appMode(void)
{
    return _appMode;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD level list
//
// #return the pointer to the ARD level list
ARD_LevelList* hModel::ardLevelList(void)
{
    return _ardLevelList;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD dependency list
//
// #return the pointer to the ARD dependency list
ARD_DependencyList* hModel::ardDependencyList(void)
{
    return _ardDependencyList;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD attributes list
//
// #return the pointer to the ARD attributes list
ARD_AttributeList* hModel::ardAttributeList(void)
{
    return _ardAttributeList;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD properties list
//
// #return the pointer to the ARD properties list
ARD_PropertyList* hModel::ardPropertyList(void)
{
    return _ardPropertyList;
}
// -----------------------------------------------------------------------------

// #brief returns the file name of the ARD model
//
// #return file name of the ARD model
QString hModel::ardFileName(void)
{
    return _ardFileName;
}
// -----------------------------------------------------------------------------

// #brief returns if the filename is a file
//
// #return if the filename is a file
bool hModel::isARDFile(void)
{
    return _isARDFile;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD model scene
//
// #return the pointer to the ARD model scene
ardGraphicsScene* hModel::ard_modelScene(void)
{
    return _ard_modelScene;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the ARD tph scene
//
// #return the pointer to the ARD tph scene
ardGraphicsScene* hModel::ard_thpScene(void)
{
    return _ard_thpScene;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the current ARD scene
//
// #return the pointer to the current ARD scene
ardGraphicsScene* hModel::ardCurrentARDscene(void)
{
    return _ardCurrentARDscene;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of XTT states
//
// #return pointer to the list of XTT states
XTT_States* hModel::xttStates(void)
{
    return _xttStates;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of output XTT states
//
// #return pointer to the list of output XTT states
XTT_States* hModel::xttOutputStates(void)
{
    return _xttOutputStates;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of XTT attribute groups
//
// #return pointer to the list of XTT attribute groups
XTT_AttributeGroups* hModel::xttAttributeGroups(void)
{
    return _xttAttributeGroups;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of XTT tables
//
// #return pointer to the list of XTT tables
XTT_TableList* hModel::xttTableList(void)
{
    return _xttTableList;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of XTT connections
//
// #return pointer to the list of XTT connections
XTT_ConnectionsList* hModel::xttConnectionsList(void)
{
    return _xttConnectionsList;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the list of XTT types
//
// #return pointer to the list of XTT types
hTypeList* hModel::xttTypeList(void)
{
    return _xttTypeList;
}
// -----------------------------------------------------------------------------

// #brief returns the XTT file name
//
// #return the XTT file name
QString hModel::xttFileName(void)
{
    return _xttFileName;
}
// -----------------------------------------------------------------------------

// #brief returns if the xttFileName is a file
//
// #return if the xttFileName is a file
bool hModel::isXTTFile(void)
{
    return _isXTTFile;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the XTT scene
//
// #return pointer to the XTT scene
MyGraphicsScene* hModel::xtt_scene(void)
{
    return _xtt_scene;
}
// -----------------------------------------------------------------------------

// #brief returns the pointer to the state XTT scene
//
// #return pointer to the state XTT scene
StateGraphicsScene* hModel::xtt_state_scene(void)
{
    return _xtt_state_scene;
}
// -----------------------------------------------------------------------------

// #brief returns the title of the model
//
// #return the title of the model
QString hModel::modelTitle(void)
{
    QString res = _ardFileName;
    if(_isXTTFile)
        res = _xttFileName;

    return res;
}
// -----------------------------------------------------------------------------

// #brief returns the ID of the model
//
// #return the ID of the model
QString hModel::modelID(void)
{
    return _modelID;
}
// -----------------------------------------------------------------------------

// #brief function triggered when model becomes active
//
// #return no values return
void hModel::activate(void)
{    
    _menuButton->setChecked(true);
}
// -----------------------------------------------------------------------------

// #brief function triggered when model becomes inactive
//
// #return no values return
void hModel::deactivate(void)
{
    _menuButton->setChecked(false);
}
// -----------------------------------------------------------------------------

// #brief function triggered when active event occurs
//
// #return no values return
void hModel::onActivate(void)
{
    emit activte(this);
}
// -----------------------------------------------------------------------------
