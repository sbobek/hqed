 /**
 * \file     XTT_AttributeGroups.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      26.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that keeps groups of attributes.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_AttributeGroupsH
#define XTT_AttributeGroupsH
//---------------------------------------------------------------------------

#include <QTreeWidgetItem>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_AttributeList.h"
//---------------------------------------------------------------------------

#define XTT_MOVE_ATTRIBUTE_RESULT_OK 0
#define XTT_MOVE_ATTRIBUTE_RESULT_NAME_OR_ACRONYM_EXISTS 1
#define XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_ATTRIBUTE    2
#define XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_SOURCE_GROUP 3
#define XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_DESTINATION_GROUP 4
//---------------------------------------------------------------------------

class hType;
class XTT_State;
class XTT_Attribute;
//---------------------------------------------------------------------------

/**
* \class      XTT_AttributeGroups
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      26.04.2007 
* \brief      Class definition that keeps groups of attributes.
*/
class XTT_AttributeGroups
{
private:

     XTT_AttributeList** fGroups;     ///< Groups of attributes table.

     unsigned int fCount;     ///< Size of groups of attributes table

     unsigned int fGId;          ///< Next group id.

     unsigned int gAttrId;     ///< Next group of attributes id.

     bool fChanged;     ///< Determines whether object was changed(True) or not(False).

     /// \brief Function creates group of global variables.
     ///
     /// \return No return value.
     void CreateGlobalVariablesGroup(void);

     /// \brief Function removes all children for the group.
     ///
     /// \param _parent Pointer to parent of the group. 
     /// \param names Reference to the list of names.
     /// \return No return value.
     void RemoveChildrens(QString, QStringList&);

     /// \brief Function creates children for the group.
     ///
     /// \param itm Pointer to the tree.
     /// \param _gId Group id which children will be created.
     /// \return No return value.
     void AddChilds(QTreeWidgetItem* itm, QString _gId);

public:

     /// \brief Constructor XTT_AttributeGroups class.
     XTT_AttributeGroups(void);

     /// \brief Destructor XTT_AttributeGroups class.
     ~XTT_AttributeGroups(void);

     /// \brief Function reads data from file in XTTML 1.0 format.
     ///
     /// \param root Reference to the section of this group.
     /// \return True when data is ok, otherwise false.
     bool FromATTML_1_0(QDomElement& root);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
     void out_Drools_Workspace(QTextStream* _javafile);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);

     /// \brief Function gets size of groups of attributes table.
     ///
     /// \return Size of groups of attributes table.
     unsigned int Count(void){ return fCount; }

     /// \brief Function gets next group id.
     ///
     /// \return Next group id.
     QString GId(void);

     /// \brief Function gets next group attribute id.
     ///
     /// \return Next group attribute id.
     unsigned int AttrId(void){ return gAttrId; }

     /// \brief Function gets pointer to chosen group.
     ///
     /// \param _index Index of chosen group.
     /// \return Pointer to chosen group. If index is not correct function returns NULL.
     XTT_AttributeList* Group(int _index);

     /// \brief Function gets pointer to chosen group.
     ///
     /// \param _name Name of chosen group.
     /// \return Pointer to the list of chosen group. If name is not correct function returns NULL.
     XTT_AttributeList* Group(QString _name);

     /// \brief Function gets list of group names.
     ///
     /// \param _parent Name of group parent.
     /// \return List of chosen group. If index is not correct function returns NULL.
     QStringList GroupList(QString _parent);

     /// \brief Function determines whether any group of attributes table was changed.
     ///
     /// \return Decision whether any group of attributes table was changed.
     bool Changed(void);

     /// \brief Function adds new group to the group of attributes table.
     ///
     /// \return No return value.
     void Add(void);

     /// \brief Function deletes group with its children.
     ///
     /// \param _name Name of the group.
     /// \return True when succeeds, false when name is incorrect.
     bool Delete(QString _name);

     /// \brief Function sets parent for the group and other settings.
     ///
     /// \param _group Pointer to the group.
     /// \param _newParent Pointer to the new parent.
     /// \return No return value.
     void setGroupParent(XTT_AttributeList* _group, XTT_AttributeList* _newParent);

     /// \brief Function sets parent for the group and other settings.
     ///
     /// \param _group Pointer to the group.
     /// \param _newParentId Pointer to the new parent id.
     /// \return No return value.
     void setGroupParent(XTT_AttributeList* _group, QString _newParentId);

     /// \brief Function sets the group as root.
     ///
     /// \param _group Pointer to the group.
     /// \return No return value.
     void setGroupAsRoot(XTT_AttributeList* _group);
     
     /// \brief Function that returns the pointer to the attribute that has the given ID
     ///
     /// \param __attId - attribute id
     /// \return the pointer to the attribute that has the given ID
     XTT_Attribute* indexOfAttId(QString __attId);

     /// \brief Function that removes attribute with specified ID
     ///
     /// \param __attId - attribute id
     /// \return return the result - success or failure
     bool removeAtt(QString __attId);

     /// \brief Function gets index of the group.
     ///
     /// \param _name Name of the group.
     /// \return Index of the group.
     int IndexOfName(QString _name);

     /// \brief Function gets index of the group.
     ///
     /// \param _id Id of the group.
     /// \return Index of the group.
     int IndexOfID(QString _id);

     /// \brief Function gets pointer to the group.
     ///
     /// \param _name Name of the group.
     /// \return Pointer of the group. If _name is not correct function returns NULL.
     XTT_AttributeList* _IndexOfName(QString _name);

     /// \brief Function gets pointer to the group.
     ///
     /// \param _id Id of the group.
     /// \return Pointer of the group. If _name is not correct function returns NULL.
     XTT_AttributeList* _IndexOfID(QString _id);
     
     /// \brief Returns only those attrributes, with a given relation
     ///
     /// \param __relation - the value of the relation. If -1 all the types of relation are accepted
     /// \return the list of only those attrributes, with a given relation
     QList<XTT_Attribute*> classFilter(int __relation = -1);

     /// \brief Function gets decision whether group of attributes table is empty.
     ///
     /// \return True when group of attributes table is empty, otherwise false.
     bool IsEmpty(void);

     /// \brief Function creates and gets tree of group names.
     ///
     /// \return Tree of group names.
     QList<QTreeWidgetItem*> CreateTree(void);

     /// \brief Function creates and gets path for the group.
     ///
     /// \param _index Index of the group.
     /// \return Path for the group.
     QString CreatePath(int _index);

     /// \brief Function creates and gets path for the group.
     ///
     /// \param _name Name of the group.
     /// \return Path for the group.
     QString CreatePath(QString _name);

     /// \brief Function creates and gets path for the group.
     ///
     /// \param _ptr Pointer to the group attribute.
     /// \return Path for the group. If pointer is incorrect function returns "#NAN".
     QString CreatePath(XTT_Attribute* _ptr);

     /// \brief Function creates and gets path for the group.
     ///
     /// \param _id Group id.
     /// \return Path for the group.
     QString _CreatePath(QString _id);

     /// \brief Function refreshes level field after changing level of nesting.
     ///
     /// \param _start Pointer to the parent group from which level should be numbered.
     /// \param _new_level Level of nesting.
     /// \return No return value.
     void RefreshLevelField(XTT_AttributeList* _start, unsigned int _new_level);

     /// \brief Function checks whether path and attribute are correct.
     ///
     /// \param _path Path concluded with attribute.
     /// \return True when path is correct, otherwise false.
     bool ExistsAttribute(QString _path);

     /// \brief Function finds and gets pointer to the group of attribute.
     ///
     /// \param _aptr Pointer to the attribute.
     /// \return Pointer to the group of attribute.
     XTT_AttributeList* AttributeGroup(XTT_Attribute*);
     
     /// \brief Function finds and gets index of the group of attribute.
     ///
     /// \param __attid - id of the attribute.
     /// \return index of the group
     int AttributeGroup(QString __attid);
     
     /// \brief Function that moves the given attribute to the given group
     ///
     /// \param __attId - id of the attribute.
     /// \param __destGroupIndex - index of the group where the attribute schould be moved
     /// \return XTT_MOVE_ATTRIBUTE_RESULT_...
     int moveAttribute(QString __attId, int __destGroupIndex);

     /// \brief Function finds and gets pointer to the attribute.
     ///
     /// \param _path Path to the attribute.
     /// \return Pointer to the attribute.
     XTT_Attribute* Attribute(QString);
     
     /// \brief Function that returns pointer to the list that contains pointers to the all attributes
     ///
     /// \return pointer to the list that contains pointers to the all attributes
     QList<XTT_Attribute*>* Attributes(void);

     /// \brief Function finds and gets pointer to the attribute.
     ///
     /// \param _id Attribute id.
     /// \return Pointer to the attribute.
     XTT_Attribute* FindAttribute(QString _id);
     
     /// \brief Function that sets the state of the attributes (model)
     ///
     /// \param __mstate - pointer to the list of lists that contains attribute name and its values
     /// \param __notUpdatedState - state to be updated
     /// \return no values return
     void setModelState(QList<QStringList>* __mstate, XTT_State* __notUpdatedState = NULL);

     /// \brief Function creates temporary argument for temporary results of calculations.
     ///
     /// \param type Type of temporary argument.
     /// \return Index of created argument.
     int CreateNewTmpArgument(hType* type);
     
     /// \brief Function creates action argument.
     ///
     /// \return Pointer to the created attribute
     XTT_Attribute* CreateNewActionArgument(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);

     /// \brief Function gets list of unused attributes.
     ///
     /// \return List of unused attributes.
     QStringList UnusedAttributesList(void);

     /// \brief Function clears groups of attributes table.
     ///
     /// \return No return value.
     void Flush(void);

     /// Ustawia wartosc okreslajaca czy obiekty byly modyfikowane
     /// \brief Function sets whether groups of attributes table were changed.
     ///
     /// \param _c Decision whether groups of attributes table were changed(True) or not(False).
     /// \return No return value.
     void setChanged(bool _c);
     
     /// \brief creates unique acronym name
     ///
     /// \param _name - the name of attribute
     /// \param _prevG - pointer to the previous group
     /// \param _currG - pointer to the current group
     /// \param _attIndex - index of attribute in previous group
     /// \return acronym as string. The acronym has unique name
     QString createAcronym(QString _name, XTT_AttributeList* _prevG, XTT_AttributeList* _currG, int _attIndex);
     
     /// \brief creates unique acronym name
     ///
     /// \param _name - the name of attribute
     /// \param _prevG - index of previous group
     /// \param _currG - index of current group
     /// \param _attIndex - index of attribute in previous group
     /// \return acronym as string. The acronym has unique name
     QString createAcronym(QString _name, int _prevG, int _currG, int _attIndex);
     
     /// \brief Function that creates uniqe attribute identifier
	///
	/// \return uniqe attribute identifier as string
     QString createUniqeAttributeIdentifier(void);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function that returns if the attributes object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool attributesVerification(QString& __error_message);
};
//---------------------------------------------------------------------------

/// Deklaracja wskaznika do grupy atrybutow
extern XTT_AttributeGroups* AttributeGroups;
//---------------------------------------------------------------------------
#endif
