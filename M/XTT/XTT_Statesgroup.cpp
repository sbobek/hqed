/*
 *     $Id: XTT_Statesgroup.cpp,v 1.2.4.2 2011-06-06 15:33:33 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QDomDocument>

#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"
#include "../../V/XTT/GStateTable.h"
#include "../../V/XTT/GStateConnection.h"


#include "../hSet.h"
#include "../hType.h"
#include "../hMultipleValue.h"

#include "XTT_State.h"
#include "XTT_States.h"
#include "XTT_Attribute.h"

#include "XTT_Statesgroup.h"
// -----------------------------------------------------------------------------

// #brief Constructor of XTT_Statesgroup class.
//
// #param __parent - pointer to the parent object
// #param __isOutputGroup - indicates if this group is a output group 0-no, 1-yes
XTT_Statesgroup::XTT_Statesgroup(XTT_States* __parent, int __isOutputGroup)
{
    _parent = __parent;
    _isOutputGroup = __isOutputGroup;
    _stateTable = new GStateTable(xtt::stateScene(), this);
}
// -----------------------------------------------------------------------------

// #brief Destructor of XTT_Statesgroup class.
XTT_Statesgroup::~XTT_Statesgroup(void)
{
    delete _stateTable;

    if(_isOutputGroup == 0)
    {
        iterator i;
        for(i=begin();i!=end();++i)
            delete i.value();
    }

    QList<QString> ks = keys();
    for(int i=0;i<ks.size();++i)
        take(ks.at(i));
}
// -----------------------------------------------------------------------------

// #brief Function that copies all the values to the destination object
//
// #param __destination - pointer to the destination object
// #return pointer to the new object
XTT_Statesgroup* XTT_Statesgroup::copyTo(XTT_Statesgroup* __destination)
{
    XTT_Statesgroup* result = __destination;
    if(result == NULL)
        result = new XTT_Statesgroup(_parent);

    iterator i;
    for(i=begin();i!=end();i++)
        (*result)[i.key()] = i.value()->copyTo();

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the object has been changed
//
// #return true if the object has been changed, otherwise false
bool XTT_Statesgroup::changed(void)
{
    iterator i;
    for(i=begin();i!=end();++i)
        if(i.value()->changed())
            return true;

    return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the group of stages is a group of output stages
//
// #return if the group of stages is a group of output stages
bool XTT_Statesgroup::isOutputGroup(void)
{
    return _isOutputGroup == 1;
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the parent object
//
// #return pointer to the parent object
XTT_States* XTT_Statesgroup::parent(void)
{
    return _parent;
}
// -----------------------------------------------------------------------------

/// \brief Function that generates the sorted or unsorted list of states from this list.
///
/// \return list of all states from this list
QMap<QString, XTT_State*>* XTT_Statesgroup::states(void)
{
    QMap<QString, XTT_State*>* result = new QMap<QString, XTT_State*>;

    // creating map
    for(iterator i=begin();i!=end();i++)
        result->insert(i.key(), i.value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the status of changed paramteter
//
// #param __c - the new status of changed parameter
// #return no values return
void XTT_Statesgroup::setChanged(bool __c)
{
    iterator i;
    for(i=begin();i!=end();++i)
        i.value()->setChanged(__c);
}
// -----------------------------------------------------------------------------

// #brief Function that returns a key of the state that has the given id
//
// #param __id - of the state that we want to find
// #return key of the state that has the given id
QString XTT_Statesgroup::indexOfStateId(QString __id)
{
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;
    QString result = "";

    for(i=sts->begin();i!=sts->end();++i)
        if(i.value()->id() == __id)
            result = i.key();

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if a state that has the given id exists
//
// #param __id - of the state that we want to check
// #return true if the state exists, otherwise false
bool XTT_Statesgroup::containsId(QString __id)
{
    return !indexOfStateId(__id).isEmpty();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of ids of the all states in the group
//
// #return the list of ids of the all states in the group
QStringList XTT_Statesgroup::ids(void)
{
    QMap<QString, XTT_State*>* sts = states();
    QStringList result;
    QMap<QString, XTT_State*>::iterator i;

    for(i=sts->begin();i!=sts->end();++i)
        result.append(i.value()->id());

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of names of the all states in the group
//
// #return the list of names of the all states in the group
QStringList XTT_Statesgroup::names(void)
{
    QMap<QString, XTT_State*>* sts = states();
    QStringList result = QStringList(sts->keys());
    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the base name of the state group
//
// #return the base name of the state group as string
QString XTT_Statesgroup::baseName(void)
{
    if(_parent == NULL)
        return "";

    return _parent->key(this);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of atributes of the all elements in the stategroup
//
// #return list of atributes of the all elements in the state
QList<XTT_Attribute*>* XTT_Statesgroup::inputAttributes(void)
{
    QList<XTT_Attribute*>* result = new QList<XTT_Attribute*>;
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;

    for(i=sts->begin();i!=sts->end();++i)
    {
        QList<XTT_Attribute*>* attlist = i.value()->inputAttributes();
        for(int a=0;a<attlist->size();++a)
            if(!result->contains(attlist->at(a)))
                result->append(attlist->at(a));

        delete attlist;
    }

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of atributes of the all elements in the stategroup
//
// #return list of atributes of the all elements in the state
QList<XTT_Attribute*>* XTT_Statesgroup::outputAttributes(void)
{
    QList<XTT_Attribute*>* result = new QList<XTT_Attribute*>;
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;

    for(i=sts->begin();i!=sts->end();++i)
    {
        QList<XTT_Attribute*>* attlist = i.value()->outputAttributes();
        for(int a=0;a<attlist->size();++a)
            if(!result->contains(attlist->at(a)))
                result->append(attlist->at(a));

        delete attlist;
    }

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that resets the input value
//
// #param __attr - pointer to the attribute
// #param __newValue - a new value related with the given attribute
// #return no values return
void XTT_Statesgroup::resetInputValueTo(XTT_Attribute* __attr, hSet* __newValue)
{
    iterator i;
    for(i=begin();i!=end();++i)
        i.value()->resetInputValueTo(__attr, __newValue->copyTo());

    _stateTable->refreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function that removes from the all states an given input attribute
//
// #param __attr - pointer to the attribute
// #return no values return
void XTT_Statesgroup::removeInputAttribute(XTT_Attribute* __attr)
{
    iterator i;
    for(i=begin();i!=end();++i)
        i.value()->removeInputAttribute(__attr);

    _stateTable->refreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function that removes from the all states an given output attribute
//
// #param __attr - pointer to the attribute
// #return no values return
void XTT_Statesgroup::removeOutputAttribute(XTT_Attribute* __attr)
{
    iterator i;
    for(i=begin();i!=end();++i)
        i.value()->removeOutputAttribute(__attr);

    _stateTable->refreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function that removes the state with the given name postfix
//
// #param __namepostfix - namepostfix of the state that should be removed
// #return no values return
void XTT_Statesgroup::removeState(QString __namepostfix)
{
    QString fullename = baseName() + XTT_Statesgroup::partSeparator() + __namepostfix;
    if(!contains(__namepostfix))
        return;

    delete take(__namepostfix);
    XTT_States::removeOutputState(fullename);
    _stateTable->refreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function that removes the state with the given name postfix
//
// #param __offset - index of the state in this the container
// #return no values return
void XTT_Statesgroup::removeState(int __offset)
{
    if((__offset < 0) || (__offset >= size()))
        return;

    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i = sts->begin();
    removeState((i + __offset).key());
    delete sts;
}
// -----------------------------------------------------------------------------

// #brief Function that loads the state into xtt model with the given name postfix
//
// #param __namepostfix - namepostfix of the state that should be loaded
// #return true on success, false on any error
bool XTT_Statesgroup::loadState(QString __namepostfix)
{
    if(!contains(__namepostfix))
        return false;

    return (*this)[__namepostfix]->loadState();
}
// -----------------------------------------------------------------------------

// #brief Function that loads the state with the given name postfix
//
// #param __offset - index of the state in this the container
// #return true on success, false on any error
bool XTT_Statesgroup::loadState(int __offset)
{
    if((__offset < 0) || (__offset >= size()))
        return false;

    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i = sts->begin();
    return loadState((i + __offset).key());
    delete sts;
}
// -----------------------------------------------------------------------------

// #brief Function that generates the uniq label of the state
//
// #param __prefix - prefix of the label
// #return the uniq label of the state as string
QString XTT_Statesgroup::findUniqLabel(QString __prefix)
{
    QString basename = __prefix;
    int index = 1;
    QString candidate;

    do
    {
        candidate = basename + QString::number(index++);
    }
    while(contains(candidate));

    return candidate;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the default created state
//
// #return the pointer to the default created state
XTT_State* XTT_Statesgroup::createdefaultState(void)
{
    XTT_State* newstate = new XTT_State(this);
    newstate->setId(_parent->findUniqId());

    QList<XTT_Attribute*>* atts = inputAttributes();
    for(int i=0;i<atts->size();++i)
    {
        hMultipleValue* mltval = atts->at(i)->Type()->typeDefaultValue();
        (*(newstate->initialValues()))[atts->at(i)] = mltval->valueField()->copyTo();
        delete mltval;
    }
    QString newlabel = findUniqLabel();
    (*this)[newlabel] = newstate;

    _stateTable->refreshAll();
    XTT_States::addNewOutputState(newstate);

    delete atts;
    return newstate;
}
// -----------------------------------------------------------------------------

// #brief Function that crerates copy of the given state nad returns the pointer to the created state
//
// #param __srcstate - state that is a source of the data
// #return the pointer to the created copy state
XTT_State* XTT_Statesgroup::createCopyOfState(XTT_State* __srcstate)
{
    if(__srcstate == NULL)
        return NULL;

    XTT_State* newstate = __srcstate->copyTo();;
    newstate->setId(_parent->findUniqId());
    QString newlabel = findUniqLabel();
    (*this)[newlabel] = newstate;

    _stateTable->refreshAll();
    XTT_States::addNewOutputState(newstate);

    return newstate;
}
// -----------------------------------------------------------------------------

// #brief Function that crerates copy of the given state nad returns the pointer to the created state
//
// #param __offset - index of the state in this container
// #return the pointer to the created copy state
XTT_State* XTT_Statesgroup::createCopyOfState(int __offset)
{
    if((__offset < 0) || (__offset >= size()))
        return NULL;

    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i = sts->begin();
    XTT_State* result = createCopyOfState((i+__offset).value());

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the state at the given position
//
// #param __offset - the offset from the first element of the container
// #return the pointer to the state at the given position, if position is ncorrect it returns NULL
XTT_State* XTT_Statesgroup::stateAt(int __offset)
{
    if((__offset < 0) || (__offset >= size()))
        return NULL;

    QMap<QString, XTT_State*>* sts = states();
    XTT_State* result = (sts->begin()+__offset).value();

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the poisition of the given state
//
// #param __state - the pointer to the state
// #return the offset from the first element of the container
int XTT_Statesgroup::stateIndex(XTT_State* __state)
{
    int result = 0;

    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;
    for(i=sts->begin();i!=sts->end();i++)
    {
        if(i.value() == __state)
            break;
        result++;
    }

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the label of the state at the given position
//
// #param __offset - the offset from the first element of the container
// #return the label of the state at the given position
QString XTT_Statesgroup::labelAt(int __offset)
{
    if((__offset < 0) || (__offset >= size()))
        return "";

    QMap<QString, XTT_State*>* sts = states();
    QString result = (sts->begin()+__offset).key();

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the label of the state at the given position
//
// #param __offset - the offset from the first element of the container
// #param __newlabel - new label of the state at the given position
// #return no values return
void XTT_Statesgroup::setLabelAt(int __offset, QString __newlabel)
{
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i = sts->begin();
    if(!sts->keys().contains(__newlabel))
        (*this)[__newlabel] = take((i+__offset).key());

    delete sts;
}
// -----------------------------------------------------------------------------

// #brief Function that returns a shape of the graphical representation of the item in scene coordinates
//
// #return a shape of the graphical representation of the item in scene coordinates
QRectF XTT_Statesgroup::shape(void)
{
    return _stateTable->shape();
}
// -----------------------------------------------------------------------------

// #brief Function that returns a pointer to the object that is a graphical representation of the group
//
// #return a pointer to the object that is a graphical representation of the group
GStateTable* XTT_Statesgroup::uiTable(void)
{
    return _stateTable;
}
// -----------------------------------------------------------------------------

// #brief Function that selects the states group
//
// #brief __select - the status of selection
// #return no values return
void XTT_Statesgroup::selectStatesgroup(bool __select)
{
    _stateTable->setTableSelection(__select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the state
//
// #brief __label - label of the state
// #brief __select - the status of selection
// #return no values return
void XTT_Statesgroup::selectState(QString __label, bool __select)
{
    if(!contains(__label))
        return;

    selectState((*this)[__label], __select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the state
//
// #brief __state - pointer to the state to be selected
// #brief __select - the status of selection
// #return no values return
void XTT_Statesgroup::selectState(XTT_State* __state, bool __select)
{
    _stateTable->selectState(__state, __select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __state - pointer to the state
// #param  __attr - pointer to the attribute in the given state
// #param  __selected - indicates the state of selection
// #return No return value.
void XTT_Statesgroup::selectCell(XTT_State* __state, XTT_Attribute* __attr, bool __selected)
{
    if(__attr == NULL)
        return;
    selectCell(__state, __attr->Id(), __selected);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __state - pointer to the state
// #param  __attid - id of the attribute
// #param  __selected - indicates the state of selection
// #return No return value.
void XTT_Statesgroup::selectCell(XTT_State* __state, QString __attid, bool __selected)
{
    _stateTable->selectCell(__state, __attid, __selected);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __label - label of the state
// #param  __attid - id of the attribute
// #param  __selected - indicates the state of selection
// #return No return value.
void XTT_Statesgroup::selectCell(QString __label, QString __attid, bool __selected)
{
    if(contains(__label))
        selectCell((*this)[__label], __attid, __selected);
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_Statesgroup::updateTypePointer(hType* __old, hType* __new)
{
    bool res = true;

    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;
    for(i=sts->begin();i!=sts->end();i++)
        res = i.value()->updateTypePointer(__old, __new) && res;

    _stateTable->refreshAll();

    delete sts;
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Statesgroup::eventMessage(QString* __errmsgs, int __event, ...)
{
    bool res = true;
    va_list argsptr;

    if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        QMap<QString, XTT_State*>* sts = states();
        QMap<QString, XTT_State*>::iterator i;
        for(i=sts->begin();i!=sts->end();i++)
            res = i.value()->eventMessage(__errmsgs, __event, attr) && res;

        refreshUI();
        delete sts;
        return res;
    }

    if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        QMap<QString, XTT_State*>* sts = states();
        QMap<QString, XTT_State*>::iterator i;
        for(i=sts->begin();i!=sts->end();i++)
            res = i.value()->eventMessage(__errmsgs, __event, attr) && res;

        refreshUI();
        delete sts;
        return res;
    }

    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the position of the graphical representation of the object
//
// #return __pos - new coordinates of position
// #return no values return
void XTT_Statesgroup::setLocalization(QPointF __pos)
{
    _stateTable->setLocalization(__pos);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the HMR representation of the states
//
// #param __basestatename - the base name of the state
// #return the HMR representation of the states
QString XTT_Statesgroup::toProlog(QString __basestatename)
{
    QString result;
    QMap<QString, XTT_State*>::iterator i;
    QMap<QString, XTT_State*>* sts = states();

    for(i=sts->begin();i!=sts->end();++i)
        result += i.value()->toProlog(implodeName(QStringList() << __basestatename << i.key())) + "\n";

    delete sts;
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the separator that is used to separate state base name and postfix
//
// #return the separator that is used to separate state base name and postfix
QString XTT_Statesgroup::partSeparator(void)
{
    return "/";
}
// -----------------------------------------------------------------------------

// #brief Function that creates a full name of the state using given list of the parts
//
// #param __parts - list of the full name parts
// #return the full name as string
QString XTT_Statesgroup::implodeName(QStringList __parts)
{
    for(int i=__parts.size()-1;i>=0;--i)
        if(__parts.at(i).simplified().isEmpty())
            __parts.removeAt(i);

    return __parts.join(XTT_Statesgroup::partSeparator());
}
// -----------------------------------------------------------------------------

// #brief Function that splits the given state full name into separated parts
//
// #param __fullname - state full name as string
// #return the list of the parts
QStringList XTT_Statesgroup::explodeName(QString __fullname)
{
    // the name of the state can be divided in to two and only two parts: base name and postfix
    QString base, postifx;
    int lio = __fullname.lastIndexOf(XTT_Statesgroup::partSeparator());
    if((lio == -1) || (lio == 0))
        base = __fullname;
    if(lio > 0)
    {
        base = __fullname.left(lio);
        postifx = __fullname.mid(lio+1);
    }

    return QStringList() << base << postifx;
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes a representation of the state
//
// #return No values return
void XTT_Statesgroup::refreshUI(void)
{
    _stateTable->refreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes the localization of the elements
//
// #return No values return
void XTT_Statesgroup::localizationRefresh(void)
{
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i;
    for(i=sts->begin();i!=sts->end();i++)
    {
        i.value()->uiConnection()->setLocalizationParameters();
        i.value()->uiConnection()->RePaint();
    }
    delete sts;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the row object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_Statesgroup::statesgroupVerification(QString& __error_message)
{
    bool res = true;

    // Checking the size of the group
    if(size() == 0)
    {
        QString _Error = "The group \'" + baseName() + "\' does not contain any states.";
        __error_message += hqed::createErrorString(baseName(), "", _Error, 0, 6);
        res = res && false;
    }

    // searching for unallocated memoroy
    QMap<QString, XTT_State*>* sts = states();
    QMap<QString, XTT_State*>::iterator i = sts->begin();
    for(;i!=sts->end();i++)
        if(i.value() == NULL)
        {
            QString _Error = "Unallocated memory for state \'" + i.key() + "\' in \'" + baseName() + "\' state group.";
            __error_message += hqed::createErrorString(baseName(), "", _Error, 0, 6);
            res = res && false;
        }

    // searching for states with the same names
    QList<QString> ks = sts->keys();
    for(int i=0;i<ks.size();++i)
        if(count(ks.at(i)) > 1)
        {
            QString _Error = "There are exist two or more states with the same label \'" + ks.at(i) + "\' in \'" + baseName() + "\' state group.";
            __error_message += hqed::createErrorString(baseName() + partSeparator() + ks.at(i), "", _Error, 0, 7);
            res = res && false;
        }

    for(i=sts->begin();i!=sts->end();i++)
        res = i.value()->stateVerification(__error_message) && res;

    delete sts;
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __postfix - the name postfix of the given stategroup
// #param __id - the id of the given state
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_Statesgroup::in_HML_2_0(QDomElement& __root, QString __postfix, QString __id, QString& __msg)
{
    int result = 0;
    QDomElement DOMstate = __root;

    // Checking if the given state id and name does not exist
    if(containsId(__id))
    {
        __msg += "\n" + hqed::createErrorString("", "", "The state id \'" + __id + "\' already exists. The state will not be read." + hqed::createDOMlocalization(DOMstate), 0, 1);
        return 1;
    }
    if(contains(__postfix))
    {
        __msg += "\n" + hqed::createErrorString("", "", "The name of the state \'" + __id + "\' already exists. The state will not be read." + hqed::createDOMlocalization(DOMstate), 0, 1);
        return 1;
    }

    // State reading
    XTT_State* candidate = new XTT_State(this);
    result = candidate->in_HML_2_0(DOMstate, __msg);

    // Checking if the state has been read correctly
    if(result > 0)
    {
        __msg += "\n" + hqed::createErrorString("", "", "Error while reading state \'" + __id + "\' element. The state \'" + __id + "\' will not be read." + hqed::createDOMlocalization(DOMstate), 0, 1);
        return 1;
    }

    // When everything is ok
    // adding a new state to the conatiner
    (*this)[__postfix] = candidate;
    candidate->setId(__id);

    _stateTable->refreshAll();

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __baseName - the base name of the state
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Statesgroup::out_HML_2_0(QString __baseName, int __mode)
{
    QStringList* res = new QStringList;
    res->clear();

    iterator i;
    for(i=begin();i!=end();++i)
    {
        *res << "<state id=\"" + i.value()->id() + "\" name=\"" + implodeName(QStringList() << __baseName << i.key()) + "\">";

        QStringList* tmp = i.value()->out_HML_2_0(__mode);
        for(int j=0;j<tmp->size();++j)
            *res << "\t" + tmp->at(j);
        delete tmp;

        *res << "</state>";
    }

    return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __baseName - the base name of the state
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_Statesgroup::out_HML_2_0(QXmlStreamWriter* _xmldoc, QString __baseName, int __mode)
{
    iterator i;
    for(i=begin();i!=end();++i)
    {
        _xmldoc->writeStartElement("state");
        _xmldoc->writeAttribute("id", i.value()->id());
        _xmldoc->writeAttribute("name", implodeName(QStringList() << __baseName << i.key()));

        i.value()->out_HML_2_0(_xmldoc, __mode);

        _xmldoc->writeEndElement();
    }
}
#endif
// -----------------------------------------------------------------------------
