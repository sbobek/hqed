/*
 *        $Id: XTT_CallbackContainer.cpp,v 1.1.2.15 2011-09-29 09:18:04 kkr Exp $
 *
 *     Implementation by �ukasz Finster, Sebastian Witowski
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "XTT_CallbackContainer.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"

XTT_CallbackContainer *CallbackContainer;

// #brief Constructor XTT_CallbackContainer class.
XTT_CallbackContainer::XTT_CallbackContainer()
{
     clbcks = new QList<XTT_Callback>;
     QString libpatch;
     libpatch = Settings->configurationPath();
     libFile += libpatch;
     libFile += "/clblib.xml";
     idprefix = "clb_";
}
// -----------------------------------------------------------------------------

// #brief Destructor XTT_CallbackContainer
XTT_CallbackContainer::~XTT_CallbackContainer()
{
    delete clbcks;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the file name of the callback library
//
// #return the file name of the callback library
QString XTT_CallbackContainer::getLibFile(void)
{
     return libFile;
}
// -----------------------------------------------------------------------------

// #brief Function adds new Callback to container.
//
// #param XTT_Callback object
// #return No return value
void XTT_CallbackContainer::add(XTT_Callback clb)
{
     clbcks->append(clb);
}
// -----------------------------------------------------------------------------

// #brief Function returns number of callbacks in container
//
// #return Number of callbacks as int
int XTT_CallbackContainer::size()
{
     return clbcks->size();
}
// -----------------------------------------------------------------------------

// #brief Function returns address of callback in container with selected index
//
// #param index of callback in container.
// #return Address to callback object.
XTT_Callback* XTT_CallbackContainer::get(int i)
{
     return &(clbcks->operator [](i));
}
// -----------------------------------------------------------------------------

// #brief Function returns index of callback with selected id
//
// #param Id of callback.
// #return index of callback in container
int XTT_CallbackContainer::getByID(QString IDsearch)
{
     int size;
     size = clbcks->size();
     for(int i = 0; i < size; i++)
          if((clbcks->operator [](i)).getID() == IDsearch) return i;
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Function deletes all callback from container
//
// #return No return value
void XTT_CallbackContainer::clear()
{
     clbcks->clear();
}
// -----------------------------------------------------------------------------

// #brief Function checks if exist callback with Id
//
// #param Callback id as QString
// #return No return value
bool XTT_CallbackContainer::exist(QString eID)
{
     if( getByID(eID) == -1)
          return false;
     else
          return true;
}
// ----------------------------------------------------------------------------

// #brief Function deletes callback from container
//
// #param index of callback to delete
// #return No return value
void XTT_CallbackContainer::del(int IDNo)
{
     clbcks->removeAt(IDNo);
}
// -----------------------------------------------------------------------------

// #brief Function returns name of callback from definition
//
// #param Id of callback.
// #return name of callback from definition
QString XTT_CallbackContainer::getPrologName(int index)
{
    QRegExp reg("xcall ([A-Za-z_]*)");
    QString in = get(index)->getDef();
    if(reg.indexIn(in,0,QRegExp::CaretAtOffset) == -1)
        return "";
    QString out = get(index)->getDef().mid(reg.pos(1),reg.matchedLength()-reg.pos(1));
    return out;
}
// -----------------------------------------------------------------------------

// #brief Function checks if selected callback will be saved to library
//
// #param index of callback in container
// #return Information as bool
bool XTT_CallbackContainer::libsave(int i)
{
     return (clbcks->operator [](i)).getSave();
}
// -----------------------------------------------------------------------------

// #brief Function reads callback from library file
//
// #return No return value
int XTT_CallbackContainer::loadFormLibrary()
{
     QFile file(libFile);
     if (!file.open(QFile::ReadOnly | QFile::Text))
     {
         return -1;
     }

     QTextStream in(&file);
     QString temp = in.readAll();

     XTT_Callback *newclb = NULL;
     QXmlStreamReader xml;
     xml.addData(temp);

     while(!xml.atEnd() && !xml.hasError())
     {
          xml.readNext();
          if (xml.isStartElement())
          {
               QString name = xml.name().toString();
               if(name == "clb")
               {
                    newclb = new XTT_Callback;
               }
               if(name == "id")
               {
                    newclb->setID(xml.readElementText());
               }
               if(name == "name")
               {
                    newclb->setName(xml.readElementText());
               }
               if(name == "type")
               {
                     newclb->setType(xml.readElementText());
               }
               if(name == "att")
               {
                    newclb->setAtt(xml.readElementText());
               }
               if(name == "def")
               {
                    newclb->setDef(xml.readElementText());
                    newclb->setSave(true);
                    this->add(*newclb);
               }
          }

          else if (xml.hasError())
          {
               return -2;
          }

     }
     return 0;
}
// -----------------------------------------------------------------------------

// #brief Function generates next free id.
//
// #return New id as QString.
QString XTT_CallbackContainer::generateID()
{
     QString id = "clb_01";
     unsigned int no = 1;
     QString postfix;
     while(exist(id))
     {
          ++no;
          if(no<10)
          {
               postfix = "0";
               postfix += QString::number(no);
          }
          else
          {
               postfix = QString::number(no);
          }

          id = idprefix;
          id += postfix;
     }
     return id;
}
// -----------------------------------------------------------------------------

// #brief Function saves all callback to library in XML format.
//
// #param XmlStreaWriter object
// #return No return value
void XTT_CallbackContainer::allToXML(QXmlStreamWriter* xmlWriter)
{
     xmlWriter->setAutoFormatting(Settings->xmlAutoformating());
     xmlWriter->writeStartDocument();
     xmlWriter->writeStartElement("lib");

     for(int i=0; i<CallbackContainer->size(); i++)
     {
          if(CallbackContainer->libsave(i))
               get(i)->toXML(xmlWriter);
     }
     xmlWriter->writeEndElement();
     xmlWriter->writeEndDocument();

}
// -----------------------------------------------------------------------------

// #brief Function set the usage of callback in model
//
// #param Id of callback, usage as bool
// #return No return value
void XTT_CallbackContainer::setUsed(QString id, bool use)
{
     if(id != "" && id != " ")
     {
          int index = getByID(id);
          get(index)->setUsage(use);
     }
}
// -----------------------------------------------------------------------------

// #brief Functions returns all used callback in XML format
//
// #param Pointer to XMLStreamWriter object
// #return No return value.
void XTT_CallbackContainer::usedToXML(QXmlStreamWriter* xmlWriter)
{
     xmlWriter->writeStartElement("callbacks");
     for(int i=0; i<size(); i++)
     {
          if(get(i)->getUsage())
               get(i)->toXML(xmlWriter);
     }

     xmlWriter->writeEndElement();

     for(int i = 0; i<size(); i++)
          get(i)->setUsage(false);
}
// -----------------------------------------------------------------------------

// #brief Function reads callback from XML format.
//
// #param Address of QDomElement, Address to message string
// #return No return value
int XTT_CallbackContainer::in_XML(QDomElement &__root, QString &__msg)
{
     QString elementName = "callbacks";
     QDomElement DOMclbs = __root.firstChildElement(elementName);

     if(DOMclbs.isNull())
     {
         __msg += "\nNo callbacks to read.";
          return -2;
     }

     XTT_Callback *newclb;

     QDomElement DOMclb = DOMclbs.firstChildElement("clb");
     while(!DOMclb.isNull())
     {
          newclb = new XTT_Callback;

          QString cid = DOMclb.firstChildElement("id").text();
          newclb->setID(cid);

          QString cname = DOMclb.firstChildElement("name").text();
          newclb->setName(cname);

          QString ctype = DOMclb.firstChildElement("type").text();
          newclb->setType(ctype);

          QString catt = DOMclb.firstChildElement("att").text();
          newclb->setAtt(catt);

          QString cdef = DOMclb.firstChildElement("def").text();
          newclb->setDef(cdef);

          if(!exist(cid))
          {
               add(*newclb);
          }
          else
          {
               int index = getByID(cid);
               QString name = get(index)->getName();
               if(cname == name)
               {
                    del(index);
                    newclb->setSave(true);
                    add(*newclb);
               }
               else
               {
                    int index = getByID(cid);
                    get(index)->setID(generateID());
                    newclb->setSave(true);
                    add(*newclb);
               }
          }
          DOMclb = DOMclb.nextSiblingElement("clb");
     }

     QFile file(libFile);
     if (!file.open(QFile::WriteOnly | QFile::Text))
     {
         __msg += "\nCan not open the Library File.";
         return -1;
     }
     QXmlStreamWriter xmlWriter(&file);
     allToXML(&xmlWriter);
     file.close();
     return 0;
}
// -----------------------------------------------------------------------------

// #brief Function return Definitions of all used callback.
//
// #return Prolog code with callback definitions
QString XTT_CallbackContainer::toProlog()
{
     QString out;
     for(int i=0; i<size(); i++)
     {
          if(get(i)->getUsage())
          {
               out += get(i)->toProlog();
               out += "\n\n";
          }
     }
     for(int i = 0; i<size(); i++)
          get(i)->setUsage(false);
     return out;
}
// -----------------------------------------------------------------------------
