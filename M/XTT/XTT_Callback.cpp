/*
 *        $Id: XTT_Callback.cpp,v 1.1.2.3 2011-06-24 17:45:22 hqedclb Exp $
 *
 *     Implementation by �ukasz Finster, Sebastian Witowski
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "XTT_Callback.h"
// -----------------------------------------------------------------------------

// #brief Constructor XTT_Callback class.
XTT_Callback::XTT_Callback()
{
     ID = "NC";
     name = "New Callback";
     type = "";
     att = "";
     def = "";
     libsave = false;
     used = false;
}
// -----------------------------------------------------------------------------

// #brief Function sets ID of callback.
//
// #param Id of callback
// #return No return value
void XTT_Callback::setID(QString newID)
{
     ID = newID;
}
// -----------------------------------------------------------------------------

// #brief Function sets name of callback.
//
// #param Name of callback
// #return No return value
void XTT_Callback::setName(QString newName)
{
     name = newName;
}
// -----------------------------------------------------------------------------

// #brief Function sets type of callback.
//
// #param type of callback
// #return No return value
void XTT_Callback::setType(QString newType)
{
     type = newType;
}
// -----------------------------------------------------------------------------

// #brief Function sets attribute type of callback.
//
// #param attribute type of callback
// #return No return value
void XTT_Callback::setAtt(QString newAtt)
{
     att = newAtt;
}
// -----------------------------------------------------------------------------

// #brief Function sets definition of callback.
//
// #param deffinition of callback
// #return No return value
void XTT_Callback::setDef(QString newDef)
{
     def = newDef;
}
// -----------------------------------------------------------------------------

// #brief Function returns ID of callback.
//
// #return Return QString type value.
QString XTT_Callback::getID()
{
     return ID;
}
// -----------------------------------------------------------------------------

// #brief Function returns attribute type of callback.
//
// #return Return QString type value.
QString XTT_Callback::getAtt()
{
     return att;
}
// -----------------------------------------------------------------------------

// #brief Function returns definition of callback.
//
// #return Return QString type value.
QString XTT_Callback::getDef()
{
     return def;
}
// -----------------------------------------------------------------------------

// #brief Function returns name of callback.
//
// #return Return QString type value.
QString XTT_Callback::getName()
{
     return name;
}
// -----------------------------------------------------------------------------

// #brief Function returns type of callback.
//
// #return Return QString type value.
QString XTT_Callback::getType()
{
     return type;
}
// -----------------------------------------------------------------------------

// #brief Function set the information if callback will be saved in library.
//
// #return No return value.
void XTT_Callback::setSave(bool save)
{
     libsave = save;
}
// -----------------------------------------------------------------------------

// #brief Function returns information about callback, to show it in Attribute Editor Window.
//
// #return Information as QString.
QString XTT_Callback::callbackInfo(bool __html)
{
     QString result = "";

     QString new_line_tag = "\n";
     QString open_bold_tag = "";
     QString close_bold_tag = "";

     if(__html)
     {
           new_line_tag = "<br>";
           open_bold_tag = "<b>";
           close_bold_tag = "</b>";
     }
     result += open_bold_tag + "Callback name: " + close_bold_tag + getName() + new_line_tag;
     result += open_bold_tag + "Type: " + close_bold_tag + getType() + new_line_tag;
     result += open_bold_tag + "Attribute type: " + close_bold_tag + getAtt() + new_line_tag;
     result += open_bold_tag + "Definition: " + close_bold_tag + getDef().left(50) + "..." + new_line_tag;

     return result;
}
// -----------------------------------------------------------------------------

// #brief Function get information if callback is about to be saved in Library.
//
// #return Information as bool.
bool XTT_Callback::getSave()
{
     return libsave;
}
// -----------------------------------------------------------------------------

// #brief Function writes definition od callback is XML format.
//
// #param Pointer to XMLStreamWriter object
// #return No return value
void XTT_Callback::toXML(QXmlStreamWriter* xmlWriter)
{
     xmlWriter->writeStartElement("clb");
     xmlWriter->writeTextElement("id", getID());
     xmlWriter->writeTextElement("name", getName());
     xmlWriter->writeTextElement("type", getType());
     xmlWriter->writeTextElement("att", getAtt());
     xmlWriter->writeStartElement("def");
     xmlWriter->writeCDATA(getDef());
     xmlWriter->writeEndElement();
     xmlWriter->writeEndElement();
}
// -----------------------------------------------------------------------------

// #brief Function set information if callback is used in model.
//
// #return No return value
void XTT_Callback::setUsage(bool clbu)
{
     used = clbu;
}
// -----------------------------------------------------------------------------

// #brief Function returns the information if the callback is used in model.
//
// #return Information as bool
bool XTT_Callback::getUsage()
{
     return used;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the definition of callback written in prolog.
//
// #return The prolog code that represents this callback as string.
QString XTT_Callback::toProlog()
{
     return def;
}
// -----------------------------------------------------------------------------
