/*
 *        $Id: XTT_ConnectionsList.cpp,v 1.34.4.2 2010-12-16 21:45:34 llk Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <stdarg.h>

#include "../../V/XTT/GConnection.h"
#include "../../namespaces/ns_hqed.h"
#include "../../C/MainWin_ui.h"
#include "XTT_ConnectionsList.h"
#include "XTT_TableList.h"
#include "XTT_Table.h"
//---------------------------------------------------------------------------

XTT_ConnectionsList* ConnectionsList;
//---------------------------------------------------------------------------

// #brief Constructor XTT_ConnectionsList class.
XTT_ConnectionsList::XTT_ConnectionsList(void)
{
     fConnections = NULL;
     fCount = 0;
     fNextConID = 0;
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_ConnectionsList class.
XTT_ConnectionsList::~XTT_ConnectionsList(void)
{
     if(fCount > 0)
     {
          for(unsigned int i=0;i<fCount;i++)
               Delete(0);
     }
}
//---------------------------------------------------------------------------

// #brief Function sets status of modyfication for the connection.
//
// #param _b Status of modyfication for the connection.
// #return No return value.
void XTT_ConnectionsList::setChanged(bool _c)
{
     for(unsigned int i=0;i<fCount;i++)
          fConnections[i]->setChanged(_c);
               
     fChanged = _c;
}
//---------------------------------------------------------------------------

// #brief Function clears the table of pointers to the connections.
//
// #return No return value.
void XTT_ConnectionsList::Flush(void)
{
     while(fCount > 0)
          Delete(0);
     
     // Rejestracja zmian
     fChanged = true;

     fCount = 0;
     fNextConID = 0;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the connection.
//
// #param _index Table index.
// #return Pointer to the connection.
XTT_Connections* XTT_ConnectionsList::Connection(unsigned int _index)
{
     if((((int)_index) < 0) || (_index >= fCount))
          return NULL;

     return fConnections[_index];
}
//---------------------------------------------------------------------------

// #brief Function retrieves decision whether the connection was modyfied.
//
// #return True when the connection was modyfied, otherwise function returns false.
bool XTT_ConnectionsList::Changed(void)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fConnections[i]->Changed())
               return true;
               
     return fChanged;
}
//---------------------------------------------------------------------------

// #brief Function adds new connection.
//
// #return Pointer to the new connection.
XTT_Connections* XTT_ConnectionsList::Add(void)
{
     XTT_Connections** tmp = new XTT_Connections*[fCount+1];

     // Kopiowanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fConnections[i];

     // Usuniecie starej tablicy wskaznikow
     if(fCount)
          delete [] fConnections;

     // Zapisanie nowej lokalizacji
     fConnections = tmp;

     // Utworenie nowego elementu
     fConnections[fCount] = new XTT_Connections(++fNextConID);

     // Powiekszenie pojemnika
     fCount++;
     
     // Rejestracja zmian
     fChanged = true;
     
     // Return the result
     return fConnections[fCount-1];
}
//---------------------------------------------------------------------------

// #brief Function adds new connection.
//
// #param _newConn Pointer to the new connection.
// #return No return value.
void XTT_ConnectionsList::Add(XTT_Connections* _newConn)
{
     XTT_Connections** tmp = new XTT_Connections*[fCount+1];

     // Kopiowanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fConnections[i];

     // Usuniecie starej tablicy wskaznikow
     if(fCount)
          delete [] fConnections;

     // Zapisanie nowej lokalizacji
     fConnections = tmp;

     // Utworenie nowego elementu
     fConnections[fCount] = _newConn;

     // Powiekszenie pojemnika
     fCount++;
     
     // Rejestracja zmian
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function adds new connection to load.
//
// #param __DOMconn - reference to the connection section
// #param __srcTable - id of source table
// #param __srcRow - id of source row
// #param __msg - reference to the error messages
// #return 0-on succes otherwise 1
int XTT_ConnectionsList::Add(QDomElement& __DOMconn, QString __srcTable, QString __srcRow, QString& __msg)
{
     QString elementName = "link";
     
     QDomElement DOMlink = __DOMconn;
     if(DOMlink.tagName().toLower() != elementName.toLower())
     {
          __msg += "\n" + hqed::createErrorString("", "", "Incorrect tag name \'" + DOMlink.tagName() + "\'.\nExpected name is \'link\'" + hqed::createDOMlocalization(DOMlink), 0, 2);
          return 1;
     }
     
     if(__srcTable == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined identifier of link source table." + hqed::createDOMlocalization(DOMlink), 0, 2);
          return 1;
     }
          
     loadConnectionItem item;
     item.DOMlink = DOMlink;
     item.srcTable = __srcTable;
     item.srcRow = __srcRow;
     
     _loadedConns.append(item);
     
     return 0;
}
//---------------------------------------------------------------------------

// #brief Function removes the connection.
//
// #param _index Index of the table.
// #return True when index is OK, otherwise function returns false.
bool XTT_ConnectionsList::Delete(unsigned int _index)
{
     if((((int)_index) < 0) || (_index >= fCount))
          return false;

     XTT_Connections** tmp = new XTT_Connections*[fCount-1];
     XTT_Connections* removePointer;

     // Kopiowanie zawartosci
     int tmp_i = 0;
     for(unsigned int i=0;i<fCount;i++)
          if(i != _index)
               tmp[tmp_i++] = fConnections[i];

     // Zapisanie pozycji do skasowania
     removePointer = fConnections[_index];
     
     // Usuniecie starej tablicy wskaznikow
     delete [] fConnections;

     // Zapisanie nowej lokalizacji
     fConnections = tmp;

     // Pomniejszenie pojemnika
     fCount--;
     
     // Polaczenie usuwamy dopiero na koncu tak aby w tabeli go juz nie bylo
     delete removePointer;
     
     // Rejestracja zmian
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the connection.
//
// #param _id Table id.
// #return Index of the connection. When error occurs function returns -1.
int XTT_ConnectionsList::IndexOfID(QString _id)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fConnections[i]->ConnId() == _id)
               return i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the connection.
//
// #param _id Table id.
// #return Pointer to the connection.
XTT_Connections* XTT_ConnectionsList::_IndexOfID(QString _id)
{
     int index = IndexOfID(_id);
     if(index == -1)
          return NULL;

     return fConnections[index];
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go out of the table and the row.
//
// #param _tid Table id.
// #param _rid Row id.
// #return List of connections that go out from the row.
QList<int>* XTT_ConnectionsList::IndexOfOutConnections(QString _tid, QString _rid)
{
     QList<int>* res = new QList<int>;
     
     for(unsigned int i=0;i<fCount;i++)
          if((fConnections[i]->SrcTable() == _tid) && (fConnections[i]->SrcRow() == _rid))
               res->append(i);

     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go out of the table and the row.
//
// #param _tid Table id.
// #param _rid Row id.
// #return List of connections that go out of the table and the row.
QList<XTT_Connections*>* XTT_ConnectionsList::_IndexOfOutConnections(QString _tid, QString _rid)
{
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     QList<int>* idxs = IndexOfOutConnections(_tid, _rid);
     
     for(int i=0;i<idxs->size();++i)
          res->append(fConnections[idxs->at(i)]);

     delete idxs;
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves decicision whether connections that go out of the table and the row exist.
//
// #param _tid Table id.
// #param _rid Row id.
// #return True when the connections exist, otherwise function returns false.
bool XTT_ConnectionsList::ExistsOutConnections(QString _tid, QString _rid)
{
     QList<int>* list = IndexOfOutConnections(_tid, _rid);
     bool res = (list->size() != 0);
     delete list;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves decicision whether connections that go into the table and the row exist.
//
// #param _tid Table id.
// #param _rid Row id.
// #return True when the connections exist, otherwise function returns false.
bool XTT_ConnectionsList::ExistsInConnections(QString _tid, QString _rid)
{
     QList<QString>* list = IndexOfInConnections(_tid, _rid);
     bool res = (list->size() != 0);
     delete list;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go into the table and the row.
//
// #param _tid Table id.
// #param _rid Row id.
// #return List of connections that go into the table and the row.
QList<QString>* XTT_ConnectionsList::IndexOfInConnections(QString _tid, QString _rid)
{
     QList<QString>* res = new QList<QString>;

     for(unsigned int i=0;i<fCount;i++)
          if((fConnections[i]->DesTable() == _tid) && (fConnections[i]->DesRow() == _rid))
               res->append(fConnections[i]->ConnId());

     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go into the table and the row.
//
// #param _tid Table id.
// #param _rid Row id.
// #return List of connections that go into the table and the row.
QList<XTT_Connections*>* XTT_ConnectionsList::_IndexOfInConnections(QString _tid, QString _rid)
{
     QList<QString>* list = IndexOfInConnections(_tid, _rid);
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(int i=0;i<list->size();i++)
          res->append(_IndexOfID(list->at(i)));
          
     delete list;
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go into the table.
//
// #param _tid Table id.
// #return Pointer to the list of connections that go into the table.
QList<XTT_Connections*>* XTT_ConnectionsList::listOfInConnections(QString _tid)
{
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(unsigned int i=0;i<fCount;i++)
          if(fConnections[i]->DesTable() == _tid)
               res->append(fConnections[i]);
               
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go out of the table.
//
// #param _tid Table id.
// #return Pointer to the list of connections that go out of the table.
QList<XTT_Connections*>* XTT_ConnectionsList::listOfOutConnections(QString _tid)
{
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(unsigned int i=0;i<fCount;i++)
          if(fConnections[i]->SrcTable() == _tid)
               res->append(fConnections[i]);
               
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of tables from which connections
// go into the table.
//
// #param _tid Table id.
// #return Pointer to the list of connections that go into the table.
QList<QString>* XTT_ConnectionsList::listOfInConnections_Table(QString _tid)
{
     QList<QString>* res = new QList<QString>;

     for(unsigned int i=0;i<fCount;i++)
     {
          if(fConnections[i]->DesTable() == _tid)
          {
              if(res->indexOf(fConnections[i]->SrcTable()) == -1)
              {
                  res->append(fConnections[i]->SrcTable());
              }

          }
     }
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of connections that go out of the table.
//
// #param _tid Table id.
// #return Pointer to the list of connections that go out of the table.
QList<QString>* XTT_ConnectionsList::listOfOutConnections_Table(QString _tid)
{
     QList<QString>* res = new QList<QString>;

     for(unsigned int i=0;i<fCount;i++)
          if(fConnections[i]->SrcTable() == _tid)
          {
              if(res->indexOf(fConnections[i]->DesTable()) == -1)
              {
                  res->append(fConnections[i]->DesTable());
              }
          }

     return res;
}
//---------------------------------------------------------------------------

// #brief Search for connections with the appropriate parameters of input and output
//
// #param unsigned int stid - the sorurce table identifier
// #param unsigned int srid - the sorurce row identifier
// #param unsigned int dtid - the destination table identifier
// #param unsigned int drid - the destination row identifier
// #return It return a list of connections with the appropriate parameters of input and output
QList<XTT_Connections*>* XTT_ConnectionsList::listOfConnections(QString stid, QString srid, QString dtid, QString drid)
{
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(unsigned int i=0;i<fCount;++i)
          if((fConnections[i]->SrcTable() == stid)
            &&
            (fConnections[i]->SrcRow() == srid)
            &&
            (fConnections[i]->DesTable() == dtid)
            &&
            (fConnections[i]->DesRow() == drid))
               res->append(fConnections[i]);
               
     return res;
}
//---------------------------------------------------------------------------

// #brief Search for connections which satisfied the search criteria
//
// #param QString _phrase - sarched phrase
// #param bool _caseSensitivity - if true search is CaseSensitivity
// #param bool* _elements - set of search criteria
// #param int _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
// #return It return a list of string whcich contains informations about result of search
QStringList* XTT_ConnectionsList::findConnections(QString _phrase, bool _caseSensitivity, bool* _elements, int _phrasesPolicy)
{
     // Each string inresult has format:
     // ELEMENT;SUBELEMENT;PARAMSLIST

     bool si_labels = _elements[0];
     bool si_descriptions = _elements[1];
     bool si_srcTablesTitles = _elements[2];
     bool si_desTablesTitles = _elements[3];
     
     Qt::CaseSensitivity cs = Qt::CaseInsensitive;
     if(_caseSensitivity)
          cs = Qt::CaseSensitive;
     QStringList phrases = _phrase.split(" ", QString::SkipEmptyParts);
     
     QStringList* res = new QStringList;
     
     for(unsigned int c=0;c<fCount;++c)
     {
          // Searching in labels
          if(si_labels)
               if(hqed::searchForPhrases(&phrases, fConnections[c]->Label(), cs, _phrasesPolicy))
                    res->append("XTT_CONNECTION;LABEL;" + fConnections[c]->ConnId());
                    
          // Searching in descriptions
          if(si_descriptions)
               if(hqed::searchForPhrases(&phrases, fConnections[c]->Description(), cs, _phrasesPolicy))
                    res->append("XTT_CONNECTION;DESCRIPTION;" + fConnections[c]->ConnId());
                    
          // Searching in source tables titles
          if(si_srcTablesTitles)
          {
               int tind = TableList->IndexOfID(fConnections[c]->SrcTable());
               if(tind > -1)
                    if(hqed::searchForPhrases(&phrases, TableList->Table(tind)->Title(), cs, _phrasesPolicy))
                         res->append("XTT_CONNECTION;SRC_TABLE;" + fConnections[c]->ConnId() + ";" + fConnections[c]->SrcTable());
          }
          
          // Searching in destination tables titles
          if(si_desTablesTitles)
          {
               int tind = TableList->IndexOfID(fConnections[c]->DesTable());
               if(tind > -1)
                    if(hqed::searchForPhrases(&phrases, TableList->Table(tind)->Title(), cs, _phrasesPolicy))
                         res->append("XTT_CONNECTION;DES_TABLE;" + fConnections[c]->ConnId() + ";" + fConnections[c]->DesTable());
          }
     }
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function removes all connections that go out of the row including the row.
//
// #param _rid Row id.
// #return No return value.
void XTT_ConnectionsList::RemoveOutConnectionsWithRowID(QString _rid)
{
     for(int i=fCount-1;i>=0;i--)
          if(fConnections[i]->SrcRow() == _rid)
               Delete(i);
}
//---------------------------------------------------------------------------

// #brief Function removes all connections that go into the row including the row.
//
// #param _rid Row id.
// #return No return value.
void XTT_ConnectionsList::RemoveInConnectionsWithRowID(QString _rid)
{
     for(int i=fCount-1;i>=0;i--)
          if(fConnections[i]->DesRow() == _rid)
               Delete(i);
}
//---------------------------------------------------------------------------

// #brief Function removes all connections connected to the row including the row.
//
// #param _rid Row id.
// #return No return value.
void XTT_ConnectionsList::RemoveConnectionsWithRowID(QString _rid)
{
     for(int i=fCount-1;i>=0;i--)
          if((fConnections[i]->SrcRow() == _rid) || (fConnections[i]->DesRow() == _rid))
               Delete(i);
}
//---------------------------------------------------------------------------

// #brief Function removes all connections connected to the table that has ID equal _tid.
//
// #param _tid - Table id.
// #return No return value.
void XTT_ConnectionsList::RemoveConnectionsWithTableID(QString _tid)
{
     for(int i=fCount-1;i>=0;i--)
          if((fConnections[i]->SrcTable() == _tid) || (fConnections[i]->DesTable() == _tid))
               Delete(i);
}
//---------------------------------------------------------------------------

// #brief Function repaints all connections connected to the table.
//
// #param _tid Table id.
// #return No return value.
void XTT_ConnectionsList::RepaintConnectionRelatedWithTable(QString _tid)
{
     for(unsigned int i=0;i<fCount;i++)
          if((fConnections[i]->SrcTable() == _tid) || (fConnections[i]->DesTable() == _tid))
          {
               fConnections[i]->Reposition();
               fConnections[i]->Repaint();
          }
}
//---------------------------------------------------------------------------

// #brief Function updates the localizations parameters for each connection. It is important to use this function after table deleting
//
// #return No return value.
void XTT_ConnectionsList::updateConnectionsParameters(void)
{
     for(unsigned int i=0;i<fCount;i++)
          fConnections[i]->Reposition();
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_ConnectionsList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int i=0;i<fCount;i++)
               res = fConnections[i]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int i=0;i<fCount;i++)
               res = fConnections[i]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 1.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returns false.
bool XTT_ConnectionsList::FromXTTML_2_0(QDomElement& root)
{
     if(root.tagName().toLower() != "xtt_list_connections")
          return false;

     bool res = true;
     
     // Wyczyszczenie zawartosci pojemnika
     Flush();
     
     QDomElement child = root.firstChildElement("xtt_connection");
     while(!child.isNull())
     {
          // Kiedy linia konczaca linie danych
          // Dodajemy nowy atrybut
          Add();

          // Wczytujemy dane z pliku
          res = res && fConnections[fCount-1]->FromXTTML_2_0(child);

          // Wczytywanie nastepnego polaczenia
          child = child.nextSiblingElement("xtt_connection");
     }
     
     // Analiza wszystkich poalczaen wszystkich tabel
     TableList->AnalyzeAllTablesConnections();

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function loads connections from the _loadedConns list
//
// #param __msg - reference to the error messages
// #return 0-on succes otherwise 1
int XTT_ConnectionsList::loadConnections(QString& __msg)
{
     int result = 0;
     while(!_loadedConns.empty())
     {
          result += in_HML_2_0(_loadedConns.takeFirst(), __msg);
     }
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the load connection item
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_ConnectionsList::in_HML_2_0(loadConnectionItem __root, QString& __msg)
{
     QDomElement DOMtabref = __root.DOMlink.firstChildElement("tabref");
     QDomElement DOMruleref = __root.DOMlink.firstChildElement("rulref");
     
     QString stid, dtid, srid, drid;
     
     if(DOMtabref.isNull())
     {
          __msg += "\n" + hqed::createErrorString("", "", "Required element \'tabref\' not found in \'link\' definition.\nThe link will not be read." + hqed::createDOMlocalization(DOMtabref), 0, 2);
          return 1;
     }
     
     stid = __root.srcTable;
     srid = __root.srcRow;
     if(!DOMtabref.isNull())
          dtid = DOMtabref.attribute("ref", "");
     if(!DOMruleref.isNull())
          drid = DOMruleref.attribute("ref", "");
     
     // Checking source ids
     XTT_Table* tab = TableList->_IndexOfID(stid);
     if(tab == NULL)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Link loading error: source table identifier \'" + stid + "\' does not exists.\nThe link will not be read." + hqed::createDOMlocalization(DOMtabref), 0, 2);
          return 1;
     }
     if(tab->indexOfRowID(srid) == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Link loading error: The table \'" + tab->Title() + "\' does not contain rule identifier \'" + srid + "\'.\nThe link will not be read." + hqed::createDOMlocalization(DOMruleref), 0, 2);
          return 1;
     }
     
     // Checking destination ids
     tab = TableList->_IndexOfID(dtid);
     if(tab == NULL)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Link loading error: destination table identifier \'" + dtid + "\' does not exists.\nThe link will not be read." + hqed::createDOMlocalization(DOMtabref), 0, 2);
          return 1;
     }
     if((drid != "") && (tab->indexOfRowID(drid) == -1))
     {
          __msg += "\n" + hqed::createErrorString("", "", "Link loading error: The table \'" + tab->Title() + "\' does not contain rule identifier \'" + drid + "\'.\nThe link will not be read." + hqed::createDOMlocalization(DOMruleref), 0, 2);
          return 1;
     }
     
     // Checking if the connection from the rule is already exists
     if(ExistsOutConnections(stid, srid))
     {
          __msg += "\n" + hqed::createErrorString("", "", "The connection from rule \'" + srid + "\' in table \'" + tab->Title() + "\' is already exist. The link will not be read." + hqed::createDOMlocalization(DOMruleref), 0, 2);
          return 1;
     }

     XTT_Connections* newConn = Add();
     newConn->setSrcTable(stid);
     newConn->setDesTable(dtid);
     newConn->setSrcRow(srid);
     newConn->setDesRow(drid);
     
     return 0;
}
// -----------------------------------------------------------------------------
