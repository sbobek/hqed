/**
 * \file     XTT_Table.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      24.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is XTT table.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_TABLEH
#define XTT_TABLEH
// -----------------------------------------------------------------------------

#include <QtCore/QPoint>
#include <QtCore/QString>
#include <QWidget>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_Row.h"
#include "XTT_Attribute.h"
// -----------------------------------------------------------------------------

class hType;
class GTable;
class XTT_Column;
// -----------------------------------------------------------------------------

/**
* \class      XTT_Table
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      24.04.2007 
* \brief      Class definition that is XTT table.
*/
// -----------------------------------------------------------------------------

class XTT_Table
{
private:

     QString fTableID;             ///< Table id.
     unsigned int* fNextRowId;     ///< Pointer to next row id.
     unsigned int fNextCellId;     ///< Next cell id.
     int fDfID;                    ///< Table ID in Drools Flow

     /// Type of the table.
     /// \li 0 - initial.
     /// \li 1 - middle.
     /// \li 2 - halt.
     unsigned int fTableType;

     QString fTitle;                         ///< Title of the table.
     QString fDescription;                   ///< Describtion of the table.
     unsigned int* fnContextSize;            ///< Pointer to the sizes of the contexts.
     unsigned int fRowCount;                 ///< Number of rows of the table.
     XTT_Row** fRows;                        ///< Table of the rows of the table.
     QList<XTT_Column*>* _columns;           ///< Pointer to the list that conatins properties of the columns

     unsigned int fNextAbstractID;           ///< Next abstract id.

     GTable* fGTable;                        ///< Pointer to the obect that draws the table.
     QPoint fPoint;                          ///< Coordinate of Canvas.

     unsigned int* fHeight;                  ///< Pointer to the table height.

     unsigned int fHeaderHeight;             ///< Header height.

     unsigned int fLevel;                    ///< Nesting level of the table.

     unsigned int fParent;                   ///< Parent of the table.

     bool fChanged;                          ///< Determines whether the table was modyfied.

public:
      
     /// \brief Constructor XTT_Table class.
     XTT_Table(void);

     /// \brief Constructor XTT_Table class.
     /// \param _tid Table id.
     /// \param _prid Pointer to the next table id.
     XTT_Table(unsigned int _tid, unsigned int* _prid);

     /// \brief Destructor XTT_Table class.
     ~XTT_Table(void);

     /// \brief Function reads data from file in XTTML 1.0 format.
     ///
     /// \param root Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returns false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function retrieves table id.
     ///
     /// \return Table id.
     QString TableId(void){ return fTableID; }

     /// \brief Function retrieves table id.
     ///
     /// \return Table id.
     int DfId(void){ return fDfID; }

     /// \brief Function sets table id for Drools Flow.
     ///
     /// \return Table id.
     void setDfId(int _id){ fDfID = _id; }
     
     /// \brief Function that returns the next id of a row
     ///
     /// \param __icrement - indicates if the value should be incremented after return
     /// \return the next id of a row
     unsigned int nextRowId(bool __icrement);
     
     /// \brief Function that returns the next id of a cell
     ///
     /// \param __icrement - indicates if the value should be incremented after return
     /// \return the next id of a cell
     unsigned int nextCellId(bool __icrement);
     
     /// \brief Function retrieves type of the table.
     ///
     /// \return Type of the table.
     unsigned int TableType(void){ return fTableType; }

     /// \brief Function retrieves title of the table.
     ///
     /// \return Title of the table.
     QString Title(void){ return fTitle; }

     /// \brief Function retrieves describtion of the table.
     ///
     /// \return Describtion of the table.
     QString Description(void){ return fDescription; }

     /// \brief Function retrieves size of the context.
     ///
     /// \param _cid Context id.
     /// \return Size of the context. When context id is incorrect, function returns -1.
     int ContextSize(unsigned int _cid);

     /// \brief Function retrieves pointer to the attribute.
     ///
     /// \param _index Index of the attribute.
     /// \return Pointer to the attribute. When context index is incorrect, function returns -1.
     XTT_Attribute* ContextAtribute(unsigned int _index);
     
     /// \brief Returns list of attributes in given context
     ///
     /// \param _ctx - context identifier
     /// \param __uniq - indicates if the list must contain onlu uniques pointers
     /// \return list of attributes in given context, if context identifier is incorrect returs empty list
     QList<XTT_Attribute*>* ContextAtributes(unsigned int _ctx, bool __uniq = false);

     /// \brief Function retrieves number of rows in the table.
     ///
     /// \return Number of rows in the table.
     unsigned int RowCount(void){ return fRowCount; }

     /// \brief Function retrieves pointer to the row of the table.
     ///
     /// \param _index Row index.
     /// \return Pointer to the row of the table.
     XTT_Row* Row(unsigned int _index);

     /// \brief Function retrieves coordinate of the table.
     ///
     /// \return Coordinate of the table.
     QPoint Localization(void){ return fPoint; }

     /// \brief Function retrieves width of the table
     ///
     /// \return Width of the table.
     unsigned int Width(void);

     /// \brief Function retrieves height of the table.
     ///
     /// \return Height of the table.
     unsigned int Height(void);

     /// \brief Function retrieves width of the column.
     ///
     /// \param _index Index of the column.
     /// \return Width of the column.
     unsigned int ColWidth(unsigned int _index);

     /// \brief Function retrieves height of the row of the table.
     ///
     /// \param _index Index of the row.
     /// \return Height of the row of the table.
     unsigned int RowHeight(unsigned int _index);

     /// \brief Function retrieves nesting level of the table.
     ///
     /// \return Nesting level of the table.
     unsigned int Level(void){ return fLevel; }

     /// \brief Function retrieves parent id of the table.
     ///
     /// \return Parent id of the table.
     unsigned int Parent(void){ return fParent; }

     /// \brief Function determines whether the table was modyfied.
     ///
     /// \return Decision whether the table was modyfied.
     bool Changed(void);

     /// \brief Function retrieves column id.
     ///
     /// \param _index Index of the column.
     /// \return Column id.
     int AbstractColID(unsigned int _index);

     /// \brief Function retrieves height of the header.
     ///
     /// \return Height of the header.
     unsigned int HeaderHeight(void){ return fHeaderHeight; }

     /// \brief Function retrieves pointer to the object that draws the table.
     ///
     /// \return Pointer to the object that draws the table.
     GTable* GraphicTable(void){ return fGTable; }

     bool cellRowColumn(XTT_Cell* _cell, unsigned int& _row, unsigned int& _column);

     /// \brief Function sets title of the table.
     ///
     /// \param _title Title of the table.
     /// \return No return value.
     void setTitle(QString _title);
     
     /// \brief Function sets ID of the table.
     ///
     /// \param __id - new table identifier
     /// \return No return value.
     void setID(QString __id);

     /// \brief Function sets describtion of the table.
     ///
     /// \param _desc Describtion of the table.
     /// \return No return value.
     void setDescription(QString _desc);

     /// \brief Function sets type of the table.
     ///
     /// \param _type Type of the table.
     /// \return No return value.
     void setTableType(unsigned int _type);

     /// \brief Function sets context of the attributes.
     ///
     /// \param _cindex Index of the column
     /// \param _pattr Pointer to the attribute.
     /// \return True when pointer to the attribute is correct, otherwise function returns false.
     bool setContextAtributes(unsigned int _cindex, XTT_Attribute* _pattr);
     
     /// \brief Function changes attribute of the cell.
     ///
     /// \param _context Index of the context.
     /// \param _cindex Index of the column
     /// \param _pattr Pointer to the attribute.
     /// \return True when pointer to the attribute is correct, otherwise function returns false.
     bool setContextAtributes(unsigned int _context, unsigned int _cindex, XTT_Attribute* _pattr);
     
     /// \brief Function sets coordinate of the table.
     ///
     /// \param _point Coordinate of the table.
     /// \return Always true.
     bool setLocalization(QPoint _point);

     /// \brief Function sets width of the column.
     ///
     /// \param _index Index of the column.
     /// \param _width Width of the column.
     /// \return Always true.
     bool setColWidth(unsigned int _index, unsigned int _width);

     /// \brief Function sets height of the row.
     ///
     /// \param _index Index of the row.
     /// \param _height Height of the table.
     /// \return Always true.
     bool setRowHeight(unsigned int _index, unsigned int _height);

     /// \brief Function sets level of nesting of the table.
     ///
     /// \param _level Level of nesting of the table.
     /// \return No return value.
     void setLevel(unsigned int _level);

     /// \brief Function sets parent id.
     ///
     /// \param _pid Parent id.
     /// \return No return value.
     void setParent(unsigned int _pid);

     /// \brief Function sets decision whether the table was modyfied.
     ///
     /// \param _b Decision whether the table was modyfied.
     /// \return No return value.
     void setChanged(bool _b);

     /// \brief Function sets size of the context.
     ///
     /// \param _ctx Index of the context.
     /// \param _size Size of the context.
     /// \return No return value.
     void setContextSize(unsigned int _ctx, unsigned int _size);

     /// \brief Function sets height of the header.
     ///
     /// \param _height Height of the header.
     /// \return No return value.
     void setHeaderHeight(unsigned int _height);
     
     /// \brief Function that returns pointer to the object that holds the column properties
     ///
     /// \param __index - index of the column
     /// \return pointer to the object that holds the column properties
     XTT_Column* column(int __index);

     XTT_Cell* cell(QString __id);
     XTT_Cell* cell(unsigned int __idx);

     /// \brief Function retrieves context of the column.
     ///
     /// \param _cindex Index of the column.
     /// \return Context of the column. When index is incorrect function returns -1.
     int ColContext(int _cindex);

     /// \brief Function retrieves container index of the column.
     ///
     /// \param _context Context of the column.
     /// \param _index Index of the column.
     /// \return Container index of hte column.
     int ColIndex(unsigned int, unsigned int);
     
     /// \brief Function that returns the sorting criterion of the given column
     ///
     /// \param __index - index of the column
     /// \return sorting criterion
     int ColSortCriterion(int __index);

     /// \brief Function retrieves number of column.
     ///
     /// \return Number of column.
     int ColCount(void);

     /// \brief Function retrieves changes two columns with each other.
     ///
     /// \param _index1 Index of the first column.
     /// \param _index2 Index of the second column.
     /// \return True when everything is ok, otherwise function returns false.
     bool Exchange(unsigned int _index1, unsigned int _index2);

     /// \brief Function that creates a new row for this table
     ///
     /// \return pointer to the created row
     XTT_Row* CreateRow(void);
     
     /// \brief Function adds new row to the table.
     ///
     /// \param __row - pointer to the row that should be added
     /// \return No return value.
     void AddRow(XTT_Row* __row);
     
     /// \brief Function adds new row to the table.
     ///
     /// \return pointer to the added row
     XTT_Row* AddRow(void);
     
     /// \brief Function removes row from the table and all connections connected to this row.
     ///
     /// \param _index Index of the row.
     /// \return True when everything is ok, otherwise function returns false.
     bool DeleteRow(unsigned int _index);

     /// \brief Function inserts new column to the table.
     ///
     /// \param _index Index of the column.
     /// \return True when index is correct, otherwise function returns false.
     bool InsertCol(unsigned int _index);

     /// \brief Function removes column from the table.
     ///
     /// \param _index Index of the column.
     /// \return True when index is correct, otherwise function returns false.
     bool DeleteCol(unsigned int _index);

     /// \brief Function inserts column of the new context.
     ///
     /// \param _ctx Index of the context.
     /// \return True when column was inserted. False when context of the column already exists.
     bool InsertNewContext(unsigned int _ctx);

     /// \brief Function moves column in other place.
     ///
     /// \param from Index of the source column.
     /// \param to Index fo the destiny column.
     /// \return True when everything is ok, otherwise function returns false.
     bool Move(unsigned int from, unsigned int to);

     /// \brief Function sets context for the column.
     ///
     /// \param _cidx Index of the column.
     /// \param _ctx Context.
     /// \return No return value.
     void setColContext(unsigned int _cidx, unsigned int _ctx);

     /// \brief Function retrieves last not empty context.
     ///
     /// \return Last not empty context.
     int LastNotEmptyContext(void);

     /// \brief Function retrieves index of column.
     ///
     /// \param _cid Column id.
     /// \return Index of the column. When id is incorrect function returns -1.
     int IndexOfColId(unsigned int _cid);
     
     /// \brief Function retrieves index of column that conatin the given cell.
     ///
     /// \param __ptr - pointer to the cell.
     /// \return Index of the column. When table doesn't contain given cell, function returns -1.
     int indexOfCol(XTT_Cell* __ptr);

     /// \brief Function retrieves index of the attribute in the table.
     ///
     /// \param id Atribute id.
     /// \return Index of the table. When id is incorrect function returns -1.
     int IndexOfAttributeID(QString id);
     
     /// \brief Function retrieves index of the attribute in given context.
     ///
     /// \param _aid Atribute id.
     /// \param __ctx - context
     /// \return Index of attribute in the table
     int IndexOfAttributeID(QString _aid, int __ctx);
     
     /// \brief Function that sorts the rules in the table using given column and criterions
     ///
     /// \param __column - index of the column
     /// \param __ascdesc - determines if the elements should be sorted ascending or descending
     /// \param __errormsg - reference to the string where error message will be stored
     /// \return true on suces, otherwise false
     bool sortRules(int __column, int __ascdesc, QString& __errormsg);

     /// \brief Function exchanges two rows.
     ///
     /// \param _index1 Index of the firs row.
     /// \param _index2 Index of the second row.
     /// \param _keepConnections Determines whether connections should be refreshed.
     /// \return True when indexes are ok, otherwise function returns false.
     bool ExchangeRows(unsigned int, unsigned int, bool _keepConnections = true);

     /// \brief Function inserts new row to the table.
     ///
     /// \param _ridx Index of the row of the table.
     /// \return True when index is ok, otherwise function returns false.
     bool insertRow(int _ridx);
     
     /// \brief Function inserts new row to the table.
     ///
     /// \param _ridx - Index of the row of the table.
     /// \param __row - pointer to the row that must be inserted
     /// \return True when index is ok, otherwise function returns false.
     bool insertRow(int _ridx, XTT_Row* __row);
     
     /// \brief Function that inserts the copy of given row to the given place
     ///
     /// \param _srcIndex - index of row that will be copied
     /// \param _desIndex - index of place where a new row will be inserted
     /// \return true when indexes are ok, otherwise function returns false.
     bool copyRow(int _srcIndex, int _desIndex);

     /// \brief Function retrieves index of the firs row that has any connections.
     ///
     /// \return Index of the firs row that has any connections.
     int firstConnectionRowIndex(void);

     /// \brief Function retrieves index of the row.
     ///
     /// \param _rid Row id.
     /// \return Index of the row.
     int indexOfRowID(QString _rid);
     
     /// \brief Function retrieves index of the row that contains the given cell.
     ///
     /// \param __ptr - pointer to the cell
     /// \return Index of the row.
     int indexOfRow(XTT_Cell* __ptr);
     
     /// \brief Function retrieves index of the row.
     ///
     /// \param __ptr - pointer to the row
     /// \return Index of the row, if row not exists returns -1.
     int indexOfRow(XTT_Row* __ptr);
     
     /// \brief Function that creates the list of rows pointers
     ///
     /// \return pointer to the list of rows pointers.
     QList<XTT_Row*>* rowList(void);

     /// \brief Function retrieves pointer to the list of input connections of the table.
     ///
     /// \return Pointer to the list of input connections of the table.
     QList<XTT_Connections*>* inConnectionsList(void);

     /// \brief Function retrieves pointer to the list of output connections of the table.
     ///
     /// \return Pointer to the list of output connections of the table.
     QList<XTT_Connections*>* outConnectionsList(void);

     /// \brief Function checks correctness of connections of the table.
     ///
     /// \param *__error_msgs - pointer to the string where the error messages will be stored
     /// \return true if ok, otherwise false
     bool connectionsDiagnostics(QString* __error_msgs = NULL);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that returns if the row object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool tableVerification(QString& __error_message);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that maps the rows to the prolog code.
     ///
     /// \return The prolog code that represents the rules as string.
     QString rulesToProlog(void);
     
     /// \brief Function that maps the table schema to the prolog code.
     ///
     /// \return The prolog code that represents the schema of the table as string.
     QString schemaToProlog(void);
     
     /// \brief Function gets all data in drools5 format.
     ///
     /// \param _csvdoc - writing device
     /// \return List of all data in drools5 format.
     void out_Drools_5_0_DecTab(QTextStream* _csvdoc);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
     /// \brief Function saves all data into Drools 5.0 format.
     /// \author Lukasz Lysik (llysik@gmail.com)
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No return value.
     void out_Drools_5_0(QXmlStreamWriter* _xmldoc);

	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     // ----------------------------------------------------------
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
     
};
// -----------------------------------------------------------------------------
#endif 
