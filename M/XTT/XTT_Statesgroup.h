/**
 * \file     XTT_Statesgroup.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      16.11.2009
 * \version     1.0
 * \brief     This file contains class definition that represents the XTT State Table.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef XTTSTATESGROUPH
#define XTTSTATESGROUPH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QHash>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

class hSet;
class hType;
class XTT_State;
class XTT_States;
class XTT_Attribute;
class GStateTable;
// -----------------------------------------------------------------------------
/**
* \class      XTT_Statesgroup
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       16.11.2009
* \brief      Class definition that represents the XTT State Table.
*/
class XTT_Statesgroup : public QHash<QString, XTT_State*>
{
private:

     XTT_States* _parent;               ///< Pointer to the parent object
     GStateTable* _stateTable;          ///< Pointer to the table that represents this group of states
     
     int _isOutputGroup;                ///< Indicates if this group is a output group 0-no, 1-yes

public:

     /// \brief Constructor of XTT_Statesgroup class.
     ///
     /// \param __parent - pointer to the parent object
     /// \param __isOutputGroup - indicates if this group is a output group 0-no, 1-yes
     XTT_Statesgroup(XTT_States* __parent, int __isOutputGroup = 0);
     
     /// \brief Destructor of XTT_Statesgroup class.
     ~XTT_Statesgroup(void);
     
     /// \brief Function that copies all the values to the destination object
	///
     /// \param __destination - pointer to the destination object
	/// \return pointer to the new object
     XTT_Statesgroup* copyTo(XTT_Statesgroup* __destination =  NULL);
     
     /// \brief Function that returns if the object has been changed
	///
	/// \return true if the object has been changed, otherwise false
     bool changed(void);
     
     /// \brief Function that returns if the group of stages is a group of output stages
     ///
     /// \return if the group of stages is a group of output stages
     bool isOutputGroup(void);
     
     /// \brief Function that returns pointer to the parent object
	///
	/// \return pointer to the parent object
     XTT_States* parent(void);

     /// \brief Function that generates the sorted or unsorted list of states from this list.
     ///
     /// \return list of all states from this list
     QMap<QString, XTT_State*>* states(void);
     
     /// \brief Function that sets the status of changed paramteter
	///
     /// \param __c - the new status of changed parameter
	/// \return no values return
     void setChanged(bool __c);
     
     /// \brief Function that returns a key of the state that has the given id
	///
     /// \param __id - of the state that we want to find
	/// \return key of the state that has the given id
     QString indexOfStateId(QString __id);
     
     /// \brief Function that returns if a state that has the given id exists
	///
     /// \param __id - of the state that we want to check
	/// \return true if the state exists, otherwise false
     bool containsId(QString __id);
     
     /// \brief Function that returns the list of ids of the all states in the group
	///
	/// \return the list of ids of the all states in the group
     QStringList ids(void);
     
     /// \brief Function that returns the list of names of the all states in the group
	///
	/// \return the list of names of the all states in the group
     QStringList names(void);
     
     /// \brief Function that returns the base name of the state group
	///
	/// \return the base name of the state group as string
     QString baseName(void);
     
     /// \brief Function that returns the pointer to the state at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the pointer to the state at the given position, if position is ncorrect it returns NULL
     XTT_State* stateAt(int __offset);
     
     /// \brief Function that returns the poisition of the given state
	///
	/// \param __state - the pointer to the state
     /// \return the offset from the first element of the container
     int stateIndex(XTT_State* __state);
     
     /// \brief Function that returns the pointer to the default created state
	///
	/// \return the pointer to the default created state
     XTT_State* createdefaultState(void);
     
     /// \brief Function that crerates copy of the given state nad returns the pointer to the created state
	///
     /// \param __srcstate - state that is a source of the data
	/// \return the pointer to the created copy state
     XTT_State* createCopyOfState(XTT_State* __srcstate);
     
     /// \brief Function that crerates copy of the given state nad returns the pointer to the created state
	///
     /// \param __offset - index of the state in this container
	/// \return the pointer to the created copy state
     XTT_State* createCopyOfState(int __offset);
     
     /// \brief Function that resets the input value
     ///
     /// \param __attr - pointer to the attribute
     /// \param __newValue - a new value related with the given attribute
     /// \return no values return
     void resetInputValueTo(XTT_Attribute* __attr, hSet* __newValue);
     
     /// \brief Function that removes from the all states an given input attribute
     ///
     /// \param __attr - pointer to the attribute
     /// \return no values return
     void removeInputAttribute(XTT_Attribute* __attr);
     
     /// \brief Function that removes from the all states an given output attribute
     ///
     /// \param __attr - pointer to the attribute
     /// \return no values return
     void removeOutputAttribute(XTT_Attribute* __attr);
     
     /// \brief Function that removes the state with the given name postfix
     ///
     /// \param __namepostfix - namepostfix of the state that should be removed
     /// \return no values return
     void removeState(QString __namepostfix);
     
     /// \brief Function that removes the state with the given name postfix
     ///
     /// \param __offset - index of the state in this the container
     /// \return no values return
     void removeState(int __offset);
     
     /// \brief Function that loads the state into xtt model with the given name postfix
     ///
     /// \param __namepostfix - namepostfix of the state that should be loaded
     /// \return true on success, false on any error
     bool loadState(QString __namepostfix);
     
     /// \brief Function that loads the state with the given name postfix
     ///
     /// \param __offset - index of the state in this the container
     /// \return true on success, false on any error
     bool loadState(int __offset);
     
     /// \brief Function that returns the label of the state at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the label of the state at the given position
     QString labelAt(int __offset);
     
     /// \brief Function that sets the label of the state at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \param __newlabel - new label of the state at the given position
	/// \return no values return
     void setLabelAt(int __offset, QString __newlabel);
     
     /// \brief Function that returns the list of atributes of the all elements in the stategroup
	///
	/// \return list of atributes of the all elements in the state
     QList<XTT_Attribute*>* inputAttributes(void);
     
     /// \brief Function that returns the list of atributes of the all elements in the stategroup
	///
	/// \return list of atributes of the all elements in the state
     QList<XTT_Attribute*>* outputAttributes(void);
     
     /// \brief Function that refreshes a representation of the state
     ///
     /// \return No values return
     void refreshUI(void);
     
     /// \brief Function that refreshes the localization of the elements
     ///
     /// \return No values return
     void localizationRefresh(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that generates the uniq label of the state
	///
     /// \param __prefix - prefix of the label
	/// \return the uniq label of the state as string
     QString findUniqLabel(QString __prefix = "");
     
     /// \brief Function that returns the HMR representation of the states
	///
     /// \param __basestatename - the base name of the state
	/// \return the HMR representation of the states
     QString toProlog(QString __basestatename);
     
     /// \brief Function that returns a shape of the graphical representation of the item in scene coordinates
	///
	/// \return a shape of the graphical representation of the item in scene coordinates
     QRectF shape(void);
     
     /// \brief Function that returns a pointer to the object that is a graphical representation of the group
	///
	/// \return a pointer to the object that is a graphical representation of the group
     GStateTable* uiTable(void);
     
     /// \brief Function that selects the states group
     ///
     /// \brief __select - the status of selection
     /// \return no values return
     void selectStatesgroup(bool __select);
     
     /// \brief Function that selects the state
     ///
     /// \brief __label - label of the state
     /// \brief __select - the status of selection
     /// \return no values return
     void selectState(QString __label, bool __select);
     
     /// \brief Function that selects the state
     ///
     /// \brief __state - pointer to the state to be selected
     /// \brief __select - the status of selection
     /// \return no values return
     void selectState(XTT_State* __state, bool __select);
     
     /// \brief Function that selects the cell in the state table
     ///
     /// \param  __state - pointer to the state 
     /// \param  __attr - pointer to the attribute in the given state
     /// \param  __selected - indicates the state of selection
     /// \return No return value.
     void selectCell(XTT_State* __state, XTT_Attribute* __attr, bool __selected);
     
     /// \brief Function that selects the cell in the state table
     ///
     /// \param  __state - pointer to the state 
     /// \param  __attid - id of the attribute
     /// \param  __selected - indicates the state of selection
     /// \return No return value.
     void selectCell(XTT_State* __state, QString __attid, bool __selected);
     
     /// \brief Function that selects the cell in the state table
     ///
     /// \param  __label - label of the state
     /// \param  __attid - id of the attribute
     /// \param  __selected - indicates the state of selection
     /// \return No return value.
     void selectCell(QString __label, QString __attid, bool __selected);
     
     /// \brief Function that sets the position of the graphical representation of the object
	///
	/// \return __pos - new coordinates of position
	/// \return no values return
     void setLocalization(QPointF __pos);
     
     /// \brief Function that returns if the row object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool statesgroupVerification(QString& __error_message);
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
	/// \param __postfix - the name postfix of the given stategroup
     /// \param __id - the id of the given state
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString __postfix, QString __id, QString& __msg);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __baseName - the base name of the state
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(QString __baseName, int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __baseName - the base name of the state
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, QString __baseName, int __mode = 0);
	#endif
     
     /// \brief Function that returns the separator that is used to separate state base name and postfix
	///
	/// \return the separator that is used to separate state base name and postfix
     static QString partSeparator(void);
     
     /// \brief Function that creates a full name of the state using given list of the parts
	///
	/// \param __parts - list of the full name parts
	/// \return the full name as string
     static QString implodeName(QStringList __parts);
     
     /// \brief Function that splits the given state full name into separated parts
	///
	/// \param __fullname - state full name as string
	/// \return the list of the parts
     static QStringList explodeName(QString __fullname);
};
// -----------------------------------------------------------------------------
#endif
