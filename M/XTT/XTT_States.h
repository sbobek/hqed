/**
 * \file     XTT_States.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      16.11.2009
 * \version     1.0
 * \brief     This file contains class definition that represents the container for XTT States.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef XTTSTATESH
#define XTTSTATESH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QHash>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

class hSet;
class hType;
class XTT_State;
class XTT_Attribute;
class XTT_Statesgroup;
// -----------------------------------------------------------------------------
/**
* \class      XTT_States
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       16.11.2009
* \brief      Class definition that represents the container for XTT States.
*/
class XTT_States : public QHash<QString, XTT_Statesgroup*>
{
public:

     /// \brief Constructor of XTT_States class.
     XTT_States(void);
     
     /// \brief Constructor of XTT_States class.
     ~XTT_States(void);
     
     /// \brief Function that removes all the object from the container
	///
     /// \return no values return
     void flush(void);
     
     /// \brief Function that returns if the object has been changed
	///
	/// \return true if the object has been changed, otherwise false
     bool changed(void);
     
     /// \brief Function that sets the status of changed paramteter
	///
     /// \param __c - the new status of changed parameter
	/// \return no values return
     void setChanged(bool __c);
     
     /// \brief Function that returns the list of ids of the all states
	///
	/// \return the list of ids of the all states
     QStringList ids(void);
     
     /// \brief Function that returns the list of names of the all states
	///
	/// \return the list of names of the all states
     QStringList names(void);
     
     /// \brief Function that returns the list of states
	///
	/// \return the list of states
     QList<XTT_State*>* states(void);
     
     /// \brief Function that generates the uniq id of the state
	///
     /// \param __prefix - prefix of the id
	/// \return the uniq id of the state as string
     QString findUniqId(QString __prefix = "");
     
     /// \brief Function that returns the HMR representation of the states
	///
	/// \return the HMR representation of the states
     QString toProlog(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that refreshes the UI of the states
	///
	/// \return no values return
     void refreshStatesUI(void);
     
     /// \brief Function that creates states compilation in the output states list
	///
	/// \return no values return
     void statesCompile(void);
     
     /// \brief Function that arrangess the element on the scene
	///
	/// \return no values return
     void groupsReposition(void);
     
     /// \brief Function that selects the states group
     ///
     /// \brief __fullname - full name of the state group
     /// \brief __select - the status of selection
     /// \return no values return
     void selectStatesgroup(QString __fullname, bool __select);
     
     /// \brief Function that selects the state
     ///
     /// \brief __fullname - full name of the state
     /// \brief __select - the status of selection
     /// \return no values return
     void selectState(QString __fullname, bool __select);
     
     /// \brief Function that selects the cell in the state table
     ///
     /// \param  __fullname - fullname of the stategroup/state
     /// \param  __attid - id of the attribute
     /// \param  __selected - indicates the state of selection
     /// \return No return value.
     void selectCell(QString __fullname, QString __attid, bool __selected);
     
     /// \brief Function that returns if the row object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool statesVerification(QString& __error_message);
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     /// \brief Function that adds new output state
     ///
     /// \param __state - pointer to the state
     /// \return no values return
     static void addNewOutputState(XTT_State* __state);

     /// \brief Function that removes the output state
     ///
     /// \param __state - pointer to the removed state
     /// \return no values return
     static void removeOutputState(XTT_State* __state);

     /// \brief Function that removes the output state
     ///
     /// \param __statename - the name of the state to remove
     /// \return no values return
     static void removeOutputState(QString __statename);
     
     /// \brief Function that returns the name of the output states group
     ///
     /// \return the name of the output states group
     static QString outputStatesGroupName(void);
     
     /// \brief Function that returns the pointer to the group of output states
     ///
     /// \return the pointer to the group of output states
     static XTT_Statesgroup* outputGroup(void);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
};
// -----------------------------------------------------------------------------

extern XTT_States* States;
extern XTT_States* OutputStates;
// -----------------------------------------------------------------------------
#endif
