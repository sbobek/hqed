/*
 *        $Id: XTT_Attribute.cpp,v 1.40.4.2.2.8 2011-07-17 15:13:41 hqedtypes Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"

#include "../hSet.h"
#include "../hType.h"
#include "../hValue.h"
#include "../hDomain.h"
#include "../hSetItem.h"
#include "../hTypeList.h"
#include "../hMultipleValue.h"

#include "XTT_CallbackContainer.h"
#include "XTT_AttributeGroups.h"
#include "XTT_Attribute.h"

#include "../../C/MainWin_ui.h"
// -----------------------------------------------------------------------------

QString XTT_Attribute::_not_definded_operator_ = "null";
QString XTT_Attribute::_any_operator_ = "any";
QString XTT_Attribute::_min_operator_ = "min";
QString XTT_Attribute::_max_operator_ = "max";
QString XTT_Attribute::_false_operator_ = "false";
QString XTT_Attribute::_true_operator_ = "true";
QStringList XTT_Attribute::_class_attribute_ = QStringList() << "simple" << "general";
// -----------------------------------------------------------------------------

// #brief Constructor XTT_Attribute class.
XTT_Attribute::XTT_Attribute(void)
{
     fAttrID = idPrefix();
     fAttrName = "";
     fAttrClbId = " ";
     fAttrAcronym = "";
     fAttrDesc = "";
     fAttrType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn[hpTypesInfo.iopti(INTEGER_BASED)]));
     fAttrValue = new hMultipleValue(NOT_DEFINED, fAttrType);
     fRelationsType = XTT_ATT_CLASS_RW;
     fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Constructor XTT_Attribute class.
//
// #param _id Identifier of attribute.
XTT_Attribute::XTT_Attribute(QString _id)
{
     fAttrID = _id;
     fAttrName = "";
     fAttrClbId = " ";
     fAttrAcronym = "";
     fAttrDesc = "";
     fRelationsType = XTT_ATT_CLASS_RW;
     fAttrType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn[hpTypesInfo.iopti(INTEGER_BASED)]));
     fAttrValue = new hMultipleValue(NOT_DEFINED, fAttrType);
     fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Destructor XTT_Attribute class.
XTT_Attribute::~XTT_Attribute(void)
{
     // Do nothing
}
// -----------------------------------------------------------------------------

// #brief Function returns path of the attribute
//
// #return path of the attribute
QString XTT_Attribute::Path(void)
{
     return AttributeGroups->CreatePath(this) + Name();
}
// -----------------------------------------------------------------------------

// #brief Function gets name of attribute.
//
// #return Name of attribute.
QString XTT_Attribute::Name(void)
{
     return fAttrName;
}
// -----------------------------------------------------------------------------

// #brief Function gets name of attribute with first capital letter
//
// #return Name of attribute.
QString XTT_Attribute::CapitalizedName(void)
{
     QString tmp = fAttrName;
     tmp[0] = fAttrName[0].toUpper();
          
     return tmp;
}
// -----------------------------------------------------------------------------

// #brief Function gets acronym of attribute.
//
// #return Acronym of attribute.
QString XTT_Attribute::Acronym(void)
{
     QString pre = "";
     QString post = "";
     if(multiplicity())
     {
          pre = "{";
          post = "}";
     }
          
     return pre + fAttrAcronym + post;
}
// -----------------------------------------------------------------------------

// #brief Function gets Callback Id of Attribute.
//
// #return Callback Id of Attribute.
QString XTT_Attribute::CallbackId()  
{
     /*QString pre = "";
     QString post = "";
     if(multiplicity())
     {
          pre = "{";
          post = "}";
     }

     return pre + fAttrClbId + post;*/
    return fAttrClbId;
}
// -----------------------------------------------------------------------------

// #brief Function returns the multiplicity of the value
//
// #return the multiplicity of the value - true if value can be multiple, if single false
bool XTT_Attribute::multiplicity(void)
{
     return !fAttrValue->singleRequirement();
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the attribute is a conceptual atribute
//
// #return true if attribute is conceptual otherwise false
bool XTT_Attribute::isConceptual(void)
{
     if(fAttrName.isEmpty())
          return true;
          
     return fAttrName.data()[0].isUpper();
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_Attribute::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     if(fAttrType == __old)
          fAttrType = __new;
          
     res = fAttrValue->updateTypePointer(__old, __new) && res;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Attribute::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = fAttrValue->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = fAttrValue->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the xtt attribute object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_Attribute::attributeVerification(QString& __error_message)
{
     bool res = true;
     
     if(fAttrType == NULL)
     {
          res = res && false;
          __error_message += hqed::createErrorString("", "", "The attribute " + fAttrName + " has not defined type", 0, 2);
     }
     
     if(fAttrValue == NULL)
     {
          res = res && false;
          __error_message += hqed::createErrorString("", "", "The attribute " + fAttrName + " has not defined value field", 0, 2);
     }
     
     // Checking the class of the attribute
     if((fRelationsType != XTT_ATT_CLASS_RO) &&
        (fRelationsType != XTT_ATT_CLASS_RW) &&
        (fRelationsType != XTT_ATT_CLASS_WO) &&
        (fRelationsType != XTT_ATT_CLASS_ST))
     {
          res = res && false;
          __error_message += hqed::createErrorString("", "", "The attribute " + fAttrName + " has incorrect class value: " + QString::number(fRelationsType), 0, 2);
     }
     
     // Checking attribute value
     hMultipleValueResult mvr = fAttrValue->value();
     if(!(bool)mvr)
     {
          __error_message += hqed::createErrorString("", "", "The attribute " + fAttrName + ": incorrect value: " + mvr.toSet(false)->toVals()->toString() + ".\nError message: " + (QString)mvr, 0, 2);
          res = res && false;
     }
     if(Type()->isSetCorrect(mvr.toSet(false), true) != H_TYPE_ERROR_NO_ERROR)
     {
          res = res && false;
          __error_message += hqed::createErrorString("", "", "The attribute " + fAttrName + ": incorrect value: " + mvr.toSet(false)->toVals()->toString() + ".\nError message: " + Type()->lastError(), 0, 2);
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function sets the id of attribute.
//
// #param _new_id - id of attribute.
// #return No return value.
void XTT_Attribute::setId(QString _new_id)
{
     fAttrID = _new_id;
}
// -----------------------------------------------------------------------------

// #brief Function sets name of attribute.
//
// #param _new_name Name of attribute.
// #return No return value.
void XTT_Attribute::setName(QString _new_name)
{
     fAttrName = _new_name; 
     setMultiplicity(multiplicity());        // Checking for name propriety
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets Callback id of attribute.
//
// #param _new_clbid Callback id of attribute.
// #return No return value.
void XTT_Attribute::setCallbackId(QString _new_clbid)    
{
     fAttrClbId = _new_clbid;
     //setMultiplicity(multiplicity());        // Checking for name propriety
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets name of acronym
//
// #param _new_acronym Name of acronym.
// #return No return value.
void XTT_Attribute::setAcronym(QString _new_arconym)
{ 
     fAttrAcronym = _new_arconym;
     setMultiplicity(multiplicity());        // Checking for name propriety
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets describtion of acronym.
//
// #param _desc Describtion of acronym.
// #return No return value.
void XTT_Attribute::setDescription(QString _desc)
{ 
     fAttrDesc = _desc;
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets type of attribute.
//
// #param _attr_type Type of attribute.
// #return No return value.
void XTT_Attribute::setType(hType* _attr_type)
{ 
     if(_attr_type == fAttrType)
          return;
     
     fAttrType = _attr_type; 
     FixValue();                    // Poprawa wartosci na zgodne z ograniczeniami
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets the value multiplicity of the attribute
//
// #param _mp if true the value is set as multiple, otherwise as single
// #return No return value.
void XTT_Attribute::setMultiplicity(bool _mp)
{
     // Checking for name PROPRIETY
     QString t[] = {fAttrName, fAttrAcronym};
     for(int i=0;i<2;++i)
     {
          // Removing the brackets from the string
          for(;t[i].contains("{");)
               t[i] = t[i].remove("{");
               
          for(;t[i].contains("}");)
               t[i] = t[i].remove("}");
     }
     
     fAttrName = t[0];
     fAttrAcronym = t[1];

     fAttrValue->setSingleRequirement(!_mp);
}
// -----------------------------------------------------------------------------

// #brief Function sets value that determines whether object was changed or not.
//
// #param _c Value that determines whether object was changed or not.
// #return No return value.
void XTT_Attribute::setChanged(bool _c)
{ 
     fChanged = _c; 
}
// -----------------------------------------------------------------------------

// #brief Function checks whether new attribute value can be set.
//
// #param _v Name of attribute.
// #return Decision whether new attribute value can be set(true) or not(false).
bool XTT_Attribute::checkValue(hMultipleValue* _v)
{
     hMultipleValueResult mvr = _v->value();
     if(!(bool)mvr)
          return false;

     return (Type()->isSetCorrect(mvr.toSet(false), true) == H_TYPE_ERROR_NO_ERROR);
}
// -----------------------------------------------------------------------------

// #brief Function fixes attribute value when it does not belong to the range.
//
// #return No return value.
void XTT_Attribute::FixValue(void)
{
     if(fAttrType == NULL)
          return;
          
     setValue(fAttrType->typeDefaultValue());
     fChanged = false;
}
// -----------------------------------------------------------------------------

// #brief Function sets value of attribute.
//
// #param _v - Value of attribute.
// #return True when operation succeeds else false.
bool XTT_Attribute::setValue(hMultipleValue* _v)
{
     bool mltp = multiplicity();   // Storing the multiplicity value

     if((fAttrValue != NULL) && (_v != fAttrValue))
          delete fAttrValue;

     // Gdy wszystko ok, to zapisujemy wartosc
     hMultipleValueResult mvr = _v->value();
     if(!(bool)mvr)
          return false;
       
     fAttrValue = new hMultipleValue(mvr.toSet(true));
          
     setMultiplicity(mltp);        // Restoring the multiplicity value   
     fChanged = true;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets representation of constraints.
//
// #return String contains representation of constraints.
QString XTT_Attribute::ConstraintsRepresentation(void)
{
     QString result = "Contraints class: " + hpTypesInfo.cn.at(hpTypesInfo.ioci(fAttrType->typeClass()));
     if(fAttrType->typeClass() == TYPE_CLASS_DOMAIN)
          result += "\nDomain: " + fAttrType->domain()->toString();
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function sets type of relation for attribute.
//
// #param _rt Type of relations.
// #return True when operation succeeds else false.
bool XTT_Attribute::setClass(unsigned int _rt)
{
     if((_rt != XTT_ATT_CLASS_RO) && (_rt != XTT_ATT_CLASS_RW) && (_rt != XTT_ATT_CLASS_WO) && (_rt != XTT_ATT_CLASS_ST))
          return false;

     fRelationsType = _rt;
     fChanged = true;
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the attribute to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this attribute as string.
QString XTT_Attribute::toProlog(int /*__option*/)
{
     QString lresult = "";
     QString mlptl = "simple";
     QString desc = "";
     int clbindex = CallbackContainer->getByID(fAttrClbId);

     
     if(multiplicity())
          mlptl = "general";
     if(!Description().isEmpty())
          desc = ",\n       desc: " + hqed::mcwp(Description());
     
     lresult +=    "xattr [name: " + hqed::mcwp(Path());
     lresult += ",\n       abbrev: " + hqed::mcwp(Acronym());
     lresult += ",\n       class: " + mlptl;
     lresult += ",\n       type: " + hqed::mcwp(Type()->name());
     lresult += ",\n       comm: " + prologClassString((int)Class());

     if(clbindex != -1)
         lresult += ",\n       callback: [" + CallbackContainer->getPrologName(clbindex) + ",[" + fAttrName + "]]";

     lresult += desc;
     lresult +=  "\n      ].";
     
     CallbackContainer->setUsed(fAttrClbId, true);

     return lresult;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string name of the attribute class.
//
// #param __class - the numeric representation of the class.
// #return the string name of the attribute class.
QString XTT_Attribute::classString(int __class)
{
     QString result = "";
     switch(__class)
     {
          case XTT_ATT_CLASS_RO:
               result = "input";
               break;
          case XTT_ATT_CLASS_RW:
               result = "input/output";
               break;
          case XTT_ATT_CLASS_WO:
               result = "output";
               break;
          case XTT_ATT_CLASS_ST:
               result = "state";
               break;
          default:
               result = "undefined";
               break;
     }
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string name of the attribute class compatible with the HML.
//
// #param __class - the numeric representation of the class.
// #return the string name of the attribute class that is compatible with the HML.
QString XTT_Attribute::xmlClassString(int __class)
{
     QString result = "";
     switch(__class)
     {
          case XTT_ATT_CLASS_RO:
               result = "in";
               break;
          case XTT_ATT_CLASS_RW:
               result = "io";
               break;
          case XTT_ATT_CLASS_WO:
               result = "out";
               break;
          case XTT_ATT_CLASS_ST:
               result = "state";
               break;
          default:
               result = "undefined";
               break;
     }
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string name of the attribute class compatible with the HeaRT.
//
// #param __class - the numeric representation of the class.
// #return the string name of the attribute class that is compatible with the HeaRT.
QString XTT_Attribute::prologClassString(int __class)
{
     QString result = "";
     switch(__class)
     {
          case XTT_ATT_CLASS_RO:
               result = "in";
               break;
          case XTT_ATT_CLASS_RW:
               result = "inter";
               break;
          case XTT_ATT_CLASS_WO:
               result = "out";
               break;
          case XTT_ATT_CLASS_ST:
               result = "state";
               break;
          default:
               result = "undefined";
               break;
     }
     return result;
}
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_Attribute::idPrefix(void)
{
     return "att_xtt_";
}
// -----------------------------------------------------------------------------

// #brief Function copies attribute properties.
//
// #param _new - attribute with desired properties
// #return no values return
void XTT_Attribute::copyProperties(XTT_Attribute* _new)
{
     setId(_new->Id());
     setName(_new->Name());
     setCallbackId(_new->CallbackId());
     setAcronym(_new->Acronym());
     setDescription(_new->Description());
     setValue(_new->Value());
     setClass(_new->Class());
     setType(_new->Type());
     setMultiplicity(_new->multiplicity());
     setChanged(true);
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 java file format.
//
// #param _javafile - writing device
// #return no values return
void XTT_Attribute::out_Drools_Workspace(QTextStream* _javafile)
{
     *_javafile << "\t" << "private " << fAttrType->GetJavaType() << " " << fAttrName << ";\n\n";

     // Getter
     *_javafile << "\t" << "public " << fAttrType->GetJavaType() << " get" << CapitalizedName() << "() {\n";
     *_javafile << "\t\t" << "return " << fAttrName << ";\n";
     *_javafile << "\t" << "}" << "\n\n";

     // Setter
     *_javafile << "\t" << "public void set" << CapitalizedName() << "(" << fAttrType->GetJavaType() << " " << fAttrName << ") {\n";
     *_javafile << "\t\t" << "this." << fAttrName << " = " << fAttrName << ";\n";
     *_javafile << "\t" << "}" << "\n\n";
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Attribute::out_HML_2_0(int /*__mode*/)
{
     QStringList* res = new QStringList;
     res->clear();
     QString aclass = multiplicity() == true?"general":"simple";
     
     QString init_line = "<attr id=\"" + fAttrID + "\"";
     init_line += " type=\"" + fAttrType->id() + "\"";
     init_line += " name=\"" + fAttrName + "\"";
     if(fAttrClbId != " " && fAttrClbId != "")
         init_line += " clb=\"" + fAttrClbId + "\"";
     init_line += " abbrev=\"" + fAttrAcronym + "\"";
     init_line += " class=\"" + aclass + "\"";
     init_line += " comm=\"" + xmlClassString(Class()) + "\"";
     init_line += ">";
     *res << init_line;
     CallbackContainer->setUsed(fAttrClbId,true);
     if(!fAttrDesc.isEmpty())
          *res << "\t<desc>" + fAttrDesc + "</desc>";
     
     *res << "</attr>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void XTT_Attribute::out_HML_2_0(QXmlStreamWriter* _xmldoc, int /*__mode*/)
{
     _xmldoc->writeStartElement("attr");
     _xmldoc->writeAttribute("id", fAttrID);
     _xmldoc->writeAttribute("type", fAttrType->id());
     _xmldoc->writeAttribute("name", fAttrName);
     if(fAttrClbId != " " && fAttrClbId != "")
         _xmldoc->writeAttribute("clb", fAttrClbId);
     _xmldoc->writeAttribute("abbrev", fAttrAcronym);
     _xmldoc->writeAttribute("class", multiplicity()?"general":"simple");
     _xmldoc->writeAttribute("comm", xmlClassString(Class()));
     if(!fAttrDesc.isEmpty())
     _xmldoc->writeTextElement("desc", fAttrDesc);
     _xmldoc->writeEndElement();

     CallbackContainer->setUsed(fAttrClbId,true);
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_Attribute::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "attr";
     
     if(__root.tagName() != elementName)
          return 0;
     QStringList commlist = QStringList() << XTT_Attribute::xmlClassString(XTT_ATT_CLASS_RO) << XTT_Attribute::xmlClassString(XTT_ATT_CLASS_RW) << XTT_Attribute::xmlClassString(XTT_ATT_CLASS_WO) << XTT_Attribute::xmlClassString(XTT_ATT_CLASS_ST);
     QString aid = __root.attribute("id", "");
     QString atype = __root.attribute("type", "");
     QString aname = __root.attribute("name", "");
     QString cid = __root.attribute("clb", " ");
     QString aabbrev = __root.attribute("abbrev", "");
     QString aclass = __root.attribute("class", XTT_Attribute::_class_attribute_.at(0));
     QString acomm = __root.attribute("comm", XTT_Attribute::xmlClassString(XTT_ATT_CLASS_RW));

     QDomElement DOMdesc = __root.firstChildElement("desc");
     if(!DOMdesc.isNull())
          fAttrDesc = DOMdesc.text();
     
     // Checking for errors
     int namecheckingresult = hqed::checkAttributeName(aname);
     if(namecheckingresult == 1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Unnamed attribute. Id = " + aid + "." + hqed::createDOMlocalization(__root), 1, 2);
          result++;
     }
     if(namecheckingresult == 2)
     {
          __msg += "\n" + hqed::createErrorString("", "", "The attribute name \'" + aname + "\' starts with incorrect character. The list of incorrect characters is: " + hqed::attributeNameNotAllowedStartingCharactersList() + "\nId = " + aid + "." + hqed::createDOMlocalization(__root), 1, 2);
          result++;
     }
     
     if(aabbrev == "")
          aabbrev = aname;
     
     // If the class is incorrect, the value is changed to default
     if(XTT_Attribute::_class_attribute_.indexOf(aclass) == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined \'class\' parameter in attribute \'" + aname + "\': \'" + aclass + "\'.\nThe class will be set to \'" + XTT_Attribute::_class_attribute_.at(0) + "\'." + hqed::createDOMlocalization(__root), 1, 2);
          aclass = XTT_Attribute::_class_attribute_.at(0);
          result++;
     }

     // If the comm is incorrect, the value is changed to default
     if(commlist.indexOf(acomm) == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined \'communication\' parameter in attribute \'" + aname + "\': \'" + acomm + "\'.\nThe communication will be set to \'" + XTT_Attribute::xmlClassString(XTT_ATT_CLASS_RW) + "\'." + hqed::createDOMlocalization(__root), 1, 2);
          result++;
     }

     // Checking if the type of this attribute exists
     int tindex = typeList->indexOfID(atype);
     if(tindex == -1)
     {
          tindex = hpTypesInfo.iopti(NUMERIC_BASED);
          __msg += "\n" + hqed::createErrorString("", "", "Undefined type of attribute \'" + aname + "\'.\nThe type will be set to \'" + hpTypesInfo.ptn.at(tindex) + "\'." + hqed::createDOMlocalization(__root), 1, 2);
          result++;
     }

     fAttrID = aid;
     fAttrName = aname;
     fAttrAcronym = aabbrev;
     fRelationsType = commlist.indexOf(acomm);
     setMultiplicity(XTT_Attribute::_class_attribute_.indexOf(aclass) == 1);
     fAttrType = typeList->at(tindex);
     fAttrValue->setParentType(fAttrType);
     fAttrClbId = cid;

     return result;
}
// -----------------------------------------------------------------------------
