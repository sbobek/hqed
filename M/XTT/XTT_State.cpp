/*
 *     $Id: XTT_State.cpp,v 1.2.4.3 2011-06-06 15:33:33 kkr Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QDomDocument>

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "../../V/XTT/GStateConnection.h"

#include "../hSet.h"
#include "../hType.h"
#include "../hMultipleValue.h"

#include "XTT_AttributeGroups.h"
#include "XTT_Statesgroup.h"
#include "XTT_Attribute.h"
#include "XTT_States.h"
#include "XTT_State.h"
// -----------------------------------------------------------------------------

// #brief Constructor of XTT_State class.
//
// #param __parent - pointer to the parent object
XTT_State::XTT_State(XTT_Statesgroup* __parent)
{
     _parent = __parent;
     _initialValues = new QHash<XTT_Attribute*, hSet*>;
     _finalValues = new QHash<XTT_Attribute*, hSet*>;
     _trajectory = new QList<XTT_Row*>;
     
     refreshOutputAttributesList();
     
     _connection = new GStateConnection(xtt::stateScene(), this);
}
// -----------------------------------------------------------------------------

// #brief Destructor of XTT_State class.
XTT_State::~XTT_State(void)
{
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();++i)
          delete i.value();
     for(i=_finalValues->begin();i!=_finalValues->end();++i)
          delete i.value();

     _initialValues->clear();
     _finalValues->clear();
     delete _initialValues;
     delete _finalValues;
     delete _trajectory;
     delete _connection;
}
// -----------------------------------------------------------------------------

// #brief Function that copies all the values to the destination object
//
// #param __destination - pointer to the destination object
// #return pointer to the new object
XTT_State* XTT_State::copyTo(XTT_State* __destination)
{
     XTT_State* result = __destination;
     if(result == NULL)
          result = new XTT_State(_parent);
     
     result->setId(_parent->parent()->findUniqId());
     
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();i++)
          (*result->_initialValues)[i.key()] = i.value()->copyTo();
     for(i=_finalValues->begin();i!=_finalValues->end();i++)
          (*result->_finalValues)[i.key()] = i.value()->copyTo();
          
     for(int t=0;t<_trajectory->size();++i)
          result->_trajectory->append(_trajectory->at(t));

     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the xtt to state
//
// #return true if everthing was ok, otherwise false
bool XTT_State::loadState(void)
{
     bool result = true;
     QHash<XTT_Attribute*, hSet*>::iterator i;
     
     for(i=_initialValues->begin();i!=_initialValues->end();i++)
     {
          hMultipleValue* mv = new hMultipleValue(i.value()->copyTo());
          if(i.key()->checkValue(mv))
               i.key()->setValue(mv);
          delete mv;
     }
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes the list of output attributes
//
// #return no values return
void XTT_State::refreshOutputAttributesList(void)
{
     QList<XTT_Attribute*>* currList = outputAttributes();
     QList<XTT_Attribute*> destList = AttributeGroups->classFilter(XTT_ATT_CLASS_WO);
     
     // Dodawanie niedodanych
     for(int i=0;i<destList.size();++i)
          if(!currList->contains(destList.at(i)))
               (*_finalValues)[destList.at(i)] = hSet::makeNotDefinedValue(destList.at(i)->Type());
               
     // Usuwanie nieusunietych
     for(int i=0;i<currList->size();++i)
          if(!destList.contains(currList->at(i)))
               delete _finalValues->take(currList->at(i));
               
     delete currList;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the object has been changed
//
// #return true if the object has been changed, otherwise false
bool XTT_State::changed(void)
{
     return _changed;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the status of changed paramteter
//
// #param __c - the new status of changed parameter
// #return no values return
void XTT_State::setChanged(bool __c)
{
     _changed = __c;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the id of the state
//
// #return the id of the state as the string
QString XTT_State::id(void)
{
     return _stateId;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the full name of the state
//
// #return the full name of the state as the string
QString XTT_State::fullname(void)
{
     if(_parent == NULL)
          return "";
          
     QString result;
     QString part1 = _parent->baseName();
     QString part2 = XTT_Statesgroup::partSeparator();
     QString part3 = _parent->key(this);
     
     result = part1 + (part3.isEmpty() == true ? "" : part2 + part3);
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the parent of the object
//
// #return pointer to the parent of the object
XTT_Statesgroup* XTT_State::parent(void)
{
     return _parent;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the id of the state
//
// #param __newid - the new id of the state as the string
// #return no values return
void XTT_State::setId(QString __newid)
{
     _stateId = __newid;
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the list of intial values
//
// #return pointer to the list of intial values
QHash<XTT_Attribute*, hSet*>* XTT_State::initialValues(void)
{
     return _initialValues;
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the list of final values
//
// #return pointer to the list of final values
QHash<XTT_Attribute*, hSet*>* XTT_State::finalValues(void)
{
     return _finalValues;
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the trajectory
//
// #return pointer to the list of trajectory
QList<XTT_Row*>* XTT_State::trajectory(void)
{
     return _trajectory;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the state has defined trajecotry to the output
//
// #return if the state has defined trajecotry to the output
bool XTT_State::hasTrajectory(void)
{
     return !_trajectory->isEmpty();
}
// -----------------------------------------------------------------------------

// #brief Function that selects the trajectory that starts from this state
//
// #brief __select - the status of selection
// #return no values return
void XTT_State::selectTrajectory(bool __select)
{
     for(int i=0;i<_trajectory->size();++i)
          _trajectory->at(i)->setSelected(__select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the state
//
// #brief __select - the status of selection
// #return no values return
void XTT_State::selectState(bool __select)
{
     if(_parent != NULL)
          _parent->selectState(this, __select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the state
//
// #brief __att - ppointer to the attribute
// #brief __select - the status of selection
// #return no values return
void XTT_State::selectCell(XTT_Attribute* __att, bool __select)
{
     if(_parent == NULL)
          return;
      _parent->selectCell(this, __att, __select);
}
// -----------------------------------------------------------------------------

// #brief Function that canceles the simulation results
//
// #return no values return
void XTT_State::cancelSimulation(void)
{
     // Cleaning trajectory
     _trajectory->clear();
     
     // Reseting the result values of simulation
     QList<XTT_Attribute*> keys = _finalValues->keys();
     for(int i=0;i<keys.size();++i)
     {
          hSet* oldset = (*_finalValues)[keys.at(i)];
          hSet* nilset = hSet::makeNotDefinedValue(keys.at(i)->Type());
          oldset->flush();
          nilset->copyTo(oldset);
          delete nilset;
     }
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of atributes of the all elements in the state
//
// #return list of atributes of the all elements in the state
QList<XTT_Attribute*>* XTT_State::inputAttributes(void)
{
     QList<XTT_Attribute*>* result = new QList<XTT_Attribute*>;
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();i++)
          result->append(i.key());
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of atributes of the all elements in the state
//
// #return list of atributes of the all elements in the state
QList<XTT_Attribute*>* XTT_State::outputAttributes(void)
{
     QList<XTT_Attribute*>* result = new QList<XTT_Attribute*>;
     QHash<XTT_Attribute*, hSet*>::iterator i;
     
     for(i=_finalValues->begin();i!=_finalValues->end();i++)
          result->append(i.key());
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the input attribute at the given position
//
// #param __offset - the offset from the first element of the container
// #return the pointer to the input attribute at the given position
XTT_Attribute* XTT_State::inputAttributeAt(int __offset)
{
     if((__offset < 0) || (__offset >= _initialValues->size()))
          return NULL;
          
     return (_initialValues->begin()+__offset).key();
}
// -----------------------------------------------------------------------------

// #brief Function that resets the value of the attribute
//
// #param __attr - pointer to the attribute
// #param __value - the pointer to the input set that is realted to the given attribute
// #return no values return
void XTT_State::resetInputValueTo(XTT_Attribute* __attr, hSet* __value)
{
     //if(_initialValues->contains(__attr))
          setInputValueAt(__attr, __value);
     
     cancelSimulation();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the value of the attribute
//
// #param __attr - pointer to the attribute
// #param __value - the pointer to the input set that is realted to the given attribute
// #return no values return
void XTT_State::setInputValueAt(XTT_Attribute* __attr, hSet* __value)
{
     if(_initialValues->contains(__attr))
          delete _initialValues->take(__attr);
          
     (*_initialValues)[__attr] = __value;
     cancelSimulation();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the input set at the given position
//
// #param __offset - the offset from the first element of the container
// #return the pointer to the input set at the given position
hSet* XTT_State::inputSetAt(int __offset)
{
     if((__offset < 0) || (__offset >= _initialValues->size()))
          return NULL;
          
     return (_initialValues->begin()+__offset).value();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the input set that is realted to the given attribute
//
// #param __attr - pointer to the attribute
// #return the pointer to the input set that is realted to the given attribute
hSet* XTT_State::inputSetAt(XTT_Attribute* __attr)
{
     return _initialValues->value(__attr);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the output attribute at the given position
//
// #param __offset - the offset from the first element of the container
// #return the pointer to the output attribute at the given position
XTT_Attribute* XTT_State::outputAttributeAt(int __offset)
{
     if((__offset < 0) || (__offset >= _finalValues->size()))
          return NULL;
          
     return (_finalValues->begin()+__offset).key();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the output set at the given position
//
// #param __offset - the offset from the first element of the container
// #return the pointer to the output set at the given position
hSet* XTT_State::outputSetAt(int __offset)
{
     if((__offset < 0) || (__offset >= _finalValues->size()))
          return NULL;
          
     return (_finalValues->begin()+__offset).value();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the output set that is realted to the given attribute
//
// #param __attr - pointer to the attribute
// #return the pointer to the output set that is realted to the given attribute
hSet* XTT_State::outputSetAt(XTT_Attribute* __attr)
{
     return _finalValues->value(__attr);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the value of the attribute
//
// #param __attr - pointer to the attribute
// #param __value - the pointer to the output set that is realted to the given attribute
// #return no values return
void XTT_State::setOutputValueAt(XTT_Attribute* __attr, hSet* __value)
{
     if(_finalValues->contains(__attr))
          delete _finalValues->take(__attr);
          
     (*_finalValues)[__attr] = __value;
}
// -----------------------------------------------------------------------------

// #brief Function that removes from the input all states an given attribute
//
// #param __attr - pointer to the given attribute
// #return no values return
void XTT_State::removeInputAttribute(XTT_Attribute* __attr)
{
     if(!_initialValues->contains(__attr))
          return;
          
     delete _initialValues->take(__attr);
     
     cancelSimulation();
}
// -----------------------------------------------------------------------------

// #brief Function that removes from the all output states an given attribute
//
// #param __attr - pointer to the given attribute
// #return no values return
void XTT_State::removeOutputAttribute(XTT_Attribute* __attr)
{
     if(!_finalValues->contains(__attr))
          return;
          
     delete _finalValues->take(__attr);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the connection
//
// #return the pointer to the connection
GStateConnection* XTT_State::uiConnection(void)
{
     return _connection;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the HMR representation of this state element
//
// #param __fullstatename - the full name of the state
// #return the HMR representation of this state element as string
QString XTT_State::toProlog(QString __fullstatename)
{
     QString result;
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();++i)
          result += "xstat " + hqed::mcwp(__fullstatename) + ": [" + hqed::mcwp(i.key()->Path()) + "," + i.value()->toProlog() + "].\n";
          
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_State::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     QHash<XTT_Attribute*, hSet*>::iterator i;
     
     for(i=_initialValues->begin();i!=_initialValues->end();i++)
          res = i.value()->updateTypePointer(__old, __new) && res;
          
     for(i=_finalValues->begin();i!=_finalValues->end();i++)
          res = i.value()->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_State::eventMessage(QString* /*__errmsgs*/, int __event, ...)
{ 
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);

          if(attr->Class() != 2)        // when not output
               removeOutputAttribute(attr);
          if(attr->Class() == 2)        // when output
          {
               if(!_finalValues->contains(attr))  // when not in container - adding a new one
               {
                    hSet* value = hSet::makeNotDefinedValue(attr->Type());
                    (*_finalValues)[attr] = value;
               }
          }
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);

          removeInputAttribute(attr);
          removeOutputAttribute(attr);
          
          cancelSimulation();
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Static function that trie to find pointer to the attribute that has the given ID
//
// #param __attid - id of the attribute
// #return pointer to the attribute, if attribute does not exists then it returns NULL
XTT_Attribute* XTT_State::findAttribute(QString __attid)
{
     return AttributeGroups->FindAttribute(__attid);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the row object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_State::stateVerification(QString& __error_message)
{
     bool res = true;
     
     // Checking the size of the initial list
     if(_initialValues->size() == 0)
     {
          QString _Error = "No attributes related with the state \'" + fullname() + "\'. The state should define an initial value of at least one attribute";
          __error_message += hqed::createErrorString(fullname(), "", _Error, 0, 7);
          res = false;
     }
     
     // Checking the correctness of the initial values
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();i++)
     {
          hType* atttype = i.key()->Type();
          hSet* set = i.value();
          
          // Checking if the set is correct
          if(atttype->isSetCorrect(set, true) != H_TYPE_ERROR_NO_ERROR)
          {
               QString _Error = "Incorrect initial value of the attribute \'" + i.key()->Name() + "\' in state \'" + fullname() + "\'.\nError message: " + atttype->lastError();
               __error_message += hqed::createErrorString(fullname(), i.key()->Id(), _Error, 0, 8);
               res = false;
          }
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id of state.
//
// #return the state prefix of the id as string.
QString XTT_State::stateIdPrefix(void)
{
     return "sts_";
}
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_State::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "attref";
     QDomElement DOMattref = __root.firstChildElement(elementName);
          
     while(!DOMattref.isNull())
     {
          // Reading attribute data
          QString attid = DOMattref.attribute("ref", "");
          if(attid == "")               // Checking if the reference is not empty
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined reference to attribute." + hqed::createDOMlocalization(DOMattref), 0, 1);
               DOMattref = DOMattref.nextSiblingElement(elementName);
               continue;
          }
          
          XTT_Attribute* attribue = findAttribute(attid);
          if(attribue == NULL)          // Checking if the reference is correct
          {
               __msg += "\n" + hqed::createErrorString("", "", "Reference to undefined attribute \'" + attid + "\'." + hqed::createDOMlocalization(DOMattref), 0, 1);
               DOMattref = DOMattref.nextSiblingElement(elementName);
               continue;
          }
          if(_initialValues->contains(attribue))  // Checking if the attribute already exists in the map
          {
               __msg += "\n" + hqed::createErrorString("", "", "The state already contains attribute \'" + attid + "\'. The value will not be replaced." + hqed::createDOMlocalization(DOMattref), 0, 1);
               DOMattref = DOMattref.nextSiblingElement(elementName);
               continue;
          }
          
          // Reading set of values
          QDomElement DOMset = DOMattref.nextSiblingElement();
          if(DOMset.tagName().toLower() != "set")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Incorrect tag sequence. The state should be defined using pairs: \'attref\', \'set\'." + hqed::createDOMlocalization(DOMset), 0, 1);
               DOMattref = DOMattref.nextSiblingElement(elementName);
               continue;
          }
          hSet* value = new hSet(attribue->Type());
          result += value->in_HML_2_0(DOMset, __msg, NULL);
          
          // Adding new element to the container
          (*_initialValues)[attribue] = value;
          
          // Reading new state element
          DOMattref = DOMattref.nextSiblingElement(elementName);
     }
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_State::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();++i)
     {
          *res << "<attref ref=\"" + i.key()->Id() + "\"/>";
          
          QStringList* tmp = i.value()->out_HML_2_0(__mode);
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_State::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     QHash<XTT_Attribute*, hSet*>::iterator i;
     for(i=_initialValues->begin();i!=_initialValues->end();++i)
     {
          _xmldoc->writeEmptyElement("attref");
          _xmldoc->writeAttribute("ref", i.key()->Id());
          i.value()->out_HML_2_0(_xmldoc, __mode);
     }
}
#endif
// -----------------------------------------------------------------------------
