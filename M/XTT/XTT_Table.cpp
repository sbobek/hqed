/*
 *        $Id: XTT_Table.cpp,v 1.46.4.6 2012-01-02 04:48:34 rkl Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>
 
#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/ErrorsListWidget.h"
#include "../../V/XTT/GTable.h"
#include "../hSet.h"
#include "../hType.h"
#include "../hSetItem.h"
#include "../hFunction.h"
#include "../hExpression.h"
#include "../hValueResult.h"
#include "../hMultipleValue.h"
#include "../hFunctionArgument.h"
#include "../hMultipleValueResult.h"
#include "XTT_AttributeGroups.h"
#include "XTT_ConnectionsList.h"
#include "XTT_Column.h"
#include "XTT_Table.h"
#include "XTT_TableList.h"
#include "XTT_Cell.h"
//---------------------------------------------------------------------------

// #brief Constructor XTT_Table class.
XTT_Table::XTT_Table(void)
{
     fNextRowId = NULL;
     fTableID = "";
     fTitle = "";
     fDescription = "";
     fnContextSize = new unsigned int[CONTEXT_COUNT];
     for(int i=0;i<CONTEXT_COUNT;i++)
          fnContextSize[i] = 0;
     fRowCount = 0;
     fRows = NULL;
     fPoint.setX(0);
     fPoint.setY(0);
     fHeight = NULL;
     fChanged = false;
     fLevel = 0;
     fParent = 0;
     fTableType = 1; 
     fNextAbstractID = 1;
     fNextCellId = 0;
     _columns = new QList<XTT_Column*>;
     fHeaderHeight = Settings->xttDefaultHeaderHeight();
     fGTable = new GTable(xtt::modelScene(), this);
}
//---------------------------------------------------------------------------

// #brief Constructor XTT_Table class.
// #param _tid Table id.
// #param _ptrid Pointer to the next table id.
XTT_Table::XTT_Table(unsigned int _tid, unsigned int* _prid)
{
     fNextRowId = _prid;
     fTableID = idPrefix() + QString::number(_tid);
     fTitle = "";
     fDescription = "";
     fnContextSize = new unsigned int[CONTEXT_COUNT];
     for(int i=0;i<CONTEXT_COUNT;i++)
          fnContextSize[i] = 0;
     fRowCount = 0;
     fRows = NULL;
     fPoint.setX(0);
     fPoint.setY(0);
     fHeight = NULL;
     fChanged = false;
     fLevel = 0;
     fParent = 0;
     fTableType = 1;
     fNextAbstractID = 1;
     fNextCellId = 0;
     _columns = new QList<XTT_Column*>;
     fHeaderHeight = Settings->xttDefaultHeaderHeight();
     fGTable = new GTable(xtt::modelScene(), this);
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_Table class.
XTT_Table::~XTT_Table(void)
{
     // Clear selection
     if(fGTable != NULL)
          fGTable->unselectAllElements();
     if(fGTable != NULL)
          delete fGTable;

     while(fRowCount > 0)
          DeleteRow(0);

     // Usuwanie zaalokowanych obiektow
     while(_columns->size() > 0)
          delete _columns->takeFirst();
     delete _columns;

     delete [] fnContextSize;

     if(fRows != NULL)
          delete [] fRows;

     // Usuwanie polaczen dolaczonych do tego wiersz
     ConnectionsList->RemoveConnectionsWithTableID(fTableID);
}
//---------------------------------------------------------------------------

// #brief Function that returns the next id of a row
//
// #param __icrement - indicates if the value should be incremented after return
// #return the next id of a row
unsigned int XTT_Table::nextRowId(bool __icrement)
{
     if(__icrement)
          ++(*fNextRowId);
     return *fNextRowId;
}
//---------------------------------------------------------------------------

// #brief Function that returns the next id of a cell
//
// #param __icrement - indicates if the value should be incremented after return
// #return the next id of a cell
unsigned int XTT_Table::nextCellId(bool __icrement)
{
     if(__icrement)
          fNextCellId++;
     return fNextCellId;
}
//---------------------------------------------------------------------------

// #brief Function retrieves number of column.
//
// #return Number of column.
int XTT_Table::ColCount(void)
{
     int cc = 0;
     for(int i=0;i<CONTEXT_COUNT;++i)
          cc += fnContextSize[i];
          
     return cc;
}
//---------------------------------------------------------------------------

// #brief Function retrieves size of the context.
//
// #param _cid Context id.
// #return Size of the context. When context id is incorrect, function returns -1.
int XTT_Table::ContextSize(unsigned int _cindex)
{
     // Sprawdzanie indexu
     if((_cindex < 1) || (_cindex > CONTEXT_COUNT))
          return -1;

     return (int)fnContextSize[_cindex-1];
}
//---------------------------------------------------------------------------

// #brief Function retrieves last not empty context.
//
// #return Last not empty context.
int XTT_Table::LastNotEmptyContext(void)
{
     int res = CONTEXT_COUNT-1;
     while((res >= 0) && (fnContextSize[res] == 0))
     {
          res--;
     }

     return res+1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the attribute.
//
// #param _index Index of the attribute.
// #return Pointer to the attribute. When context index is incorrect, function returns -1.
XTT_Attribute* XTT_Table::ContextAtribute(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= (unsigned int)ColCount()))
          return NULL;

     return _columns->at(_index)->attribute();
}
//---------------------------------------------------------------------------

// #brief Returns list of attributes in given context
//
// #param unsigned int _ctx - context identifier
// #param __uniq - indicates if the list must contain onlu uniques pointers
// #return list of attributes in given context, if context identifier is incorrect returs empty list
QList<XTT_Attribute*>* XTT_Table::ContextAtributes(unsigned int _ctx, bool __uniq)
{
     QList<XTT_Attribute*>* res = new QList<XTT_Attribute*>;
     
     if((_ctx < 1) || (_ctx > CONTEXT_COUNT))
          return res;
          
     int sindex = 0;
     for(unsigned int c=1;c<_ctx;++c)
          sindex += fnContextSize[c-1];
          
     for(unsigned int c=0;c<fnContextSize[_ctx-1];++c)
          if((!__uniq) || (res->indexOf(_columns->at(sindex+c)->attribute()) == -1))
               res->append(_columns->at(sindex+c)->attribute());
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the row of the table.
//
// #param _index Row index.
// #return Pointer to the row of the table.
XTT_Row* XTT_Table::Row(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= fRowCount))
          return NULL;

     return fRows[_index];
}
//---------------------------------------------------------------------------

// #brief Function determines whether the table was modyfied.
//
// #return Decision whether the table was modyfied.
bool XTT_Table::Changed(void)
{
     for(unsigned int i=0;i<fRowCount;i++)
          if(fRows[i]->Changed())
               return true;
          
     return fChanged;
}
//---------------------------------------------------------------------------

// #brief Function sets title of the table.
//
// #param _title Title of the table.
// #return No return value.
void XTT_Table::setTitle(QString _title)
{
     fTitle = _title;
     fChanged = true;
     
     connectionsDiagnostics();
}
//---------------------------------------------------------------------------

// #brief Function sets ID of the table.
//
// #param __id - new table identifier
// #return No return value.
void XTT_Table::setID(QString __id)
{    
     fTableID = __id;
     
     // Updateing list of in connections
     QList<XTT_Connections*>* ic = inConnectionsList();
     for(int i=0;i<ic->size();++i)
          ic->at(i)->setDesTable(fTableID);
     delete ic;
     
     // Updateing list of out connections
     QList<XTT_Connections*>* oc = outConnectionsList();
     for(int i=0;i<oc->size();++i)
          oc->at(i)->setSrcTable(fTableID);
     delete oc;
}
//---------------------------------------------------------------------------

// #brief Function sets describtion of the table.
//
// #param _desc Describtion of the table.
// #return No return value.
void XTT_Table::setDescription(QString _desc)
{
     fDescription = _desc;
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets coordinate of the table.
//
// #param _point Coordinate of the table.
// #return Always true.
bool XTT_Table::setLocalization(QPoint _point)
{
     fPoint = _point;
     fGTable->RePosition();
     
     // Odswiezenie polaczen
     ConnectionsList->RepaintConnectionRelatedWithTable(fTableID);
     
     return true;
}
//---------------------------------------------------------------------------

// #brief Function sets type of the table.
//
// #param _type Type of the table.
// #return No return value.
void XTT_Table::setTableType(unsigned int _tt)
{ 
     fTableType = _tt;
     fChanged = true;
     connectionsDiagnostics();
}
//---------------------------------------------------------------------------

// #brief Function sets level of nesting of the table.
//
// #param _level Level of nesting of the table.
// #return No return value.
void XTT_Table::setLevel(unsigned int _l)
{ 
     fLevel = _l; 
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets parent id.
//
// #param _pid Parent id.
// #return No return value.
void XTT_Table::setParent(unsigned int _p)
{ 
     fParent = _p;
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets decision whether the table was modyfied.
//
// #param _b Decision whether the table was modyfied.
// #return No return value.
void XTT_Table::setChanged(bool _changed)
{ 
     for(unsigned int i=0;i<fRowCount;i++)
          fRows[i]->setChanged(_changed);
          
     fChanged = _changed;
}
//---------------------------------------------------------------------------

// #brief Function sets height of the header.
//
// #param _height Height of the header.
// #return No return value.
void XTT_Table::setHeaderHeight(unsigned int _hh)
{ 
     fHeaderHeight = _hh; 
     //fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function that returns pointer to the object that holds the column properties
//
// #param __index - index of the column
// #return pointer to the object that holds the column properties
XTT_Column* XTT_Table::column(int __index)
{
     if((__index < 0) || (__index >= _columns->size()))
          return NULL;
          
     return _columns->at(__index);
}
//---------------------------------------------------------------------------

XTT_Cell* XTT_Table::cell(QString __id)
{
    for(unsigned int i = 0; i < RowCount(); i++)
    {
        for(int j = 0; j < ColCount(); j++)
        {
            XTT_Cell* currentCell = fRows[i]->Cell(j);
            if(currentCell->CellId() == __id)
            {
                return currentCell;
            }
        }
    }

    return 0;
}

XTT_Cell* XTT_Table::cell(unsigned int __idx)
{
    return cell(XTT_Cell::idPrefix() + QString::number(__idx));
}

// #brief Function retrieves width of the table
//
// #return Width of the table.
unsigned int XTT_Table::Width(void)
{
     unsigned int res = 0;
     for(int i=0;i<ColCount();i++)
          res += _columns->at(i)->width();

     // Na podwojna linie
     /*
     // Kiedy istnieje granica pomiedzy conditional i assert lub retract
     if((fnContextSize[0] > 0) && ((fnContextSize[1] > 0) || (fnContextSize[2] > 0)))
          res += 2;

     // Kiedy istnieje granica pomiedzy action i assert lub retract
     if((fnContextSize[3] > 0) && ((fnContextSize[1] > 0) || (fnContextSize[2] > 0)))
          res += 2;

     // Kiedy tylko wystepuja konteksty conditional i action
     if((fnContextSize[1] > 0) && (fnContextSize[1] == 0) && (fnContextSize[2] == 0) && (fnContextSize[3] > 0))
          res += 2;  */

     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves height of the table.
//
// #return Height of the table.
unsigned int XTT_Table::Height(void)
{
     unsigned int res = fHeaderHeight;
     for(unsigned int i=0;i<fRowCount;i++)
          res += fHeight[i];

     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves width of the column.
//
// #param _index Index of the column.
// #return Width of the column.
unsigned int XTT_Table::ColWidth(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= (unsigned int)ColCount()))
          return 0;

     return _columns->at(_index)->width();
}
//---------------------------------------------------------------------------

// #brief Function retrieves height of the row of the table.
//
// #param _index Index of the row.
// #return Height of the row of the table.
unsigned int XTT_Table::RowHeight(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= fRowCount))
          return 0;

     return fHeight[_index];
}
//---------------------------------------------------------------------------

// #brief Function sets width of the column.
//
// #param _index Index of the column.
// #param _width Width of the column.
// #return Always true.
bool XTT_Table::setColWidth(unsigned int _index, unsigned int _width)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= (unsigned int)ColCount()))
          return false;

     _columns->at(_index)->setWidth(_width);
     //fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function sets height of the row.
//
// #param _index Index of the row.
// #param _height Height of the table.
// #return Always true.
bool XTT_Table::setRowHeight(unsigned int _index, unsigned int _height)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= fRowCount))
          return false;

     fHeight[_index] = _height;
     //fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function sets context of the attributes.
//
// #param _cindex Index of the column
// #param _pattr Pointer to the attribute.
// #return True when pointer to the attribute is correct, otherwise function returns false.
bool XTT_Table::setContextAtributes(unsigned int _colIndex, XTT_Attribute* _newAttr)
{
     if((((int)_colIndex) < 0) || (_colIndex >= (unsigned int)ColCount()))
          return false;
     //if(_newAttr == NULL)
     //     return false;

     try
     {
          _columns->at(_colIndex)->setAttribute(_newAttr);
          for(unsigned int i=0;i<fRowCount;i++)
               fRows[i]->Cell(_colIndex)->setAttrType(_newAttr);
     }
     catch(...)
     {
          return false;
     }

     // Ze sie zmienilo
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function changes attribute of the cell.
//
// #param _context Index of the context.
// #param _cindex Index of the column
// #param _pattr Pointer to the attribute.
// #return True when pointer to the attribute is correct, otherwise function returns false.
bool XTT_Table::setContextAtributes(unsigned int _context, unsigned int _colIndex, XTT_Attribute* _newAttr)
{
     // Jezeli kontekst jest niepoprawnie okreslony
     if((_context < 1) || (_context > CONTEXT_COUNT))
          return false;

     // Jezeli index kolumny jest niepopranwnie okreslony
     if((((int)_colIndex) < 0) || (_colIndex >= fnContextSize[_context-1]))
          return false;

     // Jezeli wskaznik nie poazujacy na cos konkretnego :)
     if(_newAttr == NULL)
          return false;

     // Wyszukanie docelowej kolumny
     int col_ind = 0;
     for(unsigned int i=0;i<(_context-1);i++)
          col_ind += fnContextSize[i];
     col_ind += _colIndex;

     // Ustawienie nowego atrybutu, w razie jakis bledow zwracamy false
     try
     {
          _columns->at(col_ind)->setAttribute(_newAttr);
          for(unsigned int i=0;i<fRowCount;i++)
               fRows[i]->Cell(col_ind)->setAttrType(_newAttr);
     }
     catch(...)
     {
          return false;
     }

     // Ze sie zmienilo
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function retrieves context of the column.
//
// #param _cindex Index of the column.
// #return Context of the column. When index is incorrect function returns -1.
int XTT_Table::ColContext(int _cindex)
{
     // Sprawdzanie indexu
     if((_cindex < 0) || (_cindex >= ColCount()))
          return -1;

     // Wyszukiwanie kontekstu
     int res = 0;
     for(int i=0;i<CONTEXT_COUNT;i++)
     {
          res += fnContextSize[i];
          if(res > _cindex)
               return i+1;
     }

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves container index of the column.
//
// #param _context Context of the column.
// #param _index Index of the column.
// #return Container index of hte column.
int XTT_Table::ColIndex(unsigned int _context, unsigned int _index)
{
     // Jezeli kontekst jest niepoprawnie okreslony
     if((_context < 1) || (_context > CONTEXT_COUNT))
          return -1;

     // Jezeli index kolumny jest niepopranwnie okreslony
     if((((int)_index) < 0) || (_index >= fnContextSize[_context]))
          return -1;

     // Sumowanie kolumn z poprzedzajacych konykstow
     int res = 0;
     for(unsigned int i=0;i<(_context-1);i++)
          res += fnContextSize[i];

     // Przesuniecie o index
     res += _index;

     return res;
}
//---------------------------------------------------------------------------

// #brief Function that returns the sorting criterion of the given column
//
// #param __index - index of the column
// #return sorting criterion
int XTT_Table::ColSortCriterion(int __index)
{
     if(__index >= ColCount())
          return XTT_COLUMN_SORT_ALPHABETICAL;
     return _columns->at(__index)->sortCriterion();
}
//---------------------------------------------------------------------------

// #brief Function retrieves changes two columns with each other.
//
// #param _index1 Index of the first column.
// #param _index2 Index of the second column.
// #return True when everything is ok, otherwise function returns false.
bool XTT_Table::Exchange(unsigned int _index1, unsigned int _index2)
{
     // Sprawdzanie indexu
     if((((int)_index1) < 0) || (_index1 >= (unsigned int)ColCount()))
          return false;

     // Sprawdzanie indexu
     if((((int)_index2) < 0) || (_index2 >= (unsigned int)ColCount()))
          return false;

     // Zamienianie poszczegolnych komorek
     bool ok = true;
     for(unsigned int i=0;i<fRowCount;i++)
          ok = ok && fRows[i]->Exchange(_index1, _index2);
     _columns->swap(_index1, _index2);

     // Ze sie zmienilo
     fChanged = true;
     
     // Odswiezenie widoku tabeli
     fGTable->RePosition();

     return ok;
}
//---------------------------------------------------------------------------

// #brief Function that creates a new row for this table
//
// #return pointer to the created row
XTT_Row* XTT_Table::CreateRow(void)
{
     XTT_Row* newRow = new XTT_Row(fTableID, ++(*fNextRowId), &fNextCellId);

     // Ustawienie komorek w nowym wierszu
     for(unsigned int i=0;i<(unsigned int)ColCount();i++)
     {
          newRow->Insert(i, ColContext(i));
          newRow->Cell(i)->setAttrType(_columns->at(i)->attribute());
     }
     newRow->createDefaultRow();
     
     return newRow;
}
//---------------------------------------------------------------------------

// #brief Function adds new row to the table.
//
// #return pointer to the added row
XTT_Row* XTT_Table::AddRow(void)
{
     XTT_Row* res = CreateRow();
     AddRow(res);
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function adds new row to the table.
//
// #param __row - pointer to the row that should be added
// #return No return value.
void XTT_Table::AddRow(XTT_Row* __row)
{
     if(__row == NULL)
          return;

     // Alokacja tymczasowej tablicy
     XTT_Row** tmp = new XTT_Row*[fRowCount+1];
     unsigned int* htmp = new unsigned int[fRowCount+1];

     // Kopiowanie wskaznikow
     for(unsigned int i=0;i<fRowCount;i++)
     {
          tmp[i] = fRows[i];
          htmp[i] = fHeight[i];
     }

     // Skasowanie strej za malej tablicy elementow
     if(fRowCount)
     {
          delete [] fHeight;
          delete [] fRows;
     }

     // Zapisanie nowej lokalizacji
     fRows = tmp;
     fHeight = htmp;

     // Utworzenie nowego elementu
     fRows[fRowCount] = __row;
     fHeight[fRowCount] = Settings->xttDefaultRowHeight();

     // Powiekszenie pojemnika
     fRowCount++;
     
     fGTable->ReCreate();
     fGTable->Update();

     // Ze sie zmienilo
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function removes row from the table and all connections connected to this row.
//
// #param _index Index of the row.
// #return True when everything is ok, otherwise function returns false.
bool XTT_Table::DeleteRow(unsigned int _index)
{
     // Alokacja tymczasowej tablicy
     XTT_Row** tmp = new XTT_Row*[fRowCount-1];
     unsigned int* htmp = new unsigned int[fRowCount-1];

     // Kopiowanie wskaznikow
     int tmp_i = 0;
     for(unsigned int i=0;i<fRowCount;i++)
          if(i != _index)
          {
               tmp[tmp_i] = fRows[i];
               htmp[tmp_i++] = fHeight[i];
          }

     // Skasowanie strej za malej tablicy elementow
     if(fRowCount)
     {
          delete fRows[_index];
          delete [] fHeight;
          delete [] fRows;
     }

     // Zapisanie nowej lokalizacji
     fRows = tmp;
     fHeight = htmp;

     // Powiekszenie pojemnika
     fRowCount--;

     // Ze sie zmienilo
     fChanged = true;

     // Odsiezenie widoku
     TableList->Refresh(TableList->IndexOfID(fTableID));

     return true;
}
//---------------------------------------------------------------------------

// #brief Function inserts new column to the table.
//
// #param _index Index of the column.
// #return True when index is correct, otherwise function returns false.
bool XTT_Table::InsertCol(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index > (unsigned int)ColCount()))
          return false;

     // Alokacja pamieci na nowa szerokosc
     _columns->insert(_index, new XTT_Column);
     _columns->at(_index)->setAbstractId(fNextAbstractID++);

     // Dodawanie nowej kolumny
     int ctx = LastNotEmptyContext();
     if(_index < (unsigned int)ColCount())
          ctx = ColContext(_index);
     for(unsigned int i=0;i<fRowCount;i++)
     {
          fRows[i]->Insert(_index, ctx);
          fRows[i]->Cell(_index)->setAttrType(_columns->at(_index)->attribute());
     }
          
     // Powiekszenie kontekstu
     fnContextSize[ctx-1]++;

     // Ze sie zmienilo
     fChanged = true;
     
     return true;
}
//---------------------------------------------------------------------------

// #brief Function inserts new row to the table.
//
// #param _tidx Index of the row of the table.
// #return True when index is ok, otherwise function returns false.
bool XTT_Table::insertRow(int _index)
{
     return insertRow(_index, CreateRow());
}
//---------------------------------------------------------------------------

// #brief Function inserts new row to the table.
//
// #param _tidx Index of the row of the table.
// #param __row - pointer to the row that must be inserted
// #return True when index is ok, otherwise function returns false.
bool XTT_Table::insertRow(int _index, XTT_Row* __row)
{
     // Jezeli ondex wstawiania jest niepoprawny
     if((_index < 0) || (_index > (int)fRowCount))     
          return false;
          
     // Jezeli wszystko ok to:
     
     // Dodajemy wiersz
     AddRow(__row);
     
     // Przesuwamy go na zadana pozycje
     for(int ci=(int)fRowCount-1;ci>_index;ci--)
          ExchangeRows(ci, ci-1);
     
     // Odsiezenie widoku
     TableList->Refresh(TableList->IndexOfID(fTableID));
          
     return true;
}
//---------------------------------------------------------------------------

// #brief Function that inserts the copy of given row to the given place
//
// #param _srcIndex - index of row that will be copied
// #param _desIndex - index of place where a new row will be inserted
// #return true when indexes are ok, otherwise function returns false.
bool XTT_Table::copyRow(int _srcIndex, int _desIndex)
{
     // Jezeli ondex wstawiania jest niepoprawny
     if((_srcIndex < 0) || (_srcIndex >= (int)fRowCount))     
          return false;
     // Jezeli ondex wstawiania jest niepoprawny
     if((_desIndex < 0) || (_desIndex > (int)fRowCount))     
          return false;
          
     return insertRow(_desIndex, fRows[_srcIndex]->copyTo());
}
//---------------------------------------------------------------------------

// #brief Function removes column from the table.
//
// #param _index Index of the column.
// #return True when index is correct, otherwise function returns false.
bool XTT_Table::DeleteCol(unsigned int _index)
{
     // Sprawdzanie indexu
     if((((int)_index) < 0) || (_index >= (unsigned int)ColCount()))
          return false;

     // Zapisanie ilosc kolumn
     int cc = ColCount();
     
     // Cannot remove last column
     if(cc == 1)
          return false;

     // Pomniejszenie kontekstu
     fnContextSize[ColContext(_index)-1]--;
     delete _columns->takeAt(_index);

     // Usuwanie kolumny
     for(unsigned int i=0;i<fRowCount;i++)
          fRows[i]->Delete(_index);

     // Ze sie zmienilo
     fChanged = true;
     
     // Odsiezenie widoku
     TableList->Refresh(TableList->IndexOfID(fTableID));
     
     return true;
}
//---------------------------------------------------------------------------

// #brief Function inserts column of the new context.
//
// #param _ctx Index of the context.
// #return True when column was inserted. False when context of the column already exists.
bool XTT_Table::InsertNewContext(unsigned int _ctx)
{
     // Sprawdzanie indexu
     if((_ctx < 1) || (_ctx > CONTEXT_COUNT))
          return false;

     if(fnContextSize[_ctx-1] > 0)
          return false;

     // Wyszukiwanie indexu wstawienia
     unsigned int _index = 0;
     for(unsigned int i=0;i<(_ctx-1);i++)
          _index += fnContextSize[i];

     // Alokacja pamieci na nowa szerokosc
     _columns->insert(_index, new XTT_Column);
     _columns->at(_index)->setAbstractId(fNextAbstractID++);

     // Dodawanie nowej kolumny
     for(unsigned int i=0;i<fRowCount;i++)
     {
          fRows[i]->Insert(_index, _ctx);
          fRows[i]->Cell(_index)->setAttrType(_columns->at(_index)->attribute());
     }

     // Powiekszenie kontekstu
     fnContextSize[_ctx-1]++;

     // Ze sie zmienilo
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function moves column in other place.
//
// #param from Index of the source column.
// #param to Index fo the destiny column.
// #return True when everything is ok, otherwise function returns false.
bool XTT_Table::Move(unsigned int from, unsigned int to)
{
     // Sprawdzanie indexow
     if((((int)from) < 0) || (from >= (unsigned int)ColCount()))
          return false;
     if((((int)to) < 0) || (to >= (unsigned int)ColCount()))
          return false;
     if(from == to)
          return true;

     // Przyrost
     int dx = 1;
     if(from > to)
          dx = -1;

     // Przenoszenie
     bool ok = true;
     for(unsigned int r=0;r<fRowCount;r++)                               // Dla kazdego wiersza
     {
          // Wskaznik do komorki zrodlowej
          XTT_Cell* ptr = fRows[r]->Cell(from);
          
          for(unsigned int i=from;i!=to;i+=dx)
               ok = ok && fRows[r]->setCell(i, fRows[r]->Cell(i+dx));
          ok = ok && fRows[r]->setCell(to, ptr);
     }
     _columns->swap(from, to);

     return ok;
}
//---------------------------------------------------------------------------

// #brief Function sets context for the column.
//
// #param _cidx Index of the column.
// #param _ctx Context.
// #return No return value.
void XTT_Table::setColContext(unsigned int _col_index, unsigned int _ctx)
{
     // Sprawdzanie indexow
     if((((int)_col_index) < 0) || (_col_index >= (unsigned int)ColCount()))
          return;
     if((_ctx < 1) || (_ctx > CONTEXT_COUNT))
          return;

     fnContextSize[ColContext(_col_index)-1]--;
     fnContextSize[_ctx-1]++;

     for(unsigned int i=0;i<fRowCount;i++)
          fRows[i]->Cell(_col_index)->setContext(_ctx);
}
//---------------------------------------------------------------------------

// #brief Function sets size of the context.
//
// #param _ctx Index of the context.
// #param _size Size of the context.
// #return No return value.
void XTT_Table::setContextSize(unsigned int _ctx, unsigned int _size)
{
     if((_ctx < 1) || (_ctx > CONTEXT_COUNT))
          return;

     fnContextSize[_ctx-1] = _size;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of column.
//
// #param _cid Column id.
// #return Index of the column. When id is incorrect function returns -1.
int XTT_Table::IndexOfColId(unsigned int _a_c_id)
{
     for(int i=0;i<ColCount();i++)
          if(_columns->at(i)->abstractId() == _a_c_id)
               return i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of column that conatin the given cell.
//
// #param __ptr - pointer to the cell.
// #return Index of the column. When table doesn't contain given cell, function returns -1.
int XTT_Table::indexOfCol(XTT_Cell* __ptr)
{
     for(unsigned int i=0;i<RowCount();++i)
     {
          int ri = fRows[i]->indexOfCell(__ptr);
          if(ri > -1)
               return ri;
     }
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves column id.
//
// #param _index Index of the column.
// #return Column id.
int XTT_Table::AbstractColID(unsigned int _index)
{
     if((((int)_index) < 0) || (_index >= (unsigned int)ColCount()))
          return -1;

     return _columns->at(_index)->abstractId();
}
//---------------------------------------------------------------------------

bool XTT_Table::cellRowColumn(XTT_Cell* _cell, unsigned int& _row, unsigned int& _column)
{
    if(!_cell)
        return false;

    for(unsigned int i = 0; i < RowCount(); i++)
    {
        int j = fRows[i]->indexOfCell(_cell);

        if(j >= 0)
        {
            _row = i;
            _column = static_cast<unsigned int>(j);
            return true;
        }
    }

    return false;
}

// #brief Function retrieves index of the table.
//
// #param id Attribute id.
// #return Index of the table. When id is incorrect function returns -1.
int XTT_Table::IndexOfAttributeID(QString id)
{
     for(int i=0;i<ColCount();i++)
          if(_columns->at(i)->attribute()->Id() == id)
               return i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the attribute in given context.
//
// #param _aid Attribute id.
// #param __ctx - context
// #return Index of attribute in the table
int XTT_Table::IndexOfAttributeID(QString _aid, int __ctx)
{
     if((__ctx < 1) || (__ctx > CONTEXT_COUNT))
          return -1;
          
     int start = 0;
     int end = 0;
     for(int i=1;i<__ctx;++i)
          start += ContextSize(i);
     end = start + ContextSize(__ctx);
     
     for(int i=start;i<end;i++)
          if(_columns->at(i)->attribute()->Id() == _aid)
               return i;
      
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function that sorts the rules in the table using given column and criterions
//
// #param __column - index of the column
// #param __ascdesc - determines if the elements should be sorted ascending or descending
// #param __errormsg - reference to the string where error message will be stored
// #return true on suces, otherwise false
bool XTT_Table::sortRules(int __column, int __ascdesc, QString& __errormsg)
{
     // checking if the input parameters are correct
     if((__column < 0) || (__column >= _columns->size()))
          return (__errormsg = "Incorrect column index.") == QString();

     if((__ascdesc != XTT_COLUMN_SORT_ASCENDING) && (__ascdesc != XTT_COLUMN_SORT_DESCENDING))
          return (__errormsg = "Incorrect sorting order identifier") == QString();

     //int sortOrder[] = {H_SET_SORT_ASCENDING, H_SET_SORT_DESCEND};
     QStringList strlist;
     QList<double> numlist;
     hType* parentType = _columns->at(__column)->attribute()->Type();

     // Collecting values
     for(unsigned int i=0;i<RowCount();++i)
     {
          // checking the acces to the cell content
          if(Row(i)->Cell(__column)->Content()->expressionField() == NULL)
               return (__errormsg = "Unable to access cell content in row " + QString::number(i) + ".\nReason: not defined expression.") == QString();
          if(Row(i)->Cell(__column)->Content()->expressionField()->function() == NULL)
               return (__errormsg = "Unable to access cell content in row " + QString::number(i) + ".\nReason: not defined function.") == QString();
          if(Row(i)->Cell(__column)->Content()->expressionField()->function()->argCount() < 2)
               return (__errormsg = "Unable to access cell content in row " + QString::number(i) + ".\nReason: incorrect number of the function arguments.") == QString();
          if(Row(i)->Cell(__column)->Content()->expressionField()->function()->argument(1)->value() == NULL)
               return (__errormsg = "Unable to access cell content in row " + QString::number(i) + ".\nReason: not defined 2nd argument value.") == QString();
          
          // calculating 2nd cell argument
          hMultipleValueResult hmv = Row(i)->Cell(__column)->Content()->expressionField()->function()->argument(1)->value()->value();
          __errormsg = (QString)hmv;
          if(!(bool)hmv)
               return false;
               
          hSet* set = hmv.toSet(true);
          
          int sortcriterion = -1;
          if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MIN_VALUE)
               sortcriterion = H_SET_SORT_FROM_VALUE;
          if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MAX_VALUE)
               sortcriterion = H_SET_SORT_TO_VALUE;
          if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_ALPHABETICAL)
               sortcriterion = H_SET_SORT_ALPHABETICAL;
          
          // Adding set power to the list
          if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_POWER)
               numlist.append(set->power());
               
          // Adding value to the set
          if((_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MIN_VALUE) ||
             (_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MAX_VALUE))
          {
               // When set does not have any element
               if(set->size() == 0)
               {
                    numlist.append((double)NUMERIC_INFIMUM);
                    continue;
               }
               
               // Sorting set
               if(set->sort(sortcriterion, H_SET_SORT_ASCENDING) != H_SET_ERROR_NO_ERROR)
                    return (__errormsg = set->lastError()) == "";
                    
               hValueResult hvr;
               if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MIN_VALUE)
                    hvr = set->at(0)->from();
               if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_MAX_VALUE)
               {
                    if(set->last()->type() == SET_ITEM_TYPE_SINGLE_VALUE)
                         hvr = set->last()->from();
                    if(set->last()->type() == SET_ITEM_TYPE_RANGE)
                         hvr = set->last()->to();
               }
               
               // If an error during calculation
               __errormsg = (QString)hvr;
               if(!(bool)hvr)
                    return false;
               
               // Converting to numeric
               double value;
               int mtnr = parentType->mapToNumeric(__errormsg, value, true);
               if((mtnr == H_TYPE_ERROR_NULL_VALUE) || (mtnr == H_TYPE_ERROR_ANY_VALUE))
               {
                    numlist.append((double)NUMERIC_INFIMUM);
                    continue;
               }
               else if(mtnr != H_TYPE_ERROR_NO_ERROR)
                    return (__errormsg = parentType->lastError()) == QString();
               
               // Adding value to the list
               numlist.append(value);
          }
          
          // Adding string representation of the first item
          if(_columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_ALPHABETICAL)
          {
               // When set does not have any element
               if(set->size() == 0)
               {
                    strlist.append("");
                    continue;
               }
               strlist.append(set->at(0)->strFrom());
          }
          delete set;
     }
     // ok, here values are colleted in one of two above lists.
     // Now it these list must be sorted.
     // Simultaneously when you changes the element position you should change the row position

     // sorting list
     bool text_data_type = _columns->at(__column)->sortCriterion() == XTT_COLUMN_SORT_ALPHABETICAL;
     int listsize = numlist.size();
     if(text_data_type)
          listsize = strlist.size();
/*QMessageBox::critical(NULL, "Error localization", "listsize="+QString::number(listsize), QMessageBox::Ok);
if(text_data_type)
     QMessageBox::critical(NULL, "Error localization", strlist.join("\n"), QMessageBox::Ok);*/
     for(int i=0;i<listsize;++i)
     {
          for(int j=0;j<listsize-i-1;++j)
          {
               if(text_data_type)
               {
                    if((__ascdesc == XTT_COLUMN_SORT_ASCENDING) && (strlist.at(j) > strlist.at(j+1)))
                    {
                         strlist.swap(j ,j+1);
                         ExchangeRows(j, j+1);
                    }
                    else if((__ascdesc == XTT_COLUMN_SORT_DESCENDING) && (strlist.at(j) < strlist.at(j+1)))
                    {
                         strlist.swap(j ,j+1);
                         ExchangeRows(j, j+1);
                    }
               }
               
               else if(!text_data_type)
               {
                    if((__ascdesc == XTT_COLUMN_SORT_ASCENDING) && (numlist.at(j) > numlist.at(j+1)))
                    {
                         numlist.swap(j ,j+1);
                         ExchangeRows(j, j+1);
                    }
                    else if((__ascdesc == XTT_COLUMN_SORT_DESCENDING) && (numlist.at(j) < numlist.at(j+1)))
                    {
                         numlist.swap(j ,j+1);
                         ExchangeRows(j, j+1);
                    }
               }
          }
     }

     return true;
}
//---------------------------------------------------------------------------

// #brief Function exchanges two rows.
//
// #param _index1 Index of the firs row.
// #param _index2 Index of the second row.
// #param _keepConnections Determines whether connections should be refreshed.
// #return True when indexes are ok, otherwise function returns false.
bool XTT_Table::ExchangeRows(unsigned int _index1, unsigned int _index2, bool _keepConnections)
{
     if((((int)_index1) < 0) || (_index1 >= fRowCount))
          return false;
     if((((int)_index2) < 0) || (_index2 >= fRowCount))
          return false;
     
     // Zamiana wskaznikow do wierszy
     XTT_Row* tmp = fRows[_index1];
     fRows[_index1] = fRows[_index2];
     fRows[_index2] = tmp;
     
     // Zamiana wysokosci wierszy
     unsigned int tmph = fHeight[_index1];
     fHeight[_index1] = fHeight[_index2];
     fHeight[_index2] = tmph;
     
     // Jezeli nalezy odswiezyc polaczenia
     if(_keepConnections)
          ConnectionsList->RepaintConnectionRelatedWithTable(fTableID);
     
     // Sprawdzenie poprawnosci polaczen
     connectionsDiagnostics();
     
     // Odswiezenie widoku tabeli
     fGTable->Update();
     fGTable->RePosition();

     return true;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the firs row that has any connections.
//
// #return Index of the firs row that has any connections.
int XTT_Table::firstConnectionRowIndex(void)
{     
     int res = -1;
     
     QList<QString>* cl = ConnectionsList->IndexOfInConnections(fTableID, "");
     if(cl->size() > 0)
     {
          delete cl;
          return 0;
     }
     delete cl;
     
     for(unsigned int i=0;i<fRowCount;i++)
     {
          QList<XTT_Connections*>* conn_list = Row(i)->InConnection();
          if(conn_list->size() > 0)
               res = i;
          delete conn_list;
          
          if(res > -1)
               return res;
     }
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the row.
//
// #param _rid Row id.
// #return Index of the row.
int XTT_Table::indexOfRowID(QString _id)
{
     for(unsigned int i=0;i<fRowCount;++i)
          if(fRows[i]->RowId() == _id)
               return i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the row.
//
// #param __ptr - pointer to the row
// #return Index of the row, if row not exists returns -1.
int XTT_Table::indexOfRow(XTT_Row* __ptr)
{
     for(unsigned int i=0;i<fRowCount;++i)
          if(fRows[i] == __ptr)
               return i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the row that contains the given cell.
//
// #param __ptr - pointer to the cell
// #return Index of the row.
int XTT_Table::indexOfRow(XTT_Cell* __ptr)
{
     for(unsigned int i=0;i<fRowCount;++i)
          if(fRows[i]->indexOfCell(__ptr) > -1)
               return i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of input connections of the table.
//
// #return Pointer to the list of input connections of the table.
QList<XTT_Connections*>* XTT_Table::inConnectionsList(void)
{
     return ConnectionsList->listOfInConnections(fTableID);
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the list of output connections of the table.
//
// #return Pointer to the list of output connections of the table.
QList<XTT_Connections*>* XTT_Table::outConnectionsList(void)
{
     return ConnectionsList->listOfOutConnections(fTableID);
}
//---------------------------------------------------------------------------

// #brief Function that creates the list of rows pointers
//
// #return pointer to the list of rows pointers.
QList<XTT_Row*>* XTT_Table::rowList(void)
{
     QList<XTT_Row*>* result = new QList<XTT_Row*>;
     for(unsigned int i=0;i<RowCount();++i)
          result->append(Row(i));
          
     return result;
}
//---------------------------------------------------------------------------

// #brief Function checks correctness of connections of the table.
//
// #param *__error_msgs - pointer to the string where the error messages will be stored
// #return true if ok, otherwise false
bool XTT_Table::connectionsDiagnostics(QString* __error_msgs)
{
     bool res = true;
     QString errors = "";

     // Okresla czy wsrod polaczen przychodzacych znajduje sie chociaz jedno polaczenie nie etykietowane, jezeli tak
     // to wszystkie polaczenia bez wzgledu na etykiete moga zostac uruchomione
     bool existsNotLabeled = false;

     // Lista wszystkich wchodzacych polaczen
     QList<XTT_Connections*>* _iclist = new QList<XTT_Connections*>;
     QList<XTT_Connections*>* headerconnections = ConnectionsList->_IndexOfInConnections(fTableID, "");
     if(!headerconnections->empty())
          *_iclist << *headerconnections;
     delete headerconnections;

     // Lista polaczen znalezionych w danej iteracji
     QList<XTT_Connections*>* currentRowInConnections = NULL;
     QList<XTT_Connections*>* currentRowOutConnections = NULL;

     for(unsigned int i=0;i<RowCount();++i)
     {
          // Continue Main Loop
          bool cml = false;

          // Wyszukanie listy polaczen przychodzacych do aktualnego wiersza
          currentRowInConnections = Row(i)->InConnection();
          currentRowOutConnections = Row(i)->OutConnection();

          (*_iclist) << (*currentRowInConnections);

          // Jezeli tabela jest tabela incjalizacyjna to z danego wiersza moze wychodzic
          // conajwyzej jedno polaczenie, w innym przypadku nie wiadomo ktore wybrac
          if((fTableType == 0) && (currentRowOutConnections->size() > 1))
          {
               QString _Error = "Ambiguous connections. The row can has only one outgoing connection.";
               if(Row(i)->Length() > 0)
                    errors += hqed::createErrorString(fTableID, Row(i)->Cell(Row(i)->Length()-1)->CellId(), _Error, WARNING_TYPE, 1);
                    
               res = false;
               cml = true;
          }

          // Jezeli nie ma jeszcze zadnych polaczen wchodzacych a istneija polaczenia wychodzace
          // to jezeli tabela jest inicjalizacyjna to ok
          if((fTableType == 0) && (currentRowOutConnections->size() == 1))
               cml = true;

          // Sprawdzanie czy znajduje sie polaczenie nie etykietowane, jezeli tak to nie ma sensu
          // dalej sprawdzac pod tym katem
          for(int ic=0;(ic<currentRowInConnections->size()) && (!existsNotLabeled) && (!cml);++ic)
               if(currentRowInConnections->at(ic)->Label() == "")
                    existsNotLabeled = true;

          // Jezeli istnieja polaczenia nie etykietowane, oraz z ktoregos wiersza wychodzi wiecej niz jedno polaczenie
          // to w przypadku uzycia takiego polaczenia nie etykietowanego nie jest wiadomo ktore z tych wychodzacych
          // polaczen trzeba uzyc
          if((existsNotLabeled) && (currentRowOutConnections->size() > 1))
          {
               QString _Error = "Ambiguous connections, which one should be used ? Check not labeled connections.";
               if(Row(i)->Length() > 0)
                    errors += hqed::createErrorString(fTableID, Row(i)->Cell(Row(i)->Length()-1)->CellId(), _Error, WARNING_TYPE, 1);
                         
               res = false;
               cml = true;
          }

          // Jezeli istneije polaczenie bez etykiety to nie ma sensu sprawdzac dalej bool
          // kazde polaczenie ma szanse byc uruchomione
          if(existsNotLabeled)
               continue;

          /*
          // Sprawdzanie polaczen, czy kazde moze byc uruchomione
          for(int c=0;c<currentRowOutConnections->size() && (!existsNotLabeled) && (!cml);++c)
          {
               // Jezeli jest to polaczenie nie etykietowane to moze byc uzyte 
               // bez wzgledu na etykiete polaczenia wejsciowego
               if(currentRowOutConnections->at(c)->Label() == "")
                    cml = true;
                    
               // Jezeli jest to polaczenie etykietowane to sprawdzamy czy istneije polaczenie 
               // przychodzace o tej etykiecie i nie jest to to samo polaczenie (loop)
               for(int ini=0;ini<_iclist->size();++ini)
                    if((_iclist->at(ini) != currentRowOutConnections->at(c)) && (_iclist->at(ini)->Label() == currentRowOutConnections->at(c)->Label()))
                         cml = true;
                         
               if(!cml)
               {
                    QString _Error = "Probably impossible execution of connection \"" + currentRowOutConnections->at(c)->Label() + "\".";
                    if(Row(i)->Length() > 0)
                         MainWin->xtt_errorsList->add(fTableID, Row(i)->Cell(Row(i)->Length()-1)->CellId(), _Error, WARNING_TYPE, 1);
               }
          }*/

          delete currentRowInConnections;
          delete currentRowOutConnections;
     }
     
     if(__error_msgs != NULL)
          *__error_msgs += errors;
     
     delete _iclist;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that returns if the row object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_Table::tableVerification(QString& __error_message)
{
     bool res = true;
     
     // Checking the compatybility of an attribute class with the column context
     QList<XTT_Attribute*>* cca = ContextAtributes(1);
     QList<XTT_Attribute*>* dca = ContextAtributes(4);
     
     // The initial table can't have conditional part
     if((cca->size() > 0) && (TableType() == 0))
     {
          QString _Error = "A state table cannot have the conditional part.";
          __error_message += hqed::createErrorString(fTableID, "", _Error, ERROR_TYPE, 3);
          res = false;
     }
     
     // The initial table can have only one row
     if(RowCount() == 0)
     {
          QString _Error = "Each XTT table must have defined at least one row.";
          __error_message += hqed::createErrorString(fTableID, "", _Error, ERROR_TYPE, 3);
          res = false;
     }
     
     // The initial table can have only one row
     if((RowCount() > 1) && (TableType() == 0))
     {
          QString _Error = "A state table can have only one row.";
          __error_message += hqed::createErrorString(fTableID, "", _Error, WARNING_TYPE, 3);
          res = false;
     }
     
     for(int i=0;i<cca->size();++i)
     {
          // The conditional context can not contain the WriteOlny WO attribute
          if(cca->at(i)->Class() == XTT_ATT_CLASS_WO)
          {
               QString _Error = "Incorrect use attribute \'" + cca->at(i)->Name() + "\' in conditional context. The " + XTT_Attribute::classString(XTT_ATT_CLASS_WO) + " attribute can not be used in this context.";
               __error_message += hqed::createErrorString(fTableID, QString::number(i), _Error, WARNING_TYPE, 4);
               res = false;
          }
     }
     
     for(int i=0;i<dca->size();++i)
     {
          // The action context can not contain the ReadOlny RO attribute at least this is initial table
          if((dca->at(i)->Class() == XTT_ATT_CLASS_RO) && (fTableType != 0))
          {
               QString _Error = "Incorrect use attribute \'" + dca->at(i)->Name() + "\' in decision context. The " + XTT_Attribute::classString(XTT_ATT_CLASS_RO) + " attribute can not be used in this context.";
               __error_message += hqed::createErrorString(fTableID, QString::number(cca->size()+i), _Error, WARNING_TYPE, 4);
               res = false;
          }
     }
     
     delete cca;
     delete dca;

     for(unsigned int a=0;a<fRowCount;a++)
          res = fRows[a]->rowVerification(__error_message) && res;

     res = connectionsDiagnostics(&__error_message) && res;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_Table::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     for(unsigned int a=0;a<fRowCount;a++)
          res = fRows[a]->updateTypePointer(__old, __new) && res;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Table::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fRowCount;a++)
               res = fRows[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fRowCount;a++)
               res = fRows[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the rows to the prolog code.
//
// #return The prolog code that represents the rules as string.
QString XTT_Table::rulesToProlog(void)
{    
     QString result = "";
     
     for(unsigned int r=0;r<fRowCount;++r)
          result += fRows[r]->toProlog() + "\n";
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Function that maps the table schema to the prolog code.
//
// #return The prolog code that represents the schema of the table as string.
QString XTT_Table::schemaToProlog(void)
{
     QString result = "";
     QString condSchema = "";
     QString actnSchema = "";
     
     // creating schema
     QList<XTT_Attribute*>* ac = ContextAtributes(1, true);
     QList<XTT_Attribute*>* cc = ContextAtributes(4, true);
     
     for(int i=0;i<ac->size();++i)
     {
          condSchema += hqed::mcwp(ac->at(i)->Path());
          if(i < (ac->size()-1))
               condSchema += ",";
     }
     for(int i=0;i<cc->size();++i)
     {
          actnSchema += hqed::mcwp(cc->at(i)->Path());
          if(i < (cc->size()-1))
               actnSchema += ",";
     }
     
     delete ac;
     delete cc;
     
     // creating string schema
     result += "xschm " + hqed::mcwp(Title()) + ": ";
     result += "[" + condSchema + "] ==> [" + actnSchema + "].";
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_Table::idPrefix(void)
{
     return "tab_";
}
//---------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 1.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returns false.
bool XTT_Table::FromXTTML_2_0(QDomElement& root)      
{
     // Resetowanie ilosci kontekstow
     fnContextSize[0] = 0;
     fnContextSize[1] = 0;
     fnContextSize[2] = 0;
     fnContextSize[3] = 0;

     bool res = true;
     
     if(root.tagName().toLower() != "xtt_table")
          return false;

     // Reszta zakladamy ze jest ok
     
     QStringList tableTypes;
     tableTypes << "initial" << "middle" << "halt";

     // Okresla ktra wartosc wczytujemy z wartosci indexowanych, jak np fContextAtributes, lub fnContextSize
     int index;

     // Wczytywanie parametrow tagu glownego
     QString tableType = root.attribute("table_type", "#NAN#");
     QString tableTitle = root.attribute("name", "#NAN#");
     
     // Usuwanie spacji z wczytanych wartosci
     for(;tableType.contains(" ");)
          tableType = tableType.remove(tableType.indexOf(" "), 1);
     for(;tableTitle.contains(" ");)
          tableTitle = tableTitle.remove(tableTitle.indexOf(" "), 1);
     
     // Oba parametry sa wymagane wiec sprawdzanie czy wystepuja
     if((tableType == "") || (tableType == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing table parameter \"table_type\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     
     // Jezeli okreslono typ tabeli to trzeba sprawdzic czy poprawnie
     if(!tableTypes.contains(tableType, Qt::CaseInsensitive))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect table type: \"" + tableType + "\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     
     // Sprawdzanie podania nazwy tabeli
     if((tableTitle == "") || (tableTitle == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing table parameter \"name\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     
     fTitle = tableTitle;
     fTableType = tableTypes.indexOf(tableType.toLower());
     
     // "<row"
     QDomElement DOMrow = root.firstChildElement("row");
     while(!DOMrow.isNull())
     {
          // Kiedy linia konczaca linie danych
          // Dodajemy nowy wiersz
          AddRow();

          // Wczytujemy dane z pliku
          res = res && fRows[fRowCount-1]->FromXTTML_2_0(DOMrow);
          
          // Wczytanie kolejnego wiersza
          DOMrow =  DOMrow.nextSiblingElement("row");
     }

     // "<description>"
     QString tDesc = root.firstChildElement("description").text();
     if(!tDesc.isEmpty())
          fDescription = tDesc;

     // "<context_size"
     QDomElement DOMcs = root.firstChildElement("context_size");
     if(!DOMcs.isNull())
     {
          QString sConditional = DOMcs.attribute("conditional", "#NAN#");
          QString sAssert = DOMcs.attribute("assert", "#NAN#");
          QString sRetract = DOMcs.attribute("retract", "#NAN#");
          QString sAction = DOMcs.attribute("action", "#NAN#");
          
          // Podanie nie jest wymagane, jezeli parametr nie jest podany to przyjmujemy wielkosc za 0
          // Usuwanie spacji z wczytanych wartosci
          for(;sConditional.contains(" ");)
               sConditional = sConditional.remove(sConditional.indexOf(" "), 1);
          for(;sAssert.contains(" ");)
               sAssert = sAssert.remove(sAssert.indexOf(" "), 1);
          for(;sRetract.contains(" ");)
               sRetract = sRetract.remove(sRetract.indexOf(" "), 1);
          for(;sAction.contains(" ");)
               sAction = sAction.remove(sAction.indexOf(" "), 1);
               
          int iConditional = 0;
          int iAssert = 0;
          int iRetract = 0;
          int iAction = 0;
               
          // Proby konwersji jezeli podano
          if((sConditional != "") && (sConditional != "#NAN#"))
          {
               bool ok;
               iConditional = sConditional.toInt(&ok);
               if((!ok) || (iConditional < 0))
               {
                    QMessageBox::information(NULL, "HQEd", "Incorrect table \"" + fTitle + "\" conditional context size: \"" + sConditional + "\" at line " + QString::number(DOMcs.lineNumber()) + ".\nContext will be set as zero.", QMessageBox::Ok);
                    iConditional = 0;
               }
          }
          if((sAssert != "") && (sAssert != "#NAN#"))
          {
               bool ok;
               iAssert = sAssert.toInt(&ok);
               if((!ok) || (iAssert < 0))
               {
                    QMessageBox::information(NULL, "HQEd", "Incorrect table \"" + fTitle + "\" assert context size: \"" + sAssert + "\" at line " + QString::number(DOMcs.lineNumber()) + ".\nContext will be set as zero.", QMessageBox::Ok);
                    iAssert = 0;
               }
          }
          if((sRetract != "") && (sRetract != "#NAN#"))
          {
               bool ok;
               iRetract = sRetract.toInt(&ok);
               if((!ok) || (iRetract < 0))
               {
                    QMessageBox::information(NULL, "HQEd", "Incorrect table \"" + fTitle + "\" retract context size: \"" + sRetract + "\" at line " + QString::number(DOMcs.lineNumber()) + ".\nContext will be set as zero.", QMessageBox::Ok);
                    iRetract = 0;
               }
          }
          if((sAction != "") && (sAction != "#NAN#"))
          {
               bool ok;
               iAction = sAction.toInt(&ok);
               if((!ok) || (iAction < 0))
               {
                    QMessageBox::information(NULL, "HQEd", "Incorrect table \"" + fTitle + "\" action context size: \"" + sAction + "\" at line " + QString::number(DOMcs.lineNumber()) + ".\nContext will be set as zero.", QMessageBox::Ok);
                    iAction = 0;
               }
          }
          
          fnContextSize[0] = iConditional;
          fnContextSize[1] = iAssert;
          fnContextSize[2] = iRetract;
          fnContextSize[3] = iAction;
          
          // Suma kontekstow jest rowna ilosci kolumn, teraz sprawdzamy czy tak jest rzeczywiscie
          // NAJPIERW MUSZA ZOSTAC WCZYTANE WIERSZE     
          for(int r=0;r<(int)fRowCount;r++)
          {
               if(((int)fRows[r]->Length()) != ColCount())
               {
                    //QMessageBox::critical(NULL, "HQEd", "To long/short row " + QString::number(r+1) + ". Row length was declared as " + QString::number(ColCount()) + ". Error in table \"" + fTitle + "\".", QMessageBox::Ok);
                    return false;
               }
          }
          
          // Jezeli wszystko ok to przystepujemy do alokacji pamieci na pozostale parametry
          // Ustawianie wartosci domyslnych kolumn i ich kontekstow
          for(int i=0;i<ColCount();i++)
          {
               _columns->append(new XTT_Column);
               _columns->last()->setAbstractId(fNextAbstractID++);
               setColContext(i, ColContext(i));
          }
     }

     // "<column_attributes>"
     QDomElement DOMcolattrs = root.firstChildElement("column_attributes");
     QDomElement DOMcolwidths = root.firstChildElement("col_widths");
     QDomElement DOMrowheights = root.firstChildElement("nrow_heights");

     // Wczytanie atrybutow dla kolumn
     index = 0;
     QDomElement DOMca = DOMcolattrs.firstChildElement("ca");
     while(!DOMca.isNull())
     {
          // Sprawdzanie czy zgadza sie ilosc kolumn w kazdym wierszu z suma wielkosci kontekstow
          int ctx_sum = ColCount();
          if(index >= ctx_sum)
          {
               QMessageBox::information(NULL, "HQEd", "Too many columns are defined. Error at line " + QString::number(DOMca.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          QString path = DOMca.text();
          XTT_Attribute* attr = AttributeGroups->Attribute(path);
          if(attr == NULL)
          {
               QMessageBox::critical(NULL, "HQEd", "Undefined attribute: \"" + path + "\" at line " + QString::number(DOMca.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli odnajdziemy dany atrybut to mozemy go dodac
          _columns->at(index)->setAttribute(attr);
          
          // Oraz musimy go zapisac w kazdej komorce
          for(unsigned int r=0;r<fRowCount;r++)
               fRows[r]->Cell(index)->setAttrType(attr);
               
          // Powiekszenie indexu wczytywanej wartosci
          index++;
          
          // Wczytanie kolejengo atrybutu
          DOMca = DOMca.nextSiblingElement("ca");
     }

     // Wczytywanie rozmiarow kolumn
     index = 0;
     QDomElement DOMcw = DOMcolwidths.firstChildElement("cw");
     while(!DOMcw.isNull())
     {
          // Mozliwe jest podanie mniejszej ilosci szerokosci niz jest zadeklarowanych kolumn, wtedy szerokosc bedzie ustawiona
          // w pierszych kolumnach od prawej a pozostale szerokosci beda ustawione na 120. Podanie zbyt duzej ilosci wymiarow
          // spowoduje pominiecie tych ostatnich pozycji ktore sa nadmiarowe
          
          
          // Sprawdzanie czy zgadza sie ilosc kolumn w kazdym wierszu z suma wielkosci kontekstow
          int ctx_sum = ColCount();
          if(index >= ctx_sum)
          {
               QMessageBox::information(NULL, "HQEd", "Too many widths of columns are defined. Error at line " + QString::number(DOMcw.lineNumber()) + ".\nThis value wiil be ignored.", QMessageBox::Ok);
               continue;
          }
     
          bool ok_conv;
          QString l = DOMcw.text();
          unsigned int cw = (unsigned int)l.toInt(&ok_conv);
          
          if(!ok_conv)
          {
               QMessageBox::information(NULL, "HQEd", "Incorrect column " + QString::number(index+1) + " width. Value \"" + l + "\" is not integer type. Error at line " + QString::number(DOMcw.lineNumber()) + ".\nWidth will be set as default.", QMessageBox::Ok);
               cw = Settings->xttDefaultColumnWidth();
          }
          
          if(((int)cw) < 0)
          {
               QMessageBox::information(NULL, "HQEd", "Incorrect column " + QString::number(index+1) + " width. Value \"" + l + "\"  can't be less than zero. Error at line " + QString::number(DOMcw.lineNumber()) + ".\nWidth will be set as default.", QMessageBox::Ok);
               cw = Settings->xttDefaultColumnWidth();
          }
          
          _columns->at(index)->setWidth(cw);
          index++;
     
          // Wczytanie kolejengo rozmiaru dla kolumny
          DOMcw = DOMcw.nextSiblingElement("cw");
     }

     // Wczytywanie rozmiarow wierszy
     index = 0;
     QDomElement DOMrh = DOMrowheights.firstChildElement("rh");
     while(!DOMrh.isNull())
     {
          // Sprawdzanie czy liczba wierszy nie zostala przekroczona
          if((unsigned int)index >= fRowCount)
          {
               QMessageBox::information(NULL, "HQEd", "Too many heights of row are defined. Error at line " + QString::number(DOMrh.lineNumber()) + ".\nThis value wiil be ignored.", QMessageBox::Ok);
               continue;
          }
          
          bool ok_conv;
          QString l = DOMrh.text();
          unsigned int rh = (unsigned int)l.toInt(&ok_conv);
          
          if(!ok_conv)
          {
               QMessageBox::information(NULL, "HQEd", "Incorrect row " + QString::number(index+1) + " height. Value \"" + l + "\" is not integer type. Error at line " + QString::number(DOMrh.lineNumber()) + ".\nHeight will be set as 10.", QMessageBox::Ok);
               rh = Settings->xttDefaultRowHeight();
          }
          
          if(((int)rh) < 0)
          {
               QMessageBox::information(NULL, "HQEd", "Incorrect row " + QString::number(index+1) + " height. Value \"" + l + "\" can't be less than zero. Error at line " + QString::number(DOMrh.lineNumber()) + ".\nHeight will be set as 10.", QMessageBox::Ok);
               rh = Settings->xttDefaultRowHeight();
          }
          
          fHeight[index] = rh;
          index++;
     
          // Wczytanie kolejengo rozmiaru dla wiersza
          DOMrh = DOMrh.nextSiblingElement("rh");
     }

     // "<localization"
     QDomElement DOMlocalization = root.firstChildElement("localization");
     if(!DOMlocalization.isNull())
     {
          QString sXPos = DOMlocalization.attribute("xpos", "#NAN#");
          QString sYPos = DOMlocalization.attribute("ypos", "#NAN#");
          QString sHHeight = DOMlocalization.attribute("header_height", "#NAN#");
          QString sLevel = DOMlocalization.attribute("level", "#NAN#");
          QString sParent = DOMlocalization.attribute("parent", "#NAN#");
          
          // Podanie nie jest wymagane, jezeli parametr nie jest podany to przyjmujemy wielkosc za 0
          // Usuwanie spacji z wczytanych wartosci
          for(;sXPos.contains(" ");)
               sXPos = sXPos.remove(sXPos.indexOf(" "), 1);
          for(;sYPos.contains(" ");)
               sYPos = sYPos.remove(sYPos.indexOf(" "), 1);
          for(;sHHeight.contains(" ");)
               sHHeight = sHHeight.remove(sHHeight.indexOf(" "), 1);
          for(;sLevel.contains(" ");)
               sLevel = sLevel.remove(sLevel.indexOf(" "), 1);
          for(;sParent.contains(" ");)
               sParent = sParent.remove(sParent.indexOf(" "), 1);
               
          // Parametry nie sa wymagane, jezeli nie zostana podane to zostana im
          // przypisane nastpujace wartosci domyslne
          int iXPos = 0;
          int iYPos = 0;
          int iHHeight = 20;
          int iLevel = 0;
          int iParent = -1;
               
          // Proby konwersji jezeli podano
          if((sXPos != "") && (sXPos != "#NAN#"))
          {
               bool ok;
               iXPos = sXPos.toInt(&ok);
               if(!ok)
                    QMessageBox::information(NULL, "HQEd", "Incorrect value type. Value \"" + sXPos + "\" is not integer type. Error at line " + QString::number(DOMlocalization.lineNumber()) + ".", QMessageBox::Ok);
               fPoint.setX(iXPos);
          }
          if((sYPos != "") && (sYPos != "#NAN#"))
          {
               bool ok;
               iYPos = sYPos.toInt(&ok);
               if(!ok)
                    QMessageBox::information(NULL, "HQEd", "Incorrect value type. Value \"" + sYPos + "\" is not integer type. Error at line " + QString::number(DOMlocalization.lineNumber()) + ".", QMessageBox::Ok);
               fPoint.setY(iYPos);
          }
          if((sHHeight != "") && (sHHeight != "#NAN#"))
          {
               bool ok;
               iHHeight = sHHeight.toInt(&ok);
               if(!ok)
                    QMessageBox::information(NULL, "HQEd", "Incorrect value type. Value \"" + sHHeight + "\" is not integer type. Error at line " + QString::number(DOMlocalization.lineNumber()) + ".", QMessageBox::Ok);
               fHeaderHeight = (unsigned int)iHHeight;
          }
          if((sLevel != "") && (sLevel != "#NAN#"))
          {
               bool ok;
               iLevel = sLevel.toInt(&ok);
               if(!ok)
                    QMessageBox::information(NULL, "HQEd", "Incorrect value type. Value \"" + sLevel + "\" is not integer type. Error at line " + QString::number(DOMlocalization.lineNumber()) + ".", QMessageBox::Ok);
               fLevel = iLevel;
          }
          if((sParent != "") && (sParent != "#NAN#"))
          {
               bool ok;
               iParent = sParent.toInt(&ok);
               if(!ok)
                    QMessageBox::information(NULL, "HQEd", "Incorrect value type. Value \"" + sParent + "\" is not integer type. Error at line " + QString::number(DOMlocalization.lineNumber()) + ".", QMessageBox::Ok);
               fParent = iParent;
          }
     }

     // Po wczytaniu wszystkiego - weryfikacja wszystkich ustawien
     // Sprawdzanie czy zgadza sie ilosc kolumn w kazdym wierszu z suma wielkosci kontekstow
     int ctx_sum = ColCount();
     
     for(unsigned int r=0;r<fRowCount;r++)
     {
          if((unsigned int)ctx_sum != fRows[r]->Length())
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect col count in row " + QString::number(r+1) + ". Column count must be equal sum of all contexts size. Error at line: " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli wiersz jest odpowiednio dlugi to musimy przeanalizowac komorki
          for(unsigned int c=0;c<fRows[r]->Length();c++)
          {      
               // Zapisanie wartosci jezeli komorka jest pusta
               QString curr_content = fRows[r]->Cell(c)->strContent();
               if((!fRows[r]->Cell(c)->Analyze(curr_content)) && (Settings->xttShowErrorCellMessage()))
                    QMessageBox::warning(NULL, "HQEd", fRows[r]->Cell(c)->CellError(), QMessageBox::Ok);
                    
          }
     }

     // Odswiezenie widoku
     delete fGTable;
     fGTable = new GTable(xtt::modelScene(), this);
     fGTable->ReCreate();
     fGTable->Update();
     fGTable->RePosition();

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 format.
//
// #param _csvdoc - writing device
// #return List of all data in drools5 format.
void XTT_Table::out_Drools_5_0_DecTab(QTextStream* _csvdoc)
{
     *_csvdoc << ",,\"RuleTable " << Title() << "\"\n";

     // ---------- Table schema -----------------------------------------------

     QStringList header1;
     QStringList header2;
     QStringList header3;

     // Conditions
     QList<XTT_Attribute*>* cca = ContextAtributes(1, true);

     header1 << "\"CONDITION\"";
     header2 << "";
     header3 << "\"w:Workspace()\"";

     for(int i=0;i<cca->size();++i)
     {
          header1 << "\"CONDITION\"";
          header2 << "";
          header3 << "\"eval(WorkspaceHelper.Compare(w.get" + cca->at(i)->CapitalizedName() + "(),\"\"$1\"\",\"\"$2\"\") == true)\"";
     }

     // Actions
     QList<XTT_Attribute*>* dca = ContextAtributes(4, true);
     QList<XTT_Attribute*>* aca = ContextAtributes(5, true);
     *dca += *aca;

     for(int i=0;i<dca->size();++i)
     {
          header1 << "\"ACTION\"";
          header2 << "";
          header3 << "\"w.set" + dca->at(i)->CapitalizedName() + "($param);update(w);\"";
     }

     header1 << "\"UNLOOP\"";
     header1 << "\"RuleSet\"";

     *_csvdoc << ",," << header1.join(",") << "\n";
     *_csvdoc << ",," << header2.join(",") << "\n";
     *_csvdoc << ",," << header3.join(",") << "\n";
     *_csvdoc << "\n";

     // Rows
     for(unsigned int i=0;i<RowCount();++i)
     {
          Row(i)->out_Drools_5_0_DecTab(_csvdoc);
          *_csvdoc << "\"" << Title() << "\",\n";
     }
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Table::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(__mode != 4)
     {
          *res << "<table id=\"" + TableId() + "\" name=\"" + Title() + "\">";

          if(!fDescription.isEmpty())
               *res << "\t<desc>" + fDescription + "</desc>";
               
               // Saving table schema
               *res << "\t<schm>";
                    QList<XTT_Attribute*>* cca = ContextAtributes(1, true);
                    if(cca->size() > 0)
                    {
                         *res << "\t\t<precondition>";
                         for(int i=0;i<cca->size();++i)
                              *res << "\t\t\t<attref ref=\"" + cca->at(i)->Id() + "\"/>";
                         *res << "\t\t</precondition>";
                    }
                    
                    QList<XTT_Attribute*>* dca = ContextAtributes(4, true);
                    QList<XTT_Attribute*>* aca = ContextAtributes(5, true);
                    *dca += *aca;            // merging list
                    if(dca->size() > 0)
                    {
                         *res << "\t\t<conclusion>";
                         for(int i=0;i<dca->size();++i)
                              *res << "\t\t\t<attref ref=\"" + dca->at(i)->Id() + "\"/>";
                         *res << "\t\t</conclusion>";
                    }
                    
               *res << "\t</schm>";
               
               // Saving rules
               for(unsigned int i=0;i<RowCount();++i)
               {
                    QStringList* tmp = Row(i)->out_HML_2_0(__mode);
                    for(int j=0;j<tmp->size();++j)
                         *res << "\t" + tmp->at(j);
                    delete tmp;
               }
          
          *res << "</table>";
     }
     
     if(__mode == 4)
     {
          // Saving rules
          for(unsigned int i=0;i<RowCount();++i)
          {
               QStringList* tmp = Row(i)->out_HML_2_0(__mode);
               for(int j=0;j<tmp->size();++j)
                    *res << "\t" + tmp->at(j);
               delete tmp;
          }
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300

// #brief Function saves all data into Drools 5.0 format.
// #author Lukasz Lysik (llysik@gmail.com)
//
// #param _xmldoc Pointer to document.
// #return No return value.
void XTT_Table::out_Drools_5_0(QXmlStreamWriter* _xmldoc)
{
     _xmldoc->writeStartElement("ruleSet");
     _xmldoc->writeAttribute("id",QString::number(DfId()));
     _xmldoc->writeAttribute("inConnCount",QString::number(ConnectionsList->listOfInConnections_Table(TableId())->count()));
     _xmldoc->writeAttribute("outConnCount",QString::number(ConnectionsList->listOfOutConnections_Table(TableId())->count()));
     _xmldoc->writeAttribute("name", "tab_" + Title());
     _xmldoc->writeAttribute("x","0");
     _xmldoc->writeAttribute("y","0");
     _xmldoc->writeAttribute("width","80");
     _xmldoc->writeAttribute("height","48");
     _xmldoc->writeAttribute("ruleFlowGroup", "group_" + Title());
     _xmldoc->writeEndElement();   // ruleSet
}
// -----------------------------------------------------------------------------

// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_Table::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(__mode != 4)
     {
          _xmldoc->writeStartElement("table");
          _xmldoc->writeAttribute("id", TableId());
          _xmldoc->writeAttribute("name", Title());

          if(!fDescription.isEmpty())
               _xmldoc->writeTextElement("desc", fDescription);
               
               // Saving table schema
               _xmldoc->writeStartElement("schm");
                    QList<XTT_Attribute*>* cca = ContextAtributes(1, true);
                    if(cca->size() > 0)
                    {
                         _xmldoc->writeStartElement("precondition");
                         for(int i=0;i<cca->size();++i)
                         {
                              _xmldoc->writeEmptyElement("attref");
                              _xmldoc->writeAttribute("ref", cca->at(i)->Id());
                         }
                         _xmldoc->writeEndElement();   // precondition
                    }
                    
                    QList<XTT_Attribute*>* dca = ContextAtributes(4, true);
                    QList<XTT_Attribute*>* aca = ContextAtributes(5, true);
                    *dca += *aca;            // merging list
                    if(dca->size() > 0)
                    {
                         _xmldoc->writeStartElement("conclusion");
                         for(int i=0;i<dca->size();++i)
                         {
                              _xmldoc->writeEmptyElement("attref");
                              _xmldoc->writeAttribute("ref", dca->at(i)->Id());
                         }
                         _xmldoc->writeEndElement();   // conclusion
                    }
                    
               _xmldoc->writeEndElement();   // schm
               
               // Saving rules
               for(unsigned int i=0;i<RowCount();++i)
                    Row(i)->out_HML_2_0(_xmldoc, __mode);
          
          _xmldoc->writeEndElement();   // table
     }
     
     if(__mode == 4)
     {
          // Saving rules
          for(unsigned int i=0;i<RowCount();++i)
               Row(i)->out_HML_2_0(_xmldoc, __mode);
     }
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_Table::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "table";
     
     QDomElement DOMtable = __root;
     if(DOMtable.tagName().toLower() != elementName.toLower())
          return 0;
          
     // Reading table data
     QString tid = DOMtable.attribute("id", "");
     QString tname = DOMtable.attribute("name", "");
     QDomElement DOMdesc = DOMtable.firstChildElement("desc");

     if(tid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined table \'" + tname + "\' identifier. The object will not be read." + hqed::createDOMlocalization(DOMtable), 0, 2);
          result++;
          return result;
     }
     if(tname == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined table name.\nTable id = \'" + tid + "\'." + hqed::createDOMlocalization(DOMtable), 1, 2);
          result++;
     }

     fTitle = tname;
     fTableID = tid;
     if(!DOMdesc.isNull())
          fDescription = DOMdesc.text();

     // Reading table schema
     QDomElement DOMschm = DOMtable.firstChildElement("schm");
     if(!DOMschm.isNull())
     {
          // Reading precondition part
          QDomElement DOMcond = DOMschm.firstChildElement("precondition");
          if(!DOMcond.isNull())
          {
               QDomElement DOMattref = DOMcond.firstChildElement("attref");
               while(!DOMattref.isNull())
               {
                    QString aref = DOMattref.attribute("ref", "");
                    if(aref == "")
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Undefined attribute reference \'" + aref + "\'.\nError from \'" + DOMcond.tagName() + "\' part in table \'" + tname + "\'.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
                         result++;
                         DOMattref = DOMattref.nextSiblingElement("attref");
                         continue;
                    }
                    
                    XTT_Attribute* attr = AttributeGroups->FindAttribute(aref);
                    if(attr == NULL)
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Attribute reference error. Undefined attribute identifier \'" + aref + "\'.\nError from \'" + DOMcond.tagName() + "\' part in table \'" + tname + "\'.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
                         result++;
                         DOMattref = DOMattref.nextSiblingElement("attref");
                         continue;
                    }
                    
                    // Adding new column
                    if(ContextSize(1) > 0)
                         InsertCol(ColCount());
                    if(ContextSize(1) == 0)
                         InsertNewContext(1);
                    setContextAtributes(ColCount()-1, attr);      // Setting new attribute
                         
                    DOMattref = DOMattref.nextSiblingElement("attref");
               }
          }

          // Reading decision part
          QDomElement DOMactn = DOMschm.firstChildElement("conclusion");
          if(!DOMactn.isNull())
          {
               QDomElement DOMattref = DOMactn.firstChildElement("attref");
               while(!DOMattref.isNull())
               {
                    QString aref = DOMattref.attribute("ref", "");
                    if(aref == "")
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Undefined attribute reference \'" + aref + "\'.\nError from \'" + DOMactn.tagName() + "\' part in table \'" + tname + "\'.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
                         result++;
                         DOMattref = DOMattref.nextSiblingElement("attref");
                         continue;
                    }

                    XTT_Attribute* attr = AttributeGroups->FindAttribute(aref);
                    if(attr == NULL)
                    {
                         __msg += "\n" + hqed::createErrorString("", "", "Attribute reference error. Undefined attribute identifier \'" + aref + "\'.\nError from \'" + DOMactn.tagName() + "\' part in table \'" + tname + "\'.\nThe reference will be omitted." + hqed::createDOMlocalization(DOMattref), 1, 2);
                         result++;
                         DOMattref = DOMattref.nextSiblingElement("attref");
                         continue;
                    }

                    // Adding new column
                    if(ContextSize(4) > 0)
                         InsertCol(ColCount());
                    if(ContextSize(4) == 0)
                         InsertNewContext(4);
                    setContextAtributes(ColCount()-1, attr);      // Setting new attribute

                    DOMattref = DOMattref.nextSiblingElement("attref");
               }
          }
     }

     // Reading rules
     QDomElement DOMrule = DOMtable.firstChildElement("rule");
     while(!DOMrule.isNull())
     {
          // Checking rule data
          QString rid = DOMrule.attribute("id", "");

          if(rid == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined rule identifier in table \'" + fTitle + "\'.\nRule index = " + QString::number(RowCount()+1) + ". The object will not be read." + hqed::createDOMlocalization(DOMrule), 0, 2);
               result++;
               DOMrule = DOMrule.nextSiblingElement("rule");
               continue;
          }

          AddRow();
          fRows[fRowCount-1]->in_HML_2_0(DOMrule, __msg);

          DOMrule = DOMrule.nextSiblingElement("rule");
     }

     // Odswiezenie widoku
     delete fGTable;
     fGTable = new GTable(xtt::modelScene(), this);
     fGTable->ReCreate();
     fGTable->Update();
     fGTable->RePosition();

     return result;
}
// -----------------------------------------------------------------------------
