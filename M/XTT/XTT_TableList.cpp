/*
 *        $Id: XTT_TableList.cpp,v 1.43.4.6 2010-12-19 23:36:36 kkr Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QString>
#include <QTreeWidgetItem>
#include <QTextStream>
#include <iostream>

#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"

#include "../../V/XTT/GTable.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"
#include "../../C/XTT/widgets/ExecuteMessagesListWidget.h"

#include "../hType.h"
#include "../hTypeList.h"
#include "../hFunction.h"
#include "../hExpression.h"
#include "../hFunctionList.h"
#include "../hFunctionArgument.h"
#include "../hMultipleValue.h"

#include "XTT_Cell.h"
#include "XTT_Row.h"
#include "XTT_State.h"
#include "XTT_Table.h"
#include "XTT_TableList.h"
#include "XTT_Attribute.h"
#include "XTT_AttributeGroups.h"
#include "XTT_ConnectionsList.h"
//---------------------------------------------------------------------------

#define LEVEL_HEIGHT 50          
#define HORIZONTAL_MARGIN 50          
//---------------------------------------------------------------------------

XTT_TableList* TableList;
//---------------------------------------------------------------------------

// #brief Constructor XTT_TableList class.
XTT_TableList::XTT_TableList(void)
{
     fTables = NULL;
     fCount = 0;
     fNextTableID = 1;
     fNextRowID = 1;
     fChanged = false;
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_TableList class.
XTT_TableList::~XTT_TableList(void)
{
     if(!fCount)
          return;

     while(fCount > 0)
          Delete(0);
}
//---------------------------------------------------------------------------

// #brief Function clears table of pointers to the tabel.
//
// #return No return value.
void XTT_TableList::Flush(void)
{
     if(fCount == 0)
          return;

     while(fCount > 0)
          Delete(0);

     fTables = NULL;
     fNextTableID = 1;
     fNextRowID = 1;
     fCount = 0;
     
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the table.
//
// #param _index Index of the table.
// #return Pointer to the table. When index is incorrect funtion returns NULL.
XTT_Table* XTT_TableList::Table(unsigned int _index)
{
     if((((int)_index) < 0) || (_index >= fCount))
          return NULL;

     return fTables[_index];
}
//---------------------------------------------------------------------------

// #brief Returns list which contains all tables
//
// #return Returns list which contains all tables
QList<XTT_Table*>* XTT_TableList::tables(void)
{
     QList<XTT_Table*>* res = new QList<XTT_Table*>;
     
     for(unsigned int i=0;i<fCount;++i)
          res->append(fTables[i]);
          
     return res;
}
//---------------------------------------------------------------------------

// #brief Returns list of all rows pointers from tables
//
// #return pointer to the list of all rows pointers from tables
QList<XTT_Row*>* XTT_TableList::rows(void)
{
     QList<XTT_Row*>* res = new QList<XTT_Row*>;
     for(unsigned int i=0;i<fCount;++i)
     {
          QList<XTT_Row*>* tmp = fTables[i]->rowList();
          *res << *tmp;
          delete tmp;
     }
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Returns list of predecessor tables 
//
// #param unsigned int _id - identifier of successor
// #return Returns list of predecessor tables 
QList<XTT_Table*>* XTT_TableList::parents(QString _id)
{
     QList<XTT_Table*>* res = new QList<XTT_Table*>;     
     QList<XTT_Connections*>* cl = ConnectionsList->listOfOutConnections(_id);
     
     for(int i=0;i<cl->size();++i)
     {
          XTT_Table* t = _IndexOfID(cl->at(i)->DesTable());
          if((t != NULL) && (!res->contains(t)))
               res->append(t);
     }
     
     delete cl;
     return res;
}
//---------------------------------------------------------------------------

// #brief Returns list of predecessor tables 
//
// #param XTT_Table* _table - poiter to successor
// #return Returns list of predecessor tables 
QList<XTT_Table*>* XTT_TableList::parents(XTT_Table* _table)
{
     if(_table == NULL)
          return NULL;

     return parents(_table->TableId());
}
//---------------------------------------------------------------------------

// #brief Returns list of successors tables 
//
// #param unsigned int _id - identifier of predecessor
// #param bool skipParentNode - determines skiping node when the child is the same as parent (loop)
// #return Returns list of successors tables 
QList<XTT_Table*>* XTT_TableList::childs(QString _id, bool skipParentNode)
{
     QList<XTT_Table*>* res = new QList<XTT_Table*>;     
     QList<XTT_Connections*>* cl = ConnectionsList->listOfInConnections(_id);
     
     for(int i=0;i<cl->size();++i)
     {
          XTT_Table* t = _IndexOfID(cl->at(i)->SrcTable());
          if(t == NULL)
               continue;
               
          bool isStateTable = (t->TableType() == 0);
          bool showStateTables = Settings->xttShowInitialTables();
          bool canBeShown = ((!isStateTable) || ((isStateTable) && (showStateTables)));
               
          if((canBeShown) && (!res->contains(t)) && ((!skipParentNode) || ((skipParentNode) && (t->TableId() != _id))))
               res->append(t);
     }
     
     delete cl;
     return res;
}
//---------------------------------------------------------------------------

// #brief Returns list of successors tables 
//
// #param XTT_Table* _table - poiter to predecessor
// #param bool skipParentNode - determines skiping node when the child is the same as parent (loop)
// #return Returns list of successors tables 
QList<XTT_Table*>* XTT_TableList::childs(XTT_Table* _table, bool skipParentNode)
{
     if(_table == NULL)
          return NULL;

     return childs(_table->TableId(), skipParentNode);
}
//---------------------------------------------------------------------------

// #brief Function retrieves decision whether table was modyfied.
//
// #return True when table was modyfied, otherwise function returns false.
bool XTT_TableList::Changed(void)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fTables[i]->Changed())
               return true;
               
     return fChanged;
}
//---------------------------------------------------------------------------

// #brief Function makes decision whether table was modyfied.
//
// #param _b Decision whether table was modyfied.
// #return No return value.
void XTT_TableList::setChanged(bool _c)
{
     for(unsigned int i=0;i<fCount;i++)
          fTables[i]->setChanged(_c);
               
     fChanged = _c;
}
//---------------------------------------------------------------------------

// #brief Create and add new table
//
// #return poiter to new table
XTT_Table* XTT_TableList::Add(void)
{
     // Utworenie nowego elementu
     XTT_Table* newT = new XTT_Table(++fNextTableID, &fNextRowID);
     if(!Add(newT))
     {
          delete newT;
          newT = NULL;
     }
     
     return newT;
}
//---------------------------------------------------------------------------

// #brief Adds new table to container
//
// #param XTT_Table* _new - pointer to new table
// #return true on succes otherwise false
bool XTT_TableList::Add(XTT_Table* _new)
{
     if(_new == NULL)
          return false;

     // Sprawdzanie czy tabela juz nie istnieje
     for(unsigned int i=0;i<fCount;i++)
          if(fTables[i] == _new)
               return false;

     XTT_Table** tmp = new XTT_Table*[fCount+1];

     // Kopiowanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fTables[i];

     // Usuniecie starej tablicy wskaznikow
     if(fCount)
          delete [] fTables;

     // Zapisanie nowej lokalizacji
     fTables = tmp;

     // Utworenie nowego elementu
     fTables[fCount] = _new;
     
     // Powiekszenie pojemnika
     fCount++;
     
     // Zarejestrowanie ze sie zmienilo
     fChanged = true;
     
     return true;
}
//---------------------------------------------------------------------------

// #brief Function removes the table.
//
// #param _index Index of the table.
// #return True when table index was OK, otherwise function returns false.
bool XTT_TableList::Delete(unsigned int _index)
{
     if((((int)_index) < 0) || (_index >= fCount))
          return false;

     XTT_Table** tmp = new XTT_Table*[fCount-1];

     // Kopiowanie zawartosci
     int tmp_i = 0;
     for(unsigned int i=0;i<fCount;i++)
          if(i != _index)
               tmp[tmp_i++] = fTables[i];

     // Zapisanie lokalizacji do usuniecia
     XTT_Table* unnptr = fTables[_index];
     
     // Usuniecie starej tablicy wskaznikow
     if(fCount)
          delete [] fTables;

     // Zapisanie nowej lokalizacji
     fTables = tmp;
     
     // Pomniejszenie pojemnika
     fCount--;
     
     // Usuniecie lokalizacji
     delete unnptr;
     
     // Zarejestrowanie ze sie zmienilo
     fChanged = true;
     
     // Pousunieciu tabeli nalezy ponowanie wyliczyc ustawienia polaczen
     // aby ich wartosci nie odnosily sie do blednych tabel
     ConnectionsList->updateConnectionsParameters();

     return true;
}
//---------------------------------------------------------------------------

// #brief Function sets new name of the table.
//
// #param _baseName Base name of the table.
// #return Base name of the table with table index.
QString XTT_TableList::newBasicTableName(QString _baseName)
{
     // Ustawianie nowej nazwy tabeli
     int tableNamePostfix = 0;
     QString autoTableName;
     
     do
     {
          tableNamePostfix++;
          autoTableName = _baseName + QString::number(tableNamePostfix);
     }
     while(IndexOfTitle(autoTableName) > -1);
     
     return autoTableName;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the table.
//
// #param _title Title of the table.
// #return No return value.
int XTT_TableList::IndexOfTitle(QString _title)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fTables[i]->Title() == _title)
               return i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the table.
//
// #param _title Title of the table.
// #return Pointer to the table.
XTT_Table* XTT_TableList::_IndexOfTitle(QString _title)
{
     int index = IndexOfTitle(_title);
     if(index == -1)
          return NULL;

     return fTables[index];
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the table.
//
// #param _id Id of the table.
// #return Index of the table.
int XTT_TableList::IndexOfID(QString _id)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fTables[i]->TableId() == _id)
               return i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the table.
//
// #param _id Table id.
// #return Pointer to the table.
XTT_Table* XTT_TableList::_IndexOfID(QString _id)
{
     int index = IndexOfID(_id);
     if(index == -1)
          return NULL;

     return fTables[index];
}
//---------------------------------------------------------------------------

// #brief Function searchs for uniq table id
//
// #return uniq table id as string
QString XTT_TableList::findUniqTableID(void)
{
     int candidate = 1;
     QString strcandidate;
     
     do
     {
          strcandidate = XTT_Table::idPrefix() + QString::number(candidate);
          candidate++;
     }
     while(IndexOfID(strcandidate) > -1);
     
     return strcandidate;
}
//---------------------------------------------------------------------------

// #brief Function that search for the table that has a given title. It checks if the table can have such title w.r.t. to table type. There can be more than one fact table with the same title
//
// #param __title - given title
// #param __ignoreIndex - usually the index of the table that runs this method. If no specified then -1
// #return index of the table if exists otherwise -1
int XTT_TableList::indexOfTitle(QString __title, int __ignoreIndex)
{
     bool isFact = false, isCurrFact;
     if((__ignoreIndex >= 0) && (__ignoreIndex < (int)Count()))
          isFact = (Table(__ignoreIndex)->TableType() == 0);
     
     for(unsigned int i=0;i<Count();++i)
     {
          isCurrFact = (Table(i)->TableType() == 0);
          if((__ignoreIndex != (int)i) && (Table(i)->Title() == __title) && (!(isCurrFact && isFact)))
               return i;
     }
     
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function that adds new messages about simualtion result to executeMessagesListWidget.
//
// #param __list - list of strinlists. The list has only one element that has the following format:
// #param __notUpdatedState - state to be updated
// #li at(0) - table_1 name
// #li at(1) - rule index
// #li at(2) - table_2 name
// #li at(3) - rule index
// #li ...
// #return No return value.
void XTT_TableList::addSimulationResult(QList<QStringList>* __list, XTT_State* __notUpdatedState)
{
     if(__notUpdatedState != NULL)
          __notUpdatedState->trajectory()->clear();

     MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "------------------------ A sequence of analized rules ------------------------", 2);
     for(int i=0;i<__list->at(0).size();i+=2)
          addSimulationFiredRule(__list->at(0).at(i), __list->at(0).at(i+1), "", __notUpdatedState);
}
//---------------------------------------------------------------------------

// #brief Function that adds new message about fired rule to executeMessagesListWidget.
//
// #param __tablename - title of the table in which the rule has been fired.
// #param __rowIndex - index of fired rule
// #param __cellIndex - index of fired cell
// #param __notUpdatedState - state to be updated
// #return No return value.
void XTT_TableList::addSimulationFiredRule(QString __tablename, QString __rowIndex, QString __cellIndex, XTT_State* __notUpdatedState)
{
     int tindex = indexOfTitle(__tablename);
     int rindex = __rowIndex.toInt()-1;      // it returns -1 when conversion will fail
     int cindex = __cellIndex.toInt()-1;     // it returns -1 when conversion will fail
     
     QString tid = "",
             rid = "",
             cid = "";
     
     if(tindex == -1)
     {
          MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Error: undefined table \'" + __tablename + "\'.", 0);
          return;
     }
     tid = Table(tindex)->TableId();
          
     if(rindex >= (int)Table(tindex)->RowCount())
     {
          MainWin->xtt_runModelMessagesDockWindowMessagesList->add(tid, "", "", "Error: incorrect rule index \'" + __rowIndex + "\'.\nThe table \'" + __tablename + "\' has only " + QString::number(Table(tindex)->RowCount()) + " row(s).", 0);
          return;
     }
     rid = rindex == -1 ? "" : Table(tindex)->Row(rindex)->RowId();
     if(__notUpdatedState != NULL)
          __notUpdatedState->trajectory()->append(Table(tindex)->Row(rindex));
     
     if(rindex > -1)
     {
          if(cindex >= (int)Table(tindex)->Row(rindex)->Length())
          {
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Error: incorrect rule index \'" + __rowIndex + "\'.\nThe table \'" + __tablename + "\' has only " + QString::number(Table(tindex)->RowCount()) + " row(s).", 0);
               return;
          }
          cid = cindex == -1 ? "" : Table(tindex)->Row(rindex)->Cell(cindex)->CellId();
     }
     
     MainWin->xtt_runModelMessagesDockWindowMessagesList->add(tid, rid, cid, "fired rule.", 2);
}
//---------------------------------------------------------------------------

// #brief Function retrieves decision whether the table is empty.
//
// #return Decision whether the table is empty.
bool XTT_TableList::IsEmpty(void)
{
     return !fCount;
}
// ---------------------------------------------------------------------------

// #brief Function retrieves index to the first table that has the type.
//
// #param _type Type of the table.
// #return Index to the table.
int XTT_TableList::IndexOfType(unsigned int _type)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fTables[i]->TableType() == _type)
               return i;

     return -1;
}
// ---------------------------------------------------------------------------

// #brief Function retrieves list of all headers of tables.
//
// #param __uniq - indicates if the list cannot contain repeated titles
// #return List of all headers of tables.
QStringList XTT_TableList::TablesTitles(bool __uniq)
{
     QStringList res;
     res.clear();

     for(unsigned int i=0;i<fCount;i++)
          if((!__uniq) || (!res.contains(fTables[i]->Title())))
               res << fTables[i]->Title();

     return res;
}
// ---------------------------------------------------------------------------

// #brief Returns list of the state names
//
// #return Returns list of the state names
QStringList XTT_TableList::stateNames(void)
{
     QStringList res;
     res.clear();

     for(unsigned int i=0;i<fCount;i++)
          if((fTables[i]->TableType() == 0) && (!res.contains(fTables[i]->Title())))
               res << fTables[i]->Title();

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function retrieves decision whether the table is empty.
//
// #param _index Index of the table.
// #return No return value.
void XTT_TableList::Refresh(int _t)
{
     if((_t < 0) || (_t >= (int)fCount))
          return;
      
     fTables[_t]->GraphicTable()->ReCreate();
     fTables[_t]->GraphicTable()->Update();
     fTables[_t]->GraphicTable()->RePosition();

     ConnectionsList->RepaintConnectionRelatedWithTable(fTables[_t]->TableId());
     
     MainWin->xtt_scene->update();
     //MainWin->scaleView(1.001);
     //MainWin->scaleView(1./1.001);
}
// ---------------------------------------------------------------------------

// #brief Function refreshes all table.
//
// #return No return table.
void XTT_TableList::RefreshAll(void)
{
     for(unsigned int i=0;i<fCount;i++)
          Refresh(i);
}
// ---------------------------------------------------------------------------

// #brief Function analyzes all cells.
//
// #return No return table.
void XTT_TableList::AnalyzeAllCells(void)
{
     for(unsigned int t=0;t<fCount;t++)
     {
          for(unsigned int r=0;r<Table(t)->RowCount();r++)
          {
               for(unsigned int c=0;c<Table(t)->Row(r)->Length();c++)
               {
                    hMultipleValueResult mvr = Table(t)->Row(r)->Cell(c)->Content()->value();
                    if((!(bool)mvr) && (Settings->xttShowErrorCellMessage()))
                         QMessageBox::critical(NULL, "HQEd", Table(t)->Row(r)->Cell(c)->CellError(), QMessageBox::Ok);
               }
          }
     }
}
// ---------------------------------------------------------------------------

// #brief Function analyzes connections of the table.
//
// #param _tid Table id.
// #return No return table.
void XTT_TableList::AnalyzeTableConnections(QString _tid)
{
     int ti = IndexOfID(_tid);
     if(ti == -1)
          return;
          
     Table(ti)->connectionsDiagnostics();
}
// ---------------------------------------------------------------------------

// #brief Function analyzes connections of all tables.
//
// #return No return table.
void XTT_TableList::AnalyzeAllTablesConnections(void)
{
     for(unsigned int t=0;t<fCount;++t)
               Table(t)->connectionsDiagnostics();
}
// ---------------------------------------------------------------------------

// #brief Function search for the table that contains given row
//
// #param __ptr - pointer to the row.
// #return pointer to the table that contains the given row, is row doesn't exists return NULL
XTT_Table* XTT_TableList::findRow(XTT_Row* __ptr)
{
     for(unsigned int t=0;t<fCount;++t)
          if(Table(t)->indexOfRow(__ptr) > -1)
               return Table(t);
          
     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Function search for the table that contains given cell
//
// #param __ptr - pointer to the cell.
// #return pointer to the table that contains the given cell, is cell doesn't exists return NULL
XTT_Table* XTT_TableList::findCell(XTT_Cell* __ptr)
{
     for(unsigned int t=0;t<fCount;++t)
          if(Table(t)->indexOfRow(__ptr) > -1)
               return Table(t);
          
     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Search for tables with the given name and type
//
// #param __name - name of the table. If the string is empty each table satisfy this criterion.
// #param __ttype - type of thr table. If value is -1 each table type satisfy this criterion.
// #return It return a list of pointers to the tables that satisfy the search criteria
QList<XTT_Table*>* XTT_TableList::findTables(QString __name, int __ttype)
{
     QList<XTT_Table*>* result = new QList<XTT_Table*>;
     
     for(unsigned int i=0;i<Count();++i)
     {
          XTT_Table* tp = Table(i);
          
          if(
               ((__name.isEmpty()) || (__name == tp->Title())) &&
               ((__ttype == -1) || (__ttype == (int)tp->TableType()))
             )
               result->append(tp);
     }
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which satisfied the search criteria
//
// #param QString _phrase - sarched phrase
// #param bool _caseSensitivity - if true search is CaseSensitivity
// #param bool* _elements - set of search criteria
// #param int _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
// #return It return a list of string whcich contains informations about result of search
QStringList* XTT_TableList::findTables(QString _phrase, bool _caseSensitivity, bool* _elements, int _phrasesPolicy)
{
     // Each string inresult has format:
     // ELEMENT;SUBELEMENT;PARAMSLIST

     bool si_titles = _elements[0];
     bool si_descriptions = _elements[1];
     bool si_headers = _elements[2];
     bool si_cells = _elements[3];
     bool sic_conditional = _elements[4];
     bool sic_assert = _elements[5];
     bool sic_retract = _elements[6];
     bool sic_action = _elements[7];
     
     Qt::CaseSensitivity cs = Qt::CaseInsensitive;
     if(_caseSensitivity)
          cs = Qt::CaseSensitive;
     QStringList phrases = _phrase.split(" ", QString::SkipEmptyParts);
     
     QStringList* res = new QStringList;
     
     for(unsigned int t=0;t<fCount;++t)
     {
          if(si_titles)
               if(hqed::searchForPhrases(&phrases, fTables[t]->Title(), cs, _phrasesPolicy))
                    res->append("XTT_TABLE;TITLE;" + fTables[t]->TableId());
                    
          if(si_descriptions)
               if(hqed::searchForPhrases(&phrases, fTables[t]->Description(), cs, _phrasesPolicy))
                    res->append("XTT_TABLE;DESCRIPTION;" + fTables[t]->TableId());
          
          // Search in headers          
          if(si_headers)
          {
               for(int c=0;c<fTables[t]->ColCount();++c)
               {
                    // Sprawdzanie czy mozna wyszukiwac w danych kontekstach
                    bool ok = true;
                    if(((!sic_conditional) && (fTables[t]->ColContext(c) == 1))
                    ||
                    ((!sic_assert) && (fTables[t]->ColContext(c) == 2))
                    ||
                    ((!sic_retract) && (fTables[t]->ColContext(c) == 3))
                    ||
                    ((!sic_action) && (fTables[t]->ColContext(c) == 4)))
                         ok = false;
               
                    if(ok)
                    {
                         QString shortPath = "", 
                                   longPath = "";
                         
                         XTT_Attribute* ca = fTables[t]->ContextAtribute((unsigned int)c);
                         if(ca != NULL)
                         {
                              shortPath = AttributeGroups->CreatePath(ca) + ca->Acronym();
                              longPath = AttributeGroups->CreatePath(ca) + ca->Name();
                              
                              if((hqed::searchForPhrases(&phrases, shortPath, cs, _phrasesPolicy)) || (hqed::searchForPhrases(&phrases, longPath, cs, _phrasesPolicy)))
                                   res->append("XTT_TABLE;HEADER;" + fTables[t]->TableId() + ";" + QString::number(c));
                         }
                    }
                    
               }
          }
          
          // Search in cells
          if(si_cells)
          {
               for(unsigned int r=0;r<fTables[t]->RowCount();++r)
               {
                    for(int c=0;c<fTables[t]->ColCount();++c)
                    {
                         // Sprawdzanie czy mozna wyszukiwac w danych kontekstach
                         bool ok = true;
                         if(((!sic_conditional) && (fTables[t]->ColContext(c) == 1))
                         ||
                         ((!sic_assert) && (fTables[t]->ColContext(c) == 2))
                         ||
                         ((!sic_retract) && (fTables[t]->ColContext(c) == 3))
                         ||
                         ((!sic_action) && (fTables[t]->ColContext(c) == 4)))
                              ok = false;
                    
                         if(ok)
                         {
                              QString cnt = fTables[t]->Row(r)->Cell(c)->Content()->toString();
                              if(hqed::searchForPhrases(&phrases, cnt, cs, _phrasesPolicy))
                                   res->append("XTT_TABLE;CELL;" + fTables[t]->TableId() + ";" + QString::number(r) + ";" + QString::number(c));
                         }
                         
                    }
               }
          }
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which have appropriate attribute in one of their contexts
//
// #param QList<XTT_Table*>* _setOfTables - search set
// #param bool* _contexts - determines in which contexts works search process
// #param XTT_Attribute* _contextAttribute - pointer to wanted attribute
// #return It return a list of tables which satisfied search criteria
QList<XTT_Table*>* XTT_TableList::findTables(QList<XTT_Table*>* _setOfTables, bool* _contexts, XTT_Attribute* _contextAttribute)
{
     bool sic_conditional = _contexts[0];
     bool sic_assert = _contexts[1];
     bool sic_retract = _contexts[2];
     bool sic_action = _contexts[3];
     
     QList<XTT_Table*>* res = new QList<XTT_Table*>;
     
     for(int t=0;t<_setOfTables->size();++t)
     {
          for(int c=0;c<_setOfTables->at(t)->ColCount();++c)
          {
               // Sprawdzanie czy mozna wyszukiwac w danych kontekstach
               bool ok = true;
               if(((!sic_conditional) && (_setOfTables->at(t)->ColContext(c) == 1))
               ||
               ((!sic_assert) && (_setOfTables->at(t)->ColContext(c) == 2))
               ||
               ((!sic_retract) && (_setOfTables->at(t)->ColContext(c) == 3))
               ||
               ((!sic_action) && (_setOfTables->at(t)->ColContext(c) == 4)))
                    ok = false;
          
               if(ok)
               {
                    if(_setOfTables->at(t)->ContextAtribute((unsigned int)c) == _contextAttribute)
                         res->append(_setOfTables->at(t));
               }
               
          }
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which have appropriate attribute in one of their contexts
//
// #param bool* _contexts - determines in which contexts works search process
// #param XTT_Attribute* _contextAttribute - pointer to wanted attribute
// #return It return a list of tables which satisfied search criteria
QList<XTT_Table*>* XTT_TableList::findTables(bool* _contexts, XTT_Attribute* _contextAttribute)
{
     QList<XTT_Table*>* t = tables();
     QList<XTT_Table*>* res = findTables(t, _contexts, _contextAttribute);
     delete t;
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which have appropriate attribute in one of their contexts
//
// #param QList<XTT_Table*>* _setOfTables - search set
// #param int _ctx - number of context to search. [1;4]
// #param QList<XTT_Attribute*>* _contextAttributes - list of attributes
// #param bool _eqsize - determines search type. When true context must have the same size as _contextAttributes, otherwise context size can be greater
// #return It return a list of tables which satisfied search criteria
QList<XTT_Table*>* XTT_TableList::findTables(QList<XTT_Table*>* _setOfTables, int _ctx, QList<XTT_Attribute*>* _contextAttributes, bool _eqsize)
{
     QList<XTT_Table*>* res;

     // Copy input parameter to working variable
     QList<XTT_Table*>* clist = new QList<XTT_Table*>;
     for(int i=0;i<_setOfTables->size();++i)
     {
          if(((_eqsize) && (_setOfTables->at(i)->ContextSize(_ctx) != _contextAttributes->size()))           // If the contexts size must be the same
          || 
          (_setOfTables->at(i)->ContextSize(_ctx) < _contextAttributes->size()))     // If the table context is smaller than attribute count then on 100% all attributes can't be found in that context
               continue;
          
          clist->append(_setOfTables->at(i));
     }
     res = clist;
     
     // Setting-up search ctx array
     if((_ctx < 1) || (_ctx > 4))
          _ctx = 1;
     bool ctxs[4] = {false, false, false, false};
     ctxs[_ctx-1] = true;
     
     // Searching for tables
     for(int a=0;a<_contextAttributes->size() && !clist->isEmpty();++a)
     {
          res = findTables(clist, ctxs, _contextAttributes->at(a));
          delete clist;
          clist = res;
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which have appropriate attribute in one of their contexts
//
// #param int _ctx - number of context to search. [1;4]
// #param QList<XTT_Attribute*>* _contextAttributes - list of attributes
// #param bool _eqsize - determines search type. When true context must have the same size as _contextAttributes, otherwise context size can be greater
// #return It return a list of tables which satisfied search criteria
QList<XTT_Table*>* XTT_TableList::findTables(int _ctx, QList<XTT_Attribute*>* _contextAttributes, bool _eqsize)
{
     // If the list of attribues is empty, then returs also empty list
     if(_contextAttributes->isEmpty())
          return new QList<XTT_Table*>;

     QList<XTT_Table*>* t = tables();
     QList<XTT_Table*>* res = findTables(t, _ctx, _contextAttributes, _eqsize);
     delete t;
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which contains in one of their context given attributes
//
// #param QList<XTT_Attribute*>* _ca - attributes for conditional context
// #param QList<XTT_Attribute*>* _aa - attributes for action context
// #param XTT_Table* _with_no_pattern - poiter to the table which can't exists in result
// #return It return a list of similar tables
QList<XTT_Table*>* XTT_TableList::findSimilarTables(QList<XTT_Attribute*>* _ca, QList<XTT_Attribute*>* _aa, XTT_Table* _with_no_pattern)
{
     QList<XTT_Table*>* res;

     // First searching for table with similar context
     QList<XTT_Table*>* cc_tl = findTables(1, _ca, true);
     QList<XTT_Table*>* ac_tl;
     
     if(_with_no_pattern != NULL)
          if(cc_tl->contains(_with_no_pattern))
               cc_tl->removeAt(cc_tl->indexOf(_with_no_pattern));
     
     // If the list isn't empty then we mearge new table
     if(!cc_tl->isEmpty())
          res = cc_tl;
     
     // If the list is empty then we try to match action context
     if(cc_tl->isEmpty())
     {
          delete cc_tl;
          ac_tl = findTables(4, _aa, true);
          res = ac_tl;                              // If the list is empty that menas there is no similar tables, otherwise res contains all similar tables
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Search for tables which are similar to given table. Function intruduced for convinience.
//
// #param XTT_Table* _pattern - pattern table
// #param bool* ctxs - determines analyzed contexts
// #return It return a list of similar tables without pattern table
QList<XTT_Table*>* XTT_TableList::findSimilarTables(XTT_Table* _pattern)
{
     QList<XTT_Attribute*>* _ca = _pattern->ContextAtributes(1);
     QList<XTT_Attribute*>* _aa = _pattern->ContextAtributes(4);;
     
     // Searching for similar tables
     QList<XTT_Table*>* res = findSimilarTables(_ca, _aa, _pattern);
     if(res->contains(_pattern))
          res->removeAt(res->indexOf(_pattern));
          
     delete _ca;
     delete _aa;
          
     return res;
}
// ---------------------------------------------------------------------------

// #brief Creates table with given attributes
//
// #param QList<XTT_Attribute*>* _conAtts - conditional attributes
// #param QList<XTT_Attribute*>* _actAtts - action attributes
// #return Pointer to created table, if the table exists retursn poiter to that table
XTT_Table* XTT_TableList::createTable(QList<XTT_Attribute*>* _conAtts, QList<XTT_Attribute*>* _actAtts)
{
     XTT_Table* res;

     // First searching for table with similar context
     QList<XTT_Table*>* cc_tl = findTables(1, _conAtts, true);     
     QList<XTT_Table*>* ac_tl = findTables(4, _actAtts, true);     
     QList<XTT_Table*> t = hqed::listIntersection(*cc_tl, *ac_tl);     

     // If the table exists then we not create a new one
     if(!t.isEmpty())
          res = t.at(0);

     // If the table doesn't exists
     if(t.isEmpty())
     {
          QList<XTT_Attribute*> caList = *_conAtts + *_actAtts;
          
          // Creating new table
          XTT_Table* tbl = new XTT_Table(++fNextTableID, &fNextRowID);
          tbl->setTitle(Settings->xttTableBaseName() + QString::number(Count()+1));
          tbl->setTableType(1);
          //tbl->AddRow();                                                  // Add new row

          for(int i=0;i<caList.size();++i)                              // Addnig columns
          {
               if((i == 0) && (_conAtts->size() > 0))
                    tbl->InsertNewContext(1);
               if(i == _conAtts->size())
                    tbl->InsertNewContext(4);
               if((i == 0) || (i == _conAtts->size()))
                    continue;
                    
               tbl->InsertCol(i);
               tbl->setColContext(i, i<_conAtts->size()?1:4);
          }

          for(int i=0;i<caList.size();++i)     
          {          
               tbl->setContextAtributes(i, caList.at(i));               // Setting up context attributes
               // here can be defined a default content of a cell.
               // to see what was heree before check CVS repository
          }
          res = tbl;
     }

     delete cc_tl;
     delete ac_tl;

     return res;
}
// ---------------------------------------------------------------------------

// #brief Merges columns of given tables. The first table is the main (result of similarTables) table and the second is the subtable
//
// #param XTT_Table* _first - fist table - main table
// #param XTT_Table* _second - the second table - subtable
// #return true if the columns in _first table had been changed
bool XTT_TableList::mergeTables(XTT_Table* _first, XTT_Table* _second)
{
     bool res = false;

     QList<XTT_Attribute*>* ft_ca = _first->ContextAtributes(1);
     QList<XTT_Attribute*>* ft_aa = _first->ContextAtributes(4);
     QList<XTT_Attribute*>* st_ca = _second->ContextAtributes(1);
     QList<XTT_Attribute*>* st_aa = _second->ContextAtributes(4);
     
     /*
     QString msg = "Merging tables: " + _first->Title() + ", " + _second->Title() + ": ";
     msg += "\nFirst table: ";
     for(int i=0;i<ft_ca->size();++i) msg += ft_ca->at(i)->Name() + ", ";
     msg += " | ";
     for(int i=0;i<ft_aa->size();++i) msg += ft_aa->at(i)->Name() + ", ";
     msg += "\nSecond table: ";
     for(int i=0;i<st_ca->size();++i) msg += st_ca->at(i)->Name() + ", ";
     msg += " | ";
     for(int i=0;i<st_aa->size();++i) msg += st_aa->at(i)->Name() + ", ";*/
     
     // Merging contexts attributes from both tables
     QList<XTT_Attribute*> cc = hqed::mergeListContents(*ft_ca, *st_ca);
     QList<XTT_Attribute*> ac = hqed::mergeListContents(*ft_aa, *st_aa);
     
     // When merging with conditional context
     if(cc.size() > ft_ca->size())
     {
          //QMessageBox::critical(NULL, "addTableTemplate", "merging with conditional: " + msg, QMessageBox::Ok);
          for(int c=ft_ca->size();c<cc.size();++c)
          {
               _first->InsertCol(c);
               _first->setColContext(c, 1);
               _first->setContextAtributes(c, cc.at(c));
          }
          
          res = true;
     }
     
     // When merging with action context
     if(ac.size() > ft_aa->size())
     {
          //QMessageBox::critical(NULL, "addTableTemplate", "Merging with action: " + msg, QMessageBox::Ok);
          for(int c=ft_aa->size();c<ac.size();++c)
          {
               int colIndex = _first->ColCount();
               _first->InsertCol(colIndex);
               _first->setColContext(colIndex, 4);
               _first->setContextAtributes(colIndex, ac.at(c));
          }
          
          res = true;
     }
     
     if((cc.size() == ft_ca->size()) && (ac.size() == ft_aa->size()))
     {
          //QMessageBox::critical(NULL, "addTableTemplate", "NOT MERGED: " + msg, QMessageBox::Ok);
          res = false;
     }
     
     // Memory free
     delete ft_ca;
     delete ft_aa;
     delete st_ca;
     delete st_aa;
     
     // returns function result
     return res;
}
// ---------------------------------------------------------------------------

// #brief Adds new table template generated from ARD+
//
// #param QList<XTT_Attribute*>* _conAtts - conditional attributes
// #param QList<XTT_Attribute*>* _actAtts - action attributes
// #return true if table not exists, otherwise false
bool XTT_TableList::addTableTemplate(QList<XTT_Attribute*>* _conAtts, QList<XTT_Attribute*>* _actAtts)
{
     // Creating and merging table
     return Add(createTable(_conAtts, _actAtts));
}
// ---------------------------------------------------------------------------

// #brief Perform optimalization procedure for XTT generated from ARD+
//
// #return 0 - if succes, 1 - when dimesnions of list aren't correct
int XTT_TableList::optimizeXTT(void)
{
     QList<XTT_Table*>* simT;               // list of similars tables to current
     
     bool contLoop;
     do
     {
          contLoop = false;
          
          // for each table
          for(int i=(int)fCount-1;i>=0 && !contLoop;--i)
          {
               simT = findSimilarTables(fTables[i]);
               
               if(!simT->isEmpty())
               {
                    if(mergeTables(simT->at(0), fTables[i]))     // Merging table
                         contLoop = true;
                    Delete(i);                                   // Deleting merged table
               }
                    
               delete simT;
          }
     }
     while(contLoop);
          
     return 0;
}
// ---------------------------------------------------------------------------

// #brief Search for tables that contains input attributes in the conditional contex
//
// #return It return a list of tables
QList<XTT_Table*>* XTT_TableList::inputTables(void)
{
     QList<XTT_Table*>* result = new QList<XTT_Table*>;
     QList<XTT_Attribute*> inputatts = AttributeGroups->classFilter(XTT_ATT_CLASS_RO);

     for(int i=0;i<inputatts.size();++i)
     {
          QList<XTT_Attribute*> ctxattr = QList<XTT_Attribute*>() << inputatts.at(i);
          QList<XTT_Table*>* tmpres = findTables(1, &ctxattr, false);
          *result = hqed::mergeListContents(*result, *tmpres);
          delete tmpres;
     }
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Search for ends tables which can be interpreted as root of tables tree
//
// #return It return a list of tables
QList<XTT_Table*>* XTT_TableList::rootsTables(void)
{
     QList<XTT_Table*>* res = tables();
          
     // Removing tables form the list
     for(unsigned int c=0;c<ConnectionsList->Count();++c)
     {
          XTT_Table* srcT = _IndexOfID(ConnectionsList->Connection(c)->SrcTable());
          XTT_Table* desT = _IndexOfID(ConnectionsList->Connection(c)->DesTable());
     
          // The loop connection doesn't cose table removing
          if(srcT == desT)
               continue;
               
          // Removing srcT from the list
          if(res->contains(srcT))
               res->removeAt(res->indexOf(srcT));
     }
     
     
     // If there is no root found, then we search tables which have the biggest family
     if(res->size() == 0)
     {
          // Badanie rozmiarow rodzin poszczegolnych wezlow
          QList<int> famSize;
          int maxSize = 0;
          for(unsigned int i=0;i<fCount;++i)
          {
               QList<XTT_Table*>* family = findFamily(fTables[i]->TableId());
               famSize.append(family->size());
               if(maxSize < family->size())
                    maxSize = family->size();
               delete family;
          }
          
          // Do rezultatu dodajemy te wezly ktore maja rodzine rowna maksymalnemu rozmiarowi
          for(unsigned int i=0;i<fCount;++i)
               if(maxSize == famSize.at(i))
                    res->append(fTables[i]);
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Returns all of tables which belongs to input table family
//
// #param unsigned int _id - identifier of input table
// #return It return a list of tables which belongs to input table family
QList<XTT_Table*>* XTT_TableList::findFamily(QString _id)
{
     if(IndexOfID(_id) == -1)
          return NULL;

     QList<XTT_Table*>* _family = new QList<XTT_Table*>;
     createGenealogyTree(_id, _family);
     return _family;
}
// ---------------------------------------------------------------------------

// #brief Recursive function which creates genealogy tree for the table
//
// #param unsigned int _id - identifier of current table
// #param QList<XTT_Table*>* _currentFamily - list of founded family members
// #return No values returs
void XTT_TableList::createGenealogyTree(QString _id, QList<XTT_Table*>* _currentFamily)
{
     // Search for tables connected with current
     QList<XTT_Connections*>* directDep = ConnectionsList->listOfInConnections(_id);
     
     // Dodawanie do listy rodziny wyszukanych zaleznosci
     for(int i=0;i<directDep->size();++i)
     {
          XTT_Table* srcT = _IndexOfID(directDep->at(i)->SrcTable());
          if(srcT == NULL)
               continue;
               
          bool isStateTable = (srcT->TableType() == 0);
          bool showStateTables = Settings->xttShowInitialTables();
          bool canBeShown = ((!isStateTable) || ((isStateTable) && (showStateTables)));
               
          if((canBeShown) && (!_currentFamily->contains(srcT)))
          {
               _currentFamily->append(srcT);
               createGenealogyTree(srcT->TableId(), _currentFamily);
          }
     }
          
     delete directDep;
}
// ---------------------------------------------------------------------------

// #brief Function that return a list of tables where the first tables have the smallest numer of incomming connection, and the last ones the biggest
//
// #return a list of tables where the first tables have the smallest numer of incomming connection, and the last ones the biggest
QList<XTT_Table*>* XTT_TableList::tablesOrder(void)
{
     QList<XTT_Table*>* result = new QList<XTT_Table*>;
     QList<XTT_Table*>* roots = rootsTables();
     *result = hqed::mergeListContents(*result, *roots);
     
     for(int i=0;i<roots->size();++i)
     {
          QList<XTT_Table*>* tmp = findFamily(roots->at(i)->TableId());
          *result = hqed::mergeListContents(*result, *tmp);
          delete tmp;
     }
     delete roots;
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Arranges tables on the scene
//
// #return No values returs
void XTT_TableList::tablesLayout(void)
{
     QList<XTT_Table*>* roots = rootsTables();
     if(roots->size() == 0)
          return;
          
     createTree(roots);

     delete roots;
}
// ---------------------------------------------------------------------------

// #brief Checks if the first list contains at least one element from the second list
//
// #param QList<XTT_Table*>* _first - first list
// #param QList<XTT_Table*>* _second - second list
// #return Index of value in the first list which can be found in the second list
int XTT_TableList::hasCommonParent(QList<XTT_Table*>* _first, QList<XTT_Table*>* _second)
{
     for(int i=0;i<_first->size();++i)
          if(_second->contains(_first->at(i)))     
               return i;
          
     return -1;
}
// ---------------------------------------------------------------------------

// #brief Arranges tables on the scene
//
// #param QList<XTT_Table*>* _roots - list of tree roots
// #return No values returs
void XTT_TableList::createTree(QList<XTT_Table*>* _roots)
{
     QList<QList<XTT_Table*>*>* _nodes;
     QList<XTT_Table*>* _allNodes;
     QList<QList<float>*>* _heights;
     QList<float>* _widths;
     
     _nodes = new QList<QList<XTT_Table*>*>;
     _allNodes = new QList<XTT_Table*>;
     _heights = new QList<QList<float>*>;
     _widths = new QList<float>;
     
     _nodes->append(new QList<XTT_Table*>);
     for(int i=0;i<_roots->size();++i)
     {
          _allNodes->append(_roots->at(i));
          _nodes->at(0)->append(_roots->at(i));
     }
          
     _heights->append(new QList<float>);
     for(int i=0;i<_roots->size();++i)
          _heights->at(0)->append(_roots->at(i)->Height());
          
     // Search for root maximum width
     float mw = _roots->at(0)->Width();
     for(int i=1;i<_roots->size();++i)
          if(_roots->at(i)->Width() > mw)
               mw = _roots->at(i)->Width();
     _widths->append(mw);
     
     getTreeGeometry(0, _allNodes, _nodes, _heights, _widths);
     
     // Roots arrangemens
     float currentNodeX = 0.;
     float currentNodeY = 0.;
     for(int i=0;i<_roots->size();++i)
     {
          float root_ypos = (_heights->at(0)->at(0)/2.)-(_roots->at(i)->Height()/2.);
          _nodes->at(0)->at(i)->setLocalization(QPoint((int)currentNodeX, (int)(currentNodeY+root_ypos)));
          currentNodeY += (LEVEL_HEIGHT+_roots->at(i)->Height());
     }
     //currentNodeX -= (_widths->at(1)+HORIZONTAL_MARGIN);
     
     // Other nodes arrangemens
     for(int level=1;level<_nodes->size();++level)
     {     
          currentNodeX -= (_widths->at(level)+HORIZONTAL_MARGIN);
          
          // Obliczanie sumy szerokosci na danym poziomie dla danego rodzica
          XTT_Table*  currentParent = _nodes->at(level)->at(0);                                   // Bierzacy rodzic
          int sindex = 0;                                                                      // Index startowy potomkow danego rodzica
          float nodeHeight = .0;                                                                 // Suma wysokosci potomkow danego rodzica
          for(int pc=0;pc<_nodes->at(level)->size();++pc)
          {     
               bool ifsat = (pc == (_nodes->at(level)->size()-1));
               nodeHeight += _heights->at(level)->at(pc);
               QList<XTT_Table*>* parents1 = parents(currentParent);
               QList<XTT_Table*>* parents2 = NULL;
               if(!ifsat)
                    parents2 = parents(_nodes->at(level)->at(pc+1));
               if((ifsat) || (hasCommonParent(parents1, parents2) == -1))
               {
                    int eindex = pc;
                    
                    // Wyszukanie jednego z rodzicow wystepujacych na bierzacym poziomie
                    QList<XTT_Table*>* parents_1 = parents(_nodes->at(level)->at(eindex));
                    QList<XTT_Table*>* parents_2 = parents(currentParent);
                    int commonParentIndex = hasCommonParent(parents_1, parents_2);
                    if(commonParentIndex == -1)
                    {
                         QString params = "";
                         int ni = IndexOfID(currentParent->TableId());
                         params = "\nError params:\nCurrent Level = " + QString::number(level) + "\ncurrent table: " + fTables[ni]->TableId();
                         params += "\nindexOfTable = " + QString::number(ni);
                         QMessageBox::critical(NULL, "XTT_TableList::createTree", "Can't find parent on the higher level." + params);
                         return;
                    }
                         
                    // Ustawianie wszystkich wezlow
                    float parentTop = (float)parents_1->at(commonParentIndex)->Localization().y();
                    float parentHeight = (float)parents_1->at(commonParentIndex)->Height();
                    float parentCenter = parentTop+(parentHeight/2.);
                    delete parents_1;
                    delete parents_2;
                    
                    nodeHeight = (-nodeHeight)/2.;
                    for(int psi=sindex;psi<=eindex;++psi)
                    {
                         float realHalfHeight = (float)_nodes->at(level)->at(psi)->Height()/2.;
                         float artificialHeight = _heights->at(level)->at(psi);
                         float halfArtificialHeight = artificialHeight/2.;
                         
                         _nodes->at(level)->at(psi)->setLocalization(QPoint((int)currentNodeX, (int)(parentCenter+(nodeHeight+halfArtificialHeight-realHalfHeight))));
                         nodeHeight += artificialHeight;
                    }
                    
                    if(pc < (_nodes->at(level)->size()-1))
                         currentParent = _nodes->at(level)->at(pc+1);
                    sindex = eindex+1;
                    nodeHeight = 0.;
               }
               delete parents1;
               if(parents2 != NULL)
                    delete parents2;
          }
     }
     
     for(int i=0;i<_nodes->size();++i)
          delete _nodes->at(i);
     for(int i=0;i<_heights->size();++i)
          delete _heights->at(i);
     delete _nodes;
     delete _allNodes;
     delete _heights;
     delete _widths;
}
// ---------------------------------------------------------------------------

// #brief Analysis tree levels
//
// #param int currLevel - current level on which all parents should be found
// #param QList<XTT_Table*>* _allNodes - all added nodes
// #param QList<QList<XTT_Table*>*>* _nodes - list of nodes separated on levels
// #param QList<QList<float>*>* _heights - list of heights values for each level and node
// #param QList<float>* _widths - list of maximum widths of levels
// #return No values returs
void XTT_TableList::getTreeGeometry(int currLevel, QList<XTT_Table*>* _allNodes, QList<QList<XTT_Table*>*>* _nodes, QList<QList<float>*>* _heights, QList<float>* _widths)
{
// Wyszukiwanie potomkow wszystkich wlasciwosci na danym poziomie
     for(int i=0;i<_nodes->at(currLevel)->size();++i)
     {
          // Wyszukanie potomkow
          QList<XTT_Table*>* childList = childs(_nodes->at(currLevel)->at(i));
          
          /*
          // Wyswietlenie znalezionej listy potomkow
          QString strChilds = "Dla wezla " + QString::number(_nodes->at(currLevel)->at(i)->TableId()) + " znaleziono potomkow:\n";
          for(int i=0;i<childList->size();++i)
               strChilds += QString::number(childList->at(i)->TableId()) + ",";
          QMessageBox::critical(NULL, "HQEd", strChilds);
          */
          
          // Gdy lisc
          if(childList->size() == 0)
          {
               delete childList;
               continue;
          }
          
          // Wpisanie znaleznionych potomkow i ich szerokosci i wyszukanie maksymalnej wysokosci
          for(int c=0;c<childList->size();++c)
          {
               if(_allNodes->contains(childList->at(c)))
                    continue;
                    
               // Sprawdzanie czy sa wczytani wszyscy rodzice, jezeli nie to musimy sprawdzic czy nie jest to jakis cykl
               bool contML = false;
               QList<XTT_Table*>* parents1 = parents(childList->at(c));
               for(int parentIndex=0;parentIndex<parents1->size();++parentIndex)
               {
                    // Moze istniec kilka sytuacji osobliwych ktore sygnalizuja zaklocenie w normalnej postci drzewa
                    // Cykle i zaleznosci przechodnie i posrednie
                    // Trzeba je wykryc dla jaknajlepszego rozmieszczenie elemntow na scenie
                    // Jezeli nie wszyscy rodzice sa w liscie albo sa na ostatnim poziomie to mamy doczynienia
                    // z taka dziwna sytuacja
                    
                    // Jezeli jezeli ktorys z rodzicow jest na bierzacym poziomie to kontynujemy bo ten element zostanie dodany pozniej
                    if((_nodes->size() == (currLevel+2)) && (_nodes->at(currLevel+1)->contains(parents1->at(parentIndex))))
                    {
                         parentIndex = parents1->size();
                         contML = true;
                    }
                    
                    if(contML) continue;
                    
                    // Jezeli jakiegos rodzica nie ma jeszcze wsrod wezlow, to sprawdzamy czy, we wszystkich potomkach, naszego wezla on nie wystepuje
                    // Jezeli wystepuje to sprawdzamy rozmiary ich rodzin, jezeli nasz aktualny ma wieksza to go dodajemy
                    // Jezeli nie wystepuje to znaczy ze nie ma cyklu i dodamy go poniej jak sie pojawia wszyscy rodzice
                    if(!_allNodes->contains(parents1->at(parentIndex)))
                    {
                         QList<XTT_Table*>* family = findFamily(childList->at(c)->TableId());
                         if(!family->contains(parents1->at(parentIndex)))
                         {     
                              parentIndex = parents1->size();
                              contML = true;
                         }
                         
                         // Gdy rodzina zawiera rodzica - CYKL - dodajemy naszego aktualnego jezeli ma wieksza rodzina - jest wczesniej w drzewie
                         if((!contML) && (family->contains(parents1->at(parentIndex))))
                         {
                              QList<XTT_Table*>* family2 = findFamily(parents1->at(parentIndex)->TableId());
                              if(family2->size() > family->size())
                              {
                                   parentIndex = parents1->size();
                                   contML = true;
                              }
                              
                              delete family2;
                         }
                         
                         
                         delete family;
                    }
               }
               delete parents1;
               
               if(contML)
               {
                    //QString msg = "Dla rodzica " + _nodes->at(currLevel)->at(i)->parent()->id() + " nie dodano potomka " + childList->at(c)->parent()->id();
                    //QMessageBox::critical(NULL, "HQEd", msg);
                    continue;
               }
                    
               // Ewntualne powiekszanie list
               while(_nodes->size() < (currLevel+2))
               {
                    _nodes->append(new QList<XTT_Table*>);
                    _heights->append(new QList<float>);
                    _widths->append(0.);
               }
                    
               _allNodes->append(childList->at(c));
               _nodes->at(currLevel+1)->append(childList->at(c));
               _heights->at(currLevel+1)->append(childList->at(c)->Height()+LEVEL_HEIGHT);
               if(childList->at(c)->Width() > _widths->at(currLevel+1))
                    _widths->replace(currLevel+1, childList->at(c)->Width());
          }
          
          delete childList;
     }
     
     // Gdy na danym poziomie nie ma juz potomkow
     if(_nodes->size() < (currLevel+2))
          return;
          
     // Odpalenie funckji dla nizszych poziomow
     getTreeGeometry(currLevel+1, _allNodes, _nodes, _heights, _widths);
     
     /*
     QString t = "Utworzono drzewo (" + QString::number(currLevel) + "):";
     for(int i=0;i<_nodes->size();++i)
     {
          t += "\nPoziom " + QString::number(i) + ": ";
          for(int j=0;j<_nodes->at(i)->size();++j)
               t += QString::number(_nodes->at(i)->at(j)->TableId()) + ", ";
     }
     QMessageBox::critical(NULL, "HQEd", t); */
          
     // Teraz nastepuje poprawianie tymczasowych wysokosci wezlow
     float height_sum = 0.;
     XTT_Table* currentParent = _nodes->at(currLevel+1)->at(0);
          
     for(int i=0;i<_heights->at(currLevel+1)->size();++i)
     {
          bool ifsat = (i ==(_heights->at(currLevel+1)->size()-1));
          QList<XTT_Table*>* parents1 = parents(currentParent);
          QList<XTT_Table*>* parents2 = NULL;
          if(!ifsat)
               parents2 = parents(_nodes->at(currLevel+1)->at(i+1));
          height_sum += _heights->at(currLevel+1)->at(i);
          if((ifsat) || (hasCommonParent(parents1, parents2) == -1))
          {
               // ---- Zapisanie wyliczonej szerokosci ----
               // Wyszukanie rodzica w liscie na wyzszym poziomie
               int pindex = hasCommonParent((*_nodes)[currLevel], parents1);
                         
               // Gdy nie znajdziemy rodzica w liscie powyzej to straszny blad
               if(pindex == -1)
               {
                    QString params = "";
                    int ni = IndexOfID(currentParent->TableId());
                    params = "\nError params:\nCurrent Level = " + QString::number(currLevel) + "\ncurrent parent: " + fTables[ni]->TableId();
                    params += "\nindexOfTable = " + QString::number(ni);
                    QMessageBox::critical(NULL, "XTT_TableList::getTreeGeometry", "Can't find parent on the higher level." + params);
                    return;
               }
                    
               // Zapisanie wyliczonej szerokosci, jezeli wieksza od oryginalnej
               if(height_sum > _heights->at(currLevel)->at(pindex))
                    _heights->at(currLevel)->replace(pindex, height_sum);
               
               // Zapisanie nowych parametrow
               if(i < (_heights->at(currLevel+1)->size()-1))
                    currentParent = _nodes->at(currLevel+1)->at(i+1);
               height_sum = 0.;
          }
          delete parents1;
          if(parents2 != NULL)
               delete parents2;
     }     
}
// ---------------------------------------------------------------------------

// #brief Function that searches for best connection for Fact table
//
// #param __factTable - pointer to thr fact table
// #param __msgs - reference to the String of messages
// #return 0 on succes greater than zero on error(s)
int XTT_TableList::connectFactTable(XTT_Table* __factTable, QString& __msgs)
{
     int result = 0;

     if(__factTable->TableType() != 0)
     {
          __msgs += "\n" + hqed::createErrorString(__factTable->TableId(), "", "The table \'" + __factTable->Title() + "\' is not fact table.", 0, 2);
          return 1;
     }
     
     // Searching for link destination
     QList<XTT_Attribute*>* loa = __factTable->ContextAtributes(4, false);
     QList<XTT_Table*>* tbls = findTables(1, loa, false);
     XTT_Table* __destTable = NULL;
     
     // When table that has attribute in conditional context not exist
     if(tbls->size() == 0)
     {
          delete tbls;
          tbls = findTables(4, loa, false);
     }
     
     // When table is not found
     if(tbls->size() == 0)
     {
          __msgs += "\n" + hqed::createErrorString(__factTable->TableId(), "", "Cannot find appropriate table for state \'" + __factTable->Title() + "\'", 0, 2);
          result++;
     }
     
     // If some tables are found
     if(tbls->size() > 0)
     {
          // searching for table that has at least one row
          int tindex = 0;
          do
          {
               __destTable = tbls->at(tindex);
               tindex++;
          }
          while((tindex<tbls->size()) && (tbls->at(tindex)->RowCount()==0));
     }
     
     // Searching for the oldest non Fact parent...
     if((__destTable != NULL) && (__destTable->RowCount() > 0))
     {
          QList<XTT_Table*>* rtstbls = findFamily(__destTable->TableId());
          for(int i=0;i<rtstbls->size();++i)
          {        
               if((rtstbls->at(i)->TableType() != 0) && (rtstbls->at(i)->RowCount() > 0))
                    __destTable = rtstbls->at(i);
          }
          delete rtstbls;
     }

     if((__destTable != NULL) /*&& (__destTable->RowCount() > 0)*/)
     {
          XTT_Connections* newconn = ConnectionsList->Add();
          newconn->setSrcTable(__factTable->TableId());
          newconn->setDesTable(__destTable->TableId());
          newconn->setSrcRow(__factTable->Row(0)->RowId());
          newconn->setDesRow("");
          //newconn->setDesRow(__destTable->Row(0)->RowId());    // when we want to connect to the first row of the destination table
     }
     
     delete loa;
     delete tbls;
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function that returns if the tables objects definitions are correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_TableList::tablesVerification(QString& __error_message)
{
     bool res = true;
     
     // Checking general model
     QString traceErrors;
     QList<XTT_Row*>* loar = rows();                   // list of all rows
     QList<XTT_Row*>* lotr = xtt::trace(traceErrors);  // list of traced rows
     
     // Checking for inaccesible rows and connections
     QList<XTT_Row*> loiar = hqed::listSubtraction(*loar, *lotr);
     delete loar;
     delete lotr;
     //res = traceErrors.isEmpty() && (loiar.empty()) && res;
     __error_message += traceErrors;
     
     if(Settings->massageAboutInaccesibleRow())
     {
          for(int i=0;i<loiar.size();++i)
          {
               XTT_Table* t = findRow(loiar.at(i));
               if((t == NULL) || (loiar.at(i)->Length() == 0))
                    continue;
                
               // Message about inccesible row
               __error_message += hqed::createErrorString(t->TableId(), loiar.at(i)->Cell(0)->CellId(), "Inaccessible row.", 1, 5);
               
               // If the row is inaccesible, then all the connections from this row are also inaccesible
               QList<XTT_Connections*>* crcl = loiar.at(i)->OutConnection();
               for(int c=0;c<crcl->size();++c)
                    __error_message += hqed::createErrorString(t->TableId(), loiar.at(i)->Cell(0)->CellId(), "Inaccesible connection \'" + crcl->at(c)->Label() + "\'. The connection cannot be used because of inaccesible row.", 1, 1);
               delete crcl;
          }
     }
     
     // Checking tables
     for(unsigned int a=0;a<fCount;a++)
     {
          // sprawdzanie czy table jest polaczona z jakas inna
          int sum = 0;
          QList<XTT_Connections*>* li = fTables[a]->inConnectionsList();
          sum += li->size();
          delete li;
          li = fTables[a]->outConnectionsList();
          sum += li->size();
          delete li;
          
          if((sum == 0) && (fCount > 0))
          {
               QString _Error = "Orphan table detected: Table \'" + fTables[a]->Title() + "\' is not connected with any other.";
               __error_message += hqed::createErrorString(fTables[a]->TableId(), "", _Error, 1, 3);
               //res = false;
          }
          
          res = fTables[a]->tableVerification(__error_message) && res;
     }
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_TableList::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     for(unsigned int a=0;a<fCount;a++)
          res = fTables[a]->updateTypePointer(__old, __new) && res;
     RefreshAll();
     
     return res;
}
// ---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_TableList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fCount;a++)
               res = fTables[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fCount;a++)
               res = fTables[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the rows of the tables to the prolog code.
//
// #return The prolog code that represents the rules as string.
QString XTT_TableList::rulesToProlog(void)
{
     QString result = "";
     for(unsigned int i=0;i<fCount;++i)
          if(fTables[i]->TableType() != 0)
               result += fTables[i]->rulesToProlog() + "\n";
          
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function that maps the tables schemas to the prolog code.
//
// #return The prolog code that represents the schemas of the tables as string.
QString XTT_TableList::schemasToProlog(void)
{
     QString result = "";
     for(unsigned int i=0;i<fCount;++i)
          if(fTables[i]->TableType() != 0)
               result += fTables[i]->schemaToProlog() + "\n";
          
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 2.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returns false.
bool XTT_TableList::FromXTTML_2_0(QDomElement& root)
{
     if(root.tagName().toLower() != "xtt_list_table")
          return false;

     bool res = true;
     
     // Wyczyszczenie pojemnika
     Flush();

     // Reszta zakladamy ze jest ok

     QDomElement child = root.firstChildElement("xtt_table");
     while(!child.isNull())
     {
          // Kiedy linia konczaca linie danych atrybutu
          // Dodajemy nowy atrybut

          Add();

          // Wczytujemy dane z pliku
          res = fTables[fCount-1]->FromXTTML_2_0(child) && res;
          
          // Jezeli blad to nie czytamy dlaej
          if(!res)
               return false;
                    
          // Wczytujemy kolejna tabele
          child = child.nextSiblingElement("xtt_table");
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 format.
//
// #param __csvdoc - writing device
// #return List of all data in drools5 format.
void XTT_TableList::out_Drools_5_0_DecTab(QTextStream* __csvdoc)
{
     *__csvdoc << ",,,,\n";
     *__csvdoc << ",,\"RuleSet\",\"com.sample\",,\n";
     *__csvdoc << ",,,,\n";

     for(unsigned int i=0;i<Count();++i)
     {
          Table(i)->out_Drools_5_0_DecTab(__csvdoc);
          *__csvdoc << ",,,,,,\n";
          *__csvdoc << ",,,,,,\n";
     }
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_TableList::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(__mode != 4)
     {
          *res << "<xtt>";
          for(unsigned int i=0;i<Count();++i)
          {
               if(Table(i)->TableType() == 0)     
                    continue;
               
               QStringList* tmp = Table(i)->out_HML_2_0();
               for(int j=0;j<tmp->size();++j)
                    *res << "\t" + tmp->at(j);
               delete tmp;
          }
          *res << "</xtt>";
     }
     
     if((__mode == 4) && (IndexOfType(0) > -1))
     {
          *res << "<states>";
          
               QStringList tt = TablesTitles(true);
               for(int i=0;i<tt.size();++i)
               {
                    QList<XTT_Table*>* tl = findTables(tt.at(i), 0);
                    if(tl->empty())
                    {
                         delete tl;
                         continue;
                    }
                    
                    *res << "\t<state id=\"" + tl->at(0)->TableId() + "\" name=\"" + tt.at(i) + "\">";
                    for(int j=0;j<tl->size();++j)
                    {
                         QStringList* tmp = tl->at(j)->out_HML_2_0(__mode);
                         for(int t=0;t<tmp->size();++t)
                              *res << "\t" + tmp->at(t);
                         delete tmp;
                    }
                    *res << "\t</state>";
                    delete tl;
               }
          *res << "</states>";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_TableList::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(__mode != 4)
     {
          _xmldoc->writeStartElement("xtt");
          for(unsigned int i=0;i<Count();++i)
               if(Table(i)->TableType() != 0)          // 0 - denotes fact table, that is converted to state element
                    Table(i)->out_HML_2_0(_xmldoc);
          _xmldoc->writeEndElement();
     }
     
     if((__mode == 4) && (IndexOfType(0) > -1))
     {
          _xmldoc->writeStartElement("states");
          
               QStringList tt = TablesTitles(true);
               for(int i=0;i<tt.size();++i)
               {
                    QList<XTT_Table*>* tl = findTables(tt.at(i), 0);
                    if(tl->empty())
                    {
                         delete tl;
                         continue;
                    }
                    
                    _xmldoc->writeStartElement("state");
                    _xmldoc->writeAttribute("id", tl->at(0)->TableId());
                    _xmldoc->writeAttribute("name", tt.at(i));
                    for(int j=0;j<tl->size();++j)
                          tl->at(j)->out_HML_2_0(_xmldoc, __mode);
                    _xmldoc->writeEndElement();
                    delete tl;
               }
          _xmldoc->writeEndElement();
     }
}
// -----------------------------------------------------------------------------

// #brief Function saves all data into Drools 5.0 format.
// #author Lukasz Lysik (llysik@gmail.com)
//
// #param _xmldoc Pointer to document.
// #return No return value.
void XTT_TableList::out_Drools_5_0(QXmlStreamWriter* _xmldoc)
{
     // array of Drools Flow connection IDs
     QList<QPoint> dfConnections;
     QList<unsigned int> startTables;

     _xmldoc->writeStartElement("nodes");
     _xmldoc->writeStartElement("start");
     _xmldoc->writeAttribute("id", "1");
     _xmldoc->writeAttribute("name", "Start");
     _xmldoc->writeAttribute("x", "10");
     _xmldoc->writeAttribute("y", "10");
     _xmldoc->writeAttribute("width", "48");
     _xmldoc->writeAttribute("height", "48");
     _xmldoc->writeEndElement();

     // Drools Flow start ID for ruleSets
     int DfID = 2;

     for(unsigned int i=0;i<Count();++i)
     {
          Table(i)->setDfId(DfID++);
     }     

     for(unsigned int i=0;i<Count();++i)
     {
          Table(i)->out_Drools_5_0(_xmldoc);

          //std::cout << "PROCESSING " << Table(i)->Title().toStdString() << std::endl;
          //std::cout << "i = " << i << std::endl;
          //std::cout << "DfID = " << DfID << std::endl;
          //std::cout << "TableId() = " << Table(i)->TableId().toStdString() << std::endl;
          //std::cout << "InConn->count() = " << ConnectionsList->listOfInConnections_Table(Table(i)->TableId())->count() << std::endl;
          //std::cout << "OutConn->count() = " << ConnectionsList->listOfOutConnections_Table(Table(i)->TableId())->count() << std::endl;

          // if Halt table (last in the flow)
          if(ConnectionsList->listOfOutConnections_Table(Table(i)->TableId())->count() == 0)
          {
               int haltNodeId = DfID++;
               _xmldoc->writeStartElement("end");
               _xmldoc->writeAttribute("id", QString::number(haltNodeId));
               _xmldoc->writeAttribute("name", "End");
               _xmldoc->writeAttribute("x", "100");
               _xmldoc->writeAttribute("y", "100");
               _xmldoc->writeAttribute("width", "48");
               _xmldoc->writeAttribute("height", "48");
               _xmldoc->writeEndElement();

               dfConnections.append(QPoint(Table(i)->DfId(),haltNodeId));
               //std::cout << "[3] Adding connection from " << Table(i)->DfId() << " to " << haltNodeId << std::endl;
          }

          QList<QString> *inConn = ConnectionsList->listOfInConnections_Table(Table(i)->TableId());
          if(inConn->count() == 0)
          {
               // Add to list of Start Tables
               // std::cout << "Appending table with DfId() = " << Table(i)->DfId() << std::endl;
               startTables.append(Table(i)->DfId());
          }
          else if(inConn->count() == 1)
          {
               int srcTableId = Table(IndexOfID(inConn->at(0)))->DfId();

               dfConnections.append(QPoint(srcTableId,Table(i)->DfId()));
               //std::cout << "[2] Adding connection from " << srcTableId << " to " << Table(i)->DfId() << std::endl;
          }
          else if(inConn->count() > 1)
          {
               // Create Join node
               int joinNodeId = DfID++;
               _xmldoc->writeStartElement("join");
               _xmldoc->writeAttribute("id", QString::number(joinNodeId));
               _xmldoc->writeAttribute("name", "Join");
               _xmldoc->writeAttribute("x", "100");
               _xmldoc->writeAttribute("y", "100");
               _xmldoc->writeAttribute("width", "80");
               _xmldoc->writeAttribute("height", "40");
               _xmldoc->writeAttribute("type", "1"); // type: AND
               _xmldoc->writeEndElement();

               //std::cout << "[1] Adding connection from " << joinNodeId << " to " << Table(i)->DfId() << std::endl;
               dfConnections.append(QPoint(joinNodeId,Table(i)->DfId()));

               for(int j=0; j<inConn->count(); j++)
               {
                    int srcTableId = Table(IndexOfID(inConn->at(j)))->DfId();
                    //std::cout << "[0] Adding connection from " << srcTableId << " to " << joinNodeId << std::endl;
                    dfConnections.append(QPoint(srcTableId,joinNodeId));
               }
          }

     } // for

     // Create Split node after Start if more than one start table
     if(startTables.count() > 1)
     {
          // Create Split node
          int splitNodeId = DfID++;
          _xmldoc->writeStartElement("split");
          _xmldoc->writeAttribute("id", QString::number(splitNodeId));
          _xmldoc->writeAttribute("name", "Split");
          _xmldoc->writeAttribute("x", "100");
          _xmldoc->writeAttribute("y", "100");
          _xmldoc->writeAttribute("width", "80");
          _xmldoc->writeAttribute("height", "40");
          _xmldoc->writeAttribute("type", "1"); // type: AND
          _xmldoc->writeEndElement();

          // Start(id = 1) -> splitNode
          dfConnections.append(QPoint(1,splitNodeId));

          for(int j=0; j<startTables.count(); j++)
          {
               //std::cout << "[10]Adding connection from " << splitNodeId << " to " << startTables.at(j) << std::endl;
               dfConnections.append(QPoint(splitNodeId,startTables.at(j)));
          }
     }
     else if(startTables.count() == 1)
     {
          //std::cout << "[11]Adding connection from " << 1 << " to " << startTables.at(0) << std::endl;
          dfConnections.append(QPoint(1,startTables.at(0)));
     }

     _xmldoc->writeEndElement();

     _xmldoc->writeStartElement("connections");
     for(int i=0; i<dfConnections.count(); i++)
     {
          QPoint p = dfConnections.at(i);
          _xmldoc->writeStartElement("connection");
          _xmldoc->writeAttribute("from",QString::number(p.x()));
          _xmldoc->writeAttribute("to",QString::number(p.y()));
          _xmldoc->writeEndElement();
     }
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_TableList::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "xtt";
     QString childElementName = "table";
     
     QDomElement DOMxtt = __root.firstChildElement(elementName);
     if(DOMxtt.isNull())
          return 0;

     QDomElement DOMtable = DOMxtt.firstChildElement(childElementName);
     while(!DOMtable.isNull())
     {
          // Reading type data
          QString tid = DOMtable.attribute("id", "");
          QString tname = DOMtable.attribute("name", "");

          if(tid == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined table \'" + tname + "\' identifier. The object will not be read." + hqed::createDOMlocalization(DOMtable), 0, 2);
               DOMtable = DOMtable.nextSiblingElement(childElementName);
               result++;
               continue;
          }
          if(tname == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined table name.\nTable id = \'" + tid + "\'." + hqed::createDOMlocalization(DOMtable), 1, 2);
               result++;
          }

          QDomElement DOMtableschm = DOMtable.firstChildElement("schm");
          if(DOMtableschm.isNull())
          {
               __msg += "\n" + hqed::createErrorString("", "", "A required tag \'schm\' not found in table \'" + tname + "\' definition.\nThe table will not be read." + hqed::createDOMlocalization(DOMtable), 0, 2);
               result++;
               DOMtable = DOMtable.nextSiblingElement(childElementName);
               continue;
          }

          XTT_Table* table = Add();
          result += table->in_HML_2_0(DOMtable, __msg);

          DOMtable = DOMtable.nextSiblingElement(childElementName);
     }

     // Loading connections
     result += ConnectionsList->loadConnections(__msg);

     return result;
}
// -----------------------------------------------------------------------------
