/*
 *        $Id: XTT_Cell.cpp,v 1.41.4.7 2012-01-02 04:48:33 rkl Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */


#include <QtGui>
#include <QDialog>
#include <QObject>

#include <math.h>
#include <stdarg.h>

#include "../hSet.h"
#include "../hType.h"
#include "../hValue.h"
#include "../hDomain.h"
#include "../hSetItem.h"
#include "../hTypeList.h"
#include "../hFunction.h"
#include "../hExpression.h"
#include "../hFunctionList.h"
#include "../hMultipleValue.h"
#include "../hFunctionArgument.h"

#include "../../V/XTT/GTable.h"
#include "../../C/MainWin_ui.h"
#include "../../C/Settings_ui.h"
#include "../../C/XTT/widgets/ErrorsListWidget.h"
#include "../../namespaces/ns_hqed.h"
#include "XTT_TableList.h"
#include "XTT_Table.h"
#include "XTT_Attribute.h"
#include "XTT_AttributeGroups.h"
#include "XTT_Cell.h"
// -----------------------------------------------------------------------------

QString map_key_base_name = "expression_unique_key";

// Definicja zmiennych globalnych zawierajacych informacje o reprezentacji operatorow porownania w programie i pliku XML
const QStringList smbl_opers = (QStringList() << "=" << "!=" << ">=" << ">" << "<=" << "<" << "nin" << "in");
const QStringList text_opers = (QStringList() << "e" << "ne" << "ge" << "g" << "le" << "l" << "nin" << "in");
// -----------------------------------------------------------------------------

// #brief Constructor XTT_Cell class.
XTT_Cell::XTT_Cell(void)
{
     fParent = "";
     fCellID = "";
     fCellIdx = 0;
     fAttrType = NULL;
     fContent = new hMultipleValue(EXPRESSION);
     fCellError = "";
     fIgnore = false;
     fNoError = true;
     fContext = 0;
     fChanged = false;
     favr = Settings->xttAttrToAttrComp();
     fIsSelected = false;
}
// -----------------------------------------------------------------------------

// #brief Constructor XTT_Cell class.
//
// #param _param Pointer to the parent of the cell.
// #param _id Cell id.
// #param _context Cell context.
XTT_Cell::XTT_Cell(QString _parent, unsigned int _id, int _context)
{
     fParent = _parent;
     fCellID = idPrefix() + QString::number(_id);
     fCellIdx = _id;
     fAttrType = NULL;
     fContent = new hMultipleValue(EXPRESSION);
     fCellError = "";
     fIgnore = false;
     fNoError = true;
     fContext = _context;
     fChanged = false;
     favr = Settings->xttAttrToAttrComp();
     fIsSelected = false;
}
// -----------------------------------------------------------------------------

XTT_Cell::~XTT_Cell(void)
{
     // Usuniecie z listy bledow wyswietlanej w oknie glownym, dokowalnym
     MainWin->xtt_errorsList->deleteItem(fParent, fCellID);
     AttributeGroups->Group(2)->Delete(AttributeGroups->Group(2)->IndexOfPointer(fAttrType), false);
     
     delete fContent;
}
// -----------------------------------------------------------------------------

// #brief Function that makes the copy of the current object
//
// #param __destination - a destination object where all values will be copied
// #return Pointer to the current object
XTT_Cell* XTT_Cell::copyTo(XTT_Cell* __destination)
{
     XTT_Table* tParent = TableParent();
     if(tParent == NULL)
          return NULL;
          
     XTT_Cell* result = __destination;
     if(__destination == NULL)
          result = new XTT_Cell;
     
     delete result->fContent;
     
     result->fParent = fParent;
     result->fCellID = tParent->nextCellId(true);
     result->fAttrType = fAttrType;
     result->fContent = fContent->copyTo();
     result->fCellError = "";
     result->fIgnore = false;
     result->fNoError = true;
     result->fContext = fContext;
     result->fChanged = false;
     result->favr = favr;
     result->fIsSelected = false;
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function thatcreates a default content of the cell
//
// #return No values return
void XTT_Cell::createDefaultContent(void)
{
     if(fAttrType == NULL)
          return;

     // We destroy current content
     delete fContent;
     fContent = new hMultipleValue(EXPRESSION);

     // Searching for appropriate function
     hFunction* function;
     int functionindex = -1;

     if(fContext == 1)
          functionindex = funcList->indexOf("hbsin", true);
     if(fContext == 4)
          functionindex = funcList->indexOf("void_assign_operator", true);
     if(fContext == 5)
          functionindex = funcList->indexOf("void_action_operator", true);
     if(functionindex == -1)
          return;
     function = funcList->at(functionindex)->copyTo();
     hType* reqt = Context()==1?typeList->at(hpTypesInfo.iopti(BOOLEAN_BASED)):typeList->specialType(VOID_BASED);
     fContent->expressionField()->setRequiredType(reqt);
     fContent->expressionField()->setFunction(function);

     // now we should set function arguments
     // first argument
     hFunctionArgument* _frstArg = new hFunctionArgument;
     _frstArg->setVisible(false);
     _frstArg->setEditable(false);
     hMultipleValue firstArgVal(fAttrType);
     _frstArg->setValue(&firstArgVal, true);
     _frstArg->setType(hFunction::typeFunctionRepresentation(fAttrType->Type()) | FUNCTION_TYPE__MULTI_VALUED);
     _frstArg->findCompatibleTypes(typeList);
     function->setArgument(0, _frstArg);

     // second argument
     hFunctionArgument* _scndArg = new hFunctionArgument;
     int secondArgumentMultiplicity = fAttrType->multiplicity()?FUNCTION_TYPE__MULTI_VALUED:FUNCTION_TYPE__SINGLE_VALUED;
     if(fContext == 1)
          secondArgumentMultiplicity = FUNCTION_TYPE__MULTI_VALUED;
     if(fContext == 1) {
          hMultipleValue secondArgVal(hSet::makeAnyValue(fAttrType->Type()));
          _scndArg->setValue(&secondArgVal, true);
          //_scndArg->setValue(new hMultipleValue(MULTIPLE_VALUE_TYPE_ANY_VALUE), false);
     }
     if(fContext == 4) {
          hMultipleValue secondArgVal(fAttrType->Type(), new hValue(fAttrType), !fAttrType->multiplicity());
          _scndArg->setValue(&secondArgVal, true);
     }
     if(fContext == 5) {
          hMultipleValue secondArgVal(fAttrType->Type(), new hValue(""), !fAttrType->multiplicity());
          _scndArg->setValue(&secondArgVal, true);
     }
     _scndArg->setType(hFunction::typeFunctionRepresentation(fAttrType->Type()) | secondArgumentMultiplicity);
     _scndArg->findCompatibleTypes(typeList);
     function->setArgument(1, _scndArg);
}
// -----------------------------------------------------------------------------

// #brief Function that changes the cell operator
//
// #param __operatorname - name of a new cell operator
// #return No values return
void XTT_Cell::setCellOperator(QString __operatorname)
{
     int operindex = funcList->indexOf(__operatorname, true);
     if(operindex == -1)
          return;
     
     setCellOperator(funcList->at(operindex)->copyTo(), false);
}
// -----------------------------------------------------------------------------

// #brief Function that changes the cell operator
//
// #param __operator - new cell operator
// #param __makeCopy - indicates if the function must make a copy of the function object
// #return No values return
void XTT_Cell::setCellOperator(hFunction* __operator, bool __makeCopy)
{
     if((fAttrType == NULL) || (__operator == NULL))
          return;

     // Searching for appropriate function
     hFunction* function = __operator;
     if(__makeCopy)
          function = __operator->copyTo();
     fContent->expressionField()->setFunction(function);
     
     XTT_Table* tParent = TableParent();
     if(tParent != NULL)
          tParent->GraphicTable()->RePosition();
}
// -----------------------------------------------------------------------------

// #brief Function returns the strContent of the cell that comes from xttml file
//
// #return the strContent of the cell that comes from xttml file
QString XTT_Cell::strContent(void)
{
     return fStrContent;
}
// -----------------------------------------------------------------------------

// #brief Function retrieves contents of the cell.
//
// #return Contents of the cell.
hMultipleValue* XTT_Cell::Content(void)
{
     return fContent;
}
// -----------------------------------------------------------------------------

// #brief Function sets context of the cell.
//
// #param _ctx Cell index.
// #return No return value.
bool XTT_Cell::setContext(unsigned int _ctx)
{
     if((((int)_ctx) < 0) || (_ctx > CONTEXT_COUNT))
          return false;

     // Ustawiamy nowy atrybut typu action
     if((_ctx == 5) && (fContext != 5))
          setAttrType(AttributeGroups->CreateNewActionArgument());

     // Usuwamy atrybut typu action
     if((_ctx != 5) && (fContext == 5))
     {
          AttributeGroups->Group(2)->Delete(AttributeGroups->Group(2)->IndexOfPointer(fAttrType), false);
          setAttrType(NULL);
     }
     fContext = _ctx;
     
     fChanged = true;

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function sets type of the attribute.
//
// #param _xtt_attr_type Pointer to the type of the attribute.
// #return No return value.
void XTT_Cell::setAttrType(XTT_Attribute* _xtt_attr_type)
{
     if((Context() == 5) && (_xtt_attr_type != fAttrType) && (fAttrType != NULL))
          AttributeGroups->Group(2)->Delete(AttributeGroups->Group(2)->IndexOfPointer(fAttrType), false);

     fAttrType = _xtt_attr_type; 
     if(fAttrType != NULL)
          fContent->setParentType(fAttrType->Type());
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets contents of the cell.
//
// #param _content Contents of the cell.
// #return No return value.
void XTT_Cell::setContent(hMultipleValue* _content)
{ 
     if(_content == NULL)
          return;
          
     if((fContent != _content) && (fContent != NULL))
          delete fContent;
     
     fContent = _content; 
     
     // Checking for errors
     hMultipleValueResult mvr = fContent->value();
     if(!mvr.succes())
          setCellError(mvr.error());
     
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets whether the cell should be ignored(transparent).
//
// #param _i Determines whether the cell should be ignored(True), otherwise (false).
// #return No return value.
void XTT_Cell::setIgnore(bool _i)
{ 
     fIgnore = _i; 
     fChanged = true;
}
// -----------------------------------------------------------------------------

// #brief Function sets the mode of attribute value reference.
//
// #param _m Enable mode of attribute comparing(True), disable(False).
// #return No return value.
void XTT_Cell::setAVR(bool _nv)
{
     favr = _nv;
}
// -----------------------------------------------------------------------------

// #brief Function retrieves decision whether the cell was changed.
//
// #return True when the cell was changed, otherwise function returns false.
bool XTT_Cell::Changed(void)
{
     return fChanged;
}
// -----------------------------------------------------------------------------

// #brief Function decides whether the cell was modyfied.
//
// #param _n True when the cell was modyfied, otherwise false.
// #return No return value.
void XTT_Cell::setChanged(bool _c)
{     
     fChanged = _c;
}
// ---------------------------------------------------------------------------

// #brief Function selects or deselects the cell on the scene.
//
// #param _isSelected Determines whether the cell is selected.
// #return No return value.
void XTT_Cell::setSelected(bool _isSelected)
{
     fIsSelected = _isSelected;
     
     XTT_Table* parent = TableParent();
     if(parent == NULL)
          return;
          
     parent->GraphicTable()->ensureVisible();
}
// ---------------------------------------------------------------------------

// #brief Function retrieves decision whether the cell is selected.
//
// #return True when the cell is selected, otherwise function returns false.
bool XTT_Cell::isSelected(void)
{
     return fIsSelected;
}
// ---------------------------------------------------------------------------

// #brief Function finds and retrieves pointer to the table parent.
//
// #return Pointer to the table parent.
XTT_Table* XTT_Cell::TableParent(void)
{
     for(unsigned int i=0;i<TableList->Count();i++)
          if(TableList->Table(i)->TableId() == fParent)
               return TableList->Table(i);

     return NULL;
}
// -----------------------------------------------------------------------------

// #brief Function retrieves decision whether the contents of the cell is correct.
//
// #return True when the contents of the cell is correct, otherwise function returns false.
bool XTT_Cell::isCorrectSyntax(void)
{
     return fNoError;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the cell object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_Cell::cellVerification(QString& __error_message)
{
     fNoError = true;
     QString tmp_msgs;
     QString single_error;

     setCellError("");        // removing error messages from this cell

     if(TableParent() == NULL)
     {
          __error_message += hqed::createErrorString(fParent, fCellID, "Cell " + fCellID + " has not defined parent table", 0, 0);
          return fNoError = false;
     }

     XTT_Table* tp = TableParent();
     
     if(tp == NULL)
     {
          single_error = "Cannot find cell's parent table.";
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }
     
     int cellRow = tp->indexOfRow(this);
     int cellCol = tp->indexOfCol(this);
     QString cell_localization = "Cell in table " + tp->Title() + "(" + QString::number(cellRow) + ", " + QString::number(cellCol) + ")";

     if(fAttrType == NULL)
     {
          single_error = "Undefined cell attribute: " + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          fNoError = false;
     }
     
     if(fContent == NULL)
     {
          single_error = "Undefined cell content: " + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }

     if(fContent->type() != EXPRESSION)
     {
          single_error = "Incorect cell content type. Must be an expression.\n" + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }

     // W czesci warunkowej nie wolno u�ywa� z�o�onych wyra�e�
     if((Context() == 1) && 
        (fContent->expressionField()->function() != NULL) &&
        (fContent->expressionField()->function()->argCount() >= 2) &&
        (fContent->expressionField()->function()->argument(1)->isExpressionUsed()))
     {
          single_error = "The conditional context can not contain any expression.\n" + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }
     
     // W tabeli inicjalizacyjnej nie moga wystepowac zadne wyrazenia
     if((tp->TableType() == 0) && 
        (fContent->expressionField()->function() != NULL) &&
        (fContent->expressionField()->function()->argCount() >= 2) &&
        (fContent->expressionField()->function()->argument(1)->isExpressionUsed()))
     {
          single_error = "The fact table cannot contain any expressions.\n" + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }

     if(!fContent->expressionField()->expressionVerification(tmp_msgs))
     {
          single_error = "Error formula in " + cell_localization + ":" + tmp_msgs;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }

     // Jezeli wylaczona jest porownywanie pomiedzy atrybutami to mozemy na probe odpalic komorke
     bool avr = false;
     if(fContent->expressionField()->function()->argCount() >= 2)
          avr = fContent->expressionField()->function()->argument(1)->isAVRused();

     // W czesci warunkowej nie wolno uzywac odniesien do atrybutow
     if((Context() == 1) && (avr))
     {
          single_error = "Attribute reference used in conditional context.\n" + cell_localization;
          __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
          return fNoError = false;
     }
     
     // Sprawdzanie czy wartosc wyrazenia w komorce jest zgodna z dziedzina atrybutu przypisanego do kolumny
     if((fContent->expressionField()->function() != NULL) &&
        (fContent->expressionField()->function()->argCount() >= 2))
     {
          // Kiedy blad podczas obliczania wartosci
          hMultipleValueResult hmvr = fContent->expressionField()->function()->argument(1)->value()->value();
          if(!(bool)hmvr)
          {
               single_error = "Error during cell expression calculation.\n" + cell_localization;
               __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
               return fNoError = false;
          }
          
          // Sprawdzanie poprawnosci zbioru
          if(fAttrType->Type()->isSetCorrect(hmvr.toSet(false), true) != H_TYPE_ERROR_NO_ERROR)
          {
               single_error = "Incorrect result of cell expression calculation.\nError message: " + fAttrType->Type()->lastError() + "\n" + cell_localization;
               __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
               return fNoError = false;
          }
     }

     if(((!avr) && (Settings->xttExecuteCellWhenOff())) || ((avr) && (Settings->xttExecuteCellWhenOn())))
     {
          // Odpalenie komorki
          int execRes = Execute(false);
          
          // Jezeli odpalenie komorki sie nie udalo
          if(execRes == -1)
          {
               single_error = CellError(false);
               __error_message += hqed::createErrorString(fParent, fCellID, single_error, 0, 0);
               return fNoError = false;
          }
     }

     return fNoError;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_Cell::updateTypePointer(hType* __old, hType* __new)
{
     return fContent->updateTypePointer(__old, __new);
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Cell::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = fContent->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          res = fContent->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function sets cell error.
//
// #param _cellError Text contains error message.
// #return No return value.
void XTT_Cell::setCellError(QString _cellError)
{
     fCellError = _cellError;
     fNoError = _cellError == "";
     
     // Dodanie do listy bledow wyswietlanej w oknie glownym, dokowalnym
     MainWin->xtt_errorsList->deleteItem(fParent, fCellID);
     
     if(_cellError != "")
          MainWin->xtt_errorsList->parseMessages(hqed::createErrorString(fParent, fCellID, _cellError));
}
// -----------------------------------------------------------------------------

// #brief Function retrieves last cell error.
//
// #param addLocalizationInfo Determines whether information about error localization should be added.
// #return Last cell error.
QString XTT_Cell::CellError(bool addLocalizationInfo)
{
     if(!addLocalizationInfo)
          return fCellError;
          
     XTT_Table* parent = TableList->_IndexOfID(fParent);

     // Wyszukanie wiersza i kolumny tabeli w ktorych wystepuje komorka
     int cell_row = -1;
     int cell_col = -1;
     if(parent != NULL)
     {
          for(unsigned int row=0;row<parent->RowCount();row++)
               if(parent->Row(row)->indexOfCellID(fCellID) > -1)
               {
                    cell_row = row;
                    cell_col = parent->Row(row)->indexOfCellID(fCellID);
               }
     }
          
     if((cell_row == -1) || (cell_col == -1))
          return fCellError;
          
     QString res = fCellError + "\n";
     
     // Zapisanie naglowka tabeli
     QString tableTitle = parent->Title();
     
     // Zapisanie wspolrzednych tabeli
     QString cr = QString::number(cell_row+1);
     QString cc = QString::number(cell_col+1);
          
     // Dopisanie parametrow bledu
     res = res + "Error in table " + tableTitle + ", in row " + cr + ", column " + cc + ".";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function analyzes syntax and makes the operation.
//
// #param expr Expression.
// #return True when expression is OK, otherwise function returnes false.
bool XTT_Cell::Analyze(QString expression)
{
     // Usuniecie z listy bledow wyswietlanej w oknie glownym, dokowalnym
     MainWin->xtt_errorsList->deleteItem(fParent, fCellID);
     
     // Na poczatku kasujemy komunikat ostatniego bledu, poniewaz nie chcemy aby byl on wyswietlany
     // po analizie ktora moze zakonczyc sie pozytywnie
     fCellError = "";

     // Sprawdzanie czy komorka ma rodzica
     if(TableParent() == NULL)
     {
          setCellError("Cell hasn't got a parent.");
          return false;
     }
     
     // Sprawdzenie czy nie jest to ciag pusty
     if((expression == "") || (expression.length() == 0))
     {
          setCellError("Empty cell.");
          return false;
     }

     // Zdefiniowanie operatorow, uwaga wazna kolejnosc ich podawania!!!
     QStringList opers(smbl_opers);

     // Wstepna obrobka wyrazenia
     // Usuwanie spacji na poczatku i koncu wyrazenia
     for(;(expression.length() > 0) && (expression[0] == ' ');)
          expression = expression.remove(0, 1);

     // Jezeli nie ma juz nic wiecej
     if(expression == "")
     {
          setCellError("Syntax error. No value defined.");
          return false;
     }

     // Usuwanie spacji na poczatku i koncu wyrazenia
     for(;(expression.length() > 0) && (expression[expression.length()-1] == ' ');)
          expression = expression.remove(expression.length()-1, 1);

     // Sprawdzanie wystapienie operatora
     // Type of attribute operator:
     // 0-not defined, 1 - =, 2 - !=, 3 - >=, 4 - >, 5 - <=, 6 - <, 7 - nin, 8 - in.
     unsigned int fOperator = 0;
     for(int i=0;i<opers.size();i++)
          if(expression.indexOf(opers.at(i), 0, Qt::CaseInsensitive) == 0)
          {
               // Usuniecie znakow operatora
               expression = expression.remove(0, opers.at(i).length());

               // Usuniecie spacji po operatorze
               for(;(expression.length() > 0) && (expression[0] == ' ');)
                    expression = expression.remove(0, 1);

               // Zapisanie typu operatora
               fOperator = i+1;

               // Koniec petli
               break;
          }

     // Jezeli nie wykryto operatora zwracamy niepowodzenie
     if(fOperator == 0)
     {
          setCellError("Missing operator.");
          return false;
     }

     // Jezeli nie ma juz nic wiecej
     if(expression == "")
     {
          setCellError("Syntax error. No value defined.");
          return false;
     }

     // Jezeli wykryto to sprawdzamy czy moze nalezec do kontekstu
     // W kontekstach conditional, assert, retract dozwolone sa wszystkie typy
     // operatorow, tylko w kontekscie decision nie moga wystapic takie operatory
     // jak >, >=, <, <=, !=, nin, in natomiast moze wystapic operator =
     if((fOperator != 1) && (fContext == 4))
     {
          setCellError("Bad operator in context.");
          return false;
     }

     // Dlasza analiza

     // Sprawdzanie wsytapienia operatora <any>
     if(expression.contains("op:any", Qt::CaseInsensitive))
     {
          // Operator moze wystapic tylko w kontekscie conditional i retract
          if((fContext != 1) && ((fContext != 3)))
          {
               setCellError("Operator " + XTT_Attribute::_any_operator_ + " is not allowed in this context.");
               return false;
          }
          
          //fContent = opers.at(fOperator-1) + " <any>";
          //fValue = XTT_Attribute::_any_operator_;
          //fValDefType = 1;
          //return true;
     }

     // Wyszukiwanie odpowiedniej funkcji komorki XTTML v2.0 -> HML v1.0
     // 0-not defined, 1 - =, 2 - !=, 3 - >=, 4 - >, 5 - <=, 6 - <, 7 - nin, 8 - in.
     QString internal_function_name = "undefined operator";
     hFunction* cell_function;
     if((fOperator == 1) && (fContext == 2)) // operator retract
          internal_function_name = "void_assert_operator";
     if((fOperator == 1) && (fContext == 3)) // operator retract
          internal_function_name = "void_retract_operator";
     if((fOperator == 1) && (fContext == 4)) // operator assign
          internal_function_name = "void_assign_operator";
          
     if((fOperator == 1) && (fContext == 1)) // operator =
          internal_function_name = "bsequal";
     if((fOperator == 2) && (fContext == 1)) // operator !=
          internal_function_name = "bsnequal";
     if((fOperator == 3) && (fContext == 1)) // operator >=
          internal_function_name = "hbsgreatereq";
     if((fOperator == 4) && (fContext == 1)) // operator >
          internal_function_name = "hbsgreater";
     if((fOperator == 5) && (fContext == 1)) // operator <=
          internal_function_name = "hbslesseq";
     if((fOperator == 6) && (fContext == 1)) // operator <
          internal_function_name = "hbsless";
     if((fOperator == 7) && (fContext == 1)) // operator nin
          internal_function_name = "hbsnin";
     if((fOperator == 8) && (fContext == 1)) // operator in
          internal_function_name = "hbsin";
          
     int f_i = funcList->indexOf(internal_function_name, true);
     if(f_i == -1)
     {
          setCellError("Unable to find cell operator \'" + internal_function_name + "\'.");
          return false;
     }
     hType* reqt = Context()==1?typeList->at(hpTypesInfo.iopti(BOOLEAN_BASED)):typeList->specialType(VOID_BASED);
     cell_function = funcList->at(f_i)->copyTo();
     if(cell_function->setRequiredType(reqt) != FUNCTION_ERROR_SUCCESS)
     {
          setCellError(cell_function->lastError());
          return false;
     }

     // Setting the first argument
     // Creating the new instance of the argument
     int first_argument_type = FUNCTION_TYPE__SINGLE_VALUED;
     //if(AttrType()->multiplicity())
     //     first_argument_type = FUNCTION_TYPE__MULTI_VALUED;
     first_argument_type = first_argument_type | hFunction::typeFunctionRepresentation(AttrType()->Type());
     hMultipleValue* first_argument_value = new hMultipleValue(AttrType());
     hMultipleValue* second_argument_value = new hMultipleValue(VALUE, AttrType()->Type());
     second_argument_value->valueField()->clear();
     hFunctionArgument* first_argument = new hFunctionArgument, *second_argument = new hFunctionArgument;
     first_argument->setEditable(false);
     first_argument->setVisible(false);
     first_argument->setValue(first_argument_value, false);
     first_argument->setType(first_argument_type);
     first_argument->findCompatibleTypes(typeList);
     //second_argument->setType(first_argument_type);
     if(cell_function->setArgument(0, first_argument) != FUNCTION_ERROR_SUCCESS)
     {
          setCellError("Unable to create first function argument.\nError message: " + cell_function->lastError() + ".");
          return false;
     }
     second_argument->setValue(second_argument_value, false);

     // Sprawdzanie typu podanej wartosci, czy to jest atomic, set, range
     // Jezeli to jest set, lub range to teraz na pierwszej pozycji powinien
     // wsytapic nawias otwierajacy inabczej jest jest to typ atomic, jezeli jako
     // pierwszy znak mamy nawias ( lub [, to nalezy sprawdzic czy nie wysepuje
     // jeszcze srednik i taki sam nawias zamykajacy na koncu, wtedy jest to
     // przedzial range
     
     // 0-not defined, 1-atomic, 2-set, 3-interval.
     int fValDefType = 0;

     if(((expression[0] == '(') || (expression[0] == '[')) &&
     (expression.count(";") == 1) &&
     ((expression[expression.length()-1] == ')') || (expression[expression.length()-1] == ']')))
          fValDefType = 3;

     if(fValDefType == 0)
     {
          if(expression[0] == '{')
               fValDefType = 2;
          else
               fValDefType = 1;
     }
     
     // Jezeli wysteouje wiecej niz jeden srednik to jest to napewno set
     if(((expression.count(";") > 1) && (fValDefType != 2)) || 
        ((expression.count(";") == 1) && ((fValDefType == 0) || (fValDefType == 1))))
     {     
          setCellError("Value type seems to be \"set\". Maybe you forget the bracket \"{\"\nIf you want to define set you should use the following syntax:\n{expr1; expr2; ... ; exprn}");
          return false;
     }

     // Teraz sprawdzamy czy dla podanego typu wartosci moze istniec podany operator
     // w danym kontekscie. Rozpatrzymy tylko przypadki bledne aby zasygnalizowac
     // powstaly blad

     // Kontkest decyzyjny
     if(fContext == 1)
     {
          // Gdy to jest jeden z operatorow >=, >, <, <=, =, != i nie jest to
          // tpy atomic to blad
          if((fOperator >= 1) && (fOperator <= 6) && (fValDefType != 1))
          {
               setCellError("Bad operator. Allowed operators are only in, nin.");
               return false;
          }
     }

     // Kontkest assert i retract
     if((fContext == 2) || (fContext == 3))
     {
          if((fOperator >= 2) && (fOperator <= 6) && (fValDefType != 1))
          {
               setCellError("Bad operator. Allowed operators are only in, nin.");
               return false;
          }

          if((fOperator == 1) && (fValDefType != 1) && (fValDefType != 2))
          {
               setCellError("Bad operator. Allowed operators are only in, nin.");
               return false;
          }
     }

     // Kontkest action, tutaj sa dozwolone tylko set i atomic oraz operator =
     if(fContext == 4)
     {
          if((fOperator != 1) || (fValDefType == 3))
          {
               setCellError("Range is not allowed in context Action.");
               return false;
          }
     }
     

     // Wczytywanie argumentow, kazdy moze byc wyrazeniem arytmetycznym
     QStringList args;
     args.clear();

     // Gdy atomic, to cale wyrazenie
     if(fValDefType == 1)
          args << expression;

     // Gdy zbior
     if(fValDefType == 2)
     {
          if(expression[0] == '{')
               expression = expression.remove(0, 1);
          if(expression[expression.length()-1] == '}')
               expression = expression.remove(expression.length()-1, 1);

          // Wczytywanie argumentow, kolejnych elementow zbioru
          args = expression.split(";", QString::SkipEmptyParts, Qt::CaseSensitive);
     }

     // Gdy jest to przedzial
     if(fValDefType == 3)
     {
          expression = expression.remove(0, 1);
          expression = expression.remove(expression.length()-1, 1);
          args = expression.split(";", QString::SkipEmptyParts, Qt::CaseSensitive);
     }

     // Kiedy nie podamy argumentow
     if(!args.size())
     {
          setCellError("Expression syntax error: no arrguments passed.");
          return false;
     }

     // Usuwanie spacji na koncach kazdego wyrazenia
     for(int i=0;i<args.size();i++)
     {
          for(;args.at(i)[0] == ' ';)
          {
               QString val = args.at(i);
               args.replace(i, val.remove(0, 1));
          }
          for(;args.at(i)[args.at(i).length()-1] == ' ';)
          {
               QString val = args.at(i);
               args.replace(i, val.remove(val.length()-1, 1));
          }
     }
     
     // Obliczanie wartosci wyrazen
     QList<QMap<QString, hExpression*>*> expr_map_list;
     QList<hValue*> val_list;
     for(int i=0;i<args.size();i++)
     {
          hValue* new_pointer;
          expr_map_list.append(new QMap<QString, hExpression*>);
          QString expr = args.at(i);
          if(Calculate(expr, 0, new_pointer, expr_map_list.last()) == "#NAN")
          {
               AttributeGroups->_IndexOfName("tmp")->Flush();
               return false;
          }
          args.replace(i, expr);
          val_list.append(new_pointer);
     }
     
     // ------ Weryfikacja typu atrybutu i jego dziedziny ------
     if(Settings->xttCheckAtomicValues())
     {
          bool ver_stat = true;
          QString error_messages = "Cell values verification messages:";
          for(int i=0;i<val_list.size();i++)
               ver_stat = ver_stat && val_list.at(i)->valueVerification(error_messages);
          if(!ver_stat)
          {
               setCellError(error_messages);
               return false;
          }
     }
     

     // Jezeli wszystko ok to zapisujemy wyliczone wartosci

     // Jezeli atomic, to przepisujemy wartosc do pola fValue
     if(fValDefType == 1)
     {
          second_argument->setType(hFunction::typeFunctionRepresentation(AttrType()->Type()) | FUNCTION_TYPE__SINGLE_VALUED);
          second_argument_value->valueField()->add(val_list.at(0));
     }

     // Gdy typ wyliczeniowy
     if(fValDefType == 2)
     {
          second_argument->setType(hFunction::typeFunctionRepresentation(AttrType()->Type()) | FUNCTION_TYPE__MULTI_VALUED);
          for(int i=0;i<val_list.size();++i)
               second_argument_value->valueField()->add(val_list.at(i));
     }

     // Gdy przedzial
     if(fValDefType == 3)
     {
          second_argument->setType(hFunction::typeFunctionRepresentation(AttrType()->Type()) | FUNCTION_TYPE__MULTI_VALUED);
          second_argument_value->valueField()->add(new hSetItem(second_argument_value->valueField(), SET_ITEM_TYPE_RANGE, val_list.at(0), val_list.at(1)));
     }

     if((cell_function->argCount() >= 2) && (cell_function->setArgument(1, second_argument) != FUNCTION_ERROR_SUCCESS))
     {
          setCellError("Unable to create second function argument.\nError message: " + cell_function->lastError() + ".");
          return false;
     }

     // Setting new expression
     QString ver_msgs = "";
     if(!fContent->expressionField()->setRequiredType(fAttrType->Type()))
     {
          setCellError(cell_function->lastError());
          return false;
     }
     fContent->expressionField()->setFunction(cell_function);
     if(!fContent->multipleValueVerification(ver_msgs))
     {
          setCellError(ver_msgs);
          return false;
     }
     
     // Jezeli wylaczona jest porownywanie pomiedzy atrybutami to mozemy na probe odpalic komorke
     bool avr = second_argument_value->isAVRused();
     if(((!avr) && (Settings->xttExecuteCellWhenOff())) || ((avr) && (Settings->xttExecuteCellWhenOn())))
     {
          // Odpalenie komorki
          int execRes = Execute(false);
          
          // Jezeli odpalenie komorki sie nie udalo
          if(execRes == -1)
               return false;
     }

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function calculates operations that are included in the cell.
//
// #param cellAnalyze Determines whether the cell should be analyzed before.
// #return Result of executing.
// #li -1 - error.
// #li 0 - false.
// #li 1 - true.
int XTT_Cell::Execute(bool)
{/*
     // Sprawdzenie wyrazenia pod kontem poprawnosci
     if(cellAnalyze)
     {
          bool res = Analyze(expr);
          if(!res)
               return -1;
     }*/
     
     fCellError = "";

     // Executing the operation
     if(fContent == NULL)
     {
          setCellError("Undefined context identifier.");
          return -1;
     }
     
     if(fAttrType == NULL)
     {
          setCellError("The cell has not defined attribute.");
          return -1;
     }

     // Calculating the expression
     hMultipleValueResult mvr = fContent->value();

     // When error occurs
     if(!(bool)mvr)
     {
          setCellError((QString)mvr);
          return -1;
     }

     // If no error in conditional context
     if(fContext == 1)
     {
          if(mvr.toSet(false)->toValue() == QString::number(BOOLEAN_FALSE))
               return 0;
          if(mvr.toSet(false)->toValue() == QString::number(BOOLEAN_TRUE))
               return 1;
          return -1;
     }

     // If no error in decision context
     if((fContext == 4) || (fContext == 5))
          return 1;
          
     return 0;
}
// -----------------------------------------------------------------------------

// #brief Return the string representation of the cell expression
//
// #return String representation of the cell expression
QString XTT_Cell::ValueRepresentation(void)
{
     return fContent->toString();
}
// -----------------------------------------------------------------------------

// #brief Function that maps the cell to the prolog code.
//
// #param __isAction - indicates if the cell expression is the "some_registered_action" that is not supported by hqed but is declared in PROLOG
// #return The prolog code that represents the cell as string.
QString XTT_Cell::toProlog(bool* __isAction)
{
     bool isAction = (Context() == 5);
     
     /*if((fContent->type() == MULTIPLE_VALUE_TYPE_EXPRESSION) &&
        (fContent->expressionField()->function() != NULL) &&
        (fContent->expressionField()->function()->internalRepresentation() == "void_action_operator"))
     {
          isAction = true;
     }
     // or - i would prefer to not merge this two conditions together
     if((fContent->type() == MULTIPLE_VALUE_TYPE_VALUE) &&
        (!fContent->valueField()->isEmpty()) && 
        (fContent->valueField()->at(0)->type() == SET_ITEM_TYPE_SINGLE_VALUE) && 
        (fContent->valueField()->at(0)->fromPtr()->type() == VALUE_TYPE_EXPRESSION) &&
        (fContent->valueField()->at(0)->fromPtr()->expressionField()->function() != NULL) &&
        (fContent->valueField()->at(0)->fromPtr()->expressionField()->function()->internalRepresentation() == "void_action_operator"))
     {
          isAction = true;
     }*/
     
     if(__isAction != NULL)
          *__isAction = isAction;
     
     return fContent->toProlog();
}
// -----------------------------------------------------------------------------

// #brief Function calculates the expression.
//
// #param expression Reference to the expression.
// #param start_index Index of the begining of operation.
// #param __cell_value - contains the parsed expresion
// #param __expr_map - pointer to the map coantiner that contains the subexpreesions of the cell content
// #return Result of the operation.
QString XTT_Cell::Calculate(QString& expression, int start_index, hValue*& __value, QMap<QString, hExpression*>*& __expr_map)
{
     QString res = "#NAN";

     // Jezeli ciag zawiera <any> to zwracamy <any>
     if(expression.contains(XTT_Attribute::_any_operator_, Qt::CaseInsensitive))
     {
          __value = new hValue(ANY_VALUE);
          expression =  XTT_Attribute::_any_operator_;
          return expression;
     }

     // Jezeli ciag zawiera <any> to zwracamy <any>
     if(expression.contains(XTT_Attribute::_not_definded_operator_, Qt::CaseInsensitive))
     {
          __value = new hValue(NOT_DEFINED);
          expression =  XTT_Attribute::_not_definded_operator_;
          return expression;
     }

     // Usuwanie spacji z wyrazenia
     if((start_index == 0) && (expression.contains(" ")))
          expression = expression.remove(expression.indexOf(" "), 1);

     // Wyszukiwanie nawiasow, jezeli znajdziemy nawias to uruchamiamy jeszcze
     // raz rekurencyjnie ta funckje
     for(int i=start_index;i<expression.length();i++)
     {
          // Kiedy na koncu stringu jest otwieranie nawiasu
          if((expression[i] == '(') && (i >= (expression.length()-1)))
               return res;
               
          if((expression[i] == '(') && (i < (expression.length()-1)))
          {
               if(Calculate(expression, i+1, __value, __expr_map) == "#NAN")
                    return res;
          }
     }

     // Wyszukiwanie konca obliczen
     int end_index = -1;
     for(int i=start_index;i<expression.length();i++)
     {
          if(expression[i] == ')')
          {
               end_index = i;
               break;
          }
          if((i == (expression.length()-1)) && (start_index == 0))
          {
               end_index = i+1;
               break;
          }
     }

     // Jezeli nie znajdziemy nawiasu zamykajacego ale w nie pustym stringu, bo
     // pusty string tez moze byc rezultatem
     if((end_index == -1) && (expression != ""))
     {
          setCellError("There is a lack of right brace.");
          return "#NAN";
     }

     // Jezeli znajdzeimy to wyodrebniamy sobie substring dla latwiejsze analizy
     // Expr nie zawiera juz ani jednego nawiasu zamykajacego i otwierajacego
     QString expr = expression.mid(start_index, end_index-start_index);

     // Definicja nazw funckji matematycznych jakie obsluguje program (PRIORITY HIGH)
     QStringList func_H;
     func_H << "sin" << "cos" << "tan" << "abs" << "root" << "^" << "!" << "log" << "mod";

     // Definicja nazw funckji matematycznych jakie obsluguje program (PRIORITY NORMAL)
     QStringList func_N;
     func_N << "*" << "/";

     // Definicja nazw funckji matematycznych jakie obsluguje program (PRIORITY LOW)
     QStringList func_L;
     func_L << "+" << "-";

     // *************** WYSZUKIWANIE FUNKCJI O NAJWYZSZYM POZIOMIE *************
     if(sin_func(func_H.at(0), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(cos_func(func_H.at(1), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(tan_func(func_H.at(2), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(abs_func(func_H.at(3), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(root_func(func_H.at(4), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(pow_func(func_H.at(5), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(fac_func(func_H.at(6), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(log_func(func_H.at(7), expr, __expr_map) == "#NAN")
          return "#NAN";
     if(mod_func(func_H.at(8), expr, __expr_map) == "#NAN")
          return "#NAN";
     // ************************************************************************
     

     // ***************** WYSZUKIWANIE FUNKCJI O SREDNIM POZIOMIE **************
     int i1 = expr.indexOf(func_N.at(0));
     int i2 = expr.indexOf(func_N.at(1));
     while((i1 > -1) || (i2 > -1))
     {
          if(((i1 < i2) && (i1 != -1)) || ((i1 > -1) && (i2 == -1)))
               if(mul_func(func_N.at(0), expr, __expr_map) == "#NAN")
                    return "#NAN";
          if(((i2 < i1) && (i2 != -1)) || ((i2 > -1) && (i1 == -1)))
               if(div_func(func_N.at(1), expr, __expr_map) == "#NAN")
                    return "#NAN";

          // Ponowne wyszukanie dzialan
          i1 = expr.indexOf(func_N.at(0));
          i2 = expr.indexOf(func_N.at(1));
     }
     // ************************************************************************

     // ****************** WYSZUKIWANIE FUNKCJI O NISKIM POZIOMIE **************
     // Aby wykonywanie tych dzialan bylo od lewej do prawej nalezy sprawdzac
     // ktore wczesniej wystepuje:
     i1 = expr.indexOf(func_L.at(0));
     i2 = expr.indexOf(func_L.at(1), 1);
     while((i1 > -1) || (i2 > -1))
     {
          if(((i1 < i2) && (i1 != -1)) || ((i1 > -1) && (i2 == -1)))
               if(add_func(func_L.at(0), expr, __expr_map) == "#NAN")
                    return "#NAN";
          if(((i2 < i1) && (i2 != -1)) || ((i2 > -1) && (i1 == -1)))
               if(sub_func(func_L.at(1), expr, __expr_map) == "#NAN")
                    return "#NAN";

          // Ponowne wyszukanie dzialan
          i1 = expr.indexOf(func_L.at(0));
          i2 = expr.indexOf(func_L.at(1), 1);
     }
     // ************************************************************************

     // Kiedy sprawdzimy wszystkie funckje to pozniej jeszcze trzeba sprawdzic
     // czy nie jest to przypadkiem wyraz wolny czyli jest typu naszego
     // argumentu
     res = expr;
     if(fAttrType->Type()->baseType() == BOOLEAN_BASED)
     {
          if(expr.toLower() == "false")
               res = QString::number(BOOLEAN_FALSE);
          if(expr.toLower() == "true")
               res = QString::number(BOOLEAN_TRUE);
     }

     // Jezeli jako wyrazenie zostala podana sama nazwa atrybutu, to funckje
     // nie zamienia jej na konkretna wartosc, dlatego trezba to zrobic to
     // tutaj, i tylko w momencie kiedy jestesmy w pierwszym wywolaniu czyli teraz
     // bedziemy konczyc obliczenia. W bardziej zagniezdzonych moga wystapic
     // nazwy zmiennych tymczasowych wiec zamiana na wartosc jest nie wskazana
     // bo zostanie wyswietlony komunikat o bledzie typu atrybutow
     XTT_Attribute* attr = AttributeGroups->Attribute(res);
     
     if((!favr) && (attr != NULL))
     {     
          if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
          {
               setCellError("Attribute value reference options has been turned off.");
               return "#NAN";
          }
     }

     // Jezeli taki atrybut istnieje to:
     if((start_index == 0) && (attr != NULL))
     {
          // Sprawdzamy typ atrbutu czy jest taki sam jak naszego zrdlowego
          if((hFunction::typeFunctionRepresentation(attr->Type()) & hFunction::typeFunctionRepresentation(fAttrType->Type())) == 0)
          {
               setCellError("Inconsistent argument type \"" + res + "\".");
               return "#NAN";
          }

          if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
          {
               setCellError("Undefined argument \"" + res + "\" occurence.");
               return "#NAN";
          }

          // Jezeli poprawny to zapisujemy wartosc
          __value = new hValue(attr);

          // Oprozniamy grupe atrybutow tymczasowych
          AttributeGroups->_IndexOfName("tmp")->Flush();
     }
     
     // Jezeli tutaj jestesmy to znaczy ze dzialanie sie udalo i teraz
     // trzeba zastapi analizowany pod string wynikiem
     if(start_index > 0)
          expression = expression.replace(start_index-1, end_index-start_index+2, res);
     if((start_index == 0) && (attr == NULL))
     {
          if(__expr_map->keys().size() > 0)       // There is expression
               __value = new hValue((*__expr_map)[__expr_map->keys().last()]);
          if(__expr_map->keys().size() == 0)      // There is no expression, so the value is given direct
               __value = new hValue(res);
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all sine functions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::sin_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "sin";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all cosine functions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::cos_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "cos";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all tangent functions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::tan_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "tan";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::sin_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all absolute values that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::abs_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "abs";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::abs_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all root values that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::root_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";
     
     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Wartosci argumentow jako stringi
          QString arg1val = "";
          QString arg2val = "";

          // Dlugosci wczytanych argumentow
          int a1len;
          int a2len;
          
          // Pointers to the functions objects
          hFunctionArgument* fa1 = NULL;
          hFunctionArgument* fa2 = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&(((i==aindex)||(expr[i-1]==' '))||((i>0)&&((expr[i-1]==',')||(expr[i-1]==' '))))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Poniewaz ta funckja przyjmuje dwa argumenty oddzielone przecinkiem
          // to najpierw nalezy sprawdzic czy sa one podane i sa ok
          QStringList arglist = arg.split(",", QString::SkipEmptyParts);
          if(arglist.size() != 2)
          {
               setCellError("Incorect argument count in function root/2.");
               return "#NAN";
          }
          
          QString arg1 = arglist.at(0);
          QString arg2 = arglist.at(1);

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }

          // Checking if the function exists
          QString func_internal_rep = "root";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type
          
          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::root_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index, fname.length()+arg.length(), newKey);
     }

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all values to the power that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::pow_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Wartosci argumentow jako stringi
          QString arg1val = "";
          QString arg2val = "";

          // Dlugosci wczytanych argumentow
          int a1len;
          int a2len;
          
          // Pointers to the functions objects
          hFunctionArgument* fa1 = NULL;
          hFunctionArgument* fa2 = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg1 = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg1 = expr[i] + arg1;

          // Wczytywanie prawego argumentu
          QString arg2 = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg2 = arg2 + expr[i];

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }

          // Checking if the function exists
          QString func_internal_rep = "pow";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type
          
          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::pow_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index-a1len, fname.length()+a1len+a2len, newKey);
     }

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all modulo divisions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::mod_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";
     
     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Wartosci argumentow jako stringi
          QString arg1val = "";
          QString arg2val = "";

          // Dlugosci wczytanych argumentow
          int a1len;
          int a2len;
          
          // Pointers to the functions objects
          hFunctionArgument* fa1 = NULL;
          hFunctionArgument* fa2 = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&(((i==aindex)||(expr[i-1]==' '))||((i>0)&&((expr[i-1]==',')||(expr[i-1]==' '))))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Poniewaz ta funckja przyjmuje dwa argumenty oddzielone przecinkiem
          // to najpierw nalezy sprawdzic czy sa one podane i sa ok
          QStringList arglist = arg.split(",", QString::SkipEmptyParts);
          if(arglist.size() != 2)
          {
               setCellError("Incorect argument count in function mod/2.");
               return "#NAN";
          }
          
          QString arg1 = arglist.at(0);
          QString arg2 = arglist.at(1);

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }

          // Checking if the function exists
          QString func_internal_rep = "mod";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type
          
          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::mod_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index, fname.length()+arg.length(), newKey);
     }

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all factories that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::fac_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{    
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg = expr[i] + arg;
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "fac";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::fac_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all logarithmic functions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::log_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     while(expr.contains(fname, Qt::CaseInsensitive))
     {
          // Pointers to the functions objects
          hFunctionArgument* fa = NULL;
     
          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie argumentu
          QString arg = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg = arg + expr[i];
               
          // Dlugosci wczytanych argumentow
          int alen = arg.length();
               
          // Usuwanie spacji w wartosciach
          for(;arg.contains(" ");) arg = arg.remove(arg.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr = AttributeGroups->Attribute(arg);
          
          // Pointers to the expressions
          hExpression* hexpr = NULL;
          if(__expr_map->contains(arg))
               hexpr = (*__expr_map)[arg];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if((!favr) && (attr != NULL))
          {     
               if(AttributeGroups->AttributeGroup(attr)->Name() != "tmp")
                    attr = NULL;
          }
          
          // Checking if the function exists
          QString func_internal_rep = "log";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__OSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg + ".\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr == NULL) && (hexpr == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg == "op:nd") 
                    arg = XTT_Attribute::_not_definded_operator_;
               if(arg == "op:any") 
                    arg = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr->Id()) == -1) && (AttributeGroups->CreatePath(attr) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg + "\" occurence.");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg + " type.\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(hexpr != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!hexpr->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(hexpr->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + hexpr->requiredType()->name() + ".\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(hexpr));
               fa = new hFunctionArgument;
               fa->setValue(mv);
          }
          
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::log_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* h_new_expr = new hExpression(fAttrType->Type());
          h_new_expr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = h_new_expr;

          expr = expr.replace(index, fname.length()+alen, newKey);
     }
     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all multiplications that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::mul_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     // Wartosci argumentow jako stringi
     QString arg1val = "";
     QString arg2val = "";

     // Dlugosci wczytanych argumentow
     int a1len;
     int a2len;
     
     // Pointers to the functions objects
     hFunctionArgument* fa1 = NULL;
     hFunctionArgument* fa2 = NULL;

          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg1 = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg1 = expr[i] + arg1;

          // Wczytywanie prawego argumentu
          QString arg2 = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg2 = arg2 + expr[i];

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }
          
          // Checking if the function exists
          QString func_internal_rep = "mul";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
               
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::mul_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index-a1len, fname.length()+a1len+a2len, newKey);

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all divisions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::div_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     // Wartosci argumentow jako stringi
     QString arg1val = "";
     QString arg2val = "";

     // Dlugosci wczytanych argumentow
     int a1len;
     int a2len;
     
     // Pointers to the functions objects
     hFunctionArgument* fa1 = NULL;
     hFunctionArgument* fa2 = NULL;

          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg1 = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg1 = expr[i] + arg1;

          // Wczytywanie prawego argumentu
          QString arg2 = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg2 = arg2 + expr[i];

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }
          
          // Checking if the function exists
          QString func_internal_rep = "div";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
               
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::div_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index-a1len, fname.length()+a1len+a2len, newKey);

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all additions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::add_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     // Wartosci argumentow jako stringi
     QString arg1val = "";
     QString arg2val = "";

     // Dlugosci wczytanych argumentow
     int a1len;
     int a2len;
     
     // Pointers to the functions objects
     hFunctionArgument* fa1 = NULL;
     hFunctionArgument* fa2 = NULL;

          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg1 = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg1 = expr[i] + arg1;

          // Wczytywanie prawego argumentu
          QString arg2 = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg2 = arg2 + expr[i];

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }

          // Checking if the function exists
          QString func_internal_rep = "add";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type
          
          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::add_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index-a1len, fname.length()+a1len+a2len, newKey);

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Function calculates all substractions that are included in expression.
//
// #param fname Function name.
// #param expr Reference to expression.
// #param __expr_map pointer to the map of the expressions objects
// #return Modyfied expression.
QString XTT_Cell::sub_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map)
{
     // Zmienna przechowujaca tymczasowy wynik dzialania funckji
     QString tmp_res = "";

     // Wartosci argumentow jako stringi
     QString arg1val = "";
     QString arg2val = "";

     // Dlugosci wczytanych argumentow
     int a1len;
     int a2len;
     
     // Pointers to the functions objects
     hFunctionArgument* fa1 = NULL;
     hFunctionArgument* fa2 = NULL;

          // Index pierwszego znaku nazwy funckji
          int index = expr.indexOf(fname, 0, Qt::CaseInsensitive);
          
          // Przesuniecie na pierwszy znak argumentu
          int aindex = index + fname.length();

          // Wczytywanie lewego argumentu
          QString stop_char = "+-/*";
          QString arg1 = "";
          if(index > 0)
               for(int i=index-1;((i>=0)&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==0)||((i>0)&&(stop_char.contains(expr[i-1]))))))&&(expr[i]!='*')&&(expr[i]!='/'));i--)
                    arg1 = expr[i] + arg1;

          // Wczytywanie prawego argumentu
          QString arg2 = "";
          for(int i=aindex;((i<expr.length())&&(expr[i]!='+')&&((expr[i]!='-')||((expr[i]=='-')&&((i==aindex)||(expr[i-1]==' '))))&&(expr[i]!='*')&&(expr[i]!='/'));i++)
               arg2 = arg2 + expr[i];

          // Zapisujemy dlugosci wczytanych atrbutow
          a1len = arg1.length();
          a2len = arg2.length();

          // Usuwanie spacji w wartosciach
          for(;arg1.contains(" ");) arg1 = arg1.remove(arg1.indexOf(" "), 1);
          for(;arg2.contains(" ");) arg2 = arg2.remove(arg2.indexOf(" "), 1);

          // Sprawdanie czy istnieje podany argumnet
          XTT_Attribute* attr1 = AttributeGroups->Attribute(arg1);
          XTT_Attribute* attr2 = AttributeGroups->Attribute(arg2);


          // Pointers to the expressions
          hExpression* expr1 = NULL;
          hExpression* expr2 = NULL;
          if(__expr_map->contains(arg1))
               expr1 = (*__expr_map)[arg1];
          if(__expr_map->contains(arg2))
               expr2 = (*__expr_map)[arg2];
          
          // Jezeli nie jest wlaczony tryb porownywania atrybutow to ustawiamy atrybut na NULL
          if(!favr)
          {
               if(attr1 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr1)->Name() != "tmp")
                    {
                         attr1 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
               if(attr2 != NULL)
               {     
                    if(AttributeGroups->AttributeGroup(attr2)->Name() != "tmp")
                    {
                         attr2 = NULL;
                         setCellError("Attribute value reference options has been turned off.");
                         return "#NAN";
                    }
               }
          }
          
          // Checking if the function exists
          QString func_internal_rep = "sub";
          int f_index = funcList->indexOf(func_internal_rep, true);
          if(f_index == -1)
          {
               setCellError("Cannot find \'" + func_internal_rep + "\' function in function list.\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
               
          // When unsupported cell argument type for add function
          if(((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__BOOLEAN) > 0)  || 
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__UOSYMBL) > 0) ||
              ((hFunction::typeFunctionRepresentation(fAttrType->Type()) & FUNCTION_TYPE__VOID) > 0))
          {
               setCellError("Unsupported first argument type \'" + fAttrType->Type()->name() + "\' as an argument of " + funcList->at(f_index)->userRepresentation() + "\' function.\nParsed argument: " + arg1 + ".\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }

          // When correct type

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr1 == NULL) && (expr1 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg1, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }
               
               // The compatybility with the XTT 2.0
               if(arg1 == "op:nd") 
                    arg1 = XTT_Attribute::_not_definded_operator_;
               if(arg1 == "op:any") 
                    arg1 = XTT_Attribute::_any_operator_;

               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
               
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr1 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr1->Id()) == -1) && (AttributeGroups->CreatePath(attr1) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg1 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr1->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 1 \"" + arg1 + "\".");
                    return "#NAN";
               }*/
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr1 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr1->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr1->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr1->requiredType()->name() + ".\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr1));
               fa1 = new hFunctionArgument;
               fa1->setValue(mv);
          }

          // Kiedy nie jest to atrybut zdefiniowany w programie to probujemy go
          // konwertowac na liczbe typu float
          if((attr2 == NULL) && (expr2 == NULL))
          {
               // Checking the correctness of the value
               double __numValue;
               if(fAttrType->Type()->mapToNumeric(arg2, __numValue, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    setCellError(fAttrType->Type()->lastError());
                    return "#NAN";
               }

               // The compatybility with the XTT 2.0
               if(arg2 == "op:nd") 
                    arg2 = XTT_Attribute::_not_definded_operator_;
               if(arg2 == "op:any") 
                    arg2 = XTT_Attribute::_any_operator_;
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(arg2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }

          // Jezeli jest to konkretny argument to sprawdzamy czy jest typu liczbowego
          if(attr2 != NULL)
          {
               // Sprawdzanie czy atrybut istneieje w danej tabeli
               if((TableParent()->IndexOfAttributeID(attr2->Id()) == -1) && (AttributeGroups->CreatePath(attr2) != "tmp."))
               {
                    setCellError("Undefined argument \"" + arg2 + "\" occurence.");
                    return "#NAN";
               }

               /*if(attr2->Value().toLower() == XTT_Attribute::_not_definded_operator_)
               {
                    setCellError("Not defined value in argument 2 \"" + arg2 + "\".");
                    return "#NAN";
               }*/

               // Checking the type compatibility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(attr1->Type())) == 0)
               {
                    setCellError("Incompatible argument " + arg1 + " type.\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(attr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          
          // Jezeli jest to jakies wyrazenie znlezione wczesniej - czyli jestesmy na wyzszym poziomie zagniezdzenia
          if(expr2 != NULL)
          {
               // Expression verification
               QString msgs = "";
               if(!expr2->expressionVerification(msgs))
               {
                    setCellError("Cell error:\n" + msgs + "\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // Checking the type compatybility
               if((hFunction::typeFunctionRepresentation(fAttrType->Type()) & hFunction::typeFunctionRepresentation(expr2->requiredType())) == 0)
               {
                    setCellError("Incompatible expression required type: " + expr2->requiredType()->name() + ".\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
                    return "#NAN";
               }
               
               // If value is ok, the we create the first function argument
               hMultipleValue* mv = new hMultipleValue(fAttrType->Type(), new hValue(expr2));
               fa2 = new hFunctionArgument;
               fa2->setValue(mv);
          }
          //Tu skonczone - oba argumenty powinnym miec dobra wartosc i teraz trzeba utworzyc wyrazenie - najpierw hfunc a ponziej hexpr
          
          // Creating hFunction object
          hFunction* func = funcList->at(f_index)->copyTo();
          
          // Setting arguments
          if(func->setArgument(0, fa1) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set first argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          if(func->setArgument(1, fa2) != FUNCTION_ERROR_SUCCESS)
          {
               setCellError("Unable to set second argument for function \'" + func_internal_rep + "\'.\nError message: " + func->lastError() + "\nError from: XTT_Cell::sub_func(" + QString::number(__LINE__) + ")");
               return "#NAN";
          }
          
          // Creating hExpression object
          hExpression* hexpr = new hExpression(fAttrType->Type());
          hexpr->setFunction(func);

          QString newKey = hqed::findNextKey(__expr_map);
          (*__expr_map)[newKey] = hexpr;

          expr = expr.replace(index-a1len, fname.length()+a1len+a2len, newKey);

     return expr;
}
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_Cell::idPrefix(void)
{
     return "cll_";
}
// -----------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 2.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returnes false.
bool XTT_Cell::FromXTTML_2_0(QDomElement& root)
{
     if(root.tagName().toLower() != "cell")
          return false;

     QString opr = root.attribute("operator", "0");
     QString cnt = root.attribute("content", "#NAN#");
     QString a2a = root.attribute("attr2attr", "#NAN#");
     
     if(opr == "0")
     {
          QMessageBox::critical(NULL, "HQEd", "Missing cell parameter \"operator\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     if(cnt == "#NAN#")
     {
          QMessageBox::critical(NULL, "HQEd", "Missing cell parameter \"content\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     
     // Usuwanie spacji z tresci
     for(;opr.indexOf(" ", 0) == 0;)
          opr = opr.remove(opr.indexOf(" "), 1);
     for(;cnt.indexOf(" ", 0) == 0;)
          cnt = cnt.remove(cnt.indexOf(" "), 1);
     for(;a2a.contains(" ");)
          a2a = a2a.remove(a2a.indexOf(" "), 1);
     
     // Zamiana operatora tekstowego na symboliczny
     for(int i=0;i<text_opers.size();i++)
          if(opr.indexOf(text_opers.at(i), 0, Qt::CaseInsensitive) == 0)
               opr = smbl_opers.at(i);
     
     fStrContent = opr + " " + cnt;
          
     if((a2a.toLower() != "true") && (a2a.toLower() != "false") && (a2a != "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect cell parameter \"attr2attr\" at line " + QString::number(root.lineNumber()) + ".\nAllowed values are only \"true\", \"false\"", QMessageBox::Ok);
          return false;
     }
     favr = (a2a.toLower() == "true");

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 format.
//
// #param _csvdoc - writing device
// #return List of all data in drools5 format.
void XTT_Cell::out_Drools_5_0_DecTab(QTextStream* _csvdoc)
{
     *_csvdoc << "\"";
     *_csvdoc << fContent->toDrools5();
     *_csvdoc << "\",";
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Cell::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(fContent->type() != EXPRESSION)
          return res;
     if(fContent->expressionField()->function() == NULL)
          return res;
     
     if(__mode != 4)
     {
          QString elementName = "relation";
          if(Context() == 4)
               elementName = "trans";
          if(Context() == 5)
               elementName = "action";
               
          QString func_name = "";
          if(Context() != 4)
          {
               QString fName = fContent->expressionField()->function()->xmlRepresentation();
               func_name = " name=\"" + fName + "\"";
          }

          QString init_line = "<" + elementName;
          init_line += func_name;
          init_line += ">";
          *res << init_line;
          
          QStringList* tmp = fContent->expressionField()->function()->out_HML_2_0();
          for(int i=0;i<tmp->size();++i)
               *res << "\t" + tmp->at(i);
          delete tmp;
          
          *res << "</" + elementName + ">";
     }
     
     if(__mode == 4)
     {
          QStringList* tmp = fContent->expressionField()->function()->out_HML_2_0();
          for(int i=0;i<tmp->size();++i)
               *res << "\t" + tmp->at(i);
          delete tmp;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_Cell::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(fContent->type() != EXPRESSION)
          return;
     if(fContent->expressionField()->function() == NULL)
          return;
 
     if(__mode != 4)
     {
          QString elementName = "relation";
          if(Context() == 4)
               elementName = "trans";
          if(Context() == 5)
               elementName = "action";

          QString func_name = fContent->expressionField()->function()->xmlRepresentation();
               
          _xmldoc->writeStartElement(elementName);
          if(Context() != 4)
               _xmldoc->writeAttribute("name", func_name);
          fContent->expressionField()->function()->out_HML_2_0(_xmldoc, __mode);
          _xmldoc->writeEndElement();
     }
     
     if(__mode == 4)
     {
          fContent->expressionField()->function()->out_HML_2_0(_xmldoc, __mode);
     }
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_Cell::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     
     QStringList possibleTags = QStringList() << "relation" << "trans" << "action";
     int tagIndex = possibleTags.indexOf(__root.tagName().toLower());
     if(tagIndex == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined or misplaced tag \'" + __root.tagName() + "\'.\nAllowed elements are: " + possibleTags.join(", ") + "\nThe object will not be read." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
          return result;
     }
     fContent->setType(EXPRESSION);

     // The types of the function that is direct in cell. The order must be compatible wits order of elements in possibleTags variable!!!
     QList<hType*> cellTypes;
     cellTypes.append(typeList->at(hpTypesInfo.iopti(BOOLEAN_BASED)));
     cellTypes.append(typeList->specialType(VOID_BASED));
     cellTypes.append(typeList->specialType(ACTION_BASED));

     fContent->expressionField()->setRequiredType(cellTypes.at(tagIndex));
     int exprRes = fContent->expressionField()->in_HML_2_0(__root, __msg, fAttrType, true);
     if(exprRes == 0)    // When no errors
     {
          fContent->expressionField()->function()->argument(0)->setEditable(false);
          fContent->expressionField()->function()->argument(0)->setVisible(false);
     }
     result += exprRes;

     return result;
}
// -----------------------------------------------------------------------------

