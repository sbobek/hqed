/**
 * \file     XTT_State.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      16.11.2009
 * \version     1.0
 * \brief     This file contains class definition that represents the XTT_State.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef XTTSTATEH
#define XTTSTATEH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QHash>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

class hSet;
class hType;
class XTT_Row;
class XTT_Attribute;
class XTT_Statesgroup;
class GStateConnection;
// -----------------------------------------------------------------------------
/**
* \class      XTT_State
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       16.11.2009
* \brief      Class definition that represents the XTT_State.
*/
class XTT_State
{
private:

     XTT_Statesgroup* _parent;                         ///< Parent container of the object
     QString _stateId;                                 ///< State ID
     bool _changed;                                    ///< Indicated if the state has been changed
     
     QHash<XTT_Attribute*, hSet*>* _initialValues;     ///< The set of pairs that contains the list of attributes with the initial values
     QHash<XTT_Attribute*, hSet*>* _finalValues;       ///< The set of pairs that contains the list of attributes with the final values
     QList<XTT_Row*>* _trajectory;                     ///< The list of rows that are fired in order to reach the final state from the initial state
     
     GStateConnection* _connection;                    ///< The pointer to the graphical representation of the connection, between state and output tables
     
public:
     
     /// \brief Constructor of XTT_State class.
     ///
     /// \param __parent - pointer to the parent object
     XTT_State(XTT_Statesgroup* __parent);
     
     /// \brief Destructor of XTT_State class.
     ~XTT_State(void);
     
     /// \brief Function that copies all the values to the destination object
	///
     /// \param __destination - pointer to the destination object
	/// \return pointer to the new object
     XTT_State* copyTo(XTT_State* __destination = NULL);
     
     /// \brief Function that returns if the object has been changed
	///
	/// \return true if the object has been changed, otherwise false
     bool changed(void);
     
     /// \brief Function that sets the status of changed paramteter
	///
     /// \param __c - the new status of changed parameter
	/// \return no values return
     void setChanged(bool __c);
     
     /// \brief Function that returns the id of the state
	///
	/// \return the id of the state as the string
     QString id(void);
     
     /// \brief Function that returns the full name of the state
	///
	/// \return the full name of the state as the string
     QString fullname(void);
     
     /// \brief Function that returns pointer to the parent of the object
	///
	/// \return pointer to the parent of the object
     XTT_Statesgroup* parent(void);
     
     /// \brief Function that returns pointer to the list of intial values
	///
	/// \return pointer to the list of intial values
     QHash<XTT_Attribute*, hSet*>* initialValues(void);
     
     /// \brief Function that returns pointer to the list of final values
	///
	/// \return pointer to the list of final values
     QHash<XTT_Attribute*, hSet*>* finalValues(void);
     
     /// \brief Function that returns pointer to the trajectory
	///
	/// \return pointer to the list of trajectory
     QList<XTT_Row*>* trajectory(void);
     
     /// \brief Function that returns if the state has defined trajecotry to the output
     ///
     /// \return if the state has defined trajecotry to the output
     bool hasTrajectory(void);
     
     /// \brief Function that selects the trajectory that starts from this state
     ///
     /// \brief __select - the status of selection
     /// \return no values return
     void selectTrajectory(bool __select);
     
     /// \brief Function that selects the state
     ///
     /// \brief __select - the status of selection
     /// \return no values return
     void selectState(bool __select);
     
     /// \brief Function that selects the state
     ///
     /// \brief __att - ppointer to the attribute
     /// \brief __select - the status of selection
     /// \return no values return
     void selectCell(XTT_Attribute* __att, bool __select);
     
     /// \brief Function that canceles the simulation results
     ///
     /// \return no values return
     void cancelSimulation(void);
     
     /// \brief Function that returns the list of atributes of the all elements in the state
	///
	/// \return list of atributes of the all elements in the state
     QList<XTT_Attribute*>* inputAttributes(void);
     
     /// \brief Function that returns the list of atributes of the all elements in the state
	///
	/// \return list of atributes of the all elements in the state
     QList<XTT_Attribute*>* outputAttributes(void);
     
     /// \brief Function that returns the pointer to the input attribute at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the pointer to the input attribute at the given position
     XTT_Attribute* inputAttributeAt(int __offset);
     
     /// \brief Function that resets the value of the attribute
     ///
     /// \param __attr - pointer to the attribute
     /// \param __value - the pointer to the input set that is realted to the given attribute
     /// \return no values return
     void resetInputValueTo(XTT_Attribute* __attr, hSet* __value);
     
     /// \brief Function that sets the value of the attribute
     ///
     /// \param __attr - pointer to the attribute
     /// \param __value - the pointer to the input set that is realted to the given attribute
     /// \return no values return
     void setInputValueAt(XTT_Attribute* __attr, hSet* __value);
     
     /// \brief Function that returns the pointer to the input set at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the pointer to the input set at the given position
     hSet* inputSetAt(int __offset);
     
     /// \brief Function that returns the pointer to the input set that is realted to the given attribute
	///
     /// \param __attr - pointer to the attribute
	/// \return the pointer to the input set that is realted to the given attribute
     hSet* inputSetAt(XTT_Attribute* __attr);
     
     /// \brief Function that sets the value of the attribute
	///
     /// \param __attr - pointer to the attribute
	/// \param __value - the pointer to the output set that is realted to the given attribute
     /// \return no values return
     void setOutputValueAt(XTT_Attribute* __attr, hSet* __value);
     
     /// \brief Function that removes from the input all states an given attribute
	///
     /// \param __attr - pointer to the given attribute
	/// \return no values return
     void removeInputAttribute(XTT_Attribute* __attr);
     
     /// \brief Function that removes from the all output states an given attribute
	///
     /// \param __attr - pointer to the given attribute
	/// \return no values return
     void removeOutputAttribute(XTT_Attribute* __attr);
     
     /// \brief Function that returns the pointer to the output attribute at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the pointer to the output attribute at the given position
     XTT_Attribute* outputAttributeAt(int __offset);
     
     /// \brief Function that returns the pointer to the output set at the given position
	///
     /// \param __offset - the offset from the first element of the container
	/// \return the pointer to the output set at the given position
     hSet* outputSetAt(int __offset);
     
     /// \brief Function that returns the pointer to the output set that is realted to the given attribute
	///
     /// \param __attr - pointer to the attribute
	/// \return the pointer to the output set that is realted to the given attribute
     hSet* outputSetAt(XTT_Attribute* __attr);
     
     /// \brief Function that refreshes the list of output attributes
	///
	/// \return no values return
     void refreshOutputAttributesList(void);
     
     /// \brief Function that sets the id of the state
	///
     /// \param __newid - the new id of the state as the string
	/// \return no values return
     void setId(QString __newid);
     
     /// \brief Function that sets the xtt to state
	///
	/// \return true if everthing was ok, otherwise false
     bool loadState(void);
     
     /// \brief Function that returns the pointer to the connection
     ///
     /// \return the pointer to the connection
     GStateConnection* uiConnection(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns the HMR representation of this state element
	///
     /// \param __fullstatename - the full name of the state
	/// \return the HMR representation of this state element as string
     QString toProlog(QString __fullstatename);
     
     /// \brief Function that returns if the row object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool stateVerification(QString& __error_message);
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Static function that trie to find pointer to the attribute that has the given ID
	///
	/// \param __attid - id of the attribute
	/// \return pointer to the attribute, if attribute does not exists then it returns NULL
     static XTT_Attribute* findAttribute(QString __attid);
     
     /// \brief Static function that returns the prefix of the id of state.
     ///
     /// \return the state prefix of the id as string.
     static QString stateIdPrefix(void);
};
// -----------------------------------------------------------------------------
#endif
