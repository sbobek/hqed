/**
 * \file     XTT_ConnectionsList.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      04.05.2007
 * \version     1.15
 * \brief     This file contains class definition that is connetion between tables.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_ConnectionsListH
#define XTT_ConnectionsListH
//---------------------------------------------------------------------------

#include <QDomDocument>
#include <QList>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_Connections.h"
//---------------------------------------------------------------------------
/**
* \class       XTT_ConnectionsList
* \author      Krzysztof Kaczor kinio4444@gmail.com
* \date        04.05.2007 
* \brief       Class definition that is connection between tables.
*/
class XTT_ConnectionsList
{
private:

     struct loadConnectionItem
     {
          QDomElement DOMlink;
          QString srcTable;
          QString srcRow;
     };
     
     XTT_Connections** fConnections;    ///< Table of pointers to the connections.
     unsigned int fCount;               ///< Size of table of pointers to the connections.
     unsigned int fNextConID;           ///< Next connection id.
     bool fChanged;                     ///< Determines whether object was modyfied.
     
     QList<loadConnectionItem> _loadedConns;   ///< The list of connections that has been discovered in the file. Connections loading is done after all tables reading.

public:

     /// \brief Constructor XTT_ConnectionsList class.
     XTT_ConnectionsList(void);

     /// \brief Destructor XTT_ConnectionsList class.
     ~XTT_ConnectionsList(void);

     /// \brief Function reads data from file in XTTML 1.0 format.
     ///
     /// \param root Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returns false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function retrieves next connection id.
     ///
     /// \return Next connection id.
     unsigned int NextConID(void){ return fNextConID; }

     /// \brief Function retrieves size of the table of pointers to the connections.
     ///
     /// \return Size of the table of pointers to the connections.
     unsigned int Count(void){ return fCount; }

     /// \brief Function retrieves pointer to the connection.
     ///
     /// \param _index Table index.
     /// \return Pointer to the connection.
     XTT_Connections* Connection(unsigned int _index);

     /// \brief Function retrieves decision whether the connection was modyfied.
     ///
     /// \return True when the connection was modyfied, otherwise function returns false.
     bool Changed(void);

     /// \brief Function sets status of modyfication for the connection.
     ///
     /// \param _b Status of modyfication for the connection.
     /// \return No return value.
     void setChanged(bool _b);

     /// \brief Function adds new connection.
     ///
     /// \return Pointer to the new connection.
     XTT_Connections* Add(void);
     
     /// \brief Function adds new connection.
     ///
     /// \param _newConn Pointer to the new connection.
     /// \return No return value.
    void Add(XTT_Connections* _newConn);
    
     /// \brief Function adds new connection to load.
     ///
     /// \param __DOMconn - reference to the connection section
     /// \param __srcTable - id of source table
     /// \param __srcRow - id of source row
     /// \param __msg - reference to the error messages
     /// \return 0-on succes otherwise 1
     int Add(QDomElement& __DOMconn, QString __srcTable, QString __srcRow, QString& __msg);
     
     /// \brief Function loads connections from the _loadedConns list
     ///
     /// \param __msg - reference to the error messages
     /// \return 0-on succes otherwise 1
     int loadConnections(QString& __msg);

     /// \brief Function removes the connection.
     ///
     /// \param __index Index of the table.
     /// \return True when index is OK, otherwise function returns false.
    bool Delete(unsigned int __index);

     /// \brief Function clears the table of pointers to the connections.
     ///
     /// \return No return value.
    void Flush(void);

     /// \brief Function retrieves index of the connection.
     ///
     /// \param _id Table id.
     /// \return Index of the connection. When error occurs function returns -1.
     int IndexOfID(QString _id);

     /// \brief Function retrieves pointer to the connection.
     ///
     /// \param _id Table id.
     /// \return Pointer to the connection.
     XTT_Connections* _IndexOfID(QString _id);

     /// \brief Function retrieves pointer to the list of connections that go out of the table and the row.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return List of connections that go out from the row.
     QList<int>* IndexOfOutConnections(QString _tid, QString _rid);
     
     /// \brief Function retrieves pointer to the list of connections that go into the table and the row.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return List of connections ids that go into the table and the row.
     QList<QString>* IndexOfInConnections(QString _tid, QString _rid);
     
     /// \brief Function retrieves decicision whether connections that go out of the table and the row exist.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return True when the connections exist, otherwise function returns false.
     bool ExistsOutConnections(QString _tid, QString _rid);
     
     /// \brief Function retrieves decicision whether connections that go into the table and the row exist.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return True when the connections exist, otherwise function returns false.
     bool ExistsInConnections(QString _tid, QString _rid);

     /// \brief Function retrieves pointer to the list of connections that go out of the table and the row.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return List of connections that go out of the table and the row.
     QList<XTT_Connections*>* _IndexOfOutConnections(QString _tid, QString _rid);

     /// \brief Function retrieves pointer to the list of connections that go into the table and the row.
     ///
     /// \param _tid Table id.
     /// \param _rid Row id.
     /// \return List of connections that go into the table and the row.
     QList<XTT_Connections*>* _IndexOfInConnections(QString _tid, QString _rid);
     
     /// \brief Function retrieves pointer to the list of connections that go into the table.
     ///
     /// \param _tid Table id.
     /// \return Pointer to the list of connections that go into the table.
     QList<XTT_Connections*>* listOfInConnections(QString _tid);
     
     /// \brief Function retrieves pointer to the list of connections that go out of the table.
     ///
     /// \param _tid Table id.
     /// \return Pointer to the list of connections that go out of the table.
     QList<XTT_Connections*>* listOfOutConnections(QString _tid);

     /// \brief Function retrieves pointer to the list of connections that go into the table.
     ///
     /// \param _tid Table id.
     /// \return Pointer to the list of connections that go into the table.
     QList<QString>* listOfInConnections_Table(QString _tid);

     /// \brief Function retrieves pointer to the list of connections that go out of the table.
     ///
     /// \param _tid Table id.
     /// \return Pointer to the list of connections that go out of the table.
     QList<QString>* listOfOutConnections_Table(QString _tid);
     
     /// \brief Search for connections with the appropriate parameters of input and output
     ///
     /// \param stid - the sorurce table identifier
     /// \param srid - the sorurce row identifier
     /// \param dtid - the destination table identifier
     /// \param drid - the destination row identifier
     /// \return It return a list of connections with the appropriate parameters of input and output
     QList<XTT_Connections*>* listOfConnections(QString stid, QString srid, QString dtid, QString drid);

     /// \brief Function removes all connections connected to the table that has ID equal _tid.
     ///
     /// \param _tid - Table id.
     /// \return No return value.
     void RemoveConnectionsWithTableID(QString _tid);
     
     /// \brief Function removes all connections connected to the row including the row.
     ///
     /// \param _rid Row id.
     /// \return No return value.
     void RemoveConnectionsWithRowID(QString _rid);
     
     /// \brief Function removes all connections that go out of the row including the row.
     ///
     /// \param _rid Row id.
     /// \return No return value.
     void RemoveOutConnectionsWithRowID(QString _rid);
     
     /// \brief Function removes all connections that go into the row including the row.
     ///
     /// \param _rid Row id.
     /// \return No return value.
     void RemoveInConnectionsWithRowID(QString _rid);

     /// \brief Function repaints all connections connected to the table.
     ///
     /// \param _tid Table id.
     /// \return No return value.
     void RepaintConnectionRelatedWithTable(QString _tid);
     
     /// \brief Function updates the localizations parameters for each connection. It is important to use this function after table deleting
     ///
     /// \return No return value.
     void updateConnectionsParameters(void);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Search for connections which satisfied the search criteria
     ///
     /// \param _phrase - sarched phrase
     /// \param _caseSensitivity - if true search is CaseSensitivity
     /// \param *_elements - set of search criteria
     /// \param _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
     /// \return It return a list of string whcich contains informations about result of search
     QStringList* findConnections(QString _phrase, bool _caseSensitivity, bool* _elements, int _phrasesPolicy);
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the load connection item
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(loadConnectionItem __root, QString& __msg);
};
//---------------------------------------------------------------------------

extern XTT_ConnectionsList* ConnectionsList;
//---------------------------------------------------------------------------
#endif
