/*
 *	   $Id: XTT_Column.cpp,v 1.2 2010-01-08 19:47:40 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../C/Settings_ui.h"

#include "XTT_Attribute.h"
#include "XTT_Column.h"
// -----------------------------------------------------------------------------

// #brief Default contructor of the XTT_Column class
XTT_Column::XTT_Column(void)
{
     _sortCriterion = XTT_COLUMN_SORT_ALPHABETICAL;
     _width = Settings->xttDefaultColumnWidth();
     _abstractColID = 0;
     _contextAtribute = NULL;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the sorting criterion
//
// #return sorting criterion
int XTT_Column::sortCriterion(void)
{
     return _sortCriterion;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the width of the column
//
// #return width of the column
int XTT_Column::width(void)
{
     return _width;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the abstract id assigned to the column
//
// #return abstract id assigned to the column
unsigned int XTT_Column::abstractId(void)
{
     return _abstractColID;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the attribute realted with the column
//
// #return the pointer to the attribute realted with the column
XTT_Attribute* XTT_Column::attribute(void)
{
     return _contextAtribute;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the sorting criterion
//
// #param __criterion - identifier of criterion
// #return no values return
void XTT_Column::setSortCriterion(int __criterion)
{
     _sortCriterion = __criterion;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the width of the column
//
// #param __width - a new width of the column
// #return no values return
void XTT_Column::setWidth(int __width)
{
     _width = __width;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the abstract id assigned to the column
//
// #param __aid - a new abstract id assigned to the column
// #return no values return
void XTT_Column::setAbstractId(unsigned int __aid)
{
     _abstractColID = __aid;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the pointer to the attribute realted with the column
//
// #param __xttattribute - the pointer to the new attribute realted with the column
// #return no values return
void XTT_Column::setAttribute(XTT_Attribute* __xttattribute)
{
     _contextAtribute = __xttattribute;
}
// -----------------------------------------------------------------------------
