/*
 *        $Id: XTT_Row.cpp,v 1.39.4.4 2010-12-19 23:36:36 kkr Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"
#include "../../C/MainWin_ui.h"
#include "../../C/XTT/widgets/ErrorsListWidget.h"
#include "../hType.h"
#include "XTT_AttributeGroups.h"
#include "XTT_Attribute.h"
#include "XTT_TableList.h"
#include "XTT_Table.h"
#include "XTT_Row.h"
//---------------------------------------------------------------------------

// #brief Constructor XTT_Row class.
XTT_Row::XTT_Row(void)
{
     fCells = NULL;
     fLabel = "";
     fRowLength = 0;
     fRowID = "";
     fNextCellId = NULL;
     fParent = "";
     fChanged = false;
}
//---------------------------------------------------------------------------

// #brief Constructor XTT_Row class.
//
// #param _parent_id Row id of the parent table.
// #param _id Row id of the table.
// #param next_cell_id Next cell id of the table.
XTT_Row::XTT_Row(QString _parent_id, unsigned int _id, unsigned int* next_cell_id)
{
     fCells = NULL;
     fLabel = "";
     fRowLength = 0;
     fRowID = idPrefix() + QString::number(_id);
     fNextCellId = next_cell_id;
     fParent = _parent_id;
     fChanged = false;
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_Row class.
XTT_Row::~XTT_Row(void)
{
     // Usuniecie z listy bledow wyswietlanej w oknie glownym, dokowalnym
     if(fRowLength > 0)
          MainWin->xtt_errorsList->deleteItem(fParent, fCells[fRowLength-1]->CellId(), 1);

     // Usuwanie polaczen dolaczonych do tego wiersz
     ConnectionsList->RemoveConnectionsWithRowID(fRowID);

     if(!fRowLength)
          return;

     for(unsigned int i=0;i<fRowLength;i++)
          Delete(0);
}
//---------------------------------------------------------------------------

// #brief Function that makes the copy of the current object
//
// #param __destination - a destination object where all values will be copied
// #return Pointer to the copy object
XTT_Row* XTT_Row::copyTo(XTT_Row* __destination)
{
     XTT_Table* tParent = ParentPtr();
     if(tParent == NULL)
          return NULL;
          
     XTT_Row* result = __destination;
     if(__destination == NULL)
          result = new XTT_Row;

     result->fRowLength = fRowLength;
     result->fCells = new XTT_Cell*[fRowLength];
     for(unsigned int i=0;i<fRowLength;++i)
          result->fCells[i] = fCells[i]->copyTo();
     
     result->fLabel = fLabel;
     result->fRowID = idPrefix() + QString::number(tParent->nextRowId(true));
     result->fNextCellId = fNextCellId;
     result->fParent = fParent;
     result->fChanged = false;
          
     return result;
}
//---------------------------------------------------------------------------

// #brief Function that creates a row that is filled by cells containg default values
//
// #return No values return
void XTT_Row::createDefaultRow(void)
{
     for(unsigned int i=0;i<fRowLength;++i)
          fCells[i]->createDefaultContent();
}
//---------------------------------------------------------------------------

// #brief Function retrieves the pointer to the parent table.
//
// #return pointer to the parent table.
XTT_Table* XTT_Row::ParentPtr(void)
{
     return TableList->_IndexOfID(Parent());
}
//---------------------------------------------------------------------------

// #brief Function retrieves row index in the parent table.
//
// #return row index in the parent table.
int XTT_Row::RowIndex(void)
{
     XTT_Table* parent = ParentPtr();
     if(parent == NULL)
          return -1;
          
     return parent->indexOfRowID(RowId());
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the cell.
//
// #param index Index of the cell.
// #return Pointer to the cell.
XTT_Cell* XTT_Row::Cell(int index)
{
     // Sprawdzanie czy index jest ok
     if((index < 0) || (index >= (int)fRowLength))
          return NULL;

     return fCells[index];
}
//---------------------------------------------------------------------------

// #brief Function retrieves decision whether the row was modyfied.
//
// #return True when the row was modyfied, otherwise function returns false.
bool XTT_Row::Changed(void)
{
     for(unsigned int i=0;i<fRowLength;i++)
          if(fCells[i]->Changed())
               return true;
               
     return fChanged;
}
//---------------------------------------------------------------------------

// #brief Function determines whether the row was modyfied.
//
// #param _b True when the row was modyfied, otherwise false.
// #return No return value.
void XTT_Row::setChanged(bool _c)
{
     for(unsigned int i=0;i<fRowLength;i++)
          fCells[i]->setChanged(_c);
               
     fChanged = _c;
}
//---------------------------------------------------------------------------

// #brief Function inserts the cell in defined place.
//
// #param _index Index of the place where the cell should be set.
// #param context Context of the cell.
// #return True when operation succeeds, otherwise function returns false.
bool XTT_Row::Insert(unsigned int index, int context)
{
     // Sprawdzanie czy index jest ok
     if((((int)index) < 0) || (index > fRowLength))
          return false;

     XTT_Cell** tmp = new XTT_Cell*[fRowLength+1];
     int tmp_i = 0;

     // Kopiowanie starej tablicy
     for(unsigned int i=0;i<=fRowLength;i++)
          if(i != index)
               tmp[i] = fCells[tmp_i++];

     // Usuwanie strej tablicy, jesli nie pusta
     if(fRowLength)
          delete [] fCells;

     // Zapisanie lokalizacji
     fCells = tmp;

     // Utworzenie nowego obiektu
     fCells[index] = new XTT_Cell(fParent, ++(*fNextCellId), context);

     // Powiekszenie tablicy
     fRowLength++;
     
     // Zarejestrowanie zmiany obiektu
     fChanged = true;

     // Zwrocenie ze wszystko sie udalo
     return true;
}
//---------------------------------------------------------------------------

// #brief Function removes the cell from the table.
//
// #param index Index of the cell that has to be removed.
// #return True when operation succeeds, otherwise function returns false.
bool XTT_Row::Delete(unsigned int index)
{
     // Sprawdzanie czy index jest ok
     if((((int)index) < 0) || (index >= fRowLength))
          return false;

     XTT_Cell** tmp = new XTT_Cell*[fRowLength-1];
     int tmp_i = 0;

     // Kopiowanie starej tablicy
     for(unsigned int i=0;i<fRowLength;i++)
          if(i != index)
               tmp[tmp_i++] = fCells[i];

     // Usuwanie strej tablicy, jesli nie pusta
     if(fRowLength)
     {
          delete fCells[index];
          delete [] fCells;
     }

     // Zapisanie lokalizacji
     fCells = tmp;

     // Pomniejszenie tablicy
     fRowLength--;
     
     // Zarejestrowanie zmiany obiektu
     fChanged = true;

     // Zwrocenie ze wszystko sie udalo
     return true;
}
//---------------------------------------------------------------------------

// #brief Function exchanges two cells with each other.
//
// #param _index1 Index of the first cell.
// #param _index2 Index of the second cell.
// #return True when operation succeeds, otherwise function returns false.
bool XTT_Row::Exchange(unsigned int _index1, unsigned int _index2)
{
     if((((int)_index1) < 0) || (_index1 >= fRowLength))
          return false;
     if((((int)_index2) < 0) || (_index2 >= fRowLength))
          return false;

     XTT_Cell* tmp = fCells[_index1];
     fCells[_index1] = fCells[_index2];
     fCells[_index2] = tmp;
     
     // Zarejestrowanie zmiany obiektu
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to connections that go out of the row.
//
// #return Pointer to connections that go out of the row.
QList<XTT_Connections*>* XTT_Row::OutConnection(void)
{
     return ConnectionsList->_IndexOfOutConnections(fParent, fRowID);
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to connections that go out of the row.
//
// #param _clabel Label of the connection.
// #return Pointer to connections that go out of the row.
QList<XTT_Connections*>* XTT_Row::OutConnection(QString _clabel)
{
     QList<XTT_Connections*>* oc = ConnectionsList->_IndexOfOutConnections(fParent, fRowID);
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(int c=0;c<oc->size();++c)
          if(oc->at(c)->Label() == _clabel)
               res->append(oc->at(c));
     
     delete oc;
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to connections that go out of the row and have labels.
//
// #return Pointer to connections that go out of the row and have labels.
QList<XTT_Connections*>* XTT_Row::OutLabeledConnection(void)
{
     QList<XTT_Connections*>* oc = ConnectionsList->_IndexOfOutConnections(fParent, fRowID);
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(int c=0;c<oc->size();++c)
          if(oc->at(c)->Label() != "")
               res->append(oc->at(c));
     
     delete oc;
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to connections that go into the row and have labels.
//
// #return Pointer to connections that go into the row and have labels.
QList<XTT_Connections*>* XTT_Row::InLabeledConnection(void)
{
     QList<XTT_Connections*>* ic = InConnection();
     QList<XTT_Connections*>* res = new QList<XTT_Connections*>;
     
     for(int c=0;c<ic->size();++c)
          if(ic->at(c)->Label() != "")
               res->append(ic->at(c));
     
     delete ic;
     return res;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to connections that go into the row.
//
// #return Pointer to connections that go into the row.
QList<XTT_Connections*>* XTT_Row::InConnection(void)
{
     return ConnectionsList->_IndexOfInConnections(fParent, fRowID);
}
//---------------------------------------------------------------------------

// #brief Function sets pointer to the cell.
//
// #param _index Index of the cell.
// #param ptr Pointer to the cell.
// #return True when function succeds, otherwise function returns false.
bool XTT_Row::setCell(int index, XTT_Cell* ptr)
{
     if((index < 0) || ((unsigned int)index >= fRowLength))
          return false;

     fCells[index] = ptr;
     
     // Zarejestrowanie zmiany obiektu
     fChanged = true;

     return true;
}
//---------------------------------------------------------------------------

// #brief Function sets ID of the row.
//
// #param __id - new row identifier
// #return No return value.
void XTT_Row::setID(QString __id)
{
     fRowID = __id;
     
     // Updateing list of in connections
     QList<XTT_Connections*>* ic = InConnection();
     for(int i=0;i<ic->size();++i)
          ic->at(i)->setDesRow(fRowID);
     delete ic;
     
     // Updateing list of out connections
     QList<XTT_Connections*>* oc = OutConnection();
     for(int i=0;i<oc->size();++i)
          oc->at(i)->setSrcRow(fRowID);
     delete oc;
}
//---------------------------------------------------------------------------

// #brief Function sets label of the row of the table.
//
// #param _label Label of the row of the table.
// #return No return value.
void XTT_Row::setLabel(QString _label)
{ 
     fLabel = _label;
     
     // Zarejestrowanie zmiany obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function determines whether the rows should be selected.
//
// #param _sel Determines whether the rows should be selected.
// #return No return value.
void XTT_Row::setSelected(bool _sel)
{
     for(unsigned int i=0;i<fRowLength;i++)
          fCells[i]->setSelected(_sel);
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the cell.
//
// #param _id Cell id.
// #return Index of the cell.
int XTT_Row::indexOfCellID(QString _id)
{
     int res_ = -1;
     
     for(unsigned int i=0;i<fRowLength;i++)
          if(fCells[i]->CellId() == _id)
               return (int)i;
          
     return res_;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of the cell.
//
// #param __cellPtr - pointer to the cell.
// #return Index of the cell.
int XTT_Row::indexOfCell(XTT_Cell* __cellPtr)
{
     for(unsigned int i=0;i<fRowLength;i++)
          if(fCells[i] == __cellPtr)
               return (int)i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function that returns if the row object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_Row::rowVerification(QString& __error_message)
{    
     bool res = true;
     
     // checking row features
     if(fParent == 0)
     {
          __error_message += hqed::createErrorString("", "", "Undefined row parent", 0, 2);
          res = false;
     }
     if(fRowID == 0)
     {
          __error_message += hqed::createErrorString("", "", "Undefined row ID", 0, 2);
          res = false;
     }

     // checking cells
     for(unsigned int a=0;a<fRowLength;a++)
          res = fCells[a]->cellVerification(__error_message) && res;
          
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_Row::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     for(unsigned int a=0;a<fRowLength;a++)
          res = fCells[a]->updateTypePointer(__old, __new) && res;
     
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Row::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fRowLength;a++)
               res = fCells[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int a=0;a<fRowLength;a++)
               res = fCells[a]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the cell to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents the cell as string.
QString XTT_Row::toProlog(int /*__option*/)
{
     QString result = "";
     
     QString precond = "";
     QString actions = "";
     QString othrsac = "";
     QString linkdst = "";
     
     // searching for parent table
     XTT_Table* parent = TableList->_IndexOfID(fParent);
     if(parent == NULL)
          return "";
          
     // creating schema
     QList<XTT_Attribute*>* ac = parent->ContextAtributes(1);
     QList<XTT_Attribute*>* cc = parent->ContextAtributes(4);
     
     for(int i=0;i<ac->size();++i)
     {
          precond += (i==0?"":",\n       ") + fCells[i]->toProlog();
     }
     for(int i=0;i<cc->size();++i)
     {
          bool isAction = false;
          QString tmpStr = fCells[ac->size()+i]->toProlog(&isAction);
          if(!isAction)
               actions += (actions.isEmpty()?"":",\n       ") + tmpStr;
          if(isAction)
               othrsac += (othrsac.isEmpty()?"":",\n       ") + tmpStr;
     }
     
     // out connections
     QList<XTT_Connections*>* oclist = OutConnection();
     for(int i=0;i<oclist->size();++i)
     {
          QString dtid = oclist->at(i)->DesTable();
          QString drid = oclist->at(i)->DesRow();
          //bool isCut = oclist->at(i)->IsCut();         // but not supported yet
          
          XTT_Table* table = TableList->_IndexOfID(dtid);
          if(table == NULL)
               continue;
              
          int rindex = table->indexOfRowID(drid);
          
          linkdst += (i==0?"":",") + QString("\n    :") + hqed::mcwp(table->Title());
          if(rindex > -1)
               linkdst += "/" + QString::number(rindex+1);
     }
     
     // memory dealocation
     delete ac;
     delete cc;
     delete oclist;
          
     // building the result string
     result += "xrule " + hqed::mcwp(parent->Title()) + "/" + QString::number(parent->indexOfRowID(RowId())+1) + ":";
     //if(!precond.isEmpty())
          result += "\n      [" + precond + "]";
     //if(!actions.isEmpty())
          result += "\n    ==>\n      [" + actions + "]";
     if(!othrsac.isEmpty())
          result += "\n    **>\n      [" + othrsac + "]";
          
     result += linkdst + ".";
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_Row::idPrefix(void)
{
     return "rul_";
}
//---------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 1.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returns false.
bool XTT_Row::FromXTTML_2_0(QDomElement& root)
{
     if(root.tagName().toLower() != "row")
          return false;

     bool res = true;

     // Reszta zakladamy ze jest ok

     // Spis tagow
     QStringList tags;
     tags.clear();
     tags << "<row" << "<cell";
     
     QString rlabel = root.attribute("label", "#NAN#");          
     if((rlabel != "") && (rlabel != "#NAN#"))          // label nie jest wymagane
          fLabel = rlabel;

     QDomElement child = root.firstChildElement("cell");
     while(!child.isNull())
     {
          // Kiedy linia konczaca linie danych
          // Dodajemy nowy atrybut
          Insert(fRowLength, 1);

          // Wczytujemy dane z pliku
          res = fCells[fRowLength-1]->FromXTTML_2_0(child) && res;
               
          // Wczytywanie kolejnej komorki
          child = child.nextSiblingElement("cell");
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 format.
//
// #param _csvdoc - writing device
// #return List of all data in drools5 format.
void XTT_Row::out_Drools_5_0_DecTab(QTextStream* _csvdoc)
{
     *_csvdoc << ",\"" << RowId() << "\",\"1\",";

     for(int cell=0;cell<(int)Length();++cell)
          Cell(cell)->out_Drools_5_0_DecTab(_csvdoc);

     *_csvdoc << "\"true\",";
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Row::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(__mode != 4)
     {
          *res << "<rule id=\"" + RowId() + "\">";

          int cell = 0;
          XTT_Table* tp = ParentPtr();
          bool condition = false;
          bool decision = false;
          
          if(tp != NULL)
          {
               condition = (tp->ContextSize(1) > 0);
               decision = ((tp->ContextSize(4) + tp->ContextSize(5)) > 0);
          }
          
               // Saving conditional part
               if(condition)
               {
                    *res << "<condition>";
                    for(;(cell<(int)Length()) && (Cell(cell)->Context()==1);++cell)
                    {
                         QStringList* tmp = Cell(cell)->out_HML_2_0(__mode);
                         for(int i=0;i<tmp->size();++i)
                              *res << "\t" + tmp->at(i);
                         delete tmp;
                    }
                    *res << "</condition>";
               }
               
               // Saving action part
               if(decision)
               {
                    *res << "<decision>";
                    for(;(cell<(int)Length()) && ((Cell(cell)->Context()==4) || (Cell(cell)->Context()==5));++cell)
                    {
                         QStringList* tmp = Cell(cell)->out_HML_2_0(__mode);
                         for(int i=0;i<tmp->size();++i)
                              *res << "\t" + tmp->at(i);
                         delete tmp;
                    }
                    *res << "</decision>";
               }
               
               // Saving links
               QList<XTT_Connections*>* clist = OutConnection();
               for(int i=0;i<clist->size();++i)
               {
                    QStringList* tmp = clist->at(i)->out_HML_2_0();
                    for(int i=0;i<tmp->size();++i)
                         *res << "\t" + tmp->at(i);
                    delete tmp;
               }
               delete clist;
          
          *res << "</rule>";
     }
     
     if(__mode == 4)
     {
          for(int cell=0;cell<(int)Length();++cell)
          {
               QStringList* tmp = Cell(cell)->out_HML_2_0(__mode);
               for(int i=0;i<tmp->size();++i)
                    *res << "\t" + tmp->at(i);
               delete tmp;
          }
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_Row::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(__mode != 4)
     {
          _xmldoc->writeStartElement("rule");
          _xmldoc->writeAttribute("id", RowId());

          int cell = 0;
          XTT_Table* tp = ParentPtr();
          bool condition = false;
          bool decision = false;
          
          if(tp != NULL)
          {
               condition = (tp->ContextSize(1) > 0);
               decision = ((tp->ContextSize(4) + tp->ContextSize(5)) > 0);
          }
          
               if(condition)
               {
                    // Saving conditional part
                    _xmldoc->writeStartElement("condition");
                    for(;(cell<(int)Length()) && (Cell(cell)->Context()==1);++cell)
                         Cell(cell)->out_HML_2_0(_xmldoc, __mode);
                    _xmldoc->writeEndElement();
               }
               
               if(decision)
               {
                    // Saving action part
                    _xmldoc->writeStartElement("decision");
                    for(;(cell<(int)Length()) && ((Cell(cell)->Context()==4) || (Cell(cell)->Context()==5));++cell)
                         Cell(cell)->out_HML_2_0(_xmldoc, __mode);
                    _xmldoc->writeEndElement();
               }
               
               // Saving links
               QList<XTT_Connections*>* clist = OutConnection();
               for(int i=0;i<clist->size();++i)
                    clist->at(i)->out_HML_2_0(_xmldoc, __mode);
               delete clist;
          
          _xmldoc->writeEndElement();
     }
     
     if(__mode == 4)
     {
          for(int cell=0;cell<(int)Length();++cell)
               Cell(cell)->out_HML_2_0(_xmldoc, __mode);
     }
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_Row::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "rule";
     
     QDomElement DOMrule = __root;
     if(DOMrule.tagName().toLower() != elementName.toLower())
          return 0;
          
     XTT_Table* parentTable = ParentPtr();
     if(parentTable == NULL)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Critical error: Undefined rule parent." + hqed::createDOMlocalization(DOMrule), 0, 2);
          result++;
          return result;
     }
          
     // Reading table data
     QString rid = DOMrule.attribute("id", "");

     // Reading row data
     if(rid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined rule identifier. The object will not be read." + hqed::createDOMlocalization(DOMrule), 0, 2);
          result++;
          return result;
     }
     fRowID = rid;

     // Reading condition part
     QDomElement DOMcond = DOMrule.firstChildElement("condition");
     if(!DOMcond.isNull())
     {
          QMap<QString, int> usedAttsIds;   // the list of founded attributes
               
          // Reading relations
          QDomElement DOMrel = DOMcond.firstChildElement("relation");
          while(!DOMrel.isNull())
          {
               QDomElement DOMatt = DOMrel.firstChildElement("attref");
               if(!DOMatt.isNull())
               {
                    QString aid = DOMatt.attribute("ref", "");
                    int index = parentTable->IndexOfAttributeID(aid, 1);   // Searchnig for attribute in this context
                    
                    // If attribute exists in this context
                    if(index > -1)
                    {
                         XTT_Cell* cell = Cell(index);
                         bool alreadyUsed = usedAttsIds.contains(aid);
                         if(alreadyUsed)       // if new column is needed
                         {
                              int newColIndex = index + usedAttsIds.value(aid);
                              if(parentTable->RowCount() == 1)
                              {
                                   parentTable->InsertCol(newColIndex);
                                   parentTable->setColContext(newColIndex, 1);
                                   parentTable->setContextAtributes(newColIndex, AttributeGroups->FindAttribute(aid));
                              }
                              cell = Cell(newColIndex);
                              usedAttsIds.insert(aid, usedAttsIds.value(aid)+1);
                         }
                         if(!alreadyUsed) 
                              usedAttsIds.insert(aid, 1);

                         result += cell->in_HML_2_0(DOMrel, __msg);        // Loading cell
                    }
               }
               DOMrel = DOMrel.nextSiblingElement("relation");
          }
     }

     // Reading decision part
     QDomElement DOMdcsn = DOMrule.firstChildElement("decision");
     if(!DOMdcsn.isNull())
     {
          QMap<QString, int> usedAttsIds;   // the list of founded attributes

          // Reading relations
          QDomElement DOMtrs = DOMdcsn.firstChildElement("trans");
          while(!DOMtrs.isNull())
          {
               QDomElement DOMatt = DOMtrs.firstChildElement("attref");
               if(!DOMatt.isNull())
               {
                    QString aid = DOMatt.attribute("ref", "");
                    int index = parentTable->IndexOfAttributeID(aid, 4);   // Searchnig for attribute in this context

                    // If attribute exists in this context
                    if(index > -1)
                    {
                         XTT_Cell* cell = Cell(index);
                         bool alreadyUsed = usedAttsIds.contains(aid);
                         if(alreadyUsed)       // if new column is needed
                         {
                              int newColIndex = index + usedAttsIds.value(aid);
                              if(parentTable->RowCount() == 1)
                              {
                                   parentTable->InsertCol(newColIndex);
                                   parentTable->setColContext(newColIndex, 4);
                                   parentTable->setContextAtributes(newColIndex, AttributeGroups->FindAttribute(aid));
                              }
                              cell = Cell(newColIndex);
                              usedAttsIds.insert(aid, usedAttsIds.value(aid)+1);
                         }
                         if(!alreadyUsed) 
                              usedAttsIds.insert(aid, 1);

                         result += cell->in_HML_2_0(DOMtrs, __msg);        // Loading cell
                    }
               }
               DOMtrs = DOMtrs.nextSiblingElement("trans");
          }
 
          // Reading actions
          QDomElement DOMacn = DOMdcsn.firstChildElement("action");
          while(!DOMacn.isNull())
          {
               parentTable->InsertCol(parentTable->ColCount());
               parentTable->setColContext(parentTable->ColCount()-1, 5);
               result += Cell(parentTable->ColCount()-1)->in_HML_2_0(DOMtrs, __msg);        // Loading cell
               
               DOMacn = DOMacn.nextSiblingElement("action");
          }
     }
     
     // Reading links
     QDomElement DOMlink = DOMrule.firstChildElement("link");
     while(!DOMlink.isNull())
     {
          result += ConnectionsList->Add(DOMlink, parentTable->TableId(), fRowID, __msg);
          DOMlink = DOMlink.nextSiblingElement("link");
     }

     return result;
}
// -----------------------------------------------------------------------------
