/**
 * \file     XTT_Row.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      24.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is row of the table.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef XTT_ROWH
#define XTT_ROWH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_Cell.h"
#include "XTT_ConnectionsList.h"
// -----------------------------------------------------------------------------

class XTT_Table;
// -----------------------------------------------------------------------------

/**
* \class      XTT_Row
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      24.04.2007 
* \brief      Class definition that is row of the table.
*/
class XTT_Row
{
private:

     bool fChanged;                ///< Determines whether the row was modyfied.
     XTT_Cell** fCells;            ///< Pointer to the cells of the row of the table.
     unsigned int fRowLength;      ///< Length of the row of the table.
     unsigned int* fNextCellId;    ///< Next cell id of the table.
     QString fParent;              ///< Parent table.
     QString fLabel;               ///< Label of the row of the table.     
     QString fRowID;               ///< Row id of the table.

public:

     /// \brief Constructor XTT_Row class.
     XTT_Row(void);

     /// \brief Constructor XTT_Row class.
     ///
     /// \param _parent_id Row id of the parent table.
     /// \param _id Row id of the table.
     /// \param next_cell_id Next cell id of the table.
     XTT_Row(QString _parent_id, unsigned int _id, unsigned int* next_cell_id);

     /// \brief Destructor XTT_Row class.
     ~XTT_Row(void);
     
     /// \brief Function that makes the copy of the current object
     ///
     /// \param __destination - a destination object where all values will be copied
     /// \return Pointer to the copy object
     XTT_Row* copyTo(XTT_Row* __destination = NULL);
     
     /// \brief Function that creates a row that is filled by cells containg default values
     ///
     /// \return No values return
     void createDefaultRow(void);

     /// \brief Function reads data from file in XTTML 1.0 format.
     ///
     /// \param root Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returns false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function retrieves parent table.
     ///
     /// \return Parent table.
     QString Parent(void){ return fParent; }
     
     /// \brief Function retrieves the pointer to the parent table.
     ///
     /// \return pointer to the parent table.
     XTT_Table* ParentPtr(void);
     
     /// \brief Function retrieves row id of the table.
     ///
     /// \return Row id of the table.
     QString RowId(void){ return fRowID; }
     
     /// \brief Function sets ID of the row.
     ///
     /// \param __id - new row identifier
     /// \return No return value.
     void setID(QString __id);
     
     /// \brief Function retrieves row index in the parent table.
     ///
     /// \return row index in the parent table.
     int RowIndex(void);

     /// \brief Function retrieves length of the row of the table.
     ///
     /// \return Length of the row of the table.
     unsigned int Length(void){ return fRowLength; }

     /// \brief Function retrieves pointer to the cell.
     ///
     /// \param index Index of the cell.
     /// \return Pointer to the cell.
     XTT_Cell* Cell(int index);

     /// \brief Function retrieves label of the row of the table.     
     ///
     /// \return Label of the row of the table.     
     QString Label(void){ return fLabel; }

     /// \brief Function retrieves decision whether the row was modyfied.
     ///
     /// \return True when the row was modyfied, otherwise function returns false.
     bool Changed(void);

     /// \brief Function sets label of the row of the table.
     ///
     /// \param _label Label of the row of the table.
     /// \return No return value.
     void setLabel(QString _label);

     /// \brief Function sets pointer to the cell.
     ///
     /// \param _index Index of the cell.
     /// \param ptr Pointer to the cell.
     /// \return True when function succeds, otherwise function returns false.
     bool setCell(int _index, XTT_Cell* ptr);

     /// \brief Function determines whether the row was modyfied.
     ///
     /// \param _b True when the row was modyfied, otherwise false.
     /// \return No return value.
     void setChanged(bool _b);

     /// \brief Function inserts the cell in defined place.
     ///
     /// \param _index Index of the place where the cell should be set.
     /// \param context Context of the cell.
     /// \return True when operation succeeds, otherwise function returns false.
     bool Insert(unsigned int _index, int context);

     /// \brief Function removes the cell from the table.
     ///
     /// \param index Index of the cell that has to be removed.
     /// \return True when operation succeeds, otherwise function returns false.
     bool Delete(unsigned int index);

     /// \brief Function exchanges two cells with each other.
     ///
     /// \param _index1 Index of the first cell.
     /// \param _index2 Index of the second cell.
     /// \return True when operation succeeds, otherwise function returns false.
     bool Exchange(unsigned int _index1, unsigned int _index2);

     /// \brief Function retrieves pointer to connections that go out of the row.
     ///
     /// \return Pointer to connections that go out of the row.
     QList<XTT_Connections*>* OutConnection(void);

     /// \brief Function retrieves pointer to connections that go out of the row.
     ///
     /// \param _clabel Label of the connection.
     /// \return Pointer to connections that go out of the row.
     QList<XTT_Connections*>* OutConnection(QString _clabel);

     /// \brief Function retrieves pointer to connections that go out of the row and have labels.
     ///
     /// \return Pointer to connections that go out of the row and have labels.
     QList<XTT_Connections*>* OutLabeledConnection(void);

     /// \brief Function retrieves pointer to connections that go into the row and have labels.
     ///
     /// \return Pointer to connections that go into the row and have labels.
     QList<XTT_Connections*>* InLabeledConnection(void);

     /// \brief Function retrieves pointer to connections that go into the row.
     ///
     /// \return Pointer to connections that go into the row.
     QList<XTT_Connections*>* InConnection(void);

     /// \brief Function retrieves index of the cell.
     ///
     /// \param _id Cell id.
     /// \return Index of the cell.
     int indexOfCellID(QString _id);
     
     /// \brief Function retrieves index of the cell.
     ///
     /// \param __cellPtr - pointer to the cell.
     /// \return Index of the cell.
     int indexOfCell(XTT_Cell* __cellPtr);

     /// \brief Function determines whether the rows should be selected.
     ///
     /// \param _sel Determines whether the rows should be selected.
     /// \return No return value.
     void setSelected(bool _sel = true);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that returns if the row object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool rowVerification(QString& __error_message);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function gets all data in drools5 format.
     ///
     /// \param _csvdoc - writing device
     /// \return List of all data in drools5 format.
     void out_Drools_5_0_DecTab(QTextStream* _csvdoc);

     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     // ----------------------------------------------------------
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
// -----------------------------------------------------------------------------
#endif
