 /**
 * \file     XTT_Attribute.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      24.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is attribute, variable in XTT diagram.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl

 */
#ifndef XTT_ATTRIBUTEH
#define XTT_ATTRIBUTEH

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------
#define XTT_ATT_CLASS_RO 0      // Read-only
#define XTT_ATT_CLASS_RW 1      // Read/write
#define XTT_ATT_CLASS_WO 2      // Write-only
#define XTT_ATT_CLASS_ST 3      // State
// -----------------------------------------------------------------------------

class hType;
class hMultipleValue;
// -----------------------------------------------------------------------------
/**
* \class      XTT_Attribute
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      28.04.2007 
* \brief      Class definition that is attribute, variable in XTT diagram.
*/
class XTT_Attribute
{
private:

     QString fAttrID;          ///< Identifier of attribute, "" - no ID.
     QString fAttrName;        ///< Name of attribute.
     QString fAttrAcronym;     ///< Acronym of attribute.
     QString fAttrDesc;        ///< Describtion of attribute.
     QString fAttrClbId;       ///< Callback Id of attribute.

     /// Type of attribute relation.
     /// \li 0 - input - XTT_ATT_CLASS_RO.
     /// \li 1 - middle - XTT_ATT_CLASS_RW.
     /// \li 2 - output - XTT_ATT_CLASS_WO.
     /// \li 3 - not supported by xttml - XTT_ATT_CLASS_ST.
     unsigned int fRelationsType;

     hMultipleValue* fAttrValue;     ///< Value of attribute.
     hType* fAttrType;               ///< Type of attribute: (IN XTTML: 0 - NotDefined, bool, integer, float, symbolic, enum)

     /// Type of attribute constraint.
     /// \li 0 - no constraint.
     /// \li 1 - ragne.
     /// \li 2 - set.
     //unsigned int fAttrConstrType;

     bool fChanged;     ///< Determines whether object properties was changed since last save(true) or not(false).

public:

     static QString _not_definded_operator_;           ///< Appearance of 'not_defined' operator.
     static QString _any_operator_;                    ///< Appearance of 'any' operator.
     static QString _min_operator_;                    ///< Appearance of 'min' operator.
     static QString _max_operator_;                    ///< Appearance of 'max' operator.
     static QString _true_operator_;                   ///< Appearance of 'true' operator.
     static QString _false_operator_;                  ///< Appearance of 'false' operator.
     static QStringList _class_attribute_;             ///< String representation of the attribute classes

    /// \brief Constructor XTT_Attribute class.
     XTT_Attribute(void);

    /// \brief Constructor XTT_Attribute class.
     ///
     /// \param _id Identifier of attribute.
     XTT_Attribute(QString _id);

     /// \brief Destructor XTT_Attribute class.
     ~XTT_Attribute(void);

     /// \brief Function checks whether new attribute value can be set.
     ///
     /// \param _v Name of attribute.
     /// \return Decision whether new attribute value can be set(true) or not(false).
     bool checkValue(hMultipleValue* _v);

     /// \brief Function gets all data in xttml 1.0 format.
     ///
     /// \return List of all data in xttml 1.0 format.
     QStringList* XTTML_1_0(void);
     
     /// \brief Function fixes attribute value when it does not belong to the range.
     ///
     /// \return No return value.
     void FixValue(void);

     /// \brief Function gets identifier of attribute.
     ///
     /// \return Identifier of attribute.
     QString Id(void){ return fAttrID; }

     /// \brief Function returns path of the attribute
     ///
     /// \return path of the attribute
     QString Path(void);
     
     /// \brief Function gets name of attribute.
     ///
     /// \return Name of attribute.
     QString Name(void);
	 
	 /// \brief Function gets Callback Id of Attribute.
     ///
     /// \return Callback Id of Attribute.
     QString CallbackId(void);

     /// \brief Function gets name of attribute with first capital letter
     ///
     /// \return Name of attribute.
     QString CapitalizedName(void);

     /// \brief Function gets acronym of attribute.
     ///
     /// \return Acronym of attribute.
     QString Acronym(void);

     /// \brief Function gets describtion of attribute.
     ///
     /// \return Describtion of attribute.
     QString Description(void){ return fAttrDesc; }

     /// \brief Function gets value of attribute.
     ///
     /// \return Value of attribute.
     hMultipleValue* Value(void){ return fAttrValue; }

     /// \brief Function gets type of attribute.
     ///
     /// \return Type of attribute.
     hType* Type(void){ return fAttrType; }

     /// \brief Function gets type of relation for attribute.
     ///
     /// \return Type of relation for attribute.
     unsigned int Class(void){ return fRelationsType; }

     /// \brief Function gets representation of constraints.
     ///
     /// \return String contains representation of constraints.
     QString ConstraintsRepresentation(void);

     /// \brief Function returns the multiplicity of the value
     ///
     /// \return the multiplicity of the value - true if value can be multiple, if single false
     bool multiplicity(void);
     
     /// \brief Function that returns if the attribute is a conceptual atribute
     ///
     /// \return true if attribute is conceptual otherwise false
     bool isConceptual(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the xtt attribute object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool attributeVerification(QString& __error_message);
     
     /// \brief Function gets decision whether object was modyfied or not.
     ///
     /// \return Decision whether object was modyfied or not.
     bool Changed(void){ return fChanged; }

     /// \brief Function sets the id of attribute.
     ///
     /// \param _new_id - id of attribute.
     /// \return No return value.
     void setId(QString _new_id);
     
     /// \brief Function sets name of attribute.
     ///
     /// \param _new_name Name of attribute.
     /// \return No return value.
     void setName(QString _new_name);
	 
     /// \brief Function sets Callback id of attribute.
     ///
     /// \param _new_clbid Callback id of attribute.
     /// \return No return value.
     void setCallbackId(QString _new_clbid);

     /// \brief Function sets name of acronym.
     ///
     /// \param _new_acronym Name of acronym.
     /// \return No return value.
     void setAcronym(QString _new_acronym);

     /// \brief Function sets describtion of acronym.
     ///
     /// \param _desc Describtion of acronym.
     /// \return No return value.
     void setDescription(QString _desc);

     /// \brief Function sets value of attribute.
     ///
     /// \param _v - Value of attribute.
     /// \return True when operation succeeds else false.
     bool setValue(hMultipleValue* _v);

     /// \brief Function sets type of relation for attribute.
     ///
     /// \param _rt Type of relations.
     /// \return True when operation succeeds else false.
     bool setClass(unsigned int _rt);

     /// \brief Function sets type of attribute.
     ///
     /// \param _attr_type Type of attribute.
     /// \return No return value.
     void setType(hType* _attr_type);
     
     /// \brief Function sets the value multiplicity of the attribute
     ///
     /// \param _mp if true the value is set as multiple, otherwise as single
     /// \return No return value.
     void setMultiplicity(bool _mp);

     /// \brief Function sets value that determines whether object was changed or not.
     ///
     /// \param _c Value that determines whether object was changed or not.
     /// \return No return value.
     void setChanged(bool _c);

     /// \brief Function copies attribute properties.
     ///
     /// \param _new - attribute with desired properties
     /// \return no values return
     void copyProperties(XTT_Attribute* _new);
     
     /// \brief Function that maps the object to the prolog code.
     ///
     /// \param __option - options of the output
     /// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function that returns the string name of the attribute class.
     ///
     /// \param __class - the numeric representation of the class.
     /// \return the string name of the attribute class.
     static QString classString(int __class);
     
     /// \brief Function that returns the string name of the attribute class compatible with the HML.
     ///
     /// \param __class - the numeric representation of the class.
     /// \return the string name of the attribute class that is compatible with the HML.
     static QString xmlClassString(int __class);
     
     /// \brief Function that returns the string name of the attribute class compatible with the HeaRT.
     ///
     /// \param __class - the numeric representation of the class.
     /// \return the string name of the attribute class that is compatible with the HeaRT.
     static QString prologClassString(int __class);

     /// \brief Function gets all data in drools5 java file format.
     ///
     /// \param _javafile - writing device
     /// \return no values return
     void out_Drools_Workspace(QTextStream* _javafile);

     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \return List of all data in hml 2.0 format.
     QStringList* out_HML_2_0(int __mode = 0);

     #if QT_VERSION >= 0x040300
        /// \brief Function saves all data in hml 2.0 format.
        ///
        /// \param _xmldoc Pointer to document.
        /// \param __mode - writing mode
        /// \li 0 - set mode
        /// \li 1 - domain mode
        /// \li 2 - expr mode
        /// \li 3 - eval mode
        /// \return No retur value.
        void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
     ///
     /// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
     int in_HML_2_0(QDomElement& __root, QString& __msg);
     // ----------------------------------------------------------

     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
// -----------------------------------------------------------------------------
#endif
