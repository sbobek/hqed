/**
 * \file     XTT_CallbackContainer.h
 * \author     Lukasz Finster, Sebastian Witowski
 * \date      24.06.2011
 * \version     1.15
 * \brief     This file contains class definition that is container for the attributes.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_CallbackContainer_H
#define XTT_CallbackContainer_H
//---------------------------------------------------------------------------

#include <QString>
#include <QList>
#include "XTT_Callback.h"
#include <QtGui>
#include <QRegExp>
#include "QXmlSimpleReader"
#include "XTT_AttributeGroups.h"
#include "XTT_AttributeList.h"
//---------------------------------------------------------------------------

/**
* \class        XTT_CallbackContainer
* \author       Lukasz Finster, Sebastian Witowski
* \date         18.06.2011
* \brief        Class definition of callback.
*/

class XTT_CallbackContainer
{

public:
     /// \brief Constructor XTT_Callback class.
     XTT_CallbackContainer();

     /// \brief Destructor XTT_Callback class.
     ~XTT_CallbackContainer();

     /// \brief Function adds new Callback to container.
     ///
     /// \param clb XTT_Callback object
     /// \return No return value
     void add(XTT_Callback clb);

     /// \brief Function deletes callback from container
     ///
     /// \param IDNo index of callback to delete
     /// \return No return value
     void del(int IDNo);

     /// \brief Function checks if exist callback with Id
     ///
     /// \param eID Callback id as QString
     /// \return true if callback exists, otherwise false
     bool exist(QString eID);

     /// \brief Function returns number of callbacks in container
     ///
     /// \return Number of callbacks as int
     int size();

     /// \brief Function deletes all callback from container
     ///
     /// \return No return value
     void clear();

     /// \brief Function checks if selected callback will be saved to library
     ///
     /// \param i index of callback in container
     /// \return Information as bool
     bool libsave(int i);

     /// \brief Function set the usage of callback in model
     ///
     /// \param id Id of callback
     /// \param use usage as bool
     /// \return No return value
     void setUsed(QString id, bool use);

     /// \brief Function saves all callback to library in XML format.
     ///
     /// \param *xmlWriter XmlStreaWriter object
     /// \return No return value
     void allToXML(QXmlStreamWriter* xmlWriter);

     /// \brief Function returns address of callback in container with selected index
     ///
     /// \param i index of callback in container.
     /// \return Address to callback object.
     XTT_Callback* get(int i);

     /// \brief Function returns index of callback with selected id
     ///
     /// \param IDsearch Id of callback.
     /// \return index of callback in container
     int getByID(QString IDsearch);

     /// \brief Function returns name of callback from definition
     ///
     /// \param index Id of callback.
     /// \return name of callback
     QString getPrologName(int index);

     /// \brief Function generates next free id.
     ///
     /// \return New id as QString.
     QString generateID();

     /// \brief Function return Definitions of all used callback.
     ///
     /// \return Prolog code with callback definitions
     QString toProlog();

     /// \brief Functions returns all used callback in XML format
     ///
     /// \param *xmlWriter Pointer to XMLStreamWriter object
     /// \return No return value.
     void usedToXML(QXmlStreamWriter* xmlWriter);

     /// \brief Function reads callback from XML format.
     ///
     /// \param __root Address of QDomElement
     /// \param __msg Address to message string
     /// \return No return value
     int in_XML(QDomElement& __root, QString& __msg);

     /// \brief Function reads callback from library file
     ///
     /// \return No return value
     int loadFormLibrary();

     /// \brief Function that returns the file name of the callback library
     ///
     /// \return the file name of the callback library
     QString getLibFile(void);

private:
     QString libFile;                   ///< librery file name
     QString idprefix;                  ///< prefix of callback id
     QList<XTT_Callback> *clbcks;       ///< List of callback in container
};
// -----------------------------------------------------------------------------

extern XTT_CallbackContainer *CallbackContainer;
// -----------------------------------------------------------------------------

#endif // XTT_CallbackContainer_H
