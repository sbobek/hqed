/**
 * \file     XTT_TableList.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      04.05.2007 
 * \version     1.15
 * \brief     This file contains class definition that is container for the table.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_TableListH
#define XTT_TableListH
//---------------------------------------------------------------------------

#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
//---------------------------------------------------------------------------

class hType;
class XTT_Row;
class XTT_Cell;
class XTT_Table;
class XTT_State;
class XTT_Attribute;
class QTreeWidgetItem;

//---------------------------------------------------------------------------
/**
* \class  XTT_TableList
* \author Krzysztof Kaczor kinio4444@gmail.com
* \date   04.05.2007 
* \brief  Class definition that is container for the table.
*/
class XTT_TableList
{
private:

     XTT_Table** fTables;          ///< Table of pointers to the tabel.
     unsigned int fCount;          ///< Size of the table of pointers to the tabel.
     unsigned int fNextTableID;    ///< Next table id. 
     unsigned int fNextRowID;      ///< Next row id.
     bool fChanged;                ///< Determines whether the table was modyfied.

     /// \brief Function creates tree mask.
     ///
     /// \param _ptr Pointer to the tree.
     /// \param _tid Table id.
     /// \return No return value.
     void AddChilds(QTreeWidgetItem* _ptr, unsigned int _tid);

public:

     /// \brief Constructor XTT_TableList class.
     XTT_TableList(void);

     /// \brief Destructor XTT_TableList class.
     ~XTT_TableList(void);

     /// \brief Function reads data from file in XTTML 2.0 format.
     ///
     /// \param root - Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returns false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function retrieves next table id.
     ///
     /// \return Next table id.
     unsigned int NextTableID(void){ return fNextTableID; }

     /// \brief Function retrieves next row id.
     ///
     /// \return Next row id.
     unsigned int NextRowID(void){ return fNextRowID; }
     
     /// \brief Function searchs for uniq table id
     ///
     /// \return uniq table id as string
     QString findUniqTableID(void);

     /// \brief Function retrieves size of the table of pointers to the tabel.
     ///
     /// \return Size of the table of pointers to the tabel.
     unsigned int Count(void){ return fCount; }

     /// \brief Function retrieves pointer to the table.
     ///
     /// \param _index - Index of the table.
     /// \return Pointer to the table. When index is incorrect funtion returns NULL.
     XTT_Table* Table(unsigned int _index);

     /// \brief Function retrieves decision whether table was modyfied.
     ///
     /// \return True when table was modyfied, otherwise function returns false.
     bool Changed(void);

     /// \brief Function makes decision whether table was modyfied.
     ///
     /// \param _b - Decision whether table was modyfied.
     /// \return No return value.
     void setChanged(bool _b);

     /// \brief Create and add new table
     ///
     /// \return poiter to new table
     XTT_Table* Add(void);
     
     /// \brief Adds new table to container
     ///
     /// \param *_new - pointer to new table
     /// \return true on succes otherwise false
     bool Add(XTT_Table* _new);

     /// \brief Function removes the table.
     ///
     /// \param _index - Index of the table.
     /// \return True when table index was OK, otherwise function returns false.
     bool Delete(unsigned int _index);

     /// \brief Function sets new name of the table.
     ///
     /// \param _baseName - Base name of the table.
     /// \return Base name of the table with table index.
     QString newBasicTableName(QString _baseName = "Table");

     /// \brief Function clears table of pointers to the tabel.
     ///
     /// \return No return value.
     void Flush(void);
     
     /// \brief Returns list of the state names
     ///
     /// \return Returns list of the state names
     QStringList stateNames(void);
     
     /// \brief Returns list which contains all tables
     ///
     /// \return Returns list which contains all tables
     QList<XTT_Table*>* tables(void);
     
     /// \brief Returns list of all rows pointers from tables
     ///
     /// \return pointer to the list of all rows pointers from tables
     QList<XTT_Row*>* rows(void);
     
     /// \brief Returns list of predecessor tables 
     ///
     /// \param _id - identifier of successor
     /// \return Returns list of predecessor tables 
     QList<XTT_Table*>* parents(QString _id);
     
     /// \brief Returns list of predecessor tables 
     ///
     /// \param *_table - poiter to successor
     /// \return Returns list of predecessor tables 
     QList<XTT_Table*>* parents(XTT_Table* _table);
     
     /// \brief Returns list of successors tables 
     ///
     /// \param _id - identifier of predecessor
     /// \param skipParentNode - determines skiping node when the child is the same as parent (loop)
     /// \return Returns list of successors tables 
     QList<XTT_Table*>* childs(QString _id, bool skipParentNode = true);
     
     /// \brief Returns list of successors tables 
     ///
     /// \param *_table - poiter to predecessor
     /// \param skipParentNode - determines skiping node when the child is the same as parent (loop)
     /// \return Returns list of successors tables 
     QList<XTT_Table*>* childs(XTT_Table* _table, bool skipParentNode = true);
     
     /// \brief Function that adds new message about fired rule to executeMessagesListWidget.
     ///
     /// \param __tablename - title of the table in which the rule has been fired.
     /// \param __rowIndex - index of fired rule
     /// \param __cellIndex - index of fired cell
     /// \param __notUpdatedState - state to be updated
     /// \return No return value.
     void addSimulationFiredRule(QString __tablename, QString __rowIndex, QString __cellIndex, XTT_State* __notUpdatedState);
     
     /// \brief Function that adds new messages about simualtion result to executeMessagesListWidget.
     ///
     /// \param __list - list of strinlists. The list has only one element that has the following format:
     /// \param __notUpdatedState - state to be updated
     /// \li at(0) - table_1 name
     /// \li at(1) - rule index
     /// \li at(2) - table_2 name
     /// \li at(3) - rule index
     /// \li ...
     /// \return No return value.
     void addSimulationResult(QList<QStringList>* __list, XTT_State* __notUpdatedState = NULL);

     /// \brief Function retrieves index of the table.
     ///
     /// \param _title - Title of the table.
     /// \return No return value.
     int IndexOfTitle(QString _title);

     /// \brief Function retrieves index of the table.
     ///
     /// \param _id - Id of the table.
     /// \return Index of the table.
     int IndexOfID(QString _id);

     /// \brief Function retrieves pointer to the table.
     ///
     /// \param _title - Title of the table.
     /// \return Pointer to the table.
     XTT_Table* _IndexOfTitle(QString _title);

     /// \brief Function retrieves pointer to the table.
     ///
     /// \param _id - Table id.
     /// \return Pointer to the table.
     XTT_Table* _IndexOfID(QString _id);
     
     /// \brief Function that search for the table that has a given title. It checks if the table can have such title w.r.t. to table type. There can be more than one fact table with the same title
     ///
     /// \param __title - given title
     /// \param __ignoreIndex - usually the index of the table that runs this method. If no specified then -1
     /// \return index of the table if exists otherwise -1
     int indexOfTitle(QString __title, int __ignoreIndex = -1);

     /// \brief Function retrieves index to the first table that has the type.
     ///
     /// \param _type - Type of the table.
     /// \return Index to the table.
     int IndexOfType(unsigned int _type);

     /// \brief Function retrieves list of all headers of tables.
     ///
     /// \param __uniq - indicates if the list cannot contain repeated titles
     /// \return List of all headers of tables.
     QStringList TablesTitles(bool __uniq = false);

     /// \brief Function retrieves decision whether the table is empty.
     ///
     /// \return Decision whether the table is empty.
     bool IsEmpty(void);

     /// \brief Function retrieves decision whether the table is empty.
     ///
     /// \param _index - Index of the table.
     /// \return No return value.
     void Refresh(int _index);

     /// \brief Function refreshes all table.
     ///
     /// \return No return table.
     void RefreshAll(void);

     /// \brief Function analyzes all cells.
     ///
     /// \return No return table.
     void AnalyzeAllCells(void);

     /// \brief Function analyzes connections of all tables.
     ///
     /// \return No return table.
     void AnalyzeAllTablesConnections(void);

     /// \brief Function analyzes connections of the table.
     ///
     /// \param _tid - Table id.
     /// \return No return table.
     void AnalyzeTableConnections(QString _tid);
     
     /// \brief Function search for the table that contains given row
     ///
     /// \param __ptr - pointer to the row.
     /// \return pointer to the table that contains the given row, is row doesn't exists return NULL
     XTT_Table* findRow(XTT_Row* __ptr);
     
     /// \brief Function search for the table that contains given cell
     ///
     /// \param __ptr - pointer to the cell.
     /// \return pointer to the table that contains the given cell, is cell doesn't exists return NULL
     XTT_Table* findCell(XTT_Cell* __ptr);
     
     /// \brief Search for tables with the given name and type
     ///
     /// \param __name - name of the table. If the string is empty each table satisfy this criterion.
     /// \param __ttype - type of thr table. If value is -1 each table type satisfy this criterion.
     /// \return It return a list of pointers to the tables that satisfy the search criteria
     QList<XTT_Table*>* findTables(QString __name, int __ttype);
     
     /// \brief Search for tables which satisfied the search criteria
     ///
     /// \param _phrase - sarched phrase
     /// \param _caseSensitivity - if true search is CaseSensitivity
     /// \param *_elements - set of search criteria
     /// \param _phrasesPolicy - Policy for the phrases separated by space. Allowed values: CONJUNCTION_POLICY and DISJUNCTION_POLICY
     /// \return It return a list of string whcich contains informations about result of search
     QStringList* findTables(QString _phrase, bool _caseSensitivity, bool* _elements, int _phrasesPolicy);
     
     /// \brief Search for tables which have appropriate attribute in one of their contexts
     ///
     /// \param *_setOfTables - search set
     /// \param *_contexts - determines in which contexts works search process
     /// \param *_contextAttribute - pointer to wanted attribute
     /// \return It return a list of tables which satisfied search criteria
     QList<XTT_Table*>* findTables(QList<XTT_Table*>* _setOfTables, bool* _contexts, XTT_Attribute* _contextAttribute);
     
     /// \brief Search for tables which have appropriate attribute in one of their contexts
     ///
     /// \param *_contexts - determines in which contexts works search process
     /// \param *_contextAttribute - pointer to wanted attribute
     /// \return It return a list of tables which satisfied search criteria
     QList<XTT_Table*>* findTables(bool* _contexts, XTT_Attribute* _contextAttribute);
     
     /// \brief Search for tables which have appropriate attribute in one of their contexts
     ///
     /// \param *_setOfTables - search set
     /// \param _ctx - number of context to search. [1;4]
     /// \param *_contextAttributes - list of attributes
     /// \param _eqsize - determines search type. When true context must have the same size as _contextAttributes, otherwise context size can be greater
     /// \return It return a list of tables which satisfied search criteria
     QList<XTT_Table*>* findTables(QList<XTT_Table*>* _setOfTables, int _ctx, QList<XTT_Attribute*>* _contextAttributes, bool _eqsize);
     
     /// \brief Search for tables which have appropriate attribute in one of their contexts
     ///
     /// \param _ctx - number of context to search. [1;4]
     /// \param *_contextAttributes - list of attributes
     /// \param _eqsize - determines search type. When true context must have the same size as _contextAttributes, otherwise context size can be greater
     /// \return It return a list of tables which satisfied search criteria
     QList<XTT_Table*>* findTables(int _ctx, QList<XTT_Attribute*>* _contextAttributes, bool _eqsize);
     
     
     /// \brief Search for tables which contains in one of their context given attributes
     ///
     /// \param *_ca - attributes for conditional context
     /// \param *_aa - attributes for action context
     /// \param *_with_no_pattern - poiter to the table which can't exists in result
     /// \return It return a list of similar tables
     QList<XTT_Table*>* findSimilarTables(QList<XTT_Attribute*>* _ca, QList<XTT_Attribute*>* _aa, XTT_Table* _with_no_pattern = NULL);
     
     /// \brief Search for tables which are similar to given table. Function intruduced for convinience.
     ///
     /// \param *_pattern - pattern table
     /// \return It return a list of similar tables without pattern table
     QList<XTT_Table*>* findSimilarTables(XTT_Table* _pattern);
     
     /// \brief Search for tables that contains input attributes in the conditional contex
     ///
     /// \return It return a list of tables
     QList<XTT_Table*>* inputTables(void);
     
     /// \brief Search for ends tables which can be interpreted as root of tables tree
     ///
     /// \return It return a list of tables
     QList<XTT_Table*>* rootsTables(void);
     
     /// \brief Returns all of tables which belongs to input table family
     ///
     /// \param  _id - identifier of input table
     /// \return It return a list of tables which belongs to input table family
     QList<XTT_Table*>* findFamily(QString _id);
     
     /// \brief Function that return a list of tables where the first tables have the smallest numer of incomming connection, and the last ones the biggest
     ///
     /// \return a list of tables where the first tables have the smallest numer of incomming connection, and the last ones the biggest
     QList<XTT_Table*>* tablesOrder(void);
     
     /// \brief Recursive function which creates genealogy tree for the table
     ///
     /// \param _id - identifier of current table
     /// \param _currentFamily - list of founded family members
     /// \return No values returs
     void createGenealogyTree(QString _id, QList<XTT_Table*>* _currentFamily);
     
     /// \brief Arranges tables on the scene
     ///
     /// \return No values returs
     void tablesLayout(void);
     
     /// \brief Arranges tables on the scene
     ///
     /// \param *_roots - list of tree roots
     /// \return No values returs
     void createTree(QList<XTT_Table*>* _roots);
     
     /// \brief Checks if the first list contains at least one element from the second list
     ///
     /// \param *_first - first list
     /// \param *_second - second list
     /// \return Index of value in the first list which can be found in the second list
     int hasCommonParent(QList<XTT_Table*>* _first, QList<XTT_Table*>* _second);
     
     /// \brief Analysis tree levels
     ///
     /// \param currLevel - current level on which all parents should be found
     /// \param *_allNodes - all added nodes
     /// \param *_nodes - list of nodes separated on levels
     /// \param *_heights - list of heights values for each level and node
     /// \param *_widths - list of maximum widths of levels
     /// \return No values returs
     void getTreeGeometry(int currLevel, QList<XTT_Table*>* _allNodes, QList<QList<XTT_Table*>*>* _nodes, QList<QList<float>*>* _heights, QList<float>* _widths);
     
     /// \brief Creates table with given attributes
     ///
     /// \param *_conAtts - conditional attributes
     /// \param *_actAtts - action attributes
     /// \return Pointer to created table, if the table exists retursn poiter to that table
     XTT_Table* createTable(QList<XTT_Attribute*>* _conAtts, QList<XTT_Attribute*>* _actAtts);
     
     /// \brief Merges columns of given tables. The first table is the main (result of similarTables) table and the second is the subtable
     ///
     /// \param *_first - fist table - main table
     /// \param *_second - the second table - subtable
     /// \return true if the columns in _first table had been changed
     bool mergeTables(XTT_Table* _first, XTT_Table* _second);
     
     /// \brief Adds new table template generated from ARD+
     ///
     /// \param *_conAtts - conditional attributes
     /// \param *_actAtts - action attributes
     /// \return true if table not exists, otherwise false
     bool addTableTemplate(QList<XTT_Attribute*>* _conAtts, QList<XTT_Attribute*>* _actAtts);
     
     /// \brief Perform optimalization procedure for XTT generated from ARD+
     ///
     /// \return 0 - if succes, 1 - when dimesnions of list aren't correct
     int optimizeXTT(void);
     
     /// \brief Function that searches for best connection for Fact table
     ///
     /// \param __factTable - pointer to thr fact table
     /// \param __msgs - reference to the String of messages
     /// \return 0 on succes greater than zero on error(s)
     int connectFactTable(XTT_Table* __factTable, QString& __msgs);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that returns if the tables objects definitions are correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool tablesVerification(QString& __error_message);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that maps the rows of the tables to the prolog code.
     ///
     /// \return The prolog code that represents the rules as string.
     QString rulesToProlog(void);
     
     /// \brief Function that maps the tables schemas to the prolog code.
     ///
     /// \return The prolog code that represents the schemas of the tables as string.
     QString schemasToProlog(void);
     
     /// \brief Function gets all data in drools5 format.
     ///
     /// \param __csvdoc - writing device
     /// \return List of all data in drools5 format.
     void out_Drools_5_0_DecTab(QTextStream* __csvdoc);

     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);

     /// \brief Function saves all data into Drools 5.0 format.
     /// \author Lukasz Lysik (llysik@gmail.com)
     ///
     /// \param _xmldoc Pointer to document.
     /// \return No return value.
     void out_Drools_5_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
};
//---------------------------------------------------------------------------

extern XTT_TableList* TableList;
//---------------------------------------------------------------------------
#endif
