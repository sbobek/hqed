/**
 * \file     XTT_Cell.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      24.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is the cell of XTT.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_CELL_H
#define XTT_CELL_H
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>
#include <QMap>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_Attribute.h"
// -----------------------------------------------------------------------------

#define CONTEXT_COUNT 5
// -----------------------------------------------------------------------------

class hType;
class hValue;
class XTT_Table;
class hFunction;
class hExpression;
// -----------------------------------------------------------------------------
/**
* \class      XTT_Cell
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      24.04.2007 
* \brief      Class definition that is the cell of XTT.
*/
class XTT_Cell
{
private:

     QString fParent;              ///< Parent of the cell.
     QString fCellID;              ///< Cell id. Value 0 means that id is not defined.
     unsigned int fCellIdx;        ///< Cell index
     QString fStrContent;          ///< Str Content of the cell - keept for compatibility with xttml
     XTT_Attribute* fAttrType;     ///< Pointer to the attribute that can be set in the cell.
     hMultipleValue* fContent;     ///< The expression in the cell

     bool fIgnore;                 ///< Determines whether the cell should be ignored.

     /// Context id of the cell.
     /// \li 0 - not defined.
     /// \li 1 - decision.
     /// \li 2 - assert.
     /// \li 3 - retract.
     /// \li 4 - action.
     /// \li 5 - *
     int fContext;

     bool favr;                    ///< Determines whether comparing of attributes should be turned on.
     bool fNoError;                ///< Denotes if the cell contains error
     QString fCellError;               ///< Last cell error.
     
     /// \brief Function sets cell error.
     ///
     /// \param _cellError Text contains error message.
     /// \return No return value.
     void setCellError(QString _cellError);

     /// \brief Function calculates all sine functions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString sin_func(QString fname, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all cosine functions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString cos_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all tangent functions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString tan_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all absolute values that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString abs_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all root values that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString root_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all values to the power that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString pow_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all factories that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString fac_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all multiplications that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString mul_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all divisions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString div_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all additions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString add_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all substractions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString sub_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all logarithmic functions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString log_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     /// \brief Function calculates all modulo divisions that are included in expression.
     ///
     /// \param fname Function name.
     /// \param expr Reference to expression.
     /// \param __expr_map pointer to the map of the expressions objects
     /// \return Modyfied expression.
     QString mod_func(QString name, QString& expr, QMap<QString, hExpression*>* __expr_map);

     bool fChanged;           ///< Determines whether the cell was changed(True) or not(False).
     bool fIsSelected;          ///< Determines whether the cell was selected(True) or not(False).

public:

     /// \brief Constructor XTT_Cell class.
     XTT_Cell(void);

     /// \brief Constructor XTT_Cell class.
     ///
     /// \param _parent Pointer to the parent of the cell.
     /// \param _id Cell id.
     /// \param _context Cell context.
     XTT_Cell(QString _parent, unsigned int _id, int _context);

     /// \brief Destructor XTT_Cell class.
     ~XTT_Cell(void);

     /// \brief Function reads data from file in XTTML 2.0 format.
     ///
     /// \param root Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returnes false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function that makes the copy of the current object
     ///
     /// \param __destination - a destination object where all values will be copied
     /// \return Pointer to the copy object
     XTT_Cell* copyTo(XTT_Cell* __destination = NULL);
     
     /// \brief Function that changes the cell operator
     ///
     /// \param __operatorname - name of a new cell operator
     /// \return No values return
     void setCellOperator(QString __operatorname);
     
     /// \brief Function that changes the cell operator
     ///
     /// \param __operator - new cell operator
     /// \param __makeCopy - indicates if the function must make a copy of the function object
     /// \return No values return
     void setCellOperator(hFunction* __operator, bool __makeCopy = false);
     
     /// \brief Function that creates a default content of the cell
     ///
     /// \return No values return
     void createDefaultContent(void);
     
     /// \brief Function retrieves pointer to the parent of the cell.
     ///
     /// \return Pointer to the parent of the cell.
     QString Parent(void){ return fParent; }

     /// \brief Function retrieves cell id.
     ///
     /// \return Cell id.
     QString CellId(void){ return fCellID; }

     unsigned int CellIdx(void) { return fCellIdx; }

     /// \brief Function retrieves contents of the cell.
     ///
     /// \return Contents of the cell.
     hMultipleValue* Content(void);
     
     /// \brief Function returns the strContent of the cell that comes from xttml file
     ///
     /// \return the strContent of the cell that comes from xttml file
     QString strContent(void);

     /// \brief Function retrieves pointer to the attribute that can be set in the cell.
     ///
     /// \return Pointer to the attribute that can be set in the cell.
     XTT_Attribute* AttrType(void){ return fAttrType; }

     /// \brief Function retrieves type of attribute operator.
     ///
     /// \return Type of attribute operator.
     //unsigned int OperatorType(void){ return fOperator; }

     /// \brief Function retrieves decision whether the cell should be ignored or not.
     ///
     /// \return True when the cell should be ignored, false when the cell should not be ignored.
     bool Ignore(void){ return fIgnore; }

     /// \brief Function retrieves context of the cell.
     ///
     /// \return Context of the cell.
     int Context(void) { return fContext; }

     /// \brief Function retrieves whether mode of attribute comparing in the cell is turned on.
     ///
     /// \return True when mode of attribute comparing in the cell is turned on, otherwise function returns false.
     bool enableAVR(void){ return favr; }

     /// \brief Function retrieves last cell error.
     ///
     /// \param addLocalizationInfo Determines whether information about error localization should be added.
     /// \return Last cell error.
     QString CellError(bool addLocalizationInfo = true);

     /// \brief Function retrieves decision whether the cell was changed.
     ///
     /// \return True when the cell was changed, otherwise function returns false.
     bool Changed(void);

     /// \brief Function retrieves decision whether the contents of the cell is correct.
     ///
     /// \return True when the contents of the cell is correct, otherwise function returns false.
     bool isCorrectSyntax(void);

     /// \brief Function selects or deselects the cell on the scene.
     ///
     /// \param _isSelected Determines whether the cell is selected.
     /// \return No return value.
     void setSelected(bool _isSelected = true);

     /// \brief Function retrieves decision whether the cell is selected.
     ///
     /// \return True when the cell is selected, otherwise function returns false.
     bool isSelected(void);

     /// \brief Function sets type of the attribute.
     ///
     /// \param _xtt_attr_type Pointer to the type of the attribute.
     /// \return No return value.
     void setAttrType(XTT_Attribute* _xtt_attr_type);

     /// \brief Function sets contents of the cell.
     ///
     /// \param _content Contents of the cell.
     /// \return No return value.
     void setContent(hMultipleValue* _content);

     /// \brief Function sets context of the cell.
     ///
     /// \param _ctx Cell index.
     /// \return No return value.
     bool setContext(unsigned int _ctx);

     /// \brief Function sets whether the cell should be ignored(transparent).
     ///
     /// \param _i Determines whether the cell should be ignored(True), otherwise (false).
     /// \return No return value.
     void setIgnore(bool _i);

     /// \brief Function sets the mode of attribute comparing.
     ///
     /// \param _m Enable mode of attribute comparing(True), disable(False).
     /// \return No return value.
     void setAVR(bool _m);

     /// \brief Function decides whether the cell was modyfied.
     ///
     /// \param _n True when the cell was modyfied, otherwise false.
     /// \return No return value.
     void setChanged(bool _n);
     
     /// \brief Return the string representation of the cell expression
     ///
     /// \return String representation of the cell expression
     QString ValueRepresentation(void);

     /// \brief Function analyzes syntax and makes the operation.
     ///
     /// \param expr Expression.
     /// \return True when expression is OK, otherwise function returnes false.
     bool Analyze(QString expr);

     /// \brief Function calculates the expression.
     ///
     /// \param expression Reference to the expression.
     /// \param start_index Index of the begining of operation.
     /// \param __value - contains the parsed expresion
     /// \param __expr_map - pointer to the map coantiner that contains the subexpreesions of the cell content
     /// \return Result of the operation.
     QString Calculate(QString& expression, int start_index, hValue*& __value, QMap<QString, hExpression*>*& __expr_map);

     /// \brief Function calculates operations that are included in the cell.
     ///
     /// \param cellAnalyze Determines whether the cell should be analyzed before.
     /// \return Result of executing.
     /// \li -1 - error.
     /// \li 0 - false.
     /// \li 1 - true.
     int Execute(bool cellAnalyze = true);

     /// \brief Function finds and retrieves pointer to the table parent.
     ///
     /// \return Pointer to the table parent.
     XTT_Table* TableParent(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that returns if the cell object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool cellVerification(QString& __error_message);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that maps the cell to the prolog code.
     ///
     /// \param __isAction - indicates if the cell expression is the "some_registered_action" that is not supported by hqed but is declared in PROLOG
     /// \return The prolog code that represents the cell as string.
     QString toProlog(bool* __isAction = NULL);
     
     /// \brief Function gets all data in drools5 format.
     ///
     /// \param _csvdoc - writing device
     /// \return List of all data in drools5 format.
     void out_Drools_5_0_DecTab(QTextStream* _csvdoc);

     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \li 4 - fact2states mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     // ----------------------------------------------------------
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
// -----------------------------------------------------------------------------

extern const QStringList smbl_opers;
extern const QStringList text_opers;
// -----------------------------------------------------------------------------
#endif
