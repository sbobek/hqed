/**
 * \file     XTT_AttributeList.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      25.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is container for the attributes.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_AttributeListH
#define XTT_AttributeListH
//---------------------------------------------------------------------------

#include <QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "XTT_Attribute.h"
//---------------------------------------------------------------------------

#define XTT_ATTRIBUTE_TYPE_CONCEPTUAL 0x00000001
#define XTT_ATTRIBUTE_TYPE_PHYSICAL   0x00000002
//---------------------------------------------------------------------------
/**
* \class      XTT_AttributeList
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      25.04.2007 
* \brief      Class definition that is container for the attributes.
*/
class XTT_AttributeList
{
private:

     unsigned int fLevel;          ///< Level of nesting for the attribute list.

     XTT_Attribute** fList;        ///< Lists of attributes - container.

     unsigned int fCount;          ///< Number of attributes - size of container.
     unsigned int* fNextId;        ///< Pointer to next attribute id.

     QString fId;                  ///< Identifier of the list - container id.
     QString fName;                ///< Name of container.
     QString fParentId;            ///< Identifier of the parent.
     QString fDescription;         ///< Describtion of container.

     bool fChanged;                ///< Determines whether object was changed.

public:

     /// \brief Constructor XTT_AttributeList class.
     XTT_AttributeList(void);

     /// \brief Constructor XTT_AttributeList class.
     ///
     /// \param _id Container id.
     XTT_AttributeList(unsigned int _id);

     /// \brief Destructor XTT_AttributeList class.
     ~XTT_AttributeList(void);

     /// \brief Function retrieves size of attribute list.
     ///
     /// \return Size of attribute list.
     unsigned int Count(void){ return fCount; }
     
     /// \brief Returns only those attrributes, with a given relation
     ///
     /// \param __relation - the value of the relation. If -1 all the types of relation are accepted
     /// \return the list of only those attrributes, with a given relation
     QList<XTT_Attribute*> classFilter(int __relation = -1);

     /// \brief Function retrieves list id.
     ///
     /// \return List id.
     QString ID(void){ return fId; }

     /// \brief Function retrieves next id for the attribute and increments current attribute.
     ///
     /// \return Next id for the attribute as String.
     QString NextId(void);

     /// \brief Function retrieves name of the container.
     ///
     /// \return Name of the container.
     QString Name(void){ return fName; }

     /// \brief Function retrieves describtion of the container.
     ///
     /// \return Describtion of the container.
     QString Description(void){ return fDescription; }

     /// \brief Function retrieves pointer to the attribute that has compatible abstraction level.
     ///
     /// \param index Index of the container that is relative to the attribute.
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return Pointer to the attribute. If index is incorect function returns NULL.
     XTT_Attribute* Item(int index, int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);

     /// \brief Function retrieves parent id.
     ///
     /// \return Parent id.
     QString ParentID(void){ return fParentId; }

     /// \brief Function retrievesgets level of nesting for the attribute list.
     ///
     /// \return Level of nesting for the attribute list.
     unsigned int Level(void){ return fLevel; }
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);

     /// \brief Function sets name of container.
     ///
     /// \param name Name of container.
     /// \return No return value.
     void setName(QString name);

     /// \brief Function sets describtion of container.
     ///
     /// \param desc Describtion of container.
     /// \return No return value.
     void setDescription(QString desc);

     /// \brief Function sets id of the group.
     ///
     /// \param id - new group id.
     /// \return No return value.
     void setId(QString id);
     
     /// \brief Function sets parent id.
     ///
     /// \param id Parent id.
     /// \return No return value.
     void setParentId(QString id);

     /// \brief Function sets level of nesting.
     ///
     /// \param level Level of nesting.
     /// \return No return value.
     void setLevel(unsigned int level);

     /// \brief Function determines whether object was changed or not.
     ///
     /// \param _c Determines whether object was changed(True) or not(False).
     /// \return No return value.
     void setChanged(bool _c);

     /// \brief Function adds new element to the attribute list.
     ///
     /// \return Pointer to the new attribute.
     XTT_Attribute* Add(void);

     /// \brief Function adds new element to the list - container.
     ///
     /// \param _new Pointer to the new attribute that is about to be added to the list.
     /// \return No return value.
     void Add(XTT_Attribute* _new);

     /// \brief Function removes one element from attribute list.
     ///
     /// \param index Index of the element that will be removed.
     /// \param delObj Determines whether object should be completely removed, or only pointer should be deleted.
     /// \return True when index is correct, otherwise false.
     bool Delete(int index, bool delObj = true);
     
     /// \brief Function sorts list of attributes.
     ///
     /// \return No return value.
     void sort(void);

     /// \brief Function creates list of attribute names from the attribute list.
     ///
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return List of attribute names.
     QStringList AttributeNameList(int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);

     /// \brief Function creates acronym list of attributes.
     ///
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return Acronym list of attributes.
     QStringList AttributeAcronymList(int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);

     /// \brief Function retrieves index of attribute that is relative to the name of attribute.
     ///
     /// \param __name - Name of attribute.
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return Index of attribute. If name is incorect function returns -1.
     int IndexOfName(QString __name, int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);

     /// \brief Function retrieves index of attribute that is relative to the name of acronym.
     ///
     /// \param _name Name of acronym. 
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return Index of attribute. If name is incorect function returns -1.
     int IndexOfAcronym(QString _name, int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);
     
     /// \brief Function retrieves index of attribute that is relative to the attribute id.
     ///
     /// \param __aID Attribute id.
     /// \param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
     /// \return Index of attribute. If name is incorect function returns -1.
     int IndexOfID(QString __aID, int __attType = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);
     
     /// \brief Function retrieves index of attribute that has the given pointer.
     ///
     /// \param __attr - pointer to the attribute
     /// \return Index of attribute. If pointer is incorect function returns -1.
     int IndexOfPointer(XTT_Attribute* __attr);
     
     /// \brief Function that checks if the given attribute has compatible abstraction level
     ///
     /// \param __attr - pointer to the attribute
     /// \param __attType - indicates the abstraction level of attribute
     /// \return true or false
     bool checkAttributeCompatibility(XTT_Attribute* __attr, int __attType);
     
     /// \brief Function that checks and if it detects errors fixes the acronyms of attributes
     ///
     /// \return no values return
     void checkAndFixAttributeAcronyms(void);
     
     /// \brief Function that translates the index in one abstraction to index in the another abstraction type
     ///
     /// \param __index - index of the attribute
     /// \param __inputAbstraction - input abstraction
     /// \param __outputAbstraction - output abstraction
     /// \return true or false
     int indexTranslate(int __index, int __inputAbstraction, int __outputAbstraction);

     /// \brief Function clears container. The list of attributes is completely removed.
     ///
     /// \return No return value.
     void Flush(void);
     
     /// \brief Function determines whether any attribute from the container was changed.
     ///
     /// \return True when any attribute from the container was changed, otherwise function returns false.
     bool Changed(void);
     
     /// \brief Function that returns if the attributes object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool attributesVerification(QString& __error_message);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     // ----------------------------------------------------------
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
//---------------------------------------------------------------------------

extern XTT_AttributeList* AttributeList;
//---------------------------------------------------------------------------
#endif
