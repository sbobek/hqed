/**
 * \file     XTT_Connections.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      24.04.2007 
 * \version     1.15
 * \brief     This file contains class definition that is connetion between tables.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_CONNECTIONSH
#define XTT_CONNECTIONSH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

class GConnection;
class GWire;
// -----------------------------------------------------------------------------
/**
* \class      XTT_Connections
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date      24.04.2007 
* \brief      Class definition that is connection between tables.
*/
class XTT_Connections
{
private:

     QString fConnID;              ///< Connection id.
     bool fConnCut;                ///< Determines whether the connection should block recurences.
     QString fConnLabel;           ///< Label of the connection.
     QString fConnDesc;            ///< Describtion of the connection.

     QString fSrcTablID;           ///< Source table id.
     QString fDesTablID;           ///< Destiny table id.
     QString fSrcRowID;            ///< Source row id.
     QString fDesRowID;            ///< Destiny row id.

     GConnection* fGConn;          ///< Pointer to the connection on the scene.     
     bool fChanged;                ///< Determines whether the connection was modyfied.

public:

     /// \brief Constructor XTT_Connections class.
     XTT_Connections(void);

     /// \brief Constructor XTT_Connections class.
     ///
     /// \param _id Connection id.
     XTT_Connections(unsigned int _id);

     /// \brief Destructor XTT_Connections class.
     ~XTT_Connections(void);

     /// \brief Function reads data from file in XTTML 1.0 format.
     ///
     /// \param root Reference to the section that concerns this attribute.
     /// \return True when data is ok, otherwise function returns false.
     bool FromXTTML_2_0(QDomElement& root);

     /// \brief Function retrieves connection id.
     ///
     /// \return Connection id.
     QString ConnId(void){ return fConnID; }

     /// \brief Function retrieves source table id.
     ///
     /// \return Source table id.
     QString SrcTable(void){return fSrcTablID; }

     /// \brief Function retrieves destiny table id.
     ///
     /// \return Destiny table id.
     QString DesTable(void){return fDesTablID; }

     /// \brief Function retrieves source row id.
     ///
     /// \return Source row id.
     QString SrcRow(void){return fSrcRowID; }

     /// \brief Function retrieves destiny row id.
     ///
     /// \return Destiny row id.
     QString DesRow(void){return fDesRowID; }

     /// \brief Function retrieves decision whether recursive of the connection should be blocked.
     ///
     /// \return True when recursive of the connection should be blocked, otherwise function returns false.
     bool IsCut(void){ return fConnCut; }

     /// \brief Function retrieves describtion of the connection.
     ///
     /// \return Describtion of the connection.
     QString Description(void){ return fConnDesc; }

     /// \brief Function retrieves label of the connection.
     ///
     /// \return Label of the connection.
     QString Label(void){ return fConnLabel; }

     /// \brief Function retrieves decision whether the connection was changed.
     ///
     /// \return True when  the connection was changed, otherwise function returns false.
     bool Changed(void){ return fChanged; }

     /// \brief Function retrieves pointer to the connection.
     ///
     /// \return Pointer to the connection.
     GConnection* gConnection(void){ return fGConn; }

     /// \brief Function sets connection property.
     ///
     /// \param _cut Determines whether recursive of the connection should be active.
     /// \return No return value.
     void setCut(bool _cut);

     /// \brief Function sets label of the connection.
     ///
     /// \param _label Label of the connection.
     /// \return No return value.
     void setLabel(QString _label);

     /// \brief Function sets describtion of the connection.
     ///
     /// \param _label Describtion of the connection.
     /// \return No return value.
     void setDescription(QString _label);

     /// \brief Function sets source table id.
     ///
     /// \param _src Source table id.
     /// \return No return value.
     void setSrcTable(QString _src);

     /// \brief Function sets destiny table id.
     ///
     /// \param _dst Destiny table id.
     /// \return No return value.
     void setDesTable(QString _dst);

     /// \brief Function sets source row id.
     ///
     /// \param _src Source row id.
     /// \return No return value.
     void setSrcRow(QString _src);

     /// \brief Function sets destiny row id.
     ///
     /// \param _dst Destiny row id.
     /// \return No return value.
     void setDesRow(QString _dst);

     /// \brief Function sets status of modyfication of the connection.
     ///
     /// \param _b Determines whether the connection was modyfied.
     /// \return No return value.
     void setChanged(bool _b);

     /// \brief Function sets location parameters of the connection.
     ///
     /// \return No return value.
     void Reposition(void);

     /// \brief Function repaints the connection on the screen.
     ///
     /// \return No return value.
     void Repaint(void);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Static function that returns the prefix of the id.
     ///
     /// \return the prefix of the id as string.
     static QString idPrefix(void);
};
// -----------------------------------------------------------------------------
#endif 
