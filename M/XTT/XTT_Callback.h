/**
 * \file     XTT_Callback.h
 * \author     Lukasz Finster, Sebastian Witowski
 * \date      25.04.2007
 * \version     1.15
 * \brief     This file contains class definition that is container for the attributes.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_CALLBACK_H
#define XTT_CALLBACK_H
//---------------------------------------------------------------------------

#include <QString>
#include <QXmlStreamWriter>
//---------------------------------------------------------------------------

/**
* \class       XTT_Callback
* \author      Lukasz Finster, Sebastian Witowski
* \date        18.06.2011
* \brief       Class definition of callback.
*/

class XTT_Callback
{
public:
     /// \brief Constructor XTT_Callback class.
     XTT_Callback();

     /// \brief Function sets ID of callback.
     ///
     /// \param newID Id of callback
     /// \return No return value
     void setID(QString newID);

     /// \brief Function sets name of callback.
     ///
     /// \param newName Name of callback
     /// \return No return value
     void setName(QString newName);

     /// \brief Function sets type of callback.
     ///
     /// \param newType type of callback
     /// \return No return value
     void setType(QString newType);

     /// \brief Function sets attribute type of callback.
     ///
     /// \param newAtt attribute type of callback
     /// \return No return value
     void setAtt(QString newAtt);

     /// \brief Function sets definition of callback.
     ///
     /// \param newDef deffinition of callback
     /// \return No return value
     void setDef(QString newDef);

     /// \brief Function set the information if callback will be saved in library.
     ///
     /// \param save
     /// \return No return value.
     void setSave(bool save);

     /// \brief Function set information if callback is used in model.
     ///
     /// \param clbu
     /// \return No return value
     void setUsage(bool clbu);

     /// \brief Function writes definition od callback is XML format.
     ///
     /// \param xmlWriter Pointer to XMLStreamWriter object
     /// \return No return value
     void toXML(QXmlStreamWriter* xmlWriter);

     /// \brief Function returns ID of callback.
     ///
     /// \return Return QString type value.
     QString getID();

     /// \brief Function returns name of callback.
     ///
     /// \return Return QString type value.
     QString getName();

     /// \brief Function returns type of callback.
     ///
     /// \return Return QString type value.
     QString getType();

     /// \brief Function returns attribute type of callback.
     ///
     /// \return Return QString type value.
     QString getAtt();

     /// \brief Function returns definition of callback.
     ///
     /// \return Return QString type value.
     QString getDef();

     /// \brief Function that returns the definition of callback written in prolog
     ///
     /// \return The prolog code that represents this callback as string.
     QString toProlog();

     /// \brief Function get information if callback is about to be saved tp Library.
     ///
     /// \return Information as bool.
     bool getSave();

     /// \brief Function returns the information if the callback is used in model.
     ///
     /// \return Information as bool
     bool getUsage();

     /// \brief Function returns information about callback, to show it in Attribute Editor Window.
     ///
     /// \return Information as QString.
     QString callbackInfo(bool __html);

private:
     bool libsave;                        ///< if save to library
     bool used;                           ///< if used in model
     QString ID;                          ///< ID of Callback
     QString name;                        ///< name of callback
     QString type;                        ///< type of callback
     QString att;                         ///< attribute type of callback
     QString def;                         ///< definition of callback
};

#endif
