/*
 *     $Id: XTT_States.cpp,v 1.2.4.2 2011-06-06 15:33:33 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>

#include <QKeyEvent>
#include <QStringList>
#include <QString>
#include <QList>
#include <QDomDocument>

#include "../../namespaces/ns_hqed.h"

#include "../hSet.h"

#include "XTT_Attribute.h"
#include "XTT_State.h"
#include "XTT_Statesgroup.h"

#include "XTT_States.h"
// -----------------------------------------------------------------------------

XTT_States* States;
XTT_States* OutputStates;
// -----------------------------------------------------------------------------

// #brief Constructor of XTT_States class.
XTT_States::XTT_States(void)
{
}
// -----------------------------------------------------------------------------

// #brief Constructor of XTT_States class.
XTT_States::~XTT_States(void)
{
    flush();
}
// -----------------------------------------------------------------------------

// #brief Function that removes all the object from the container
//
// #return no values return
void XTT_States::flush(void)
{
    iterator i;
    for(i=begin();i!=end();i++)
        delete i.value();

    QList<QString> ks = keys();
    for(int i=0;i<ks.size();i++)
        take(ks.at(i));
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the object has been changed
//
// #return true if the object has been changed, otherwise false
bool XTT_States::changed(void)
{
    iterator i;
    for(i=begin();i!=end();i++)
        if(i.value()->changed())
            return true;

    return false;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the status of changed paramteter
//
// #param __c - the new status of changed parameter
// #return no values return
void XTT_States::setChanged(bool __c)
{
    iterator i;
    for(i=begin();i!=end();i++)
        i.value()->setChanged(__c);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of ids of the all states in the group
//
// #return the list of ids of the all states in the group
QStringList XTT_States::ids(void)
{
    QStringList result;
    iterator i;
    for(i=begin();i!=end();i++)
        result << i.value()->ids();

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of names of the all states
//
// #return the list of names of the all states
QStringList XTT_States::names(void)
{
    QStringList result;
    iterator i;
    for(i=begin();i!=end();i++)
    {
        QStringList tmp = i.value()->names();
        for(int n=0;n<tmp.size();++n)
            result << XTT_Statesgroup::implodeName(QStringList() << i.key() << tmp.at(n));
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of states
//
// #return the list of states
QList<XTT_State*>* XTT_States::states(void)
{
    QList<XTT_Statesgroup*> groups = values();
    QList<XTT_State*>* states = new QList<XTT_State*>;

    for(int i=0;i<groups.size();++i)
        (*states) << groups.at(i)->states()->values();

    return states;
}
// -----------------------------------------------------------------------------

// #brief Function that generates the uniq id of the state
//
// #param __prefix - prefix of the id
// #return the uniq id of the state as string
QString XTT_States::findUniqId(QString __prefix)
{
    QStringList stateids = ids();
    QString basename = __prefix;

    if(__prefix.isEmpty())
        basename = XTT_State::stateIdPrefix();

    int index = 0;
    QString candidate;
    do
    {
        candidate = basename + QString::number(index++);
    }
    while(stateids.contains(candidate));

    return candidate;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_States::updateTypePointer(hType* __old, hType* __new)
{
    bool res = true;

    iterator i;
    for(i=begin();i!=end();i++)
        res = i.value()->updateTypePointer(__old, __new) && res;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_States::eventMessage(QString* __errmsgs, int __event, ...)
{ 
    bool res = true;
    va_list argsptr;

    if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        iterator i;
        for(i=begin();i!=end();i++)
            res = i.value()->eventMessage(__errmsgs, __event, attr) && res;

        return res;
    }

    if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
    {
        va_start(argsptr, __event);
        XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
        va_end(argsptr);

        iterator i;
        for(i=begin();i!=end();i++)
            res = i.value()->eventMessage(__errmsgs, __event, attr) && res;

        return res;
    }

    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that arrangess the element on the scene
//
// #return no values return
void XTT_States::groupsReposition(void)
{
    int groupscount = 0;

    qreal maxwidth = 0.;               // maximum width of the state table
    qreal vmargin = 40.;               // vertical margin between tables
    qreal hmargin = 70.;               // horizontal margin (between initial tables and final table)
    qreal sumheight = 0.;              // the sum of the initial tables heights

    // shape of final table
    QPointF finaltablepos(outputGroup()->shape().topLeft());
    qreal finaltableheight = outputGroup()->shape().height();

    qreal startylocalization = 0.;     // y coordinate of the state table

    // Searchning for max width and sum of heights
    groupscount = size();
    iterator i;

    for(i=begin();i!=end();i++)
    {
        QRectF shape = i.value()->shape();

        if(maxwidth < shape.width())
            maxwidth = shape.width();
        sumheight += shape.height();
    }

    qreal wholearea = sumheight + (vmargin * ((qreal)groupscount-1.));
    qreal halfwholearea = wholearea/2.;
    qreal halffinaltableheight = finaltableheight/2.;

    startylocalization = finaltablepos.y() + halffinaltableheight - halfwholearea;
    for(i=begin();i!=end();i++)
    {
        QRectF shape = i.value()->shape();
        qreal xcoord = finaltablepos.x() - shape.width() - hmargin;
        qreal ycoord = startylocalization;
        startylocalization = startylocalization + shape.height() + vmargin;

        i.value()->setLocalization(QPointF(xcoord, ycoord));
    }
    //refreshStatesUI();
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes the UI of the states
//
// #return no values return
void XTT_States::refreshStatesUI(void)
{
    QList<XTT_Statesgroup*> groups = values();
    for(int i=0;i<groups.size();++i)
        groups.at(i)->refreshUI();
}
// -----------------------------------------------------------------------------

// #brief Function that selects the states group
//
// #brief __fullname - full name of the state group
// #brief __select - the status of selection
// #return no values return
void XTT_States::selectStatesgroup(QString __fullname, bool __select)
{
    QStringList sepname = XTT_Statesgroup::explodeName(__fullname);
    QString groupname = sepname.at(0);
    if(!contains(groupname))
        return;

    (*this)[groupname]->selectStatesgroup(__select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the state
//
// #brief __fullname - full name of the state
// #brief __select - the status of selection
// #return no values return
void XTT_States::selectState(QString __fullname, bool __select)
{
    QStringList sepname = XTT_Statesgroup::explodeName(__fullname);
    if(sepname.size() < 2)
        return;

    QString groupname = sepname.at(0);
    QString statename = sepname.at(1);
    if(!contains(groupname))
        return;

    (*this)[groupname]->selectState(statename, __select);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the cell in the state table
//
// #param  __fullname - fullname of the stategroup/state
// #param  __attid - id of the attribute
// #param  __selected - indicates the state of selection
// #return No return value.
void XTT_States::selectCell(QString __fullname, QString __attid, bool __selected)
{
    QStringList sepname = XTT_Statesgroup::explodeName(__fullname);
    if(sepname.size() < 2)
        return;

    QString groupname = sepname.at(0);
    QString statename = sepname.at(1);
    if(!contains(groupname))
        return;

    (*this)[groupname]->selectCell(statename, __attid, __selected);
}
// -----------------------------------------------------------------------------

// #brief Function that creates states compilation in the output states list
//
// #return no values return
void XTT_States::statesCompile(void)
{
    QList<XTT_State*>* sts = states();

    for(int s=0;s<sts->size();++s)
        XTT_States::addNewOutputState(sts->at(s));

    delete sts;

    // refreshing GUI
    //OutputStates->refreshStatesUI();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the HMR representation of the states
//
// #return the HMR representation of the states
QString XTT_States::toProlog(void)
{
    QString result;
    iterator i;
    for(i=begin();i!=end();i++)
        result += i.value()->toProlog(i.key());
    result = result.replace("\n\n", "\n");

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the row object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_States::statesVerification(QString& __error_message)
{
    bool res = true;

    // searching for unallocated memoroy
    iterator i = begin();
    for(;i!=end();i++)
        if(i.value() == NULL)
        {
            QString _Error = "Unallocated memory for state group \'" + i.key() + "\'.";
            __error_message += hqed::createErrorString("", "", _Error, 0, 2);
            res = res && false;
        }

    // searching for groups with the same names
    QList<QString> ks = keys();
    for(int i=0;i<ks.size();++i)
        if(count(ks.at(i)) > 1)
        {
            QString _Error = "There are exist two or more groups of states with the same name \'" + ks.at(i) + "\'";
            __error_message += hqed::createErrorString(ks.at(i), "", _Error, 0, 6);
            res = res && false;
        }


    for(i=begin();i!=end();i++)
        res = i.value()->statesgroupVerification(__error_message) && res;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the output states group
//
// #return the name of the output states group
QString XTT_States::outputStatesGroupName(void)
{
    return "output";
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the group of output states
//
// #return the pointer to the group of output states
XTT_Statesgroup* XTT_States::outputGroup(void)
{
    QString outputGroupName = outputStatesGroupName();     // The name of the output group

    // Checking if the container is not empty
    if(OutputStates->empty())
        (*OutputStates)[outputGroupName] = new XTT_Statesgroup(OutputStates, 1);

    return (*OutputStates)[outputGroupName];
}
// -----------------------------------------------------------------------------

// #brief Function that adds new output state
//
// #param __label - label of the state
// #param __state - pointer to the state
// #return no values return
void XTT_States::addNewOutputState(XTT_State* __state)
{
    QString outputGroupName = outputStatesGroupName();     // The name of the output group
    QString stateFullName = __state->fullname();
    XTT_Statesgroup* stsgrp = NULL;

    // Checking if the container is not empty
    if(OutputStates->empty())
        (*OutputStates)[outputGroupName] = new XTT_Statesgroup(OutputStates, 1);
    stsgrp = (*OutputStates)[outputGroupName];

    // Adding new state if it does not exists
    bool cnts = stsgrp->contains(stateFullName);

    if(!cnts)
        (*stsgrp)[stateFullName] = __state;
    if(cnts)
    {
        delete (*stsgrp)[stateFullName];
        (*stsgrp)[stateFullName] = __state;
    }

    // refreshing GUI
    stsgrp->refreshUI();
}
// -----------------------------------------------------------------------------

// #brief Function that removes the output state
//
// #param __state - pointer to the removed state
// #return no values return
void XTT_States::removeOutputState(XTT_State* __state)
{
    if(__state == NULL)
        return;
    removeOutputState(__state->fullname());
}
// -----------------------------------------------------------------------------

// #brief Function that removes the output state
//
// #param __statename - the name of the state to remove
// #return no values return
void XTT_States::removeOutputState(QString __statename)
{
    QString outputGroupName = outputStatesGroupName();     // The name of the output group
    QString stateFullName = __statename;
    XTT_Statesgroup* stsgrp = NULL;

    // Checking if the container is not empty
    if(!OutputStates->contains(outputGroupName))
        return;
    stsgrp = (*OutputStates)[outputGroupName];

    // Removing the state if it exists
    bool cnts = stsgrp->contains(stateFullName);
    if(cnts)
        stsgrp->take(stateFullName);

    // refreshing GUI
    stsgrp->refreshUI();
}
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_States::in_HML_2_0(QDomElement& __root, QString& __msg)
{
    int result = 0;
    QString elementName = "states";
    QString childElementName = "state";

    QDomElement DOMstates = __root.firstChildElement(elementName);

    if(DOMstates.isNull())
        return result;

    QDomElement DOMstate = DOMstates.firstChildElement(childElementName);
    while(!DOMstate.isNull())
    {
        // Reading attribute data
        QString stsid = DOMstate.attribute("id", "").simplified();
        QString stsname = DOMstate.attribute("name", "").simplified();

        if(stsid == "")               // Checking if the reference is not empty
        {
            __msg += "\n" + hqed::createErrorString("", "", "Undefined state id. The state will not be read." + hqed::createDOMlocalization(DOMstate), 0, 1);
            DOMstate = DOMstate.nextSiblingElement(childElementName);
            continue;
        }
        if(stsname == "")               // Checking if the reference is not empty
        {
            __msg += "\n" + hqed::createErrorString("", "", "Undefined state name. The state will not be read." + hqed::createDOMlocalization(DOMstate), 0, 1);
            DOMstate = DOMstate.nextSiblingElement(childElementName);
            continue;
        }

        // Checking the name of the state
        QStringList nameparts = XTT_Statesgroup::explodeName(stsname);
        QString basename = nameparts.at(0);
        QString postfix = nameparts.at(1);

        // Checking if the group named basename already exists
        XTT_Statesgroup* destinationGroup;
        if(contains(basename))
            destinationGroup = (*this)[basename];
        else
        {
            destinationGroup = new XTT_Statesgroup(this);
            (*this)[basename] = destinationGroup;
        }

        // Reading state
        result += destinationGroup->in_HML_2_0(DOMstate, postfix, stsid, __msg);

        // Got to new state element
        DOMstate = DOMstate.nextSiblingElement(childElementName);
    }

    statesCompile();
    groupsReposition();

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_States::out_HML_2_0(int __mode)
{
    QStringList* res = new QStringList;
    res->clear();

    iterator i;
    for(i=begin();i!=end();i++)
    {
        *res << "<states>";
        QStringList* tmp = i.value()->out_HML_2_0(i.key(), __mode);
        for(int j=0;j<tmp->size();++j)
            *res << "\t" + tmp->at(j);
        delete tmp;
        *res << "</states>";
    }

    return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void XTT_States::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
    _xmldoc->writeStartElement("states");

    iterator i;
    for(i=begin();i!=end();i++)
        i.value()->out_HML_2_0(_xmldoc, i.key(), __mode);
    _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------
