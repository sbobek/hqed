/*
 *        $Id: XTT_Connections.cpp,v 1.34 2010-01-08 19:47:40 kinio Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
 
#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"

#include "../../V/XTT/GConnection.h"
#include "../../V/XTT/GWire.h"  

#include "XTT_Table.h"
#include "XTT_TableList.h"
#include "XTT_Connections.h"
//---------------------------------------------------------------------------

// #brief Constructor XTT_Connections class.
XTT_Connections::XTT_Connections(void)
{
     fConnID = idPrefix();
     fConnCut = false;
     fConnLabel = "";
     fConnDesc = "";
     fSrcTablID = "";
     fDesTablID = "";
     fSrcRowID = "";
     fDesRowID = "";
     fChanged = false;
     fGConn = NULL;
}
//---------------------------------------------------------------------------

// #brief Constructor XTT_Connections class.
//
// #param _id Connection id.
XTT_Connections::XTT_Connections(unsigned int _id)
{
     fConnID = idPrefix() + QString::number(_id);
     fConnCut = false;
     fConnLabel = "";
     fConnDesc = "";
     fSrcTablID = "";
     fDesTablID = "";
     fSrcRowID = "";
     fDesRowID = "";
     fChanged = false;
     fGConn = NULL;
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_Connections class.
XTT_Connections::~XTT_Connections(void)
{
     delete fGConn;
     
     // Po usunieciu polaczenia nalezy ponownie zbadac polaczenia tabelami
     QString st = fSrcTablID;
     QString dt = fDesTablID;
     
     fSrcTablID = "";
     fDesTablID = "";
     fSrcRowID = "";
     fDesRowID = "";
     
     TableList->AnalyzeTableConnections(st);
     TableList->AnalyzeTableConnections(dt);
}
//---------------------------------------------------------------------------

// #brief Function sets status of modyfication of the connection.
//
// #param _b Determines whether the connection was modyfied.
// #return No return value.
void XTT_Connections::setChanged(bool _c)
{
     fChanged = _c;
}
//---------------------------------------------------------------------------

// #brief Function sets connection property.
//
// #param _cut Determines whether recursive of the connection should be active.
// #return No return value.
void XTT_Connections::setCut(bool _cut)
{
     fConnCut = _cut;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets label of the connection.
//
// #param _label Label of the connection.
// #return No return value.
void XTT_Connections::setLabel(QString _label)
{
     fConnLabel = _label;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets describtion of the connection.
//
// #param _label Describtion of the connection.
// #return No return value.
void XTT_Connections::setDescription(QString _desc)
{
     fConnDesc = _desc;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets source table id.
//
// #param _src Source table id.
// #return No return value.
void XTT_Connections::setSrcTable(QString _st)
{
     fSrcTablID = _st;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets destiny table id.
//
// #param _dst Destiny table id.
// #return No return value.
void XTT_Connections::setDesTable(QString _dt)
{
     fDesTablID = _dt;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets source row id.
//
// #param _src Source row id.
// #return No return value.
void XTT_Connections::setSrcRow(QString _sr)
{
     fSrcRowID = _sr;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function sets destiny row id.
//
// #param _dst Destiny row id.
// #return No return value.
void XTT_Connections::setDesRow(QString _dr)
{
     fDesRowID = _dr;
     fChanged = true;
     Repaint();
}
//---------------------------------------------------------------------------

// #brief Function repaints the connection on the screen.
//
// #return No return value.
void XTT_Connections::Repaint(void)
{
     if((!fSrcTablID.isEmpty()) && (!fDesTablID.isEmpty()) && (!fSrcRowID.isEmpty()) /*&& (!fDesRowID.isEmpty())*/)
     {
          if(fGConn == NULL)
               fGConn = new GConnection(xtt::modelScene(), this);
          fGConn->RePaint();
     }
}
//---------------------------------------------------------------------------

// #brief Function sets location parameters of the connection.
//
// #return No return value.
void XTT_Connections::Reposition(void)
{
     fGConn->setLocalizationParameters();
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_Connections::eventMessage(QString* /*__errmsgs*/, int /*__event*/, ...)
{
     /*
     va_list argsptr;
     va_start(argsptr, __event);
     va_arg(argsptr, typ);
     va_end(argsptr);
     */
     
     // bool res = true;
     
     /*if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
          return res;*/
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_Connections::idPrefix(void)
{
     return "con_";
}
// -----------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 1.0 format.
//
// #param root Reference to the section that concerns this attribute.
// #return True when data is ok, otherwise function returns false.
bool XTT_Connections::FromXTTML_2_0(QDomElement& root)
{
     if(root.tagName().toLower() != "xtt_connection")
          return false;

     QString sCut = root.attribute("cut", "#NAN#");
     QString sSTN = root.attribute("source_table_name", "#NAN#");
     QString sDTN = root.attribute("destination_table_name", "#NAN#");
     QString sSR = root.attribute("source_row", "#NAN#");
     QString sDR = root.attribute("destination_row", "#NAN#");
     
     // Usuwanie spacji z wczytanych wartosci
     for(;sCut.contains(" ");)
          sCut = sCut.remove(sCut.indexOf(" "), 1);
     for(;sSTN.contains(" ");)
          sSTN = sSTN.remove(sSTN.indexOf(" "), 1);
     for(;sDTN.contains(" ");)
          sDTN = sDTN.remove(sDTN.indexOf(" "), 1);
     for(;sSR.contains(" ");)
          sSR = sSR.remove(sSR.indexOf(" "), 1);
     for(;sDR.contains(" ");)
          sDR = sDR.remove(sDR.indexOf(" "), 1);
          
     // Sprawdzanie poprawnosci wczytanych wartosci parametrow
     
     bool bCut = false;          // Nie wymagany, default=false
     QString stName = "";     // Wymagany
     QString dtName = "";     // Wymagany
     int iSRow = -1;          // Wymagany
     int iDRow = -1;          // Wymagany
     
     // Sprawdzanie podania czy polaczenie jest odcinajace nawroty, jezeli jest nie podane to przyjmujemy false
     if((sCut != "") && (sCut != "#NAN#") && (sCut.toLower() != "true") && (sCut.toLower() != "false"))
          QMessageBox::information(NULL, "HQEd", "Incorrent connection parameter \"cut\" at line " + QString::number(root.lineNumber()) + ".\nParameter will be set as \"false\"", QMessageBox::Ok);
     bCut = (sCut.toLower() == "true");
     fConnCut = bCut;
          
     // Jezeli nie podano nazwy tabeli zrodlowej
     if((sSTN == "") || (sSTN == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing connection parameter \"source_table_name\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     // Natomiast jezeli podano to ustawianie identyfikatora
     int SrcTableIndex = TableList->IndexOfTitle(sSTN);
     if(SrcTableIndex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Undefined table name \"" + sSTN + "\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     if(SrcTableIndex > -1)
          fSrcTablID = TableList->Table(SrcTableIndex)->TableId();
          
          
          
     // Jezeli nie podano nazwy tabeli docelowej
     if((sDTN == "") || (sDTN == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing connection parameter \"destination_table_name\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     // Natomiast jezeli podano to ustawianie identyfikatora
     int DesTableIndex = TableList->IndexOfTitle(sDTN);
     if(DesTableIndex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Undefined table name \"" + sDTN + "\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     if(DesTableIndex > -1)
          fDesTablID = TableList->Table(DesTableIndex)->TableId();
     
     
     // Sprawdzanie czy podano index wiersza zrodlowego
     if((sSR == "") || (sSR == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing connection parameter \"source_row\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     // Proby konwersji jezeli podano
     if((sSR != "") && (sSR != "#NAN#"))
     {
          bool ok;
          iSRow = sSR.toInt(&ok);
          if(!ok)
          {
               QMessageBox::critical(NULL, "HQEd", sSR + " is not integer type.\nIncorrect connection parameter \"source_row\" type: \"" + sSR + "\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli konwersja sie udala, to robujemy uzyskac identyfikator zadanego wiersza
          if((iSRow >= (int)TableList->Table(SrcTableIndex)->RowCount()) || (iSRow < 0))
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect source row index value: " + QString::number(iSRow) + ". Table " + TableList->Table(SrcTableIndex)->Title() + " has only " + QString::number(TableList->Table(SrcTableIndex)->RowCount()) + " rows.\nError at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli jest ok to zapisujemy ID wiersza
          fSrcRowID = TableList->Table(SrcTableIndex)->Row(iSRow)->RowId();
     }
     
     
     // Sprawdzanie czy podano index wiersza docelowego
     if((sDR == "") || (sDR == "#NAN#"))
     {
          QMessageBox::critical(NULL, "HQEd", "Missing connection parameter \"destination_row\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
          return false;
     }
     // Proby konwersji jezeli podano
     if((sDR != "") && (sDR != "#NAN#"))
     {
          bool ok;
          iDRow = sDR.toInt(&ok);
          if(!ok)
          {
               QMessageBox::critical(NULL, "HQEd", sDR + " is not integer type.\nIncorrect connection parameter \"destination_row\" type: \"" + sDR + "\" at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli konwersja sie udala, to robujemy uzyskac identyfikator zadanego wiersza
          if((iDRow >= (int)TableList->Table(DesTableIndex)->RowCount()) || (iDRow < 0))
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect destination row index value: " + QString::number(iDRow) + ". Table " + TableList->Table(DesTableIndex)->Title() + " has only " + QString::number(TableList->Table(DesTableIndex)->RowCount()) + " rows.\nError at line " + QString::number(root.lineNumber()) + ".", QMessageBox::Ok);
               return false;
          }
          
          // Jezeli jest ok to zapisujemy ID wiersza
          fDesRowID = TableList->Table(DesTableIndex)->Row(iDRow)->RowId();
     }
     
     QString label = root.firstChildElement("label").text();     
     QString desc = root.firstChildElement("description").text();     
     if(!label.isEmpty())
          fConnLabel = label;
     if(!desc.isEmpty())
          fConnDesc = desc;

     Repaint();
     
     //QStringList* h = XTTML_1_0();
     //for(int i=0;i<h->size();i++)
     //QMessageBox::warning(NULL, "HQEd", h->at(i));

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_Connections::out_HML_2_0(int /*__mode*/)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(fDesTablID == "")
          return res;
          
     XTT_Table* dt = TableList->_IndexOfID(fDesTablID);
     if(dt == NULL)
          return res;
     int rowindex = dt->indexOfRowID(fDesRowID);

     *res << "<link>";
     *res << "\t<tabref ref=\"" + dt->TableId() + "\"/>";
     if(rowindex > -1)
          *res << "\t<rulref ref=\"" + dt->Row(rowindex)->RowId() + "\"/>";
     *res << "</link>";
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void XTT_Connections::out_HML_2_0(QXmlStreamWriter* _xmldoc, int /*__mode*/)
{
     if(fDesTablID == "")
          return;
          
     XTT_Table* dt = TableList->_IndexOfID(fDesTablID);
     if(dt == NULL)
          return;
     int rowindex = dt->indexOfRowID(fDesRowID);
     
     _xmldoc->writeStartElement("link");
          _xmldoc->writeEmptyElement("tabref");
          _xmldoc->writeAttribute("ref", dt->TableId());
          if(rowindex > -1)
          {
               _xmldoc->writeEmptyElement("rulref");
               _xmldoc->writeAttribute("ref", dt->Row(rowindex)->RowId());
          }
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

