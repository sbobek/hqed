/**
 * \file     XTT_Column.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      05.11.2009
 * \version     1.0
 * \brief     This file contains class definition that is XTT XTT_Column.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef XTT_COLUMNH
#define XTT_COLUMNH
// -----------------------------------------------------------------------------

#define XTT_COLUMN_SORT_MIN_VALUE    0
#define XTT_COLUMN_SORT_MAX_VALUE    1
#define XTT_COLUMN_SORT_POWER        2
#define XTT_COLUMN_SORT_ALPHABETICAL 3

#define XTT_COLUMN_SORT_ASCENDING    0
#define XTT_COLUMN_SORT_DESCENDING   1
// -----------------------------------------------------------------------------

class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class      XTT_Column
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       05.11.2009
* \brief      Class definition that is XTT table.
*/
// -----------------------------------------------------------------------------

class XTT_Column
{
private:

     int _sortCriterion;                ///< Criterion of sorting
     int _width;                        ///< The width of the column
     unsigned int _abstractColID;       ///< The abstract column ID
     XTT_Attribute* _contextAtribute;   ///< Pointer to the attribute that is related with column
     
public:

     /// \brief Default contructor of the XTT_Column class
     XTT_Column(void);
     
     /// \brief Function that returns the sorting criterion
     ///
     /// \return sorting criterion
     int sortCriterion(void);
     
     /// \brief Function that returns the width of the column
     ///
     /// \return width of the column
     int width(void);
     
     /// \brief Function that returns the abstract id assigned to the column
     ///
     /// \return abstract id assigned to the column
     unsigned int abstractId(void);
     
     /// \brief Function that returns the pointer to the attribute realted with the column
     ///
     /// \return the pointer to the attribute realted with the column
     XTT_Attribute* attribute(void);
     
     /// \brief Function that sets the sorting criterion
     ///
     /// \param __criterion - identifier of criterion
     /// \return no values return
     void setSortCriterion(int __criterion);
     
     /// \brief Function that sets the width of the column
     ///
     /// \param __width - a new width of the column
     /// \return no values return
     void setWidth(int __width);
     
     /// \brief Function that sets the abstract id assigned to the column
     ///
     /// \param __aid - a new abstract id assigned to the column
     /// \return no values return
     void setAbstractId(unsigned int __aid);
     
     /// \brief Function that sets the pointer to the attribute realted with the column
     ///
     /// \param __xttattribute - the pointer to the new attribute realted with the column
     /// \return no values return
     void setAttribute(XTT_Attribute* __xttattribute);
};
// -----------------------------------------------------------------------------
#endif 
