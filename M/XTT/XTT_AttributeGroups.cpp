/*
 *        $Id: XTT_AttributeGroups.cpp,v 1.39.4.1.2.3 2011-06-23 09:22:22 hqedtypes Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QString>
#include <QStringList>

#include <stdarg.h>

#include "../../C/MainWin_ui.h"
#include "../../C/XTT/widgets/ExecuteMessagesListWidget.h"
#include "../../namespaces/ns_hqed.h"

#include "../hSet.h"
#include "../hType.h"
#include "../hValue.h"
#include "../hDomain.h"
#include "../hSetItem.h"
#include "../hTypeList.h"
#include "../hMultipleValue.h"

#include "../ARD/ARD_Attribute.h"
#include "../ARD/ARD_AttributeList.h"
#include "XTT_Table.h"
#include "XTT_State.h"
#include "XTT_States.h"
#include "XTT_TableList.h"
#include "XTT_Attribute.h"
#include "XTT_AttributeGroups.h"
// ---------------------------------------------------------------------------

XTT_AttributeGroups* AttributeGroups;
// ---------------------------------------------------------------------------

// #brief Constructor XTT_AttributeGroups class.
XTT_AttributeGroups::XTT_AttributeGroups(void)
{
     fGroups = NULL;
     fCount = 0;
     fGId = 0;
     gAttrId = 0;
     fChanged = false;
     CreateGlobalVariablesGroup();
}
// ---------------------------------------------------------------------------

// #brief Destructor XTT_AttributeGroups class.
XTT_AttributeGroups::~XTT_AttributeGroups(void)
{
     if(fCount)
     {
          for(unsigned int i=0;i<fCount;i++)
               delete fGroups[i];
          delete [] fGroups;
     }
}
// ---------------------------------------------------------------------------

// #brief Function gets next group id.
//
// #return Next group id.
QString XTT_AttributeGroups::GId(void)
{
     return XTT_AttributeList::idPrefix() + QString::number(fGId);
}
// ---------------------------------------------------------------------------

// #brief Function determines whether any group of attributes table was changed.
//
// #return Decision whether any group of attributes table was changed.
bool XTT_AttributeGroups::Changed(void)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->Changed())
               return true;
               
     return fChanged;
}
// ---------------------------------------------------------------------------

// Ustawia wartosc okreslajaca czy obiekty byly modyfikowane
// #brief Function sets whether groups of attributes table were changed.
//
// #param _c Decision whether groups of attributes table were changed(True) or not(False).
// #return No return value.
void XTT_AttributeGroups::setChanged(bool _c)
{     
     for(unsigned int i=0;i<fCount;i++)
          fGroups[i]->setChanged(_c);
               
     fChanged = _c;
}
// ---------------------------------------------------------------------------

// #brief Function clears groups of attributes table.
//
// #return No return value.
void XTT_AttributeGroups::Flush(void)
{
     if(fCount == 0)
          return;

     for(unsigned int i=0;i<fCount;i++)
          delete fGroups[i];
     delete [] fGroups;

     fGroups = NULL;
     fGId = 0;
     gAttrId = 0;
     fCount = 0;
     fChanged = true;

     // Te grupy musza byc zawsze
     CreateGlobalVariablesGroup();
}
//---------------------------------------------------------------------------

// #brief Function creates group of global variables.
//
// #return No return value.
void XTT_AttributeGroups::CreateGlobalVariablesGroup(void)
{
     if(fCount > 0)
          return;

     Add();
     fGroups[0]->setName("Global");
     fGroups[0]->setDescription("This group contains a global variables, which not belong to groups.");

     // Grupa na atrybuty tymczasowe
     Add();
     fGroups[1]->setName("tmp");
     fGroups[1]->setDescription("This group contains temporary attributes.");
     
     // Grupa na atrybuty typu action
     Add();
     fGroups[2]->setName("action");
     fGroups[2]->setDescription("This group contains action attributes.");
}
// ---------------------------------------------------------------------------

// #brief Function creates temporary argument for temporary results of calculations.
//
// #param type Type of temporary argument.
// #return Index of created argument.
int XTT_AttributeGroups::CreateNewTmpArgument(hType* type)
{
     QString base_name = "tmp_";

     int aindex = 0;     // Index argumentu/nazwy

     // Wyszukiwanie indexu nawy
     for(;fGroups[1]->IndexOfName(base_name+QString::number(aindex))>-1;)
          aindex++;

     // Kiedy znajdziemy to tworzymy nowy atrybut o podanym typie z automatycznie nadawana nazwa
     fGroups[1]->Add();
     fGroups[1]->Item(fGroups[1]->Count()-1)->setType(type);
     fGroups[1]->Item(fGroups[1]->Count()-1)->setName(base_name+QString::number(aindex));
     fGroups[1]->Item(fGroups[1]->Count()-1)->setAcronym(base_name+QString::number(aindex));
     
     fChanged = true;

     // Zwracamy index nowoutworzonego atrybutu
     return fGroups[1]->Count()-1;
}
// ---------------------------------------------------------------------------

// #brief Function creates action argument.
//
// #return Pointer to the created attribute
XTT_Attribute* XTT_AttributeGroups::CreateNewActionArgument(void)
{
     QString base_name = "*";

     // Kiedy znajdziemy to tworzymy nowy atrybut o podanym typie z automatycznie nadawana nazwa
     fGroups[2]->Add();
     fGroups[2]->Item(fGroups[2]->Count()-1)->setType(typeList->at(typeList->indexOfName(hpTypesInfo.ptn[hpTypesInfo.iopti(SYMBOLIC_BASED)])));
     fGroups[2]->Item(fGroups[2]->Count()-1)->setMultiplicity(false);
     fGroups[2]->Item(fGroups[2]->Count()-1)->setName(base_name);
     fGroups[2]->Item(fGroups[2]->Count()-1)->setAcronym(base_name);
     
     fChanged = true;

     // Zwracamy index nowoutworzonego atrybutu
     return fGroups[2]->Item(fGroups[2]->Count()-1);
}
// ---------------------------------------------------------------------------

// #brief Function gets pointer to chosen group.
//
// #param _name Name of chosen group.
// #return Pointer to the list of chosen group. If name is not correct function returns NULL.
XTT_AttributeList* XTT_AttributeGroups::Group(QString _name)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->Name() == _name)
               return fGroups[i];

     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Function gets pointer to chosen group.
//
// #param _index Index of chosen group.
// #return Pointer to chosen group. If index is not correct function returns NULL.
XTT_AttributeList* XTT_AttributeGroups::Group(int _index)
{
     if((_index < 0) || (_index >= (int)fCount))
          return NULL;

     return fGroups[_index];
}
// ---------------------------------------------------------------------------

// #brief Function gets list of group names.
//
// #param _parent Name of group parent.
// #return List of chosen group. If index is not correct function returns NULL.
QStringList XTT_AttributeGroups::GroupList(QString _parent)
{
     // Lista zwracana
     QStringList res;
     res.clear();

     // Wyszukiwanie grup
     for(unsigned int i=0;i<fCount;i++)
     {
          if((_parent == "") && (fGroups[i]->Level() == 0))
               res << fGroups[i]->Name();
          if((fGroups[i]->Level() > 0) && (IndexOfID(fGroups[i]->ParentID()) > 0))
                    res << _IndexOfID(fGroups[i]->ParentID())->Name();
     }

     // Zwrocenie listy nazw
     return res;
}
// ---------------------------------------------------------------------------

// #brief Function sets parent for the group and other settings.
//
// #param _group Pointer to the group.
// #param _newParent Pointer to the new parent.
// #return No return value.
void XTT_AttributeGroups::setGroupParent(XTT_AttributeList* _group, XTT_AttributeList* _newParent)
{
     if((_group == NULL) || (_newParent == NULL))
          return;

     _group->setParentId(_newParent->ID());       // Ustawienie wskaznika
     _group->setLevel(_newParent->Level()+1);     // Usawienie levelu
     
     fChanged = true;
}
// ---------------------------------------------------------------------------

// #brief Function sets parent for the group and other settings.
//
// #param _group Pointer to the group.
// #param _newParentId Pointer to the new parent id.
// #return No return value.
void XTT_AttributeGroups::setGroupParent(XTT_AttributeList* _group, QString _newParentId)
{
     if((_group == NULL) || (_newParentId <= 0) || (IndexOfID(_newParentId) == -1))
          return;

     // Sprawdzanie czy rodzic i potomek to nie ta sama grupa
     int index = IndexOfID(_newParentId);
     if(_group->ID() == fGroups[index]->ID())
     {
          const QString content = "The group can't be parent for themselves.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy podany nowy rodzic nie jest potomkiem aktualnej grupy
     while(fGroups[index]->Level() > 0)
     {
          // Sprawdzanie czy
          if(_group->ID() == fGroups[index]->ParentID())
          {
               const QString content = "You can't set child of this group as his parent.";
               QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
               return;
          }
          index = IndexOfID(fGroups[index]->ParentID());
     }

     _group->setParentId(_newParentId);                               // Ustawienie wskaznika
     RefreshLevelField(_group, _IndexOfID(_newParentId)->Level()+1);  // Usawienie levelu na najwyzszy i usaktalnienie pozostalych
     
     fChanged = true;
}
// ---------------------------------------------------------------------------

// #brief Function sets the group as root.
//
// #param _group Pointer to the group.
// #return No return value.
void XTT_AttributeGroups::setGroupAsRoot(XTT_AttributeList* _group)
{
     if(_group == NULL)
          return;

     _group->setParentId(0);                      // Ustawienie wskaznika na NULL - ze nie ma rodzicow
     RefreshLevelField(_group, 0);                // Usawienie levelu na najwyzszy i usaktalnienie pozostalych
     
     fChanged = true;
}
// ---------------------------------------------------------------------------

// #brief Function refreshes level field after changing level of nesting.
//
// #param _start Pointer to the parent group from which level should be numbered.
// #param _new_level Level of nesting.
// #return No return value.
void XTT_AttributeGroups::RefreshLevelField(XTT_AttributeList* _start, unsigned int _new_level)
{
     // Kiedy wskaznik jest jakis taki niepoprawny :)
     if(_start == NULL)
          return;

     // Odswierzanie watosci
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->ParentID() == _start->ID())
               RefreshLevelField(fGroups[i], _new_level+1);

     // Odsiwiezenie bierzacej grupy
     _start->setLevel(_new_level);
     
     fChanged = true;
}
// ---------------------------------------------------------------------------

// #brief Function adds new group to the group of attributes table.
//
// #return No return value.
void XTT_AttributeGroups::Add(void)
{
     // Utworzenie nowej wiekszej tablicy wskaznikow
     XTT_AttributeList** tmp = new XTT_AttributeList*[fCount+1];

     // Kopiowanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fGroups[i];

     // Usuniecie starej tablicy wskaznikow
     if(fCount)
          delete [] fGroups;

     // Zapisanie lokalizacji
     fGroups = tmp;

     // Utworzenie nowego elementu
     fGroups[fCount] = new XTT_AttributeList(++fGId);

     // Powiekszenie rozmiaru
     fCount++;
     
     fChanged = true;
}
// ---------------------------------------------------------------------------

// #brief Function deletes group with its children.
//
// #param _name Name of the group.
// #return True when succeeds, false when name is incorrect.
bool XTT_AttributeGroups::Delete(QString _name)
{
     // Wyszukiwanie index
     int index = IndexOfName(_name);

     // Jezeli nie ma takiej grupy o podanej nazwie
     if(index == -1)
          return false;

     // Lista nazw grup ktore trzeba usunac
     QStringList names;
     names.clear();
     names << _name;

     // Wyznazanie nazw do usuniecia
     RemoveChildrens(fGroups[index]->ID(), names);

     // Usuwanie wszystkich grup z listy

     // Utworzenie nowej mniejszej tablicy wskaznikow
     XTT_AttributeList** tmp = new XTT_AttributeList*[fCount-names.size()];

     // Kopiowanie wskaznkow do nowej tablicy pomijajac wskazniki usuwane
     unsigned int tmp_i = 0;
     for(unsigned int i=0;i<fCount;i++)
          if(!names.contains(fGroups[i]->Name()))
               tmp[tmp_i++] = fGroups[i];

     // Kasowanie grup
     for(int i=0;i<names.size();i++)
     {
          int v = IndexOfName(names.at(i));
          if(v > -1)
               delete fGroups[v];
     }

     // Kasowanie starej tablicy wskaznikow
     if(fCount)
          delete [] fGroups;

     // Zapisanie nowej lokalizaji
     fGroups = tmp;

     // Pomniejszenie rozmiaru pojemnika
     fCount -= names.size();
     
     fChanged = true;

     // UFF
     return true;
}
// ---------------------------------------------------------------------------

// #brief Function removes all children for the group.
//
// #param _parent Pointer to parent of the group. 
// #param names Reference to the list of names.
// #return No return value.
void XTT_AttributeGroups::RemoveChildrens(QString _parent, QStringList& names)
{
     // Wyszkiwanie grup majacych za potomka dana grupe
     for(unsigned int i=0;i<fCount;i++)
     {
          if(fGroups[i]->ParentID() == _parent)
          {
               names << fGroups[i]->Name();
               RemoveChildrens(fGroups[i]->ID(), names);
          }
     }
}
// ---------------------------------------------------------------------------

// #brief Function that returns the pointer to the attribute that has the given ID
//
// #param __attId - attribute id
// #return the pointer to the attribute that has the given ID
XTT_Attribute* XTT_AttributeGroups::indexOfAttId(QString __attId)
{
     for(unsigned int i=0;i<fCount;i++)
     {
          int agi = fGroups[i]->IndexOfID(__attId);
          if(agi > -1)
               return fGroups[i]->Item(agi);
     }

     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Function that removes attribute with specified ID
//
// #param __attId - attribute id
// #return the result - success or failure
bool XTT_AttributeGroups::removeAtt(QString __attId)
{
     for(unsigned int i = 0; i < fCount; i++)
     {
         return fGroups[i]->Delete(fGroups[i]->IndexOfID(__attId), true);
     }

     return false;
}
// ---------------------------------------------------------------------------

// #brief Function gets index of the group.
//
// #param _name Name of the group.
// #return Index of the group.
int XTT_AttributeGroups::IndexOfName(QString _name)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->Name() == _name)
               return (int)i;

     return -1;
}
// ---------------------------------------------------------------------------

// #brief Function gets pointer to the group.
//
// #param _name Name of the group.
// #return Pointer of the group. If _name is not correct function returns NULL.
XTT_AttributeList* XTT_AttributeGroups::_IndexOfName(QString _name)
{
     int index = IndexOfName(_name);
     if(index > -1)
          return fGroups[index];

     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Function gets index of the group.
//
// #param _id Id of the group.
// #return Index of the group.
int XTT_AttributeGroups::IndexOfID(QString _id)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->ID() == _id)
               return (int)i;

     return -1;
}
// ---------------------------------------------------------------------------

// #brief Function gets pointer to the group.
//
// #param _id Id of the group.
// #return Pointer of the group. If _name is not correct function returns NULL.
XTT_AttributeList* XTT_AttributeGroups::_IndexOfID(QString _id)
{
     int index = IndexOfID(_id);
     if(index > -1)
          return fGroups[index];

     return NULL;
}
// ---------------------------------------------------------------------------

// #brief Returns only those attrributes, with a given relation
//
// #param __relation - the value of the relation
// #return the list of only those attrributes, with a given relation
QList<XTT_Attribute*> XTT_AttributeGroups::classFilter(int __relation)
{
     QList<XTT_Attribute*> result;
     
     for(unsigned int i=0;i<fCount;i++)
     {
          if((i == 1) || (i == 2))
               continue;
          result << fGroups[i]->classFilter(__relation);
     }
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function gets decision whether group of attributes table is empty.
//
// #return True when group of attributes table is empty, otherwise false.
bool XTT_AttributeGroups::IsEmpty(void)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fGroups[i]->Count())
               return false;

     return true;
}
// ---------------------------------------------------------------------------

// #brief Function creates and gets tree of group names.
//
// #return Tree of group names.
QList<QTreeWidgetItem*> XTT_AttributeGroups::CreateTree(void)
{
     QList<QTreeWidgetItem*> items;

     for(unsigned int i=0;i<fCount;i++)
     {
          // Niewyswietlamy grupy tymczasowej
          if(fGroups[i]->Name() == "tmp")
               continue;
               
          // Niewyswietlamy grupy action
          if(fGroups[i]->Name() == "action")
               continue;

          if(fGroups[i]->Level() == 0)
          {
               items.append(new QTreeWidgetItem((QTreeWidget*)0, QStringList(fGroups[i]->Name())));
               AddChilds(items.last(), fGroups[i]->ID());
          }

          //items.last()->addChild(new QTreeWidgetItem(QStringList(QString("child_item: %1"))));
          //items.last()->child(0)->addChild(new QTreeWidgetItem(QStringList(QString("child_child_item1: %1"))));
          //items.last()->child(0)->addChild(new QTreeWidgetItem(QStringList(QString("child_child_item2: %1"))));
          //items.last()->child(0)->addChild(new QTreeWidgetItem(QStringList(QString("child_child_item3: %1"))));
     }

     return items;
}
// ---------------------------------------------------------------------------

// #brief Function creates children for the group.
//
// #param itm Pointer to the tree.
// #param _gId Group id which children will be created.
// #return No return value.
void XTT_AttributeGroups::AddChilds(QTreeWidgetItem* itm, QString _gId)
{
     for(unsigned int i=0;i<fCount;i++)
     {
          if(fGroups[i]->ParentID() == _gId)
          {
               itm->addChild(new QTreeWidgetItem(QStringList(fGroups[i]->Name())));
               AddChilds(itm->child(itm->childCount()-1), fGroups[i]->ID());
          }
     }
}
// ---------------------------------------------------------------------------

// #brief Function creates and gets path for the group.
//
// #param _id Group id.
// #return Path for the group.
QString XTT_AttributeGroups::_CreatePath(QString _id)
{
     QString res = "";                                                          // Zwracany rezultat

     QString curr_id = _id;                                                         // Bierzace id
     int index = IndexOfID(curr_id);                                            // Bierzacy ndex grupy

     // Jezeli grupa jest niepoprawna
     if(index == -1)
     {
          const QString content = "Application can't find requested group.";
          QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
          return res;
     }

     res = fGroups[index]->Name();                                              // Uaktualnienie rezultatu

     // Generowanie scieki "w gore"
     while((index > -1) && (index < (int)fCount) && (fGroups[index]->Level() > 0))
     {
          for(unsigned int i=0;i<fCount;i++)
               if(fGroups[index]->ParentID() == fGroups[i]->ID())
               {
                    index = i;
                    break;
               }
          res = fGroups[index]->Name() + "." + res;
     }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function creates and gets path for the group.
//
// #param _name Name of the group.
// #return Path for the group.
QString XTT_AttributeGroups::CreatePath(QString _name)
{
     QString res = "";

     int index = IndexOfName(_name);
     if(index != -1)
          res = _CreatePath(fGroups[index]->ID());

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function creates and gets path for the group.
//
// #param _index Index of the group.
// #return Path for the group.
QString XTT_AttributeGroups::CreatePath(int _index)
{
     QString res = "";

     if((_index > -1) && (_index < (int)fCount))
          res = _CreatePath(fGroups[_index]->ID());

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function checks whether path and attribute are correct.
//
// #param _path Path concluded with attribute.
// #return True when path is correct, otherwise false.
bool XTT_AttributeGroups::ExistsAttribute(QString _path)
{
     QStringList list = _path.split(".", QString::SkipEmptyParts);

     if(list.size() == 0)
          return false;

     // Usuwamy spacje na poczatku i koncu kazdego wiersza
     for(int i=0;i<list.size();i++)
     {
          QString line = list.at(i);
          for(;line[0] == ' ';)
               line = line.remove(0,1);
          for(;line[line.length()-1] == ' ';)
               line = line.remove(line.length()-1,1);
          list.replace(i, line);
     }

     // Gdy jest podany tylko atrybut, to sprawdzamy czy istnieje w Global
     if(list.size() == 1)
     {
          if((fGroups[0]->IndexOfName(list.at(0)) == -1) && (fGroups[0]->IndexOfAcronym(list.at(0)) == -1))
               return false;
          return true;
     }

     // Kiedy wieksza ilosc
     // Najpierw sprawdzamy czy podany atrybut istnieje w podanej grupie
     int index = IndexOfName(list.at(list.size()-2));
     if(index == -1)
          return false;
     if((fGroups[index]->IndexOfName(list.at(list.size()-1)) == -1) && (fGroups[index]->IndexOfAcronym(list.at(list.size()-1)) == -1))
          return false;
     
     // Sprawdzanie sciezki
     QString group_name;
     QString group_parent;
     int group_index;
     int group_parent_index;
     for(int i=list.size()-3;i>=0;i--)
     {
          group_name = list.at(i+1);
          group_parent = list.at(i);
          group_index = IndexOfName(group_name);
          group_parent_index = IndexOfName(group_parent);

          // Sprawdzanie istnienia grup
          if((group_index == -1) || (group_parent_index == -1))
               return false;

          // Sprawdzanie id rodzica
          if(fGroups[group_index]->ParentID() != fGroups[group_parent_index]->ID())
               return false;

     }

     // Sprawdzanie czy grupa na szczycie jest grupa glowna
     group_parent_index = IndexOfName(list.at(0));
     if(fGroups[group_parent_index]->Level() > 0)
          return false;
     return true;
}
// ---------------------------------------------------------------------------

// #brief Function finds and gets pointer to the attribute.
//
// #param _path Path to the attribute.
// #return Pointer to the attribute.
XTT_Attribute* XTT_AttributeGroups::Attribute(QString _path)
{
     if(!ExistsAttribute(_path))
          return NULL;

     // Dalej nie musimy robic przechwytywania wyjatkow, poniewaz powyzsza funckja
     // to za nas zalatwia :)

     QStringList list = _path.split(".", QString::SkipEmptyParts);

     // Usuwamy spacje na poczatku i koncu kazdego wiersza
     for(int i=0;i<list.size();i++)
     {
          QString line = list.at(i);
          for(;line[0] == ' ';)
               line = line.remove(0,1);
          for(;line[line.length()-1] == ' ';)
               line = line.remove(line.length()-1,1);
          list.replace(i, line);
     }

     // Gdy jest podany tylko atrybut, to zwracamy jego index w Global
     if(list.size() == 1)
     {
          int index1 = fGroups[0]->IndexOfName(list.at(0));
          int index2 = fGroups[0]->IndexOfAcronym(list.at(0));
          int index = index1;
          if(index1 == -1)
               index = index2;
          return fGroups[0]->Item(index);
     }

     // Kiedy wieksza ilosc
     // Najpierw obliczamy index jego grupy
     int gindex = IndexOfName(list.at(list.size()-2));
     int index1 = fGroups[gindex]->IndexOfName(list.last());
     int index2 = fGroups[gindex]->IndexOfAcronym(list.last());
     int index = index1;
     if(index1 == -1)
          index = index2;

     // a teraz zwracamy jego wskaznik w danej grupie
     return fGroups[gindex]->Item(index);
}
// ---------------------------------------------------------------------------

// #brief Function that sets the state of the attributes (model)
//
// #param __mstate - pointer to the list of lists that contains attribute name and its values
// #param __notUpdatedState - state to be updated
// #return no values return
void XTT_AttributeGroups::setModelState(QList<QStringList>* __mstate, XTT_State* __notUpdatedState)
{
     MainWin->xtt_runModelMessagesDockWindowMessagesList->deleteAll();
     MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "------------------------ New model state ------------------------", 2);
     for(int i=0;i<__mstate->size();++i)
     {
          // if empty list
          if(__mstate->at(i).size() == 0)
               continue;
               
          // searching for attribute
          XTT_Attribute* att = Attribute(__mstate->at(i).at(0));
          if(att == NULL)
          {
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Changing attribute value error: cannot find attribute \'" + __mstate->at(i).at(0) + "\'.", 0);
               continue;
          }
          
          // creating set of values
          hSet* newset = new hSet(att->Type());
          for(int v=1;v<__mstate->at(i).size();++v)
               newset->add(new hValue(__mstate->at(i).at(v)));
          hMultipleValue* newattval = new hMultipleValue(newset);
          
          // simplifying values
          if(newattval->toCurrentNumericFormat() != H_TYPE_ERROR_NO_ERROR)
          {
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Converting result to current numeric format error: " + newattval->parentType()->lastError(), 0);
               continue;
          }

          // setting new value
          bool isValOk = att->checkValue(newattval);
          if(!isValOk)
          {
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Changing attribute value error: incorrect value \'" + newset->toString() + "\' for attribute \'" + __mstate->at(i).at(0) + "\'.", 0);
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Type error: " + att->Type()->lastError(), 0);
               continue;
          }
          if(isValOk)
          {
               if((att->Class() == XTT_ATT_CLASS_WO) && (__notUpdatedState != NULL))
                    __notUpdatedState->setOutputValueAt(att, newset->copyTo());
               
               att->setValue(newattval);
               MainWin->xtt_runModelMessagesDockWindowMessagesList->add("", "", "", "Updated value of attribute \'" + __mstate->at(i).at(0) + "\' to \'" + newset->toString() + "\'.", 2);
          }
     }
     OutputStates->refreshStatesUI();
}
// ---------------------------------------------------------------------------

// #brief Function that returns pointer to the list that contains pointers to the all attributes
//
// #return pointer to the list that contains pointers to the all attributes
QList<XTT_Attribute*>* XTT_AttributeGroups::Attributes(void)
{
     QList<XTT_Attribute*>* result = new QList<XTT_Attribute*>;
     
     for(unsigned int i=0;i<fCount;++i)
     {
          if((i == 1) || (i == 2))
               continue;
                    
          for(unsigned int j=0;j<fGroups[i]->Count();++j)
               result->append(fGroups[i]->Item(j));
     }
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function creates and gets path for the group.
//
// #param _ptr Pointer to the group attribute.
// #return Path for the group. If pointer is incorrect function returns "#NAN".
QString XTT_AttributeGroups::CreatePath(XTT_Attribute* _ptr)
{
     QString res = "#NAN";

     for(unsigned int i=0;i<fCount;i++)
          for(unsigned int j=0;j<fGroups[i]->Count();j++)
               if(fGroups[i]->Item(j) == _ptr)
               {
                    res = CreatePath(i) + ".";
                    if((fGroups[i]->Name() == "Global") || (fGroups[i]->Name() == "action"))
                         res = "";
                    break;
               }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function finds and gets pointer to the attribute.
//
// #param _id Attribute id.
// #return Pointer to the attribute.
XTT_Attribute* XTT_AttributeGroups::FindAttribute(QString _attrID)
{
     XTT_Attribute* res = NULL;

     for(unsigned int i=0;i<fCount;i++)
          for(unsigned int j=0;j<fGroups[i]->Count();j++)
               if(fGroups[i]->Item(j)->Id() == _attrID)
               {
                    res = fGroups[i]->Item(j);
                    break;
               }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function finds and gets pointer to the group of attribute.
//
// #param _aptr Pointer to the attribute.
// #return Pointer to the group of attribute.
XTT_AttributeList* XTT_AttributeGroups::AttributeGroup(XTT_Attribute* _aptr)
{
     XTT_AttributeList* res = NULL;

     for(unsigned int i=0;i<fCount;i++)
          for(unsigned int j=0;j<fGroups[i]->Count();j++)
               if(fGroups[i]->Item(j) == _aptr)
               {
                    res = fGroups[i];
                    break;
               }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function finds and gets index of the group of attribute.
//
// #param attid - id of the attribute.
// #return index of the group
int XTT_AttributeGroups::AttributeGroup(QString __attid)
{
     int res = -1;

     for(unsigned int i=0;i<fCount;i++)
          for(unsigned int j=0;j<fGroups[i]->Count();j++)
               if(fGroups[i]->Item(j)->Id() == __attid)
               {
                    res = i;
                    break;
               }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function that moves the given attribute to the given group
//
// #param __attId - id of the attribute.
// #param __destGroupIndex - index of the group where the attribute schould be moved
// #return XTT_MOVE_ATTRIBUTE_RESULT_...
int XTT_AttributeGroups::moveAttribute(QString __attId, int __destGroupIndex)
{
     int srcGroupIndex = AttributeGroup(__attId);
     int attrIndex = AttributeGroups->Group(srcGroupIndex)->IndexOfID(__attId);

     // Gdy nie znaleziono
     if(srcGroupIndex == -1)
          return XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_SOURCE_GROUP;
     if(__destGroupIndex == -1)
          return XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_DESTINATION_GROUP;
     if(attrIndex == -1)
          return XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_ATTRIBUTE;

     // Sprawdzanie czy podana nazwa juz nie istnieje
     QString name = AttributeGroups->Group(srcGroupIndex)->Item(attrIndex)->Name();
     QString atrrNewName = name;                                                               // Nazwa atrybutu
     int sgindex = srcGroupIndex;                                                              // Index starej grupy
     int dgindex = __destGroupIndex;                                                           // Index nowej grupy

     int sgai = attrIndex;                                                                    // Index atrybutu w starej grupie
     int dgai = AttributeGroups->Group(dgindex)->IndexOfName(atrrNewName);                     // Index atrybutu w nowej grupie
     int dgaci = AttributeGroups->Group(dgindex)->IndexOfAcronym(atrrNewName);                 // Index atrybutu w nowej grupie o akronimie takim jak nazwa atrybutu

     if(((dgai > -1) && (sgindex == dgindex) && (sgai != dgai)) ||
      ((dgai > -1) && (sgindex != dgindex)) ||
      ((dgaci > -1) && (sgindex == dgindex) && (sgai != dgaci)) ||
      ((dgaci > -1) && (sgindex != dgindex)))
          return XTT_MOVE_ATTRIBUTE_RESULT_NAME_OR_ACRONYM_EXISTS;

     // Sprawdzanie czy podany akronim juz nie istnieje
     QString atrrNewAcronym = AttributeGroups->Group(sgindex)->Item(attrIndex)->Acronym();    // Nowy akronim atrybutu
     dgai = AttributeGroups->Group(dgindex)->IndexOfAcronym(atrrNewAcronym);                   // Index atrybutu w nowej grupie
     dgaci = AttributeGroups->Group(dgindex)->IndexOfName(atrrNewAcronym);                     // Index atrybutu o nazwie jak akronim aktalnie przenoszonego

     if(((dgai > -1) && (sgindex == dgindex) && (sgai != dgai)) ||
      ((dgai > -1) && (sgindex != dgindex)) ||
      ((dgaci > -1) && (sgindex == dgindex) && (sgai != dgaci)) ||
      ((dgaci > -1) && (sgindex != dgindex)))
          return XTT_MOVE_ATTRIBUTE_RESULT_NAME_OR_ACRONYM_EXISTS;

     // Gdy nastepuje zmiana grupy to trzeba przeniesc atrybut
     if(sgindex != dgindex)
     {
          AttributeGroups->Group(dgindex)->Add(AttributeGroups->Group(sgindex)->Item(attrIndex));   // Dodawanie do nowej grupy
          AttributeGroups->Group(sgindex)->Delete(sgai, false);	                                   // Usuwanie ze starej grupy
     }
     
     return XTT_MOVE_ATTRIBUTE_RESULT_OK;
}
// ---------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_AttributeGroups::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     for(unsigned int g=0;g<fCount;g++)
          res = fGroups[g]->updateTypePointer(__old, __new) && res;
          
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_AttributeGroups::eventMessage(QString* __errmsgs, int __event, ...)
{ 
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int g=0;g<fCount;g++)
               res = fGroups[g]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int g=0;g<fCount;g++)
               res = fGroups[g]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets list of unused attributes.
//
// #return List of unused attributes.
QStringList XTT_AttributeGroups::UnusedAttributesList(void)
{
     QStringList res;
     res.clear();

     for(unsigned int g=0;g<fCount;g++)
     {
          for(unsigned int a=0;a<fGroups[g]->Count();a++)
          {
               XTT_Attribute* _ptr = fGroups[g]->Item(a);
               
               if(_ptr->isConceptual())
                    continue;
               
               // Wyszukujemy w tabelach czy istnieje powyzszy atrybut
               int index = -1;
               for(unsigned int t=0;t<TableList->Count();t++)
               {
                    if(TableList->Table(t)->TableType() == 0)         // The state tables are ignored
                         continue;
                         
                    index = TableList->Table(t)->IndexOfAttributeID(_ptr->Id());
                    if(index > -1)
                         t = TableList->Count();
               }

               // Gdy nie znaleziono tabeli w ktorej bylby uzyty dany atrybut, to
               // dodajemy go do listy nieuzywanych
               if(index == -1)
                    res << CreatePath(_ptr) + _ptr->Name();
          }
     }

     return res;
}
// ---------------------------------------------------------------------------

// #brief Function that creates uniqe attribute identifier
//
// #return uniqe attribute identifier as string
QString XTT_AttributeGroups::createUniqeAttributeIdentifier(void)
{
     int id_index = 1;
     QString candidate = "";
     QString prefix = XTT_Attribute::idPrefix();
     
     do
     {
          candidate = prefix + QString::number(id_index++);
     }
     while(indexOfAttId(candidate) != NULL);
     
     return candidate;
}
// ---------------------------------------------------------------------------

// #brief creates unique acronym name
//
// #param _name - the name of attribute
// #param _prevG - pointer to the previous group
// #param _currG - pointer to the current group
// #param _attIndex - index of attribute in previous group
// #return acronym as string. The acronym has unique name
QString XTT_AttributeGroups::createAcronym(QString _name, XTT_AttributeList* _prevG, XTT_AttributeList* _currG, int _attIndex)
{
     QString newAcronymBaseName = _name;
     
     XTT_AttributeList* sg = _prevG;                                                                     // Index starej grupy
     XTT_AttributeList* dg = _currG;                                                                     // Index nowej grupy
     int sgai = _attIndex;                                                                      // Index atrybutu w starej grupie
     
     // Jezeli tekst jest zbyt dlugi
     if(newAcronymBaseName.length() > 4)
          newAcronymBaseName = newAcronymBaseName.remove(4, newAcronymBaseName.length()-4);
          
     int newAcronymIndex = 0;
     bool is_ok;
     QString newAcronymName;
     
     // Wyszukiwanie odpowiedniej nazwy dla akronimu
     do
     {
          is_ok = true;
          
          newAcronymIndex++;
          newAcronymName = QString::number(newAcronymIndex);
          
          // Poniewaz dlugosc nazwy akronium nie moze byc dluzsza niz 5 znakow to musimy jeszcze obciac
          // aktualna nazwe bazowa akronimu
          int lenSum = newAcronymName.length() + newAcronymBaseName.length();
          if(lenSum > 5)
               newAcronymBaseName = newAcronymBaseName.remove(newAcronymBaseName.length()-(lenSum-5), (lenSum-5));
               
          // Utworzenie nowej nazwy
          newAcronymName = newAcronymBaseName + newAcronymName;
          
          // Sprawdzanie czy w aktualnie wybranej grupie przeznaczenia nie wystepuje juz dany akronim lub nazwa
          int dgai = dg->IndexOfName(newAcronymName);                  // Index atrybutu w nowej grupie
          int dgaci = dg->IndexOfAcronym(newAcronymName);              // Sprawdzanie czy nie istnieje taki akronim
          
          // Jezeli grupa docelowa jest taka sama jak bierzaca
          if(sg == dg)
          {
               // Jezeli istnieje juz taki akronim
               if((dgaci > -1) && (dgaci != sgai))
                    is_ok = false;
                    
               // Jezeli istnieje juz taka nazwa
               if((dgai > -1) && (dgai != sgai))
                    is_ok = false;
          }
          
          // Jezeli grupa docelowa jest inna niz bierzaca
          if(sg != dg)
          {
               // Jezeli istnieje juz taki akronim
               if(dgaci > -1)
                    is_ok = false;
                    
               // Jezeli istnieje juz taka nazwa
               if(dgai > -1)
                    is_ok = false;
          }
     }
     while(!is_ok);
     
     return newAcronymName;
}
// ---------------------------------------------------------------------------

// #brief creates unique acronym name
//
// #param QString _name - the name of attribute
// #param int _prevG - index of previous group
// #param int _currG - index of current group
// #param int _attIndex - index of attribute in previous group
// #return acronym as string. The acronym has unique name
QString XTT_AttributeGroups::createAcronym(QString _name, int _prevG, int _currG, int _attIndex)
{
     if((_prevG < 0) || (_currG < 0) || (_prevG >= (int)Count()) || (_currG >= (int)Count()))
          return "";
     
     return createAcronym(_name, Group(_prevG), Group(_currG), _attIndex);
}
// ---------------------------------------------------------------------------

// #brief Function that maps the attributes to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this attributes as string.
QString XTT_AttributeGroups::toProlog(int /*__option*/)
{
     QString result = "";
     for(unsigned int a=0;a<fCount;a++)
     {
          // Jezeli grupa tmp, to nie zapisujemy jej
          if(a == 1)
               continue;
          result += fGroups[a]->toProlog();
     }
     
     return result;
}
// ---------------------------------------------------------------------------

// #brief Function that returns if the attributes object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_AttributeGroups::attributesVerification(QString& __error_message)
{
     bool res = true;
     
     QStringList ua = UnusedAttributesList();
     for(int i=0;i<ua.size();++i)
          __error_message += hqed::createErrorString("", "", "Unused attribute \'" + ua.at(i) + "\'.", 1, 2);
     //res = res && ua.isEmpty();
     
     for(unsigned int a=0;a<fCount;a++)
          res = fGroups[a]->attributesVerification(__error_message) && res;
     return res;
}
// ---------------------------------------------------------------------------

// #brief Function reads data from file in XTTML 1.0 format.
//
// #param root Reference to the section of this group.
// #return True when data is ok, otherwise false.
bool XTT_AttributeGroups::FromATTML_1_0(QDomElement& root)
{
     bool res = true;
          
     // Reszta zakladamy ze jest ok
     // Gdy wcztujemy grupy
     if(root.tagName().toLower() == "group_list")
     {
          /*
          // Usuwanie danych dotychczasowych
          if(fCount > 0)
          {
               for(unsigned int i=0;i<fCount;i++)
                    delete fGroups[i];
               delete [] fGroups;

               fGroups = NULL;
               fGId = 0;
               gAttrId = 0;
               fCount = 0;
          }
          
          // Utworzenie grupy atrybutow globalnych i tymczasowych
          CreateGlobalVariablesGroup();*/
          
          XTT_AttributeList* group = NULL;
          QDomElement child = root.firstChildElement("group");
          while(!child.isNull()) 
          {
               // Wczytywanie parametrow w zaleznosci od wykrytego tagu
               QString gName = child.attribute("name", "#NAN#");
               QString gParent = child.attribute("parent", "#NAN#");
               
               // Usuwanie spacji z nazwy
               for(;gName.contains(" ");)
                    gName = gName.remove(gName.indexOf(" "), 1);
               if((gName == "#NAN#") || (gName == ""))
               {
                    QMessageBox::critical(NULL, "HQEd", "Missing group name", QMessageBox::Ok);
                    return false;
               }
               
               // Index grupy - sprawdzamy czy taka istnieje
               int group_index = IndexOfName(gName);
               
               // Dodawanie nowej grupy gdy ta nie istnieje
               if(group_index == -1)
               {
                    Add();
                    group_index = fCount-1;
               }
                    
               group = fGroups[group_index];
               group->setName(gName);
               
               // Wyszukiwanie grupy rodzica
               // Usuwanie spacji z nazwy
               for(;gParent.contains(" ");)
                    gParent = gParent.remove(gParent.indexOf(" "), 1);
               if((gParent != "#NAN#") && (gParent != ""))
               {
                    int gindex = IndexOfName(gParent);
                    if(gindex == -1)
                    {
                         QMessageBox::critical(NULL, "HQEd", "Not definded parent group " + gParent + ".\nPlease define this group first and try again.", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Ustawianie rodzica grupy
                    setGroupParent(group, fGroups[gindex]->ID());
               }

               // Wczytanie opisu grupy
               QString gDesc = child.firstChildElement("description").text();
               if(group == NULL)
               {
                    QMessageBox::critical(NULL, "HQEd", "Missplaced group description tag.", QMessageBox::Ok);
                    return false;
               }
               if(!gDesc.isEmpty())
                    group->setDescription(gDesc);
               
               group = NULL;
               child = child.nextSiblingElement("group");
          }
     }
     
     
     // Gdy wcztujemy atrybuty
     if(root.tagName().toLower() == "attribute_list")
     {
          // Aktualnie wczytywany atrybut
          XTT_Attribute* attr = NULL;
          
          // Okresla czy w przypadku zdefiniowania atrybutu jako enum, zbior mozliwych wartosci zostal okreslony
          bool EnumerateValuesAreGiven = false;
          
          // Okresla czy w przypadku zdefiniowania ograniczen dziedziny atrybutu, dziedzina zostala okreslona
          bool ConstraintsValuesAreGiven = false;
          
          // Tutaj bedziemy przechowywac wartosci atrybutu, bo musimy je ustawic na koncu wczytywania
          // inaczej moga powodowac bledy jezeli ograniczenia sa nie wczytane
          QString aCurrVal, aDefVal;
          
          QDomElement child = root.firstChildElement("xtt_attribute");
          while(!child.isNull()) 
          {
               // Wczytywanie parametrow w zaleznosci od wykrytego tagu
               // Tworzymy nowy atrybut
               gAttrId++;
               attr = new XTT_Attribute("att_" + QString::number(gAttrId));
               attr->setMultiplicity(false);
               
               // Teraz wczytujemy informacje o atrybutach
               QStringList arelationstypes, atypes, acontraints;
               arelationstypes << "input" << "middle" << "output";
               atypes << "bool" << "integer" << "float" << "symbloic" << "enum";
               acontraints << "no" << "range" << "set";

               QString aName = child.attribute("name", "#NAN#");
               QString aAcronym = child.attribute("acronym", "#NAN#");
               QString aGroup = child.attribute("group", "#NAN#");
               QString aRelations = child.attribute("realtions", "#NAN#");
               QString aType = child.attribute("type", "#NAN#");
               QString aConstrType = child.attribute("constraint", "#NAN#");
               aDefVal = child.attribute("defvalue", "#NAN#");
               aCurrVal = child.attribute("currvalue", "#NAN#");
               
               // Usuwanie spacji z wczytanych wartosci
               for(;aName.contains(" ");)
                    aName = aName.remove(aName.indexOf(" "), 1);
               for(;aAcronym.contains(" ");)
                    aAcronym = aAcronym.remove(aAcronym.indexOf(" "), 1);
               for(;aRelations.contains(" ");)
                    aRelations = aRelations.remove(aRelations.indexOf(" "), 1);
               for(;aGroup.contains(" ");)
                    aGroup = aGroup.remove(aGroup.indexOf(" "), 1);
               for(;aType.contains(" ");)
                    aType = aType.remove(aType.indexOf(" "), 1);
               for(;aDefVal.contains(" ");)
                    aDefVal = aDefVal.remove(aDefVal.indexOf(" "), 1);
               for(;aCurrVal.contains(" ");)
                    aCurrVal = aCurrVal.remove(aCurrVal.indexOf(" "), 1);
               for(;aConstrType.contains(" ");)
                    aConstrType = aConstrType.remove(aConstrType.indexOf(" "), 1);
                    
               // Sprawdzanie podania nazwy
               if((aName == "#NAN#") || (aName == ""))
               {
                    QMessageBox::critical(NULL, "HQEd", "Missing attribute name at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie podania akronimu
               if((aAcronym == "#NAN#") || (aAcronym == ""))
               {
                    QMessageBox::critical(NULL, "HQEd", "Missing attribute acronym at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie podania proawidlowego typu relacji atrybutu
               if((aRelations == "#NAN#") || (aRelations == ""))
               {
                    QMessageBox::critical(NULL, "HQEd", "Missing attribute relation at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               if(!arelationstypes.contains(aRelations, Qt::CaseInsensitive))
               {
                    QMessageBox::critical(NULL, "HQEd", "Incorrect relations type value: \"" + aRelations + "\" at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie podania proawidlowego typu atrybutu
               if((aType == "#NAN#") || (aType == ""))
               {
                    QMessageBox::critical(NULL, "HQEd", "Missing attribute type at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               if(!atypes.contains(aType, Qt::CaseInsensitive))
               {
                    QMessageBox::critical(NULL, "HQEd", "Incorrect attribute type: \"" + aType + "\" at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Jezeli nie podano typu ograniczen to uznajemy ze ich nie ma
               if((aConstrType == "#NAN#") || (aConstrType == ""))
               {
                    aConstrType = "no";
               }
               
               // Sprawdzanie czy podano prawidlowe okreslenie ograniczen
               if((aConstrType != "#NAN#") && (aConstrType != "") && (!acontraints.contains(aConstrType, Qt::CaseInsensitive)))
               {
                    QMessageBox::critical(NULL, "HQEd", "Incorrect attribute constraints defined: \"" + aConstrType + "\" at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Sprawdzanie czy podano grupe, jezeli nie to ustawiamy na Global
               if((aGroup == "#NAN#") || (aGroup == ""))
               {
                    aGroup = "Global";
               }
               
               // Sprawdzanie czy podano grupe ktora jest juz zdefiniowana
               if(IndexOfName(aGroup) == -1)
               {
                    QMessageBox::critical(NULL, "HQEd", "Not defined group " + aGroup + " at line " + QString::number(child.lineNumber()) + ".", QMessageBox::Ok);
                    return false;
               }
               
               // Ustawienie grupy atrybutu
               int ion = fGroups[IndexOfName(aGroup)]->IndexOfName(aName);
               int ioa = fGroups[IndexOfName(aGroup)]->IndexOfAcronym(aAcronym);
               
               // Jezeli podany atrybut nie istnieje w danej grupie
               if((ion == -1) && (ioa == -1))
                    fGroups[IndexOfName(aGroup)]->Add(attr);
                    
               // Jezeli dany argument istnieje w tej grupie, to uaktualniamy jego parametry
               if((ion > -1) && (ioa > -1) && (ion == ioa))
               {
                    delete attr;
                    attr = fGroups[IndexOfName(aGroup)]->Item(ion);
               }
               
               // Jezeli istnieje argument o podanej nazwie lub akronimie i drugiego parametru nie ma takiego samego
               // to  wczytujemy nastepny atrybut
               if(((ion > -1) || (ioa > -1)) && (ion != ioa))
               {
                    delete attr;
                    attr = NULL;
                    continue;
               }
               
               // Ustawianie parametrow atrybutu
               int fAttrConstrType = 0;           // typ ograniczen atrybutu
               
               // Tworzenie typu dla atrybutu
               // Searching for free name
               int cntr = 1;
               QString cname = aName;
               while(typeList->indexOfName(cname) > -1)
               {
                    cname = aName + QString::number(cntr);
                    cntr++;
               }
               
               // reading the type of constraints
               int oht = atypes.indexOf(aType.toLower())+1;
               int fat = acontraints.indexOf(aConstrType.toLower());
               
               if(oht == 5)
                    fAttrConstrType = 3;
               if((fAttrConstrType != 3) && (fat == 0))     // gdy typ bez ograniczen, to jako typ atrybutu ustawiamy typ prymitywny
               {
                    int tids[] = {BOOLEAN_BASED, BOOLEAN_BASED, INTEGER_BASED, NUMERIC_BASED, SYMBOLIC_BASED};
                    attr->setType(typeList->at(hpTypesInfo.iopti(tids[oht])));
               }
               if((fAttrConstrType == 3) || (fat != 0))     // jezeli mamy jakiekolwiek ograniczenia, to tworzymy nowy typ na bazie prymitywnego
               {
                    attr->setType(typeList->createNewType(oht));
                    attr->Type()->setName(cname);
                    attr->Type()->setClass(TYPE_CLASS_DOMAIN);
               }
               
               // Ustawianie parametrow atrybutu
               attr->setName(aName);
               attr->setAcronym(aAcronym);
               attr->setClass((unsigned int)(arelationstypes.indexOf(aRelations.toLower())));
               //attr->setConstraintsType((unsigned int)(acontraints.indexOf(aConstrType.toLower())));
               
               // Jezeli atrybut nie jest typu enum to ustawiamy flage podania wartosci wyliczenioeych na ture
               if(aType.toLower() != "enum")
                    EnumerateValuesAreGiven = true;
               
               // Jezeli atrybut jest bez ograniczen to flage zdefiniowania dziedziny ustawiamy na true
               if(aConstrType.toLower() == "no")
                    ConstraintsValuesAreGiven = true;
                         
               
               // Wczytywanie opisu
               QDomElement description = child.firstChildElement("description");
               if(!description.isNull())
               {
                    QString aDesc = description.text();
                    if(attr == NULL)
                    {
                         QMessageBox::critical(NULL, "HQEd", "Missplaced attribute description tag.", QMessageBox::Ok);
                         return false;
                    }
                    attr->setDescription(aDesc);
               }
               
               // <enum_values>
               QDomElement enmuvals = child.firstChildElement("enum_values");
               if(!enmuvals.isNull())
               {
                    // Jezeli typ atrybutu zostal ustawiony na enumaerate
                    if(attr == NULL)
                         return false;     
                    if(attr->Type()->fromHekateType() == 5)
                         EnumerateValuesAreGiven = true;
                    attr->Type()->setClass(TYPE_CLASS_DOMAIN);
                    
                    QDomElement enmuval = enmuvals.firstChildElement("ev");
                    while(!enmuval.isNull())
                    {
                         if(attr == NULL)
                              return false;     
                         int valnumrepr = attr->Type()->domain()->size();
                         attr->Type()->domain()->add(new hSetItem(attr->Type()->domain(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(enmuval.text(), QString::number(valnumrepr)), new hValue, false));
                         enmuval = enmuval.nextSiblingElement("ev");
                    }
               }
               
               // <low_constraint
               QDomElement lowconstr = child.firstChildElement("low_constraint");
               if(!lowconstr.isNull())
               {
                    // Okreslenie mozliwych typow nawiasow na granicach przedzialu
                    QStringList braceTypes;
                    braceTypes << "square" << "round";
                    
                    if(attr == NULL)
                         return false;     
                    ConstraintsValuesAreGiven = true;
                         
                    // Wczytywanie parametrow tagu
                    QString bracetype = lowconstr.attribute("brackettype", "#NAN#");
                    QString limit = lowconstr.attribute("limit", "#NAN#");
                    QString ignore = lowconstr.attribute("ignore", "#NAN#");
                    
                    // Usuwanie spacji z wczytanych wartosci
                    for(;bracetype.contains(" ");)
                         bracetype = bracetype.remove(bracetype.indexOf(" "), 1);
                    for(;limit.contains(" ");)
                         limit = limit.remove(limit.indexOf(" "), 1);
                    for(;ignore.contains(" ");)
                         ignore = ignore.remove(ignore.indexOf(" "), 1);
                         
                    // Sprawdzanie podania limitu
                    if((limit == "#NAN#") || (limit == ""))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Missing contraint attribute parameter \"limit\" at line " + QString::number(lowconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie podania parametru ignore
                    if((ignore == "#NAN#") || (ignore == ""))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Missing contraint attribute parameter \"ignore\" at line " + QString::number(lowconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie czy podana wartosc ignore jest poprawna
                    if((ignore != "true") && (ignore != "false"))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Incorrect contraint parameter \"ignore\" value at line " + QString::number(lowconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie podania typu nawiasu
                    if((bracetype == "#NAN#") || (bracetype == ""))
                         bracetype = "round";
                         
                    // Jezeli podano typ nawiasu to sprawdzanie czy jest on wlasciwy
                    if((bracetype != "#NAN#") && (bracetype != "") && (!braceTypes.contains(bracetype, Qt::CaseInsensitive)))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Incorrect brace type: \"" + bracetype + "\" at line " + QString::number(lowconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Ustawianie wartosci parametrow
                    //attr->setLoBraceType((bool)braceTypes.indexOf(bracetype.toLower()));
                    //attr->setLoConstraints(limit.toDouble());
                    //attr->setLoIgnoreConstraints((ignore=="true")?true:false);
                    
                    // If there is no object in the domain
                    if(attr->Type()->domain()->size() == 0)
                         attr->Type()->domain()->add(new hSetItem(attr->Type()->domain(), SET_ITEM_TYPE_RANGE, new hValue(QString::number(NUMERIC_INFIMUM)), new hValue(QString::number(NUMERIC_SUPREMUM)), false));
                    
                    bool ic  = (ignore=="true")?true:false;
                    if(!ic)
                    {
                         if((attr->Type()->baseType() == INTEGER_BASED) || (attr->Type()->baseType() == NUMERIC_BASED))
                              attr->Type()->toCurrentNumericFormat(limit, true);
                         attr->Type()->domain()->at(0)->setFrom(new hValue(limit));
                    }
               }
               
               // <hi_constraint
               QDomElement hiconstr = child.firstChildElement("hi_constraint");
               if(!hiconstr.isNull())
               {
                    // Okreslenie mozliwych typow nawiasow na granicach przedzialu
                    QStringList braceTypes;
                    braceTypes << "square" << "round";
                    
                    if(attr == NULL)
                         return false;     
                    ConstraintsValuesAreGiven = true;
                         
                    // Wczytywanie parametrow tagu
                    QString bracetype = hiconstr.attribute("brackettype", "#NAN#");
                    QString limit = hiconstr.attribute("limit", "#NAN#");
                    QString ignore = hiconstr.attribute("ignore", "#NAN#");
                    
                    // Usuwanie spacji z wczytanych wartosci
                    for(;bracetype.contains(" ");)
                         bracetype = bracetype.remove(bracetype.indexOf(" "), 1);
                    for(;limit.contains(" ");)
                         limit = limit.remove(limit.indexOf(" "), 1);
                    for(;ignore.contains(" ");)
                         ignore = ignore.remove(ignore.indexOf(" "), 1);
                         
                    // Sprawdzanie podania limitu
                    if((limit == "#NAN#") || (limit == ""))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Missing contraint attribute parameter \"limit\" at line " + QString::number(hiconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie podania parametru ignore
                    if((ignore == "#NAN#") || (ignore == ""))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Missing contraint attribute parameter \"ignore\" at line " + QString::number(hiconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie czy podana wartosc ignore jest poprawna
                    if((ignore != "true") && (ignore != "false"))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Incorrect contraint parameter \"ignore\" value at line " + QString::number(hiconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Sprawdzanie podania typu nawiasu
                    if((bracetype == "#NAN#") || (bracetype == ""))
                         bracetype = "round";
                         
                    // Jezeli podano typ nawiasu to sprawdzanie czy jest on wlasciwy
                    if((bracetype != "#NAN#") && (bracetype != "") && (!braceTypes.contains(bracetype, Qt::CaseInsensitive)))
                    {
                         QMessageBox::critical(NULL, "HQEd", "Incorrect brace type: \"" + bracetype + "\" at line " + QString::number(hiconstr.lineNumber()) + ".", QMessageBox::Ok);
                         return false;
                    }
                    
                    // Ustawianie wartosci parametrow
                    //attr->setHiBraceType((bool)braceTypes.indexOf(bracetype.toLower()));
                    //attr->setUpConstraints(limit.toDouble());
                    //attr->setHiIgnoreConstraints((ignore=="true")?true:false);
                    
                    // If there is no object in the domain
                    if(attr->Type()->domain()->size() == 0)
                         attr->Type()->domain()->add(new hSetItem(attr->Type()->domain(), SET_ITEM_TYPE_RANGE, new hValue(QString::number(NUMERIC_INFIMUM)), new hValue(QString::number(NUMERIC_SUPREMUM)), false));
                    
                    bool ic  = (ignore=="true")?true:false;
                    if(!ic)
                    {
                         if((attr->Type()->baseType() == INTEGER_BASED) || (attr->Type()->baseType() == NUMERIC_BASED))
                              attr->Type()->toCurrentNumericFormat(limit, true);
                         attr->Type()->domain()->at(0)->setTo(new hValue(limit));
                    }
               }
               
               // <enum_constraint>
               QDomElement enumconstr = child.firstChildElement("enum_constraint");
               if(!enumconstr.isNull())
               {
                    // Jezeli typ atrybutu zostal ustawiony na enumaerate
                    if(attr == NULL)
                         return false;     
                    ConstraintsValuesAreGiven = true;
                    attr->Type()->setClass(TYPE_CLASS_DOMAIN);
                    
                    QDomElement enmuval = enumconstr.firstChildElement("ecv");
                    while(!enmuval.isNull())
                    {
                         QString ev_num = QString::number(attr->Type()->domain()->size());
                         QString ev_str = enmuval.text();
                         if((attr->Type()->baseType() == INTEGER_BASED) || (attr->Type()->baseType() == NUMERIC_BASED))
                              attr->Type()->toCurrentNumericFormat(ev_str, true);
                         attr->Type()->domain()->add(new hSetItem(attr->Type()->domain(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(ev_str, ev_num), new hValue, false));
                         enmuval = enmuval.nextSiblingElement("ecv");
                    }     
               }
               

               // Sprawdzanie czy podano dzeidzine atrybutu, weryfikacja wczytanych danych
               if(!ConstraintsValuesAreGiven)
               {
                    QMessageBox::critical(NULL, "HQEd", "Not defined attribute \"" + attr->Name() + "\" domain.", QMessageBox::Ok);
                    return false;
               }
               
               //if((aDefVal != "#NAN#") && (aDefVal != ""))
               //     attr->setDefaultValue(aDefVal);
               if((aCurrVal != "#NAN#") && (aCurrVal != ""))
               {
                    // The compatybility with the XTT 2.0
                    if(aCurrVal == "op:nd") 
                         aCurrVal = XTT_Attribute::_not_definded_operator_;
                    if(aCurrVal == "op:any") 
                         aCurrVal = XTT_Attribute::_any_operator_;
                        
                    // Creating new instance of value
                    hMultipleValue* val = hMultipleValue::makeSingleValue(attr->Type(), aCurrVal, true);
                    if(val == NULL)
                         QMessageBox::critical(NULL, "HQEd", "The definition of the attribute \'" + attr->Name() + "\' value is out of domain.", QMessageBox::Ok);
                    if(val != NULL)
                         attr->setValue(val);
               }
               
               attr = NULL;
               child = child.nextSiblingElement("xtt_attribute");

          }
          
          // Sortowanie atrybutow
          for(unsigned int gindex=0;gindex<fCount;gindex++)
               AttributeGroups->Group(gindex)->sort();
     }
     

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_AttributeGroups::out_HML_2_0(int /*__mode*/)
{
     QStringList* res = new QStringList;
     res->clear();

     // Groups saving
     for(unsigned int i=0;i<fCount;++i)
     {
          if(i <= 2)
               continue;
               
          QStringList* tmp = fGroups[i]->out_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << "\t" + tmp->at(j);
          delete tmp;
     }
     
     // Attributes saving
     for(unsigned int i=0;i<fCount;++i)
     {
          if((i == 1) || (i == 2))
               continue;
               
          for(unsigned int j=0;j<fGroups[i]->Count();++j)
          {
               QStringList* tmp = fGroups[i]->Item(j)->out_HML_2_0();
               for(int j=0;j<tmp->size();++j)
                    *res << "\t" + tmp->at(j);
               delete tmp;
          }
     }
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void XTT_AttributeGroups::out_HML_2_0(QXmlStreamWriter* _xmldoc, int /*__mode*/)
{
     for(unsigned int i=0;i<fCount;++i)      // Saving groups
     {
          if(i <= 2)
               continue;
          fGroups[i]->out_HML_2_0(_xmldoc);
     }

     for(unsigned int i=0;i<fCount;++i)      // Saving attributes
     {
          if((i == 1) || (i == 2))
               continue;
          for(unsigned int j=0;j<fGroups[i]->Count();++j)
               fGroups[i]->Item(j)->out_HML_2_0(_xmldoc);
     }
}
// -----------------------------------------------------------------------------

void XTT_AttributeGroups::out_Drools_Workspace(QTextStream* _javafile)
{
     *_javafile << "package com.sample;" << "\n\n";
     *_javafile << "public class Workspace {" << "\n\n";

     for(unsigned int i=0;i<fCount;++i)      // Saving attributes
     {
          if((i == 1) || (i == 2))
               continue;
          for(unsigned int j=0;j<fGroups[i]->Count();++j)
               fGroups[i]->Item(j)->out_Drools_Workspace(_javafile);
     }

     *_javafile << "}" << "\n";
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_AttributeGroups::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "attributes";
     QDomElement DOMatts = __root.firstChildElement(elementName);   // one element is required
     
     // Checking for errors
     if(DOMatts.isNull())
     {
          __msg += "\n" + hqed::createErrorString("", "", "Mssing \'" + elementName + "\' element." + hqed::createDOMlocalization(DOMatts), 0, 2);
          return 1;
     }
     
     QDomElement DOM2nd = DOMatts.nextSiblingElement(elementName);
     if(!DOM2nd.isNull())
     {
          __msg += "\n" + hqed::createErrorString("", "", "Too many elements \'" + elementName + "\'." + hqed::createDOMlocalization(DOM2nd), 0, 2);
          return 1;
     }

     // Reading attributes
     QString attr = "attr";
     QDomElement DOMattr = DOMatts.firstChildElement(attr);
     while(!DOMattr.isNull())
     {
          QString aid = DOMattr.attribute("id", "");
          
          if(aid == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined attribute id." + hqed::createDOMlocalization(DOMattr), 0, 2);
               result++;
               DOMattr = DOMattr.nextSiblingElement(attr);
               continue;
          }
          if(fGroups[0]->IndexOfID(aid) > -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "The attribute with id \'" + aid + "\' already exists." + hqed::createDOMlocalization(DOMattr), 0, 2);
               result++;
               DOMattr = DOMattr.nextSiblingElement(attr);
               continue;
          }

          XTT_Attribute* newxttatt = fGroups[0]->Add();
          result += newxttatt->in_HML_2_0(DOMattr, __msg);

          // Adding new ARD attribute
          ARD_Attribute* newardatt;
          newardatt = new ARD_Attribute("att_ard_"+newxttatt->Id(), newxttatt->Name());
          newardatt->setDescription(newxttatt->Description());
          newardatt->setXTTequivalent(newxttatt);
          ardAttributeList->add(newardatt);
               
          // Search for next group
          DOMattr = DOMattr.nextSiblingElement(attr);
     }

     // Reading groups
     QString agroup = "agroup";
     QDomElement DOMagroup = DOMatts.firstChildElement(agroup);
     while(!DOMagroup.isNull())
     {
          QString gid = DOMagroup.attribute("id", "");
          
          if(gid == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined group id." + hqed::createDOMlocalization(DOMagroup), 0, 2);
               result++;
               DOMagroup = DOMagroup.nextSiblingElement(agroup);
               continue;
          }
          if(IndexOfID(gid) > -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "The group with id \'" + gid + "\' already exists." + hqed::createDOMlocalization(DOMagroup), 0, 2);
               result++;
               DOMagroup = DOMagroup.nextSiblingElement(agroup);
               continue;
          }
          
          // Adding group
          Add();
          result += fGroups[fCount-1]->in_HML_2_0(DOMagroup, __msg);
          
          // Search for next group
          DOMagroup = DOMagroup.nextSiblingElement(agroup);
     }
     
     // Fixing acronym problems
     for(unsigned int i=0;i<Count();++i)
     {
          if((i == 1) || (i == 2))
               continue;
               
          fGroups[i]->checkAndFixAttributeAcronyms();
     }

     return result;
}
// -----------------------------------------------------------------------------
