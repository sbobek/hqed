/*
 *        $Id: XTT_AttributeList.cpp,v 1.36.4.1 2010-11-24 10:40:51 llk Exp $
 *     
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>

#include <stdarg.h>

#include "../../namespaces/ns_hqed.h"

#include "XTT_Table.h"
#include "XTT_TableList.h"
#include "XTT_Attribute.h"
#include "XTT_AttributeGroups.h"

#include "XTT_AttributeList.h"
#include <iostream>
//---------------------------------------------------------------------------

// Deklaracja glownego zbiornika atrybutow
XTT_AttributeList* AttributeList;
//---------------------------------------------------------------------------

// #brief Constructor XTT_AttributeList class.
XTT_AttributeList::XTT_AttributeList(void)
{
     fId = idPrefix();
     fList = NULL;
     fCount = 0;
     fName = "";
     fDescription = "";
     fParentId = "";
     fLevel = 0;
     fChanged = false;
}
//---------------------------------------------------------------------------

// #brief Constructor XTT_AttributeList class.
//
// #param _id Container id.
XTT_AttributeList::XTT_AttributeList(unsigned int _id)
{
     fId = idPrefix() + QString::number(_id);
     fList = NULL;
     fCount = 0;
     fName = "";
     fDescription = "";
     fParentId = "";
     fLevel = 0;
     fChanged = false;
}
//---------------------------------------------------------------------------

// #brief Destructor XTT_AttributeList class.
XTT_AttributeList::~XTT_AttributeList(void)
{
     while(fCount)
     {
          // Uzywamy tej metody, gdyz zalezy nam na tym aby dabc o sposob
          // usuwania zalezny od typu pojemnika a ta metoda o ot dba
          Delete(0);
     }
     delete [] fList;
}
//---------------------------------------------------------------------------

// #brief Function retrieves pointer to the attribute.
//
// #param index Index of the container that is relative to the attribute.
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return Pointer to the attribute. If index is incorect function returns NULL.
XTT_Attribute* XTT_AttributeList::Item(int index, int __attType)
{
     if((index < 0) || (index >= (int)fCount))
          return NULL;
          
     int result = 0;
     for(unsigned int i=0;i<fCount;++i)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
               
          if(result == index)
               return fList[i];
          result++;
     }

     return NULL;
}
//---------------------------------------------------------------------------

// #brief Function sets name of container.
//
// #param name Name of container.
// #return No return value.
void XTT_AttributeList::setName(QString _nn)
{ 
     // Usuwanie spacji
     while(_nn.indexOf(" ") > 0)
     {
          _nn = _nn.remove(_nn.indexOf(" "), 1);
     }

     fName = _nn; 
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets describtion of container.
//
// #param desc Describtion of container.
// #return No return value.
void XTT_AttributeList::setDescription(QString _nd)
{ 
     fDescription = _nd; 
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets parent id.
//
// #param id Parent id.
// #return No return value.
void XTT_AttributeList::setParentId(QString _newParent)
{ 
     fParentId = _newParent; 
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function sets id of the group.
//
// #param id - new group id.
// #return No return value.
void XTT_AttributeList::setId(QString id)
{
     fId = id;
}
//---------------------------------------------------------------------------

// #brief Function determines whether object was changed or not.
//
// #param _c Determines whether object was changed(True) or not(False).
// #return No return value.
void XTT_AttributeList::setChanged(bool _c)
{
     for(unsigned int i=0;i<fCount;i++)
          fList[i]->setChanged(_c);
               
     fChanged = _c;
}
//---------------------------------------------------------------------------

// #brief Function determines whether any attribute from the container was changed.
//
// #return True when any attribute from the container was changed, otherwise function returns false.
bool XTT_AttributeList::Changed(void)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fList[i]->Changed())
               return true;
               
     return fChanged;
}
//---------------------------------------------------------------------------

// #brief Function adds new element to the attribute list.
//
// #return Pointer to the new attribute.
XTT_Attribute* XTT_AttributeList::Add(void)
{
     // Wskazik tymczasowy
     XTT_Attribute** tmp = new XTT_Attribute*[fCount+1];

     // Przepisywanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fList[i];

     // Utowrzenie nowego obiektu
     tmp[fCount] = new XTT_Attribute(NextId());

     // Kiedy lista nie jest pusta to nalezy tez skaswac liste wskaznikow
     if(fCount)
          delete [] fList;

     // Powiekszenie rozmiaru pojemnika
     fCount++;

     // Zapisanie lokalizacji nowej tablicy
     fList = tmp;
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
     
     // Zwrocenie utworzonego atrybutu
     return fList[fCount-1];
}
//---------------------------------------------------------------------------

// #brief Function adds new element to the list - container.
//
// #param _new Pointer to the new attribute that is about to be added to the list.
// #return No return value.
void XTT_AttributeList::Add(XTT_Attribute* _new)
{
     // Wskazik tymczasowy
     XTT_Attribute** tmp = new XTT_Attribute*[fCount+1];

     // Przepisywanie zawartosci
     for(unsigned int i=0;i<fCount;i++)
          tmp[i] = fList[i];

     // Utowrzenie nowego obiektu
     tmp[fCount] = _new;

     // Kiedy lista nie jest pusta to nalezy tez skaswac liste wskaznikow
     if(fCount)
          delete [] fList;

     // Powiekszenie rozmiaru pojemnika
     fCount++;

     // Zapisanie lokalizacji nowej tablicy
     fList = tmp;
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function removes one element from attribute list.
//
// #param index Index of the element that will be removed.
// #param delObj Determines whether object should be completely removed, or only pointer should be deleted.
// #return True when index is correct, otherwise false.
bool XTT_AttributeList::Delete(int index, bool delObj)
{
     if((index < 0) || (index >= (int)fCount))
          return false;

     // Wskazik tymczasowy
     XTT_Attribute** tmp = new XTT_Attribute*[fCount-1];

     // Przepisywanie zawartosci
     int tmp_i = 0;
     for(unsigned int i=0;i<fCount;i++)
          if((int)i != index)
               tmp[tmp_i++] = fList[i];

     // Jezeli trzeba skasowac obiekt
     if(delObj)
     {
          // Najpierw natepuje kasowanie kolumn zawierajacych dany argument
          // Przeszukuje tabele
          for(int t=(int)TableList->Count()-1;t>=0;t--)
          {
               // Kasowanie z tabeli kolumn
               int colIndex = TableList->Table(t)->IndexOfAttributeID(fList[index]->Id());
               while(colIndex > -1)
               {
                    if(TableList->Table(t)->ColCount() > 1)
                    {
                         TableList->Table(t)->DeleteCol(colIndex);
                         colIndex = TableList->Table(t)->IndexOfAttributeID(fList[index]->Id());
                    }
                    
                    // Jezeli tabela nie ma kolumn to ja usuwamy
                    if(TableList->Table(t)->ColCount() == 1)
                    {
                         TableList->Delete(t);
                         colIndex = -1;
                    }
               }
          }
          delete fList[index];
     }

     // Kiedy lista nie jest pusta to nalezy tez skaswac liste wskaznikow
     if(fCount)
          delete [] fList;

     // Pomniejszenie rozmiaru pojemnika
     fCount--;

     // Zapisanie lokalizacji nowej tablicy
     fList = tmp;
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;

     // Jezeli tu jestesmy to wszystko ok
     return true;
}
//---------------------------------------------------------------------------

// #brief Function sorts list of attributes.
//
// #return No return value.
void XTT_AttributeList::sort(void)
{
     for(unsigned int i=0;i<fCount;i++)
     {
          for(unsigned j=0;j<fCount-i-1;j++)
          {
               if(fList[j]->Name() > fList[j+1]->Name())
               {
                    XTT_Attribute* tmp = fList[j];
                    fList[j] = fList[j+1];
                    fList[j+1] = tmp;
               }
          }
     }
}
//---------------------------------------------------------------------------

// #brief Function creates list of attribute names from the attribute list.
//
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return List of attribute names.
QStringList XTT_AttributeList::AttributeNameList(int __attType)
{
     QStringList res;
     for(unsigned int i=0;i<fCount;i++)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
          res << fList[i]->Name();
     }

     return res;
}
//---------------------------------------------------------------------------

// #brief Function creates acronym list of attributes.
//
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return Acronym list of attributes.
QStringList XTT_AttributeList::AttributeAcronymList(int __attType)
{
     QStringList res;
     for(unsigned int i=0;i<fCount;i++)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
          res << fList[i]->Acronym();
     }

     return res;
}
//---------------------------------------------------------------------------

// #brief Function that maps the attributes to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this attributes as string.
QString XTT_AttributeList::toProlog(int /*__option*/)
{
     QString result = "";
     for(unsigned int i=0;i<Count();++i)
          if(!fList[i]->isConceptual())
               result += fList[i]->toProlog() + "\n";
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Function clears container. The list of attributes is completely removed.
//
// #return No return value.
void XTT_AttributeList::Flush(void)
{
     while(fCount)
     {
          // Uzywamy tej metody, gdyz zalezy nam na tym aby dabc o sposob
          // usuwania zalezny od typu pojemnika a ta metoda o ot dba
          Delete(0);
     }
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool XTT_AttributeList::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     for(unsigned int i=0;i<fCount;i++)
          res = fList[i]->updateTypePointer(__old, __new) && res;
          
     return res;
}
//---------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool XTT_AttributeList::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int i=0;i<fCount;i++)
               res = fList[i]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(unsigned int i=0;i<fCount;i++)
               res = fList[i]->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function retrieves index of attribute that is relative to the name of attribute.
//
// #param __name - Name of attribute.
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return Index of attribute. If name is incorect function returns -1.
int XTT_AttributeList::IndexOfName(QString _name, int __attType)
{
     int result = -1;
     for(unsigned int i=0;i<fCount;i++)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
          
          result++;
          if(fList[i]->Name() == _name)
               return result;
     }

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of attribute that is relative to the name of acronym.
//
// #param _name Name of acronym. 
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return Index of attribute. If name is incorect function returns -1.
int XTT_AttributeList::IndexOfAcronym(QString _acronym, int __attType)
{
     int result = -1;
     for(unsigned int i=0;i<fCount;i++)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
          
          result++;
          if(fList[i]->Acronym() == _acronym)
               return result;
     }

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of attribute that is relative to the attribute id.
//
// #param __aID - Attribute id.
// #param __attType - the type of the attribute XTT_ATTRIBUTE_TYPE_CONCEPTUAL or/and XTT_ATTRIBUTE_TYPE_PHYSICAL
// #return Index of attribute. If name is incorect function returns -1.
int XTT_AttributeList::IndexOfID(QString __aID, int __attType)
{
     int result = -1;
     for(unsigned int i=0;i<fCount;i++)
     {
          if(!checkAttributeCompatibility(fList[i], __attType))
               continue;
               
          result++;
          if(fList[i]->Id() == __aID)
               return result;
     }

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function retrieves index of attribute that has the given pointer.
//
// #param __attr - pointer to the attribute
// #return Index of attribute. If pointer is incorect function returns -1.
int XTT_AttributeList::IndexOfPointer(XTT_Attribute* __attr)
{
     for(unsigned int i=0;i<fCount;i++)
          if(fList[i] == __attr)
               return (int)i;

     return -1;
}
//---------------------------------------------------------------------------

// #brief Function that checks if the given attribute has compatible abstraction level
//
// #param __attr - pointer to the attribute
// #param __attType - indicates the abstraction level of attribute
// #return true or false
bool XTT_AttributeList::checkAttributeCompatibility(XTT_Attribute* __attr, int __attType)
{
     if(__attr == NULL)
          return false;
     
     if((__attr->isConceptual()) && ((__attType & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) != 0))
          return true;
     if((!__attr->isConceptual()) && ((__attType & XTT_ATTRIBUTE_TYPE_PHYSICAL) != 0))
          return true;
          
     return false;
}
//---------------------------------------------------------------------------

// #brief Function that checks and if it detects errors fixes the acronyms of attributes
//
// #return no values return
void XTT_AttributeList::checkAndFixAttributeAcronyms(void)
{
     QStringList nms = AttributeNameList();
     QStringList acs = AttributeAcronymList();
     
     for(unsigned int i=0;i<Count();++i)
     {
          bool change = false;
          XTT_Attribute* att = fList[i];
          QString acr = att->Acronym();
          
          if(acr.length() > 5)
               change = true;
          
          int nms_iof = nms.indexOf(acr);
          int nms_cnt = nms.count(acr);
          if((nms_iof > -1) && (((unsigned int)nms_iof != i) || (nms_cnt > 1)))
               change = true;
               
          int acs_iof = acs.indexOf(acr);
          int acs_cnt = acs.count(acr);
          if((acs_iof > -1) && (((unsigned int)acs_iof != i) || (acs_cnt > 1)))
               change = true;
          
          if(!change)
               continue;
          
          att->setAcronym(AttributeGroups->createAcronym(att->Name(), this, this, i));
     }
}
//---------------------------------------------------------------------------

// #brief Function that translates the index in one abstraction to index in the another abstraction type
//
// #param __index - index of the attribute
// #param __inputAbstraction - input abstraction
// #param __outputAbstraction - output abstraction
// #return true or false
int XTT_AttributeList::indexTranslate(int __index, int __inputAbstraction, int __outputAbstraction)
{
     int gIndex = -1;    // general index
     int cIndex = -1;    // conceptual index
     int pIndex = -1;    // physical index
     
     if(__index >= (int)fCount)
          return -1;
     for(unsigned int i=0;i<fCount;++i)
     {
          gIndex++;
          cIndex++;
          if(!fList[i]->isConceptual())
               pIndex++;
          
          bool ret = false;
          ret = ret || (((__inputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) != 0) && ((__inputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) != 0) && (gIndex == __index));
          ret = ret || (((__inputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) != 0) && ((__inputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) == 0) && (cIndex == __index));
          ret = ret || (((__inputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) == 0) && ((__inputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) != 0) && (pIndex == __index));
          
          if(ret)
          {
               if(((__outputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) != 0) && ((__outputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) != 0))
                    return gIndex;
               if(((__outputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) != 0) && ((__outputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) == 0))
                    return cIndex;
               if(((__outputAbstraction & XTT_ATTRIBUTE_TYPE_CONCEPTUAL) == 0) && ((__outputAbstraction & XTT_ATTRIBUTE_TYPE_PHYSICAL) != 0))
                    return pIndex;
          }
     }
     
     return -1;
}
//---------------------------------------------------------------------------

// #brief Returns only those attrributes, with a given relation
//
// #param __relation - the value of the relation
// #return the list of only those attrributes, with a given relation
QList<XTT_Attribute*> XTT_AttributeList::classFilter(int __relation)
{
     QList<XTT_Attribute*> result;
     
     for(unsigned int i=0;i<fCount;++i)
     {
          if((!fList[i]->isConceptual()) && ((__relation == -1) || (fList[i]->Class() == (unsigned int)__relation)))
               result.append(fList[i]);
     }
     return result;
}
//---------------------------------------------------------------------------

// #brief Function retrieves next id for the attribute and increments current attribute.
//
// #return Next id for the attribute as String.
QString XTT_AttributeList::NextId(void)
{
     return AttributeGroups->createUniqeAttributeIdentifier();
}
//---------------------------------------------------------------------------

// #brief Function sets level of nesting.
//
// #param level Level of nesting.
// #return No return value.
void XTT_AttributeList::setLevel(unsigned int _level)
{
     fLevel = _level;

     // Jezeli jest to glowny poziom zagniezdzenia
     if(!fLevel)
     {
          // Automatycznie ustawiamy brak rodzicow
          fParentId = "";
     }
     
     // Zarejestrowanie modyfikacji obiektu
     fChanged = true;
}
//---------------------------------------------------------------------------

// #brief Function that returns if the attributes object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool XTT_AttributeList::attributesVerification(QString& __error_message)
{
     bool res = true;
     for(unsigned int i=0;i<fCount;i++)
          res = fList[i]->attributeVerification(__error_message) && res;
     return res;
}
//---------------------------------------------------------------------------

// #brief Static function that returns the prefix of the id.
//
// #return the prefix of the id as string.
QString XTT_AttributeList::idPrefix(void)
{
     return "agr_";
}
//---------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* XTT_AttributeList::out_HML_2_0(int /*__mode*/)
{
     QStringList* res = new QStringList;
     res->clear();
     
     QString init_line = "<agroup id=\"" + fId + "\"";
     init_line += "\" name=\"" + fName + "\"";
     init_line += ">";
     *res << init_line;
     
     if(!fDescription.isEmpty())
          *res << "\t<desc>" + fDescription + "</desc>";
     
     for(unsigned int i=0;i<fCount;++i)
          *res << "\t<attref ref=\"" + fList[i]->Id() + "\"/>";
     
     *res << "</agroup>";
     return res;
}
//---------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void XTT_AttributeList::out_HML_2_0(QXmlStreamWriter* _xmldoc, int /*__mode*/)
{
     _xmldoc->writeStartElement("agroup");
          _xmldoc->writeAttribute("id", fId);
          _xmldoc->writeAttribute("name", fName);
          
          if(!fDescription.isEmpty())
               _xmldoc->writeTextElement("desc", fDescription);

          for(unsigned int i=0;i<fCount;++i)
          {
               _xmldoc->writeEmptyElement("attref");
               _xmldoc->writeAttribute("ref", fList[i]->Id());
          }
     _xmldoc->writeEndElement();
}
#endif
//---------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int XTT_AttributeList::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "agroup";
     
     if(__root.tagName() != elementName)
          return 0;
     
     // Reading group data
     QString gid = __root.attribute("id", "");
     QString gname = __root.attribute("name", "");
     QString gparent = __root.attribute("parent", "");
     
     QDomElement DOMdesc = __root.firstChildElement("desc");
     if(!DOMdesc.isNull())
          fDescription = DOMdesc.text();
     
     if(gid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined group \'" + gname + "\' identifier." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
     }
     if(gname == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Unnamed group. Id = " + gid + "." + hqed::createDOMlocalization(__root), 1, 2);
          result++;
     }
     
     fId = gid;
     fName = gname;
     fParentId = gparent;

     // Reading attributes references
     QDomElement DOMaref = __root.firstChildElement("attref");
     while(!DOMaref.isNull())
     {
          QString aref = DOMaref.attribute("ref", "");
          
          if(aref == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined attribute reference in group \'" + gname + "\'." + hqed::createDOMlocalization(DOMaref), 0, 2);
               result++;
               DOMaref = DOMaref.nextSiblingElement("attref");
               continue;
          }

          XTT_Attribute* attr = AttributeGroups->FindAttribute(aref);
          if(attr == NULL)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Cannot find attribute. Misplaced reference to \'" + aref + "\' from \'" + gname + "\' group." + hqed::createDOMlocalization(DOMaref), 0, 2);
               result++;
               DOMaref = DOMaref.nextSiblingElement("attref");
               continue;
          }

          XTT_AttributeList* agroup = AttributeGroups->AttributeGroup(attr);
          if(agroup == NULL)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Cannot find attribute group. Misplaced reference to \'" + aref + "\' from \'" + gname + "\' group." + hqed::createDOMlocalization(DOMaref), 0, 2);
               result++;
               DOMaref = DOMaref.nextSiblingElement("attref");
               continue;
          }

          // Moving attribute
          int aindex = agroup->IndexOfPointer(attr);
          if(aindex == -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Cannot find attribute in the group." + hqed::createDOMlocalization(DOMaref), 0, 2);
               result++;
               DOMaref = DOMaref.nextSiblingElement("attref");
               continue;
          }
          agroup->Delete(aindex, false);
          Add(attr);
          
          DOMaref = DOMaref.nextSiblingElement("attref");
     }
     
     return result;
}
//---------------------------------------------------------------------------
