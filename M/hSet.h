 /**
 * \file	hSet.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008 
 * \version	1.15
 * \brief	This file contains class definition that represents the set of the XTT values
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HSETH
#define HSETH

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define H_SET_TRUE_VALUE                      1
#define H_SET_FALSE_VALUE                     0

#define H_SET_SORT_FROM_VALUE                 0
#define H_SET_SORT_TO_VALUE                   1
#define H_SET_SORT_POWER                      2
#define H_SET_SORT_ALPHABETICAL               3

#define H_SET_SORT_ASCENDING                  0  // rosnaco
#define H_SET_SORT_DESCEND                    1  // malejaco

#define H_SET_ERROR_NO_ERROR                  0
#define H_SET_OTHER_OBJECT_ERROR             -2
#define H_SET_ERROR_NOT_DEFINED_TYPE         -3
#define H_SET_ERROR_INCORRECT_DOMAIN_ITEM    -4
#define H_SET_ERROR_PARENT_TYPE_NOT_DEFINED  -5
#define H_SET_ERROR_NOT_DEFINED_SET_ITEM     -6
#define H_SET_ERROR_OUT_OF_DOMAIN            -7
#define H_SET_ERROR_SET_EMPTY                -8
// -----------------------------------------------------------------------------

class hValue;
class hType;
class hSetItem;
class XTT_Attribute;
// -----------------------------------------------------------------------------
/**
* \class 	hSet
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that represents the set of the XTT values
*/
class hSet : public QList<hSetItem*>
{
protected:
     
     hType* _parentType;           ///< The set type (all the values in this set must be compatible with this type)
     QString _lastError;           ///< The last error message
     
public:
     
     /// \brief Constructor hSet class.
     hSet(void);
     
     /// \brief Constructor hSet class.
     ///
     /// \param *__parentType - poiter to the type of data that the set can contain
     hSet(hType* __parentType);
     
     /// \brief Destructor hSet class.
     virtual ~hSet(void);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     virtual hSet* copyTo(hSet* __destinationItem = NULL);
     
     /// \brief Function that returns the pointer to the type of the set
     ///
     /// \return The pointer to the type of the set
     hType* parentType(void){ return _parentType; }
     
     /// \brief Function that sets the parent type of the set
     ///
     /// \param *__pt - poiter to the type
     /// \return true on succes, otherwise false
     virtual bool setParentType(hType* __pt);
     
     /// \brief Function that returns if the power of the set is one
     ///
     /// \return true if the power of the set is one, otherwise false
     bool isSingleValue(void);
     
     /// \brief Function that transforms the current object that contains a set of values to an object that contains a single value
     ///
     /// \return true on success otherwise false
     bool makeItSingle(void);
     
     /// \brief Function that sets the last error message
	///
     /// \param __lastError - the content of the error message
	/// \return No return value.
     void setLastError(QString __lastError);
     
     /// \brief Function that sets the last error message
     ///
     /// \param __errorId - the error ID (defined in '#define' section)
     /// \param __errorMsg - the additional error message
     /// \return The given error code.
     int setLastError(int __errorId, QString __errorMsg = "");
     
     /// \brief Function that returns the last error message
	///
	/// \return The last error message as string
     QString lastError(void){ return _lastError; }
     
     /// \brief Function that returns value from the set if the set is single valued, otherwise return nod defined operator
	///
	/// \return value from the set if the set is single valued, otherwise return nod defined operator
     QString toValue(void);
     
     /// \brief Function that returns the valued representation of the set
	///
	/// \return the valued representation of the set
     hSet* toVals(void);

     /// \brief Function that tries to match the current value to format that is required for this type
     ///
     /// \param __allowTrunc - denotes if the digiths can be cut
     /// \return The result code
     int toCurrentNumericFormat(bool __allowTrunc = false);
     
     /// \brief Function that returns the string representation of the set
        ///
     /// \param __forceSetParentheses - add set delimiters ("{}") to string representation even if set contains only one item
     /// \return The string representation of the set
     virtual QString toString(bool __forceSetParentheses = false);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);

     /// \brief Function that returns the drools5 representation of the set
     ///
     /// \return The drools5 representation of the set
     virtual QString toDrools5(void);
     
     /// \brief Function that returns the string list representation of the items
	///
	/// \return The string list representation of the items
     virtual QStringList stringItems(void);
     
     /// \brief The functions that adds the value to the set
     ///
     /// \param *__value - pointer to the value that should be added
     /// \return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
     virtual int add(hValue* __value);
     
     /// \brief The functions that adds the item to the set
     ///
     /// \param *__item - item that should be added
     /// \return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
     virtual int add(hSetItem* __item);
     
     /// \brief The functions that adds the item to the set
     ///
     /// \param __index - item position
     /// \param *__item - item that should be inserted
     /// \return no values return
     virtual void insertItem(int __index, hSetItem* __item);
     
     /// \brief The functions that removes the item with given index from the set
     ///
     /// \param __index - idenx of item that should be deleted
     /// \return true if __index is correct otherwise false
     virtual bool deleteItem(int __index);
     
     /// \brief The functions that removes all the items from the set
     ///
     /// \return No value return.
     void flush(void);
     
     /// \brief The functions that calculates the the sets substraction
     ///
     /// \param *__subtrahend - subtrahend set
     /// \return The set that is result of substraction
     hSet* sub(hSet* __subtrahend);
     
     /// \brief The functions that substract the item from the set
     ///
     /// \param *__subtrahend - subtrahend item
     /// \return The set that is result of substraction
     hSet* sub(hSetItem* __subtrahend);
     
     /// \brief The virtual function that return the index of the first item that contains given value
     ///
     /// \param __value - the value as string
     /// \return index of the first item that contains given value
     virtual int contain(QString __value);
     
     /// \brief The functions that checks if the item is in domain
     ///
     /// \param *__item - pointer to the item
     /// \return H_SET_TRUE_VALUE if item is in set otherwise H_SET_FALSE_VALUE. On error returns error code.
     virtual int contain(hSetItem* __item);
     
     /// \brief The functions that checks if the set __item is a sub set of this
     ///
     /// \param *__item - pointer to the set
     /// \return H_SET_TRUE_VALUE if item is a sub set of this set otherwise H_SET_FALSE_VALUE. On error returns error code.
     virtual int contain(hSet* __item);
     
     /// \brief The functions calculates the power of the set
     ///
     /// \param *ok - the pointer to the error code
     /// \return the number of items in current set
     quint64 power(int* ok = NULL);
     
     /// \brief The functions calculates the power of the item
     ///
     /// \param __index - item index
     /// \param *ok - the pointer to the error code
     /// \return the number of items in given item
     quint64 itemPower(int __index, int* ok = NULL);
     
     /// \brief The functions sorts the set
     ///
     /// \param __policy - the sorting criterions
     /// \param __direction - ascending, descent
     /// \return the result code
     int sort(int __policy, int __direction);
     
     /// \brief The functions swaps two items
     ///
     /// \param __index1 - index of the first item
     /// \param __index2 - index of the second item
     /// \return no values return
     virtual void swapItems(int __index1, int __index2);
     
     /// \brief The functions transforms the set to the more simple form. The new items has no intersections.
     ///
     /// \return result code
     int simplify(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);

     /// \brief Function that returns if the value is not defined
     ///
     /// \return true if yes otherwise false
     bool isNotDefinedValue(void);
     
     /// \brief Function that returns if the set contains only constant definitions of values
     ///
     /// \return true if yes otherwise false
     bool hasOnlyConstantValues(void);
     
     /// \brief Function that returns if the type object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     virtual bool setVerification(QString& __error_message);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - default - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	virtual QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - default - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	virtual void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief The functions returns the set with one item that has value equal to not_defined operator
     ///
     /// \param *__parentType - the parent type of the set
     /// \return pointer to the set object that conatains not_defined value
     static hSet* makeNotDefinedValue(hType* __parentType = NULL);
     
     /// \brief The functions returns the set with one item that has value equal to any operator
     ///
     /// \param *__parentType - the parent type of the set
     /// \return pointer to the set object that conatains any value
     static hSet* makeAnyValue(hType* __parentType = NULL);
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr);
};
// -----------------------------------------------------------------------------
#endif
