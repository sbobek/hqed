/*
 *	   $Id: hFunctionList.cpp,v 1.11.8.1 2011-03-15 22:10:38 lspas Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

// Headers of availible functions
#include "hFunctions/snglrslt/hsin.h"
#include "hFunctions/snglrslt/hcos.h"
#include "hFunctions/snglrslt/htan.h"
#include "hFunctions/snglrslt/hlog.h"
#include "hFunctions/snglrslt/habs.h"
#include "hFunctions/snglrslt/hpow.h"
#include "hFunctions/snglrslt/hadd.h"
#include "hFunctions/snglrslt/hsub.h"
#include "hFunctions/snglrslt/hmul.h"
#include "hFunctions/snglrslt/hdiv.h"
#include "hFunctions/snglrslt/hmod.h"
#include "hFunctions/snglrslt/hfac.h"
#include "hFunctions/snglrslt/hroot.h"
#include "hFunctions/snglrslt/hsetpow.h"
#include "hFunctions/snglrslt/hbsin.h"
#include "hFunctions/snglrslt/hbsnin.h"
#include "hFunctions/snglrslt/hbsless.h"
#include "hFunctions/snglrslt/hbsequal.h"
#include "hFunctions/snglrslt/hbslesseq.h"
#include "hFunctions/snglrslt/hbsnequal.h"
#include "hFunctions/snglrslt/hopassign.h"
#include "hFunctions/snglrslt/hopdontcare.h"
#include "hFunctions/snglrslt/hopaction.h"
#include "hFunctions/snglrslt/hbsgreater.h"
#include "hFunctions/snglrslt/hbsgreatereq.h"

#include "hFunctions/mltplrslt/hunion.h"
#include "hFunctions/mltplrslt/hexcept.h"
#include "hFunctions/mltplrslt/hintersec.h"
#include "hFunctions/mltplrslt/hcomplement.h"

#include "hFunctions/snglrslt/hbmsim.h"
#include "hFunctions/snglrslt/hbmnsim.h"
#include "hFunctions/snglrslt/hbmequal.h"
#include "hFunctions/snglrslt/hbmnequal.h"
#include "hFunctions/snglrslt/hbmsubset.h"
#include "hFunctions/snglrslt/hbmsupset.h"

#include "hFunction.h"
#include "hFunctionList.h"
// -----------------------------------------------------------------------------

hFunctionList* funcList;
// -----------------------------------------------------------------------------

// #brief Constructor hFunction class.
hFunctionList::hFunctionList(void)
{
}
// -----------------------------------------------------------------------------

// #brief Destructor hFunction class.
hFunctionList::~hFunctionList(void)
{
     flush();
}
// -----------------------------------------------------------------------------

// #brief Fills the list with the new instances of object of each function
//
// #return no value return
void hFunctionList::fill(void)
{
     // Creating functions object. Each object is added to this list automatically.

     // action operators
     new hopassign;
     new hopaction;
     new hopdontcare;

     // evaluation operators
     new hadd;
     new hsub;
     new hmul;
     new hdiv;
     new hmod;
     new hsin;
     new hcos;
     new htan;
     new hlog;
     new habs;
     new hfac;
     new hpow;
     new hroot;
     new hsetpow;

     new hunion;
     new hexcept;
     new hintersec;
     new hcomplement;

     // conditional operators
     new hbsless;
     new hbsequal;
     new hbmequal;
     new hbmnequal;
     new hbslesseq;
     new hbsnequal;
     new hbsgreater;
     new hbsgreatereq;

     new hbmsubset;
     new hbmsupset;
     new hbmsim;
     new hbmnsim;
     new hbsin;
     new hbsnin;
}
// -----------------------------------------------------------------------------

// #brief Function that removes all the items from the set
//
// #return no value return.
void hFunctionList::flush(void)
{
     for(;size();)
          delete takeFirst();
}
// -----------------------------------------------------------------------------

// #brief Function that search for function that has the given internal name
//
// #param __representation - the name of the function (unique)
// #param __representationType - the type of the function representation
// #return index in the list if the function exists, otherwise -1
int hFunctionList::findFunction(QString __representation, int __representationType)
{
     const int fcount = 5;
     QString (hFunction::*funcRepresentation[fcount])(void);
     
     funcRepresentation[0] = &hFunction::internalRepresentation;
     funcRepresentation[1] = &hFunction::xmlRepresentation;
     funcRepresentation[2] = &hFunction::prologRepresentation;
     funcRepresentation[3] = &hFunction::userRepresentation;
     funcRepresentation[4] = &hFunction::operatorRepresentation;
     
     if((__representationType < 0) || (__representationType >= fcount))
          return -1;
     
     for(int i=0;i<size();++i)
          if((at(i)->*funcRepresentation[__representationType])() == __representation)
               return i;

     return -1;
}
// -----------------------------------------------------------------------------

// #brief Function that appends the object
//
// #param hFunction* __new_function - poiter to the new object
// #return the result succes if such object does not exists, otherwise error
int hFunctionList::registerFunction(hFunction* __new_function)
{
     if(__new_function == NULL)
          return setLastError(FUNC_LIST_ERROR__NULL_POINTER, "Error while function registering.");
          
     if(findFunction(__new_function->internalRepresentation()) > -1)
          return setLastError(FUNC_LIST_ERROR__OBJECT_EXIST, "Can't register " + __new_function->internalRepresentation() + " function.");
          
     append(__new_function);
     
     return FUNC_LIST_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Function that search for function that are compatible with given parameters
//
// #param hType* __returned_type - the type that is returned by the function
// #param int multiplicity - denotes if the result should be single or multiple
// #param bool __create_copies - denotes if the function should create new instance of the function object
// #return the list of pointers to the function object that are compatible with given parameters
QList<hFunction*> hFunctionList::findFunctions(hType* __returned_type, int __multiplicity, bool __create_copies, const QList<int>& __arg_types)
{
     QList<hFunction*> result;
     
     int fcharacteristics = hFunction::typeFunctionRepresentation(__returned_type);
     for(int i=0;i<size();++i)
     {
          //QMessageBox::critical(NULL, "Error localization", "Func: " + at(i)->userRepresentation() + "\nChar: " + QString::number(at(i)->functionCharacterisitics(), 16) + "\neqchar: " + QString::number(fcharacteristics, 16) + "\neqmltplct: " + QString::number(multiplicity, 16), QMessageBox::Ok);
          if((at(i)->functionCharacterisitics() & fcharacteristics) && (at(i)->functionCharacterisitics() & __multiplicity))
          {
              bool compatible = true;

              for(int j=0;(j<__arg_types.size()) && (j<at(i)->argTypeConfCount());++j)
              {
                  if((__arg_types.at(j) & at(i)->argTypeConf(j)) == 0)
                  {
                        compatible = false;
                        break;
                  }
              }

              if(compatible)
              {
                  if(__create_copies)
                       result.append(at(i)->copyTo());
                  else
                       result.append(at(i));
              }
          }
     }
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns all defined operators
//
// #param __create_copies - denotes if the function should create new instance of the function object
// #return the list of pointers to the operators
QList<hFunction*> hFunctionList::findOperators(bool __create_copies)
{
    QList<hFunction*> result;

    for(int i=0;i<size();++i)
    {
        if(at(i)->isOperator())
        {
            if(__create_copies)
                result.append(at(i)->copyTo());
            else
                result.append(at(i));
        }
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param QString __error_content - last error message
// #return no return value
void hFunctionList::setLastError(QString __error_content)
{
     _lastError = __error_content;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param int __error_code - code of the error
// #param QString __error_desc - the additional error description
// #return returns the given error code
int hFunctionList::setLastError(int __error_code, QString __error_desc)
{
     QString complete_error_message = "";

     switch(__error_code)
     {
          case FUNC_LIST_ERROR__OBJECT_EXIST:
               complete_error_message = "The object is already exists.";
               break;
               
          case FUNC_LIST_ERROR__NULL_POINTER:
               complete_error_message = "The pointer is NULL.";
               break;
     }
     
     if(!__error_desc.isEmpty())
          complete_error_message += "\n" + __error_desc;
     setLastError(complete_error_message);
     
     return __error_code;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the last error message
//
// #return the last error message as string
QString hFunctionList::lastError(void)
{
     return _lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that search for function that has given internal or user representation
//
// #param __representation - the representation of the function
// #param __internal - denotes if given representation is internal or user friendly
// #return the index of the function. If the function does not exists return -1
int hFunctionList::indexOf(QString __representation, bool __internal)
{
     QString (hFunction::*res)(void);
     res = &hFunction::userRepresentation;
     if(__internal)
          res = &hFunction::internalRepresentation;
          
     for(int i=0;i<size();++i)
          if((at(i)->*res)() == __representation)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------
