/*
 *	   $Id: hintersec.cpp,v 1.2.4.4 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hintersec.h"
// -----------------------------------------------------------------------------

// #brief Constructor hintersec class.
hintersec::hintersec(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hintersec::setupFunction(void)
{
     _userRepresentation = QString::fromWCharArray(L"\u2229"); // UNICODE character: INTERSECTION
     _xmlRepresentation = "intersec";
     _prologRepresentation = "intersec";
     _drools5Representation = "intersec";
     _internalRepresentation = "intersec";
     
     _resultType = FUNCTION_TYPE__MULTI_VALUED;              
     _returnedTypes = /*FUNCTION_TYPE__BOOLEAN | */FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;
     _argTypesConf.append(/*FUNCTION_TYPE__BOOLEAN | */FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(FUNCTION_TYPE__MULTI_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);          
     setArgCount(2);     

     // Defining special names for arguments
     _argNames.append("set1 (req)");
     _argNames.append("set2 (req)");
     _argNames.append("set");
     refreshArgumentsNames();
     

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Function that calculates the intersection of two sets.<br>This function can used only with action context.<br>";
     _description += "<b>Returned type:</b> numeric, symbolic<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>set1</b> - the first set.<br>";
     _description += "<b>set2</b> - the second set.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the destination object
// #return pointer to the new instance of function object
hFunction* hintersec::copyTo(hFunction*)
{
     hintersec* dest = new hintersec;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hintersec::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hintersec::calculate(void)
{
     /*
     // Checking arguments
     if(arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_NOT_DEFINED);
     if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_ANY_VALUE);*/
     
     // The values of the first argument
     hMultipleValueResult mvr1 = _args->at(0)->value()->value();
     if(!(bool)mvr1)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + (QString)mvr1);
          return NULL;
     }
 
     // The values of the second argument
     hMultipleValueResult mvr2 = _args->at(1)->value()->value();
     if(!(bool)mvr2)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + (QString)mvr2);
          return NULL;
     }
     
     hSet* s1 = mvr1.toSet(false)->toVals();
     hSet* s2 = mvr2.toSet(false)->toVals();
     hSet* s3 = s1->sub(s2);
     hSet* rs = s1->sub(s3);
     
     // Checking if the set is correct
     if(_requiredType->isSetCorrect(rs, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "The result set: " + rs->toString() + " is incorrect.\nError message: " + _requiredType->lastError());
          delete rs;
          return NULL;
     }
     
     delete s1;
     delete s2;
     delete s3;

     return new hMultipleValue(rs);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hintersec::toString(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";
     
     QString res = "";
     
     res += _userRepresentation + "(";
     
     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toString();
          
     if((_args->at(0)->isVisible()) && (_args->at(1)->isVisible()))
          res += ",";
     
     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toString();
          
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hintersec::toProlog(int /*__option*/)
{
     QString res = "";
     
     res += _userRepresentation + "(";
     res += _args->at(0)->value()->toString() + "(";
     res += _args->at(1)->value()->toString() + "))";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hintersec::toDrools5(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";

     QString res = "";

     res += _drools5Representation + "(";

     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toDrools5();

     if((_args->at(0)->isVisible()) && (_args->at(1)->isVisible()))
          res += ",";

     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toDrools5();

     res += ")";

     return res;
}
// -----------------------------------------------------------------------------
