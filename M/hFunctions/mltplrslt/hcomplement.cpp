/*
 *	   $Id: hcomplement.cpp,v 1.2.4.2 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hcomplement.h"
// -----------------------------------------------------------------------------

// #brief Constructor hcomplement class.
hcomplement::hcomplement(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hcomplement::setupFunction(void)
{
     _userRepresentation = "complement";
     _xmlRepresentation = "complement";
     _prologRepresentation = "complement";
     _drools5Representation = "complement";
     _internalRepresentation = "complement";
     
     _resultType = FUNCTION_TYPE__MULTI_VALUED;              
     _returnedTypes = /*FUNCTION_TYPE__BOOLEAN | */FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;
     _argTypesConf.append(/*FUNCTION_TYPE__BOOLEAN | */FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(FUNCTION_TYPE__MULTI_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);          
     setArgCount(2);     

     // Defining special names for arguments
     _argNames.append("attribute (req)");
     _argNames.append("set (req)");
     _argNames.append("set");
     refreshArgumentsNames();
     

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Function that calculates the complement of a set to domain of the attribute.<br>This function can used only with action context.<br>";
     _description += "<b>Returned type:</b> numeric, symbolic<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>attribute</b> - the source of the domain according which the complementary set will be calculated.<br>";
     _description += "<b>set</b> - the set for which the complementary set will be calculated.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the destination object
// #return pointer to the new instance of function object
hFunction* hcomplement::copyTo(hFunction*)
{
     hcomplement* dest = new hcomplement;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hcomplement::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     // Checking the correctness of the first attribute
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\': The first argument must be defined as an attribute.");
          return false;
     }

     // Checking if the attribute filed has not the NULL value
     if(_args->at(0)->value()->attributeField() == NULL)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\': Undefined attribute definition in the first argument.");
          return false;
     }
     
     // Checking if the attribute filed has correct type
     if(_args->at(0)->value()->attributeField()->Type() == NULL)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\': Undefined type of attribute in the first argument.");
          return false;
     }
     
     // Checking if the attribute has domain
     if(_args->at(0)->value()->attributeField()->Type()->typeClass() != TYPE_CLASS_DOMAIN)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\': The type \'" + _args->at(0)->value()->attributeField()->Type()->name() + "\' has not defined domain.");
          return false;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hcomplement::calculate(void)
{
     /*
     // Checking arguments
     if(arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_NOT_DEFINED);
     if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_ANY_VALUE);*/
          
     // Checking if the fist argument is defined as attribute
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "The function \'" + _userRepresentation + "\' The first argument is not defined as an attribute.");
          return NULL;
     }
     
     // Checking if the attribute filed has not the NULL value
     if(_args->at(0)->value()->attributeField() == NULL)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "The function \'" + _userRepresentation + "\': Undefined attribute definition in the first argument.");
          return NULL;
     }
     
     // Checking if the attribute filed has correct type
     if(_args->at(0)->value()->attributeField()->Type() == NULL)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "\nThe function \'" + _userRepresentation + "\': Undefined type of attribute in the first argument.");
          return false;
     }
     
     // Checking if the attribute has domain
     if(_args->at(0)->value()->attributeField()->Type()->typeClass() != TYPE_CLASS_DOMAIN)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "\nThe function \'" + _userRepresentation + "\': The type \'" + _args->at(0)->value()->attributeField()->Type()->name() + "\' has not defined domain.");
          return false;
     }
 
     // The values of the second argument
     hMultipleValueResult mvr2 = _args->at(1)->value()->value();
     if(!(bool)mvr2)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + (QString)mvr2);
          return NULL;
     }
     
     hSet* s1 = _args->at(0)->value()->attributeField()->Type()->domain()->copyTo();
     hSet* s2 = mvr2.toSet(false)->toVals();
     hSet* rs = s1->sub(s2);
     
     // Checking if the set is correct
     if(_requiredType->isSetCorrect(rs, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "The result set: " + rs->toString() + " is incorrect.\nError message: " + _requiredType->lastError());
          delete rs;
          return NULL;
     }
     
     delete s1;
     delete s2;

     return new hMultipleValue(rs);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hcomplement::toString(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";
     
     QString res = "";
     
     res += _userRepresentation + "(";
     
     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toString();
          
     if((_args->at(0)->isVisible()) && (_args->at(1)->isVisible()))
          res += ",";
     
     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toString();
          
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hcomplement::toProlog(int /*__option*/)
{
     QString res = "";
     
     res += _userRepresentation + "(";
     res += _args->at(0)->value()->toString() + "(";
     res += _args->at(1)->value()->toString() + "))";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hcomplement::toDrools5(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";

     QString res = "";

     res += _drools5Representation + "(";

     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toDrools5();

     if((_args->at(0)->isVisible()) && (_args->at(1)->isVisible()))
          res += ",";

     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toDrools5();

     res += ")";

     return res;
}
// -----------------------------------------------------------------------------
