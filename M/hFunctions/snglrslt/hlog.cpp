/*
 *	   $Id: hlog.cpp,v 1.12.4.1 2010-12-19 23:36:36 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <math.h>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hlog.h"
// -----------------------------------------------------------------------------

// #brief Constructor hlog class.
hlog::hlog(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hlog::setupFunction(void)
{
     _userRepresentation = "log";
     _xmlRepresentation = "log";
     _prologRepresentation = "log";
     _drools5Representation = "Math.log";
     _internalRepresentation = "log";

     _resultType = FUNCTION_TYPE__SINGLE_VALUED;              
     _returnedTypes = FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC;                              
     _minArgCount = 1;                  
     _maxArgCount = 1;
     _argTypesConf.append(_returnedTypes);
     _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(1);      

     // Defining special names for arguments
     _argNames.append("value");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> The function computes the natural logarithm of an argument.<br>";
     _description += "<b>Returned type: </b>numeric<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "A domain error occurs is the argument is negative. A range error may occur if the argument is zero.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hlog::copyTo(hFunction*)
{
     hlog* dest = new hlog;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hlog::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hlog::calculate(void)
{
     double _result = 0.0;
     double d_arg_val;
     QString str_arg_val;
      
     // Checking value mutiplicity (this function requires that operation)
     if(_args->at(0)->valueMultiplicity() != FUNCTION_TYPE__SINGLE_VALUED)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument " + _args->at(0)->name() + " multiplicity.");
          return NULL;
     }

     hSet* argVal = _args->at(0)->value()->value().toSet(true);  // The value of the argument

     QString arg_value = argVal->toValue();       // Value if the argument

     // Checking for errors
     if(arg_value.contains(FUNCTION_ERROR_STRING))
     {
          setLastError(arg_value);
          return NULL;
     }

     // Checking arguments
     if(arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return new hMultipleValue(NOT_DEFINED, _requiredType);
     if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
          return new hMultipleValue(ANY_VALUE, _requiredType);

     // Getting value numeric representation
     if(_args->at(0)->value()->parentType()->mapToNumeric(arg_value, d_arg_val) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _args->at(0)->value()->parentType()->lastError());
          return NULL;
     }
     
     if(d_arg_val <= 0.)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Logarithm error: argument out of domain: argument value: " + QString::number(d_arg_val));
          return NULL;
     }

     _result = log(d_arg_val);
     str_arg_val = QString::number(_result, 'f', _requiredType->allowedDecimalLength());
     _requiredType->toCurrentNumericFormat(str_arg_val, true);

     if(_requiredType->mapFromNumeric(str_arg_val, str_arg_val, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _requiredType->lastError());
          return NULL;
     }
     
     // Checking if the set is correct
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(str_arg_val)));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hlog::toString(void)
{
     QString res = "";
     
     res += _userRepresentation + "(";
     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
          {
               res += _args->at(i)->value()->toString(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += ",";
          }
     }
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the prolog that is the user representation of the functions inculing arguments
//
// #param __option - options of the output
// #return the prolog that is the user representation of the functions inculing arguments
QString hlog::toProlog(int /*__option*/)
{
     QString res = "";
     
     res += _prologRepresentation + "(";
     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
          {
               res += _args->at(i)->value()->toProlog(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += ",";
          }
     }
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hlog::toDrools5(void)
{
     QString res = "";

     res += _drools5Representation + "(";
     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
          {
               res += _args->at(i)->value()->toDrools5(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += ",";
          }
     }
     res += ")";

     return res;
}
// -----------------------------------------------------------------------------
