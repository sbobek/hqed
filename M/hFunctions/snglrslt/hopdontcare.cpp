/*
 *	   $Id: hopdontcare.cpp,v 1.1.2.4 2011-03-18 11:26:51 kkr Exp $
 *
 *     Implementation by Łukasz Spas <coder89@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "../../../namespaces/ns_hqed.h"

#include "hopdontcare.h"
// -----------------------------------------------------------------------------

// #brief Constructor hadd class.
hopdontcare::hopdontcare(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hopdontcare::setupFunction(void)
{
     _userRepresentation = "_";
     _xmlRepresentation = "donotcare";
     _prologRepresentation = "_";
     _drools5Representation = "";
     _internalRepresentation = "void_dontcare_operator";
     _operatorRepresentation = "_";

     _isOperator = true;
     _operatorLeftAssociative = false;
     _operatorPrecedence = 3;

     _resultType = FUNCTION_TYPE__SINGLE_VALUED | FUNCTION_TYPE__MULTI_VALUED;
     _returnedTypes = FUNCTION_TYPE__VOID;
     _minArgCount = 1;
     _maxArgCount = 1;

     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(_resultType);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(1);

     // Defining special names for arguments
     _argNames.append("attribute");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Function that represents the information we do not care about the value of the <b>attribute</b>.<br>This function can be used only with action context.<br>";
     _description += "<b>Returned type:</b> void<br><br>";

     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>attribute</b> - the attribute assigned to the cell's column.<br>";
     //_description += "<b>attribute</b> - the attribute assigned to the cell's column. It is also equals to the first argument and it is <b>NOT</b> editable.<br>";

     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the destination object
// #return pointer to the new instance of function object
hFunction* hopdontcare::copyTo(hFunction*)
{
     hopdontcare * dest = new hopdontcare;
     hFunction::copyTo(dest);

     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hopdontcare::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     int fsa_res = hFunction::setArgument(__arg_index, __arg);
     if(fsa_res != FUNCTION_ERROR_SUCCESS)
          return fsa_res;

     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 0)
     {
          if(argCount() < 1)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
          _args->at(0)->setName("attribute");
     }

     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hopdontcare::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;

     // Checking the correctness of the first attribute
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\' error: lvalue error.\nThe first argument must be defined as an attribute.");
          return false;
     }

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hopdontcare::calculate(void)
{
     // Checking if the first argument has got an attribute type
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _userRepresentation + " error: the first argument is not defined as attribute.");
          return NULL;
     }

     return new hMultipleValue(hValue::toSet(_requiredType, new hValue));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopdontcare::toString(void)
{
     if(_args->size() < 1)
          return _internalRepresentation + " erorr: incorrect number of arguments.";

     QString res = "";
     res += _userRepresentation;

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hopdontcare::toProlog(int /*__option*/)
{
     return _prologRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopdontcare::toDrools5(void)
{
    return _drools5Representation;
}
// -----------------------------------------------------------------------------
