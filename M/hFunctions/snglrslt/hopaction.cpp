/*
 *	   $Id: hopaction.cpp,v 1.7.4.2 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hopaction.h"
// -----------------------------------------------------------------------------

// #brief Constructor hadd class.
hopaction::hopaction(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hopaction::setupFunction(void)
{
     _userRepresentation = "action";
     _xmlRepresentation = "action";
     _prologRepresentation = "action";
     _drools5Representation = "";
     _internalRepresentation = "void_action_operator";
     
     _resultType = FUNCTION_TYPE__SINGLE_VALUED | FUNCTION_TYPE__MULTI_VALUED;              
     _returnedTypes = FUNCTION_TYPE__ACTION;                              
     _minArgCount = 1;
     _maxArgCount = 2;
     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argTypesConf.append(FUNCTION_TYPE__UOSYMBL);
     _argMltplConf.append(_resultType);
     _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
     _additionalArgsType = _argTypesConf.at(1) | _argMltplConf.at(1);
     setArgCount(2);
     _minArgCount = 2;

     // Defining special names for arguments
     _argNames.append("attribute");
     _argNames.append("action");
     _argNames.append("action");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Function that allows for use the <b>some registered in PROLOG action</b>, or <b>PROLOG predicate</b>.<br>";
     _description += "<b>Returned type:</b> void<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>attribute</b> - attribute in action context. The function does not change any of the attribute features.<br>";
     _description += "<b>action</b> - the string that calls the <b>some registered in PROLOG action</b>, or <b>PROLOG predicate</b>.<br>";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the destination object
// #return pointer to the new instance of function object
hFunction* hopaction::copyTo(hFunction*)
{
     hopaction* dest = new hopaction;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hopaction::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     int fsa_res = hFunction::setArgument(__arg_index, __arg);
     if(fsa_res != FUNCTION_ERROR_SUCCESS)
          return fsa_res;
     
     
     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 0)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
          _args->at(0)->setName("attribute");
     }
     
     // When the second argument is going to be change by the user
     if(__arg_index == 1)
     {
          if(argCount() != 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
               
          hType* symbolicType = typeList->at(hpTypesInfo.iopti(SYMBOLIC_BASED));
          int argType = hFunction::typeFunctionRepresentation(symbolicType) | FUNCTION_TYPE__SINGLE_VALUED;
          _args->at(1)->setName("action");
          _args->at(1)->setType(argType);
          _args->at(1)->value()->setType(VALUE);
          _args->at(1)->compatibleTypes()->clear();
          _args->at(1)->compatibleTypes()->append(symbolicType);
     }

     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hopaction::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     // Checking the correctness of the first attribute
     return (_args->at(1)->value()->type() == VALUE);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hopaction::calculate(void)
{
     // Checking if the first argument has got an attribute type
     if(_args->at(1)->value()->type() != VALUE)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _userRepresentation + " error: the first argument is not defined as value.");
          return NULL;
     }
     
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopaction::toString(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";
     
     QString res = "";
     
     res += _userRepresentation + ": ";
     res += _args->at(1)->value()->toString();
     
     return res;
}
// -----------------------------------------------------------------------------


// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopaction::toDrools5(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";

     QString res = "";

     res += _drools5Representation;
     res += _args->at(1)->value()->toDrools5();

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hopaction::toProlog(int /*__option*/)
{
     QString res = _args->at(1)->value()->toString();
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hopaction::out_HML_2_0(int /*__mode*/)
{
     QStringList* res = new QStringList;
     res->clear();
     
     for(int i=1;i<argCount();++i)
     {
          QStringList* tmp = argument(i)->out_HML_2_0();
          for(int j=0;j<tmp->size();++j)
               *res << tmp->at(i);
          delete tmp;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void hopaction::out_HML_2_0(QXmlStreamWriter* _xmldoc, int /*__mode*/)
{
     for(int i=1;i<argCount();++i)
          argument(i)->out_HML_2_0(_xmldoc);
}
// -----------------------------------------------------------------------------
#endif

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hopaction::in_HML_2_0(QDomElement& /*__root*/, QString& /*__msg*/)
{
     return 0;
}
// -----------------------------------------------------------------------------
