/*
 *	   $Id: hsub.cpp,v 1.14.4.2 2012-02-26 18:17:37 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hExpression.h"

#include "hsub.h"
// -----------------------------------------------------------------------------

// #brief Constructor hsub class.
hsub::hsub(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hsub::setupFunction(void)
{
     _userRepresentation = "sub";
     _xmlRepresentation = "sub";
     _prologRepresentation = "-";
     _drools5Representation = "-";
     _internalRepresentation = "sub";
     _operatorRepresentation = "-";

     _isOperator = true;
     _operatorLeftAssociative = true;
     _operatorPrecedence = 12;

     
     _resultType = FUNCTION_TYPE__SINGLE_VALUED;              
     _returnedTypes = FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__OSYMBL;                              
     _minArgCount = 2;                  
     _maxArgCount = -1;
     _argTypesConf.append(_returnedTypes);
     _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(2);      

     // Defining special names for arguments
     _argNames.append("minuend (req)");
     _argNames.append("subtrahend (req)");
     _argNames.append("subtrahend");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> The function computes the difference of all arguments.<br>";
     _description += "<b>Returned type:</b> integer, numeric, symbolic (only ordered)<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "All of the arguments are subtrahends of subtraction. The type of each argument must be compatible with returned type.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hsub::copyTo(hFunction*)
{
     hsub* dest = new hsub;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hsub::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hsub::calculate(void)
{
     double _result = 0.0;
     double d_arg_val;
     QString str_arg_val;
    
     for(int i=0;i<argCount();++i)
     {   
          // Checking value mutiplicity (this function requires that operation)
          if(_args->at(i)->valueMultiplicity() != FUNCTION_TYPE__SINGLE_VALUED)
          {
               setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument " + _args->at(i)->name() + " multiplicity.");
               return NULL;
          }

          hSet* argVal = _args->at(i)->value()->value().toSet(true);  // The value of the argument

          QString arg_value = argVal->toValue();       // Value if the argument

          // Checking for errors
          if(arg_value.contains(FUNCTION_ERROR_STRING))
          {
               setLastError(arg_value);
               return NULL;
          }

          // Checking arguments
          if(arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
               return new hMultipleValue(NOT_DEFINED, _requiredType);
          if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
               return new hMultipleValue(ANY_VALUE, _requiredType);
    
          // Getting value numeric representation
          if(_args->at(i)->value()->parentType()->mapToNumeric(arg_value, d_arg_val) != H_TYPE_ERROR_NO_ERROR)
          {
               setLastError(QString(FUNCTION_ERROR_STRING) + _args->at(i)->value()->parentType()->lastError());
               return NULL;
          }

          if(i == 0)
               _result = d_arg_val;
          if(i > 0)
               _result -= d_arg_val;
     }

     if(_requiredType->mapFromNumeric(QString::number(_result, 'f', _requiredType->allowedDecimalLength()), str_arg_val, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _requiredType->lastError());
          return NULL;
     }
     
     // Checking if the set is correct
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(str_arg_val)));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hsub::toString(void)
{
    QString res = "";

    for(int i=0;i<argCount();++i)
    {
        if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
        {
            bool addParentheses = false;
            if(!_args->at(i)->value()->valueField()->isEmpty()) {
                addParentheses = _args->at(i)->value()->valueField()->at(0)->fromPtr()->type() == EXPRESSION &&
                        _args->at(i)->value()->valueField()->at(0)->fromPtr()->expressionField()->function()->userRepresentation() == userRepresentation();
            }

            if(addParentheses)
            {
                res += "(";
            }

            res += _args->at(i)->value()->toString();

            if(addParentheses)
            {
                res += ")";
            }

            if(i < (argCount()-1))
                res += "-";
        }
    }

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hsub::toProlog(int /*__option*/)
{
     QString res = "(";
     
     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
          {
               res += _args->at(i)->value()->toProlog(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += _prologRepresentation;
          }
     }
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hsub::toDrools5(void)
{
     QString res = "";

     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
          {
               res += _args->at(i)->value()->toDrools5(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += _drools5Representation;
          }
     }
     res += "";

     return res;
}
// -----------------------------------------------------------------------------
