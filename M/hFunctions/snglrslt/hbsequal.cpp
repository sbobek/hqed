/*
 *	   $Id: hbsequal.cpp,v 1.13.4.1 2010-12-19 23:36:36 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hbsequal.h"
// -----------------------------------------------------------------------------

// #brief Constructor hbsequal class.
hbsequal::hbsequal(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hbsequal::setupFunction(void)
{
     _userRepresentation = "=";
     _xmlRepresentation = "eq";
     _prologRepresentation = "eq";
     _drools5Representation = "=";
     _internalRepresentation = "bsequal";
     _operatorRepresentation = "=";

     _isOperator = true;
     _operatorLeftAssociative = true;
     _operatorPrecedence = 9;
     
     _resultType = FUNCTION_TYPE__SINGLE_VALUED;              
     _returnedTypes = FUNCTION_TYPE__BOOLEAN;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;
     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(2);      

     // Defining special names for arguments
     _argNames.append("lsv (req)");
     _argNames.append("rsv (req)");
     _argNames.append("rsv");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Compares two single values.<br>";
     _description += "<b>Returned type:</b> boolean<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "l(r)sv - left (right) single value. The base type of each argument must be the same.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hbsequal::copyTo(hFunction*)
{
     hbsequal* dest = new hbsequal;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hbsequal::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     int fsa_res = hFunction::setArgument(__arg_index, __arg);
     if(fsa_res != FUNCTION_ERROR_SUCCESS)
          return fsa_res;
     
     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 0)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
               
          _args->at(0)->setName("lsv (req)");
          _args->at(1)->setType(_args->at(0)->type());
          _args->at(1)->findCompatibleTypes(typeList);
          _args->at(1)->setValueParentType(_args->at(0)->value()->parentType(), true);
     }

     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 1)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
          _args->at(1)->setName("rsv (req)");
     }

     
     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hbsequal::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hbsequal::calculate(void)
{
     int _iresult;
     bool _result = true;
     double d_arg_val, d_prev_arg_val = 0.;
     QString str_prev_arg_val;
    
     for(int i=0;i<argCount();++i)
     {   
          // Checking value mutiplicity (this function requires that operation)
          if(_args->at(i)->valueMultiplicity() != FUNCTION_TYPE__SINGLE_VALUED)
          {
               setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument " + _args->at(i)->name() + " multiplicity. Must be single valued.");
               return NULL;
          }

          hSet* argVal = _args->at(i)->value()->value().toSet(true);  // The value of the argument
          QString arg_value = argVal->toValue();                      // Value if the argument
          delete argVal;

          // Checking for errors
          if(arg_value.contains(FUNCTION_ERROR_STRING))
          {
               setLastError(arg_value);
               return NULL;
          }

          // Checking arguments
          if((arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower()) || 
             (str_prev_arg_val.toLower() == XTT_Attribute::_not_definded_operator_.toLower()))
          {
               if(i == 0)
                    str_prev_arg_val = arg_value.toLower();
               _result = _result && (str_prev_arg_val == arg_value.toLower());
               continue;
          }
          if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
          {
               if((i == 0) ||     // The attribute cannot have value equal to ANY
                  (str_prev_arg_val == XTT_Attribute::_not_definded_operator_.toLower()))
                    _result = _result && false;
               _result = _result && true;
               continue;
          }
          
          // Storing type address
          hType* t = _args->at(i)->value()->parentType();
          
          // If the type is not constrainted sybolic
          if((t->baseType() == SYMBOLIC_BASED) && (t->typeClass() == NO_TYPE_CLASS))
          {
               if(i == 0)
               {
                    str_prev_arg_val = arg_value;
                    continue;
               }
               _result = _result && (str_prev_arg_val == arg_value);
          }
          
          // If the type is symbolic without ordered domain
          else if((t->baseType() == SYMBOLIC_BASED) && (t->typeClass() == TYPE_CLASS_DOMAIN) && (!t->domain()->ordered()))
          {
               if(t->mapToNumeric(arg_value, d_arg_val) != H_TYPE_ERROR_NO_ERROR)
               {
                    setLastError(QString(FUNCTION_ERROR_STRING) + t->lastError());
                    return NULL;
               }
               if(i == 0)
               {
                    str_prev_arg_val = arg_value;
                    continue;
               }
               _result = _result && (str_prev_arg_val == arg_value);
          }
          
          // Other cases
          else
          {
               if(t->mapToNumeric(arg_value, d_arg_val) != H_TYPE_ERROR_NO_ERROR)
               {
                    setLastError(QString(FUNCTION_ERROR_STRING) + t->lastError());
                    return NULL;
               }
               if(i == 0)
               {
                    d_prev_arg_val = d_arg_val;
                    continue;
               }
               _result = _result && (d_arg_val == d_prev_arg_val);
          }
     }
     
     _iresult = BOOLEAN_FALSE;
     if(_result)
          _iresult = BOOLEAN_TRUE;
     
     // Checking if the set is correct
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(QString::number(_iresult))));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hbsequal::toString(void)
{
     QString res = "";
     
     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
               res += _args->at(i)->value()->toString();
          if(i < (argCount()-1))
               res += " " + _userRepresentation + " ";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hbsequal::toProlog(int /*__option*/)
{
     QString res = "";
     
     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
               res += _args->at(i)->value()->toProlog();
          if(i < (argCount()-1))
               res += " " + _prologRepresentation + " ";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hbsequal::toDrools5(void)
{
     QString res = "";

     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
               res += _args->at(i)->value()->toDrools5();
          if(i < (argCount()-1))
               res += " " + _drools5Representation + " ";
     }

     return res;
}
// -----------------------------------------------------------------------------
