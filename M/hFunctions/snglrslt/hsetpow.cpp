/*
 *	   $Id: hsetpow.cpp,v 1.14.4.1 2010-12-19 23:36:36 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hsetpow.h"
// -----------------------------------------------------------------------------

// #brief Constructor hadd class.
hsetpow::hsetpow(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hsetpow::setupFunction(void)
{
     _userRepresentation = "setpower";
     _xmlRepresentation = "setpower";
     _prologRepresentation = "setpower";
     _drools5Representation = "setpower";
     _internalRepresentation = "setpower";
     
     _resultType = FUNCTION_TYPE__SINGLE_VALUED;              
     _returnedTypes = FUNCTION_TYPE__INTEGER;                              
     _minArgCount = 1;                  
     _maxArgCount = 1;
     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(FUNCTION_TYPE__MULTI_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(1);      

     // Defining special names for arguments
     _argNames.append("set");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> The function computes the power of the set.<br>";
     _description += "<b>Returned type:</b> integer<br><br>";                                                                
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>set</b> - set that power is calculated by this function. The type of the set's items is not important.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hsetpow::copyTo(hFunction*)
{
     hsetpow* dest = new hsetpow;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hsetpow::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hsetpow::calculate(void)
{
     quint64 _result = 0;
     QString str_arg_val;  

     // Checking arguments
     if(_args->at(0)->value()->type() == NOT_DEFINED)
          return new hMultipleValue(NOT_DEFINED, _requiredType);
     if(_args->at(0)->value()->type() == ANY_VALUE)
          return new hMultipleValue(ANY_VALUE, _requiredType);

     hMultipleValueResult mvr = _args->at(0)->value()->value();
     if(!(bool)mvr)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + (QString)mvr);
          return NULL;
     }

     int calc_result;
     _result = mvr.toSet(false)->power(&calc_result);
     if(calc_result != H_SET_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + mvr.toSet(false)->lastError());
          return NULL;
     }

     if(_requiredType->mapFromNumeric(QString::number(_result), str_arg_val, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _requiredType->lastError());
          return NULL;
     }

     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(str_arg_val)));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hsetpow::toString(void)
{
     QString res = "";
     
     res += _userRepresentation + "(";
     res += _args->at(0)->value()->toString(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hsetpow::toProlog(int /*__option*/)
{
     return "+ 0 +";
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hsetpow::toDrools5(void)
{
     QString res = "";

     res += _drools5Representation + "(";
     res += _args->at(0)->value()->toDrools5(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
     res += ")";

     return res;
}
// -----------------------------------------------------------------------------
