 /**
 * \file	hdiv.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	26.01.2009 
 * \version	1.0
 * \brief	This file contains class definition that represents the HeKatE function sub that divides values
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HDIV
#define HDIV
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QList>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif

#include "../../hFunction.h"
// -----------------------------------------------------------------------------

/**
* \class 	hdiv
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	9.09.2008 
* \brief 	Class definition that represents the HeKatE function add that divides values
*/
class hdiv : public hFunction
{
private:

     /// \brief Function that sets up the internal parameters of the function
     ///
     /// \return no value returns
     virtual void setupFunction(void);

public:
     
     /// \brief Constructor hdiv class.
     hdiv(void);
     
     /// \brief Function that creates the new instance of the function object
     ///
     /// \param hFunction* - poiter to the fdestination object
     /// \return pointer to the new instance of function object
     virtual hFunction* copyTo(hFunction*);
     
     /// \brief Function that returns if the function object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool functionVerification(QString& __error_message);
     
     /// \brief Function that returns the result of the fnction caculation. This is pure virtual function.
     ///
     /// \return the hMultipleValue object as the result of the caculation.
     virtual hMultipleValue* calculate(void);
     
     /// \brief Function that returns the string that is the user representation of the functions inculing arguments
     ///
     /// \return the string that is the user representation of the functions inculing arguments
     virtual QString toString(void);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     virtual QString toProlog(int __option = -1);

     /// \brief Function that maps the function to the prolog code.
     ///
     /// \return The prolog code that represents this function as string.
     virtual QString toDrools5(void);
};
// -----------------------------------------------------------------------------
#endif
