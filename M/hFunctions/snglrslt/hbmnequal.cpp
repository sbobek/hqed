/*
 *	   $Id: hbmnequal.cpp,v 1.11.4.4 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hbmnequal.h"
// -----------------------------------------------------------------------------

// #brief Constructor hbmnequal class.
hbmnequal::hbmnequal(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hbmnequal::setupFunction(void)
{
     _userRepresentation = "!="; //QString::fromWCharArray(L"\u2260"); // UNICODE character: NOT EQUAL TO
     _xmlRepresentation = "neq";
     _prologRepresentation = "neq";
     _drools5Representation = "!=";
     _internalRepresentation = "hbmnequal";
     _operatorRepresentation = "!=";

     //_isOperator = true;
     _operatorLeftAssociative = true;
     _operatorPrecedence = 9;

     _resultType = FUNCTION_TYPE__MULTI_VALUED;         
     _returnedTypes = FUNCTION_TYPE__BOOLEAN;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;                 
     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(FUNCTION_TYPE__MULTI_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(2);      

     // Defining special names for arguments
     _argNames.append("A");
     _argNames.append("B");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Checks if the sets A and B are not equal. The sets are <u>equal</u> if the set A contains all elements from B and the set B contains all elements from the set A. The power of the sets are <u>not</u> important.<br>";
     _description += "<b>Returned type:</b> boolean<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "The base type of each argument must be the same.";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hbmnequal::copyTo(hFunction*)
{
     hbmnequal* dest = new hbmnequal;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hbmnequal::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     int fsa_res = hFunction::setArgument(__arg_index, __arg);
     if(fsa_res != FUNCTION_ERROR_SUCCESS)
          return fsa_res;
     
     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 0)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
               
          // the type of the second argument must be the same as the first, but it multiplicity must be always equal to FUNCTION_TYPE__MULTI_VALUED
          int arg2type = ((~FUNCTION_TYPE__SINGLE_VALUED) & (_args->at(0)->type())) | (FUNCTION_TYPE__MULTI_VALUED);
          _args->at(0)->setName("A");
          _args->at(1)->setType(arg2type);
          _args->at(1)->findCompatibleTypes(typeList);
          _args->at(1)->setValueParentType(_args->at(0)->value()->parentType(), true);
     }

     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 1)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
          _args->at(1)->setName("B");
     }

     
     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hbmnequal::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hbmnequal::calculate(void)
{
     int _iresult = BOOLEAN_TRUE;
     bool _result = true;
     
     // Checking the number of arguments
     if(_args->size() != 2)
     {    
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " error: incorrect number of the arguments(" + QString::number(_args->size()) + ")");
          return NULL;
     }

     hSet* arg1Val = _args->at(0)->value()->value().toSet(true);  // The value of the argument
     hSet* arg2Val = _args->at(1)->value()->value().toSet(true);  // The value of the argument
     hSet* result1 = arg1Val->sub(arg2Val);
     hSet* result2 = arg2Val->sub(arg1Val);
     
     // when error during substracion
     if(result1 == NULL)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " error: " + arg1Val->lastError());
          delete arg1Val;
          delete arg2Val;
          return NULL;
     }
     if(result2 == NULL)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " error: " + arg2Val->lastError());
          delete arg1Val;
          delete arg2Val;
          return NULL;
     }
     
     _result = (result1->isEmpty() && result2->isEmpty());
     
     delete arg1Val;
     delete arg2Val;
     delete result1;
     delete result2;
     
     if(_result)
          _iresult = BOOLEAN_FALSE;
     
     // Checking if the set is correct
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(QString::number(_iresult))));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hbmnequal::toString(void)
{
     QString res = "";
     
     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
               res += _args->at(i)->value()->toString(true);
          if(i < (argCount()-1))
               res += " " + _userRepresentation + " ";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hbmnequal::toProlog(int /*__option*/)
{
     QString res = "";
     
     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
               res += _args->at(i)->value()->toProlog();
          if(i < (argCount()-1))
               res += " " + _prologRepresentation + " ";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hbmnequal::toDrools5(void)
{
     QString res = "";

     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
               res += _args->at(i)->value()->toDrools5();
          if(i < (argCount()-1))
               res += " " + _drools5Representation + " ";
     }

     return res;
}
// -----------------------------------------------------------------------------

