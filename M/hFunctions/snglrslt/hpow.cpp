/*
 *	   $Id: hpow.cpp,v 1.13.4.2 2012-02-26 18:17:37 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <math.h>

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "hExpression.h"

#include "hpow.h"
// -----------------------------------------------------------------------------

// #brief Constructor hpow class.
hpow::hpow(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hpow::setupFunction(void)
{
     _userRepresentation = "pow";
     _xmlRepresentation = "pow";
     _prologRepresentation = "**";
     _drools5Representation = "Math.pow";
     _internalRepresentation = "pow";
     _operatorRepresentation = "^";

     _isOperator = true;
     _operatorLeftAssociative = false;
     _operatorPrecedence = 14;
     
     _resultType = FUNCTION_TYPE__SINGLE_VALUED;              
     _returnedTypes = FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;
     _argTypesConf.append(_returnedTypes);
     _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(2);      

     // Defining special names for arguments
     _argNames.append("base");
     _argNames.append("exponent");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> The function computes \'base\' raised to the power \'exponent\'.<br>";
     _description += "<b>Returned type:</b> integer, numeric<br><br>";
     
     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "An error occurs if: <ul><li>\'base\' is negative and \'exponent\' is not an integral value.</li><li>the result cannot be represented when \'base\' is zero and \'exponent\' is less than or equal to zero.</li></ul>";
     
     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the fdestination object
// #return pointer to the new instance of function object
hFunction* hpow::copyTo(hFunction*)
{
     hpow* dest = new hpow;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hpow::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hpow::calculate(void)
{
     double _result = 0.0;
     double d_bse_val;
     double d_exp_val;
     QString str_bse_val;
     QString str_exp_val;
     QString str_arg_val;
     
     // checking number of arguments
     if(_args->size() != 2)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument count (" + QString::number(_args->size()) + ").");
          return NULL;
     }
      
     // Checking value mutiplicity (this function requires that operation)
     if(_args->at(0)->valueMultiplicity() != FUNCTION_TYPE__SINGLE_VALUED)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument " + _args->at(0)->name() + " multiplicity.");
          return NULL;
     }
     if(_args->at(1)->valueMultiplicity() != FUNCTION_TYPE__SINGLE_VALUED)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " calculation error: unsiutable argument " + _args->at(1)->name() + " multiplicity.");
          return NULL;
     }

     hSet* bseVal = _args->at(0)->value()->value().toSet(true);  // The value of the argument
     hSet* expVal = _args->at(1)->value()->value().toSet(true);  // The value of the argument

     QString bse_value = bseVal->toValue();       // Value if the argument
     QString exp_value = expVal->toValue();       // Value if the argument

     // Checking for errors
     if(bse_value.contains(FUNCTION_ERROR_STRING))
     {
          setLastError(bse_value);
          return NULL;
     }
     if(exp_value.contains(FUNCTION_ERROR_STRING))
     {
          setLastError(exp_value);
          return NULL;
     }

     // Checking arguments
     if((bse_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower()) || (exp_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower()))
          return new hMultipleValue(NOT_DEFINED, _requiredType);
     if((bse_value.toLower() == XTT_Attribute::_any_operator_.toLower()) || (exp_value.toLower() == XTT_Attribute::_any_operator_.toLower()))
          return new hMultipleValue(ANY_VALUE, _requiredType);

     // Getting value numeric representation
     if(_args->at(0)->value()->parentType()->mapToNumeric(bse_value, d_bse_val) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _args->at(0)->value()->parentType()->lastError());
          return NULL;
     }
     if(_args->at(1)->value()->parentType()->mapToNumeric(exp_value, d_exp_val) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _args->at(1)->value()->parentType()->lastError());
          return NULL;
     }
     
     if((d_bse_val < 0.) && (d_exp_val != (int)d_exp_val))
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " error: root of negative value(" + QString::number(d_bse_val) + ")");
          return NULL;
     }
     
     if((d_bse_val == 0.) && (d_exp_val < 0.))
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + "Function " + userRepresentation() + " error: result can not be represented. Divisor is equal zero.");
          return NULL;
     }

     _result = pow(d_bse_val, d_exp_val);
     str_arg_val = QString::number(_result, 'f', _requiredType->allowedDecimalLength());
     _requiredType->toCurrentNumericFormat(str_arg_val, true);

     if(_requiredType->mapFromNumeric(str_arg_val, str_arg_val, _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _requiredType->lastError());
          return NULL;
     }
     
     // Checking if the set is correct
     return new hMultipleValue(hValue::toSet(_requiredType, new hValue(str_arg_val)));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hpow::toString(void)
{
    QString res = "";

    for(int i=0;i<argCount();++i)
    {
        if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
        {
            bool addParentheses = false;
            if(!_args->at(i)->value()->valueField()->isEmpty()) {
                addParentheses = _args->at(i)->value()->valueField()->at(0)->fromPtr()->type() == EXPRESSION &&
                        _args->at(i)->value()->valueField()->at(0)->fromPtr()->expressionField()->function()->operatorPrecedence() < operatorPrecedence();
            }

            if(addParentheses)
            {
                res += "(";
            }

            res += _args->at(i)->value()->toString();

            if(addParentheses)
            {
                res += ")";
            }

            if(i < (argCount()-1))
                res += "^";
        }
    }

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hpow::toProlog(int /*__option*/)
{
     QString res = "(";
     
     for(int i=0;i<argCount();++i)
     {
          if(_args->at(i) != NULL)
               res += _args->at(i)->value()->toProlog();
          if(i < (argCount()-1))
               res += _prologRepresentation;
     }
     res += ")";
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hpow::toDrools5(void)
{
     QString res = "";

     res += _drools5Representation + "(";
     for(int i=0;i<argCount();++i)
     {
          if((_args->at(i) != NULL) && (_args->at(i)->isVisible()))
          {
               res += _args->at(i)->value()->toDrools5(); // + "(name: " + _args->at(i)->value()->parentType()->name() + ")";
               if(i < (argCount()-1))
                    res += ",";
          }
     }
     res += ")";

     return res;
}
// -----------------------------------------------------------------------------
