/*
 *	   $Id: hopassign.cpp,v 1.14.4.3 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../../XTT/XTT_Attribute.h"
#include "../../hSet.h"
#include "../../hType.h"
#include "../../hValue.h"
#include "../../hDomain.h"
#include "../../hSetItem.h"
#include "../../hFunction.h"
#include "../../hTypeList.h"
#include "../../hFunctionList.h"
#include "../../hMultipleValue.h"
#include "../../hFunctionArgument.h"

#include "../../../namespaces/ns_hqed.h"

#include "hopassign.h"
// -----------------------------------------------------------------------------

// #brief Constructor hadd class.
hopassign::hopassign(void)
{
     setupFunction();
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the internal parameters of the function
//
// #return no value returns
void hopassign::setupFunction(void)
{
     _userRepresentation = ":=";
     _xmlRepresentation = ":=";
     _prologRepresentation = "set";
     _drools5Representation = ":=";
     _internalRepresentation = "void_assign_operator";
     _operatorRepresentation = ":=";

     _isOperator = true;
     _operatorLeftAssociative = false;
     _operatorPrecedence = 3;

     _resultType = FUNCTION_TYPE__SINGLE_VALUED | FUNCTION_TYPE__MULTI_VALUED;              
     _returnedTypes = FUNCTION_TYPE__VOID;                              
     _minArgCount = 2;                  
     _maxArgCount = 2;

     _argTypesConf.append(FUNCTION_TYPE__BOOLEAN | FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__NUMERIC | FUNCTION_TYPE__UOSYMBL | FUNCTION_TYPE__OSYMBL);
     _argMltplConf.append(_resultType);
     _additionalArgsType = _argTypesConf.at(0) | _argMltplConf.at(0);
     setArgCount(2);      

     // Defining special names for arguments
     _argNames.append("attribute");
     _argNames.append("expression");
     _argNames.append("expression");
     refreshArgumentsNames();

     // Defining function description
     _description = "<b>Function name:</b> " + _userRepresentation + "<br>";
     _description += "<b>Function description:</b> Function that assings the value of the <b>expression</b> value to the <b>attribute</b>.<br>This function can used only with action context.<br>";
     _description += "<b>Returned type:</b> void<br><br>";

     _description += "<b>Minimum argument count:</b> " + QString::number(_minArgCount) + "<br>";
     if(_maxArgCount >= 0)
          _description += "<b>Maximum argument count:</b> " + QString::number(_maxArgCount) + "<br>";
     if(_maxArgCount < 0)
          _description += "<b>Maximum argument count:</b> unimportant<br>";
     _description += "<b>Current argument count:</b> " + QString::number(_minArgCount) + "<br>";
     _description += "<b>Argument description:</b><br>";
     _description += "<b>attribute</b> - the attribute assigned to the cell's column.<br>";
     _description += "<b>expression</b> - represents the expression, which calculated value is assgned to the <b>attribute</b>.";

     hFunction::registerFunction(funcList);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* - poiter to the destination object
// #return pointer to the new instance of function object
hFunction* hopassign::copyTo(hFunction*)
{
     hopassign* dest = new hopassign;
     hFunction::copyTo(dest);
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hopassign::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     int fsa_res = hFunction::setArgument(__arg_index, __arg);
     if(fsa_res != FUNCTION_ERROR_SUCCESS)
          return fsa_res;
     
     // When the first argument (here attribute) is going to be change by the user
     if(__arg_index == 0)
     {
          if(argCount() < 2)
               return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE);
          while(_argMltplConf.size() < 2)
          {
               _argMltplConf.append(FUNCTION_TYPE__SINGLE_VALUED);
          }
               
          _args->at(0)->setName("attribute");
          _argMltplConf[1] = _args->at(0)->value()->isSingle() ? FUNCTION_TYPE__SINGLE_VALUED : FUNCTION_TYPE__MULTI_VALUED;
          _args->at(1)->setType(_args->at(0)->type());
          _args->at(1)->findCompatibleTypes(typeList);
          _args->at(1)->setValueParentType(_args->at(0)->value()->parentType(), true);
     }

     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hopassign::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;

     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);

          if(_args->at(0)->value()->attributeField() == attr)
               res = (setArgument(0, _args->at(0)) == FUNCTION_ERROR_SUCCESS) && res;

          return hFunction::eventMessage(__errmsgs, __event, attr);
     }

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hopassign::functionVerification(QString& __error_message)
{
     if(!hFunction::functionVerification(__error_message))
          return false;
          
     // Checking if the first argument has got an attribute type
     if((_args->at(0)->value()->isSingle()) && (!_args->at(1)->value()->isSingle()))
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\' error: Incorrect multiplicity of the right value. You cannot assign multiple value to the single valued argument.");
          return false;
     }
     
     // Checking the correctness of the first attribute
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(__error_message += "\nThe function \'" + _userRepresentation + "\' error: lvalue error.\nThe first argument must be defined as an attribute.");
          return false;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the result of the fnction caculation. This is pure virtual function.
//
// #return the hSet object as the result of the caculation.
hMultipleValue* hopassign::calculate(void)
{
     /*
     // Checking arguments
     if(arg_value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_NOT_DEFINED);
     if(arg_value.toLower() == XTT_Attribute::_any_operator_.toLower())
          return new hMultipleValue(MULTIPLE_VALUE_TYPE_ANY_VALUE);*/
          
     // Checking if the first argument has got an attribute type
     if(_args->at(0)->value()->type() != ATTRIBUTE)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _userRepresentation + " error: the first argument is not defined as attribute.");
          return NULL;
     }
     
     /* This is checked in the tableVerification function */
     /*
     // Checking if the attribute is not read-only
     if(_args->at(0)->value()->attributeField()->Class() == XTT_ATT_CLASS_RO)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _userRepresentation + " error: the attribute \'" + _args->at(0)->value()->attributeField()->Name() + "\' is read only.");
          return NULL;
     }*/
 
     hMultipleValueResult mvr = _args->at(1)->value()->value();
     if(!(bool)mvr)
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + (QString)mvr);
          return NULL;
     }
     
     // Checking if the set is correct
     if(_args->at(0)->value()->attributeField()->Type()->isSetCorrect(mvr.toSet(false), _verifyResult) != H_TYPE_ERROR_NO_ERROR)
     {
          hSet* vals = mvr.toSet(false)->toVals();
          setLastError(QString(FUNCTION_ERROR_STRING) + "The result set: " + mvr.toSet(false)->toString() + " = " + vals->toString() + " is incorrect.\nError message: " + _args->at(0)->value()->attributeField()->Type()->lastError());
          delete vals;
          return NULL;
     }
     
     hMultipleValue* mv = new hMultipleValue(mvr.toSet(false)->toVals());
     if(!_args->at(0)->value()->attributeField()->checkValue(mv))
     {
          setLastError(QString(FUNCTION_ERROR_STRING) + _userRepresentation + " error: incorrect attribute \'" + _args->at(0)->value()->attributeField()->Name() + "\' value: " + mv->toString() + ".");
          return NULL;
     }
     _args->at(0)->value()->attributeField()->setValue(mv);
     delete mv;

     return new hMultipleValue(hValue::toSet(_requiredType, new hValue));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopassign::toString(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";
     
     QString res;

     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toString();
     
     res += " " + _userRepresentation + " ";
     
     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toString();
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the function to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this function as string.
QString hopassign::toProlog(int /*__option*/)
{
     QString res = "";
     QString lbrace = "";
     QString rbrace = "";
     
     // Checking if the first argument has got an attribute type
     if((_args->at(0)->value()->type() == ATTRIBUTE) && (_args->at(0)->value()->attributeField()->multiplicity()))
     {
          lbrace = "[";
          rbrace = "]";
     }
     
     if((_args->at(0) != NULL) && (_args->at(1) != NULL))
          res += _args->at(0)->value()->toProlog() + " " + _prologRepresentation + " " + lbrace + _args->at(1)->value()->toProlog() + rbrace;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the functions inculing arguments
//
// #return the string that is the user representation of the functions inculing arguments
QString hopassign::toDrools5(void)
{
     if(_args->size() != 2)
          return _internalRepresentation + " error: incorrect number of arguments.";

     QString res = "";

     res += _drools5Representation + " ";

     if(_args->at(0)->isVisible())
          res += _args->at(0)->value()->toDrools5();

     if((_args->at(0)->isVisible()) && (_args->at(1)->isVisible()))
          res += ",";

     if(_args->at(1)->isVisible())
          res += _args->at(1)->value()->toDrools5();

     return res;
}
// -----------------------------------------------------------------------------
