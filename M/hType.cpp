/*
 *	   $Id: hType.cpp,v 1.20.4.3.2.3 2011-06-13 17:02:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>
#if QT_VERSION >= 0x040300
     #include <QtScript/QScriptEngine>
#endif

#include <math.h>

#include "../C/Settings_ui.h"
#include "../C/XTT/widgets/ErrorsListWidget.h"
#include "../C/devPluginInterface/devPlugin.h"
#include "../C/devPluginInterface/devPluginController.h"
#include "../namespaces/ns_hqed.h"
#include "XTT/XTT_Attribute.h"
#include "hSet.h"
#include "hValue.h"
#include "hDomain.h"
#include "hSetItem.h"
#include "hMultipleValue.h"
#include "hType.h"
#include "hEcmaScript.h"
// -----------------------------------------------------------------------------

hPrimitiveTypesInfo hpTypesInfo;
// -----------------------------------------------------------------------------

// #brief Constructor hType class.
// #param __typeList - pointer to the type list in which this type is created
hType::hType(hTypeList* __typeList)
{
     initObject(__typeList);
}
// -----------------------------------------------------------------------------

// #brief Constructor hType class.
//
// #param int __class - identifer of the type class
// #param int __type - identifer of the primitive type
// #param __typeList - pointer to the type list in which this type is created
hType::hType(hTypeList* __typeList, int __class, int __type)
{
     initObject(__typeList);
     setClass(__class);
     setType(__type);
}
// -----------------------------------------------------------------------------

// #brief Destructor hType class.
hType::~hType(void)
{
     delete _domain;
}
// -----------------------------------------------------------------------------

// #brief Function sets the object properties to default
//
// #param __typeList - pointer to the type list in which this type is created
// #return no values return
void hType::initObject(hTypeList* __typeList)
{
     _class = NO_TYPE_CLASS;
     _id = "";
     _name = "";
     _description = "";
     _baseType = INTEGER_BASED;
     _length = 0;
     _scale = 0;
     _typeList = __typeList;
     _domain = new hDomain(this);
     
     _presentationSourceID = TYPE_PRESENTATION_ORIGIN;
     _presentationSourceName = "";
     _presentationSourceType = "";
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hType* hType::copyTo(hType* __destinationItem)
{
     hType* res = __destinationItem;
     if(res == NULL)
          res = new hType(_typeList);
          
     res->_class = _class;
     res->_id = _id;
     res->_name = _name;
     res->_description = _description;
     res->_presentationSourceID = _presentationSourceID;
     res->_presentationSourceName = _presentationSourceName;
     res->_presentationSourceType = _presentationSourceType;
     res->_baseType = _baseType;
     res->_length = _length;
     res->_scale = _scale;
     res->_domain->flush();
     _domain->copyToDomain(res->_domain);
     res->_domain->setParentType(res);
      
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the type. If the type is not constrained the name is "" the name generated automatically
//
// #return The name of the type as string.
QString hType::name(void)
{
     QString res = "";
     
     if((_class == NO_TYPE_CLASS) && (_name == ""))
          res = primitiveType(_baseType);
     
     if(_name != "")
          res = _name;

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the id of the presentation source
//
// #return the id of the presentation source
int hType::presentationSourceId(void)
{
     return _presentationSourceID;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the presentation source
//
// #return the name of the presentation source
QString hType::presentationSourceName(void)
{
     return _presentationSourceName;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the type of the presentation source
//
// #return the type of the presentation source
QString hType::presentationSourceType(void)
{
     return _presentationSourceType;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the ID of the type
//
// #param QString __id - new id of the type
// #return No value return
void hType::setID(QString __id)
{
     _id = __id;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the name of the type
//
// #param QString __name - new name of the type
// #return No value return
void hType::setName(QString __name)
{
     _name = __name;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the description of the type
//
// #param QString __description - new description of the type
// #return No value return
void hType::setDescription(QString __description)
{
     _description = __description;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the id of the presentation source
//
// #param __psrcid - new id of the presentation source
// #return No value return
void hType::setPresentationSourceId(int __psrcid)
{
     if((__psrcid != TYPE_PRESENTATION_ORIGIN) && 
        (__psrcid != TYPE_PRESENTATION_ECMA) && 
        (__psrcid != TYPE_PRESENTATION_DEVPC))
          return;

     _presentationSourceID = __psrcid;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the name of the presentation source
//
// #param __psrcname - new name of the presentation source
// #return No value return
void hType::setPresentationSourceName(QString __psrcname)
{
     _presentationSourceName = __psrcname;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the type of the presentation source
//
// #param __psrctype - new type of the presentation source
// #return No value return
void hType::setPresentationSourceType(QString __psrctype)
{
     _presentationSourceType = __psrctype;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of digithis before decimal symbol
//
// #return The number of digithis before decimal symbol
int hType::decimalSize(void)
{
     return _length - _scale;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param QString __lastError - the content of the error message
// #return No return value.
void hType::setLastError(QString __lastError)
{
     _lastError = __lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param int __errorId - the error ID (defined in #define section)
// #param QString __errorMsg - addiotional error message
// #return The given error code.
int hType::setLastError(int __errorId, QString __errorMsg)
{
     QString err_msgs[] = {"The type is unordered.", 
                           "Inconsistent type.",
                           "The value is out of range.",
                           "Incorrect number format. Check length and scale parameters.",
                           "Incorrect value type. The extreme value of range can not be equal to \'" + XTT_Attribute::_not_definded_operator_ + "\' or \'" + XTT_Attribute::_any_operator_ + "\'.",
                           "Conversion error. The value has incorrect numeric format.",
                           "The range extreme values are incorrect.",
                           "Cannot find the previous value.",
                           "Cannot perform request action because of " + XTT_Attribute::_not_definded_operator_ + " operator",
                           "Cannot perform request action because of " + XTT_Attribute::_any_operator_ + " operator",
                           __errorMsg};
                           
     switch(__errorId)
     {
          case H_TYPE_ERROR_TYPE_UNORDERED:
               setLastError(err_msgs[0] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_TYPE_INCONSISTENT:
               setLastError(err_msgs[1] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_VALUE_OUT_OF_RANGE:
               setLastError(err_msgs[2] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_INCORRECT_NUMBER_FORMAT:
               setLastError(err_msgs[3] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_INCORRECT_VALUE_TYPE:
               setLastError(err_msgs[4] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT:
               setLastError(err_msgs[5] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_INCOORECT_EXTREME_VALUES:
               setLastError(err_msgs[6] + __errorMsg);
               break;
               
          case H_TYPE_ERROR_PREVIOUS_VALUE_NOT_FOUND:
               setLastError(err_msgs[7] + __errorMsg);
               break;

          case H_TYPE_ERROR_NULL_VALUE:
               setLastError(err_msgs[8]);
               break;
               
          case H_TYPE_ERROR_ANY_VALUE:
               setLastError(err_msgs[9]);
               break;
               
          case H_TYPE_ERROR_OTHER_OBJECT_ERROR:
               setLastError(err_msgs[10]);
               break;

          default:
               setLastError("hType. Not defined error code (" + QString::number(__errorId) + ").\nAdditional error message: " + __errorMsg);
               break;
     }
     
     return __errorId;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the length field
//
// #param int __length - the new value of the field
// #return No return value.
void hType::setLength(int __length)
{
     if(__length <= _scale)
          _scale = __length-1;
     if(_scale < 0)
          _scale = 0;
          
     _length = __length;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the scale field
//
// #param int __scale - the new value of the field
// #return No return value.
void hType::setScale(int __scale)
{
     if((__scale >= _length) && (_length > 0))
          _length = __scale+1;
     _scale = __scale;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of digithis after decimal symbol.
//
// #return The number of digithis after decimal symbol as integer.
int hType::allowedDecimalLength(void)
{
     if(_length == 0)
          return Settings->defaultAccuracy();
          
     return _scale;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of important digiths before the decimal symbol
//
// #param QString __value - The value as string
// #return if the result is non negative - the number of digiths otherwise error code
int hType::decimalCount(QString __value)
{
     QChar dps = hqed::localSettings().decimalPoint();
     bool ok;
     double dval = __value.toDouble(&ok);
     if(!ok)
          return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, " Error while checking \'" + __value + "\' value against \'" + name() + "\' type. Error localization: hType::decimalCount(" + QString::number(__LINE__) + ")");

     dval = dval<0.?-dval:dval;
     __value = QString::number(dval, 'f', 6);
     
     int dec_smbl_pos = __value.indexOf(dps);   
     QString intval = __value.mid(0, dec_smbl_pos);
     int ival = intval.toInt();
     intval = QString::number(ival);
     
     return intval.length();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of digiths after the decimal symbol
//
// #param QString __value - The value as string
// #return if the result is non negative - the number of digiths otherwise error code
int hType::factorialCount(QString __value)
{
     QChar dps = hqed::localSettings().decimalPoint();
     int dec_smbl_pos = __value.indexOf(dps);
     
     int res = 0;
     
     // When the decimal point symbol does not exists
     if(dec_smbl_pos == -1)
          res = 0;
     
     if(dec_smbl_pos > -1)
     {
          bool ok;
          __value.toDouble(&ok);
          if(!ok)
               return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "Error localization: hType::factorialCount(" + QString::number(__LINE__) + ")");
               
          QString facval = __value.mid(dec_smbl_pos+1);
          
          while((facval.length()) && (facval[facval.length()-1] == '0'))
          {    
               facval.chop(1);
          }
          
          res = facval.length();
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the numeric value that is given as string has correct format according to _length and _scale fields
//
// #param QString __value - The value as string
// #return result code
int hType::numericValidator(QString __value)
{
     int int_len = decimalSize();
     int dc = decimalCount(__value);
     int fc = factorialCount(__value);

     // When error
     if(dc < 0)
          return dc;
     if(fc < 0)
          return fc;
          
     int res;
     if(_length == 0)                   // When length is zero then the fields: _length and _scale are ignored
          res = H_TYPE_ERROR_NO_ERROR;
     else if((dc <= int_len) && (fc <= _scale))
          res = H_TYPE_ERROR_NO_ERROR;
     else
          res = setLastError(H_TYPE_ERROR_TYPE_INCONSISTENT);
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the numeric value that is given as double has correct format according to _length and _scale fields
//
// #param double __value - The value as double
// #return result code
int hType::numericValidator(double __value)
{
     return numericValidator(QString::number(__value, 'f', 20));
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the numeric value that is given as integer has correct format according to _length and _scale fields
//
// #param int __value - The value as integer
// #return result code
int hType::numericValidator(int __value)
{
     return numericValidator(QString::number(__value));
}
// -----------------------------------------------------------------------------

// #brief Function that tries to match the current value to format that is required for this type
//
// #param QString& __value - the given value
// #param bool allowTrunc - denotes if the digiths can be cut
// #return The result code
int hType::toCurrentNumericFormat(QString& __value, bool allowTrunc)
{
     int int_len = decimalSize();
     int dc = decimalCount(__value);
     int fc = factorialCount(__value);
     bool negative = (__value.toDouble() < 0);
     
     // When error
     if(dc < 0)
          return dc;
     if(fc < 0)
          return fc;
          
 
     QChar dps = hqed::localSettings().decimalPoint();
     int dec_smbl_pos = __value.indexOf(dps);
     int split_index = dec_smbl_pos==-1?__value.length():dec_smbl_pos;
     
     QString dec_str = __value.mid(split_index-dc, dc);
     QString fac_str = __value.mid(split_index+1, fc);
     
     if(_length == 0)    // When length is zero then the fields: _length and _scale are ignored. We remove only the unimportant zero
     {
          if(dec_smbl_pos > -1)
          {
               while((fac_str.length() > 0) && (fac_str[fac_str.length()-1] == '0'))
               {
                    fac_str.chop(1);
               }
               __value = "";
               if(negative)
                    __value = "-";
               __value += dec_str;
               
               bool add_fac = (fac_str.length() > 0);       // Czesc ulakowa dodajemy jezeli istnieje
               add_fac = add_fac && ((baseType() == NUMERIC_BASED) || ((baseType() != NUMERIC_BASED) && (!allowTrunc)));     // oraz jest to liczba zmiennoprzecikowa, lub nie zmiennoprzecinkowa ale nie wolno obcinac
               if(add_fac)
                    __value = __value + QString(dps) + fac_str;
          }
          return numericValidator(__value);
     }

     // Adding neutral chars
     if(Settings->addNeutralZerosBefore())
     {
          while(dec_str.length() < int_len)
          {
               dec_str = dec_str.insert(0, "0");
          }
     }
     if(Settings->addNeutralZerosAfter())
     {
          while(fac_str.length() < _scale)
          {
               fac_str = fac_str + "0";
          }
     }
     
     // Removing chars
     while((allowTrunc) && (fac_str.length() > _scale))
     {
          fac_str.chop(1);
     }
     while((allowTrunc) && (dec_str.length() > int_len))
     {
          dec_str = dec_str.mid(1);
     }
     
     // The result form
     __value = "";
     if(negative)
          __value = "-";
     __value += dec_str;
     if(_scale > 0)
          __value = __value + QString(dps) + fac_str;
     
     return numericValidator(__value);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the numeric representation of the value (only in case of ordres domains). In case of integer and nueric types the result is equal to __value
//
// #param QString __value - The value as string
// #param double& __numValue - The numeric representation of the value. In case of integer and nueric types the result is equal to __value
// #param bool __check_constraints - if true, the value should satisfy the type constraints
// #return mapping result code.
int hType::mapToNumeric(QString __value, double& __numValue, bool __check_constraints)
{
     // Value conversion
     bool to_int_ok, to_num_ok;
     int ival = __value.toInt(&to_int_ok);
     double dval = __value.toDouble(&to_num_ok);
     
     if(__value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return setLastError(H_TYPE_ERROR_NULL_VALUE);
     if(__value.toLower() == XTT_Attribute::_any_operator_.toLower())
          return setLastError(H_TYPE_ERROR_ANY_VALUE);

     // If there is no constraints, the value can be returned only for numeric types and boolean type
     if(_class == NO_TYPE_CLASS)
     {
          if(_baseType == BOOLEAN_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nReason: value = " + __value);
               
               if((ival != BOOLEAN_FALSE) && (ival != BOOLEAN_TRUE))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               __numValue = (double)ival;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          if(_baseType == INTEGER_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nReason: value = " + __value);
               
               if((ival < NUMERIC_INFIMUM) || (ival > NUMERIC_SUPREMUM))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               __numValue = (double)ival;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == NUMERIC_BASED)
          {
               if(!to_num_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nReason: value = " + __value);
               
               if((dval < NUMERIC_INFIMUM) || (dval > NUMERIC_SUPREMUM))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               __numValue = dval;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == SYMBOLIC_BASED)
          {
               __numValue = 0;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else
               return setLastError(H_TYPE_ERROR_TYPE_INCONSISTENT);
     }
     
     // If the type is constrained with the help of domain
     if(_class == TYPE_CLASS_DOMAIN)
     {
          // This condition is currently commented because of it there is noe possible to check if the value, which is a candidate to unordered domain, belongs to this domain
          //if(!_domain->ordered())
          //     return setLastError(H_TYPE_ERROR_TYPE_UNORDERED);
               
          if(_baseType == INTEGER_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               int num_validation = numericValidator(__value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
               
               int dc_res = _domain->contain(__value);
               if((ival < NUMERIC_INFIMUM) || (ival > NUMERIC_SUPREMUM) || ((__check_constraints) && (dc_res == -1)))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               __numValue = (double)ival;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == NUMERIC_BASED)
          {
               if(!to_num_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               int num_validation = numericValidator(__value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
               
               if((dval < NUMERIC_INFIMUM) || (dval > NUMERIC_SUPREMUM) || ((__check_constraints) && (_domain->contain(__value) == -1)))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               __numValue = dval;
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == SYMBOLIC_BASED)
          {
               QString smbl_rep;
               bool elemExists;
               int domain_num = _domain->toNumericEquivalent(__value, elemExists);
               int domain_sym = mapFromNumeric(__value, smbl_rep);
               if((!elemExists) && (domain_sym != H_TYPE_ERROR_NO_ERROR))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __value + "\nError localization: hType::mapToNumeric(" + QString::number(__LINE__) + ")");
                    
               if(domain_sym == H_TYPE_ERROR_NO_ERROR)
                    __numValue = __value.toDouble();
               if(elemExists)
                    __numValue = (double)domain_num;
                    
               return H_TYPE_ERROR_NO_ERROR;
          }
     }
     
     // If the type is constrained with the groups
     if(_class == TYPE_CLASS_GROUP)
     {
          // Not supported
     }
     
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the numeric value (only in case of ordres domains). In case of integer and nueric types the result is equal to __num_value
//
// #param QString __numValue - The numeric representation of the value. In case of integer and nueric types the result is equal to __value
// #param QString __value - The reference to value as string
// #param bool __check_constraints - if true, the value should satisfy the type constraints
// #return mapping result code.
int hType::mapFromNumeric(QString __num_value, QString& __value, bool __check_constraints)
{
     bool to_int_ok, to_num_ok;
     double d__num_value = __num_value.toDouble(&to_num_ok);
     
     int i__num_value = (int)d__num_value; //__num_value.toInt(&to_int_ok);
     to_int_ok = (i__num_value == d__num_value) && (to_num_ok);
     
     // If there is no constraints, the value can be returned only for numeric types and booelan type
     if(_class == NO_TYPE_CLASS)
     {
          if(_baseType == BOOLEAN_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                   
               if((i__num_value != BOOLEAN_FALSE) && (i__num_value != BOOLEAN_TRUE))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               __value = QString::number(i__num_value);
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          if(_baseType == INTEGER_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               int num_validation = numericValidator(i__num_value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
                   
               if((i__num_value < NUMERIC_INFIMUM) || (i__num_value > NUMERIC_SUPREMUM))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               __value = QString::number(i__num_value);
               toCurrentNumericFormat(__value);
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == NUMERIC_BASED)
          { 
               if(!to_num_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               int num_validation = numericValidator(d__num_value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
                    
               if((d__num_value < (double)NUMERIC_INFIMUM) || (d__num_value > (double)NUMERIC_SUPREMUM))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
        
               __value = QString::number(d__num_value, 'f', allowedDecimalLength());
               toCurrentNumericFormat(__value);
               return H_TYPE_ERROR_NO_ERROR;
          }
     }
     
     // If the type is constrained with the help of domain
     else if(_class == TYPE_CLASS_DOMAIN)
     {
          if(_baseType == INTEGER_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nType: " + name() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                   
               int num_validation = numericValidator(i__num_value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
                    
               if((i__num_value < NUMERIC_INFIMUM) || (i__num_value > NUMERIC_SUPREMUM) || ((__check_constraints) && (_domain->contain(__num_value) == -1)))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                  
               __value = QString::number(i__num_value);
               toCurrentNumericFormat(__value);
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == NUMERIC_BASED)
          { 
               if(!to_num_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT);
                    
               int num_validation = numericValidator(__num_value);
               if(num_validation != H_TYPE_ERROR_NO_ERROR)
                    return num_validation;
               
               if((d__num_value < (double)NUMERIC_INFIMUM) || (d__num_value > (double)NUMERIC_SUPREMUM) || ((__check_constraints) && (_domain->contain(__num_value) == -1)))
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
        
               __value = QString::number(d__num_value, 'f', allowedDecimalLength());
               toCurrentNumericFormat(__value);
               return H_TYPE_ERROR_NO_ERROR;
          }
          
          else if(_baseType == SYMBOLIC_BASED)
          {
               if(!to_int_ok)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT);
                    
               //if((i__num_value < 0) || (i__num_value >= _domain->size()))
               //     return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __num_value + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               hValueResult hvr = _domain->fromNumericEquivalent(i__num_value);
               if(!(bool)hvr)
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nValue: " + __num_value + "\nError: " + (QString)hvr + "\nError localization: hType::mapFromNumeric(" + QString::number(__LINE__) + ")");
                    
               __value = (QString)hvr;
               return H_TYPE_ERROR_NO_ERROR;
          }
     }
       
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that translates the reference value with the help of plugin
//
// #param __refvalue - input value to translation
// #param __ok - reference to the value that contains the exit code: H_TYPE_ERROR_NO_ERROR on succes or other on error
// #param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
// #return translated value, or error if __ok != H_TYPE_ERROR_NO_ERROR
QString hType::translate(QString __refvalue, int& __ok, bool __isInput)
{
     __ok = H_TYPE_ERROR_NO_ERROR;
     if(_presentationSourceID == TYPE_PRESENTATION_ECMA)
     {
          QFile f(Settings->ecmascriptPath() + "/" + _presentationSourceName);
          return translateScript(f, __refvalue, __ok, __isInput);
     }
     if(_presentationSourceID == TYPE_PRESENTATION_DEVPC)
     {
          return translatePlugin(__refvalue, __ok);
     }
          
     return __refvalue;
}
// -----------------------------------------------------------------------------

// #brief Function that translates the given value using defined plugin
//
// #param __value - value to be translated
// #param __ok - indicates if the returned value is a result of a translation or error message
// #return translared value or error message
QString hType::translatePlugin(QString __value, int& __ok)
{
     // Searching for plugin
     int pi = devPC->indexOfName(_presentationSourceName);
     if(pi == -1)
     {
          __ok = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
          return "Cannot find presentation plugin \'" + _presentationSourceName + "\'.";
     }
     
     devPlugin* plugin = devPC->plugins()->at(pi);
     if(plugin->isEnabled())
     {
          __ok = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
          return "The plugin \'" + _presentationSourceName + "\' is disabled.";
     }
     if((plugin->configuration() & DEV_PLUGIN_SUPPORTED_MODULES_P) == 0)
     {
          __ok = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
          return "The plugin \'" + _presentationSourceName + "\' does not support presentation module.";
     }
     
     // Running plugin
     int result;
     QString strresult = plugin->valueTranslation(_presentationSourceType, __value, result);
     __ok = result == DEV_PLUGIN_RESULT_CODE_SUCCESS ? H_TYPE_ERROR_NO_ERROR : H_TYPE_ERROR_OTHER_OBJECT_ERROR;
     
     return strresult;
}
// -----------------------------------------------------------------------------

// #brief Function that returns previous value that is compatible with type
//
// #param QString __ref_value - the reference value
// #param QString& __result - the symbolic representation of previous value
// #param bool __asNumeric - denotes if the result should be returned as numeric representation
// #return result code.
int hType::typePreviousValue(QString __ref_value, QString& __result, bool __asNumeric)
{
     // Conversion to the numeric values
     double d__ref_value;
     
     int mtn = mapToNumeric(__ref_value, d__ref_value);
     if(mtn != H_TYPE_ERROR_NO_ERROR)
          return setLastError(mtn, "\nCan not establish the previous value.");
          
     if(_baseType == INTEGER_BASED)
     {
          int pv = (int)d__ref_value;
          __result = QString::number(pv-1);
          if(!__asNumeric)
               toCurrentNumericFormat(__result);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     else if(_baseType == NUMERIC_BASED)
     {
          double pv = d__ref_value;
          pv -= pow(10, -allowedDecimalLength());
          __result = QString::number(pv, 'f', allowedDecimalLength());
          if(!__asNumeric)
               toCurrentNumericFormat(__result);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     else if(_baseType == SYMBOLIC_BASED)
     {
          bool ok;
          int i__num_value = (int)d__ref_value;
          int i__prv_value = _domain->prevNumericValue(i__num_value, ok);

          if(!ok)
               return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE);
               
          if(!__asNumeric)
          {
               hValueResult hvr = _domain->fromNumericEquivalent(i__prv_value);
               if(!(bool)hvr)
                    return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               __result = (QString)hvr;
          }
          if(__asNumeric)
               __result = QString::number(i__prv_value);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns next value that is compatible with type
//
// #param QString __ref_value - the reference value
// #param QString& __result - the symbolic representation of next value
// #param bool __asNumeric - denotes if the result should be returned as numeric representation
// #return result code.
int hType::typeNextValue(QString __ref_value, QString& __result, bool __asNumeric)
{
     // Conversion to the numeric values
     double d__ref_value;
     
     int mtn = mapToNumeric(__ref_value, d__ref_value);
     if(mtn != H_TYPE_ERROR_NO_ERROR)
          return setLastError(mtn, "\nCan not establish the next value.");
          
     if(_baseType == INTEGER_BASED)
     {
          int pv = (int)d__ref_value;
          __result = QString::number(pv+1);
          if(!__asNumeric)
               toCurrentNumericFormat(__result);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     else if(_baseType == NUMERIC_BASED)
     {
          double pv = d__ref_value;
          pv += pow(10, -allowedDecimalLength());
          __result = QString::number(pv, 'f', allowedDecimalLength());
          if(!__asNumeric)
               toCurrentNumericFormat(__result);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     else if(_baseType == SYMBOLIC_BASED)
     {
          bool ok;
          int i__num_value = (int)d__ref_value;
          int i__nxt_value = _domain->nextNumericValue(i__num_value, ok);

          if(!ok)
               return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE);
               
          if(!__asNumeric)
          {
               hValueResult hvr = _domain->fromNumericEquivalent(i__nxt_value);
               if(!(bool)hvr)
                    return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               __result = (QString)hvr;
          }
          if(__asNumeric)
               __result = QString::number(i__nxt_value);
          return H_TYPE_ERROR_NO_ERROR;
     }
     
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the default value that is compatible with type
//
// #return pointer to the value.
hMultipleValue* hType::typeDefaultValue(void)
{
     QString str_val = "";
     
     // When no contraints
     if(_class == NO_TYPE_CLASS)
     {
          if(_baseType == BOOLEAN_BASED)
               str_val = QString::number(BOOLEAN_FALSE);
          if((_baseType == INTEGER_BASED) || (_baseType == NUMERIC_BASED))
          {
               str_val = "0";
               if(toCurrentNumericFormat(str_val, true) != H_TYPE_ERROR_NO_ERROR)
                    str_val = XTT_Attribute::_not_definded_operator_;
          }
          if(_baseType == SYMBOLIC_BASED)
               str_val = "";
     }
     
     // when there are contraints
     if(_class == TYPE_CLASS_DOMAIN)
     {
          if(_domain->size() == 0)
               str_val = XTT_Attribute::_not_definded_operator_;
          if(_domain->size() > 0)
          {
               hValueResult hvr = _domain->at(0)->value();
               if(!(bool)hvr)
                    str_val = XTT_Attribute::_not_definded_operator_;
               if((bool)hvr)
                    str_val = (QString)hvr;
          }
     }
     
     if(_class == TYPE_CLASS_GROUP)
     {
          // not suported yet
     }
     
     hValue* __value = new hValue(str_val);
     return new hMultipleValue(this, __value, true);
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the type identifier is correct
//
// #param The candidate type identifier
// #return true if the identifier is correct
bool hType::isTypeCorrect(int __ct)
{
     bool res;
     switch(__ct)
     {
          case BOOLEAN_BASED:
               res = true;
               break;
               
          case INTEGER_BASED:
               res = true;
               break;
               
          case NUMERIC_BASED:
               res = true;
               break;
               
          case SYMBOLIC_BASED:
               res = true;
               break;
               
          case VOID_BASED:
               res = true;
               break;
               
          case ACTION_BASED:
               res = true;
               break;
               
          default:
               res = false;
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the class of the type is correct
//
// #param The candidate class identifier
// #return true if the identifier is correct
bool hType::isClassCorrect(int __cc)
{
     bool res;
     switch(__cc)
     {
          case NO_TYPE_CLASS:
               res = true;
               break;
               
          case TYPE_CLASS_DOMAIN:
               res = true;
               break;
               
          case TYPE_CLASS_GROUP:
               res = true;
               break;

          default:
               res = false;
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the set is correct
//
// #param hSet* __set - pointer to the set object
// #param bool __checkConstraints - denotes if value should be compatible with constraints
// #return 0 on success otherwise error code
int hType::isSetCorrect(hSet* __set, bool __checkConstraints)
{
     for(int i=0;i<__set->size();++i)
     {
          int iicrc = isItemCorrect(__set->at(i), __checkConstraints);          // IsItemCorrectResultCode
          if(iicrc != H_TYPE_ERROR_NO_ERROR)
               return iicrc;
     }

     // If error does not exists the return succes code
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the item is correct
//
// #param hSetItem* __item - pointer to the item
// #param bool __checkConstraints - denotes if value should be compatible with constraints
// #return 0 on success otherwise error code
int hType::isItemCorrect(hSetItem* __item, bool __checkConstraints)
{
     return isItemCorrect(__item->type(), __item->fromPtr(), __item->toPtr(), __checkConstraints);
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the values are correct
//
// #param int __type - type of the item
// #param hValue* __from - from value
// #param hValue* __to - to value
// #param bool __checkConstraints - denotes if value should be compatible with constraints
// #return 0 on success otherwise error code
int hType::isItemCorrect(int __type, hValue* __from, hValue* __to, bool __checkConstraints)
{
     // Each single value can have one of two sepcial values: "not defined value" and "any value"
     if((__type == SET_ITEM_TYPE_SINGLE_VALUE) && ((__from->type() == NOT_DEFINED) || (__from->type() == ANY_VALUE)))  
          return H_TYPE_ERROR_NO_ERROR;

     // Checking if numeric values are not out of INFIMUM and SUPREMUM
     if(_class == NO_TYPE_CLASS)
     {
          if((baseType() == INTEGER_BASED) || (baseType() == NUMERIC_BASED))
          {
               double mnum;
               if(!__from->isAVRused())
               {
                    hValueResult hvr = __from->value();
                    if(!(bool)hvr)
                         return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);

                    if(mapToNumeric((QString)hvr, mnum, false) != H_TYPE_ERROR_NO_ERROR)
                         return H_TYPE_ERROR_OTHER_OBJECT_ERROR;
               }
                    
               if(__type == SET_ITEM_TYPE_RANGE)
               {
                    if(!__to->isAVRused())
                    {
                         hValueResult htr = __to->value();
                         if(!(bool)htr)
                              return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)htr);
                         if(mapToNumeric((QString)htr, mnum, false) != H_TYPE_ERROR_NO_ERROR)
                              return H_TYPE_ERROR_OTHER_OBJECT_ERROR;
                    }

                    // Checking for the item correctness
                    if(__checkConstraints)
                    {
                         hSet *tmp = new hSet(this);
                         hSetItem* si = new hSetItem(tmp, __type, __from, __to, true);
                         tmp->add(si);

                         // Checking the item correctness
                         if(si->typeVerification() != SET_ITEM_SUCCESS_VALUE)
                         {
                              QString le = si->lastError();
                              delete tmp;
                              return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, le);
                         }
                    }
               }
          }
     }
     // If domain
     if(_class == TYPE_CLASS_DOMAIN)
     {
          if(__from->isAVRused())
               return H_TYPE_ERROR_NO_ERROR;
               
          if((baseType() == INTEGER_BASED) || (baseType() == NUMERIC_BASED))
          {
               hValueResult hvr = __from->value();

               if(!(bool)hvr)
                    return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);

               if(numericValidator((QString)hvr) != H_TYPE_ERROR_NO_ERROR)
                    return setLastError(H_TYPE_ERROR_INCORRECT_NUMBER_FORMAT, "\nError has been caused by value: " + (QString)hvr + ".");
          }
          if(__type == SET_ITEM_TYPE_RANGE)
          {
               if(__to->isAVRused())
                    return H_TYPE_ERROR_NO_ERROR;
          
               // The range can have any and not defined operator
               if((__from->type() == NOT_DEFINED) || (__from->type() == ANY_VALUE) || (__to->type() == NOT_DEFINED) || (__to->type() == ANY_VALUE))
                    return setLastError(H_TYPE_ERROR_INCORRECT_VALUE_TYPE);
               
               if((baseType() == INTEGER_BASED) || (baseType() == NUMERIC_BASED))
               {
                    hValueResult hvr1 = __from->value(),
                                 hvr2 = __to->value();
                                 
                    if(!(bool)hvr1)
                         return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr1);
                    if(!(bool)hvr2)
                         return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr2);
                    
                    double v1_num_map, v2_num_map;
                    bool v1_cnv_res, v2_cnv_res;
                    v1_num_map = ((QString)hvr1).toDouble(&v1_cnv_res);
                    v2_num_map = ((QString)hvr2).toDouble(&v2_cnv_res);
                    
                    if(numericValidator((QString)hvr2) != H_TYPE_ERROR_NO_ERROR)
                         return setLastError(H_TYPE_ERROR_INCORRECT_NUMBER_FORMAT, "\nError has been caused by value: " + (QString)hvr2 + ".");
                    
                    if(!v1_cnv_res)
                         return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nError has been caused by value: " + (QString)hvr1 + ".");
                    if(!v2_cnv_res)
                         return setLastError(H_TYPE_ERROR_INCORRECT_NUMERIC_FORMAT, "\nError has been caused by value: " + (QString)hvr2 + ".");
                    
                    if(v1_num_map >= v2_num_map)
                         return setLastError(H_TYPE_ERROR_INCOORECT_EXTREME_VALUES);
               }
               
               if(baseType() == SYMBOLIC_BASED)
               {
                    hValueResult hvr1 = __from->value(),
                                 hvr2 = __to->value();
                                 
                    if(!(bool)hvr1)
                         return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr1);
                    if(!(bool)hvr2)
                         return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, (QString)hvr2);
                         
                    double v1_num_map, v2_num_map;
                    int from_map_res = mapToNumeric((QString)hvr1, v1_num_map);
                    int to_map_res = mapToNumeric((QString)hvr2, v2_num_map);
                    if(from_map_res != H_TYPE_ERROR_NO_ERROR)
                    {
                         setLastError("Value mapping error.\nError has been casused by value: " + (QString)hvr1 + "\nError message: " + lastError());
                         return from_map_res;
                    }
                    if(to_map_res != H_TYPE_ERROR_NO_ERROR)
                    {
                         setLastError("Value mapping error.\nError has been casused by value: " + (QString)hvr2 + "\nError message: " + lastError());
                         return to_map_res;
                    }
                    
                    if(v1_num_map >= v2_num_map)
                         return setLastError(H_TYPE_ERROR_INCOORECT_EXTREME_VALUES);
               }
          }
          
          // Checking if the value belongs to the domain
          if(__checkConstraints)
          {
               // Not defined yes becasuse of a lack of the required functions
               // Treba od itema odjac domain - jezeli zbior pusty to ok
               hSet *tmp = new hSet(this), *result;
               hSetItem* si = new hSetItem(tmp, __type, __from, __to, true);
               tmp->add(si);
               QString str_si = tmp->toString();

               // Checking the item correctness
               if(si->typeVerification() != SET_ITEM_SUCCESS_VALUE)
               {
                    QString le = si->lastError();
                    delete tmp;
                    return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, le);
               }
               
               result = tmp->sub(_domain);
               if(result == NULL)
               {
                    QString le = tmp->lastError();
                    delete tmp;
                    return setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, le);
               }
               
               bool empty = result->isEmpty();
               QString str_result = result->toString();
               
               delete tmp;
               delete result;
               
               if(!empty)
                    return setLastError(H_TYPE_ERROR_VALUE_OUT_OF_RANGE, "\nType: " + name() + "\nDomain: " + _domain->toString() + "\nCandidate: " + str_si + "\nIncorrect value(s): " + str_result);
          }
     }
     
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that tries to setup the baseType of this object
//
// #param The candidate type identifier
// #return true if the identifier is correct
bool hType::setType(int __nt)
{
     if(!isTypeCorrect(__nt))
          return false;
          
     _baseType = __nt;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that tries to setup the class of this object
//
// #param int __nc - The candidate class identifier
// #return true if the identifier is correct
bool hType::setClass(int __nc)
{
     if(!isClassCorrect(__nc))
          return false;
          
     _class = __nc;
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hType::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     if(_class == TYPE_CLASS_DOMAIN)
          res = _domain->updateTypePointer(__old, __new) && res;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the type object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hType::typeVerification(QString& __error_message)
{
     if(_class == TYPE_CLASS_DOMAIN)
     {
          if(isSetCorrect(_domain, true) != H_TYPE_ERROR_NO_ERROR)
          {
               __error_message += hqed::createErrorString("", "", "\nType " + name() + " error: " + lastError(), ERROR_TYPE, 2);
               return false;
          }
          
          if(!_domain->setVerification(__error_message))
               return false;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the information about type
//
// #param bool __html - denotes if the html tag should be used
// #return the information about type as string
QString hType::typeInfo(bool __html)
{
     QString result = "";
     
     QString new_line_tag = "\n";
     QString open_bold_tag = "";
     QString close_bold_tag = "";
     if(__html)
     {
          new_line_tag = "<br>";
          open_bold_tag = "<b>";
          close_bold_tag = "</b>";
     }
     
     // Info collect
     result += open_bold_tag + "Type name: " + close_bold_tag + name() + new_line_tag;
     result += open_bold_tag + "Type id: " + close_bold_tag + id() + new_line_tag;
     result += open_bold_tag + "Description: " + close_bold_tag + description() + new_line_tag;
     result += open_bold_tag + "Base type: " + close_bold_tag + hpTypesInfo.ptn.at(hpTypesInfo.iopti(baseType())) + new_line_tag;
     if((baseType() == INTEGER_BASED) || (baseType() == NUMERIC_BASED))
          result += open_bold_tag + "Length: " + close_bold_tag + QString::number(length()) + new_line_tag;
     if(baseType() == INTEGER_BASED)
          result += open_bold_tag + "Scale: " + close_bold_tag + "unimportant" + new_line_tag;
     if(baseType() == NUMERIC_BASED)
          result += open_bold_tag + "Scale: " + close_bold_tag + QString::number(scale()) + new_line_tag;
     result += open_bold_tag + "Type constraints: " + close_bold_tag + hpTypesInfo.cn.at(hpTypesInfo.ioci(typeClass()));
     if(typeClass() == TYPE_CLASS_DOMAIN)
          result += new_line_tag + open_bold_tag + "Domain: " + close_bold_tag + _domain->toString();
          
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the type to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this type as string.
QString hType::toProlog(int /*__option*/)
{
     QString lresult = "";
     QString lbasic = "numeric";
     QString llength = "";
     QString lordered = "";
     QString ldesc = "";
     
     if(baseType() == SYMBOLIC_BASED)
          lbasic = "symbolic";
     if(length() > 0)
     {
          llength += ",\n       length: " + QString::number(length());
          llength += ",\n       scale: " + QString::number(scale());
     }
     
     if(!_description.isEmpty())
          ldesc = ",\n       desc: " + hqed::mcwp(_description);
     
     lresult +=    "xtype [name: " + hqed::mcwp(name());
     lresult += ",\n       base: " + hqed::mcwp(lbasic);
     lresult += llength;
     lresult += ldesc;
     if(_class == TYPE_CLASS_DOMAIN)
     {
          lresult += ",\n       domain: " + _domain->toProlog();
     }
     if(_class == TYPE_CLASS_GROUP)
     {
          // not supported yet
     }
     
     lresult += "\n      ].";
     
     return lresult;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the old identifier of type, that have been used in xttml and attml
//
// #return The old identifier of type, that have been used in xttml and attml
int hType::fromHekateType(void)
{
     if((_baseType == SYMBOLIC_BASED) && (_class == TYPE_CLASS_DOMAIN))
          return 5;      // the old enum type identifier
          
     return hType::fromHekateType(_baseType);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the old identifier of contraints type, that have been used in xttml and attml
//
// #return The old identifier of contraints type, that have been used in xttml and attml
int hType::toOldXttContraintsType(void)
{
     // Ograniczenia
     int result = 0;
     if((_class == TYPE_CLASS_DOMAIN) && (_domain->at(0)->type() == SET_ITEM_TYPE_RANGE))             // We take only first range, we do not take other items
          result = 1;
     if((_class == TYPE_CLASS_DOMAIN) && (_domain->at(0)->type() == SET_ITEM_TYPE_SINGLE_VALUE))      // We assume that this is the list of values (not ranges)
          result = 2;
          
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that defines the default prefix of the type
//
// #return The default prefix of the type as string (current "tpe_")
QString hType::idPrefix(void)
{
     return "tpe_";
}
// -----------------------------------------------------------------------------



// #brief Function that returns the list of the names of the primitive types
//
// #return The list of the names of the primitive types
QStringList hType::primitiveTypes(void)
{
     QStringList res = (QStringList() << "boolean" << "integer" << "numeric" << "symbolic");
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of identifiers of primitive types
//
// #return The list of identifiers of primitive types
int* hType::primitiveTypesIndentifiers(void)
{
     int* res = new int[hType::primitiveTypesCount()];
     res[0] = BOOLEAN_BASED;
     res[1] = INTEGER_BASED;
     res[2] = NUMERIC_BASED;
     res[3] = SYMBOLIC_BASED;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the count of primitive types
//
// #return The count of primitive types
int hType::primitiveTypesCount(void)
{
     return hType::primitiveTypes().size();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the primitive type
//
// #param __typeIdentifier - identifer of the primitive type
// #return The name of the primitive type as string
QString hType::primitiveType(int __typeIdentifier)
{
     int lut[] = {BOOLEAN_BASED, INTEGER_BASED, NUMERIC_BASED, SYMBOLIC_BASED};
     QStringList types = primitiveTypes();
     QString res = "";
     
     for(int i=0;i<types.size();++i)
          if(__typeIdentifier == lut[i])
               res = types.at(i);
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of the names of the constraints types
//
// #return The list of the names of the constraints types
QStringList hType::constraintsNames(void)
{
     QStringList res = (QStringList() << "no constraints" << "domain");
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of the presentation sources
//
// #return The list of the presentation sources
QStringList hType::presentationSources(void)
{
     QStringList res = (QStringList() << "none" << "ecma" << "devplugin");
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the count of constraints types
//
// #return The count of constraints types
int hType::constraintsTypesCount(void)
{
     return constraintsNames().size();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of identifiers of constraints types
//
// #return The list of identifiers of constraints types
int* hType::constraintsIndentifiers(void)
{
     int count = hType::constraintsTypesCount();
     int* res = new int[count];

     switch (count) {
          case 3: res[2] = TYPE_CLASS_GROUP;
          case 2: res[1] = TYPE_CLASS_DOMAIN;
          case 1: res[0] = NO_TYPE_CLASS;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the old identifier of type, that have been used in xttml and attml
//
// #param __baseType - the base type identifier
// #return The old identifier of type, that have been used in xttml and attml
int hType::fromHekateType(int __baseType)
{
     switch(__baseType)
     {
          case BOOLEAN_BASED:
               return 1;
               break;
               
          case INTEGER_BASED:
               return 2;
               break;
               
          case NUMERIC_BASED:
               return 3;
               break;
               
          case SYMBOLIC_BASED:
               return 4;
               break;
               
          default:
               return 0;
               break;
     }
     
     return 0;
}
// -----------------------------------------------------------------------------

// #brief Function that translates the given value using script from the given file
//
// #param __scriptfile - file that contains the script
// #param __value - value to be translated
// #param __ok - indicates if the returned value is a result of a translation or error message
// #param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
// #return translared value or error message
QString hType::translateScript(QFile& __scriptfile, QString __value, int& __ok, bool __isInput)
{
     if(!__scriptfile.exists())
     {
          __ok = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
          return "The file \'" + __scriptfile.fileName() + "\' does not exists.";
     }
     
     if(!__scriptfile.open(QIODevice::ReadOnly | QIODevice::Text))
     {
          __ok = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
          return "Cannot open file \'" + __scriptfile.fileName() + "\'.";
     }
     
     QString scriptcontent = QString(__scriptfile.readAll());
     __scriptfile.close();
     
     return translateScript(scriptcontent, __value, __ok, __isInput);
}

bool hType::translateScriptAvailable(bool __checkInput)
{
     QFile scriptFile(Settings->ecmascriptPath() + "/" + _presentationSourceName);

     if(!scriptFile.exists())
     {
          return false;
     }

     if(!scriptFile.open(QIODevice::ReadOnly | QIODevice::Text))
     {
          return false;
     }

     QString scriptContent = QString(scriptFile.readAll());
     scriptFile.close();

     return __checkInput ?
          hEcmaScript::inputFunctionExists(scriptContent)
        : hEcmaScript::outputFunctionExists(scriptContent);
}
// -----------------------------------------------------------------------------

// #brief Function that translates the given value using given script content
//
// #param __scriptcontent - content of the translation script
// #param __value - value to be translated
// #param __ok - indicates if the returned value is a result of a translation or error message
// #param __isInput - if set to true, the Input function from Ecmascript will be used. Otherwise the Output function will be called.
// #return translared value or error message
QString hType::translateScript(QString __scriptcontent, QString __value, int& __ok, bool __isInput)
{
     QString ret = "";

     if (__isInput) {
          hEcmaScript::parseInput(__scriptcontent, __value, ret, __ok);
     }
     else {
          hEcmaScript::parseOutput(__scriptcontent, __value, ret, __ok);
     }

     return ret;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* hType::out_HML_2_0(void)
{
     QStringList* res = new QStringList;
     res->clear();
     
     QString baseTypeName = "numeric";
     if(baseType() == SYMBOLIC_BASED)
          baseTypeName = "symbolic";
          
     if((_length == 0) && (baseType() == INTEGER_BASED))
     {
          _length = QString::number(NUMERIC_SUPREMUM).length();
          _scale = 0;
     }
     
     QString init_line = "<type id=\"" + _id + "\" name=\"" + _name + "\" base=\"" + baseTypeName + "\"";
     if(_length > 0)
          init_line += " length=\"" + QString::number(_length) + "\" scale=\"" + QString::number(_scale) + "\"";
     init_line += ">";
     *res << init_line;
     
     if(_presentationSourceID != TYPE_PRESENTATION_ORIGIN)
     {
          QStringList srcs = presentationSources();
          init_line = "<presentation sourceid=\"" + QString::number(_presentationSourceID) + "\" sourcename=\"" + _presentationSourceName + "\"";
          if(_presentationSourceID == TYPE_PRESENTATION_DEVPC)
               init_line += " sourcetype=\"" + _presentationSourceType + "\"";
          init_line += "/>";
          *res << init_line;
     }
     
     if(!_description.isEmpty())
          *res << "\t<desc>" + _description + "</desc>";
     
     if(_class == TYPE_CLASS_DOMAIN)
     {
          QStringList* al = _domain->out_HML_2_0();
          for(int l=0;l<al->size();l++)
               *res << "\t" + al->at(l);
          delete al;
     }
     
     *res << "</type>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void hType::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     QString baseTypeName = "numeric";
     if(baseType() == SYMBOLIC_BASED)
          baseTypeName = "symbolic";
          
     if((_length == 0) && (baseType() == INTEGER_BASED))
     {
          _length = QString::number(NUMERIC_SUPREMUM).length();
          _scale = 0;
     }
     
     _xmldoc->writeStartElement("type");
     _xmldoc->writeAttribute("id", _id);
     _xmldoc->writeAttribute("name", _name);
     _xmldoc->writeAttribute("base", baseTypeName);
     if(_length > 0)
     {
          _xmldoc->writeAttribute("length", QString::number(_length));
          _xmldoc->writeAttribute("scale", QString::number(_scale));
     }
     
     if(_presentationSourceID != TYPE_PRESENTATION_ORIGIN)
     {
          QStringList srcs = presentationSources();
          _xmldoc->writeEmptyElement("presentation");
          _xmldoc->writeAttribute("sourceid", srcs.at(_presentationSourceID));
          _xmldoc->writeAttribute("sourcename", _presentationSourceName);
          if(_presentationSourceID == TYPE_PRESENTATION_DEVPC)
               _xmldoc->writeAttribute("sourcetype", _presentationSourceType);
     }
     
     if(!_description.isEmpty())
          _xmldoc->writeTextElement("desc", _description);

     if(_class == TYPE_CLASS_DOMAIN)
          _domain->out_HML_2_0(_xmldoc);
     
     _xmldoc->writeEndElement();
}
// -----------------------------------------------------------------------------

QString hType::GetJavaType()
{
     QString javaType;

     switch(baseType())
     {
          case BOOLEAN_BASED:
               javaType = "boolean";
               break;
          case INTEGER_BASED:
               javaType = "int";
               break;
          case NUMERIC_BASED:
               javaType = "float";
               break;
          case SYMBOLIC_BASED:
               javaType = "String";
               break;
          default:
               javaType = "!UNKNOWN_TYPE!";
     }

     return javaType;
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hType::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "type";
     
     if(__root.tagName().toLower() != elementName)
          return 0;
     
     int t1 = hpTypesInfo.iopti(NUMERIC_BASED);
     int t2 = hpTypesInfo.iopti(SYMBOLIC_BASED);
     int t3 = hpTypesInfo.iopti(INTEGER_BASED);
     QStringList baseTypes = QStringList() << hpTypesInfo.ptn.at(t1) << hpTypesInfo.ptn.at(t2);
          
     // Reading type data
     QString tid = __root.attribute("id", "");
     QString tname = __root.attribute("name", "");
     QString tbase = __root.attribute("base", "");
     QString tlength = __root.attribute("length", "0");
     QString tscale = __root.attribute("scale", "0");
               
     // Reading presentationproperities
     QDomElement DOMpres = __root.firstChildElement("presentation");
     if(!DOMpres.isNull())
     {
          QStringList srcs = presentationSources();
          int psrcid = srcs.indexOf(DOMpres.attribute("sourceid", srcs.at(0)));
          QString psrcname = DOMpres.attribute("sourcename", "");
          QString psrctype = DOMpres.attribute("sourcetype", "");
          
          if((psrcid != TYPE_PRESENTATION_ORIGIN) && (psrcname == ""))
          {
               __msg += "\n" + hqed::createErrorString("", "", "Unspecified name of the presentation source." + hqed::createDOMlocalization(__root), 0, 2);
               result++;
          }
          if((psrcid == TYPE_PRESENTATION_DEVPC) && (psrctype == ""))
          {
               __msg += "\n" + hqed::createErrorString("", "", "Unspecified name of the plugin presentation service name." + hqed::createDOMlocalization(__root), 0, 2);
               result++;
          }
          
          _presentationSourceID = psrcid;
          _presentationSourceName = psrcname;
          _presentationSourceType = psrctype;
     }
     
     QDomElement DOMdesc = __root.firstChildElement("desc");
     if(!DOMdesc.isNull())
          _description = DOMdesc.text();
          
     if(tid == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined identifier of the \'" + tname + "\' type." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
     }
     
     if(tname == "")
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined type name. Id = \'" + tid + "\'" + hqed::createDOMlocalization(__root), 0, 2);
          result++;
     }
     
     if(baseTypes.indexOf(tbase) == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined base type \'" + tbase + "\'. Error from \'" + tname + "\' type." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
     }
     
     bool convStat;
     int lengthNum = tlength.toInt(&convStat);
     if((!convStat) || (lengthNum < 0))
     {
          __msg += "\n" + hqed::createErrorString("", "", "Incorrect value of \'length\' parameter in \'" + tname + "\' type.\nRead value: \'" + tlength + "\'.\nThe value will be set to 0." + hqed::createDOMlocalization(__root), 1, 2);
          lengthNum = 0;
          result++;
     }
     
     int scaleNum = tscale.toInt(&convStat);
     if((!convStat) || (tscale < 0))
     {
          __msg += "\n" + hqed::createErrorString("", "", "Incorrect value of \'scale\' parameter in \'" + tname + "\' type.\nRead value: \'" + tscale + "\'.\nThe value will be set to 0." + hqed::createDOMlocalization(__root), 1, 2);
          scaleNum = 0;
          result++;
     }
     
     // Saving read data - must before domain processing
     _id = tid;
     _name = tname;
     _baseType = hpTypesInfo.ptids[hpTypesInfo.ptn.indexOf(tbase)];
     _length = lengthNum;
     _scale = scaleNum;
     if((_length == QString::number(NUMERIC_SUPREMUM).length()) && (_scale == 0))
     {
          _length = 0;
          _baseType = hpTypesInfo.ptids[t3];
     }
     
     // Reading domain
     QDomElement DOMdomain = __root.firstChildElement("domain");
     bool domainIsNull = DOMdomain.isNull();
     if(!domainIsNull)
     {
          result += _domain->in_HML_2_0(DOMdomain, __msg);
          _class = TYPE_CLASS_DOMAIN;
          
          if(!_domain->setVerification(__msg))
               result++;
     }
          
     return result;
}
// -----------------------------------------------------------------------------
