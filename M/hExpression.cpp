/*
 *	   $Id: hExpression.cpp,v 1.17.4.3.2.2 2011-09-01 19:32:10 hqeddoxy Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../namespaces/ns_hqed.h"
#include "hSet.h"
#include "hType.h"
#include "hValue.h"
#include "hDomain.h"
#include "hFunction.h"
#include "hTypeList.h"
#include "hExpression.h"
#include "hFunctionList.h"
#include "hMultipleValue.h"
#include "hFunctionArgument.h"
// -----------------------------------------------------------------------------

// #brief Default constructor hFunction class.
hExpression::hExpression(void)
{
     _function = NULL;
     _requiredType = NULL;
}
// -----------------------------------------------------------------------------

// #brief Constructor hFunction class.
//
// #param hType* __required_type - poiter to the object type that is required
hExpression::hExpression(hType* __required_type)
{
     _function = NULL;
     _requiredType = __required_type;
}
// -----------------------------------------------------------------------------

// #brief Default destructor hFunction class.
hExpression::~hExpression(void)
{
     if(_function != NULL)
     {
          delete _function;
          _function = NULL;
     }
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param hExpression* __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hExpression* hExpression::copyTo(hExpression* __destinationItem)
{
     hExpression* res = __destinationItem;
     if(res == NULL)
          res = new hExpression;
          
     res->_requiredType = _requiredType;
     res->_memberOfDomain = _memberOfDomain;
     if(_function != NULL)
          res->_function = _function->copyTo();
     if(_function == NULL)
          res->_function = NULL;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief The function that prepare the function to call by setting the arguments, required type, ...
//
// #return the result of function call
int hExpression::prepareFunction(void)
{
     if(_function == NULL)
          return setLastError(H_EXPRESSION_ERROR_NOT_DEFINED_FUNCTION);
     
     /*int srtres = _function->setRequiredType(_requiredType);
     if(srtres != FUNCTION_ERROR_SUCCESS)
          return setLastError(H_EXPRESSION_ERROR_OTHER_OBJECT_ERROR, _function->lastError());*/
          
     return H_EXPRESSION_SUCCES;
}
// -----------------------------------------------------------------------------

// #brief Calls the given function with given arguments
//
// #return the result of function call
hMultipleValue* hExpression::calculate(void)
{
     if(prepareFunction() != H_EXPRESSION_SUCCES)
          return NULL;
     
     hMultipleValue* result = _function->calculate();
     if(result == NULL)
          setLastError(_function->lastError());
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string that is the user representation of the expression
//
// #return the string that is the user representation of the expression
QString hExpression::toString(void)
{
     if(prepareFunction() != H_EXPRESSION_SUCCES)
          return lastError();
    
     return _function->toString();
}
// -----------------------------------------------------------------------------

// #brief Function that maps the expression to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this expression as string.
QString hExpression::toProlog(int /*__option*/)
{
     if(prepareFunction() != H_EXPRESSION_SUCCES)
          return lastError();
    
     return _function->toProlog();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param QString __error_content - last error message
// #return no return value
void hExpression::setLastError(QString __error_content)
{
     _lastError = __error_content;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param int __error_code - code of the error
// #param QString __error_desc - the additional error description
// #return returns the given error code
int hExpression::setLastError(int __error_code, QString __error_desc)
{
     QString complete_error_message = "";

     switch(__error_code)
     {
          case H_EXPRESSION_ERROR_OTHER_OBJECT_ERROR:
               complete_error_message = __error_desc;
               __error_desc = "";
               break;
               
          case H_EXPRESSION_ERROR_NOT_DEFINED_FUNCTION:
               complete_error_message = "The function is not defined";
               break;
               
          case H_EXPRESSION_ERROR_NOT_DEFINED_REQUIRED_TYPE:
               complete_error_message = "Function: " + _function->userRepresentation() + " error message:\nThe required type is not defined.";
               break;
     }
     
     if(!__error_desc.isEmpty())
          complete_error_message += "\n" + __error_desc;
     setLastError(complete_error_message);
     
     return __error_code;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the function object
//
// #return returns the pointer to the function object
hFunction* hExpression::function(void)
{
     return _function;
}
// -----------------------------------------------------------------------------

// #brief Function if the result of this expression is a member of domain
//
// #return if the result of this expression is a member of domain (true), otherwise false
bool hExpression::isMemberOfDomain(void)
{
     return _memberOfDomain;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the last error message
//
// #return returns the last error message as string
QString hExpression::lastError(void)
{
     return _lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the required type
//
// #return the pointer to the required type
hType* hExpression::requiredType(void)
{
     return _requiredType;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the pointer to the function object
//
// #param hFunction* __function - pointer to the function
// #return no value return
void hExpression::setFunction(hFunction* __function)
{
     QList<hFunctionArgument*> arglist;
     if(_function != NULL)
     {
          // args copying
          for(int i=0;i<_function->argCount();++i)
               arglist.append(_function->argument(i)->copyTo());

          delete _function;
     }

     _function = __function;
     _function->setResultVerificationStatus(!_memberOfDomain);
     setArgumentCount(_function->minArgCount());       
     _function->setRequiredType(_requiredType);
     _function->refreshArgumentCompatibleTypesLists();
     
     for(int i=0;i<arglist.size();++i)
     {
          __function->setArgument(i, arglist.at(i));
          
          // simplify the arguments if necessary
          if(((__function->argMltplConf(i) & FUNCTION_TYPE__SINGLE_VALUED) != 0) && 
             ((__function->argMltplConf(i) & FUNCTION_TYPE__MULTI_VALUED) == 0))
          {
               int argtype = arglist.at(i)->type();
               argtype = (argtype & (~(FUNCTION_TYPE__SINGLE_VALUED | FUNCTION_TYPE__MULTI_VALUED))) | FUNCTION_TYPE__SINGLE_VALUED;
               arglist.at(i)->setType(argtype);
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Function that sets the pointer to the required type
//
// #param hType* __req_type - pointer to the required type
// #return true on succes otherwise false
bool hExpression::setRequiredType(hType* __req_type)
{
     bool res = true;

     _requiredType = __req_type;
     if(_function != NULL)
     {
          if(_function->setRequiredType(__req_type) != FUNCTION_ERROR_SUCCESS)
          {
               res = false;
               setLastError(_function->lastError());
          }
          _function->refreshArgumentCompatibleTypesLists();
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument count
///
// #param __arg_count
// #return true if success otherwise false
bool hExpression::setArgumentCount(int __arg_count)
{
     // Creating arguments in the function object
     if(_function->setArgCount(__arg_count) != FUNCTION_ERROR_SUCCESS)
     {
          setLastError(_function->lastError());
          return false;
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the status of being a member of domain
//
// #param bool __mod - new value of the status
// #return no value return
void hExpression::setAsMemberOfDomain(bool __mod)
{
     _memberOfDomain = __mod;
     if(_function != NULL)
          _function->setResultVerificationStatus(!_memberOfDomain);
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hExpression::updateTypePointer(hType* __old, hType* __new)
{
     if(_requiredType == __old)
          _requiredType = __new;
          
     if(_function != NULL)
          return _function->updateTypePointer(__old, __new);
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hExpression::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          if(function() != NULL)
               res = function()->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          if(function() != NULL)
               res = function()->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hExpression::isAVRused(void)
{
     if(_function == NULL)
          return false;
          
     return _function->isAVRused();
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hExpression::isExpressionUsed(void)
{
     if(_function == NULL)
          return false;
          
     return _function->isExpressionUsed();
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression objest definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hExpression::expressionVerification(QString& __error_message)
{
     bool res = true;

     // Checking the function
     if(prepareFunction() != H_EXPRESSION_SUCCES)
     {
          __error_message += "\n" + lastError();
          return false;
     }

     // Checking the required type
     if(_requiredType == NULL)
     {
          __error_message += "\nThe expression " + toString() + " type is not defined.";
          return false;
     }

     // Checking the function object
     res = _function->functionVerification(__error_message) && res;      

     // If avr is not used we can calculate the expression value
     if((!isAVRused()) && (!_function->isCellFunction()))
     {
          hMultipleValue* result = calculate();
          if(result == NULL)
          {
               __error_message += "\n" + lastError();
               res = false;
          }

          if((result != NULL) && (!result->multipleValueVerification(__error_message)))
               res = false;
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in drools5 format.
//
// #return the expression in drools5 format
QString hExpression::toDrools5(void)
{
     if(_function == NULL)
          return "";
     return _function->toDrools5();
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hExpression::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(_function == NULL)
          return res;
     
     // Setting the root element name
     QString elementName = "expr";
     if(__mode == 3)
          elementName = "eval";

     // Retrieving funtion name
     QString fName = _function->xmlRepresentation();
     QString func_name = " name=\"" + fName + "\"";

     QString init_line = "<" + elementName;
     init_line += func_name;
     init_line += ">";
     
     // root element
     *res << init_line;

     // Function saving
     QStringList* tmp = _function->out_HML_2_0(__mode);
     for(int j=0;j<tmp->size();++j)
          *res << tmp->at(j);
     delete tmp;
     
     *res << "</" + elementName + ">";
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void hExpression::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     if(_function == NULL)
          return;
     
     // Setting the root element name
     QString elementName = "expr";
     if(__mode == 3)
          elementName = "eval";

     // Retrieving funtion name
     QString fName = _function->xmlRepresentation();
     
     _xmldoc->writeStartElement(elementName);
          _xmldoc->writeAttribute("name", fName);
          _function->out_HML_2_0(_xmldoc, __mode);
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #param __cellExpression - denotes if this object represents the whole cell expression
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hExpression::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression)
{
     int result = 0;

     QStringList possibleTags = QStringList() << "relation" << "trans" << "action" << "eval" << "expr";
     QList<QStringList> possibleNames;
     
     possibleNames.append(QStringList() << "eq" << "neq" << "in" << "notin" << "subseteq" << "supseteq" << "sim" << "notsim" << "lt" << "gt" << "lte" << "gte");
     possibleNames.append(QStringList() << ":=" << "donotcare");
     possibleNames.append(QStringList());
     possibleNames.append(QStringList() << "union" << "intersec" << "except" << "complement");
     possibleNames.append(QStringList());
     
     int tagIndex = possibleTags.indexOf(__root.tagName().toLower());
     if(tagIndex == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Undefined or misplaced tag \'" + __root.tagName() + "\'.\nAllowed tag names are: " + possibleTags.join(", ") + "\nThe object will not be read." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
          return result;
     }
     
     // Reading function name
     QString fname = __root.attribute("name", ":=");
     if((tagIndex == 1) && (__root.childNodes().length() == 1))
          fname = "donotcare";
     
     // Checking correctness
     if((!possibleNames.at(tagIndex).empty()) && (possibleNames.at(tagIndex).indexOf(fname) == -1))
     {
          __msg += "\n" + hqed::createErrorString("", "", "Incorrect use of function \'" + fname + "\' with element \'" + __root.tagName() + "\'.\nAllowed function names are: " + possibleNames.at(tagIndex).join(", ") + "\nThe object will not be read." + hqed::createDOMlocalization(__root), 0, 2);
          result++;
          return result;
     }

     // Searchnig for function
     int index = funcList->findFunction(fname, FUNC_REPRESENTATION_XML);

     if(index == -1)
     {
          __msg += "\n" + hqed::createErrorString("", "", "Critical error: Cannot find function \'" + fname + "\'" + hqed::createDOMlocalization(__root), 0, 2);
          result++;
          return result;
     }

     QDomElement DOMargs = __root.firstChildElement();
     hFunction* func = funcList->at(index)->copyTo();
     setFunction(func);   
     result += func->in_HML_2_0(DOMargs, __msg, __attr, __cellExpression);  
     
     return result;
}
// -----------------------------------------------------------------------------
