/*
 *	   $Id: hSetItem.cpp,v 1.22.4.2.2.1 2011-06-13 17:02:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../namespaces/ns_hqed.h"
#include "XTT/XTT_Attribute.h"

#include "hExpression.h"
#include "hValue.h"
#include "hType.h"
#include "hSet.h"
#include "hType.h"
#include "hSetItem.h"
// -----------------------------------------------------------------------------

// #brief Constructor hSetItem class.
hSetItem::hSetItem(void)
{
     _type = SET_ITEM_TYPE_NOT_DEFINED;
     
     _value1 = NULL;
     _value2 = NULL;
     _parentSet = NULL;
}
// -----------------------------------------------------------------------------

// #brief Constructor hSetItem class.
//
// #param hSet* __parent - the pointer to the parent set
// #param int __type - defines the type of the item as range or single value
// #param QString __from - defines the first value of the range, or single value
// #param QString __to - defines the second value of the range, in case of _type == SET_ITEM_TYPE_RANGE is unimportant
// #param bool __copyObject - defines if the object should be created as new, or the pointer can be used
hSetItem::hSetItem(hSet* __parent, int __type, hValue* __from, hValue* __to, bool __copyObject)
{
     _value1 = NULL;
     _value2 = NULL;
     
     _parentSet = __parent;
     _type = __type;
     setFrom(__from, __copyObject);
     setTo(__to, __copyObject);
}
// -----------------------------------------------------------------------------

// #brief Destructor hSetItem class.
hSetItem::~hSetItem(void)
{
     if(_value1 != NULL)
          delete _value1;
          
     if(_value2 != NULL)
          delete _value2;
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param hSetItem* __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hSetItem* hSetItem::copyTo(hSetItem* __destinationItem)
{
     hSetItem* res = __destinationItem;
     if(res == NULL)
          res = new hSetItem;
          
     res->setParent(_parentSet);
     res->setType(_type);
     res->setFrom(_value1, true);
     res->setTo(_value2, true);
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the new type of item
//
// #param int __type - new type of item
// #return No return value.
void hSetItem::setType(int __type)
{
     _type = __type;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the left value of the range
//
// #param QString __from - the left value of the range
// #param bool __copyObject - defines if the object should be created as new, or the pointer can be used
// #return No return value.
void hSetItem::setFrom(hValue* __from, bool __copyObject)
{
     if(__from == NULL)
          return;
          
     if((_value1 != NULL) && (_value1 != __from))
          delete _value1;
          
     if(__copyObject)
          _value1 = new hValue(__from);
          
     if(!__copyObject)
          _value1 = __from;
          
     setParentType(_parentSet->parentType());
}
// -----------------------------------------------------------------------------

// #brief Function that sets the left value of the range
//
// #param QString __from - the left value of the range
// #param bool __copyObject - defines if the object should be created as new, or the pointer can be used
// #return No return value.
void hSetItem::setTo(hValue* __to, bool __copyObject)
{
     if(__to == NULL)
          return;
          
     if((_value2 != NULL) && (_value2 != __to))
          delete _value2;
          
     if(__copyObject)
          _value2 = new hValue(__to);
          
     if(!__copyObject)
          _value2 = __to;
          
     setParentType(_parentSet->parentType());
}
// -----------------------------------------------------------------------------

// #brief Function that sets value of the item (in case of single value type)
//
// #param QString __value - new value of the item
// #param bool __copyObject - defines if the object should be created as new, or the pointer can be used
// #return true if the given range is correct, otherwise false.
bool hSetItem::setValue(hValue* __value, bool __copyObject)
{
     setFrom(__value, __copyObject);
     setType(SET_ITEM_TYPE_SINGLE_VALUE);
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the range value
//
// #param QString __from - the left value
// #param QString __to - the right value
// #param bool __copyObject - defines if the object should be created as new, or the pointer can be used
// #return true if the given range is correct, otherwise false.
bool hSetItem::setRange(hValue* __from, hValue* __to, bool __copyObject)
{
     setFrom(__from, __copyObject);
     setTo(__to, __copyObject);
     setType(SET_ITEM_TYPE_RANGE);
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the pointer to the parent container
//
// #param hSet* __ps - the parent set of this item
// #return No return value.
void hSetItem::setParent(hSet* __ps)
{
     _parentSet = __ps;
     setParentType(_parentSet->parentType());
}
// -----------------------------------------------------------------------------

// #brief Function that sets the pointer to the parent type of the parent set. The function updates all the expressions _requiredType fields
//
// #param hType* __pt - poiter to the new parent set parent type
// #return No return value.
void hSetItem::setParentType(hType* __pt)
{
     if((_value1 != NULL) && (_value1->type() == EXPRESSION))
          _value1->expressionField()->setRequiredType(__pt);
          
     if((_value2 != NULL) && (_value2->type() == EXPRESSION))
          _value2->expressionField()->setRequiredType(__pt);
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hSetItem::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     if(_value1 != NULL)
          res = _value1->expressionField()->updateTypePointer(__old, __new) && res;
          
     if(_value2 != NULL)
          res = _value2->expressionField()->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hSetItem::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          if(_value1 != NULL)
               res = _value1->eventMessage(__errmsgs, __event, attr) && res;
          if(_value2 != NULL)
               res = _value2->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          if(_value1 != NULL)
               res = _value1->eventMessage(__errmsgs, __event, attr) && res;
          if(_value2 != NULL)
               res = _value2->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hSetItem::isAVRused(void)
{
     bool res = false;
     if(_value1 != NULL)
          res = res || _value1->isAVRused();
          
     if(_value2 != NULL)
          res = res || _value2->isAVRused();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hSetItem::isExpressionUsed(void)
{
     bool res = false;
     if(_value1 != NULL)
          res = res || _value1->isExpressionUsed();
          
     if(_value2 != NULL)
          res = res || _value2->isExpressionUsed();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the left value of the range
//
// #return The string representation of the left value of the range
hValueResult hSetItem::from(void)
{
     if((_value1->definitionType() == SYM) || (_value1->type() != VALUE))
          return _value1->value();
          
     QString __symbolicValue = "";
     if((_value1->definitionType() == NUM) && (_value1->type() == VALUE))
     {
          if(_parentSet == NULL)
               return hValueResult("Undefined parent set for value1: " + _value1->toString() + "", false);
          if(_parentSet->parentType() == NULL)
               return hValueResult("Undefined parent type for set: " + _parentSet->toString() + "", false);
          
          hValueResult hvr = _value1->value();
          if(!(bool)hvr)
               return hvr;
          
          if(_parentSet->parentType()->mapFromNumeric((QString)hvr, __symbolicValue, true) != H_TYPE_ERROR_NO_ERROR)
               return hValueResult(_parentSet->parentType()->lastError(), false);
     }
     
     return hValueResult(__symbolicValue);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the right value of the range
//
// #return The string representation of the right value of the range
hValueResult hSetItem::to(void)
{
     if((_value2->definitionType() == SYM) || (_value2->type() != VALUE))
          return _value2->value();
          
     QString __symbolicValue = "";
     if((_value2->definitionType() == NUM) && (_value2->type() == VALUE))
     {
          if(_parentSet == NULL)
               return hValueResult("Undefined parent set for value2: " + _value2->toString() + "", false);
          if(_parentSet->parentType() == NULL)
               return hValueResult("Undefined parent type for set: " + _parentSet->toString() + "", false);
          
          hValueResult hvr = _value2->value();
          if(!(bool)hvr)
               return hvr;
          
          if(_parentSet->parentType()->mapFromNumeric((QString)hvr, __symbolicValue, true) != H_TYPE_ERROR_NO_ERROR)
               return hValueResult(_parentSet->parentType()->lastError(), false);
     }
     
     return hValueResult(__symbolicValue);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of the item
//
// #return The string representation of the value
hValueResult hSetItem::value(void)
{
     return from();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the value. The function tries to find the values that has the symbolic representation
//
// #return The string representation of the value
QString hSetItem::toSymbolicFormat(QString __value)
{
     QString res = __value;

     // Conversion of boolean values
     if((_parentSet != NULL) && 
        (_parentSet->parentType() != NULL) && 
        (_parentSet->parentType()->baseType() == BOOLEAN_BASED))
        {
               bool rti_ok;
               int rti = res.toInt(&rti_ok);
               if((!rti_ok) && (res != XTT_Attribute::_any_operator_) && (res != XTT_Attribute::_not_definded_operator_))    
                    return "not defined boolean value: " + __value;
               
               if((rti_ok) && (rti == BOOLEAN_FALSE))
                    res = XTT_Attribute::_false_operator_;
               if((rti_ok) && (rti == BOOLEAN_TRUE))
                    res = XTT_Attribute::_true_operator_;
        }
        
     // conversion of numeric values
     if((_parentSet != NULL) && 
        (_parentSet->parentType() != NULL) && 
        ((_parentSet->parentType()->baseType() == INTEGER_BASED) || (_parentSet->parentType()->baseType() == NUMERIC_BASED)))
        {
               _parentSet->parentType()->toCurrentNumericFormat(res);
               
               if(res.toDouble() == (double)NUMERIC_INFIMUM)
                    res = XTT_Attribute::_min_operator_;
               if(res.toDouble() == (double)NUMERIC_SUPREMUM)
                    res = XTT_Attribute::_max_operator_;
        }
        
     // Using value presentaion service
     if((_parentSet != NULL) && 
        (_parentSet->parentType() != NULL))
     {
          int err;
          hType* type = _parentSet->parentType();

          res = type->translate(__value, err, false);

          if (type->presentationSourceId() == TYPE_PRESENTATION_ECMA) {
              if (err)
                  res = __value;
              else
                  res = "\"" + res + "\"";
          }
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the drools5 representation of the value. The function tries to find the values that has the symbolic representation
//
// #return The drools5 representation of the value
QString hSetItem::toDrools5Format(QString __value)
{
     QString res = __value;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the PROLOG representation of the value. The function tries to find the values that has the symbolic representation
//
// #return The PROLOG representation of the value
QString hSetItem::toPrlgFormat(QString __value)
{
     QString res = __value;
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the left value of the range as string representation
//
// #return The string representation of the left value of the range
QString hSetItem::strFrom(void)
{
     if((_value1->isAVRused()) || (_value1->isExpressionUsed()))
          return _value1->toString();
     return toSymbolicFormat(_value1->toString());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the left value of the range as drools5 representation
//
// #return The drools5 representation of the left value of the range
QString hSetItem::drls5From(void)
{
     if((_value1->isAVRused()) || (_value1->isExpressionUsed()))
          return _value1->toDrools5();
     return toDrools5Format(_value1->toDrools5());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the left value of the range as PROLOG representation
//
// #param __option - options of the output
// #return The PROLOG representation of the left value of the range
QString hSetItem::prlgFrom(int __option)
{
     return toPrlgFormat(_value1->toProlog(__option));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the right value of the range as string representation
//
// #return The string representation of the right value of the range
QString hSetItem::strTo(void)
{
     if((_value2->isAVRused()) || (_value2->isExpressionUsed()))
          return _value2->toString();
     return toSymbolicFormat(_value2->toString());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the right value of the range as drools5 representation
//
// #return The drools5 representation of the right value of the range
QString hSetItem::drls5To(void)
{
     if((_value2->isAVRused()) || (_value2->isExpressionUsed()))
          return _value2->toDrools5();
     return toDrools5Format(_value2->toDrools5());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the right value of the range as PROLOG representation
//
// #param __option - options of the output
// #return The PROLOG representation of the right value of the range
QString hSetItem::prlgTo(int __option)
{
     return toPrlgFormat(_value2->toProlog(__option));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of the item as string representation
//
// #return The string representation of the value
QString hSetItem::strValue(void)
{
     return strFrom();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of the item as drools5 representation
//
// #return The drools5 representation of the value
QString hSetItem::drls5Value(void)
{
     return drls5From();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of the item as PROLOG representation
//
// #param __option - options of the output
// #return The PROLOG representation of the value
QString hSetItem::prlgValue(int __option)
{
     return prlgFrom(__option);
}
// -----------------------------------------------------------------------------

// #brief Function that tries to match the current value to format that is required for this type
//
// #param __allowTrunc - denotes if the digiths can be cut
// #return The result code
int hSetItem::toCurrentNumericFormat(bool __allowTrunc)
{
     hType* t = _parentSet->parentType();
     if(t == NULL)
          return H_TYPE_ERROR_NULL_VALUE;

     // if non numerical type
     if((t->baseType() != INTEGER_BASED) && (t->baseType() != NUMERIC_BASED))
          return H_TYPE_ERROR_NO_ERROR;

     // converting single valued item
     if(type() == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          ValueDefinitionType v1deft = _value1->definitionType();
          _value1->setDefinitionType(SYM);

          // converting first value
          if(_value1->type() == VALUE)
          {
               QString v1 = "";
               hValueResult vr = _value1->value();
               if((bool)vr == true)
               {
                    v1 = vr.result();
                    int tcnfrc = t->toCurrentNumericFormat(v1, __allowTrunc);
                    qDebug() << "Converted " << vr.result() << " to " << v1;
                    if(tcnfrc != H_TYPE_ERROR_NO_ERROR)
                         return tcnfrc;
                    _value1->setValue(v1);
               }
               else
                    return t->setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, vr.error());
          }
          _value1->setDefinitionType(v1deft);
     }

     // converting range item
     else if(type() == SET_ITEM_TYPE_RANGE)
     {
          ValueDefinitionType v1deft = _value1->definitionType(),
                              v2deft = _value2->definitionType();
          _value1->setDefinitionType(SYM);
          _value2->setDefinitionType(SYM);

          // converting first value
          if(_value1->type() == VALUE)
          {
               QString v1 = "";
               hValueResult vr = _value1->value();
               if((bool)vr == true)
               {
                    v1 = vr.result();
                    int tcnfrc = t->toCurrentNumericFormat(v1, __allowTrunc);
                    if(tcnfrc != H_TYPE_ERROR_NO_ERROR)
                         return tcnfrc;
                    _value1->setValue(v1);
               }
               else
                    return t->setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, vr.error());
          }

          // converting second value
          if(_value2->type() == VALUE)
          {
               QString v2 = "";
               hValueResult vr = _value2->value();
               if((bool)vr == true)
               {
                    v2 = vr.result();
                    int tcnfrc = t->toCurrentNumericFormat(v2, __allowTrunc);
                    if(tcnfrc != H_TYPE_ERROR_NO_ERROR)
                         return tcnfrc;
                    _value2->setValue(v2);
               }
               else
                    return t->setLastError(H_TYPE_ERROR_OTHER_OBJECT_ERROR, vr.error());
          }

          _value1->setDefinitionType(v1deft);
          _value2->setDefinitionType(v2deft);
     }

     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the item
//
// #return The string representation of the item
QString hSetItem::toString(void)
{
     QString res = "";
     
     switch(_type)
     {
          case SET_ITEM_TYPE_NOT_DEFINED:
               res = "not defined type of set item";
               break;
               
          case SET_ITEM_TYPE_SINGLE_VALUE:
               res = strValue();
               break;
               
          case SET_ITEM_TYPE_RANGE:
               res = "[" + strFrom() + "," + strTo() + "]";
               break;
               
          default:
               res = "set item type out of range";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the drools5 representation of the item
//
// #return The drools5 representation of the item
QString hSetItem::toDrools5(void)
{
     QString res = "";

     switch(_type)
     {
          case SET_ITEM_TYPE_NOT_DEFINED:
               res = "not defined type of set item";
               break;

          case SET_ITEM_TYPE_SINGLE_VALUE:
               res = drls5Value();
               break;

          case SET_ITEM_TYPE_RANGE:
               res = "[" + drls5From() + "," + drls5To() + "]";
               break;

          default:
               res = "set item type out of range";
               break;
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the item to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this item as string.
QString hSetItem::toProlog(int __option)
{
     QString res = "";
     
     switch(_type)
     {
          case SET_ITEM_TYPE_NOT_DEFINED:
               res = "not defined type of set item";
               break;
               
          case SET_ITEM_TYPE_SINGLE_VALUE:
               res = prlgValue(__option);
               break;
               
          case SET_ITEM_TYPE_RANGE:
               res = prlgFrom() + " to " + prlgTo();
               break;
               
          default:
               res = "set item type out of range";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the values of the item
//
// #return The string representation of the values of the item
QString hSetItem::toVals(void)
{
     QString res = "";
     hValueResult fvr, tvr;
     
     switch(_type)
     {
          case SET_ITEM_TYPE_NOT_DEFINED:
               res = "not defined type of set item";
               break;
               
          case SET_ITEM_TYPE_SINGLE_VALUE:
               res = (QString)value();
               break;
               
          case SET_ITEM_TYPE_RANGE:
               fvr = from();
               tvr = to();
               res = "[" + (QString)fvr + "," + (QString)tvr + "]";
               break;
               
          default:
               res = "set item type out of range";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param QString __lastError - the content of the error message
// #return No return value.
void hSetItem::setLastError(QString __lastError)
{
     _lastError = __lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that transforms the current object that contains a set of values to an object that contains a single value
//
// #return true on success otherwise false
bool hSetItem::makeItSingle(void)
{    
     setType(SET_ITEM_TYPE_SINGLE_VALUE);
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param int __errorId - the error ID (defined in #define section)
// #param QString __errorMsg - the additional error message
// #return The given error code.
int hSetItem::setLastError(int __errorId, QString __errorMsg)
{
     QString err_msgs[] = {"Not defined parent set.", 
                           "Not defined type of the parent set.",
                           "Inconsistent input type.",
                           "Set item type is not defined.",
                           "Inconsistent item type.",
                           "Incorrect number format. Check length and scale parameters.",
                           "The range extreme values are incorrect.",
                           "Conversion error. The value has incorrect numeric format.",
                           "Incorrect value type. The extreme value of range can not be equal to \'" + XTT_Attribute::_not_definded_operator_ + "\' or \'" + XTT_Attribute::_any_operator_ + "\'."};
                           
     switch(__errorId)
     {
          case SET_ITEM_ERROR_PARENT_SET_NOT_DEFINED:
               setLastError(err_msgs[0] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_NOT_ACCESIBLE_TYPE:
               setLastError(err_msgs[1] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_INCONSISTENT_INPUT_TYPE:
               setLastError(err_msgs[2] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_NOT_DEFINED_ITEM_TYPE:
               setLastError(err_msgs[3] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE:
               setLastError(err_msgs[4] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_INCORRECT_NUMBER_FORMAT:
               setLastError(err_msgs[5] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_INCOORECT_EXTREME_VALUES:
               setLastError(err_msgs[6] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_INCORRECT_NUMERIC_FORMAT:
               setLastError(err_msgs[7] + __errorMsg);
               break;
           
          case SET_ITEM_ERROR_INCORRECT_VALUE_TYPE:
               setLastError(err_msgs[8] + __errorMsg);
               break;
               
          case SET_ITEM_ERROR_OTHER_OBJECT_ERROR:
               setLastError(__errorMsg);
               break;

          default:
               setLastError("hSetItem. Not defined error code (" + QString::number(__errorId) + ").\nAdditional error message: " + __errorMsg);
               break;
     }
     
     return __errorId;
}
// -----------------------------------------------------------------------------

// #brief Function that check if the type of the item is correct. If not the functions tries to fix it
//
// #return SET_ITEM_SUCCESS_VALUE i all is ok otherwise error code
int hSetItem::typeVerification(void)
{
     if(_parentSet == NULL)
          return setLastError(SET_ITEM_ERROR_PARENT_SET_NOT_DEFINED);
          
     if(_parentSet->parentType() == NULL)
          return setLastError(SET_ITEM_ERROR_NOT_ACCESIBLE_TYPE);
          
     // Verification of the first value
     QString err_msgs1 = "";
     if(!_value1->valueVerification(err_msgs1))
          return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, err_msgs1);
          
     // Verification variables
     int from_2_num_res, to_2_num_res;
     double from_num, to_num;
     
     hValueResult hfr = from();
     if(!(bool)hfr)
          return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hfr);
     
     from_2_num_res = _parentSet->parentType()->mapToNumeric((QString)hfr, from_num);
           
     // Single type verification
     if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          if(from_2_num_res != H_TYPE_ERROR_NO_ERROR)
          {
               setLastError(_parentSet->parentType()->lastError());
               return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
          }
          return SET_ITEM_SUCCESS_VALUE;
     }
     
     // If avr is used then we cannot check the correctness
     /*if(isAVRused())
          return SET_ITEM_SUCCESS_VALUE;     */
          
     // Verification of the second value
     QString err_msgs2 = "";
     if(!_value2->valueVerification(err_msgs2))
          return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, err_msgs2);

     hValueResult htr = to();
     if(!(bool)htr)
          return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)htr);
          
     to_2_num_res = _parentSet->parentType()->mapToNumeric((QString)htr, to_num);
     
     if((from_2_num_res != H_TYPE_ERROR_NO_ERROR) || 
        (to_2_num_res != H_TYPE_ERROR_NO_ERROR))
     {
          setLastError(_parentSet->parentType()->lastError());
          return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
     }
     
     if(from_num == to_num)
     {
          setType(SET_ITEM_TYPE_SINGLE_VALUE);
          return SET_ITEM_SUCCESS_VALUE;
     }
     if(from_num < to_num)
     {
          setType(SET_ITEM_TYPE_RANGE);
          return SET_ITEM_SUCCESS_VALUE;
     }
     if(from_num > to_num)
          return setLastError(SET_ITEM_ERROR_INCOORECT_EXTREME_VALUES);
          
     return SET_ITEM_SUCCESS_VALUE;
}
// -----------------------------------------------------------------------------

// #brief Function that check if the current item includes given value
//
// #param QString __value - value as string
// #return 1 if this item include given value, if no 0 otherwise error code
int hSetItem::contains(QString __value)
{
     if(_parentSet == NULL)
          return setLastError(SET_ITEM_ERROR_PARENT_SET_NOT_DEFINED);
          
     if(_parentSet->parentType() == NULL)
          return setLastError(SET_ITEM_ERROR_NOT_ACCESIBLE_TYPE);
          
     if(_type == SET_ITEM_TYPE_NOT_DEFINED)
          return setLastError(SET_ITEM_ERROR_NOT_DEFINED_ITEM_TYPE);
          
     if(__value.toLower() == XTT_Attribute::_any_operator_.toLower())
     {
          // If single value: we must check if the value is defined
          if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               hValueResult hvr = value();
               if(!(bool)hvr)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               if((QString)hvr == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_FALSE_VALUE;
               if((QString)hvr == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_FALSE_VALUE;
               
               return SET_ITEM_TRUE_VALUE;
          }
          
          // When range then the element must contain at least one correct value
          if(_type == SET_ITEM_TYPE_RANGE)
               return SET_ITEM_TRUE_VALUE;
     }
          
     if(__value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          return SET_ITEM_FALSE_VALUE;

     if(_parentSet->parentType()->baseType() == INTEGER_BASED)
     {
          bool conv_sts;
          
          int int_val = __value.toInt(&conv_sts);
          if(!conv_sts)
               return setLastError(SET_ITEM_ERROR_INCONSISTENT_INPUT_TYPE, "\nThe input value: " + __value);
           
          // Gdy jest to pojedyncza wartosc
          if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               hValueResult hvr = value();
               if(!(bool)hvr)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               QString strval = (QString)hvr;
               
               // checking for any and null operators
               if(strval.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               int int_itm_val = strval.toInt(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item value: " + (QString)value());

               if(int_itm_val == int_val)
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
          
          // Gdy jest to pojedyncza wartosc
          if(_type == SET_ITEM_TYPE_RANGE)
          {
               hValueResult hvr1 = from(), 
                            hvr2 = to();
                            
               if(!(bool)hvr1)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr1);
               if(!(bool)hvr2)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr2);
               
               QString strval1 = (QString)hvr1;
               QString strval2 = (QString)hvr2;

               // checking for any and null operators
               if(strval1.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval1.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
          
               int int_itm_from = strval1.toInt(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item from value: " + (QString)from());
                    
               // checking for any and null operators
               if(strval2.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval2.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               int int_itm_to = strval2.toInt(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item to value: " + (QString)to());
                    
               if((int_val >= int_itm_from) && (int_val <= int_itm_to))
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
     }
     
     if(_parentSet->parentType()->baseType() == NUMERIC_BASED)
     {
          bool conv_sts;
          
          double d_val = __value.toDouble(&conv_sts);
          if(!conv_sts)
               return setLastError(SET_ITEM_ERROR_INCONSISTENT_INPUT_TYPE, "\nThe input value: " + __value);
           
          // Gdy jest to pojedyncza wartosc
          if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               hValueResult hvr = value();
               if(!(bool)hvr)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               QString strval = (QString)hvr;
               
               // checking for any and null operators
               if(strval.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               double d_itm_val = strval.toDouble(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item value: " + (QString)value());
                    
               if(d_itm_val == d_val)
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
          
          // Gdy jest to przedzial
          if(_type == SET_ITEM_TYPE_RANGE)
          {
               hValueResult hvr1 = from(), 
                            hvr2 = to();
                            
               if(!(bool)hvr1)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr1);
               if(!(bool)hvr2)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr2);
                    
               QString strval1 = (QString)hvr1;
               QString strval2 = (QString)hvr2;
               
               // checking for any and null operators
               if(strval1.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval1.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
               
               double d_itm_from = strval1.toDouble(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item from value: " + (QString)from());
                    
               // checking for any and null operators
               if(strval2.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval2.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               double d_itm_to = strval2.toDouble(&conv_sts);
               if(!conv_sts)
                    return setLastError(SET_ITEM_ERROR_INCONSISTENT_ITEM_TYPE, "\nThe item to value: " + (QString)to());
                    
               if((d_val >= d_itm_from) && (d_val <= d_itm_to))
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
     }
     
     if(_parentSet->parentType()->baseType() == SYMBOLIC_BASED)
     {
          double val_num_rep;
          if(_parentSet->parentType()->mapToNumeric(__value, val_num_rep) != H_TYPE_ERROR_NO_ERROR)
          {
               setLastError(_parentSet->parentType()->lastError() + "\nError reason value: " + __value);
               return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
          }
          
          // Gdy jest to pojedyncza wartosc
          if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               double val_itm_num_rep;
               hValueResult hvr = value();
               if(!(bool)hvr)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr);
                    
               QString strval = (QString)hvr;
                    
               // checking for any and null operators
               if(strval.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               if(_parentSet->parentType()->mapToNumeric(strval, val_itm_num_rep) != H_TYPE_ERROR_NO_ERROR)
               {
                    setLastError(_parentSet->parentType()->lastError() + "\nError reason value: " + (QString)value());
                    return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
               }
               
               if(val_itm_num_rep == val_num_rep)
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
          
          // Gdy jest to pojedyncza wartosc
          if(_type == SET_ITEM_TYPE_RANGE)
          {
               double val_from_num_rep;
               hValueResult hvr1 = from();
               hValueResult hvr2 = to();
               
               if(!(bool)hvr1)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr1);
               if(!(bool)hvr2)
                    return setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr2);
                    
               QString strval1 = (QString)hvr1;
               QString strval2 = (QString)hvr2;
               
               // checking for any and null operators
               if(strval1.toLower() == XTT_Attribute::_any_operator_.toLower())
                    return SET_ITEM_ANY_VALUE;
               if(strval1.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
                    return SET_ITEM_ND_VALUE;
                    
               if(_parentSet->parentType()->mapToNumeric((QString)hvr1, val_from_num_rep) != H_TYPE_ERROR_NO_ERROR)
               {
                    setLastError(_parentSet->parentType()->lastError() + "\nError reason value: " + (QString)from());
                    return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
               }
               
               double val_to_num_rep;
               if(_parentSet->parentType()->mapToNumeric((QString)hvr2, val_to_num_rep) != H_TYPE_ERROR_NO_ERROR)
               {
                    setLastError(_parentSet->parentType()->lastError() + "\nError reason value: " + (QString)to());
                    return SET_ITEM_ERROR_OTHER_OBJECT_ERROR;
               }
               
               if((val_num_rep >= val_from_num_rep) && (val_num_rep <= val_to_num_rep))
                    return SET_ITEM_TRUE_VALUE;
               else
                    return SET_ITEM_FALSE_VALUE;
          }
     }
     
     return SET_ITEM_FALSE_VALUE;
}
// -----------------------------------------------------------------------------

// #brief Function that calculates the result of substraction
//
// #param hSetItem* __subtrahend - poiter to the substrahend
// #return list of items that are the result of substraction
QList<hSetItem*>* hSetItem::sub(hSetItem* __subtrahend)
{
     if(_parentSet == NULL)
     {
          setLastError(SET_ITEM_ERROR_PARENT_SET_NOT_DEFINED);
          return NULL;
     }
          
     if(_parentSet->parentType() == NULL)
     {
          setLastError(SET_ITEM_ERROR_NOT_ACCESIBLE_TYPE);
          return NULL;
     }
            
     if(__subtrahend->type() == SET_ITEM_TYPE_NOT_DEFINED)
     {
          setLastError(SET_ITEM_ERROR_NOT_DEFINED_ITEM_TYPE);
          return NULL;
     }
     if(type() == SET_ITEM_TYPE_NOT_DEFINED)
     {
          setLastError(SET_ITEM_ERROR_NOT_DEFINED_ITEM_TYPE);
          return NULL;
     }
     
     
     QList<hSetItem*>* res = new QList<hSetItem*>;
     
     // When subtrahend is a single value
     if(__subtrahend->type() == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          hValueResult hvr_itm = __subtrahend->value();
          if(!(bool)hvr_itm)
          {
               setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_itm);
               return NULL;
          }
                    
          int cnt_res = contains((QString)hvr_itm);
          if(cnt_res == SET_ITEM_FALSE_VALUE)
          {
               res->append(copyTo());
               return res;
          }
          else if(cnt_res == SET_ITEM_ANY_VALUE)
          {
               hSetItem* newItem = new hSetItem(_parentSet, SET_ITEM_TYPE_SINGLE_VALUE, new hValue(ANY_VALUE), new hValue, false);
               newItem->typeVerification();
               res->clear();
               res->append(newItem);
               return res;
          }
          else if(cnt_res == SET_ITEM_ND_VALUE)
          {
               hSetItem* newItem = new hSetItem(_parentSet, SET_ITEM_TYPE_SINGLE_VALUE, new hValue, new hValue, false);
               newItem->typeVerification();
               res->clear();
               res->append(newItem);
               return res;
          }
          else if(cnt_res != SET_ITEM_TRUE_VALUE)      // That means error code
          {
               delete res;
               return NULL;
          }
          
          // Otherwise no error appeared and the item contains element
          double subtrahend_num;
          QString prevVal1, prevVal2, nextVal1, nextVal2;
          int smtn_res = _parentSet->parentType()->mapToNumeric((QString)hvr_itm, subtrahend_num);
          
          if(smtn_res != H_TYPE_ERROR_NO_ERROR)
          {
               delete res;
               setLastError(_parentSet->parentType()->lastError());
               return NULL;
          }
          
          // That means this element contains value and is a single value, so after substraction the set is empty
          if(type() == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               return res;
          }
               
          // When the this item is a range
          if(type() == SET_ITEM_TYPE_RANGE)
          {
               int from_2_num_res, to_2_num_res;
               double from_num, to_num;
               
               hValueResult hvr_from = from();
               hValueResult hvr_to = to();
               
               if(!(bool)hvr_from)
               {
                    setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_from);
                    return NULL;
               }
               if(!(bool)hvr_to)
               {
                   setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_to);
                   return NULL;
               }
               
               from_2_num_res = _parentSet->parentType()->mapToNumeric((QString)hvr_from, from_num);
               to_2_num_res = _parentSet->parentType()->mapToNumeric((QString)hvr_to, to_num);

               if((from_2_num_res != H_TYPE_ERROR_NO_ERROR) || 
                  (to_2_num_res != H_TYPE_ERROR_NO_ERROR))
               {
                    setLastError(_parentSet->parentType()->lastError());
                    return NULL;
               }
               
               if(subtrahend_num == from_num) 
               {    
                    int nx_res1 = _parentSet->parentType()->typeNextValue((QString)hvr_itm, nextVal1, false);
                    if(nx_res1 != H_TYPE_ERROR_NO_ERROR)
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem = copyTo();
                    newItem->setFrom(new hValue(nextVal1), false);
                    res->append(newItem);
                    newItem->typeVerification();
               }
               if(subtrahend_num == to_num) 
               {
                    int pv_res1 = _parentSet->parentType()->typePreviousValue((QString)hvr_itm, prevVal1, false);
                    if(pv_res1 != H_TYPE_ERROR_NO_ERROR)
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem = copyTo();
                    newItem->setTo(new hValue(prevVal1), false);
                    res->append(newItem);
                    newItem->typeVerification();
               }
               if((subtrahend_num > from_num) && (subtrahend_num < to_num))
               {
                    int pv_res = _parentSet->parentType()->typePreviousValue((QString)hvr_itm, prevVal1, false);
                    int nx_res = _parentSet->parentType()->typeNextValue(__subtrahend->value(), nextVal1, false);
                    if((pv_res != H_TYPE_ERROR_NO_ERROR) || (nx_res != H_TYPE_ERROR_NO_ERROR))
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem1 = copyTo();
                    hSetItem* newItem2 = copyTo();
                    newItem1->setTo(new hValue(prevVal1), false);
                    newItem2->setFrom(new hValue(nextVal1), false);
                    res->append(newItem1);
                    res->append(newItem2);
                    newItem1->typeVerification();
                    newItem2->typeVerification();
               }
          }
     }
     
     // When subtrahend is a single value
     if(__subtrahend->type() == SET_ITEM_TYPE_RANGE)
     {
          // Otherwise no error appeared and the item contains element
          double subtrahend_from_num, subtrahend_to_num;
          QString prevVal1, prevVal2, nextVal1, nextVal2;
          
          hValueResult hvr_sub_from = __subtrahend->from();
          hValueResult hvr_sub_to = __subtrahend->to();
          
          if(!(bool)hvr_sub_from)
          {
               setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_sub_from);
               return NULL;
          }
          if(!(bool)hvr_sub_from)
          {
               setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_sub_to);
               return NULL;
          }
          
          int smtn_res1 = _parentSet->parentType()->mapToNumeric((QString)hvr_sub_from, subtrahend_from_num);
          int smtn_res2 = _parentSet->parentType()->mapToNumeric((QString)hvr_sub_to, subtrahend_to_num);
          
          if((smtn_res1 != H_TYPE_ERROR_NO_ERROR) || (smtn_res2 != H_TYPE_ERROR_NO_ERROR))
          {
               delete res;
               setLastError(_parentSet->parentType()->lastError());
               return NULL;
          }
          
          // That means __subtrahend contains this element which is single value. After substraction this element is empty
          if(type() == SET_ITEM_TYPE_SINGLE_VALUE)     // < {} > - nothing left
          {
               //QMessageBox::critical(NULL, "HQEd", "sub - range, this - single", QMessageBox::Ok);
               hValueResult hvr_itm_val = value();
          
               if(!(bool)hvr_itm_val)
               {
                    setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_itm_val);
                    return NULL;
               }
                    
               int cnt_res = __subtrahend->contains((QString)hvr_itm_val);
               if(cnt_res == SET_ITEM_FALSE_VALUE)
               {
                    res->append(copyTo());
                    return res;
               }
               else if(cnt_res == SET_ITEM_ANY_VALUE)
               {
                    hSetItem* newItem = new hSetItem(_parentSet, SET_ITEM_TYPE_SINGLE_VALUE, new hValue(ANY_VALUE), new hValue, false);
                    newItem->typeVerification();
                    res->clear();
                    res->append(newItem);
                    return res;
               }
               else if(cnt_res == SET_ITEM_ND_VALUE)
               {
                    hSetItem* newItem = new hSetItem(_parentSet, SET_ITEM_TYPE_SINGLE_VALUE, new hValue, new hValue, false);
                    newItem->typeVerification();
                    res->clear();
                    res->append(newItem);
                    return res;
               }
               else if(cnt_res == SET_ITEM_TRUE_VALUE)      
                    return res;
               else                                         // That means error code
               {
                    delete res;
                    setLastError(__subtrahend->lastError());
                    return NULL;
               }
          }
               
          // When the this item is a range
          if(type() == SET_ITEM_TYPE_RANGE)
          {    
               //QMessageBox::critical(NULL, "HQEd", "sub - range, this - range", QMessageBox::Ok);
               int from_2_num_res, to_2_num_res;
               double from_num, to_num;
               
               hValueResult hvr_from = from();
               hValueResult hvr_to = to();
               
               if(!(bool)hvr_from)
               {
                    setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_from);
                    return NULL;
               }
               if(!(bool)hvr_to)
               {
                    setLastError(SET_ITEM_ERROR_OTHER_OBJECT_ERROR, (QString)hvr_to);
                    return NULL;
               }
               
               from_2_num_res = _parentSet->parentType()->mapToNumeric((QString)hvr_from, from_num);
               to_2_num_res = _parentSet->parentType()->mapToNumeric((QString)hvr_to, to_num);
               
               if((from_2_num_res != H_TYPE_ERROR_NO_ERROR) || 
                  (to_2_num_res != H_TYPE_ERROR_NO_ERROR))
               {
                    setLastError(_parentSet->parentType()->lastError());
                    return NULL;
               }
               
               // [] - subtrahend brackets   // odjemnik
               // () - minued brackets       // odjemna
               
               if((subtrahend_from_num <= from_num) && (subtrahend_to_num >= to_num))  // [ () ] - nothing left
               {
                    //QMessageBox::critical(NULL, "HQEd", "case 1 result", QMessageBox::Ok);
                    return res;
               }
               
               else if((subtrahend_from_num > from_num) && (subtrahend_to_num < to_num))  // ( [] )
               {
                    int pv_res1 = _parentSet->parentType()->typePreviousValue((QString)hvr_sub_from, prevVal1, false);
                    int nx_res1 = _parentSet->parentType()->typeNextValue((QString)hvr_sub_to, nextVal1, false);
                    if((nx_res1 != H_TYPE_ERROR_NO_ERROR) || ((pv_res1 != H_TYPE_ERROR_NO_ERROR)))
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem1 = copyTo();
                    hSetItem* newItem2 = copyTo();
                    newItem1->setTo(new hValue(prevVal1), false);
                    newItem2->setFrom(new hValue(nextVal1), false);
                    res->append(newItem1);
                    res->append(newItem2);
                    newItem1->typeVerification();
                    newItem2->typeVerification();
                    //QMessageBox::critical(NULL, "HQEd", "case 2 result: " + newItem1->toString() + " oraz " + newItem2->toString(), QMessageBox::Ok);
               }
               
               else if((subtrahend_from_num <= from_num) && (subtrahend_to_num >= from_num) && (subtrahend_to_num < to_num))  // [ ( ] )
               {
                    int nx_res1 = _parentSet->parentType()->typeNextValue((QString)hvr_sub_to, nextVal1, false);
                    if(nx_res1 != H_TYPE_ERROR_NO_ERROR)
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem = copyTo();
                    newItem->setFrom(new hValue(nextVal1), false);
                    res->append(newItem);
                    newItem->typeVerification();
                    //QMessageBox::critical(NULL, "Value Editor", "case 3 result: " + newItem->toString(), QMessageBox::Ok);
               }
               
               else if((subtrahend_from_num > from_num) && (subtrahend_from_num <= to_num) && (subtrahend_to_num >= to_num))  // ( [ ) ]
               {
                    int pv_res1 = _parentSet->parentType()->typePreviousValue((QString)hvr_sub_from, prevVal1, false);
                    if(pv_res1 != H_TYPE_ERROR_NO_ERROR)
                    {
                         delete res;
                         setLastError(_parentSet->parentType()->lastError());
                         return NULL;
                    }
                    
                    hSetItem* newItem = copyTo();
                    newItem->setTo(new hValue(prevVal1), false);
                    res->append(newItem);
                    newItem->typeVerification();
                    //QMessageBox::critical(NULL, "Value Editor", "case 4 result: " + newItem->toString(), QMessageBox::Ok);
               }
               
               else if((subtrahend_to_num < from_num) || (subtrahend_from_num > to_num))  // [] () lub () []
               {    
                    hSetItem* newItem = copyTo();
                    res->append(newItem);
                    newItem->typeVerification();
                    //QMessageBox::critical(NULL, "Value Editor", "case 5 result: " + newItem->toString(), QMessageBox::Ok);
               }
          }
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #param ... - the list of others params.
// #return List of all data in hml 2.0 format.
QStringList* hSetItem::out_HML_2_0(int __mode, ...)
{
     QStringList* res = new QStringList;
     
     if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          if((_value1->type() == NOT_DEFINED) || (_value1->type() == ANY_VALUE) || (_value1->type() == VALUE))
          {
               QString line = "<value";
               QStringList* tmp = _value1->out_HML_2_0(__mode, "is");
               line += " " + tmp->at(0);
               delete tmp;
               
               // Zapis numerowania
               if(__mode == 1)
               {
                    _value1->setDefinitionType(NUM);
                    tmp = _value1->out_HML_2_0(__mode, "num");
                    line += " " + tmp->at(0);
                    delete tmp;
                    _value1->setDefinitionType(SYM);
               }
               
               line += "/>";
               *res << line;
          }
          
          if(_value1->type() == ATTRIBUTE)
          {
               QStringList* tmp = _value1->out_HML_2_0(__mode, "ref");
               *res << "<attref " + tmp->at(0) + "/>";
               delete tmp;
          }
          
          if(_value1->type() == EXPRESSION)
          {
               QStringList* tmp = _value1->out_HML_2_0(__mode, "");
               for(int i=0;i<tmp->size();++i)
                    *res << "\t" + tmp->at(i);
               delete tmp;
          }
     }
          
     if(_type == SET_ITEM_TYPE_RANGE)
     {
          QStringList *tmp1 = _value1->out_HML_2_0(__mode, "from"), 
                      *tmp2 = _value2->out_HML_2_0(__mode, "to");
          *res << "<value " + tmp1->at(0) + " " + tmp2->at(0) + "/>";
          
          delete tmp1;
          delete tmp2;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #param ... - the list of others params.
// #return No retur value.
void hSetItem::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode, ...)
{
     if(_type == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          if((_value1->type() == NOT_DEFINED) || (_value1->type() == ANY_VALUE) || (_value1->type() == VALUE))
          {
               _xmldoc->writeEmptyElement("value");
               if(_value1->definitionType() == SYM)
                    _value1->out_HML_2_0(_xmldoc, __mode, "is");
               if(_value1->definitionType() == NUM)
                    _value1->out_HML_2_0(_xmldoc, __mode, "num");
               
               if(__mode == 1)     // saving numbering
               {
                    _value1->setDefinitionType(NUM);
                    _value1->out_HML_2_0(_xmldoc, __mode, "num");
                    _value1->setDefinitionType(SYM);
               }
          }
          
          if(_value1->type() == ATTRIBUTE)
          {
               _xmldoc->writeEmptyElement("attref");
               _value1->out_HML_2_0(_xmldoc, __mode, "ref");
          }
          
          if(_value1->type() == EXPRESSION)
               _value1->out_HML_2_0(_xmldoc, __mode, "");
     }
          
     if(_type == SET_ITEM_TYPE_RANGE)
     {
          _xmldoc->writeEmptyElement("value");
          _value1->out_HML_2_0(_xmldoc, __mode, "from");
          _value2->out_HML_2_0(_xmldoc, __mode, "to");
     }
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hSetItem::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr)
{
     int result = 0;
     
     if(_value1 == NULL)
          _value1 = new hValue;
     if(_value2 == NULL)
          _value2 = new hValue;
     setParentType(_parentSet->parentType());
          
     _type = SET_ITEM_TYPE_SINGLE_VALUE;
     
     // When defined as value
     if(__root.tagName().toLower() == "value")
     {
          if(__root.hasAttribute("num"))
          {
               _value1->setDefinitionType(NUM);
               result += _value1->in_HML_2_0(__root, __msg, __attr, "num");
          }
          if(__root.hasAttribute("is"))
          {
               _value1->setDefinitionType(SYM);
               result += _value1->in_HML_2_0(__root, __msg, __attr, "is");
          }
          
          if((__root.hasAttribute("from")) && (__root.hasAttribute("to")))
          {
               _type = SET_ITEM_TYPE_RANGE;
               result += _value1->in_HML_2_0(__root, __msg, __attr,"from");
               result += _value2->in_HML_2_0(__root, __msg, __attr, "to");
          }
     }
     if(__root.tagName().toLower() != "value")
          result += _value1->in_HML_2_0(__root, __msg, __attr, "");
     
     return result;
}
// -----------------------------------------------------------------------------
