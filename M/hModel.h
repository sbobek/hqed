/**
 * \file	hModel.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	01.07.2009
 * \version	1.0
 * \brief	This file contains class definition that holds data of ARD and XTT model
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HMODELH
#define HMODELH
// -----------------------------------------------------------------------------

#include <QObject>
#include <QAction>

#include "../C/MainWin_ui.h"
// -----------------------------------------------------------------------------

class ARD_LevelList;
class ARD_PropertyList;
class ARD_AttributeList;
class ARD_DependencyList;

class XTT_States;
class XTT_TableList;
class XTT_AttributeGroups;
class XTT_ConnectionsList;

class hTypeList;

class ardGraphicsScene;
class MyGraphicsScene;
class StateGraphicsScene;
// -----------------------------------------------------------------------------

/**
* \class 	hModel
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	01.07.2009
* \brief 	Class definition that holds data of ARD and XTT model
*/
class hModel : public QObject
{
     Q_OBJECT
     
private:

     // The ARD model data
     ARD_LevelList* _ardLevelList;                ///< The level list of the ARD diagram
     ARD_DependencyList* _ardDependencyList;      ///< The list of ARD dependecies
     ARD_AttributeList* _ardAttributeList;        ///< The list of ARD attributes
     ARD_PropertyList* _ardPropertyList;          ///< The list of ARD properties
     QString _ardFileName;                        ///< The name of the ARD file
     bool _isARDFile;                             ///< Indicates if the _ardFileName is the name of the file
     ardGraphicsScene* _ard_modelScene;           ///< Pointer to scene of ARD drawing.
     ardGraphicsScene* _ard_thpScene;             ///< Pointer to scene of TPH drawing.
     ardGraphicsScene* _ardCurrentARDscene;       ///< Pointer to the current ARD scene
     
     // The XTT model data
     XTT_States* _xttStates;                      ///< The map of XTT states
     XTT_States* _xttOutputStates;                ///< The map of XTT output states
     XTT_TableList* _xttTableList;                ///< The list of XTT tables
     XTT_AttributeGroups* _xttAttributeGroups;    ///< The list of XTT attribute groups
     XTT_ConnectionsList* _xttConnectionsList;    ///< The list of XTT connections
     hTypeList* _xttTypeList;                     ///< The list of XTT types
     QString _xttFileName;                        ///< The name of the XTT file
     bool _isXTTFile;                             ///< Indicates if the _ardFileName is the name of the file
     MyGraphicsScene* _xtt_scene;                 ///< Pointer to scene of XTT drawing.
     StateGraphicsScene* _xtt_state_scene;        ///< Pointer to scene of XTT drawing.
     
     bool _dockWidgetsVisibleStatus[DOCKED_WIDGETS];///< The array of values that indicates the visibility of the docked widgets
     int _appMode;                                ///< Determines the mode of program (0 - editor ARD,1 - editor XTT).
     
     QAction* _menuButton;                        ///< Pointer to the ARD menu button
     
     QString _modelID;                            ///< UniqeModelID

public:

     /// \brief Default construcotr of the class
     hModel(void);
     
     /// \brief Destructor of the class
     ~hModel(void);
     
     /// \brief function that infroms all the necessary object about model
     ///
     /// \return no values return
     void registerModel(void);
     
     /// \brief function that clears the model
     ///
     /// \return no values return
     void flush(void);
     
     /// \brief Sets the change status for XTT model
     ///
     /// \param __c - the status value
     /// \return no values return
     void setXttChanged(bool __c);
     
     /// \brief Sets the change status for ARD model
     ///
     /// \param __c - the status value
     /// \return no values return
     void setArdChanged(bool __c);
     
     /// \brief Sets the change status for whole model
     ///
     /// \param __c - the status value
     /// \return no values return
     void setChanged(bool __c);
     
     /// \brief Sets the application mode
     ///
     /// \param __newMode - mode identifier ARD_MODE or XTT_MODE
     /// \return no values return
     void setAppMode(int __newMode);
     
     /// \brief Sets the model ID
     ///
     /// \param __newID - ID of the model
     /// \return no values return
     void setModelID(QString __newID);
     
     /// \brief Sets the status of visibility of DockedWindows
     ///
     /// \param __index - window index
     /// \param __status - new status
     /// \return no values return
     void setDockWidgetsVisibleStatus(int __index, bool __status);
     
     /// \brief Sets the name of ARD model file name
     ///
     /// \param __filename - new file name
     /// \return no values return
     void setARDfileName(QString __filename);
     
     /// \brief Sets the name of XTT model file name
     ///
     /// \param __filename - new file name
     /// \return no values return
     void setXTTfileName(QString __filename);
     
     /// \brief Sets isARDfile flag
     ///
     /// \param __v - new value of the flag
     /// \return no values return
     void setARDisFile(bool __v);
     
     /// \brief Sets isXTTfile flag
     ///
     /// \param __v - new value of the flag
     /// \return no values return
     void setXTTisFile(bool __v);
     
     /// \brief return the status of change of XTT model
     ///
     /// \return the status of change of XTT model
     bool xttChanged(void);
     
     /// \brief return the status of change of ARD model
     ///
     /// \return the status of change of ARD model
     bool ardChanged(void);
     
     /// \brief return the status of change of whole model
     ///
     /// \return the status of change of whole model
     bool changed(void);
     
     /// \brief returns the pointer to the ARD level list
     ///
     /// \return the pointer to the ARD level list
     ARD_LevelList* ardLevelList(void);
     
     /// \brief returns the pointer to the ARD dependency list
     ///
     /// \return the pointer to the ARD dependency list
     ARD_DependencyList* ardDependencyList(void);
     
     /// \brief returns the pointer to the ARD attributes list
     ///
     /// \return the pointer to the ARD attributes list
     ARD_AttributeList* ardAttributeList(void);
     
     /// \brief returns the pointer to the ARD properties list
     ///
     /// \return the pointer to the ARD properties list
     ARD_PropertyList* ardPropertyList(void);
     
     /// \brief returns the file name of the ARD model
     ///
     /// \return file name of the ARD model
     QString ardFileName(void);   
     
     /// \brief returns if the ardFileName is a file
     ///
     /// \return if the ardFileName is a file
     bool isARDFile(void);
     
     /// \brief returns the pointer to the ARD model scene
     ///
     /// \return the pointer to the ARD model scene
     ardGraphicsScene* ard_modelScene(void);
     
     /// \brief returns the pointer to the ARD tph scene
     ///
     /// \return the pointer to the ARD tph scene
     ardGraphicsScene* ard_thpScene(void);
     
     /// \brief returns the pointer to the current ARD scene
     ///
     /// \return the pointer to the current ARD scene
     ardGraphicsScene* ardCurrentARDscene(void);
     
     /// \brief returns the pointer to the list of XTT states
     ///
     /// \return pointer to the list of XTT states
     XTT_States* xttStates(void);
     
     /// \brief returns the pointer to the list of output XTT states
     ///
     /// \return pointer to the list of output XTT states
     XTT_States* xttOutputStates(void);
     
     /// \brief returns the pointer to the list of XTT attribute groups
     ///
     /// \return pointer to the list of XTT attribute groups
     XTT_AttributeGroups* xttAttributeGroups(void);
     
     /// \brief returns the pointer to the list of XTT tables
     ///
     /// \return pointer to the list of XTT tables
     XTT_TableList* xttTableList(void);
     
     /// \brief returns the pointer to the list of XTT connections
     ///
     /// \return pointer to the list of XTT connections
     XTT_ConnectionsList* xttConnectionsList(void);
     
     /// \brief returns the pointer to the list of XTT types
     ///
     /// \return pointer to the list of XTT types
     hTypeList* xttTypeList(void);
     
     /// \brief returns the XTT file name
     ///
     /// \return the XTT file name
     QString xttFileName(void);
     
     /// \brief returns if the xttFileName is a file
     ///
     /// \return if the xttFileName is a file
     bool isXTTFile(void);
     
     /// \brief returns the pointer to the XTT scene
     ///
     /// \return pointer to the XTT scene
     MyGraphicsScene* xtt_scene(void);
     
     /// \brief returns the pointer to the state XTT scene
     ///
     /// \return pointer to the state XTT scene
     StateGraphicsScene* xtt_state_scene(void);

     /// \brief returns sth visibility status of the docked window
     ///
     /// \param __index - index of the docked window
     /// \return true - if window is visible, otherwise false
     bool dockWidgetsVisibleStatus(int __index);   
     
     /// \brief returns the application mode
     ///
     /// \return ARD_MODE or XTT_MODE
     int appMode(void);
     
     /// \brief returns the title of the model
     ///
     /// \return the title of the model
     QString modelTitle(void);
     
     /// \brief returns the ID of the model
     ///
     /// \return the ID of the model
     QString modelID(void);
     
     /// \brief function triggered when model becomes active
     ///
     /// \return no values return
     void activate(void);
     
     /// \brief function triggered when model becomes inactive
     ///
     /// \return no values return
     void deactivate(void);
     
public slots:

     /// \brief function triggered when active event occurs
     ///
     /// \return no values return
     void onActivate(void);
     
signals:

     /// \brief signal emited when activate event is recieved
     ///
     /// \param __pointer - pointer to the object that received signal
     /// \return no values return
     void activte(hModel* __pointer);
};
// -----------------------------------------------------------------------------
#endif
