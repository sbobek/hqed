/*
 *	   $Id: hValue.cpp,v 1.18.4.2.2.1 2011-09-01 19:32:10 hqeddoxy Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../C/Settings_ui.h"
#include "../namespaces/ns_hqed.h"

#include "XTT/XTT_Attribute.h"
#include "XTT/XTT_AttributeGroups.h"

#include "hSet.h"
#include "hType.h"
#include "hSetItem.h"
#include "hFunction.h"
#include "hExpression.h"
#include "hMultipleValue.h"
#include "hMultipleValueResult.h"
#include "hValue.h"
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
hValue::hValue(void)
{
     _type = NOT_DEFINED;
     _defType = SYM;
     _sym_value = XTT_Attribute::_not_definded_operator_;
     _num_value = XTT_Attribute::_not_definded_operator_;
     _attribute = NULL;
     _expression = new hExpression;
}
// -----------------------------------------------------------------------------

// #brief Copying constructor
//
// #param hValue* __source - the source object
hValue::hValue(hValue* __source)
{
     _type = __source->_type;
     _defType = __source->_defType;
     _sym_value = __source->_sym_value;
     _num_value = __source->_num_value;
     _attribute = __source->_attribute;
     _expression = __source->_expression->copyTo();
}
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
//
// #param int __type - the type of value definition
hValue::hValue(ValueType __type)
{
     _type = __type;
     _defType = SYM;
     _sym_value = XTT_Attribute::_not_definded_operator_;
     _num_value = XTT_Attribute::_not_definded_operator_;
     if(_type == ANY_VALUE)
     {
          _sym_value = XTT_Attribute::_any_operator_;
          _num_value = XTT_Attribute::_any_operator_;
     }
     
     _attribute = NULL;
     _expression = new hExpression;
}
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
//
// #param QString __value - value of the item
hValue::hValue(QString __value)
{
     _type = VALUE;
     _defType = SYM;
     if(__value.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          _type = NOT_DEFINED;
     if(__value.toLower() == XTT_Attribute::_any_operator_.toLower())
          _type = ANY_VALUE;
          
     _attribute = NULL;
     _expression = new hExpression;
          
     _sym_value = __value;
     _num_value = "";
}
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
//
// #param QString __symvalue - symbolic value of the item
// #param QString __numvalue - numeric value of the item
hValue::hValue(QString __symvalue, QString __numvalue)
{
     _type = VALUE;
     _defType = SYM;
     if(__symvalue.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
          _type = NOT_DEFINED;
     if(__symvalue.toLower() == XTT_Attribute::_any_operator_.toLower())
          _type = ANY_VALUE;
          
     _attribute = NULL;
     _expression = new hExpression;
          
     _sym_value = __symvalue;
     _num_value = __numvalue;
}
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
//
// #param XTT_Attribute* __xtt_attribute - attribute reference
hValue::hValue(XTT_Attribute* __xtt_attribute)
{
     _type = ATTRIBUTE;
     _defType = SYM;
     _sym_value = "";
     _num_value = "";
     _attribute = __xtt_attribute;
     _expression = new hExpression;
}
// -----------------------------------------------------------------------------

// #brief Constructor hValue class.
//
// #param *__expression - pointer to the expression object
hValue::hValue(hExpression* __expression)
{
     _type = EXPRESSION;
     _defType = SYM;
     _sym_value = "";
     _num_value = "";
     _attribute = NULL;
     _expression = __expression;
}
// -----------------------------------------------------------------------------

// #brief Destrucotr hValue class.
hValue::~hValue(void)
{    
     if(_expression != NULL)
          delete _expression;
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hValue* hValue::copyTo(hValue* __destinationItem)
{
     hValue* res = __destinationItem;
     if(res == NULL)
          res = new hValue;
          
     res->_type = _type;
     res->_defType = _defType;
     res->_sym_value = _sym_value;
     res->_num_value = _num_value;
     res->_attribute = _attribute;
     _expression->copyTo(res->_expression);
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the type of the value
//
// #param int __type - new value of type
// #return No value return
void hValue::setType(ValueType __type)
{          
     _type = __type;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the type of the value definition
//
// #param int __defType - new type of value definition
// #return No value return
void hValue::setDefinitionType(ValueDefinitionType __defType)
{
     _defType = __defType;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the value
//
// #param QString __value - the new value
// #return No value return
void hValue::setValue(QString __value)
{
     if(_defType == SYM)
          _sym_value = __value;
     if(_defType == NUM)
          _num_value = __value;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the attribute pointer
//
// #param __attr_ptr - the pointer to the attribute
// #return No value return
void hValue::setAttribute(XTT_Attribute* __attr_ptr)
{
     _attribute = __attr_ptr;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the poiter to the expression object
//
// #param hExpression* __expression - pointer to the new expression object
// #return No value return
void hValue::setExpression(hExpression* __expression)
{
     if((_expression != NULL) && (_expression != __expression))
          delete _expression;
          
     _expression = __expression;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value of this object as string
//
// #return The current value of this object as string
hValueResult hValue::value(void)
{
     hValueResult res;
     hMultipleValueResult mvres;
     
     hMultipleValue* mv;
     
     switch(_type)
     {
          case NOT_DEFINED:
               res = hValueResult(XTT_Attribute::_not_definded_operator_);
               break;
               
          case ANY_VALUE:
               res = hValueResult(XTT_Attribute::_any_operator_);
               break;
               
          case VALUE:
                if(_defType == SYM)
                    res = hValueResult(_sym_value);
                if(_defType == NUM)
                    res = hValueResult(_num_value);
               break;
               
          case ATTRIBUTE:
               if(_attribute->multiplicity())
                    res = hValueResult("Attribute " + _attribute->Name() + " is not single valued.", false);
               if(!_attribute->multiplicity())
               {
                    hMultipleValueResult mvr = _attribute->Value()->value();
                    if(!(bool)mvr)
                         res = hValueResult((QString)mvr, false);
                    if((bool)mvr)
                         res = hValueResult(mvr.toSet(false)->toValue());
               }
               break;
               
          case EXPRESSION:
               mv = _expression->calculate();
               if(mv == NULL)
                    res = hValueResult("Expression error: " + _expression->lastError(), mv != NULL);
               if(mv != NULL)
               {
                    mvres = mv->value();
                    bool mvresok = (bool)mvres;
                    if(mvresok)
                         res = hValueResult(mvres.toSet(false)->toValue(), mvresok);
                    if(!mvresok)
                         res = hValueResult(((QString)mvres), mvresok);
               }
               break;
               
          default:
               res = hValueResult(XTT_Attribute::_not_definded_operator_);
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the form of the value as value, attribute name, expression form as string
//
// #return The form of the value as value, attribute name, expression form as string
QString hValue::toString(void)
{
     QString res = "";
     QString (XTT_Attribute::*reffunc)(void) = Settings->attsReferenceByAcronyms() == true ? &XTT_Attribute::Acronym : &XTT_Attribute::Name;
     
     switch(_type)
     {
          case NOT_DEFINED:
               res = XTT_Attribute::_not_definded_operator_;
               break;
               
          case ANY_VALUE:
               res = XTT_Attribute::_any_operator_;
               break;
               
          case VALUE:
               if(_defType == SYM)
                    res = _sym_value;
               if(_defType == NUM)
                    res = _num_value;
               break;
               
          case ATTRIBUTE:
               res = AttributeGroups->CreatePath(_attribute) + (_attribute->*reffunc)();
               break;
               
          case EXPRESSION:
               res = _expression->toString();
               break;
               
          default:
               res = "unsupported type of value";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the form of the value as value, attribute name, expression form in drools5 format
//
// #return The form of the value as value, attribute name, expression form in drools5 format
QString hValue::toDrools5(void)
{
     QString res = "";

     switch(_type)
     {
          case NOT_DEFINED:
               res = XTT_Attribute::_not_definded_operator_;
               break;

          case ANY_VALUE:
               res = XTT_Attribute::_any_operator_;
               break;

          case VALUE:
               if(_defType == SYM)
                    res = _sym_value;
               if(_defType == NUM)
                    res = _num_value;
               break;

          case ATTRIBUTE:
               res = "w.get" + _attribute->CapitalizedName() + "()";
               break;

          case EXPRESSION:
               res = _expression->toDrools5();
               break;

          default:
               res = "unsupported type of value";
               break;
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the item to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this item as string.
QString hValue::toProlog(int __option)
{
     QString res = "";
     switch(_type)
     {
          case NOT_DEFINED:
               res = hqed::mcwp(XTT_Attribute::_not_definded_operator_);
               break;
               
          case ANY_VALUE:
               res = hqed::mcwp(XTT_Attribute::_any_operator_);
               break;
               
          case VALUE:
               if((_defType == SYM) && (__option == -1))
                    res = hqed::mcwp(_sym_value);
               if((_defType == NUM) && (__option == -1))
                    res = hqed::mcwp(_num_value);
               if((__option == 1))
                    res = hqed::mcwp(_sym_value) + "/" + hqed::mcwp(_num_value);
               break;
               
          case ATTRIBUTE:
               res = hqed::mcwp(_attribute->Path());
               break;
               
          case EXPRESSION:
               res = _expression->toProlog();
               break;
               
          default:
               res = "unsupported type of value";
               break;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the attribute field
//
// #return the string representation of the attribute field
QString hValue::attributeFieldToString(void)
{
     if(_attribute == NULL)
          return "Attribute is not selected";
 
     QString res = AttributeGroups->CreatePath(_attribute) + _attribute->Name();
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hValue::isAVRused(void)
{
     if(_type == ATTRIBUTE)
          return true;
     if(_type == EXPRESSION)
          return _expression->isAVRused();
     
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hValue::isExpressionUsed(void)
{
     if(_type == EXPRESSION)
          return true;
     
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the value objest definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hValue::valueVerification(QString& __error_message)
{
     bool res = true;
     
     if(_type == ATTRIBUTE)
     {
          // Here attribute must be single
          if(_attribute->multiplicity())
          {
               __error_message += "\nincorrect attribute multiplicity.\nAllowed multiplicity is \'" + XTT_Attribute::_class_attribute_.at(0) + "\'.";
               res = false;
          }
          
          return _attribute->attributeVerification(__error_message) && res;
     }

     if(_type == EXPRESSION)
          return _expression->expressionVerification(__error_message) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hValue::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;

     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* att_ptr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          // Parent type update
          if((_type == ATTRIBUTE) && (_attribute == att_ptr))
          {
               if(att_ptr == NULL)
                    _type = NOT_DEFINED;
          }
          
          if(_type == ATTRIBUTE)
               res = _attribute->eventMessage(__errmsgs, __event, att_ptr) && res;

          if(_type == EXPRESSION)
               res = _expression->eventMessage(__errmsgs, __event, att_ptr) && res;
     } 
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* att_ptr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          // Parent type update
          if((_type == ATTRIBUTE) && (_attribute == att_ptr))
          {
               _type = NOT_DEFINED;
               _attribute = NULL;
          }
          
          if(_type == ATTRIBUTE)
               res = _attribute->eventMessage(__errmsgs, __event, att_ptr) && res;

          if(_type == EXPRESSION)
               res = _expression->eventMessage(__errmsgs, __event, att_ptr) && res;
     } 
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the set object by using hValue object. The single object becomes a member of the set
//
// #param hType* __parent_type - pointer to the parent type of the set
// #param hValue* __set_item - pointer to the source item
// #return Return the pointer to the set is succes, otherwise NULL
hSet* hValue::toSet(hType* __parent_type, hValue* __set_item)
{
     if(/*(__parent_type == NULL) || */(__set_item == NULL))
          return NULL;

     hSet* result = new hSet(__parent_type);
     hSetItem* si = new hSetItem(result, SET_ITEM_TYPE_SINGLE_VALUE, __set_item, new hValue, false);
     result->append(si);

     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of value types as string representation
//
// #return Return the list of value types as string representation
QStringList hValue::stringTypes(void)
{
     return QStringList() << XTT_Attribute::_not_definded_operator_ << XTT_Attribute::_any_operator_ << "value" << "attref" << "expression";
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #param QString __elementName - the name of the tag element
// #return List of all data in hml 2.0 format.
QStringList* hValue::out_HML_2_0(int, QString __elementName)
{
     QStringList* res = new QStringList;
     res->clear();
     
     if(_type == NOT_DEFINED)
          *res << __elementName + "=\"" + XTT_Attribute::_not_definded_operator_ + "\"";
     if(_type == ANY_VALUE)
          *res << __elementName + "=\"" + XTT_Attribute::_any_operator_ + "\"";
     if(_type == VALUE)
     {
          if(definitionType() == SYM)
               *res << __elementName + "=\"" + _sym_value + "\"";
          if(definitionType() == NUM)
               *res << __elementName + "=\"" + _num_value + "\"";
     }
     if(_type == ATTRIBUTE)
          *res << __elementName + "=\"" + _attribute->Id() + "\"";
     if(_type == EXPRESSION)
     {
          QStringList* expr = _expression->out_HML_2_0(2);
          for(int i=0;i<expr->size();++i)
               *res << expr->at(i);
          delete expr;
     }

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #param QString __elementName - the name of the tag element
// #return No retur value.
void hValue::out_HML_2_0(QXmlStreamWriter* _xmldoc, int, QString __elementName)
{
     if(_type == NOT_DEFINED)
          _xmldoc->writeAttribute(__elementName, XTT_Attribute::_not_definded_operator_);
     if(_type == ANY_VALUE)
          _xmldoc->writeAttribute(__elementName, XTT_Attribute::_any_operator_);
     if(_type == VALUE)
     {
          if(definitionType() == SYM)
               _xmldoc->writeAttribute(__elementName, _sym_value);
          if(definitionType() == NUM)
               _xmldoc->writeAttribute(__elementName, _num_value);
     }
     if(_type == ATTRIBUTE)
          _xmldoc->writeAttribute(__elementName, _attribute->Id());
     if(_type == EXPRESSION)
          _expression->out_HML_2_0(_xmldoc, 2);
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #param __att - denotes which attribute must read
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hValue::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, QString __att)
{
     int result = 0;
     
     if(__root.tagName().toLower() == "value")
     {
          _type = VALUE;
          QString val = __root.attribute(__att, XTT_Attribute::_not_definded_operator_);      
          
          if(val.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
               _type = NOT_DEFINED;
          if(val.toLower() == XTT_Attribute::_any_operator_.toLower())
               _type = ANY_VALUE;
              
          setValue(val);
     }
     
     if(__root.tagName().toLower() == "attref")
     {
          _type = ATTRIBUTE;
          QString ref = __root.attribute("ref", "");
          
          if(ref == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined reference to attribute." + hqed::createDOMlocalization(__root), 0, 2);
               result++;
          }
          AttributeGroups->FindAttribute(ref);
     }
     
     if(__root.tagName().toLower() == "expr")
     {
          _type = EXPRESSION;
          result += _expression->in_HML_2_0(__root, __msg, __attr);
     }
     
     return result;
}
// -----------------------------------------------------------------------------
