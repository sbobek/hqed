 /**
 * \file	hDomain.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008 
 * \version	1.15
 * \brief	This file contains class definition that represents the domain of the XTT attribute.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HDOMAINH
#define HDOMAINH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif

#include "hSet.h"
// -----------------------------------------------------------------------------

class hType;
class hValue;
class hSetItem;
class hValueResult;
// -----------------------------------------------------------------------------

/**
* \class 	hDomain
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that represents the domain of the XTT attribute.
*/
class hDomain : public hSet
{
private:

     bool _orderd;                      ///< Denotes if the domain is ordered
     QList<int> _numbering;             ///< The numerical values that are equivalent to symblic values

public:

     /// \brief Constructor hSet class.
     hDomain(void);
     
     /// \brief Constructor hDomain class.
     ///
     /// \param *__parentType - poiter to the type of data that the set can contain
     hDomain(hType* __parentType);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     virtual hSet* copyTo(hSet* __destinationItem = NULL);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     virtual hDomain* copyToDomain(hDomain* __destinationItem = NULL);
     
     /// \brief Function that returns if the domain is the ordered set
	///
	/// \return true if the domain is ordered otherwise false
     bool ordered(void){ return _orderd; }
     
     /// \brief Function that sets the _ordered field
	///
     /// \param __ordered - new value of field
	/// \return No value return.
     void setOrdered(bool __ordered){ _orderd = __ordered; }
     
     /// \brief Function that sets the parent type of the set
     ///
     /// \param *__pt - poiter to the type
     /// \return true on succes, otherwise false
     virtual bool setParentType(hType* __pt);
     
     /// \brief The functions that adds the value to the set
     ///
     /// \param *__value - pointer to the value that should be added
     /// \return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
     virtual int add(hValue* __value);
     
     /// \brief The functions that adds the item to the set
     ///
     /// \param *__item - item that should be added
     /// \return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
     virtual int add(hSetItem* __item);
     
     /// \brief The functions that adds the item to the set
     ///
     /// \param __index - item position
     /// \param *__item - item that should be inserted
     /// \return no values return
     virtual void insertItem(int __index, hSetItem* __item);
     
     /// \brief The functions that removes the item with given index from the set
     ///
     /// \param __index - idenx of item that should be deleted
     /// \return true if __index is correct otherwise false
     virtual bool deleteItem(int __index);
     
     /// \brief Function that returns the string representation of the set
	///
	/// \return The string representation of the set
     virtual QString toString(void);
     
     /// \brief Function that returns the string representation of the set item
	///
     /// \param __index - index of the item
	/// \return The string representation of the set item
     QString toString(int __index);
     
     /// \brief Function that returns the string list representation of the items
	///
	/// \return The string list representation of the items
     virtual QStringList stringItems(void);
     
     /// \brief Function that returns the set of single values of domain
	///
	/// \return the set of single values of domain
     hSet* expand(void);
     
     /// \brief Function that returns if the domain can be expanded in to single values
	///
	/// \return true on yes, false on no
     bool canBeExpanded(void);
     
     /// \brief Function that returns if it is possible to create menu that defines ranges
	///
     /// \param __singleRequirement - indicates if the external dialog requires single value
	/// \return true on yes, false on no
     bool canCreateMenu(bool __singleRequirement);
     
     /// \brief The functions swaps two items
     ///
     /// \param __index1 - index of the first item
     /// \param __index2 - index of the second item
     /// \return no values return
     void swapItems(int __index1, int __index2);
     
     /// \brief The virtual function that return the index of the first item that contains given value
     ///
     /// \param __value - the value as string
     /// \return index of the first item that contains given value
     virtual int contain(QString __value);
     
     /// \brief The functions that checks if the item is in domain
     ///
     /// \param *__item - pointer to the item
     /// \return H_SET_TRUE_VALUE if item is in set otherwise H_SET_FALSE_VALUE. On error returns error code.
     virtual int contain(hSetItem* __item);
     
     /// \brief The functions that checks if the set __item is a sub set of this
     ///
     /// \param *__item - pointer to the set
     /// \return H_SET_TRUE_VALUE if item is a sub set of this set otherwise H_SET_FALSE_VALUE. On error returns error code.
     virtual int contain(hSet* __item);
     
     /// \brief The function that return the numeric equivalent of the __value. This function can be used only with ordered symbolic domains.
     ///
     /// \param __value - the value as string
     /// \param __exists - the boolean value where the status of exit is stored
     /// \return numeric equivalent of the __value.
     int toNumericEquivalent(QString __value, bool& __exists);
     
     /// \brief Function that return the number on the given position
	///
     /// \param __index - the index of value
	/// \return number on the given position
     int numberEquivalent(int __index);
     
     /// \brief Function that return the index of the item that is equivalent to numeric __value
	///
     /// \param __value - the numeric equivalent of item
	/// \return index of the item that is equivalent to numeric __value
     int symbolicEquivalent(int __value);
     
     /// \brief The function that return the String equivalent of the __value. This function can be used only with ordered symbolic domains.
     ///
     /// \param __value - the value as integer
     /// \return string value
     hValueResult fromNumericEquivalent(int __value);
     
     /// \brief The function that return the next value (not index) w.r.t. __ref_value. The function can be used only with SYMBOLIC BASED domains
     ///
     /// \param __ref_value - the reference value as integer
     /// \param __exists - reference to the boolean value, that holds the result of function. If true then returned value is correct, otherwise is incorrect
     /// \return the next value w.r.t. __ref_value
     int nextNumericValue(int __ref_value, bool& __exists);
     
     /// \brief The function that return the previous value (not index) w.r.t. __ref_value. The function can be used only with SYMBOLIC BASED domains
     ///
     /// \param __ref_value - the reference value as integer
     /// \param __exists - reference to the boolean value, that holds the result of function. If true then returned value is correct, otherwise is incorrect
     /// \return the previous value w.r.t. __ref_value
     int prevNumericValue(int __ref_value, bool& __exists);
     
     /// \brief Function that returns the maximum numeric value
     ///
     /// \return the maximum numeric value
     int maxNumericValue(void);
     
     /// \brief The function that updates the setItems w.r.t. numbering, it renumber all items again only if the type is apropriate.
     ///
     /// \return on succes, otherwise false
     bool updateItemsNumbering(void);
     
     /// \brief The function that updates the numbering using setItems, it creates new number list only if the type is apropriate.
     ///
     /// \return on succes, otherwise false
     bool updateNumbering(void);
     
     /// \brief The function that creates numbering of the items, or updates it
     ///
     /// \return on succes, otherwise false
     bool recreateNumbering(void);
     
     /// \brief Function that returns if the type object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool setVerification(QString& __error_message);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     virtual QString toProlog(int __option = -1);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
	/// \return List of all data in hml 2.0 format.
	virtual QStringList* out_HML_2_0(void);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
	/// \return No retur value.
	virtual void out_HML_2_0(QXmlStreamWriter* _xmldoc);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	virtual int in_HML_2_0(QDomElement& __root, QString& __msg);
     
     /// \brief Function that creates the menu that allows for selecting ranges
	///
     /// \param __title - title of the menu
     /// \param __cmd - command that is executed after action selecting
	/// \return pointer to the menu
     QMenu* createRangeMenu(QString __title, QString __cmd);
};
// -----------------------------------------------------------------------------
#endif
