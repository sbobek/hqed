#include "hEcmaScript.h"
#include "hType.h"
// -----------------------------------------------------------------------------
static bool handleResult(const QScriptEngine& engine, const QScriptValue& res, int& resultCode, QString& resultString);

// -----------------------------------------------------------------------------
// #brief Function that returns the name of the script translation value
//
// #return the name of the script translation value
QString hEcmaScript::scriptTranslationVariable(void)
{
     return "TVAL";
}

// -----------------------------------------------------------------------------
bool hEcmaScript::parseInput(const QString &__scriptContent, const QString &__inValue, QString &__outValue, int &__resultCode)
{
     return parse(QString("input"), __scriptContent, __inValue, __outValue, __resultCode);
}

bool hEcmaScript::parseOutput(const QString &__scriptContent, const QString &__inValue, QString &__outValue, int &__resultCode)
{
     return parse(QString("output"), __scriptContent, __inValue, __outValue, __resultCode);
}

bool hEcmaScript::inputFunctionExists(const QString &__scriptContent)
{
     return functionExists(QString("input"), __scriptContent);
}

bool hEcmaScript::outputFunctionExists(const QString &__scriptContent)
{
     return functionExists(QString("output"), __scriptContent);
}

bool hEcmaScript::parse(QString __parseFunctionName, const QString &__scriptContent, const QString &__inValue, QString &__outValue, int &__ok)
{
#if QT_VERSION < 0x040300

     return "Required version of Qt is 4.3.";

#elif QT_VERSION >= 0x040300

     QScriptEngine engine;
     bool ret = true;

     __ok = H_TYPE_ERROR_NO_ERROR;

     try {
          QScriptValue scriptResult = engine.evaluate(__scriptContent);
          checkResult(engine, scriptResult);

          QScriptValue parseFunction = engine.globalObject().property(__parseFunctionName);

          if(!parseFunction.isValid()) {
              QString msg = "No " + __parseFunctionName + " function in presentation plugin has been found.";
              throw hCheckResultException(H_TYPE_ERROR_OTHER_OBJECT_ERROR, msg);
          }

          QScriptValue parseResult = parseFunction.call(QScriptValue(), QScriptValueList() << __inValue);
          checkResult(engine, parseResult);

          __outValue = parseResult.toString();
     } catch (hCheckResultException e) {
          __ok = e.getCode();
          __outValue = e.getMessage();
         ret = false;
     }

     return ret;
#endif
}

bool hEcmaScript::functionExists(QString __parseFunctionName, const QString &__scriptContent)
{
#if QT_VERSION < 0x040300

     return false;

#elif QT_VERSION >= 0x040300

     QScriptEngine engine;
     bool ret = false;

     try {
          QScriptValue scriptResult = engine.evaluate(__scriptContent);
          checkResult(engine, scriptResult);

          QScriptValue parseFunction = engine.globalObject().property(__parseFunctionName);

          ret = parseFunction.isValid();
     } catch (hCheckResultException e) {
         ret = false;
     }

     return ret;
#endif
}
void hEcmaScript::checkResult(const QScriptEngine& __engine, const QScriptValue& __result) {
     int resultCode;
     QString resultString;

     if(__engine.hasUncaughtException())
     {
           resultCode = H_TYPE_ERROR_OTHER_OBJECT_ERROR;
           resultString = "Exception Occured: " + __result.toString() + "\n";
           resultString += "Uncaught Exception found on line: " + QString::number(__engine.uncaughtExceptionLineNumber()) + "\nException Backtrace:";
           foreach(QString backtraceLine, __engine.uncaughtExceptionBacktrace())
                 resultString += "\n" + backtraceLine;

           throw hCheckResultException(resultCode, resultString);
     }
}
