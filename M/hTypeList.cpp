/*
 *	   $Id: hTypeList.cpp,v 1.15.8.2 2011-07-05 07:06:43 hqedtypes Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "../namespaces/ns_ard.h"
#include "../namespaces/ns_hqed.h"

#include "XTT/XTT_States.h"
#include "XTT/XTT_TableList.h"
#include "XTT/XTT_AttributeGroups.h"

#include "hType.h"
#include "hsTypes/hstBoolean.h"
#include "hsTypes/hstInteger.h"
#include "hsTypes/hstNumeric.h"
#include "hsTypes/hstSymbolic.h"

#include "hTypeList.h"
// -----------------------------------------------------------------------------

hTypeList* typeList;
// -----------------------------------------------------------------------------

// #brief Constructor hTypeList class.
hTypeList::hTypeList(void)
{
     for(int i=0;i<SPECIAL_TYPES_COUNT;++i)
          specialTypes[i] = NULL;
}
// -----------------------------------------------------------------------------

// #brief Destructor hTypeList class.
hTypeList::~hTypeList(void)
{
     flush();
     for(int i=0;i<SPECIAL_TYPES_COUNT;++i)
          delete specialTypes[i];
}
// -----------------------------------------------------------------------------

// #brief Function that returns the index to the type that has given ID
//
// #param QString __id - ID of type
// #return returns the index to the type that has given ID as iteger if not found return -1
int hTypeList::indexOfID(QString __id)
{
     for(int i=0;i<size();++i)
          if(at(i)->id() == __id)
               return i;
               
     return -1;
}    
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the type that has given ID
//
// #param QString __id - ID of type
// #return returns the pointer to the type that has given ID as iteger if not found return NULL
hType* hTypeList::_indexOfID(QString __id)
{
     int ioi = indexOfID(__id);
     if(ioi == -1)
          return NULL;
          
     return at(ioi);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the index to the type that has given name
//
// #param QString __name - name of type
// #return returns the index to the type that has given name as iteger if not found return -1
int hTypeList::indexOfName(QString __name)
{
     for(int i=0;i<size();++i)
          if(at(i)->name() == __name)
               return i;
               
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the type that has given name
//
// #param QString __id - name of type
// #return returns the pointer to the type that has given name as iteger if not found return NULL
hType* hTypeList::_indexOfName(QString __name)
{
     int ioi = indexOfName(__name);
     if(ioi == -1)
          return NULL;
          
     return at(ioi);
}
// -----------------------------------------------------------------------------

// #brief Function that create the base types such as bool, iteger,...
//
// #return No value return
void hTypeList::createBaseTypes(void)
{
     hType** baseTypes = new hType*[hpTypesInfo.ptc];

     int typeIndex = 0;  // Boolean
     baseTypes[typeIndex] = new hstBoolean(this, NO_TYPE_CLASS, hpTypesInfo.ptids[typeIndex]);
     baseTypes[typeIndex]->setName(hpTypesInfo.ptn.at(typeIndex));
     baseTypes[typeIndex]->setDescription(hpTypesInfo.ptn.at(typeIndex));

     typeIndex = 1;      // Integer
     baseTypes[typeIndex] = new hstInteger(this, NO_TYPE_CLASS, hpTypesInfo.ptids[typeIndex]);
     baseTypes[typeIndex]->setName(hpTypesInfo.ptn.at(typeIndex));
     baseTypes[typeIndex]->setDescription(hpTypesInfo.ptn.at(typeIndex));
     
     typeIndex = 2;      // Numeric
     baseTypes[typeIndex] = new hstNumeric(this, NO_TYPE_CLASS, hpTypesInfo.ptids[typeIndex]);
     baseTypes[typeIndex]->setName(hpTypesInfo.ptn.at(typeIndex));
     baseTypes[typeIndex]->setDescription(hpTypesInfo.ptn.at(typeIndex));
     
     typeIndex = 3;      // Symbolic
     baseTypes[typeIndex] = new hstSymbolic(this, NO_TYPE_CLASS, hpTypesInfo.ptids[typeIndex]);
     baseTypes[typeIndex]->setName(hpTypesInfo.ptn.at(typeIndex));
     baseTypes[typeIndex]->setDescription(hpTypesInfo.ptn.at(typeIndex));
     
     for(int i=0;i<hpTypesInfo.ptc;++i)
          append(baseTypes[i]);
          
     baseTypes[0]->initObject(this);
     baseTypes[1]->initObject(this);

     delete [] baseTypes;

     // Sets the ids of the special type
     renewSTids();
     
     for(int i=0;i<SPECIAL_TYPES_COUNT;++i)
          if(specialTypes[i] != NULL)
               delete specialTypes[i];
               
     specialTypes[0] = new hType(this, NO_TYPE_CLASS, VOID_BASED);
     specialTypes[0]->setName("void");
     specialTypes[1] = new hType(this, NO_TYPE_CLASS, ACTION_BASED);
     specialTypes[1]->setName("action");
}
// -----------------------------------------------------------------------------

// #brief Function that renews the identifiers of the special types. It is necessary for compatybility with types that can be read from XML file
//
// #return No value return
void hTypeList::renewSTids(void)
{
     for(int i=0;i<hpTypesInfo.ptc;++i)
          at(i)->setID(searchNewId());
}
// -----------------------------------------------------------------------------

// #brief Function that resets the identifiers of the special types. It is necessary for compatybility with types that can be read from XML file
//
// #return No value return
void hTypeList::resetSTids(void)
{
     for(int i=0;i<hpTypesInfo.ptc;++i)
          at(i)->setID(hpTypesInfo.ptn.at(i));
}
// -----------------------------------------------------------------------------

// #brief Function that returns the ID that is not used yet
//
// #return returns the ID that is not used yet as string
QString hTypeList::searchNewId(void)
{
     int counter = 1;
     QString id_prefix = hType::idPrefix();
     QString candidate;
     
     do
     {
          candidate = id_prefix + QString::number(counter);
          counter++;
     }
     while(indexOfID(candidate) != -1);
     
     return candidate;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new type
//
// #param __oldHekateType - the identifier of old HeKatE type
// #return pointer to the new type
hType* hTypeList::createNewType(int __oldHekateType)
{
     if(__oldHekateType == 0)
          return NULL;
     if(__oldHekateType == 1)
          return createNewType(NO_TYPE_CLASS, BOOLEAN_BASED);
     if(__oldHekateType == 2)
          return createNewType(NO_TYPE_CLASS, INTEGER_BASED);
     if(__oldHekateType == 3)
          return createNewType(NO_TYPE_CLASS, NUMERIC_BASED);
     if(__oldHekateType == 4)
          return createNewType(NO_TYPE_CLASS, SYMBOLIC_BASED);
     if(__oldHekateType == 5)
          return createNewType(TYPE_CLASS_DOMAIN, SYMBOLIC_BASED);
          
     return NULL;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new type
//
// #param __class - the class of contraints for new type
// #param __type - the identifier of base type
// #return pointer to the new type
hType* hTypeList::createNewType(int __class, int __type)
{
     hType* candidate = new hType(this, __class, __type);
     add(candidate);
     
     return candidate;
}
// -----------------------------------------------------------------------------

// #brief Function that adds new item to the list
//
// #param hType* __new_item - poiter to the new item
// #return no value return.
void hTypeList::add(hType* __new_item)
{
     if(__new_item == NULL)
          return; 
          
     __new_item->setID(searchNewId());
     append(__new_item);
}
// -----------------------------------------------------------------------------

// #brief Function that removes the given type
//
// #param int __index - the type index
// #return true on succes otherwise false
bool hTypeList::remove(int __index)
{
     if((__index < 0) || (__index >= size()))
          return false;

     hType* rem_ptr = typeList->takeAt(__index);
     
     // Searching for index of baseType
     int ion = hpTypesInfo.iopti(rem_ptr->baseType());
     
     // Updating types
     updateTypePointer(rem_ptr, at(ion));
     
     delete rem_ptr;
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hTypeList::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     for(int i=hpTypesInfo.ptc;i<size();++i)
          res = at(i)->updateTypePointer(__old, __new) && res;
      
     res = AttributeGroups->updateTypePointer(__old, __new) && res;
     res = TableList->updateTypePointer(__old, __new) && res;
     res = States->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the types objects are correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hTypeList::verification(QString& __error_message)
{
     bool res = true;
     for(int i=hpTypesInfo.ptc;i<size();++i)
          res = at(i)->typeVerification(__error_message) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that removes all the items from the set
//
// #return no value return.
void hTypeList::flush(void)
{
     /*for(int i=0;i<size();++i)
          at(i)->setID(at(i)->id()+"sdfsdfsdfsd");*/
     
     while(count())
     {
          delete takeFirst();
     }
}
// -----------------------------------------------------------------------------

// #brief Function that creates the list of type names that are in the list
//
// #param bool __primitives - denotes if the list should contains the names of prmimitive types
// #return The list of names
QStringList hTypeList::names(bool __primitives)
{
     QStringList res;
     int init = __primitives==true?0:hpTypesInfo.ptc;
     
     for(int i=init;i<size();++i)
          res << at(i)->name();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the type to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this type as string.
QString hTypeList::toProlog(int /*__option*/)
{
     QString result = "";
     for(int i=0;i<size();++i)
          result += at(i)->toProlog() + "\n";
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the one of the special type VOID, ACTION
//
// #param __type - the type identifier
// #return The pointer to the apropriate special type.
hType* hTypeList::specialType(int __type)
{
     if(__type == VOID_BASED)
          return specialTypes[0];
     if(__type == ACTION_BASED)
          return specialTypes[1];
          
     return NULL;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #return List of all data in hml 2.0 format.
QStringList* hTypeList::out_HML_2_0(void)
{
     // First set the special types IDs
     resetSTids();
     renewSTids();
     
     QStringList* res = new QStringList;
     res->clear();
     
     *res << "<types>";
     
     if(indexOfID(ard::defaultArdAttributeTypeId()) == -1)
          *res << "\t" + ard::xmlDefaultArdAttributeType();
     for(int i=0;i<size();++i)
     {
          QStringList* al = at(i)->out_HML_2_0();
          for(int l=0;l<al->size();l++)
               *res << "\t" + al->at(l);
          delete al;
     }
     
     *res << "</types>";
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #return No retur value.
void hTypeList::out_HML_2_0(QXmlStreamWriter* _xmldoc)
{
     // First set the special types IDs
     resetSTids();
     renewSTids();
     
     _xmldoc->writeStartElement("types");
     
     if(indexOfID(ard::defaultArdAttributeTypeId()) == -1)
          ard::xmlDefaultArdAttributeType(_xmldoc);
     for(int i=0;i<size();++i)
          at(i)->out_HML_2_0(_xmldoc);
     
     _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hTypeList::in_HML_2_0(QDomElement& __root, QString& __msg)
{
     int result = 0;
     QString elementName = "types";
     QString childElementName = "type";
     int t1 = hpTypesInfo.iopti(NUMERIC_BASED);
     int t2 = hpTypesInfo.iopti(SYMBOLIC_BASED);
     QStringList baseTypes = QStringList() << hpTypesInfo.ptn.at(t1) << hpTypesInfo.ptn.at(t2);
     
     QDomElement DOMtypes = __root.firstChildElement(elementName);
     if(DOMtypes.isNull())
          return 0;

     resetSTids();  // Reseting ids of special types. For compatybility the new values of ids will be read from file.

     QDomElement DOMtype = DOMtypes.firstChildElement(childElementName);
     while(!DOMtype.isNull())
     {
          // Reading type data
          QString tid = DOMtype.attribute("id", "");
          QString tname = DOMtype.attribute("name", "");
          QString tbase = DOMtype.attribute("base", "");

          if(tid == "")
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined type \'" + tname + "\' identifier. The object will not be read." + hqed::createDOMlocalization(DOMtype), 0, 2);
               DOMtype = DOMtype.nextSiblingElement(childElementName);
               result++;
               continue;
          }
          
          if(indexOfID(tid) > -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "The identifier of the \'" + tname + "\' type already exists. The object will not be read." + hqed::createDOMlocalization(DOMtype), 0, 2);
               DOMtype = DOMtype.nextSiblingElement(childElementName);
               result++;
               continue;
          }
          
          if(baseTypes.indexOf(tbase) == -1)
          {
               __msg += "\n" + hqed::createErrorString("", "", "Undefined base type of \'" + tname + "\' type. The object will not be read." + hqed::createDOMlocalization(DOMtype), 0, 2);
               DOMtype = DOMtype.nextSiblingElement(childElementName);
               result++;
               continue;
          }
          
          // Reading type
          hType* candidate = NULL;
          
          // Checking if it is not special type
          for(int i=0;i<hpTypesInfo.ptc;++i)
               if(tname == hpTypesInfo.ptn.at(i))
                    candidate = at(i);
          
          // If it is not special type, then create a new one
          if(candidate == NULL)
          {
               candidate = new hType(this);
               add(candidate);
          }
          
          // Reading type from XML
          result += candidate->in_HML_2_0(DOMtype, __msg);
  
          DOMtype = DOMtype.nextSiblingElement(childElementName);
     }

     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that creates uniqe type identifier
//
// #return uniqe type identifier as string
QString hTypeList::createUniqeTypeIdentifier(void)
{
    int id_index = 1;
    QString candidate = "";
    QString prefix = hType::idPrefix();

    do
    {
         candidate = prefix + QString::number(id_index++);
    }
    while(indexOfID(candidate) != -1);

    return candidate;
}
