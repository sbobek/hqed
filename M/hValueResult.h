 /**
 * \file	hValueResult.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	16.10.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the result of the calculation
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HVALUERESULTH
#define HVALUERESULTH
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

/**
* \class 	hValueResult
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	16.10.2008 
* \brief 	Class definition that represents the result of the calculation
*/
class hValueResult
{
private:

     bool _succes;                 ///< Describes if the calculation finished successfully
     QString _error;               ///< Contains the error message
     QString _result;              ///< Contains the result of the calculation
     
public:

     /// \brief Constructor hValueResult class.
     hValueResult(void);
     
     /// \brief Constructor hValueResult class.
     ///
     /// \param __result - the result as string
     hValueResult(QString __result);
     
     /// \brief Constructor hValueResult class.
     ///
     /// \param __msg - the message as string. When __succes is true then __msg is stored in _result if false in _error
     /// \param __succes - denotes if the result of calculation finished successfully and simultanously dentoes where the __msg have to be stored
     hValueResult(QString __msg, bool __succes);
     
     /// \brief Constructor hValueResult class.
     ///
     /// \param __succes - denotes if the result of calculation finished successfully
     hValueResult(bool __succes);
     
     /// \brief Function that returns the value of _succes field
     ///
     /// \return the value of _succes field
     bool succes(void) { return _succes; }
     
     /// \brief Function that returns the value of _result field
     ///
     /// \return the value of _result field
     QString result(void) { return _result; }
     
     /// \brief Function that returns the value of _error field
     ///
     /// \return the value of _error field
     QString error(void) { return _error; }
     
     /// \brief Overloaded assignment operator
     ///
     /// \param hValueResult& __right - the reference to the right object that is a source of data
     /// \return the current object
     hValueResult& operator=(hValueResult __right);
     
     /// \brief To boolean conversion method
     ///
     /// \return the boolean representation of object
     operator bool(void);
     
     /// \brief To QString conversion method
     ///
     /// \return the QString representation of object. The value of the string depends on _succes field value. When true returns _result otherwise _error field
     operator QString(void);
};
// -----------------------------------------------------------------------------
#endif
