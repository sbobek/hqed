 /**
 * \file	hFunctionArgument.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	12.09.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the HeKatE function argument
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HFUNCTIONARGUMENTH
#define HFUNCTIONARGUMENTH

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QList>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// 1. Type definition
// 2. Mulitiplicity definition, that denotes if the argument can has only single or also multiple value from given type
// For example:
// -----------------------------------------------------------------------------

// Each argument should have defined type. The type consist of two parts:
// Integer argument that can has more than one value i defined as: FUNCTION_TYPE__INTEGER | FUNCTION_TYPE__MULTI_VALUED
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hFunction;
class hFunctionList;
class hMultipleValue;
class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class 	hFunctionArgument
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	12.09.2008 
* \brief 	Class definition that represents the HeKatE function argument
*/
class hFunctionArgument
{
private:

     int _argType;                           ///< Argument type
     QString _argName;                       ///< Argument name
     hMultipleValue* _argValue;              ///< Argument value
     QList<hType*>* _compatibleTypes;        ///< List of compatible types
     bool _isEditable;                       ///< Determines if the argument can be modified by the user in expression editor window
     bool _isVisible;                        ///< Determines if the argument is visible in string representation
     bool _argValueDeleteLater;              ///< Denotes if the value object must be deleted when the object id destroyed
     
     QString _lastError;                     ///< The last error message
     
public:

     /// \brief Constructor hFunctionArgument class.
     hFunctionArgument(void);
     
     /// \brief Destructor hFunctionArgument class.
     ~hFunctionArgument(void);
     
     /// \brief Function that returns the type of the argument
     ///
     /// \return the type of the argument as integer
     int type(void);
     
     /// \brief Function that returns the name of the argument
     ///
     /// \return the name of the argument as string
     QString name(void);
     
     /// \brief Function that returns the last error message
     ///
     /// \return the last error message as string
     QString lastError(void);
     
     /// \brief Function that returns the value of the argument
     ///
     /// \return the value of the argument that is represented as hSet object
     hMultipleValue* value(void);
     
     /// \brief Function that returns the list of compatible types
     ///
     /// \return the value of the list of compatible types
     QList<hType*>* compatibleTypes(void);
     
     /// \brief Function that returns the argument value multiplicity
     ///
     /// \return the argument value multiplicity
     int valueMultiplicity(void);
     
     /// \brief Function that returns if the argument can be modified by the user in expression editor window
     ///
     /// \return returns true if the argument can be modified by the user in expression editor window
     bool isEditable(void);
     
     /// \brief Function that returns if the argument should be visible in string representation
     ///
     /// \return returns true if the argument should be visible in string representation
     bool isVisible(void);
     
     /// \brief Function that copies the current object to the other
     ///
     /// \param __destination - the pointer to the destination object
     /// \return the pointer to the object that is a copy of current object
     hFunctionArgument* copyTo(hFunctionArgument* __destination = NULL);
     
     // ----------------------------
     
     /// \brief Function that changes the type of the argument
     ///
     /// \param __new_type - new value that is the new type of the argument
     /// \return no value return
     void setType(int __new_type);
     
     /// \brief Function that changes the parent type of the value
     ///
     /// \param __parent_type - pointer to the new parent type
     /// \param __checkConstraints - denotes if the value of the argument should be compatible with new parent type constraints
     /// \return true on succes, otherwise false
     bool setValueParentType(hType* __parent_type, bool __checkConstraints);
     
     /// \brief Function that changes the name of the argument
     ///
     /// \param __new_name - string that is the new name of the argument
     /// \return no value return
     void setName(QString __new_name);
     
     /// \brief Function that sets the last error message
     ///
     /// \param __last_error - the last error message
     /// \return no value return
     void setLastError(QString __last_error);
     
     /// \brief Function that changes the value of the argument
     ///
     /// \param __new_value - pointer to the set object that is the new value of the argument
     /// \param __copy_obj - denotes if the set object should be copied
     /// \return no value return
     void setValue(hMultipleValue* __new_value,  bool __copy_obj = false);
     
     /// \brief Function that changes the value of the field _isEditable
     ///
     /// \param _e - new value of field _isEditable (see description)
     /// \return no value return
     void setEditable(bool _e);
     
     /// \brief Function that changes the value of the field _isVisible
     ///
     /// \param _v - new value of field _isVisible (see description)
     /// \return no value return
     void setVisible(bool _v);
     
     /// \brief Function that searches for types that are compatible with current argument type
     ///
     /// \param __src_types_list - pointer to the list of types
     /// \return no value return
     void findCompatibleTypes(QList<hType*>* __src_types_list);
     
     /// \brief Function that returns the index of current type of argument
     ///
     /// \return index of current type of argument as integer. In case of error returns -1
     int currentTypeIndex(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
     ///
     /// \param __old - the value of the old pointer
     /// \param __new - the value of the new pointer
     /// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that returns if the function argument object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool functionArgumentVerification(QString& __error_message);
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
     /// \param __cellExpression - denotes if this object represents the whole cell expression
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression = false);
};
// -----------------------------------------------------------------------------
#endif
