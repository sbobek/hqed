/*
 *	   $Id: hValueResult.cpp,v 1.11 2010-01-08 19:47:35 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include "hValueResult.h"
// -----------------------------------------------------------------------------

// #brief Constructor hValueResult class.
hValueResult::hValueResult(void)
{
     _succes = true;
     _error = "";
     _result = "";
}
// -----------------------------------------------------------------------------

// #brief Constructor hValueResult class.
//
// #param QString __result - the result as string
hValueResult::hValueResult(QString __result)
{
     _succes = true;
     _result = __result;
}
// -----------------------------------------------------------------------------

// #brief Constructor hValueResult class.
//
// #param QString __msg - the message as string. When __succes is true then __msg is stored in _result if false in _error
// #param bool __succes - denotes if the result of calculation finished successfully and simultanously dentoes where the __msg have to be stored
hValueResult::hValueResult(QString __msg, bool __succes)
{
     _error = "";
     _result = "";
     _succes = __succes;
     
     if(__succes)
          _result = __msg;
     if(!__succes)
          _error = __msg;
}
// -----------------------------------------------------------------------------

// #brief Constructor hValueResult class.
//
// #param bool __succes - denotes if the result of calculation finished successfully
hValueResult::hValueResult(bool __succes)
{
     _error = "";
     _result = "";
     _succes = __succes;
}
// -----------------------------------------------------------------------------

// #brief Overloaded assignment operator
//
// #param hValueResult& __right - the reference to the right object that is a source of data
// #return the current object
hValueResult& hValueResult::operator=(hValueResult __right)
{
     _result = __right._result;
     _error = __right._error;
     _succes = __right._succes;
     
     return *this;
}
// -----------------------------------------------------------------------------

// #brief To boolean conversion method
//
// #return the boolean representation of object
hValueResult::operator bool(void)
{
     return _succes;
}
// -----------------------------------------------------------------------------

// #brief To QString conversion method
//
// #return the QString representation of object. The value of the string depends on _succes field value. When true returns _result otherwise _error field
hValueResult::operator QString(void)
{
     if(!_succes)
          return _error;
          
     return _result;
}
// -----------------------------------------------------------------------------
