 /**
 * \file	hValue.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	2.08.2008 
 * \version	1.15
 * \brief	This file contains class definition that represents the HeKatE value type
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HVALUE
#define HVALUE

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

enum ValueType
{
     NOT_DEFINED = 0,
     ANY_VALUE,
     VALUE,
     ATTRIBUTE,
     EXPRESSION
};
// -----------------------------------------------------------------------------

enum ValueDefinitionType
{
     SYM = 0,
     NUM
};
// -----------------------------------------------------------------------------

#include "hValueResult.h"
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hExpression;
class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class 	hValue
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	2.08.2008 
* \brief 	Class definition that represents the HeKatE value type
*/
class hValue
{
private:
     
     /// The type of the value:
     /// \li NOT_DEFINED - the value is not defined
     /// \li ANY_VALUE - denotes any value
     /// \li VALUE - the value is defined as constant value
     /// \li ATTRIBUTE - the value is equal to selected attribute value (attribute with attomic value are allowed)
     /// \li EXPRESSION - the value is defined through expression that must be calculated
     ValueType _type;
     
     /// The type of the value definition:
     /// \li SYM - value is defined as symbolic
     /// \li NUM - value is defined as numeric (allowed only for values from symbolic, ordered domains!!!)
     ValueDefinitionType _defType;
     
     QString _sym_value;                ///< The value in case of _type == VALUE_TYPE_VALUE && VALUE_DEFINITION_TYPE_SYM
     QString _num_value;                ///< The value in case of _type == VALUE_TYPE_VALUE && VALUE_DEFINITION_TYPE_NUM
     XTT_Attribute* _attribute;         ///< The value in case of _type == VALUE_TYPE_ATTRIBUTE
     hExpression* _expression;          ///< The value in case of _type == VALUE_TYPE_EXPRESSION
     
     // The others types of values are not supported yet.
     
public:
     
     /// \brief Constructor hValue class.
     hValue(void);
     
     /// \brief Copying constructor
     ///
     /// \param __source - the source object
     hValue(hValue* __source);
     
     /// \brief Constructor hValue class.
     ///
     /// \param __type - the type of value definition
     hValue(ValueType __type);
     
     /// \brief Constructor hValue class.
     ///
     /// \param __value - value of the item
     hValue(QString __value);
     
     /// \brief Constructor hValue class.
     ///
     /// \param __symvalue - symbolic value of the item
     /// \param __numvalue - numeric value of the item
     hValue(QString __symvalue, QString __numvalue);
     
     /// \brief Constructor hValue class.
     ///
     /// \param __xtt_attribute - attribute reference
     hValue(XTT_Attribute* __xtt_attribute);
     
     /// \brief Constructor hValue class.
     ///
     /// \param *__expression - pointer to the expression object
     hValue(hExpression* __expression);
     
     /// \brief Destrucotr hValue class.
     ~hValue(void);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param __destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     hValue* copyTo(hValue* __destinationItem = NULL);
     
     /// \brief Function that returns the value of this object as string
     ///
     /// \return The current value of this object as string
     hValueResult value(void); 

     /// \brief Function that returns the form of the value as value, attribute name, expression form as string
     ///
     /// \return The form of the value as value, attribute name, expression form as string
     QString toString(void);

     /// \brief Function that returns the form of the value as value, attribute name, expression form in drools5 format
     ///
     /// \return The form of the value as value, attribute name, expression form in drools5 format
     QString toDrools5(void);
     
     /// \brief Function that maps the object to the prolog code.
     ///
     /// \param __option - options of the output
     /// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function that returns the string representation of the attribute field
     ///
     /// \return the string representation of the attribute field
     QString attributeFieldToString(void);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that returns if the value objest definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool valueVerification(QString& __error_message);
     
     /// \brief Function that returns the type of the value identidier (see description of the "_type" field)
     ///
     /// \return The current value of this object as string
     ValueType type(void){ return _type; }
     
     /// \brief Function that returns the type of the value definition
     ///
     /// \return the type of the value definition as integer
     ValueDefinitionType definitionType(void){ return _defType; }
     
     /// \brief Function that sets the type of the value
     ///
     /// \param __type - new value of type
     /// \return No value return
     void setType(ValueType __type);
     
     /// \brief Function that sets the type of the value definition
     ///
     /// \param __defType - new type of value definition
     /// \return No value return
     void setDefinitionType(ValueDefinitionType __defType);
     
     /// \brief Function that sets the value
     ///
     /// \param __value - the new value
     /// \return No value return
     void setValue(QString __value);
     
     /// \brief Function that sets the attribute pointer
     ///
     /// \param __attr_ptr - the pointer to the attribute
     /// \return No value return
     void setAttribute(XTT_Attribute* __attr_ptr);
     
     /// \brief Function that sets the poiter to the expression object
     ///
     /// \param __expression - pointer to the new expression object
     /// \return No value return
     void setExpression(hExpression* __expression);
     
     /// \brief Function that returns the list of value types as string representation
     ///
     /// \return Return the list of value types as string representation
     QStringList stringTypes(void);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
     ///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
     /// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     // -----------------------------
     
     /// \brief Function that returns the value of the "_attribute" field
     ///
     /// \return Return the value of the "_attribute" field
     XTT_Attribute* attributeField(void) { return _attribute; }
     
     /// \brief Function that returns the value of the "_expression" field
     ///
     /// \return Return the value of the "_expression" field
     hExpression* expressionField(void) { return _expression; }
     
     // -----------------------------
     
     /// \brief Function that creates the set object by using hValue object. The single object becomes a member of the set
     ///
     /// \param __parent_type - pointer to the parent type of the set
     /// \param __set_item - pointer to the source item
     /// \return Return the pointer to the set is succes, otherwise NULL
     static hSet* toSet(hType* __parent_type, hValue* __set_item);
     
     /// \brief Function gets all data in hml 2.0 format.
     ///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \param __elementName - the name of the tag element
     /// \return List of all data in hml 2.0 format.
     QStringList* out_HML_2_0(int __mode, QString __elementName);

     #if QT_VERSION >= 0x040300
     /// \brief Function saves all data in hml 2.0 format.
     ///
     /// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
     /// \param QString __elementName - the name of the tag element
     /// \return No retur value.
     void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode, QString __elementName);
     #endif
     
     /// \brief Function loads all data from hml 2.0 format.
     ///
     /// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
     /// \param __att - denotes which attribute must read
     /// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
     int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, QString __att);
};
// -----------------------------------------------------------------------------
#endif
