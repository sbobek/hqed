/*
 *	   $Id: hFunction.cpp,v 1.16.4.4 2012-02-26 18:17:37 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <stdarg.h>

#include "../namespaces/ns_hqed.h"
#include "XTT/XTT_Attribute.h"

#include "hSet.h"
#include "hType.h"
#include "hDomain.h"
#include "hTypeList.h"
#include "hFunction.h"
#include "hFunctionList.h"
#include "hMultipleValue.h"
#include "hFunctionArgument.h"
// -----------------------------------------------------------------------------

// #brief Constructor hFunction class.
hFunction::hFunction(void)
{
     _resultType = FUNCTION_RESULT_TYPE_NOT_DEFINED;                   
     _returnedTypes = FUNCTION_TYPE__NOT_DEFINED;               
     _minArgCount = 0;                
     _maxArgCount = 0;                
     _additionalArgsType = 0;    
     _requiredType = NULL;
     _verifyResult = true;
     _isOperator = false;
     
     _args = new QList<hFunctionArgument*>;     
     
     _userRepresentation = "";
     _internalRepresentation = "";  
     _prologRepresentation = "";  
     _xmlRepresentation = "";
     _operatorRepresentation = "";

     _operatorLeftAssociative = true;
     _operatorPrecedence = -1;
     
     _lastError = "";              
}
// -----------------------------------------------------------------------------

// #brief Destructor hFunction class.
hFunction::~hFunction(void)
{
     for(;_args->size();)
          delete _args->takeFirst();
     delete _args;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the new instance of the function object
//
// #param hFunction* __destination - poinyter to the destination object
// #return pointer to the new instance of function object
hFunction* hFunction::copyTo(hFunction* __destination)
{
     hFunction* dest = __destination;
     if(dest == NULL)
          return NULL;
          
     dest->_verifyResult = _verifyResult;
     dest->_requiredType = _requiredType;              
     dest->_resultType = _resultType;                  
     dest->_returnedTypes = _returnedTypes;                
     dest->_minArgCount = _minArgCount;            
     dest->_maxArgCount = _maxArgCount;                  
     dest->_additionalArgsType = _additionalArgsType;     
     dest->_argNames = _argNames;   
     dest->_argTypesConf = _argTypesConf;   
     dest->_argMltplConf = _argMltplConf;   
     
     for(;dest->_args->size();)
          delete dest->_args->takeLast();
     for(int i=0;i<_args->size();++i)
          dest->_args->append(_args->at(i)->copyTo());
     
     dest->_userRepresentation = _userRepresentation;       
     dest->_internalRepresentation = _internalRepresentation; 
     dest->_prologRepresentation = _prologRepresentation; 
     dest->_xmlRepresentation = _xmlRepresentation; 
     dest->_description = _description;             
     
     dest->_lastError = _lastError;           
          
     return dest;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the value of the required type.
//
// #param hType* __required_type - the value of required type
// #return the result of compatybility this function with required type
int hFunction::setRequiredType(hType* __required_type)
{
     int newTypeRepresentation = typeFunctionRepresentation(__required_type);
     
     if((newTypeRepresentation & _returnedTypes) != 0)
          _requiredType = __required_type;
          
     for(int i=0;i<argCount();++i)
     {
          if((newTypeRepresentation & argTypeConf(i)) == 0)
               continue;
          _additionalArgsType = hFunction::typeFunctionRepresentation(__required_type) | argMltplConf(i);  
          _args->at(i)->setType(_additionalArgsType);
          _args->at(i)->findCompatibleTypes(typeList);      
          _args->at(i)->setValueParentType(__required_type, true);     
     }
          
     refreshArgumentCompatibleTypesLists();

     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the number of arguments
//
// #param int __arg_count - the number of the arguments
// #return the result of compatybility this function with the number of arguments
int hFunction::setArgCount(int __arg_count)
{
     if((__arg_count < _minArgCount) || ((_maxArgCount >= 0) && (__arg_count > _maxArgCount)))
          return setLastError(FUNCTION_ERROR_ARG_CNT_OUT_OF_RANGE, "Required argument count: " + QString::number(__arg_count));
          
     // Uaktualnienie listy typow argumentow
     while(argCount() < __arg_count)
     {
          _args->append(new hFunctionArgument);
          _args->at(_args->size()-1)->setType(argTypeConf(_args->size()-1) | argMltplConf(_args->size()-1));
          _args->at(_args->size()-1)->setValueParentType(_requiredType, false);
          _args->at(_args->size()-1)->findCompatibleTypes(typeList);
     }
     while(argCount() > __arg_count)
     {
          delete _args->takeAt(__arg_count);
     }
     
     // Names refreshing
     refreshArgumentsNames();
     
     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that performs the compatible types lists for each argument
//
// #return no value return
void hFunction::refreshArgumentCompatibleTypesLists(void)
{
     // Refreshing lists of compatible types
     for(int i=0;i<argCount();++i)
     {
          _args->at(i)->findCompatibleTypes(typeList);
          //_args->at(i)->setValueParentType(_requiredType);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes the names of the arguments
//
// #return no value returns
void hFunction::refreshArgumentsNames(void)
{
     if(_argNames.empty())
          return;
     
     for(int i=0;i<argCount();++i)
     {
          if(i < _argNames.size())
               _args->at(i)->setName(_argNames.at(i));
          if(i >= _argNames.size())
               _args->at(i)->setName(_argNames.last());
     }
}
// -----------------------------------------------------------------------------

// #brief Function that sets the argument for the function
//
// #param int __arg_index - the index of argument
// #param hFunctionArgument* __arg - the pointer to the argument
// #return success code if all is ok otherwise error code
int hFunction::setArgument(int __arg_index, hFunctionArgument* __arg)
{
     if((__arg_index < 0) || (__arg_index >= argCount()))
          return setLastError(FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE, "Incorrect index value: " + QString(__arg_index) + ".\nFunction " + _userRepresentation + " accepts " + QString(argCount()) + " arguments.");

     // Checking the inconsistency of argument type
     if((argTypeConf(__arg_index) & typeFunctionRepresentation(__arg->value()->parentType())) == 0)
          return setLastError(FUNCTION_ERROR_INCONSISTENT_ARG_TYPES, "Function: " + _userRepresentation + "\nArgument index: " + QString::number(__arg_index));

     if(_args->at(__arg_index) != __arg)
          delete _args->at(__arg_index);
     _args->replace(__arg_index, __arg);
     
     // Names refreshing
     refreshArgumentsNames();
     
     return FUNCTION_ERROR_SUCCESS;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the status of calculation result verification
//
// #param bool __rvs - the new value of the status
// #return no value returns
void hFunction::setResultVerificationStatus(bool __rvs)
{
     _verifyResult = __rvs;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param QString __error_content - last error message
// #return no return value
void hFunction::setLastError(QString __error_content)
{
     _lastError = __error_content;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error
//
// #param int __error_code - code of the error
// #param QString __error_desc - the additional error description
// #return returns the given error code
int hFunction::setLastError(int __error_code, QString __error_desc)
{
     QString complete_error_message = "";

     switch(__error_code)
     {
          case FUNCTION_ERROR_ARG_CNT_OUT_OF_RANGE:
               complete_error_message = "The number of argumetns is out of range.";
               break;
               
          case FUNCTION_ERROR_INCONSISTENT_TYPES:
               complete_error_message = "Inconsistent required result type.";
               break;
               
          case FUNCTION_ERROR_ARG_INDEX_OUT_OF_RANGE:
               complete_error_message = "The incorrect index of the argument.";
               break;
               
          case FUNCTION_ERROR_INCONSISTENT_ARG_TYPES:
               complete_error_message = "Inconsistent argument type.";
               break;
     }
     
     if(!__error_desc.isEmpty())
          complete_error_message += "\n" + __error_desc;
     setLastError(complete_error_message);
     
     return __error_code;
}
// -----------------------------------------------------------------------------

// #brief Function that appends the current object to the list of functions
//
// #param hFunctionList* __flist - the destination container
// #return the result of registration as integer
int hFunction::registerFunction(hFunctionList* __flist)
{
     return __flist->registerFunction(this);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the minimal value of the arguments
//
// #return the minimal value of the arguments
int hFunction::minArgCount(void)
{
     return _minArgCount;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the maximal value of the arguments
//
// #return the maximal value of the arguments
int hFunction::maxArgCount(void)
{
     return _maxArgCount;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the current number of arguments
//
// #return the current number of arguments
int hFunction::argCount(void)
{
     return _args->size();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value that denotes the type of the result
//
// #return the value that denotes the type of the result
int hFunction::resultType(void)
{
     return _resultType;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value that denotes what types can be returned by function
//
// #return the value is a integer type. If less than zero then error.
int hFunction::returnedTypes(void)
{
     return _returnedTypes;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the value (without multiplicity) that indicates which types the given argument can have
//
// #param __argument - the argument index
// #return value (without multiplicity) that indicates which types the given argument can have as integer
int hFunction::argTypeConf(int __argument)
{
     if((__argument < 0) || (_argTypesConf.size() == 0))
          return 0;
          
     if(__argument >= _argTypesConf.size())
          return _argTypesConf.last();
          
     return _argTypesConf.at(__argument);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of items in argument type configuration list
//
// #return number of items in argument type configuration list.
int hFunction::argTypeConfCount()
{
    return _argTypesConf.size();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the multiplicity which argument can have
//
// #param __argument - the argument index
// #return multiplicity which argument can have as integer
int hFunction::argMltplConf(int __argument)
{
     if((__argument < 0) || (_argMltplConf.size() == 0))
          return 0;
          
     if(__argument >= _argMltplConf.size())
          return _argMltplConf.last();
          
     return _argMltplConf.at(__argument);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the function characterisitics (result type and mulitiplicity)
//
// #return the function characterisitics (result type and mulitiplicity) as integer
int hFunction::functionCharacterisitics(void)
{
     return _resultType | _returnedTypes;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the required type returned by the function
//
// #return the pointer to the required type returned by the function
hType* hFunction::requiredType(void)
{
     return _requiredType;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the XML representation of the function
//
// #return the XML representation of the function as string
QString hFunction::xmlRepresentation(void)
{
     return _xmlRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the PROLOG representation of the function
//
// #return the PROLOG representation of the function as string
QString hFunction::prologRepresentation(void)
{
     return _prologRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the user representation of the function
//
// #return the user representation of the function as string
QString hFunction::userRepresentation(void)
{
     return _userRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the internal representation of the function
//
// #return the internal representation of the function as string
QString hFunction::internalRepresentation(void)
{
     return _internalRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns operator representation of the function
//
// #return operator representation of the function
QString hFunction::operatorRepresentation(void)
{
    return _operatorRepresentation;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the description of the function object
//
// #return the description of the function object as string
QString hFunction::description(void)
{
     return _description;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the name of the argument
//
// #return the name of the argument as string
QString hFunction::argumentName(int __arg_index)
{
     if((__arg_index < 0) || ((_maxArgCount >= 0) && (__arg_index >= _maxArgCount)))
          return "";
      
     if(__arg_index < _minArgCount)
          return _args->at(__arg_index)->name();
          
     QString result = _args->at(__arg_index)->name() + QString::number(__arg_index-_minArgCount+1);
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the argument object
//
// #param int __arg_index - index of the argument
// #return the pointer to the argument object
hFunctionArgument* hFunction::argument(int __arg_index)
{
     if((__arg_index < 0) || (__arg_index >= argCount()))
          return NULL;
          
     return _args->at(__arg_index);
}
// -----------------------------------------------------------------------------

// #brief Function that return the status of calculation result verification
//
// #return status of calculation result verification as boolean value
bool hFunction::resultVerificationStatus(void)
{
     return _verifyResult;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the last error message
//
// #return the last error message as string
QString hFunction::lastError(void)
{
     return _lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hFunction::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;

     if(_requiredType == __old)
          _requiredType = __new;
          
     for(int i=0;i<_args->size();++i)
          res = _args->at(i)->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hFunction::eventMessage(QString* __errmsgs, int __event, ...)
{
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int i=0;i<_args->size();++i)
               res = _args->at(i)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int i=0;i<_args->size();++i)
               res = _args->at(i)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #param __error_message - the reference to the error message(s)
// #return true is yes otherwise false
bool hFunction::isAVRused(void)
{
     for(int i=0;i<_args->size();++i)
          if(_args->at(i)->isAVRused())
               return true;
          
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hFunction::isExpressionUsed(void)
{
     for(int i=0;i<_args->size();++i)
          if(_args->at(i)->isExpressionUsed())
               return true;
          
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the function is the root function in the cell
//
// #return true if yes otherwise false
bool hFunction::isCellFunction(void)
{
     return ((argCount() > 0) && (!argument(0)->isEditable()));
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #param __error_message - the reference to the error message(s)
// #return true is yes otherwise false
bool hFunction::functionVerification(QString& __error_message)
{
     bool res = true;

     if(_requiredType == NULL)
     {
          __error_message += "\nThe function " + _userRepresentation + " has not defined required type.";
          res = false;
     }

     QString f_prefix = "\nThe function " + _userRepresentation + " error(s): ";
     QString fa_error;
     for(int i=0;i<_args->size();++i)
     {         
          fa_error = "";
          
          // Checking arguments type and multiplicity
          if((argTypeConf(i) & _args->at(i)->type()) == 0)
          {
               fa_error = "the type of the argument " + QString::number(i+1) + " is not supported in this function. Maybe you changed the type of some attribute?";
               __error_message += f_prefix + fa_error;
               res = false;
          }
          
          if((argMltplConf(i) == FUNCTION_TYPE__SINGLE_VALUED) && (argMltplConf(i) & _args->at(i)->valueMultiplicity()) == 0)
          {
               fa_error = "a current multiplicity of the argument " + QString::number(i+1) + " is not allowed in this function.\nAllowed multiplicity is: ";
               if((argMltplConf(i) & FUNCTION_TYPE__SINGLE_VALUED) != 0)
                    fa_error += XTT_Attribute::_class_attribute_.at(0);
               else if((argMltplConf(i) & FUNCTION_TYPE__MULTI_VALUED) != 0)
                    fa_error += XTT_Attribute::_class_attribute_.at(1);
               
               __error_message += f_prefix + fa_error;
               res = false;
          }
          
          // Checking other features of arguments
          if((!_args->at(i)->isAVRused()) && (!_args->at(i)->functionArgumentVerification(fa_error)))
          {
               __error_message += f_prefix + fa_error;
               res = false;
          }
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if function represents operator
//
// #return true if the function represents operator
bool hFunction::isOperator(void)
{
    return _isOperator;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if operator is left-associative
//
// #return true if function represents operator that is left-associative
bool hFunction::isOperatorLeftAssociative(void)
{
    return _operatorLeftAssociative;
}
// -----------------------------------------------------------------------------

// #brief Function that returns operator precedence
//
// #return operator precedence
int hFunction::operatorPrecedence(void)
{
    return _operatorPrecedence;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the specify function representation of the given type
//
// #param hType* __type - the type object
// #return the specify function representation of the given type
int hFunction::typeFunctionRepresentation(hType* __type)
{
     int result = FUNCTION_TYPE__NOT_DEFINED;
     if(__type == NULL)
          return result;

     switch(__type->baseType())
     {
          case BOOLEAN_BASED:
               result =  FUNCTION_TYPE__BOOLEAN;
               break;
               
          case INTEGER_BASED:
               result = FUNCTION_TYPE__INTEGER;
               break;
               
          case NUMERIC_BASED:
               result = FUNCTION_TYPE__NUMERIC;
               break;
               
          case SYMBOLIC_BASED:
               if((__type->typeClass() == TYPE_CLASS_DOMAIN) && (__type->domain()->ordered()))
                    result = FUNCTION_TYPE__OSYMBL;
               else
                    result = FUNCTION_TYPE__UOSYMBL;
               break;
               
          case VOID_BASED:
               result = FUNCTION_TYPE__VOID;
               break;
               
          case ACTION_BASED:
               result = FUNCTION_TYPE__ACTION;
               break;
           
          default:
               result = FUNCTION_TYPE__NOT_DEFINED;
               break;
     }
    
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hFunction::out_HML_2_0(int __mode)
{    
     QStringList* res = new QStringList;
     res->clear();
     
     for(int i=0;i<argCount();++i)
     {
          int mode = ((i == 0) && (__mode == 4)) ? 0 : __mode;
          QStringList* tmp = argument(i)->out_HML_2_0(mode);
          for(int j=0;j<tmp->size();++j)
               *res << tmp->at(j);
          delete tmp;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #li 4 - fact2states mode
// #return No retur value.
void hFunction::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     for(int i=0;i<argCount();++i)
     {
          int mode = ((i == 0) && (__mode == 4)) ? 0 : __mode;
          argument(i)->out_HML_2_0(_xmldoc, mode);
     }
}
#endif
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #param __cellExpression - denotes if this object represents the whole cell expression
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hFunction::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression)
{
     int result = 0;

     int argIndex = 0;
     QDomElement DOMarg = __root;
     while(!DOMarg.isNull())
     {
          if((argIndex >= argCount()) && ((argCount() < maxArgCount()) || (maxArgCount() < 0)))
               setArgCount(argCount()+1);

          if(argIndex < argCount())
               result += argument(argIndex)->in_HML_2_0(DOMarg, __msg, __attr, ((__cellExpression) && (argIndex >= 0)));

          argIndex++;
          DOMarg = DOMarg.nextSiblingElement();
     }
     
     return result;
}
// -----------------------------------------------------------------------------
