 /**
 * \file	hMultipleValue.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	16.09.2008 
 * \version	1.0
 * \brief	This file contains class definition that represents the HeKatE multiple value type
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef HMULTIPLEVALUE
#define HMULTIPLEVALUE

// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QStringList>
#include <QDomDocument>

#if QT_VERSION >= 0x040300
	#include <QXmlStreamWriter>
#endif
// -----------------------------------------------------------------------------

#define MULTIPLE_VALUE_ERROR_SUCCES  0
#define MULTIPLE_VALUE_ERROR_ERROR  -1
// -----------------------------------------------------------------------------

#include "hMultipleValueResult.h"
#include "hValue.h"
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hValue;
class hExpression;
class XTT_Attribute;
// -----------------------------------------------------------------------------
/**
* \class 	hMultipleValue
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	16.09.2008 
* \brief 	Class definition that represents the HeKatE multiple value type
*/
class hMultipleValue
{
private:
     
     /// The type of the value:
     /// \li NOT_DEFINED - the value is not defined
     /// \li VALUE - the value is defined as constant value
     /// \li ATTRIBUTE - the value is equal to selected attribute value (attribute with attomic value are allowed)
     /// \li EXPRESSION - the value is defined through expression that must be calculated
     ValueType _type;
     
     bool _singleRequired;              ///< Denotes if the value must be single
     
     hType* _valueType;                 ///< Pointer to  the multiple value type
     hSet* _value;                      ///< The value in case of _type == VALUE_TYPE_VALUE
     XTT_Attribute* _attribute;         ///< The value in case of _type == VALUE_TYPE_ATTRIBUTE
     hExpression* _expression;          ///< The value in case of _type == VALUE_TYPE_EXPRESSION - the result of the expression can be only multivalued. The single valued expression is defined as item of the set _value
     
     QString _lastError;                ///< the message of the last error
     
     // The others types of values are not supported yet.
     
public:
     
     /// \brief Constructor hMultipleValue class.
     hMultipleValue(void);
     
     /// \brief Copying constructor
     ///
     /// \param *__source - the source object
     hMultipleValue(hMultipleValue* __source);
     
     /// \brief Constructor hMultipleValue class.
     ///
     /// \param __type - the type of value definition
     hMultipleValue(ValueType __type);
     
     /// \brief Constructor hMultipleValue class.
     ///
     /// \param __type - the type of value definition
     /// \param *__valueType - the pointer to the type object that is a parent type of the value
     hMultipleValue(ValueType __type, hType* __valueType);
     
     /// \brief Constructor hMultipleValue class.
     ///
     /// \param *__valueType - pointer to the value type
     /// \param *__value - initial value
     /// \param __sinlge_required - determinres if the value shoud be single or multiple, default is true
     hMultipleValue(hType* __valueType, hValue* __value, bool __sinlge_required = true);
     
     /// \brief Constructor hMultipleValue class.
     ///
     /// \param *__value - initial value
     hMultipleValue(hSet* __value);
     
     /// \brief Constructor hMultipleValue class.
     ///
     /// \param *__xtt_attribute - attribute reference
     hMultipleValue(XTT_Attribute* __xtt_attribute);
     
     /// \brief Destrucotr hMultipleValue class.
     virtual ~hMultipleValue(void);
     
     /// \brief Function that creates copy of this object
     ///
     /// \param *__destinationItem - the destination copy localization. If NULL function creates new instace of object
     /// \return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
     hMultipleValue* copyTo(hMultipleValue* __destinationItem = NULL);
     
     /// \brief Function that returns the value of this object as hMultipleValueResult object
     ///
     /// \return The current value of this object as hMultipleValueResult object
     hMultipleValueResult value(void); 

     /// \brief Function that returns the form of the value as value, attribute name, expression form as string
     ///
     /// \param __forceSetParentheses - add set delimiters ("{}") to string representation even if set contains only one item (applies only when
     ///  type of the stored value is VALUE)
     /// \return The form of the value as value, attribute name, expression form as string
     QString toString(bool __forceSetParentheses = false);

     /// \brief Function that returns the form of the value as value, attribute name, expression form in drools5 format
     ///
     /// \return The form of the value as value, attribute name, expression form in drools5 format
     QString toDrools5(void);
     
     /// \brief Function that returns the string list representation of the items
	///
	/// \return The string list representation of the items
     virtual QStringList stringItems(void);

     /// \brief Function that tries to match the current value to format that is required for this type
     ///
     /// \param __allowTrunc - denotes if the digiths can be cut
     /// \return The result code
     int toCurrentNumericFormat(bool __allowTrunc = false);
     
     /// \brief Function that maps the object to the prolog code.
	///
     /// \param __option - options of the output
	/// \return The prolog code that represents this object as string.
     QString toProlog(int __option = -1);
     
     /// \brief Function that returns the string representation of the attribute field
     ///
     /// \return the string representation of the attribute field
     QString attributeFieldToString(void);
     
     /// \brief Function that returns the type of the value identidier (see description of the "_type" field)
     ///
     /// \return The current value of this object as string
     ValueType type(void){ return _type; }
     
     /// \brief Function that returns the status of single requirement
     ///
     /// \return the status of single requirement as boolean value
     bool singleRequirement(void) { return _singleRequired; }
     
     /// \brief Function that returns if the object have only single values
     ///
     /// \return true if yes, false if no
     bool isSingle(void);
     
     /// \brief Function that transforms the current object that contains a set of values to an object that contains a single value
     ///
     /// \return true on success otherwise false
     bool makeItSingle(void);
     
     /// \brief Function that sets the type of the value
     ///
     /// \param __type - new value of type
     /// \return No value return
     void setType(ValueType __type);
     
     /// \brief Function that sets the value
     ///
     /// \param *__value - the value set
     /// \return No value return
     void setValue(hSet* __value);
     
     /// \brief Function that sets the attribute pointer
     ///
     /// \param *__attr - the pointer to the attribute
     /// \return No value return
     void setAttribute(XTT_Attribute* __attr);
     
     /// \brief Function that sets the poiter to the expression object
     ///
     /// \param *__expression - pointer to the new expression object
     /// \return No value return
     void setExpression(hExpression* __expression);
     
     /// \brief Function that sets the requirement of single value
     ///
     /// \param __sr_val - new value of requirement
     /// \return No value return
     void setSingleRequirement(bool __sr_val);
     
     /// \brief Function that sets the parent type of the value
     ///
     /// \param *__parentType - pointer to the value type
     /// \return true if all is ok otherwise false
     bool setParentType(hType* __parentType);
     
     /// \brief Function that sets the message of the last error
     ///
     /// \param __msg - the content of the last error message
     /// \return No value returs
     void setLastError(QString __msg);
     
     /// \brief Function that sets the message of the last error
     ///
     /// \param __error_code - the code of the error
     /// \param __additional_msg - the additional message of the error
     /// \return the error code
     int setLastError(int __error_code, QString __additional_msg);
     
     /// \brief Function that returns the list of value types as string representation
     ///
     /// \return Return the list of value types as string representation
     QStringList stringTypes(void);
     
     /// \brief Function that updates int all the types the pointer to the new type
	///
     /// \param *__old - the value of the old pointer
     /// \param *__new - the value of the new pointer
	/// \return true on succes otherwise false
     bool updateTypePointer(hType* __old, hType* __new);
     
     /// \brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
	///
     /// \param *__errmsgs - pointer to the string that contains the error message that are generated on event.
     /// \param __event - the event identifier
     /// \param ... - the list of event params
	/// \return true on succes otherwise false
     bool eventMessage(QString* __errmsgs, int __event, ...);
     
     /// \brief Function that returns if the avr is used to define this object's value
     ///
     /// \return true is yes otherwise false
     bool isAVRused(void);
     
     /// \brief Function that returns if the expression is used to define this object's value
     ///
     /// \return true if yes otherwise false
     bool isExpressionUsed(void);
     
     /// \brief Function that returns if the multiple value object definition is correct
     ///
     /// \param __error_message - the reference to the error message(s)
     /// \return true if yes otherwise false
     bool multipleValueVerification(QString& __error_message);
     
     // -----------------------------
     
     /// \brief Function that returns the value of the "_value" field
     ///
     /// \return Return the value of the "_value" field
     hSet* valueField(void) { return _value; }
     
     /// \brief Function that returns the value of the "_attribute" field
     ///
     /// \return Return the value of the "_attribute" field
     XTT_Attribute* attributeField(void) { return _attribute; }
     
     /// \brief Function that returns the value of the "_expression" field
     ///
     /// \return Return the value of the "_expression" field
     hExpression* expressionField(void) { return _expression; }
     
     /// \brief Function that returns the value type
     ///
     /// \return Return the pointer to the type of value
     hType* parentType(void);
     
     /// \brief Function that returns the message of the last error
     ///
     /// \return Return the message of the last error
     QString lastError(void);
     
     // -----------------------------
     
     /// \brief Function that returns the message of the last error
     ///
     /// \param __type - the type of the value
     /// \param __strValue - the candidate for the value given as string
     /// \param __check_contraints - denotes if the value must be siutable with type domain
     /// \return Return the poniter to the created value, if error returns NULL
     static hMultipleValue* makeSingleValue(hType* __type, QString __strValue, bool __check_contraints);
     // -----------------------------
     
     /// \brief Function gets all data in hml 2.0 format.
	///
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return List of all data in hml 2.0 format.
	QStringList* out_HML_2_0(int __mode = 0);

	#if QT_VERSION >= 0x040300
	/// \brief Function saves all data in hml 2.0 format.
	///
	/// \param _xmldoc Pointer to document.
     /// \param __mode - writing mode
     /// \li 0 - set mode
     /// \li 1 - domain mode
     /// \li 2 - expr mode
     /// \li 3 - eval mode
	/// \return No retur value.
	void out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode = 0);
	#endif
     
     /// \brief Function loads all data from hml 2.0 format.
	///
	/// \param __root - reference to the parent element
     /// \param __msg - reference to the error messages
     /// \param __attr - pointer to the attribute
     /// \param __cellExpression - denotes if this object represents the whole cell expression
	/// \return the result code:
     /// \li 0 - on success
     /// \li 1 - on error
	int in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr, bool __cellExpression = false);
};
// -----------------------------------------------------------------------------
#endif
