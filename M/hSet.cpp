/*
 *	   $Id: hSet.cpp,v 1.20.4.7 2012-01-11 22:43:35 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QStringList>

#include <math.h>
#include <stdarg.h>

#include "../namespaces/ns_hqed.h"

#include "XTT/XTT_Attribute.h"
#include "hType.h"
#include "hValue.h"
#include "hSetItem.h"
#include "hSet.h"
// -----------------------------------------------------------------------------

// #brief Constructor hSet class.
hSet::hSet(void)
{
     _parentType = NULL;
}
// -----------------------------------------------------------------------------

// #brief Constructor hSet class.
//
// #param hType* __parentType - poiter to the type of data that the set can contain
hSet::hSet(hType* __parentType)
{
     _parentType = __parentType;
}
// -----------------------------------------------------------------------------

// #brief Destructor hSet class.
hSet::~hSet(void)
{
     while(count())
     {
          delete takeFirst();
     }
}
// -----------------------------------------------------------------------------

// #brief Function that creates copy of this object
//
// #param hSet* __destinationItem - the destination copy localization. If NULL function creates new instace of object
// #return The same pointer as given or if __destinationItem = NULL the poiter to the new object that is a copy of current
hSet* hSet::copyTo(hSet* __destinationItem)
{
     hSet* res = __destinationItem;
     if(res == NULL)
          res = new hSet;

     res->setParentType(_parentType);
     for(int i=0;i<size();++i)
     {
          hSetItem* currItm = at(i)->copyTo();
          res->add(currItm);
          //res->append(currItm);
     }
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the parent type of the set
//
// #param hType* __pt - poiter to the type
// #return true on succes, otherwise false
bool hSet::setParentType(hType* __pt)
{
     _parentType = __pt;
     for(int i=0;i<size();++i)
          at(i)->setParentType(__pt);
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the power of the set is one
//
// #return true if the power of the set is one, otherwise false
bool hSet::isSingleValue(void)
{
     bool res = false;
     
     if((size() == 1) && (at(0)->type() == SET_ITEM_TYPE_SINGLE_VALUE))
          res = true;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that transforms the current object that contains a set of values to an object that contains a single value
//
// #return true on success otherwise false
bool hSet::makeItSingle(void)
{
     if(isEmpty())
          return false;
          
     while(size() >= 2)
     {
          delete takeAt(1);
     }
     return at(0)->makeItSingle();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param QString __lastError - the content of the error message
// #return No return value.
void hSet::setLastError(QString __lastError)
{
     _lastError = __lastError;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the last error message
//
// #param int __errorId - the error ID (defined in #define section)
// #param QString __errorMsg - the additional error message
// #return The given error code.
int hSet::setLastError(int __errorId, QString __errorMsg)
{
     QString err_msgs[] = {"Not defined type.",
                           "Incorrect domain item type.",
                           "Not defined parent type.",
                           "Not defined set item.",
                           "Out of domain.",
                           "The set is empty."};
                           
     switch(__errorId)
     {
          case H_SET_ERROR_NOT_DEFINED_TYPE:
               setLastError(err_msgs[0] + __errorMsg);
               break;
               
          case H_SET_ERROR_INCORRECT_DOMAIN_ITEM:
               setLastError(err_msgs[1] + __errorMsg);
               break;
               
          case H_SET_ERROR_PARENT_TYPE_NOT_DEFINED:
               setLastError(err_msgs[2] + __errorMsg);
               break;
               
          case H_SET_ERROR_NOT_DEFINED_SET_ITEM:
               setLastError(err_msgs[3] + __errorMsg);
               break;
               
          case H_SET_ERROR_OUT_OF_DOMAIN:
               setLastError(err_msgs[4] + __errorMsg);
               break;
               
          case H_SET_ERROR_SET_EMPTY:
               setLastError(err_msgs[5] + __errorMsg);
               break;
               
          case H_SET_OTHER_OBJECT_ERROR:
               setLastError(__errorMsg);
               break;

          default:
               setLastError("hSet. Not defined error code (" + QString::number(__errorId) + ").\nAdditional error message: " + __errorMsg);
               break;
     }
     
     return __errorId;
}
// -----------------------------------------------------------------------------

// #brief Function that returns value from the set if the set is single valued, otherwise return nod defined operator
//
// #return value from the set if the set is single valued, otherwise return nod defined operator
QString hSet::toValue(void)
{
     if(isEmpty())
          return toString();
     
     if(!isSingleValue())
          return XTT_Attribute::_not_definded_operator_;
          
     return at(0)->value();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the valued representation of the set
//
// #return the valued representation of the set
hSet* hSet::toVals(void)
{
     hSet* result = new hSet(_parentType);
     for(int i=0;i<size();++i)
     {
          hValueResult hvrf = at(i)->from();
          hValueResult hvrt = at(i)->to();
          result->add(new hSetItem(result, at(i)->type(), new hValue((QString)hvrf), new hValue((QString)hvrt)));
     }
     
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that tries to match the current value to format that is required for this type
//
// #param __allowTrunc - denotes if the digiths can be cut
// #return The result code
int hSet::toCurrentNumericFormat(bool __allowTrunc)
{
     for(int i=0;i<size();++i)
     {
          int r = at(i)->toCurrentNumericFormat(__allowTrunc);
          if(r != H_TYPE_ERROR_NO_ERROR)
               return r;
     }
     return H_TYPE_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string representation of the set
//
// #param __forceSetParentheses - add set delimiters ("{}") to string representation even if set contains only one item
// #return The last error message as string
QString hSet::toString(bool __forceSetParentheses)
{
    if(size() == 0)
        return "{}";

    if(size() == 1 && !__forceSetParentheses)
        return at(0)->toString();
    if(size() == 1 && __forceSetParentheses)
        return "{" + at(0)->toString() + "}";

    QString singleElements = "";
    for(int i=0;i<size();++i)
    {
        if(at(i)->type() == SET_ITEM_TYPE_RANGE)
        {
            if(!singleElements.isEmpty())
                singleElements += ",";
            singleElements += at(i)->toString();
        }

        if(at(i)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
        {
            if(!singleElements.isEmpty())
                singleElements += ",";
            if(singleElements.isEmpty())
                singleElements += "{";
            singleElements += at(i)->toString();
        }
    }

    if(!singleElements.isEmpty())
        singleElements += "}";

    return singleElements;

//     QString setSep = " U ";
//     QString elmSep = ",";
     
//     if(size() == 0)
//          return "{}";
          
//     if(size() == 1 && !__forceSetParentheses)
//          return at(0)->toString();
//     if(size() == 1 && __forceSetParentheses)
//         return "{" + at(0)->toString() + "}";

//     QString singleElements = "";
//     QString rangeElements = "";
//     for(int i=0;i<size();++i)
//     {
//          if(at(i)->type() == SET_ITEM_TYPE_RANGE)
//          {
//               if(!rangeElements.isEmpty())
//                    rangeElements += setSep;
//               rangeElements += at(i)->toString();
//          }
          
//          if(at(i)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
//          {
//               if(!singleElements.isEmpty())
//                    singleElements += elmSep;
//               if(singleElements.isEmpty())
//                    singleElements += "{";
//               singleElements += at(i)->toString();
//          }
//     }
//     if(!singleElements.isEmpty())
//          singleElements += "}";
          
//     if(singleElements.isEmpty())
//          return rangeElements;
//     if(rangeElements.isEmpty())
//          return singleElements;
          
//     return rangeElements + setSep + singleElements;
}
// -----------------------------------------------------------------------------

// #brief Function that maps the set to the prolog code.
//
// #param __option - options of the output
// #return The prolog code that represents this set as string.
QString hSet::toProlog(int __option)
{
     QString result = "";
     
     //bool braces = ((__forceBraces) || (isEmpty()) || (size() > 1)/* or maybe || (power() > 1)*/);
     //if(braces)
     //     result = "[";
     
     if(size() > 0)
          result += at(0)->toProlog(__option);
     
     for(int i=1;i<size();++i)
          result += "," + at(i)->toProlog(__option);
          
     /*if(braces)
          result += "]";*/
     
     return result;
}
// -----------------------------------------------------------------------------

/// \brief Function that returns the drools5 representation of the set
///
/// \return The drools5 representation of the set
QString hSet::toDrools5(void)
{
     QString setSep = " U ";
     QString elmSep = "\\,";

     if(size() == 0)
          return "{}";

     if(size() == 1)
          return at(0)->toString();

     QString singleElements = "";
     QString rangeElements = "";
     for(int i=0;i<size();++i)
     {
          if(at(i)->type() == SET_ITEM_TYPE_RANGE)
          {
               if(!rangeElements.isEmpty())
                    rangeElements += setSep;
               rangeElements += at(i)->toDrools5();
          }

          if(at(i)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
          {
               if(!singleElements.isEmpty())
                    singleElements += elmSep;
               if(singleElements.isEmpty())
                    singleElements += "{";
               singleElements += at(i)->toDrools5();
          }
     }
     if(!singleElements.isEmpty())
          singleElements += "}";

     if(singleElements.isEmpty())
          return rangeElements;
     if(rangeElements.isEmpty())
          return singleElements;

     return rangeElements + setSep + singleElements;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string list representation of the items
//
// #return The string list representation of the items
QStringList hSet::stringItems(void)
{
     QStringList res;
     for(int i=0;i<size();++i)
          res << at(i)->toString();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief The virtual function that return the index of the first item that contains given value
//
// #param QString __value - the value as string
// #return index of the first item that contains given value
int hSet::contain(QString __value)
{
     for(int i=0;i<size();++i)
     {
          int itm_res = at(i)->contains(__value);
          if(itm_res == SET_ITEM_TRUE_VALUE)
               return i;
          
          // Gdy jakis blad
          if(itm_res < 0)
          {
               setLastError("Set::contain error with input value " + __value + ". Error using item: " + at(i)->toString() + ".\nItem error message: " + at(i)->lastError());
               return H_SET_OTHER_OBJECT_ERROR;
          }
     }
     
     return -1;
}
// -----------------------------------------------------------------------------

// #brief The functions that removes the item with given index from the set
//
// #param int __index - idenx of item that should be deleted
// #return true if __index is correct otherwise false
bool hSet::deleteItem(int __index)
{
     if((__index < 0) || (__index >= size()))
     {
          setLastError("Index of item out of range.");
          return false;
     }
     
     delete takeAt(__index);
     if(isEmpty())
          add(new hValue);
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief The functions that removes all the items from the set
//
// #return No value return.
void hSet::flush(void)
{
     while(size())
     {
          delete takeLast();
     }
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the value to the set
//
// #param *__value - pointer to the value that should be added
// #return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
int hSet::add(hValue* __value)
{
     return add(new hSetItem(this, SET_ITEM_TYPE_SINGLE_VALUE, __value, new hValue, false));
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the item to the set
//
// #param hSetItem* __item - item that should be added
// #return H_SET_ERROR_NO_ERROR if all is ok otherwise error code
int hSet::add(hSetItem* __item)
{
     //if(_parentType == NULL)
     //     return setLastError(H_SET_ERROR_PARENT_TYPE_NOT_DEFINED);
     if(__item == NULL)
          return setLastError(H_SET_ERROR_NOT_DEFINED_SET_ITEM);

     __item->setParent(this);
     append(__item);

     return H_SET_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief The functions that adds the item to the set
//
// #param __index - item position
// #param *__item - item that should be inserted
// #return no values return
void hSet::insertItem(int __index, hSetItem* __item)
{
     insert(__index, __item);
}
// -----------------------------------------------------------------------------

// #brief The functions that substract the item from the set
//
// #param hSetItem* __subtrahend - subtrahend item
// #return The set that is result of substraction
hSet* hSet::sub(hSetItem* __subtrahend)
{
     hSet* res = new hSet(_parentType);
     
     for(int i=0;i<size();++i)
     {
          QString s1 = at(i)->toString();
          QString s2 = __subtrahend->toString();
          QString sr = "";
          
          QList<hSetItem*>* tmp = at(i)->sub(__subtrahend);
          if(tmp == NULL)
          {
               delete res;
               setLastError(at(i)->lastError());
               return NULL;
          }
          
          for(int si=0;si<tmp->size();++si)
          {
               sr = sr + tmp->at(si)->toString() + ", ";
               if(res->add(tmp->at(si)) != H_SET_ERROR_NO_ERROR)
               {
                    setLastError(res->lastError());
                    delete res;
                    return NULL;
               }
          }
          delete tmp;
     }
     return res;
}
// -----------------------------------------------------------------------------

// #brief The functions that calculates the sets substraction
//
// #param hSet* __subtrahend - subtrahend set
// #return The set that is result of substraction
hSet* hSet::sub(hSet* __subtrahend)
{
     hSet* res = copyTo();    // Initial result is the surrent set
     for(int i=0;i<__subtrahend->size();++i)
     {
          hSet* sub_res = res->sub(__subtrahend->at(i));
          
          // When error occurs
          if(sub_res == NULL)
          {
               setLastError(res->lastError());
               return NULL;
          }
          
          delete res;

          // When ok
          res = sub_res;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief The functions that checks if the item is in domain
//
// #param hSetItem* __item - pointer to the item
// #return H_SET_TRUE_VALUE if item is in domain otherwise H_SET_FALSE_VALUE. On error returns error code.
int hSet::contain(hSetItem* __item)
{
     hSet* res = new hSet(_parentType);
     res->add(__item->copyTo());
     hSet* substraction = res->sub(this);
     
     if(substraction == NULL)
     {
          setLastError(res->lastError());
          delete res;
          return H_SET_OTHER_OBJECT_ERROR;
     }
     
     bool isempty = substraction->empty();
     delete substraction;
     delete res;
     
     if(isempty)
          return H_SET_TRUE_VALUE;
          
     return H_SET_FALSE_VALUE;
}
// -----------------------------------------------------------------------------

// #brief The functions that checks if the set __item is a sub set of this
//
// #param hSet* __item - pointer to the set
// #return H_SET_TRUE_VALUE if item is a sub set of this set otherwise H_SET_FALSE_VALUE. On error returns error code.
int hSet::contain(hSet* __item)
{
     for(int i=0;i<__item->size();++i)
     {
          int result = contain(__item->at(i)); 
          
          if(result == H_SET_OTHER_OBJECT_ERROR)
               return H_SET_OTHER_OBJECT_ERROR;
               
          if(result == H_SET_FALSE_VALUE)
               return H_SET_FALSE_VALUE;
     }
     
     return H_SET_TRUE_VALUE;
}
// -----------------------------------------------------------------------------

// #brief The functions calculates the power of the item
//
// #param int __index - item index
// #param int* ok - the pointer to the error code
// #return the number of items in given item
quint64 hSet::itemPower(int __index, int* ok)
{
     if((__index < 0) || (__index >= size()))
          return 0;
          
     if(_parentType == NULL)
     {
          if(ok != NULL)
               *ok = H_SET_ERROR_NOT_DEFINED_TYPE;
          setLastError(H_SET_ERROR_NOT_DEFINED_TYPE);
     }
     
     double from_num, to_num;
     int res1, res2;
          
     if(at(__index)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
     {
          res1 = _parentType->mapToNumeric(at(__index)->from(), from_num);
          if(res1 != H_TYPE_ERROR_NO_ERROR)
          {
               setLastError(_parentType->lastError());
               if(ok != NULL)
                    *ok = H_SET_OTHER_OBJECT_ERROR;
               return 0;
          }
          
          if(ok != NULL)
               *ok = H_SET_ERROR_NO_ERROR;
          return 1;
     }
          
     quint64 res = 1;
     
     // Getting the numeric representation of items
     res1 = _parentType->mapToNumeric(at(__index)->from(), from_num);
     res2 = _parentType->mapToNumeric(at(__index)->to(), to_num);
     
     if((res1 != H_TYPE_ERROR_NO_ERROR) || (res2 != H_TYPE_ERROR_NO_ERROR))
     {
          setLastError(_parentType->lastError());
          if(ok != NULL)
               *ok = H_SET_OTHER_OBJECT_ERROR;
          return 0;
     }
     
     if(from_num > to_num)
     {
          if(ok != NULL)
               *ok = H_SET_ERROR_NO_ERROR;
          return 0;
     }
          
     // Power calculation
     double d = to_num-from_num;
     if(_parentType->baseType() == NUMERIC_BASED)
          d *= pow(10, _parentType->allowedDecimalLength());     // When numeric

     res += (quint64)d;
     
     if(ok != NULL)
          *ok = H_SET_ERROR_NO_ERROR;
     return res;
}
// -----------------------------------------------------------------------------

// #brief The functions calculates the power of the set
//
// #param int* ok - the pointer to the error code
// #return the number of items in current set
quint64 hSet::power(int* ok)
{
     quint64 res = 0;
     for(int i=0;i<size();++i)
     {
          int oki;
          res += itemPower(i, &oki);
          
          if(oki != H_SET_ERROR_NO_ERROR)
          {
               if(ok != NULL)
                    *ok = oki;
               return 0;
          }
     }
     
     if(ok != NULL)
          *ok = H_SET_ERROR_NO_ERROR;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief The functions sorts the set
//
// #param int __policy - the sorting criterions
// #param int __direction - ascending, descent
// #return the result code
int hSet::sort(int __policy, int __direction)
{
     if(_parentType == NULL)
          return setLastError(H_SET_ERROR_PARENT_TYPE_NOT_DEFINED);
          
     bool _text_data_type = (__policy == H_SET_SORT_ALPHABETICAL);

     for(int i=0;i<size();++i)
     {
          for(int j=0;j<size()-i-1;++j)
          {
               int res1, res2;
               double d_itm1, d_itm2;
               QString s_itm1, s_itm2;
               
               // Sorting with from value
               if(__policy == H_SET_SORT_FROM_VALUE)
               {
                    res1 = _parentType->mapToNumeric(at(j)->value(), d_itm1);
                    res2 = _parentType->mapToNumeric(at(j+1)->value(), d_itm2);
                    if((res1 != H_TYPE_ERROR_NO_ERROR) || (res2 != H_TYPE_ERROR_NO_ERROR))
                    {
                         setLastError("Sort action fialed.\nError message: " + _parentType->lastError());
                         return H_SET_OTHER_OBJECT_ERROR;
                    }
               }
               
               // Sorting with to value
               if(__policy == H_SET_SORT_TO_VALUE)
               {
                    QString v1 = at(j)->to();
                    if(at(j)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
                    {
                         hValueResult hvr = at(j)->value();
                         if(!(bool)hvr)
                              return setLastError(H_SET_OTHER_OBJECT_ERROR, (QString)hvr);
                         v1 = (QString)hvr;
                    }
                    QString v2 = at(j+1)->to();
                    if(at(j+1)->type() == SET_ITEM_TYPE_SINGLE_VALUE)
                    {
                         hValueResult hvr = at(j+1)->value();
                         if(!(bool)hvr)
                              return setLastError(H_SET_OTHER_OBJECT_ERROR, (QString)hvr);
                         v2 = (QString)hvr;
                    }
                    
                    res1 = _parentType->mapToNumeric(v1, d_itm1);
                    res2 = _parentType->mapToNumeric(v2, d_itm2);
                    if((res1 != H_TYPE_ERROR_NO_ERROR) || (res2 != H_TYPE_ERROR_NO_ERROR))
                    {
                         setLastError("Sort action fialed.\nError message: " + _parentType->lastError());
                         return H_SET_OTHER_OBJECT_ERROR;
                    }
               }
               
               // Sorting with power
               if(__policy == H_SET_SORT_POWER)
               {
                    int res1, res2;
                    d_itm1 = (double)itemPower(j, &res1);
                    d_itm2 = (double)itemPower(j+1, &res2);
                    if((res1 != H_SET_ERROR_NO_ERROR) || (res2 != H_SET_ERROR_NO_ERROR))
                    {
                         setLastError("Sort action fialed.\nError message: " + lastError());
                         return H_SET_OTHER_OBJECT_ERROR;
                    }
               }
               
               // Sorting alphabetical
               if(__policy == H_SET_SORT_ALPHABETICAL)
               {
                    hValueResult hvr1 = at(j)->value(),
                                 hvr2 = at(j+1)->value();
                                 
                    if(!(bool)hvr1)
                         return setLastError(H_SET_OTHER_OBJECT_ERROR, (QString)hvr1);
                    if(!(bool)hvr2)
                         return setLastError(H_SET_OTHER_OBJECT_ERROR, (QString)hvr2);
                                 
                    s_itm1 = (QString)hvr1;
                    s_itm2 = (QString)hvr2;
               }
               
               // Sorting
               
               // When text is sorted
               if(_text_data_type)
               {
                    if((__direction == H_SET_SORT_ASCENDING) && (s_itm1 > s_itm2))
                         swapItems(j, j+1);
                    if((__direction == H_SET_SORT_DESCEND) && (s_itm1 < s_itm2))
                         swapItems(j, j+1);
               }
               
               // When numbers are soreted
               if(!_text_data_type)
               {
                    if((__direction == H_SET_SORT_ASCENDING) && (d_itm1 > d_itm2))
                         swapItems(j, j+1);
                    if((__direction == H_SET_SORT_DESCEND) && (d_itm1 < d_itm2))
                         swapItems(j, j+1);
               }
          }
     }
     
     return H_SET_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief The functions swaps two items
//
// #param __index1 - index of the first item
// #param __index2 - index of the second item
// #return no values return
void hSet::swapItems(int __index1, int __index2)
{
     swap(__index1, __index2);
}
// -----------------------------------------------------------------------------

// #brief The functions transforms the set to the more simple form. The new items has no intersections.
//
// #return result code
int hSet::simplify(void)
{
     if(empty())
          return setLastError(H_SET_ERROR_SET_EMPTY);

     // For better quality of result the sort function is called
     sort(H_SET_SORT_POWER, H_SET_SORT_DESCEND);
     
     for(int i=0;i<size()-1;++i)
     {    
          for(int j=size()-1;j>i;--j)
          {
               QList<hSetItem*>* t = at(j)->sub(at(i));
               
               // When error
               if(t == NULL)
               {
                    setLastError(at(j)->lastError());
                    return H_SET_OTHER_OBJECT_ERROR;
               }
               
               // Otherwise
               delete takeAt(j);
               for(int g=0;g<t->size();++g)
                    insert(j+g, t->at(g));
               delete t;
          }
     }
     
     return H_SET_ERROR_NO_ERROR;
}
// -----------------------------------------------------------------------------

// #brief Function that updates int all the types the pointer to the new type
//
// #param hType* __old - the value of the old pointer
// #param hType* __new - the value of the new pointer
// #return true on succes otherwise false
bool hSet::updateTypePointer(hType* __old, hType* __new)
{
     bool res = true;
     
     if(_parentType == __old)
          _parentType = __new;
     
     for(int i=0;i<size();++i)
          res = at(i)->updateTypePointer(__old, __new) && res;
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that is called when some imporatant event occurs. It sends the information about event to the others object.
//
// #param *__errmsgs - pointer to the string that contains the error message that are generated on event.
// #param __event - the event identifier
// #param ... - the list of event params
// #return true on succes otherwise false
bool hSet::eventMessage(QString* __errmsgs, int __event, ...)
{  
     bool res = true;
     va_list argsptr;
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_CHANGED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int i=0;i<size();++i)
               res = at(i)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
     
     if(__event == XTT_SIGNAL_ATTRIBUTE_REMOVED)
     {
          va_start(argsptr, __event);
          XTT_Attribute* attr = va_arg(argsptr, XTT_Attribute*);
          va_end(argsptr);
          
          for(int i=0;i<size();++i)
               res = at(i)->eventMessage(__errmsgs, __event, attr) && res;
          
          return res;
     }
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the avr is used to define this object's value
//
// #return true is yes otherwise false
bool hSet::isAVRused(void)
{
     for(int i=0;i<size();++i)
          if(at(i)->isAVRused())
               return true;
          
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the expression is used to define this object's value
//
// #return true if yes otherwise false
bool hSet::isExpressionUsed(void)
{
     for(int i=0;i<size();++i)
          if(at(i)->isExpressionUsed())
               return true;
          
     return false;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the value is not defined
//
// #return true if yes otherwise false
bool hSet::isNotDefinedValue(void)
{
    //qDebug() << "isSingleValue(): " << isSingleValue() << ",  at(0)->type() == NOT_DEFINED: " <<  (at(0)->type() == NOT_DEFINED);
    return isSingleValue() && at(0)->type() == NOT_DEFINED;
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the set contains only constant definitions of values
//
// #return true if yes otherwise false
bool hSet::hasOnlyConstantValues(void)
{
     return (!isAVRused()) && (!isExpressionUsed());
}
// -----------------------------------------------------------------------------

// #brief Function that returns if the type object definition is correct
//
// #param __error_message - the reference to the error message(s)
// #return true if yes otherwise false
bool hSet::setVerification(QString& /*__error_message*/)
{   
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function gets all data in hml 2.0 format.
//
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return List of all data in hml 2.0 format.
QStringList* hSet::out_HML_2_0(int __mode)
{
     QStringList* res = new QStringList;
     res->clear();
     
     bool isSet = (!isExpressionUsed()) && 
                  (__mode != 2) &&
                  (
                    ((isSingleValue()) && 
                         ((at(0)->valuePtr()->type() == NOT_DEFINED) ||
                          (at(0)->valuePtr()->type() == ANY_VALUE) ||
                          (at(0)->valuePtr()->type() == VALUE)
                         )
                    ) ||
                     (!isSingleValue())
                  ); 
                   
     if(isSet)
          *res << "<set>";

     // Zapis tabel
     for(int a=0;a<size();++a)
     {
          QStringList* al = at(a)->out_HML_2_0(__mode);
          for(int l=0;l<al->size();l++)
               *res << "\t" + al->at(l);
          delete al;
     }
     
     if(isSet)
          *res << "</set>";

     return res;
}
// -----------------------------------------------------------------------------

#if QT_VERSION >= 0x040300
// #brief Function saves all data in hml 2.0 format.
//
// #param _xmldoc Pointer to document.
// #param __mode - writing mode
// #li 0 - default - set mode
// #li 1 - domain mode
// #li 2 - expr mode
// #li 3 - eval mode
// #return No retur value.
void hSet::out_HML_2_0(QXmlStreamWriter* _xmldoc, int __mode)
{
     bool isSet = (!isExpressionUsed()) && 
                  (__mode != 2) &&
                  (
                    ((isSingleValue()) && 
                         ((at(0)->valuePtr()->type() == NOT_DEFINED) ||
                          (at(0)->valuePtr()->type() == ANY_VALUE) ||
                          (at(0)->valuePtr()->type() == VALUE)
                         )
                    ) ||
                     (!isSingleValue())
                  ); 
           
     if((isSet) && (__mode != 2))
          _xmldoc->writeStartElement("set");

     // Zapis tabel
     for(int a=0;a<size();++a)
          at(a)->out_HML_2_0(_xmldoc, __mode);
          
     if((isSet) && (__mode != 2))
          _xmldoc->writeEndElement();
}
#endif
// -----------------------------------------------------------------------------

// #brief The functions returns the set with one item that has value equal to not_defined operator
//
// #param hType* __parentType - the parent type of the set
// #return pointer to the set object that conatains not_defined value
hSet* hSet::makeNotDefinedValue(hType* __parentType)
{
     return hValue::toSet(__parentType, new hValue(XTT_Attribute::_not_definded_operator_));
}
// -----------------------------------------------------------------------------

// #brief The functions returns the set with one item that has value equal to any operator
//
// #param hType* __parentType - the parent type of the set
// #return pointer to the set object that conatains any value
hSet* hSet::makeAnyValue(hType* __parentType)
{
     return hValue::toSet(__parentType, new hValue(XTT_Attribute::_any_operator_));
}
// -----------------------------------------------------------------------------

// #brief Function loads all data from hml 2.0 format.
//
// #param __root - reference to the parent element
// #param __msg - reference to the error messages
// #param __attr - pointer to the attribute
// #return the result code:
// #li 0 - on success
// #li 1 - on error
int hSet::in_HML_2_0(QDomElement& __root, QString& __msg, XTT_Attribute* __attr)
{
     int result = 0;
     QString elementName = "set";

     if(__root.tagName().toLower() != elementName)
          return 0;

     QDomElement DOMhval = __root.firstChildElement();
     while(!DOMhval.isNull())
     {
          hSetItem* newItem = new hSetItem(this, SET_ITEM_TYPE_SINGLE_VALUE, new hValue, new hValue, false);
          add(newItem);
          result += newItem->in_HML_2_0(DOMhval, __msg, __attr);
          
          DOMhval = DOMhval.nextSiblingElement();
     }
          
     return result;
}
// -----------------------------------------------------------------------------
