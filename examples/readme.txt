The "examples" directory contains the xml files that are used to HQEd testing.
This file contains the description of the files in the "examples" directory.
Some of the files have errors and some of them have not.
The appropriate information about errors is placed next to a file name:
file name (ERRORS) - indicates the file that has error(s)
file name (OK)	   - indicates the file that has NOT errors.

Description:

========== HML FILES ==========

  * ATM.hml (ERRORS)
Model of the ATM. The model has the following errors:
- AVR that is used in conditional parts of the rule.
- Incorrect value w.r.t. to attribute domain.
- Rules that have defined more than one link.
- Incorrect use of output attribute in conditional context.

  * ATMst.hml (ERRORS)
Model of the ATM. The model is very similar to the foregoing, but instead of some tables the file contains the definitions of the states.

  * factor2.0.hml (ERRORS)
The example of factorial calculation. The model has the following errors:
- Incorrect values w.r.t. to domains.

  * factor2.0st.hml (OK)
The example of factorial calculation. The example has not got errors.

  * therm-nost.hml (OK)
The model of the Thermostat system. The model does not contain states definition.

  * t1.hml (OK)
The ARD model of the Thermostat system.
The file can be used to ARD loading and generating XTT.

  * t2.hml (OK)
The ARD userv model.
The file can be used to ARD loading and generating XTT.

  * thermostat2.0.hml (ERROR)
The model of the thermostat system. The model does not conatin information about states and has the following errors:
- The expression which result is out of domain.

  * thermostat2.0ok.hml (OK)
The complete model of the Thermostat system.

  * thermostat2.0st.hml (ERROR)
The model of the thermostat system witht states definition. The model contains the following errors:
- The expression which result is out of domain.
- The incorrect value w.r.t. domain in the state definition.

  * userv.hml (OK)
The schema of userv model that has been generated from ARD model.

  * loanard.hml (OK)
This is an ARD model of a system which computes cutomer's credit rating. 
It takes into account a few parameters describing customer status and computes his status in two stages. 
Firstly it does a preliminary check using customer's base financial parameters and if he passes this test his financial status will be analyzed more thoroughly.

  * healthard.hml (OK)
An ARD model of the system that comes from http://openrules.com/examples.htm

  * healthxtt.hml (OK)
An XTT model of the system that comes from http://openrules.com/examples.htm

  * healthxtterr.hml (ERROR)
An XTT model of the system that comes from http://openrules.com/examples.htm
The model contains a following errors:
- Inaccesible row and connection
- One cell contains value out of domain

  * helloard.hml (OK)
The ARD model of the system that tries is to tell greetings based on the time of day and customer's attributes.


========== XTTML FILES ==========

  * ATM.xttml (ERROR)
Same as ATM.hml but defined in XTTML 2.0 format.
For more information see ATM.hml.

  * cond2.0.xttml
  * cut_test.xttml
  * factor.xttml
  * linkLabelingTest2.0.xttml
  * sum.xttml
The files in XTTML 1.0 format. The current version of HQEd does not support this format.

  * cut_test2.0.xttml (ERROR)
The file that contains the model that tests the cut connection.
The cut connections are not supported in hml 2.0.
The file contains the following errors:
- Incorrect use of the attributes in the context.

  * easy_factor2.0.xttml (ERROR)
The file conatins incomplete model (without cell definitions).

  * factor2.0.xttml (ERROR)
The example that shows a factor calculation.
Errors:
- Incorrect use of attribute in context.

  * ff2.0.xttml (??)
The model that conatins some calculation.
HQEd does not pass (at this moment)  this test - it crashes - seg fault.

  * loop2.0.xttml (ERROR)
The simple loop implementation.
Errors:
- HQEd does not recognize the root function (BUG).
- Incorrect use of attribute in context.

  * student_thermostat_mess2.0.xttml (ERROR)
The model of the thermostat system in xttml 2.0 format.
The model contains the following errors:
- Incorrect definitions of sets and ranges in cells.
- Incorrect use of attribute in context.

  * sum2.0.xttml (ERROR)
The example that shows the simple loop implementation.
The file has been used to testing HQEd compatybility with xttml2.0
Errors:
- Incorrect use of attribute in context.

  * test2.0.xttml (OK)
The model of system that tests the correctness of table analysis.

  * thermostat2.0.xttml (ERROR)
Again, the thermostat system model.
Errors:
- Incorrect use of attribute in context.

  * userv_wc.xttml (OK)
The schemas of the tables of the userv model.

  * uservopt.xttml (OK)
The tables with connections of the userv model.
The file does not contain Fact table/states definitions.

  * week_test2.0.xttml (ERROR)
The simple example testing conditional statements.
Errors:
- Incorrect use of attribute in context.

========== ATTML FILES ==========

  * all.attml
The file contains lot of attributes. The file has been used to testing attributes importing.

  * week.attml
The file contains the attributes used in week_test2.0.xttml model.

  * myk.attml
The next file with attributes.


========== ARDML FILES ==========

These files contains the ARD models.
