/*
 *	  $Id: main.cpp,v 1.48.4.1.2.8 2011-09-29 09:18:04 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QApplication>
#include <QPlastiqueStyle>
#include <QCDEStyle>
#include <QWindowsXPStyle>
#include <QMotifStyle>
#include <QMessageBox>
#include <QtDebug>

#include "namespaces/ns_hqed.h"

#include "M/ARD/ARD_LevelList.h"
#include "M/ARD/ARD_AttributeList.h"
#include "M/ARD/ARD_PropertyList.h"
#include "M/ARD/ARD_DependencyList.h"

#include "M/hModel.h"
#include "M/hTypeList.h"
#include "M/hModelList.h"
#include "M/hFunctionList.h"

#include "M/XTT/XTT_States.h"
#include "M/XTT/XTT_TableList.h"
#include "M/XTT/XTT_AttributeGroups.h"
#include "M/XTT/XTT_ConnectionsList.h"
#include "M/XTT/XTT_CallbackContainer.h"

#include "C/devPluginInterface/devPluginController.h"

#include "C/ARD/ardAttributeManager.h"

#include "C/hqedserver.h"

#include "C/XTT/PluginEventListner_ui.h"
#include "C/XTT/ItemFinder_ui.h"
#include "C/XTT/ValEdit_ui.h"
#include "C/XTT/UnusedAttributes_ui.h"
#include "C/XTT/ConnectionManager_ui.h"
#include "C/XTT/DeleteConnection_ui.h"
#include "C/XTT/ConnectionEditor_ui.h"
#include "C/XTT/CellEditor_ui.h"
#include "C/XTT/DeleteTable_ui.h"
#include "C/XTT/AttributeEditor_ui.h"
#include "C/XTT/AttributeDelete_ui.h"
#include "C/XTT/AttributeManager_ui.h"
#include "C/XTT/GroupManager_ui.h"
#include "C/XTT/GroupEditor_ui.h"
#include "C/XTT/SelectGroup_ui.h"
#include "C/XTT/TableEditor_ui.h"
#include "C/XTT/ColumnEditor_ui.h"
#include "C/XTT/TypeManager_ui.h"
#include "C/XTT/TypeEditor_ui.h"
#include "C/XTT/PluginEditor_ui.h"
#include "C/XTT/StateSchemaEditor_ui.h"
#include "C/XTT/ValuePresentationDialog_ui.h"
#include "C/XTT/ValuePresentationScriptEditor_ui.h"
#include "C/XTT/SetCreator_ui.h"
#include "C/Settings_ui.h"
#include "C/TextEditor_ui.h"
#include "C/MainWin_ui.h"
#include "C/About_ui.h"
// -----------------------------------------------------------------------------

/// \brief main function of the appliaction
///
/// \param argc - the number of command line arguments
/// \param *argv[] - pointer to arrary of the arguments
/// \return result of qt application into OS
int main(int argc, char* argv[])
{   
    QApplication app(argc, argv, true);
    Q_INIT_RESOURCE(appresfile);

    QApplication::setStyle(new QPlastiqueStyle);
    QApplication::setDesktopSettingsAware(false);

    hqed::setLocalSettings();
    hqed::initClipboard();

    QApplication::setEffectEnabled(Qt::UI_AnimateMenu);
    QApplication::setEffectEnabled(Qt::UI_FadeMenu);
    QApplication::setEffectEnabled(Qt::UI_AnimateCombo);
    QApplication::setEffectEnabled(Qt::UI_AnimateTooltip);
    QApplication::setEffectEnabled(Qt::UI_FadeTooltip);
    QApplication::setEffectEnabled(Qt::UI_AnimateToolBox);
    
    app.setWindowIcon(QIcon(":/all_images/images/mainico.png"));

    // Tworzenie interfejsu pluginow
    devPC = new devPluginController;

    // Tworzenie okienka ustawien przed wszystim, aby nie bylo mozliwosci odwolan do nieistniejecych obiektow
    MainWin = NULL;
    Settings = new Settings_ui;

    CallbackContainer = new XTT_CallbackContainer;

    hqed::createModelList();                               // Creating model list
    hqed::modelList()->createNewModel(true);               // Creating new model
    //heart::createSocket();                               // Creating new socket for heart communication
    //devPC->registerPlugin(heart::socket(), hqed::logicServerName());

    // Creating server
    hqedServerInstance = new hqedServer;

    // Creating function list
    funcList = new hFunctionList;
    funcList->fill();

    // Alokacja pamieci na okienka
    ardAttributeManager = new ardAttributeManager_ui;

    TypeManager = new TypeManager_ui;
    TypeEditor = new TypeEditor_ui;
    ItemFinderWin = new ItemFinder_ui;
    AttributeEditor = new AttrEditor;
    AttributeDelete = new AttributeDelete_ui;
    AttrManager = new AttributeManager_ui;
    GroupMan = new GroupManager_ui;
    GroupEdit = new GroupEditor_ui;
    SelGroup = new SelectGroup_ui;
    TableEdit = new TableEditor_ui;
    ColumnEdit = new ColumnEditor_ui;
    DelTable = new DeleteTable_ui;
    CellEdit = new CellEditor_ui;
    ConnEdit = new ConnectionEditor_ui;
    DelConn = new DeleteConnection_ui;
    ConnManager = new ConnectionManager_ui;
    UnusedAttr = new UnusedAtrtribute_ui;
    AboutWin = new About_ui;
    ValEdit = new ValueEdit_ui;
    PluginEditor = new PluginEditor_ui;
    pluginEventListener = new PluginEventListner_ui;
    TextEditor = new TextEditor_ui;
    ValuePresentationDialog = new ValuePresentationDialog_ui;
    ValuePresentationScriptEditor = new ValuePresentationScriptEditor_ui;
    SetCreator = new SetCreator_ui;
    StateSchemaEditor = new StateSchemaEditor_ui;

    MainWin = new MainWin_ui;
    hqed::modelList()->activeModel()->registerModel();
    MainWin->initWindow(hqed::modelList()->activeModel());

    // Zastosowanie ustawien
    Settings->applyOptions();
    MainWin->show();

    return app.exec();
}
// -----------------------------------------------------------------------------
