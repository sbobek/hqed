/**
 * \file     MainWin_ui.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date       24.04.2007 
 * \version    1.0
 * \brief      This file contains class definition arranges main window of application. CONTROLLER.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef MAINWINUI_H
#define MAINWINUI_H
// -----------------------------------------------------------------------------

#include <QDomDocument>
#include <QAbstractSocket>

#include "ui_MainWin.h"
#include "widgets/GSceneLocalizator.h"
// -----------------------------------------------------------------------------

#define ARD_MODE 0
#define XTT_MODE 1
#define DOCKED_WIDGETS 9
// -----------------------------------------------------------------------------

class QDial;
class QMenu;
class QLabel;
class QSpinBox;
class QCheckBox;
class QListWidget;
class QGridLayout;
class QSpacerItem;
class QGraphicsView;
class QGraphicsScene;

class devPlugin;

class hModel;
class XTT_Table;
class XTT_Connections;

class sysTrayObject;
class MyGraphicsView;
class MyGraphicsScene;
class ardGraphicsView;
class ardGraphicsScene;
class ErrorsListWidget;
class ExecMsgListWidget;
class StateGraphicsView;
class StateGraphicsScene;

class HelpEngine;
class HelpWidget;
// -----------------------------------------------------------------------------
/**
* \class      MainWin_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       24.04.2007
* \brief      This class arranges main window of application.
*/
class MainWin_ui : public QMainWindow, public Ui_MainWindow
{
     Q_OBJECT

private:
     
     QPrinter* printer;                      ///< Pointer to printer.
     
     QMenu* toolBarMenu;                     ///< Pointer to menu toolbar.
     QMenu* dockedWindows;                   ///< Pointer to menu docked widnows.

     QToolBar* fileToolBar;                  ///< Pointer to file toolbar.
     QToolBar* viewToolBar;                  ///< Pointer to view toolbar.
     QToolBar* tableToolBar;                 ///< Pointer to table toolbar.
     QToolBar* attributesToolBar;            ///< Pointer to attributes toolbar.
     QToolBar* typeToolBar;                  ///< Pointer to type toolbar.
     QToolBar* connectionsToolBar;           ///< Pointer to connections toolbar.
     QToolBar* executeToolBar;               ///< Pointer to execute toolbar.
     QToolBar* toolsToolBar;                 ///< Pointer to tools toolbar.
     QToolBar* helpToolBar;                  ///< Pointer to help toolbar.
     
     ardGraphicsScene* fCurrentARDscene;     ///< Pointer to the one of scenes. It determines  which scene was lately active, or is still active.

     bool fShowARDtip;     ///< Determines whether ARD tips should be visible after switching ARD mode.

     bool fShowXTTtip;     ///< Determines whether XTT tips should be visible after switching XTT mode.
     
     bool fDockWidgetsVisibleStatus[DOCKED_WIDGETS];      ///< Holds the visiblity status of the docked widgets
     
     int appMode;          ///< Determines the mode of program (0 - editor ARD,1 - editor XTT).

     QString fArdFileName;     ///< Name of current opened ARD file.

     QString fXttFileName;     ///< Name of current opened XTT file.

     bool isARDFile;          ///< Determines whether current project is ARD file.

     bool isXTTFile;          ///< Determines whether current project is XTT file.

     /// \brief Function triggered when window is closed.
     ///
     /// \param event Pointer to event object.
     /// \return No return value.     
     void closeEvent(QCloseEvent* event);
     
     /// \brief Create dock windows for ARD mode.
     ///
     /// \return No return value.
     void ard_createDockWindows(void);
     
     /// \brief Create dock windows for XTT mode.
     ///
     /// \return No return value.
     void xtt_createDockWindows(void);
     
     /// \brief Create toolbars for XTT mode.
     ///
     /// \return No return value.
     void xtt_createToolBars(void);
     
     /// \brief Create menu for XTT mode.
     ///
     /// \return No return value.
     void xtt_createMenu(void);
     
     /// \brief Create statusbar.
     ///
     /// \return No return value.
     void createStatusBar(void);
     
     /// \brief Form 'open recent files' menu.
     ///
     /// \return No return value.
     void formRecentFilesList();

     void createHelpWidget();

public:

     // ----------------- Objects -----------------
     
     MyGraphicsView* xtt_graphicsView;                                ///< Pointer to screen for XTT scene.
     StateGraphicsView* xtt_state_graphicsView;                       ///< Pointer to screen for XTT states scene.
     ardGraphicsView* ard_modelView;                                  ///< Pointer to screen for ARD scene.
     ardGraphicsView* ard_tphView;                                    ///< Pointer to screen for TPH scene.
     
     MyGraphicsScene* xtt_scene;                                      ///< Pointer to scene of XTT drawing.
     StateGraphicsScene* xtt_state_scene;                             ///< Pointer to scene of XTT states drawing.
     ardGraphicsScene* ard_modelScene;                                ///< Pointer to scene of ARD drawing.
     ardGraphicsScene* ard_thpScene;                                  ///< Pointer to scene of TPH drawing.
     
     QDockWidget* ard_tphDockWindow;                                  ///< Pointer to TPH dock window.
     QDockWidget* ard_localizatorDockWindow;                          ///< Pointer to ARD scene localizator
     QDockWidget* tph_localizatorDockWindow;                          ///< Pointer to TPH scene localizator
     QDockWidget* xtt_errorsDockWindow;                               ///< Pointer to XTT errors dock window.
     QDockWidget* xtt_statesDockWindow;                               ///< Pointer to XTT states dock window.
     QDockWidget* xtt_runModelMessagesDockWindow;                     ///< Pointer to XTT simulation messages dock window.
     QDockWidget* xtt_localizatorDockWindow;                          ///< Pointer to XTT scene localizator
     QDockWidget* xtt_itemFinderDockWindow;                           ///< Pointer to finder dock window.
     QDockWidget* xtt_pluginEventListenerDockWindow;                  ///< Pointer to plugin event listener dialog
     
     ErrorsListWidget* xtt_errorsList;                                ///< Pointer to error list displaying in dock window.
     
     GSceneLocalizator* ard_localizator;                              ///< Pointer to ARD scene localizator
     GSceneLocalizator* tph_localizator;                              ///< Pointer to TPH scene localizator
     GSceneLocalizator* xtt_localizator;                              ///< Pointer to XTT scene localizator
     
     QWidget* xtt_runModelMessagesDockWindowContents;                 ///< Pointer to object of dock window displaying runtime error model.
     QGridLayout* xtt_runModelMessagesDockWindowGridLayout;           ///< Pointer to layout of dock window displaying runtime error model.
     ExecMsgListWidget* xtt_runModelMessagesDockWindowMessagesList;   ///< Pointer to list of messages in dock window displaying runtime error model.
     QSpacerItem* xtt_runModelMessagesDockWindowSpacerItem;           ///< Pointer to spacer object of dock window displaying runtime error model.
     QCheckBox* xtt_runModelMessagesDockWindowInfoCheckBox;           ///< Pointer to filter checkbox (info).
     QCheckBox* xtt_runModelMessagesDockWindowWarningCheckBox;        ///< Pointer to filter checkbox (warning).
     QCheckBox* xtt_runModelMessagesDockWindowErrorCheckBox;          ///< Pointer to filter checkbox (error).
     QLabel* xtt_runModelMessagesDockWindowLabel;                     ///< Pointer to filter label in dock window displaying runtime error model.
     QLabel* xtt_runModelMessagesDockWindowControlLabel;              ///< Pointer to control label in dock window displaying runtime error model.
     QToolButton* xtt_runModelMessagesDockWindowControlPlay;          ///< Pointer to the play tool button
     QToolButton* xtt_runModelMessagesDockWindowControlStop;          ///< Pointer to the stop tool button
     QToolButton* xtt_runModelMessagesDockWindowControlPause;         ///< Pointer to the pause tool button
     QToolButton* xtt_runModelMessagesDockWindowControlStepBack;      ///< Pointer to the step back tool button
     QToolButton* xtt_runModelMessagesDockWindowControlStepNext;      ///< Pointer to the step next tool button
     QToolButton* xtt_runModelMessagesDockWindowControlBegin;         ///< Pointer to the begin tool button
     QToolButton* xtt_runModelMessagesDockWindowControlEnd;           ///< Pointer to the end tool button
     QToolButton* xtt_runModelMessagesDockWindowControlInterval;      ///< Pointer to the interval tool button
     
     sysTrayObject* trayIcon;                                         ///< Displaying object in system resource.
     QProgressDialog* publicProgressDialog;                           ///< Pointer to the public progress dialog, that can be used by other objects

     //HELP
     HelpEngine *helpEngine;
     HelpWidget *helpWidget;     


     // ----------------- Methods -----------------

     /// \brief Constructor MainWin_ui class.
     ///
     /// \param *parent - Pointer to parent.
     MainWin_ui(QMainWindow *parent = 0);
     
     /// \brief Destructore MainWin_ui class.
     ~MainWin_ui(void);
     
     /// \brief Function that sets the initial values of the window class
     ///
     /// \param __initModel - pointer to the optional initialize model
     /// \return no values returns
     void initWindow(hModel* __initModel = NULL);
     
     /// \brief Stores current values to the given model
     ///
     /// \param __model - pointer to the model
     /// \return no values return
     void updateModel(hModel* __model);
     
     /// \brief Switches beetween two models
     ///
     /// \param __current - pointer to the current model
     /// \param __destination - pointer to the destination model model
     /// \return no values return
     void switchModels(hModel* __current, hModel* __destination);
     
     /// \brief Get current mode of application.
     ///
     /// \return 0 - editor ARD, 1 - editor XTT.
     int currentMode(void);
     
     /// \brief Set current mode of application.
     ///
     /// \param _newMode - New mode of application: 0 - editor ARD, 1 - editor XTT.
     /// \return No return value.
     void setCurrentMode(int _newMode);

     /// \brief Function is triggered when key is pressed.
     ///
     /// \param event - Pointer to event object.
     /// \return No return value.
     void keyPressEvent(QKeyEvent* event);
     
     /// \brief Set current ART scene.
     ///
     /// \param ardncs - Pointer to current scene ARD.
     /// \return No return value..
     void setCurrentARDscene(ardGraphicsScene* ardncs);
     
     /// \brief Get pointer to current ARD scene.
     ///
     /// \return Pointer to current ARD scene.
     ardGraphicsScene* currentARDscene(void) { return fCurrentARDscene; }

     /// \brief Check ARD model changes.
     ///
     /// \return True when model is changed, otherwise false.
     bool ARD_Changed(void);
     
     /// \brief Check XTT model changes.
     ///
     /// \return True when model is changed, otherwise false.
     bool XTT_Changed(void);
     
     /// \brief Set status of modyfication ARD model.
     /// 
     /// \param _c - Status of modyfication.
     /// \return No return value.
     void setARD_Changed(bool _c);
     
     /// \brief Set status of modyfication XTT model.
     /// 
     /// \param _c Status of modyfication.
     /// \return No return value.
     void setXTT_Changed(bool _c);

     /// \brief Ask the user if the changes should be saved
     ///
     /// \param __mode - denotes where the changes must be confirmed
     /// \li ARD_MODE - changes in the ARD model
     /// \li XTT_MODE - changes in the XTT model
     /// \return 0 - Cancel, 1 - Yes, -1 - No.
     int confirmDiscardChanges(int __mode);
     
     /// \brief Create new XTT project.
     ///
     /// \param _keepAttributes - Determines whether leave argument list.
     /// \return no values return
     void CreateNewXTTProject(bool _keepAttributes);
     
     /// \brief Create new ARD project.
     ///
     /// \param __startnew - indicates if the new ARD model should start from editable root (true) or it will be read from file (false)
     /// \return no values return
     void CreateNewARDProject(bool __startnew = false);

     /// \brief Get state of black and white color of model view.
     ///
     /// \return True when model view should be black and white.
     bool ModelBW(void){ return actionB_W_view->isChecked(); }
     
     /// \brief Get state of antialiasing.
     ///
     /// \return True when antialiasing is active.
     bool antialiasingStatus(void){ return actionAntialiasing->isChecked(); }
     
     /// \brief Get state of antialiasing for text.
     ///
     /// \return True when antialiasing is active for text.
     bool textAntialiasingStatus(void){ return actionTextAntialiasing->isChecked(); }
     
     /// \brief Get information about smooth bitmap transformation.
     ///
     /// \return True when smooth bitmap transformation active.
     bool smoothPixmapTransformStatus(void){ return actionSmooth_pixmap_transform->isChecked(); }
     
     /// \brief Get state of ARD tip.
     ///
     /// \return True when ARD tip is active.
     bool showARDtip(void) { return fShowARDtip; }
     
     /// \brief Get state of XTT tip.
     ///
     /// \return True when XTT tip is active.
     bool showXTTtip(void) { return fShowXTTtip; }
     
     /// \brief Set state of ARD tip.
     ///
     /// \param _v - State of ARD tip, true - active, false - inactive.
     /// \return No return value.
     void setShowARDtip(bool _v) { fShowARDtip = _v; }
     
     /// \brief Set state of XTT tip.
     ///
     /// \param _v - State of XTT tip, true - active, false - inactive.
     /// \return No return value.
     void setShowXTTtip(bool _v) { fShowXTTtip = _v; }
     
     /// \brief Handle event of dragging file on window area and check it data tape.
     ///
     /// \param event Pointer to parameters of dragging data on form.
     /// \return No return value.
     void dragEnterEvent(QDragEnterEvent* event);
     
     /// \brief Handle event of dropping file on window area and check it data tape.
     ///
     /// \param event - Pointer to parameters of dropping data on form.
     /// \return No return value.
     void dropEvent(QDropEvent* event);
     
     /// \brief Function that registers plugin in main window
     ///
     /// \param __pluginPtr - pointer to the plugin
     /// \return no values return
     void registerPlugin(devPlugin* __pluginPtr);
     
     /// \brief Function that collects information about plugins
     ///
     /// \return no values return
     void getPlugins(void);
     
     /// \brief Function that returns a pointer to one of the main window menus
     ///
     /// \param __menuIndex - index of menu
     /// \return pointer to one of the main window menus
     QMenu* getMenu(int __menuIndex);
     
public slots:

     /// \brief Function that updates the windows title
     ///
     /// \return No return value.
     void updateWindowTitle(void);

     /// \brief Function trggered when timer interval has been changed
     ///
     /// \return No return value.
     void xttRunModelMessagesDockWindowTimerIntervalChanged(int __newInterval);

     /// \brief Open type manager
     ///
     /// \return No return value.
     void typeManagerShow(void);

     /// \brief Performs XTT+ model generation based on ARD+ model
     ///
     /// \return No return value.
     void generateXTT(void);
     
     /// \brief Performs PROLOG code generation
     ///
     /// \return No return value.
     void generatePROLOG(void);

     /// \brief Refreshes the content of the model scenes
     ///
     /// \return no values return
     void updateModelScenes(void);

     /// \brief Function executes when window is showing.
     ///
     /// \param event - Event parameters.
     /// \return No return value.
     void showEvent(QShowEvent* event);

     /// \brief Exectue mode.
     ///
     /// \return No return value.
     void ExecuteModel(void);

     /// \brief Open ARD file dialog.
     ///
     /// \return No return value.
     void openARDFileDialog(void);
     
     /// \brief Open XTT file dialog.
     ///
     /// \return No return value.
     void openXTTFileDialog(void);
     
     /// \brief Open file.
     ///
     /// \param _fileName - File name to open.
     /// \return No return value.
     void openFile(QString _fileName);
     
     /// \brief Open recent file.
     ///
     /// \param action - Action associated with menu entry of file to open.
     /// \return No return value.     
     void openRecentClick(QAction* action);
     
     /// \brief Clear recent files list.
     ///
     /// \return No return value.
     void clearRecentList(void);
     
     /// \brief Save file as.
     ///
     /// \param version - Filter file version.
     /// \return No return value.
     void saveAs(int version);
     
     /// \brief Save changes.
     ///
     /// \return No return value.
     void save(void);
     
     /// \brief Save all changes in all models
     ///
     /// \return No return value.
     void saveAll(void);
     
     /// \brief Executes action of saving file to hml version 2.0.
     ///
     /// \return No return value.
     void saveAs_HML_2_0(void);
     
     /// \brief Save file to format xttml 2.0.
     ///
     /// \param __fileName - the destination file name
     /// \return No return value.
     void saveTo_HML_2_0(QString __fileName);

     /// \brief Save file to Drools 5.0 format
     ///
     /// \param __fileName - the destination file name
     /// \return No return value.
     void saveTo_Drools_5_0(QString __fileName);
     
     /// \brief Export attributes to HML 2.0 file.
     ///
     /// \return No return value.
     void exportAttributesToHML20file(void);

     /// \brief Import attributes from HML 2.0 file.
     ///
     /// \return No return value.
     void importAttributesFromHML20file(void);
     
     /// \brief Export scene to svg file.
     ///
     /// \return No return value.
     void exportSceneToSVGfile(void);
     
     /// \brief Export scene to bmp file.
     ///
     /// \return No return value.
     void exportSceneToBMPfile(void);
     
     /// \brief Export scene to jpg file.
     ///
     /// \return No return value.
     void exportSceneToJPGfile(void);
     
     /// \brief Export scene to png file.
     ///
     /// \return No return value.
     void exportSceneToPNGfile(void);
     
     /// \brief Export scene to ppm file.
     ///
     /// \return No return value.
     void exportSceneToPPMfile(void);
     
     /// \brief Export scene to xbm file.
     ///
     /// \return No return value.
     void exportSceneToXBMfile(void);
     
     /// \brief Export scene to xpm file.
     ///
     /// \return No return value.
     void exportSceneToXPMfile(void);
     
     /// \brief Export scene as a picture.
     ///
     /// \param _format - File format.
     /// \return No return value.
     void exportSceneAsPicture(QString _format);

     /// \brief Export tph as picture.
     ///
     /// \param _format - File format.
     /// \return No return value.
     void exportTPHasPicture(QString _format);
     
     /// \brief Export tph scene to svg file.
     ///
     /// \return No return value.
     void exportTPHtoSVGfile(void);
     
     /// \brief Export tph scene to bmp file.
     ///
     /// \return No return value.
     void exportTPHtoBMPfile(void);
     
     /// \brief Export tph scene to jpg file.
     ///
     /// \return No return value.
     void exportTPHtoJPGfile(void);
     
     /// \brief Export tph scene to png file.
     ///
     /// \return No return value.
     void exportTPHtoPNGfile(void);
     
     /// \brief Export tph scene to ppm file.
     ///
     /// \return No return value.
     void exportTPHtoPPMfile(void);
     
     /// \brief Export tph scene to xbm file.
     ///
     /// \return No return value.
     void exportTPHtoXBMfile(void);
     
     /// \brief Export tph scene to xpm file.
     ///
     /// \return No return value.
     void exportTPHtoXPMfile(void);
     
     /// \brief Export tph as picture.
     ///
     /// \param _format - File format.
     /// \return No return value.
     void exportSTSasPicture(QString _format);
     
     /// \brief Export tph scene to svg file.
     ///
     /// \return No return value.
     void exportSTStoSVGfile(void);
     
     /// \brief Export tph scene to bmp file.
     ///
     /// \return No return value.
     void exportSTStoBMPfile(void);
     
     /// \brief Export tph scene to jpg file.
     ///
     /// \return No return value.
     void exportSTStoJPGfile(void);
     
     /// \brief Export tph scene to png file.
     ///
     /// \return No return value.
     void exportSTStoPNGfile(void);
     
     /// \brief Export tph scene to ppm file.
     ///
     /// \return No return value.
     void exportSTStoPPMfile(void);
     
     /// \brief Export tph scene to xbm file.
     ///
     /// \return No return value.
     void exportSTStoXBMfile(void);
     
     /// \brief Export tph scene to xpm file.
     ///
     /// \return No return value.
     void exportSTStoXPMfile(void);

     /// \brief Function that is called when XTTML 2.0 file has been loaded
     ///
     /// \param __sourceName - Reference to section with file contants.
     /// \return No return value.
     void onLoad_XTTML_2_0_file(QString __sourceName);
     
     /// \brief Function that is called when ARDML 1.0 file has been loaded
     ///
     /// \param __sourceName - the name of the source
     /// \return No return value.
     void onLoad_ARDML_1_0_file(QString __sourceName);
     
     /// \brief Function that is called when HML 1.0 file has been loaded
     ///
     /// \param __sourceName - Reference to section with file contants.
     /// \return No return value.
     void onLoad_HML_1_0_file(QString __sourceName);
     
     /// \brief Function that is called when HML 2.0 file has been loaded
     ///
     /// \param __sourceName - Reference to section with file contants.
     /// \param __msgs - the reading messages.
     /// \return No return value.
     void onLoad_HML_2_0_file(QString __sourceName, QString __msgs);

     /// \brief Function that is called when HML 2.0 model has been loaded from protocol message
     ///
     /// \return No return value.
     void onLoad_HML_2_0_server(void);

     /// \brief Create new project with the same attributes.
     /// 
     /// \return No return value.
     void NewWhithOldAttributiesClick(void);

     /// \brief Create new Ard project.
     ///
     /// \return No return value.
     void NewArdClick(void);

     /// \brief Create new XTT project.
     ///
     /// \return No return value.
     void NewXttClick(void);
     
     /// \brief Create new XTT project with wizard.
     ///
     /// \return No return value.
     void NewXttWithWizardClick(void);

     /// \brief Removes all the data from ARD model
     ///
     /// \return No return value.
     void clearARDmodel(void);

     /// \brief Removes all the data from XTT model
     ///
     /// \return No return value.
     void clearXTTmodel(void);

     /// \brief Removes all the data from ARD and XTT model
     ///
     /// \return No return value.
     void clearBothNModels(void);

     /// \brief Closes current model
     ///
     /// \return No return value.
     void closeModel(void);

     /// \brief Starts the hqed server
     ///
     /// \return No return value.
     void hqedServerStart(void);

     /// \brief Stops the hqed server
     ///
     /// \return No return value.
     void hqedServerStop(void);

     /// \brief Closes all models
     ///
     /// \param __confirm - determines confirmation of the action
     /// \return No return value.
     void closeAllModels(bool __confirm = true);

     /// \brief Closes all models except current
     ///
     /// \return No return value.
     void closeAllModelsExceptCurrent(void);
     
     /// \brief Switch application in ARD mode.
     ///
     /// \return No return value.
     void useARDmode(void);
     
     /// \brief Switch application in XTT mode.
     ///
     /// \return No return value.
     void useXTTmode(void);

     /// \brief Switch color of model (colour, black and white).
     ///
     /// \return No return value.
     void SwitchColorMode(void);

     /// \brief Show window where you can add new attribute.
     ///
     /// \return No return value.
     void AddNewAttribute(void);

     /// \brief Show window where you can edit attribute.
     ///
     /// \return No return value.
     void EditAttribute(void);

     /// \brief Show window where you can delete attribute.
     ///
     /// \return No return value.
     void DeleteAttribute(void);

     /// \brief Show group menager window.
     ///
     /// \return No return value.
     void GroupManagerShow(void);

     /// \brief Show the ARD attribute menager window.
     ///
     /// \return No return value.
     void ardAttributeManagerShow(void);

     /// \brief Show the XTT attribute menager window.
     ///
     /// \return No return value.
     void xttAttributeManagerShow(void);

     /// \brief Show table editor window.
     ///
     /// \return No return value.
     void TableEditorShow(void);

     /// \brief Show table editor window in edit mode.
     ///
     /// \return No return value.
     void TableEditorEdit(void);

     /// \brief Show delete table window.
     ///
     /// \return No return value.
     void DeleteTableShow(void);

     /// \brief Show cell editor window.
     ///
     /// \return No return value.
     void ShowCellEditor(void);

     /// \brief Show connetion editor window.
     ///
     /// \return No return value.
     void ShowConnEditor(void);

     /// \brief Show connetion editor window in edit mode.
     ///
     /// \return No return value.
     void ShowConnEditorInEditMode(void);

     /// \brief Show delete connetion window.
     ///
     /// \return No return value.
     void ShowConnectionDeleteWindow(void);

     /// \brief Show connetion menager window.
     ///
     /// \return No return value.
     void ShowConnectionManager(void);

     /// \brief Update render hints.
     ///
     /// \return No return value.
     void UpdateRenderHints(void);

     /// \brief Change antialiasing status.
     ///
     /// \return No return value.
     void ChangeAntialiasingStatus(void);

     /// \brief Close application.
     ///
     /// \return No return value.
     void CloseForm(void);
     
     /// \brief Print model.
     ///
     /// \return No return value.
     void printModel(void);
     
     /// \brief Show print setup window.
     ///
     /// \return No return value.
     void printSetup(void);

     /// \brief Show list of unused attributes.
     ///
     /// \return No return value.
     void ShowUnusedAttributes(void);

     /// \brief Show program information.
     ///
     /// \return No return value.
     void ShowAboutWin(void);
     
     /// \brief Set color mode.
     ///
     /// \param isBW - Determines whether black and white model should be visible.
     /// \return No return value.
     void setColorMode(bool isBW);
     
     /// \brief Set antialiasing status.
     ///
     /// \param isON - Determines whether antiasliasing in on.
     /// \return No return value.
     void setAntialiasingStatus(bool isON);
     
     /// \brief Set antialiasing status for text.
     ///
     /// \param isON - Determines whether antiasliasing in on.
     /// \return No return value.
     void setTextAntialiasingStatus(bool isON);
     
     /// \brief Set smooth pixel map tranformation status.
     ///
     /// \param isON - Determines whether smooth pixel map tranformation status is active.
     /// \return No return value.
     void setSmoothPixmapTransformStatus(bool isON);
     
     /// \brief Delete connection.
     ///
     /// \param cID Id connection.
     /// \return No return value.
     void deleteConnection(QString cID);
     
     /// \brief Delete table.
     ///
     /// \param tID - Table id.
     /// \return No return value.
     void deleteTable(QString tID);
     
     /// \brief Delete selected connection.
     ///
     /// \return No return value.
     void deleteSelectedConnection(void);
     
     /// \brief Delete selected table.
     ///
     /// \return No return value.
     void deleteSelectedTable(void);
     
     /// \brief Show configuration dialog.
     ///
     /// \return No return value.
     void showConfigurationDialog(void);
     
     /// \brief Performs tables autoarrangment procedure
     ///
     /// \return No return value.
     void tablesArrangment(void);
     
     /// \brief Performs  autoarrangment procedure
     ///
     /// \return No return value.
     void statesArrangment(void);
     
     /// \brief Function that shows hqedTextEditor
     ///
     /// \return No return value.
     void hqedTextEditor(void);
     
     /// \brief Calls for plugins' event listener window dialog
     ///
     /// \return No return value.
     void onPluginsEventListener(void);
     
     /// \brief Calls for StateSchemaEditor
     ///
     /// \return No return value.
     void onAddNewStateGroup(void);

     /// \brief Show wizard dialog
     ///
     /// \return No return value.
     void showWizardDialog(void);
     
     /// \brief Show startup dialog
     ///
     /// \return No return value.
     void showStartupDialog(void);
};
// -----------------------------------------------------------------------------
extern MainWin_ui* MainWin;     ///< Main window.

#endif
