/**
* \file	hqedserver.cpp
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	13.06.2007
* \version 1.0
* \brief	This file defines class that represents the hqedServer object
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include <QTcpSocket>
#include <QMap>
// -----------------------------------------------------------------------------

#include "../namespaces/ns_hqed.h"
#include "../namespaces/ns_xtt.h"
#include "../M/hModelList.h"
#include "../M/hModel.h"
#include "../V/vasXML.h"
#include "../V/vasProlog.h"
#include "MainWin_ui.h"

#include "XTT/xttSimulationParams.h"
#include "XTT/XTT_AttributeGroups.h"
#include "../M/hMultipleValue.h"
#include "../M/hValue.h"
#include "../M/hSet.h"
#include "../M/hSetItem.h"
#include "../M/XTT/XTT_Attribute.h"
#include "../M/XTT/XTT_State.h"
#include "../M/XTT/XTT_States.h"
#include "../M/XTT/XTT_Statesgroup.h"
#include "../M/XTT/XTT_Row.h"
#include "devPluginInterface/devPluginController.h"

#include "devPluginInterface/devIODeviceSingalHandler.h"
#include "hqedserver.h"
// -----------------------------------------------------------------------------

hqedServer* hqedServerInstance;
// -----------------------------------------------------------------------------

// #brief Default class constructor
hqedServer::hqedServer(void) : devIODeviceSingalHandler("server")
{
    _server = new QTcpServer(NULL);
    setName("hqedServer");
    connect(_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
}
// -----------------------------------------------------------------------------

// #brief Class destructor
hqedServer::~hqedServer(void)
{
    stop();
    delete _server;
}
// -----------------------------------------------------------------------------

// #brief Returns the status of the server
//
// #return true if the server is listening, otherwise false
bool hqedServer::isListening(void)
{
    return _server->isListening();
}
// -----------------------------------------------------------------------------

// #brief Returns the port number on which the serwer listen
//
// #return the port number on which the serwer listen
quint16 hqedServer::port(void)
{
    return _server->serverPort();
}
// -----------------------------------------------------------------------------

// #brief Starts the server listening on the given port. When the server is already running it changes its port
//
// #param __port - number of the port to listen on
// #return no values return
void hqedServer::listen(int __port)
{
    stop();
    createEvent(2, "Starting " + name() + "...");
    if(!_server->listen(QHostAddress::LocalHost, __port))
    {
        createEvent(2, "Server starting... failed!");
        return;
    }
    createEvent(2, "Listen on port " + QString::number(__port));
}
// -----------------------------------------------------------------------------

// #brief Stops the server
//
// #return no values return
void hqedServer::stop(void)
{
    createEvent(2, "Stopping " + name() + "...");
    if(_server->isListening())
        _server->close();
}
// -----------------------------------------------------------------------------

// #brief Slot called when new connection incomes
void hqedServer::onNewConnection(void)
{
    QString str_readdata;

    QTcpSocket* newcon = _server->nextPendingConnection();
    connect(newcon, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error2String(QAbstractSocket::SocketError)));
    connect(newcon, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(state2String(QAbstractSocket::SocketState)));

    createEvent(2, "Received new connection from " + newcon->localAddress().toString());
    if(newcon->waitForReadyRead(5000))
    {
        QByteArray ba_readdatal = newcon->readAll();
        str_readdata = QString(ba_readdatal);
        createEvent(2, "Received message: " + str_readdata);

        int rcode;
        QString response = interpretMessage(str_readdata, rcode);
        if(rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
            response = createErrorResponce("Internal server error.");

        createEvent(2, "Sending response to client: " + response);
        newcon->write(response.toAscii());
        createEvent(2, "Response sent.");

        //newcon->close();
        //delete newcon;
        return;
    }
    createEvent(1, "Connection timeout.");
}
// -----------------------------------------------------------------------------

// #brief Function that parses the message specified with the help of __message.
//
// #param __message - the message that will be parsed
// #param rcode - result code of the function
// #param rmsg - result message
// #return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well
QStringList hqedServer::parseMessage(QString __message, int& rcode, QString& rmsg)
{
    QStringList result = hqed::parseProtocolV1Message(__message, rcode, rmsg);
    if(rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
    {
        if(rcode == PROTOCOL_PARSING_RESULT_ERROR_FORMAT)
            rcode = setLastError(rmsg, HQED_SERVER_MESSAGE_TYPE_FORMAT);
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that interprets received message
//
// #param __messgae - message to interpretation
// #param __rcode - result code
// #return the message that should be sent to client as response
QString hqedServer::interpretMessage(QString __message, int& __rcode)
{
    QString rmsg;
    QStringList messageItems = parseMessage(__message, __rcode, rmsg);

    if(__rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
    {
        __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
        createEvent(1, "Incorrect of received message. Error message: " + rmsg);
        return createErrorResponce(rmsg);
    }

    if(messageItems.size() < 1)
    {
        QString m = "Incorrect format of received message. The message must contain at least one element.";
        __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
        createEvent(1, m + "Error message: " + rmsg);
        return createErrorResponce(m);
    }

    // beggining message parsing
    QString firstPart = messageItems.at(0).toLower();

    // interpreting message

    // when hello message
    if(firstPart == "hello")
    {
        QString response = interpretMessageHello(messageItems, __rcode);
        if(__rcode == PROTOCOL_PARSING_RESULT_SUCCESS)
            return response;
        if(__rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
            return "";
    }

    // when model
    if(firstPart == "model")
    {
        QString secondPart = "", response;
        if(messageItems.size() > 1)
            secondPart = messageItems.at(1).toLower();

        if(secondPart == "add") {
            response = interpretMessageModelAdd(messageItems, __rcode);
        } else if(secondPart == "get") {
             response = interpretMessageModelGet(messageItems, __rcode);
        } else if(secondPart == "getlist") {
             response = interpretMessageModelGetList(messageItems, __rcode);
        } else if(secondPart == "exists") {
             response = interpretMessageModelExists(messageItems, __rcode);
        } else if(secondPart == "run") {
             response = interpretMessageModelRun(messageItems, __rcode);
        } else if(secondPart == "remove") {
             response = interpretMessageModelRemove(messageItems, __rcode);
        } else {
            createEvent(1, "Unrecognized message: " + __message);
            __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
            return createErrorResponce("Unrecognized message.");
        }

        if(__rcode == PROTOCOL_PARSING_RESULT_SUCCESS)
            return response;
        if(__rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
            return "";
    }

    // when state
	if(firstPart == "state")
    {
            QString secondPart = "", response;
            if(messageItems.size() > 1)
                secondPart = messageItems.at(1).toLower();

            if(secondPart == "add") {
                response = interpretMessageStateAdd(messageItems, __rcode);
            } else if(secondPart == "remove") {
                response = interpretMessageStateRemove(messageItems, __rcode);
            } else {
                createEvent(1, "Unrecognized message: " + __message);
                __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
                return createErrorResponce("Unrecognized message.");
            }

            if(__rcode == PROTOCOL_PARSING_RESULT_SUCCESS)
                return response;
            if(__rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
                return "";
        }

    // When unrecognized message
    createEvent(1, "Unrecognized message: " + __message);
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
    return createErrorResponce("Unrecognized message.");
}
// -----------------------------------------------------------------------------

// #brief Function that interprets the helle message
//
// #param messageItems - the list of message elements
// #param __rcode - result code
// #return the message that should be sent to client as response
QString hqedServer::interpretMessageHello(QStringList messageItems, int& __rcode)
{
    // Message format: [hello, +client_name]

    createEvent(2, "Interpreting Hello message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
    if(messageItems.size() != 2)
    {
        QString msg = "Incorrect format of hello message. It must contain two elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString responce = createList(QStringList() << interpretationSuccess() << createList(QStringList() << hqed::hqedVersion().toLower() << "hello" << "1.0" << "12" << createList(QStringList())));

    createEvent(2, "Interpreting Hello message success!");
    return responce;
}
// -----------------------------------------------------------------------------

// #brief Function that interprets the model getlist message
//
// #param messageItems - the list of message elements
// #param __rcode - result code
// #return the message that should be sent to client as response
QString hqedServer::interpretMessageModelGetList(QStringList messageItems, int& __rcode)
{
    // Message format: [model, getlist]

    createEvent(2, "Interpreting ModelGetList message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;
    if(messageItems.size() != 2)
    {
        QString msg = "Incorrect format of ModelGetList message. It must contain two elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString responce = "";
    QRegExp filter = QRegExp("^" + modelIdPrefix() + ".*");
    QList<hModel*> modellist = hqed::modelList()->filter(filter);
    for(int i=0;i<modellist.size();++i)
    {
        QString mid = modellist.at(i)->modelID();
        if(mid.startsWith(modelIdPrefix()))
            mid.replace(0, modelIdPrefix().length(), "");

        QStringList idparts = mid.split(modelIdInfix());
        if(idparts.size() == 2)
            responce += createList(idparts) + ",";
    }
    if(responce.endsWith(","))
        responce.remove(responce.length()-1, 1);
    responce = createList(QStringList() << responce);

    createEvent(2, "Interpreting ModelGetList message success!");
    return createList(QStringList() << interpretationSuccess() << responce);
}
// -----------------------------------------------------------------------------

static bool isStringInList(const QString& string, const QList<QString>& list)
{
   foreach (QString listString, list) {
      if (QString::compare(string, listString, Qt::CaseInsensitive) == 0)
        return true;
   }
   return false;
}

extern devPluginController* devPC;
extern XTT_States* States;

QString hqedServer::interpretMessageModelRun(QStringList messageItems, int& __rcode)
{
    /* [model, run, +modelname, +username, +{ddi | gdi | tdi | foi}, +tables,
     * +{statename | statedef} ].
     */
    createEvent(2, "Interpreting ModelRun message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    if (messageItems.size() != 7)
    {
        QString msg("Incorrect format of ModelExists message. It must contain seven elements.");
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString modelname = messageItems.at(2);
    QString username = messageItems.at(3);
    QString simulationType = messageItems.at(4);
    QString tables = messageItems.at(5);
    QString state = messageItems.at(6);

    /*
     * Check simulation type
     */
    if (simulationType != "tdi") // only tdi is supported
    {
        QString msg = QString("Unexpected value of fifth argument: %1.").arg(simulationType);
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    /*
     * Check if model exists.
     */
    QString modelId = createModelId(modelname, username);
    hModelList *globalList = hqed::modelList();
    int modelIndex = globalList->indexOfID(modelId);
    if (modelIndex == -1) {
        QString msg
            = QString("The requested model does not exists:"
                      " modelname: %1, username: %2").arg(modelname, username);
        createEvent(1, msg);
        return createErrorResponce(msg);
    }
    globalList->activateModel(modelIndex);

    /*
     * Create or load state
     */
    XTT_State* xttState = NULL;
    XTT_Statesgroup *statesGroup = NULL;
    bool useNewState = state.startsWith("[");
    if (useNewState) // use new state
    {
        // Parse state definition
        QString stateDef = state;
        int stateDefParsingResultCode = HQED_SERVER_RESULT_CODE_SUCCESS;
        QString stateDefParsingResultMessage = "";
        QMap<QString, QString> stateDefMap = parseStateDefCommand(stateDef, stateDefParsingResultCode, stateDefParsingResultMessage);
        if(stateDefParsingResultCode != HQED_SERVER_RESULT_CODE_SUCCESS)
        {
            QString msg = "Error during parsing state definition: " + stateDefParsingResultMessage + ".";
            createEvent(1, msg);
            return createErrorResponce(msg);
        }

        // create temporary state
        QString stateName = "tempState";        
        XTT_States *globalStates = States;
        if(globalStates->contains(stateName)){
            XTT_Statesgroup* stsgrp = (*globalStates)[stateName];
            stsgrp->clear();
        }

        statesGroup = new XTT_Statesgroup(globalStates, false);
        XTT_State *newState = statesGroup->createdefaultState();

        QList<QStringList> attributesList;

        for(QMap<QString, QString>::const_iterator it=stateDefMap.constBegin();
                it!=stateDefMap.constEnd(); it++){
            QString ik=it.key();
            QString iv=it.value();
            QStringList newList;
            newList.append(ik);
            newList.append(iv);
            attributesList.append(newList);
        }

        AttributeGroups->setModelState(&attributesList, newState);

        xttState = newState;
    }
    else // use existing state
    {
        QStringList splittedState = state.split("/");
        QString stateName = splittedState.at(0);
        /* get state */
        if(!States->contains(stateName))
        {
            QString msg = QString("Failed to start simulation,"
                                  " missing state '%1'.").arg(stateName);
            createEvent(1, msg);
            return createErrorResponce(msg);
        }
        XTT_Statesgroup* stsgrp = (*States)[stateName];
        int stateIndex = 0;
        if (splittedState.size() == 2)
        {
            stateIndex = splittedState.at(1).toInt() - 1; // indexes start from 0
        }

        xttState = stsgrp->stateAt(stateIndex);
        xttState->loadState();
    }

    /*
     * Run simulation
     */
    xtt::run(xttState);

    /*
     * Create system state string
     */
    QString stateString = "";
    QStringList statesList;
    QList<XTT_Attribute*> *attrs = globalList->activeModel()->xttAttributeGroups()->Attributes();
    int attrssize = attrs->size();
    for (int i = 0; i < attrssize; i++)
    {
        XTT_Attribute *attr = attrs->at(i);
        if(attr->Changed() == false)
            continue;
        QString path = attr->Path();
        QString toprolog = attr->toProlog();
        QString attrName = attr->Name();
        QString val = attr->Value()->valueField()->at(0)->toString();
        QStringList nameValPair;
        nameValPair.append(attrName);
        nameValPair.append(val);
        statesList.append(createList(nameValPair));
    }
    stateString = createList(statesList);

    /*
     *  create trajectory string
     */
    QList<XTT_Row*> *trajectory = xttState->trajectory();
    QStringList trajectoryList;
    int trajsize = trajectory->size();
    for (int i = 0; i < trajsize; i++)
    {
        XTT_Row *row = trajectory->at(i);
        int index = row->RowIndex() + 1;
        QString labelTitle = row->ParentPtr()->Title();
        trajectoryList.append(labelTitle);
        trajectoryList.append(QString::number(index));
    }
    QString trajectoryString = createList(trajectoryList);

    States->remove("tempState");

    /*
     * Return results.
     */
    QStringList result;
    result.append("true");
    result.append(stateString);
    result.append(trajectoryString);
    return createList(result);
}

static bool checkVerificationType(QString type)
{
   QStringList allowedTypes;
   allowedTypes << "vcomplete" << "vcontradict" << "vsubsume" << "vreduce";
   return isStringInList(type, allowedTypes);
}

QString hqedServer::interpretMessageModelExists(QStringList messageItems, int& __rcode)
{
    createEvent(2, "Interpreting ModelExists message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    if(messageItems.size() != 4)
    {
        QString msg = "Incorrect format of ModelExists message. It must contain four elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString modelname = messageItems.at(2);
    QString username = messageItems.at(3);

    QString modelId = createModelId(modelname, username);

    QStringList result;

    int modelIdx = hqed::modelList()->indexOfID(modelId);
    if(modelIdx == -1) {
        result << interpretationSuccess() << interpretationFailed();
    } else {
        result << interpretationSuccess() << interpretationSuccess();
    }

    createEvent(2, "Interpreting ModelExists message success!");
    return createList(result);
}

QString hqedServer::interpretMessageModelRemove(QStringList messageItems, int& __rcode)
{
    /* [model,remove, +modelname, +username]. - removes a model specified by
     * modelname and username. After successful deletion fo the model
     *
     *     [true]
     *
     * is returned. In case of an error, following error message is sent back
     * to the client
     * 
     *     [false,'Error while deleting model.']
     */
    createEvent(2, "Interpreting ModelRemove message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    if (messageItems.size() != 4) {
        QString msg
            = "Incorrect format of \"model remove\" message. It must contain 4 elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString modelname = messageItems.at(2);
    QString username  = messageItems.at(3);

    QString modelId = createModelId(modelname, username);
    hModelList *globalList = hqed::modelList();
    int modelIndex = globalList->indexOfID(modelId);
    if (modelIndex == -1) {
        QString msg = QString("The requested model does not exists:"
                              " modelname: %1, username: %2").arg(modelname, username);
        createEvent(1, msg);
        return createErrorResponce(msg);
    }
    /*  apparently funcion hModelList::removeModel(int index) does nothing if
     *  index is active model index, so it seems that checking that before hand
     *  and turining it into an error is a good idea */
    if (modelIndex == globalList->activeModelIndex()) {
        QString msg
            = QString("The requested model is active and could not be deleted"
                      ": modelname: %1, username: %2").arg(modelname, username);
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    bool result = globalList->tryRemoveModel(modelIndex);
    if (result == false) {
        QString msg
            = QString("Failed to delete requested model:"
                      " modelname: %1, username: %2").arg(modelname, username);
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    return createList(QStringList() << interpretationSuccess());
}

// #brief Function that interprets the model add message
//
// #param messageItems - the list of message elements
// #param __rcode - result code
// #return the message that should be sent to client as response
QString hqedServer::interpretMessageModelAdd(QStringList messageItems, int& __rcode)
{
    // Message format: [model, add, +format, +modelname, +username, +'MODEL']

    createEvent(2, "Interpreting ModelAdd message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    if(messageItems.size() != 6)
    {
        QString msg = "Incorrect format of \"model add\" message. It must contain six elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QStringList allowedFormat = QStringList() << "hmr" << "hml";
    QString format = messageItems.at(2),
            modelname = messageItems.at(3),
            username = messageItems.at(4),
            modelcontent = messageItems.at(5);
    createEvent(2, "Model format: " + format);
    createEvent(2, "Model username: " + username);
    createEvent(2, "Model modelname: " + modelname);
    createEvent(2, "Model content: " + modelcontent);

    if(allowedFormat.indexOf(format) == -1)
    {
        QString msg = "Incorrect format definition \"" + format + "\" in \"model add\" message. The allowed formats are: " + allowedFormat.join(",") + ".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    hModel* amodel = NULL;
    QString modelId = createModelId(modelname, username);
    int modelIdx = hqed::modelList()->indexOfID(modelId);
    if(modelIdx == -1)
    {
        hqed::modelList()->createNewModel(modelId, false);
        modelIdx = hqed::modelList()->size()-1;
        hqed::modelList()->registerModel(modelIdx);
    }
    hqed::modelList()->activateModel(modelIdx);       // activating model
    amodel = hqed::modelList()->activeModel();
    if(amodel == NULL)
    {
        QString msg = "Incorrect model index.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    amodel->flush();                                  // clearing model

    // reading model
    int parts = VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS | VAS_XML_MODEL_PARTS_XTT_TABLS | VAS_XML_MODEL_PARTS_XTT_STATS;
    QBuffer buf;
    QString msg ,frmt;
    buf.setData(modelcontent.toAscii());

    int rcode = vas_in_XML(&buf, msg, frmt, parts);

    if(rcode == VAS_XML_UNKNOWN_DEVICE)
        return createErrorResponce("Loading error: device is not specified.");
    if(rcode == VAS_XML_DEVICE_NOT_READABLE)
        return createErrorResponce("Loading error: device is not readable.");
    if(rcode == VAS_XML_FORMAT_ERROR)
        return createErrorResponce("Loading error: " + hqed::retrieveMessageParts(msg));
    if(rcode == VAS_XML_FORMAT_NOT_SUPPORTED)
        return createErrorResponce("Loading error: the file format is not supported.");
    if(rcode == VAS_XML_VERSION_NOT_SUPPORTED)
        return createErrorResponce("Loading error: the xml version is not supported or not specified.");
    if(rcode == VAS_XML_XTT_SYNTAX_ERROR)
        return createErrorResponce(msg);
    if(rcode == VAS_XML_NOTHING_DONE)
        return createErrorResponce("Reading error: nothing has been done.");

    MainWin->onLoad_HML_2_0_server();

    createEvent(2, "Interpreting ModelAdd message success!");
    return createList(QStringList() << interpretationSuccess());
}
// -----------------------------------------------------------------------------

// #brief Function that interprets the model get message
//
// #param messageItems - the list of message elements
// #param __rcode - result code
// #return the message that should be sent to client as response
QString hqedServer::interpretMessageModelGet(QStringList messageItems, int& __rcode)
{
    // Message format: [model, get, +format, +modelname, +username, +[+parts]]

    // Cheking the number of message elements
    createEvent(2, "Interpreting ModelGet message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    if(messageItems.size() != 6)
    {
        QString msg = "Incorrect format of \"model get\" message. It must contain six elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    // Collecting data...
    QString format = messageItems.at(2),
            modelname = messageItems.at(3),
            username = messageItems.at(4),
            parts = messageItems.at(5);

    QString modelId = createModelId(modelname, username);
    QStringList allowedFormats = QStringList() << "hml" << "hmr";

    // Checking format corectness
    if(allowedFormats.indexOf(format) == -1)
    {
        QString msg = "Incorrect format definition \"" + format + "\" in \"model get\" message. The allowed formats are: " + allowedFormats.join(",") + ".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    // Checking if the model exists
    int modelIdx = hqed::modelList()->indexOfID(modelId);
    if(modelIdx == -1)
    {
        QString msg = "The requested model does not exist: modelname: \"" + modelname + "\", username: \"" + username + "\".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    // Parsing parts comamnd
    int partsParsingResultCode = HQED_SERVER_RESULT_CODE_SUCCESS;
    QString partsParsingResultMessage = "";
    QMap<QString, QString> partsMap = parsePartsCommand(parts, partsParsingResultCode, partsParsingResultMessage);

    if(partsParsingResultCode != HQED_SERVER_RESULT_CODE_SUCCESS)
    {
        QString msg = "Error while interpreting \"model get\" message during parsing model parts for retrival: " + partsParsingResultMessage + ".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    /// Now we can retrieve required parts of the model in required format
    int retrivalResultCode = VAS_XML_SUCCES;
    QString retrivalResultMessage = "";
    QString retrivalModelContent = "";
    QBuffer* buf = new QBuffer;

    buf->open(QIODevice::ReadWrite);
    if(format == "hml")
        retrivalResultCode = vas_out_HML_2_0(buf, false, partsMap, retrivalResultMessage);
    else if(format == "hmr")
        retrivalResultCode = vas_out_SWI_Prolog(buf, retrivalResultMessage, partsMap, "", "", true);
    buf->close();

    retrivalModelContent = "\'" + QString(buf->data()) + "\'";
    delete buf;

    // When error occurs during model parts retrival
    if(retrivalResultCode != VAS_XML_SUCCES)
    {
        QString msg = "Error while interpreting \"model get\" message during retrieving model partsl: " + retrivalResultMessage + ".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    // When success
    createEvent(2, "Interpreting ModelGet message success!");
    return createList(QStringList() << interpretationSuccess() << retrivalModelContent);
}

/// \brief Function that interprets the state add message
///
/// \param messageItems - the list of message elements
/// \param __rcode - result code
/// \return the message that should be sent to client as response
QString hqedServer::interpretMessageStateAdd(QStringList messageItems, int& __rcode)
{
    // Message format: [state, add ,+modelname, +username, +statename, +statedef]

    createEvent(2, "Interpreting StateAdd message...");
    __rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

    /**
     * 1. Get model
     * 2. Parse stateDef
     * 3. Inject new state
     */

    /**
     * 1. Get model
     */

    if(messageItems.size() != 6)
    {
        QString msg = "Incorrect format of \"state add\" message. It must contain six elements.";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    QString modelname = messageItems.at(2),
            username = messageItems.at(3),
            stateName = messageItems.at(4),
            stateDef = messageItems.at(5);

    QString modelId = createModelId(modelname, username);

    int modelIdx = hqed::modelList()->indexOfID(modelId);
    if(modelIdx == -1)
    {
        QString msg = "The requested model does not exist: modelname: \"" + modelname + "\", username: \"" + username + "\".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    hqed::modelList()->activateModel(modelIdx);
	hModel *amodel = hqed::modelList()->activeModel();
	if(amodel == NULL)
	{
		QString msg = "Incorrect model index.";
		createEvent(1, msg);
		return createErrorResponce(msg);
	}

    /*
     * 2. Parse stateDef
     */

    int stateDefParsingResultCode = HQED_SERVER_RESULT_CODE_SUCCESS;
    QString stateDefParsingResultMessage = "";
    QMap<QString, QString> stateDefMap = parseStateDefCommand(stateDef, stateDefParsingResultCode, stateDefParsingResultMessage);

    if(stateDefParsingResultCode != HQED_SERVER_RESULT_CODE_SUCCESS)
    {
        QString msg = "Error while interpreting \"state add\" message during parsing state definition: " + stateDefParsingResultMessage + ".";
        createEvent(1, msg);
        return createErrorResponce(msg);
    }

    /*
     * 3. Inject new state
     */

    XTT_States *states = amodel->xttStates();
	if(states->contains(stateName)){
		XTT_Statesgroup* stsgrp = (*states)[stateName];
		stsgrp->clear();
	}

    XTT_Statesgroup *statesGroup = new XTT_Statesgroup(states, false);
    (*states)[stateName] = statesGroup;
    XTT_State *state=statesGroup->createdefaultState();

    QList<QStringList> attributesList;

    for(QMap<QString, QString>::const_iterator it=stateDefMap.constBegin();
            it!=stateDefMap.constEnd(); it++){
        QString ik=it.key();
        QString iv=it.value();
        QStringList newList;
        newList.append(ik);
        newList.append(iv);
        attributesList.append(newList);
    }

    AttributeGroups->setModelState(&attributesList, state);

    createEvent(2, "Interpreting StateAdd message success!");
    return createList(QStringList() << interpretationSuccess());
}

/// \brief Function that interprets the state remove message
///
/// \param messageItems - the list of message elements
/// \param __rcode - result code
/// \return the message that should be sent to client as response
QString hqedServer::interpretMessageStateRemove(QStringList messageItems, int& __rcode)
{
	// Message format: [state, remove, +modelname, +username, +statename]

	createEvent(2, "Interpreting StateRemove message...");
	__rcode = PROTOCOL_PARSING_RESULT_SUCCESS;

	/**
	 * 1. Get model
	 * 2. Remove state
	 */

	/**
	 * 1. Get model
	 */

	if(messageItems.size() != 5)
	{
		QString msg = "Incorrect format of \"state add\" message. It must contain five elements.";
		createEvent(1, msg);
		return createErrorResponce(msg);
	}

	QString modelname = messageItems.at(2),
			username = messageItems.at(3),
			stateName = messageItems.at(4);

	QString modelId = createModelId(modelname, username);

	int modelIdx = hqed::modelList()->indexOfID(modelId);
	if(modelIdx == -1)
	{
		QString msg = "The requested model does not exist: modelname: \"" + modelname + "\", username: \"" + username + "\".";
		createEvent(1, msg);
		return createErrorResponce(msg);
	}
	hqed::modelList()->activateModel(modelIdx);
	hModel *amodel = hqed::modelList()->activeModel();
	if(amodel == NULL)
	{
		QString msg = "Incorrect model index.";
		createEvent(1, msg);
		return createErrorResponce(msg);
	}


	/*
	 * 2. Remove state
	 */

	XTT_States *states = amodel->xttStates();
	if(states->contains(stateName)){
		XTT_Statesgroup* stsgrp = (*states)[stateName];
		stsgrp->clear();
	}
	amodel->setChanged(true);

	//@TODO Do we need to save anything here?

	createEvent(2, "Interpreting StateRemove message success!");
	return createList(QStringList() << interpretationSuccess());
}

// -----------------------------------------------------------------------------

// #brief Function that parses the map command
//
// #param __parts - the string that provides information about required parts
// #param __rcode - result code
// #param __rmsg - result message
// #return the map that constains the information about parts for retrival. If the error occures the rmsg contains error message.
QMap<QString, QString> hqedServer::parsePartsCommand(QString __parts, int& __rcode, QString& __rmsg)
{
    QMap<QString, QString> result;
    QStringList allowedKeys = QStringList() << "all" << "tpes" << "tpe" << "atrs" << "atr" << "tbls" << "tbl" << "clbs" << "clb";

    __rcode = HQED_SERVER_RESULT_CODE_SUCCESS;

    // 1st level parsing
    QStringList msgparts = parseMessage(__parts, __rcode, __rmsg);
    if(__rcode != HQED_SERVER_RESULT_CODE_SUCCESS)
    {
        __rmsg = "Parsing parts message: " + __rmsg;
        createEvent(1, __rmsg);
        return result;
    }
    if(msgparts.size() == 0)
    {
        __rcode = HQED_SERVER_RESULT_CODE_ERROR;
        __rmsg = "Not defined list of model parts for retrival.";
        createEvent(1, __rmsg);
        return result;
    }

    // Parts parsing
    for(int i=0;i<msgparts.size();++i)
    {
        QStringList singlepartmessage = parseMessage(msgparts.at(i), __rcode, __rmsg);
        if(__rcode != HQED_SERVER_RESULT_CODE_SUCCESS)
        {
            __rmsg = "Parsing " + QString::number(i+1) + ". part definition message: " + __rmsg;
            createEvent(1, __rmsg);
            return result;
        }
        if(singlepartmessage.size() == 0)
        {
            __rcode = HQED_SERVER_RESULT_CODE_ERROR;
            __rmsg = "Incorrect format of " + QString::number(i+1) + ". part definition - too few arguments";
            createEvent(1, __rmsg);
            return result;
        }
        if(singlepartmessage.size() >= 3)
        {
            __rcode = HQED_SERVER_RESULT_CODE_ERROR;
            __rmsg = "Incorrect format of " + QString::number(i+1) + ". part definition - too much arguments";
            createEvent(1, __rmsg);
            return result;
        }

        // If one-element definition
        if(singlepartmessage.size() == 1)
        {
            if(result.keys().contains(singlepartmessage.first()))
                continue;
            result[singlepartmessage.first()] = "";
        }

        // If two-elements definition
        else if(singlepartmessage.size() == 2)
        {
            if(!result.keys().contains(singlepartmessage.at(0)))
                result[singlepartmessage.at(0)] = singlepartmessage.at(1);
            else
                result[singlepartmessage.at(0)] += singlepartmessage.at(1);
        }
    }

    // Checking result consistency

    // Checking if the parsed keys are allowed
    for(int k=0;k<result.keys().size();++k)
        if(!allowedKeys.contains(result.keys().at(k)))
        {
            __rcode = HQED_SERVER_RESULT_CODE_ERROR;
            __rmsg = "Unknown model part \"" + result.keys().at(k) + "\"";
            createEvent(1, __rmsg);
            return result;
        }

    // When all parts of the model are required, then other parts cannot be required simultaneously
    if((result.keys().contains("all")) && (result.keys().count() > 1))
    {
        __rcode = HQED_SERVER_RESULT_CODE_ERROR;
        __rmsg = "Inconsistent list of parts definition: the \"all\" command excludes other parts";
        createEvent(1, __rmsg);
        return result;
    }

    QList<QPair<QString, QString> > inconsistentPairs;
    for(int i=1;i<allowedKeys.size();i+=2)
        inconsistentPairs.append(QPair<QString, QString>(allowedKeys.at(i), allowedKeys.at(i+1)));

    // Checking inconsisency pairs
    for(int i=0;i<inconsistentPairs.size();++i)
    {
        QString f = inconsistentPairs.at(i).first;
        QString s = inconsistentPairs.at(i).second;
        if((result.keys().contains(f)) && (result.keys().contains(s)))
        {
            __rcode = HQED_SERVER_RESULT_CODE_ERROR;
            __rmsg = "Inconsistent list of parts definition: the \"" + f + "\" includes \"" + s + "\" command";
            createEvent(1, __rmsg);
            return result;
        }
    }

    return result;
}

// -----------------------------------------------------------------------------

/// #brief Function that parses the state definition passed by [state, add] command
///
/// #param __stateDef - the string that provides new state definition
/// #param __rcode    - result code
/// #param __rmsg     - result message
/// #return the map that constains the information about parts for retrival. If the error occures the rmsg contains error message.
QMap<QString, QString> hqedServer::parseStateDefCommand(QString __parts, int& __rcode, QString& __rmsg)
{
	QMap<QString, QString> result;

	__rcode = HQED_SERVER_RESULT_CODE_SUCCESS;

	// 1st level parsing
	QStringList parts = parseMessage(__parts, __rcode, __rmsg);
	if(__rcode != HQED_SERVER_RESULT_CODE_SUCCESS)
	{
		__rmsg = "Parsing state definition: " + __rmsg;
		createEvent(1, __rmsg);
		return result;
	}

	// Parts parsing
	for(int i=0;i<parts.size();++i)
	{
		QStringList singlepartmessage = parseMessage(parts.at(i), __rcode, __rmsg);
		if(__rcode != HQED_SERVER_RESULT_CODE_SUCCESS)
		{
			__rmsg = "Parsing " + QString::number(i+1) + ". state definition part: " + __rmsg;
			createEvent(1, __rmsg);
			return result;
		}
		if(singlepartmessage.size() < 2)
		{
			__rcode = HQED_SERVER_RESULT_CODE_ERROR;
			__rmsg = "Incorrect format of " + QString::number(i+1) + ". tate definition part - too few arguments";
			createEvent(1, __rmsg);
			return result;
		}
		if(singlepartmessage.size() > 2)
		{
			__rcode = HQED_SERVER_RESULT_CODE_ERROR;
			__rmsg = "Incorrect format of " + QString::number(i+1) + ". tate definition part - too much arguments";
			createEvent(1, __rmsg);
			return result;
		}
		if(!result.keys().contains(singlepartmessage.at(0)))
			result[singlepartmessage.at(0)] = singlepartmessage.at(1);
		else
			result[singlepartmessage.at(0)] += singlepartmessage.at(1);
	}

	return result;
}
// -----------------------------------------------------------------------------

// #brief Creates the identifier of the model using modelname and username
//
// #param __modelname - the name of the model send by protocol
// #param __username - the name of the user send by protocol
// #return the identifier of the model using modelname and username
QString hqedServer::createModelId(QString __modelname, QString __username)
{
    return modelIdPrefix() + __modelname + modelIdInfix() + __username + modelIdPostfix();
}
// -----------------------------------------------------------------------------

// #brief Returns the prefix of the autogenerated model id
//
// #return the prefix of the autogenerated model id as string
QString hqedServer::modelIdPrefix(void)
{
    return "hqed_server_model_";
}
// -----------------------------------------------------------------------------

// #brief Returns the infix of the autogenerated model id
//
// #return the infix of the autogenerated model id as string
QString hqedServer::modelIdInfix(void)
{
    return "-|-";
}
// -----------------------------------------------------------------------------

// #brief Returns the postfix of the autogenerated model id
//
// #return the postfix of the autogenerated model id as string
QString hqedServer::modelIdPostfix(void)
{
    return "";
}
// -----------------------------------------------------------------------------

// #brief Function that creates the PROLOG list using given argument
//
// #param __list - result code of the function
// #return string that is a PROLOG-formated list
QString hqedServer::createList(QStringList __list)
{
    return "[" + __list.join(",") + "]";
}
// -----------------------------------------------------------------------------

// #brief Function that returns the string protocol message containing error message for the user
//
// #param __error4client - error message for client
// #return protocol message with given error message
QString hqedServer::createErrorResponce(QString __error4client)
{
    __error4client = "\'" + __error4client + "\'";
    return createList(QStringList() << interpretationFailed() << __error4client);
}
// -----------------------------------------------------------------------------

// #brief Allow to set a new last error message
//
// #param __lastError - new error message
// #param __errorType - type of the error. Defined types are: DEV_PLUGIN_MESSAGE_TYPE_CONNECTION, DEV_PLUGIN_MESSAGE_TYPE_FORMAT, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE
// #return error code
int hqedServer::setLastError(QString __lastError, int __errorType)
{
    devIODeviceSingalHandler::setLastError(__lastError, __errorType);
    return HQED_SERVER_RESULT_CODE_ERROR;
}
//---------------------------------------------------------------------------

// #brief Returns the string denoting the success of message parsing
//
// #return the string denoting the success of message parsing
QString hqedServer::interpretationSuccess(void)
{
    return "true";
}
//---------------------------------------------------------------------------

// #brief Returns the string denoting the failed message parsing
//
// #return the string denoting the failed message parsing
QString hqedServer::interpretationFailed(void)
{
    return "false";
}
//---------------------------------------------------------------------------
