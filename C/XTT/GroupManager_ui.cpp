/*
 *	   $Id: GroupManager_ui.cpp,v 1.32 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "GroupManager_ui.h"
#include "GroupEditor_ui.h"
#include "SelectGroup_ui.h"

#include "../../namespaces/ns_hqed.h"

#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
// -----------------------------------------------------------------------------

GroupManager_ui* GroupMan;
// -----------------------------------------------------------------------------

// #brief Constructor GroupManager_ui class.
//
// #param parent Pointer to parent object.
GroupManager_ui::GroupManager_ui(QWidget*)
{
     setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 

     treeWidget->insertTopLevelItems(0, AttributeGroups->CreateTree());

     // signals/slots mechanism in action
     connect(treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(TreeClick(QTreeWidgetItem*, int)));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(CreateNewMainGroup()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(CreateNewSubGroup()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(EditGroup()));
     connect(pushButton_4, SIGNAL(clicked()), this, SLOT(SetAsMain()));
     connect(pushButton_5, SIGNAL(clicked()), this, SLOT(SelectNewParent()));
     connect(pushButton_6, SIGNAL(clicked()), this, SLOT(DeleteGroup()));
     connect(pushButton_7, SIGNAL(clicked()), this, SLOT(OnClose()));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when tree object is clicked.
//
// #param item Pointer to tree object.
// #param col Column of Tree object that was clicked.
// #return No return value.
void GroupManager_ui::TreeClick(QTreeWidgetItem* item, int col)
{
     // Pobranie nazwy gkliknietej grupy
     QString gname = item->text(col);

     int index = AttributeGroups->IndexOfName(gname);

     if(index == -1)
          return;

     lineEdit->setText(AttributeGroups->Group(index)->Name());
     lineEdit_2->setText(AttributeGroups->Group(index)->Description());
     lineEdit_3->setText(QString::number(AttributeGroups->Group(index)->Level(), 10));
     lineEdit_4->setText(QString::number(AttributeGroups->Group(index)->Count(), 10));
}
// -----------------------------------------------------------------------------

// #brief Function refreshes view of tree.
//
// #return No return value.
void GroupManager_ui::refreshGroupTree(void)
{
     treeWidget->clear();
     treeWidget->insertTopLevelItems(0, AttributeGroups->CreateTree());
}
// -----------------------------------------------------------------------------

// #brief Function creates and sets new main group window.
//
// #return No return value.
void GroupManager_ui::CreateNewMainGroup(void)
{
     delete GroupEdit;
     GroupEdit = new GroupEditor_ui(true, 0, true);
     connect(GroupEdit, SIGNAL(onClose()), this, SLOT(refreshGroupTree()));
     GroupEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function creates and sets new sub group window.
//
// #return No return value.
void GroupManager_ui::CreateNewSubGroup(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());

     if(index == -1)
     {
          const QString content = "There is no group selected.\nPlease select a group and try again.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }
     
     // Nie mozna doawac nowej podgrupy do grupy Global
     if(index == 0)
     {
          const QString content = "You can't add a new sub group for Global group.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     delete GroupEdit;
     GroupEdit = new GroupEditor_ui(true, index, false);
     
     // podlaczenie sygnalu na zamykanie okienka z odswierzenie listy grup
     connect(GroupEdit, SIGNAL(onClose()), this, SLOT(refreshGroupTree()));
     GroupEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function shows edit group window.
//
// #return No return value.
void GroupManager_ui::EditGroup(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());

     if(index == -1)
     {
          const QString content = "There is no group selected.\nPlease select a group and try again.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Nie mozna edytowac grupy glownej Global
     if(index == 0)
     {
          const QString content = "You can't change a Global group properties.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     delete GroupEdit;
     GroupEdit = new GroupEditor_ui(false, index, AttributeGroups->Group(index)->Level() == 0);
     
     // podlaczenie sygnalu na zamykanie okienka z odswierzenie listy grup
     connect(GroupEdit, SIGNAL(onClose()), this, SLOT(refreshGroupTree()));
     GroupEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function deletes group that was chosen.
//
// #return No return value.
void GroupManager_ui::DeleteGroup(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());

     if(index == -1)
     {
          const QString content = "There is no group selected.\nPlease select a group and try again.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Nie mozna usuwac grupy glownej Global
     if(index == 0)
     {
          const QString content = "You can't remove a Global group.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     if(QMessageBox::question(NULL, "HQEd", "Do you realy want to remove this group?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     AttributeGroups->Delete(AttributeGroups->Group(index)->Name());
     refreshGroupTree();

     // Odswiezenie widolu tabel
     TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function shows select group window.
//
// #return No return value.
void GroupManager_ui::SelectNewParent(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());

     if(index == -1)
     {
          const QString content = "There is no group selected.\nPlease select a group and try again.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }
     if(index == 0)
     {
          const QString content = "You can't change a parent for group Global";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }
     
     delete SelGroup;
     SelGroup = new SelectGroup_ui;
     connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(ChangeParent()));
     SelGroup->show();
}
// -----------------------------------------------------------------------------

// #brief Function changes parent for the group.
//
// #return No return value.
void GroupManager_ui::ChangeParent(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());
     int pindex = AttributeGroups->IndexOfName(SelGroup->Result());

     if(pindex == -1)
          return;
          
     if(pindex == 0)
     {
          const QString content = "You can't set Global group as parent.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     AttributeGroups->setGroupParent(AttributeGroups->Group(index), AttributeGroups->Group(pindex)->ID());
     refreshGroupTree();

     // Odswiezenie widolu tabel
     TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function sets group as main group.
//
// #return No return value.
void GroupManager_ui::SetAsMain(void)
{
     // Wyszukiwanie indexu grupy do ktorej chcemy dodac potomka
     int index = AttributeGroups->IndexOfName(lineEdit->text());

     if(index == -1)
     {
          const QString content = "There is no group selected.\nPlease select a group and try again.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }
          
     if(AttributeGroups->Group(index)->Level() == 0)
          return;

     AttributeGroups->setGroupAsRoot(AttributeGroups->Group(index));
     refreshGroupTree();

     // Odswiezenie widolu tabel
     TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when group manager window is closed, it also refreshes tables view.
//
// #return No return value
void GroupManager_ui::OnClose(void)
{
     TableList->RefreshAll();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void GroupManager_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               OnClose();
               break;

          case Qt::Key_Return:
               EditGroup();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
