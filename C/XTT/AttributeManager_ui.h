 /**
 * \file	AttributeManager_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	29.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges attribute manager window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ATTRIBUTEMANAGER_H
#define ATTRIBUTEMANAGER_H
// -----------------------------------------------------------------------------
 
#include <QGridLayout>
#include <QTreeWidgetItem>
 
#include "ui_AttributeManager.h"
// -----------------------------------------------------------------------------

class hType;
class XTT_Cell;
class XTT_Attribute;
class AttributeGroupsTreeWidget;
class AttributeManagerTableWidget;
// -----------------------------------------------------------------------------
/**
* \class 	AttributeManager_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	29.04.2007 
* \brief 	Class definition that arranges attribute manager window.
*/
class AttributeManager_ui : public QWidget, private Ui_attributeManager
{
    Q_OBJECT

private:

     AttributeManagerTableWidget* tableWidget;    ///< Table that conatins the list of attributes
     AttributeGroupsTreeWidget* treeWidget;       ///< Tree that conatins the list of groups
     unsigned int fGroupIndex;	               ///< Index of group which attribute belongs to.
     
     XTT_Attribute* _selectedAttribute;      ///< Pointer to the selected attribute
     
     // the filed realted with attribute filter
     hType* _type;                           ///< Attribute compatible type
     int _setClass;                          ///< The class of the defined set (0-set, 1-domain, 2-decision context)
     bool _sreq;                             ///< Determines if the requirements of single valued attributes
     XTT_Cell* _cell;                        ///< Pointer to the edited cell
     XTT_Attribute* _currentAttribute;       ///< Pointer to the current attribute in the expression
     bool _useFilter;                        ///< Determines whether the filter should be used
     
     QGridLayout* formLayout;                ///< Pointer to grid layout form.
     QGridLayout* gb1Layout;	               ///< Pointer to groupBox grid layout.
     QGridLayout* gb2Layout;	               ///< Pointer to groupBox_2 grid layout.
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);

public:

     /// \brief Constructor AttributeManager_ui class.
	///
	/// \param parent Pointer to parent object.
	AttributeManager_ui(QWidget *parent = 0);
     
     /// \brief Destructor AttributeManager_ui class.
	~AttributeManager_ui(void);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function that returns the attribute selection result
	///
	/// \return the attribute selection result
     XTT_Attribute* selectionResult(void);
     
     /// \brief Function that sets the window mode
	///
     /// \param *__type - pointer to the type of attribute
     /// \param __setClass - the class of defined set (0-set, 1-domain, 2-decision context, 3-conditional context)
     /// \param __singleRequired - determines if the multiplicity of argument value must be single (true) or multiple (false)
     /// \param *__currentAttribute - the pointer to the current attribute in the expression
     /// \param *__cell - the pointer to the edited cell.
     /// \param __useFilter - determines whether given filter must be used
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, int __setClass, bool __singleRequired, XTT_Attribute* __currentAttribute, XTT_Cell* __cell, bool __useFilter = true);

public slots:

	/// \brief Function is triggered when you click the tree object.
	///
	/// \param item Pointer to tree object.
	/// \param col Number of column that was clicked.
	/// \return No return value.
	void TreeClick(QTreeWidgetItem* item, int col);

	/// \brief Function reads information about attributes belnogs to the group.
	///
	/// \return No return value.
	void ReadAttributes(void);

	/// \brief Function shows edit attribute window.
	///
	/// \return No return value.
	void AddNewAttribute(void);
	
	/// \brief Function shows edit attribute window.
	///
	/// \return No return va
	void EditAttribute(void);

	/// \brief Function deletes attribute that was chosen.
	///
	/// \return No return value.
	void DeleteClick(void);

	/// \brief Function shows select group window.
	///
	/// \return No return value.
	void NewGroupSelect(void);

	/// \brief Function is triggered when you change the group of attribute.
	///
	/// \return No return value.
	void NewGroupChange(void);
     
     /// \brief Function is triggered when the select button is clicked
	///
	/// \return No return value.
     void selectClick(void);
     
signals:

     /// \brief Signal emited when the window is closed by the select button
	///
	/// \return No return value.
     void onSelectClose(void);
};
// -----------------------------------------------------------------------------

extern AttributeManager_ui* AttrManager;
// -----------------------------------------------------------------------------

#endif
