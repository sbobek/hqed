/*
 *	   $Id: PluginEditor_ui.cpp,v 1.6.8.1 2011-06-13 17:02:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../namespaces/ns_hqed.h"
#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"
#include "../Settings_ui.h"

#include "PluginEditor_ui.h"
// -----------------------------------------------------------------------------

PluginEditor_ui* PluginEditor;
// -----------------------------------------------------------------------------

// #brief Constructor UnusedAtrtribute_ui class.
//
// #param parent Pointer to parent object.
PluginEditor_ui::PluginEditor_ui(QWidget*)
{
    initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor PluginEditor_ui class.
PluginEditor_ui::~PluginEditor_ui(void)
{
     delete gp3Layout;
     delete gp5Layout;
     delete sw1p1layout;
     delete sw1p0layout;
     delete gp4Layout;
     delete gp2Layout;
     delete gp1Layout;
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void PluginEditor_ui::initForm(void)
{
    setupUi(this);
    hqed::centerWindow(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    setWindowModality(Qt::ApplicationModal);
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(okClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(cancelClick()));
    connect(toolButton, SIGNAL(clicked()), this, SLOT(selectFileClick()));
    connect(comboBox, SIGNAL(currentIndexChanged(int)), stackedWidget, SLOT(setCurrentIndex(int)));

    connect(checkBox, SIGNAL(stateChanged(int)), this, SLOT(modulesSelectionChanged(int)));
    connect(checkBox_3, SIGNAL(stateChanged(int)), this, SLOT(modulesSelectionChanged(int)));
    connect(checkBox_4, SIGNAL(stateChanged(int)), this, SLOT(modulesSelectionChanged(int)));
    
    _mode = -1;
    _plindex = -1;
    comboBox->setCurrentIndex(0);
    stackedWidget->setCurrentIndex(0);

    comboBox_2->addItem("HMR");
    comboBox_2->addItem("HML");
    
    layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void PluginEditor_ui::layoutForm(void)
{    
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   1, 0, 1, 3);
     formLayout->addWidget(pushButton,   2, 1);
     formLayout->addWidget(pushButton_2, 2, 2);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);

     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(comboBox, 0, 0);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);

     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(groupBox_4, 0, 0);
     gp2Layout->addWidget(stackedWidget, 1, 0);
     gp2Layout->setRowStretch(1, 1);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);

     gp4Layout = new QGridLayout;
     gp4Layout->addWidget(label,      0, 0);
     gp4Layout->addWidget(lineEdit,   0, 1, 1, 5);
     gp4Layout->addWidget(label_3,    1, 0);
     gp4Layout->addWidget(checkBox,   1, 1);
     gp4Layout->addWidget(checkBox_4, 1, 2);
     gp4Layout->addWidget(checkBox_2, 1, 3);
     gp4Layout->addWidget(checkBox_3, 1, 4);
     gp4Layout->addWidget(label_6,    2, 0);
     gp4Layout->addWidget(comboBox_2, 2, 1, 1, 5);
     gp4Layout->addWidget(label_53,   3, 0);
     gp4Layout->addWidget(spinBox_9,  3, 1, 1, 5);
     gp4Layout->addWidget(label_5,    4, 0);
     gp4Layout->addWidget(checkBox_5, 4, 1, 1, 5);
     gp4Layout->addWidget(checkBox_6, 5, 1, 1, 5);
     gp4Layout->addWidget(checkBox_7, 6, 1, 1, 5);
     gp4Layout->addWidget(checkBox_8, 7, 1, 1, 5);
     gp4Layout->addWidget(checkBox_9, 8, 1, 1, 5);
     gp4Layout->setColumnStretch(5, 1);
     hqed::setupLayout(gp4Layout);
     groupBox_4->setLayout(gp4Layout);

     sw1p0layout = new QGridLayout;
     sw1p0layout->addWidget(groupBox_5, 0, 0);
     hqed::setupLayout(sw1p0layout);
     stackedWidget->widget(0)->setLayout(sw1p0layout);

     sw1p1layout = new QGridLayout;
     sw1p1layout->addWidget(groupBox_3, 0, 0);
     hqed::setupLayout(sw1p1layout);
     stackedWidget->widget(1)->setLayout(sw1p1layout);

     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(label_2,    0, 0);
     gp3Layout->addWidget(lineEdit_2, 1, 0);
     gp3Layout->addWidget(toolButton, 1, 1);
     gp3Layout->addWidget(label_4,    2, 0);
     gp3Layout->addWidget(lineEdit_4, 3, 0, 1, 2);
     gp3Layout->setColumnStretch(0, 1);
     gp3Layout->setRowStretch(1, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);

     gp5Layout = new QGridLayout;
     gp5Layout->addWidget(label_52,   0, 0);
     gp5Layout->addWidget(comboBox_5, 0, 1);
     gp5Layout->addWidget(label_50,   1, 0);
     gp5Layout->addWidget(lineEdit_3, 1, 1);
     gp5Layout->addWidget(label_51,   2, 0);
     gp5Layout->addWidget(spinBox_8,  2, 1);
     gp5Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp5Layout);
     groupBox_5->setLayout(gp5Layout);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the entered plugin name
//
// #return the entered plugin name as string
QString PluginEditor_ui::plname(void)
{
     return lineEdit->text();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the entered plugin address
//
// #return the entered plugin address as string
QString PluginEditor_ui::pladdress(void)
{
     QString res = "";
     int type = comboBox->currentIndex();
     int serverType = 0;
     int procesType = 1;
     
     if(type == serverType)
          res = lineEdit_3->text();
     if(type == procesType)
          res = lineEdit_2->text();
          
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the entered plugin process params
//
// #return the entered plugin address as string
QString PluginEditor_ui::plparams(void)
{
     return lineEdit_4->text().simplified();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the entered plugin connection timeout
//
// #return the entered plugin connection timeout
int PluginEditor_ui::pltimeout(void)
{
     return spinBox_9->value();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the entered plugin connection port
//
// #return the entered plugin connection port
int PluginEditor_ui::plportnumber(void)
{
     return spinBox_8->value();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the plugin configuration
//
// #return the plugin configuration
int PluginEditor_ui::plconfiguration(void)
{
     int res = 0x00000000;
     
     int pltype = comboBox->currentIndex();
     int fttype = comboBox_2->currentIndex();
     int sctype = comboBox_5->currentIndex();
     bool v = checkBox->isChecked(),
          s = checkBox_4->isChecked(),
          p = checkBox_2->isChecked(),
          t = checkBox_3->isChecked();
     bool mc = checkBox_5->isChecked(),
          mf = checkBox_6->isChecked(),
          ms = checkBox_7->isChecked(),
          mk = checkBox_8->isChecked(),
          mo = checkBox_9->isChecked();
          
     int plt[] = {DEV_PLUGIN_CONNECTION_TYPE_NETWORK, DEV_PLUGIN_CONNECTION_TYPE_PROCESS};
     int ftt[] = {DEV_PLUGIN_PREFFERED_FORMAT_HMR, DEV_PLUGIN_PREFFERED_FORMAT_HML};
     int sct[] = {DEV_PLUGIN_SOCKET_TYPE_TCP, DEV_PLUGIN_SOCKET_TYPE_UDP};
     
     res |= plt[pltype];
     res |= ftt[fttype];
     res |= (v == true ? DEV_PLUGIN_SUPPORTED_MODULES_V : 0);
     res |= (s == true ? DEV_PLUGIN_SUPPORTED_MODULES_S : 0);
     res |= (p == true ? DEV_PLUGIN_SUPPORTED_MODULES_P : 0);
     res |= (t == true ? DEV_PLUGIN_SUPPORTED_MODULES_T : 0);
     
     res |= (mc == true ? DEV_PLUGIN_MESSAGE_TYPE_CONNECTION : 0);
     res |= (mf == true ? DEV_PLUGIN_MESSAGE_TYPE_FORMAT : 0);
     res |= (ms == true ? DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE : 0);
     res |= (mk == true ? DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG : 0);
     res |= (mo == true ? DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS : 0);
     
     res |= ((res & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0 ? sct[sctype] : 0);
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function that allow for setting mode of the dialog
//
// #param __mode - the mode of the dialog
// #param __plindex - index of the edited plugin
// #return SET_WINDOW_MODE_xxx value as a result
int PluginEditor_ui::setMode(int __mode, int __plindex)
{
     _mode = __mode;
     _plindex = __plindex;
     
     // When editing plugin, then we must set up windows w.r.t. values of the edited plugin properties
     if(_mode == PLUGINDIALOGEDITORMODE_EDT)
     {
          int plc = Settings->pluginsCount();
          if((__plindex < 0) || (__plindex >= plc))
          {
               QMessageBox::warning(this, "HQEd", "Incorrect edit mode, reason: incorrect plugin identifier received.\nThe dialog will be switched to new plugin mode.", QMessageBox::Ok);
               _mode = PLUGINDIALOGEDITORMODE_NEW;
               return SET_WINDOW_MODE_FAIL;
          }
          
          // reading plugins properties
          QString plname = Settings->readPluginProperty(_plindex, "name", "");
          QString pladdr = Settings->readPluginProperty(_plindex, "address", "");
          QString plparams = Settings->readPluginProperty(_plindex, "params", "");
          int plpnumber = Settings->readPluginProperty(_plindex, "pnumber", "1").toInt();
          int plctimeout = Settings->readPluginProperty(_plindex, "ctimeout", "-1").toInt();
          int plconfig = Settings->readPluginProperty(_plindex, "config", "0").toInt();
          
          // Setting the window
          lineEdit->setText(plname);
          spinBox_8->setValue(plpnumber);
          spinBox_9->setValue(plctimeout);
          
          comboBox->setCurrentIndex(-1);
          if((plconfig & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0)
          {
               comboBox->setCurrentIndex(0);
               lineEdit_3->setText(pladdr);
          }
          if((plconfig & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
          {
               comboBox->setCurrentIndex(1);
               lineEdit_2->setText(pladdr);
               lineEdit_4->setText(plparams);
          }

          if((plconfig & DEV_PLUGIN_PREFFERED_FORMAT_HMR) > 0)
               comboBox_2->setCurrentIndex(0);
          else if((plconfig & DEV_PLUGIN_PREFFERED_FORMAT_HML) > 0)
               comboBox_2->setCurrentIndex(1);
               
          checkBox->setChecked((plconfig & DEV_PLUGIN_SUPPORTED_MODULES_V) > 0);
          checkBox_2->setChecked((plconfig & DEV_PLUGIN_SUPPORTED_MODULES_P) > 0);
          checkBox_3->setChecked((plconfig & DEV_PLUGIN_SUPPORTED_MODULES_T) > 0);
          checkBox_4->setChecked((plconfig & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0);
          
          checkBox_5->setChecked((plconfig & DEV_PLUGIN_MESSAGE_TYPE_CONNECTION) > 0);
          checkBox_6->setChecked((plconfig & DEV_PLUGIN_MESSAGE_TYPE_FORMAT) > 0);
          checkBox_7->setChecked((plconfig & DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE) > 0);
          checkBox_8->setChecked((plconfig & DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG) > 0);
          checkBox_9->setChecked((plconfig & DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS) > 0);
          
          if((plconfig & DEV_PLUGIN_SOCKET_TYPE_TCP) > 0)
               comboBox_5->setCurrentIndex(0);
          if((plconfig & DEV_PLUGIN_SOCKET_TYPE_UDP) > 0)
               comboBox_5->setCurrentIndex(1);

          modulesSelectionChanged(0);
     }
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function returns the mode of the dialog
//
// #return the mode of the dialog
// #li 0 - create new plugin
// #li 1 - edit plugin
int PluginEditor_ui::mode(void)
{
     return _mode;
}
// -----------------------------------------------------------------------------

// #brief Function returns the index of the edited plugin
//
// #return the index of the edited plugin
int PluginEditor_ui::plindex(void)
{
     return _plindex;
}
// -----------------------------------------------------------------------------

// #brief Function that verifies the entered parameters
//
// #return true on success otherwise false
bool PluginEditor_ui::verifyParams(void)
{
     int type = comboBox->currentIndex();
     int format = comboBox_2->currentIndex();
     int serverType = 0;
     int procesType = 1;
     
     QString serverName = plname();
     bool v = checkBox->isChecked(),
          s = checkBox_4->isChecked(),
          p = checkBox_2->isChecked(),
          t = checkBox_3->isChecked();
          
     // Checking dialog mode
     if((_mode != PLUGINDIALOGEDITORMODE_NEW) && (_mode != PLUGINDIALOGEDITORMODE_EDT))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect mode identifier.", QMessageBox::Ok);
          return false;
     }
          
     if(serverName.isEmpty())
     {
          QMessageBox::critical(NULL, "HQEd", "Please, enter the name of the plugin.", QMessageBox::Ok);
          return false;
     }
     
     if(Settings->pluginIndexOfname(serverName, _plindex) > -1)
     {
          QMessageBox::critical(NULL, "HQEd", "The given name of the plugin \'" + serverName + "\' already exists. Please give a different name.", QMessageBox::Ok);
          return false;
     }
     
     if(!(v || s || p || t))
     {
          QMessageBox::critical(NULL, "HQEd", "Please, select at least one supported module.", QMessageBox::Ok);
          return false;
     }
     
     if(type == serverType)
     {
          QString ac = lineEdit_3->text();
          QHostAddress ha;
          if(!ha.setAddress(ac))
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect format of the IP address.", QMessageBox::Ok);
               return false;
          }
     }

     if((format == -1) && ((v) || (s) || (t)))
     {
          QMessageBox::critical(NULL, "HQEd", "Please choose the preffered model format.", QMessageBox::Ok);
          return false;
     }
     
     if(type == procesType)
     {
          // Checking if the given file name is correct
          QString fn = lineEdit_2->text();
          if(!QFile::exists(fn))
          {
               QMessageBox::critical(NULL, "HQEd", "The given file \'" + fn + "\' does not exists.", QMessageBox::Ok);
               return false;
          }
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when ok key is pressed.
//
// #return No return value.
void PluginEditor_ui::okClick(void)
{
     if(!verifyParams())
          return;
          
     emit onOkClose();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when cancel key is pressed.
//
// #return No return value.
void PluginEditor_ui::cancelClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select file key is pressed.
//
// #return No return value.
void PluginEditor_ui::selectFileClick(void)
{
     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getOpenFileName(this,
                                 tr("Select executable file"),
                                 Settings->projectPath(),
                                 tr("All files (*)"));
     lineEdit_2->setText(fileName);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when modules selection has been changed
//
// #param __state - the new state of the selected item
// #return No return value.
void PluginEditor_ui::modulesSelectionChanged(int /*__state*/)
{
     bool v = checkBox->isChecked(),
          s = checkBox_4->isChecked(),
          //p = checkBox_2->isChecked(),
          t = checkBox_3->isChecked();

     bool vis = v || s || t;
     label_6->setVisible(vis);
     comboBox_2->setVisible(vis);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void PluginEditor_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               cancelClick();
               break;

          case Qt::Key_Return:
               okClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
