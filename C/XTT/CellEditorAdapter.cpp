#include "CellEditorAdapter.h"

#include "QSizeGrip"
#include "QVBoxLayout"
#include "QMessageBox"

#include "CellExpressionTextEditor.h"
#include "M/XTT/XTT_Cell.h"
#include "M/hExpression.h"
#include "M/hType.h"
#include "M/hMultipleValue.h"
#include "namespaces/ns_xtt.h"

CellEditorAdapter::CellEditorAdapter(XTT_Cell *cell, QWidget *parent) : QWidget(parent)
{
    m_EditedCell = cell;
    m_CellEditor = new CellExpressionTextEditor(cell, this);

    m_Layout = new QVBoxLayout;
    m_Layout->setMargin(0);
    m_Layout->addWidget(m_CellEditor);

    setLayout(m_Layout);

    connect(m_CellEditor, SIGNAL(editingFinished()), this, SLOT(slotEditorFinished()));
    connect(m_CellEditor, SIGNAL(editingCancelled()), this, SLOT(slotEditorCancelled()));

    m_CellEditor->adjustEditorSize();
}

void CellEditorAdapter::setMinimumSize(QSize size)
{
    m_CellEditor->setMinimumSize(size);
}

void CellEditorAdapter::focus()
{
    m_CellEditor->setFocus();
}

void CellEditorAdapter::adjustEditorSize()
{
    m_CellEditor->adjustEditorSize();
}

void CellEditorAdapter::slotEditorFinished()
{
    emit editingFinished();
}

void CellEditorAdapter::slotEditorCancelled()
{
    emit editingCancelled();
}
