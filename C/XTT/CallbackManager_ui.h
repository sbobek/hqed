/**
 * \file	CallbackManager_ui.h
 * \author	Lukasz Finster, Sebastian Witowski
 * \date 	24.06.2011
 * \version	1.15
 * \brief	This file contains class definition that arranges delete connection window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef CALLBACKMANAGER_UI_H
#define CALLBACKMANAGER_UI_H
// -----------------------------------------------------------------------------

#include <QWidget>
#include <QString>

#include "../../M/XTT/XTT_CallbackContainer.h"
#include "ui_CallbackManager.h"
#include "../te/sh/HSyntaxHighlighter.h"
#include "../Settings_ui.h"
// -----------------------------------------------------------------------------

/**
* \class       CallbackManager
* \author      Lukasz Finster, Sebastian Witowski
* \date        24.06.2011
* \brief       Class definition of callback.
*/

namespace Ui {
     class CallbackManager;
}

class CallbackManager : public QWidget
{
     Q_OBJECT

public:
     explicit CallbackManager(QString clbId, QWidget *parent = 0);
     ~CallbackManager();

private:

     /// \brief Callback Manager constructor
     Ui::CallbackManager *ui;

     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm();

     /// \brief Function loads data and adds callbacks ids to combo box
     ///
     /// \return No return value
     void loadCombo();

     /// \brief Function loads callback data and show in the window
     ///
     /// \param new callback id
     /// \return No return value
     void ChangeContent(QString id);

     /// \brief Function is triggered after clock on New button, it let to edit data of new callback
     ///
     /// \return No return value
     void NewCallback();

     /// \brief Function saves changes to callback container
     ///
     /// \return No return value
     void ApplyChanges();

     /// \brief Function is Saves changes of callback data to callback container
     ///
     /// \return No return value
     void Save();

     /// \brief Function saves callbacks to callback librery in xml format
     ///
     /// \return No return value
     void SaveToLibrary();

     HSyntaxHighlighter *highlighter;                      ///< Pointer to syntax highlighter
     QString MODE;                                         ///< Variable saves actual working mode
     QString libFile;                                      ///< Patch to library file
     bool changed;                                         ///< inform if any callback in container has benn changed
     QGridLayout *formLayout;

private slots:

     /// \brief Function is triggered when text in combo box is changed;
     ///
     /// \param New callback id
     /// \return No return value
     void ComboChanged(QString id);

     /// \brief Function is triggered after click on Aplly button
     ///
     /// \return No return value
     void ApplyClicked();

     /// \brief Function is triggered when ok Button is clicked
     ///
     /// \return No return value
     void okClicked();

     /// \brief Function is triggered when new button is clicked
     ///
     /// \return No return value
     void NewClicked();

     /// \brief Function is triggered when cancel button is clicked
     ///
     /// \return No return value
     void cancelClick();

     /// \brief Function is triggered when delete button clicked
     ///
     /// \return No return value
     void deleteClick();

     /// \brief Function is triggered after edit button click, it let to edit selected callback data
     ///
     /// \return No return value
     void editClick();

signals:
     void okClick(QString);
     void cancelClicked();
};
// -----------------------------------------------------------------------------

extern CallbackManager* ClbManager;
// -----------------------------------------------------------------------------

#endif // CALLBACKMANAGER_UI_H
