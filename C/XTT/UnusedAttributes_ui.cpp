/*
 *	   $Id: UnusedAttributes_ui.cpp,v 1.32 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_Attribute.h"

#include "../../namespaces/ns_hqed.h"
#include "UnusedAttributes_ui.h"
// -----------------------------------------------------------------------------

UnusedAtrtribute_ui* UnusedAttr;
// -----------------------------------------------------------------------------

// #brief Constructor UnusedAtrtribute_ui class.
//
// #param parent Pointer to parent object.
UnusedAtrtribute_ui::UnusedAtrtribute_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(FindUnusedAtrributes()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);

    FindUnusedAtrributes();
}
// -----------------------------------------------------------------------------

// #brief Function finds unused attributes.
//
// #return No return value.
void UnusedAtrtribute_ui::FindUnusedAtrributes(void)
{
     listWidget->clear();
     listWidget->addItems(AttributeGroups->UnusedAttributesList());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void UnusedAtrtribute_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               close();
               break;

          case Qt::Key_F5:
               FindUnusedAtrributes();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
