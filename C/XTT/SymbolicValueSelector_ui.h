/**
 * \file	SymbolicValueSelector_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	11.05.2009
 * \version	1.0
 * \brief	This file contains class definition that arranges the SymbolicValueSelector window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef SYMBOLICVALUESELECTORH
#define SYMBOLICVALUESELECTORH
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include <QTableWidgetItem>

#include "ui_SymbolicValueSelector.h"
// -----------------------------------------------------------------------------

class hType;
class hValue;
// -----------------------------------------------------------------------------

/**
* \class 	SymbolicValueSelector_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	11.05.2009
* \brief 	Class definition that arranges the SymbolicValueSelector window.
*/
class SymbolicValueSelector_ui : public QWidget, private Ui_SymbolicValueSelector
{
     Q_OBJECT

     QGridLayout* formLayout;	                    ///< Pointer to grid layout form.
     QGridLayout* swp1Layout;	                    ///< Pointer to stackedWidgetPage1 grid layout.   
     QGridLayout* swp2Layout;	                    ///< Pointer to stackedWidgetPage2 grid layout.   
     QGridLayout* gb1Layout;	                    ///< Pointer to groupBox1 grid layout.   
     QGridLayout* gb2Layout;	                    ///< Pointer to groupBox2 grid layout.   
     QGridLayout* gb3Layout;	                    ///< Pointer to groupBox3 grid layout.   
     
     hType* _type;                                ///< The type of edited value
     hValue* _value;                              ///< Edited value
     int _setClass;                               ///< The type of the set
     
public:

     /// \brief Constructor SymbolicValueSelector_ui class.
     ///
     /// \param parent Pointer to parent object.
     SymbolicValueSelector_ui(QWidget *parent = 0);
     
     /// \brief Destructor SymbolicValueSelector_ui class.
     ~SymbolicValueSelector_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function that checks the correctness of the recived parameters
	///
	/// \return true if ok, otherwise false
     bool checkMode(void);
     
     /// \brief Function sets the form according to the recieved parameters
	///
     /// \param __type - Pointer to type of the value object
     /// \param __setClass - the type of the set
     /// \li 0-set
     /// \li 1-domain
     /// \li 2-action context
     /// \li 3-coditional context
     /// \param __value - Pointer to the edited value
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, int __setClass, hValue* __value);
     
     /// \brief Function that updates the current value object according to user setting
	///
	/// \return true on succes, otherwise false
     bool apply(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent*);
     
public slots:

     /// \brief Function triggered when OK button is pressed
	///
	/// \return No return value.
     void okClick(void);
     
     /// \brief Function triggered when cancel button is pressed
	///
	/// \return No return value.
     void closeClick(void);
     
signals:

     /// \brief Signal emited when the ok button is clicked
	///
	/// \return No return value.
     void onOk(void);
     
     /// \brief Signal emited when cancel button is clicked
	///
	/// \return No return value.
     void onClose(void);
};
// -----------------------------------------------------------------------------

#endif
