 /**
 * \file	ItemFinder_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.0
 * \brief	This file contains class definition that can arrange dialog 'ItemFinder...'.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef ITEMFINDERH
#define ITEMFINDERH
// -----------------------------------------------------------------------------

#include <QGridLayout>
 
#include "ui_ItemFinder.h"
// -----------------------------------------------------------------------------

class SearchResultListWidget;
// -----------------------------------------------------------------------------
/**
* \class 	ItemFinder_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	This class contains methods that can arrange dialog 'ItemFinder...'.
*/
class ItemFinder_ui : public QWidget, private Ui_ItemFinder
{
    Q_OBJECT
    
    QGridLayout* formLayout;	///< Pointer to grid layout form.
    QGridLayout* gb1Layout;	///< Pointer to groupBox grid layout.
    QGridLayout* gb2Layout;	///< Pointer to groupBox_2 grid layout.
    QGridLayout* gb3Layout;	///< Pointer to groupBox_3 grid layout.
    QGridLayout* p0Layout;	///< Pointer to page0 grid layout.
    QGridLayout* p1Layout;	///< Pointer to page1 grid layout.
    QGridLayout* p2Layout;	///< Pointer to page2 grid layout.
    
    SearchResultListWidget* resultList;		///< Pointer to object which contains results of search

public:

     /// \brief Constructor ItemFinder_ui class.
	///
	/// \param parent Pointer to parent object.
    ItemFinder_ui(QWidget *parent = 0);
	
        /// \brief Destructor ItemFinder_ui class.
	~ItemFinder_ui(void);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
    
public slots:

	/// \brief Function is triggered when search key is pressed. Function performs search of items.
	///
	/// \return No return value.
	void performSearch(void);
	
	/// \brief Function is triggered when the searched phrase is changed.
	///
        /// \param _newPhrase - new value of searched text.
	/// \return No return value.
	void phraseChange(QString _newPhrase);
};
// -----------------------------------------------------------------------------

extern ItemFinder_ui* ItemFinderWin;
// -----------------------------------------------------------------------------

#endif
