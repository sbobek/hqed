#ifndef PARSERTOKEN_H
#define PARSERTOKEN_H

#include <QList>

#include "LexerToken.h"

enum ParserTokenType
{
    PTT_UNDEFINED,
    PTT_ATTRIBUTE,
    PTT_SYMBOLIC_VALUE,
    PTT_INTEGER_VALUE,
    PTT_NUMERIC_VALUE,
    PTT_SPECIAL_VALUE,
    PTT_QUOTATION_VALUE,
    PTT_SET,
    PTT_RANGE,
    PTT_FUNCTION,
    PTT_OPERATOR,
    PTT_PARENTHESIS_OPENING,
    PTT_PARENTHESIS_CLOSING,
    PTT_SEPARATOR
};

class ParserToken
{
public:
    ParserToken()
        : m_Type(PTT_UNDEFINED), m_IndexInString(-1), m_FunctionArgumentCount(-1),
          m_FunctionOpeningParenthesisFound(false)
    { }

    ParserToken(const ParserTokenType type, const QString &text, const int index)
        : m_Type(type), m_Text(text), m_IndexInString(index), m_FunctionArgumentCount(-1),
          m_FunctionOpeningParenthesisFound(false)
    { }

    ParserTokenType type() const
    {
        return m_Type;
    }

    QString text() const
    {
        return m_Text;
    }

    int indexInString() const
    {
        return m_IndexInString;
    }

    int length() const
    {
        return m_Text.length();
    }

    void setArgumentCount(int count) { m_FunctionArgumentCount = count; }

    int functionArgumentCount() const
    {
        return m_FunctionArgumentCount;
    }

    QList<ParserToken*> *childrenTokens()
    {
        return &m_ChildrenTokens;
    }

private:
    ParserTokenType m_Type;
    QString m_Text;
    int m_IndexInString;

    int m_FunctionArgumentCount;
    bool m_FunctionOpeningParenthesisFound;
    QList<ParserToken*> m_ChildrenTokens;
};

#endif // PARSERTOKEN_H
