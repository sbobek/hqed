 /**
 * \file	ColumnEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges column table editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef COLUMNEDITOR_H
#define COLUMNEDITOR_H
// -----------------------------------------------------------------------------
 
#include "ui_ColumnEditor.h"
// -----------------------------------------------------------------------------
/**
* \class 	CellEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges column table editor window.
*/
class ColumnEditor_ui : public QWidget, private Ui_ColumnEditor
{
    Q_OBJECT

private:

	bool fNew;			///< Determines whether new group should be added.

	int fcIndex;		///< Index of the context.

	QString fPath;		///< Path to attribute name.
	QString groupName;	///< Name of group - kept in window.
	QString attrName;	///< Name of attribute - kept in window.

	int ctxIndex;		///< Index of the context.

	/// \brief Function reads attributs from the group.
	///
	/// \param gindex Index of the group.
	/// \return No return value.
	void ReadAttributes(int gindex);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public:

	/// \brief Constructor ColumnEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	ColumnEditor_ui(QWidget *parent = 0);

	/// \brief Constructor ColumnEditor_ui class.
	///
	/// \param _new Determines whether new column should be created.
	/// \param _path Path to attribute name.
	/// \param _cindex Determines which element is edited.
	/// \param parent Pointer to parent object.	
	ColumnEditor_ui(bool _new, QString _path, int _cindex, QWidget *parent = 0);

	/// \brief Function gets group name that is kept in window.
	///
	/// \return Name of the group.
	QString GroupName(void){ return groupName; }

	/// \brief Function gets attribute name that is kept in window.
	///
	/// \return Name of the attribute.
	QString AttributeName(void){ return attrName; }

	/// \brief Function gets index of the context.
	///
	/// \return Index of the context.
	int ContextIndex(void){ return ctxIndex; }

public slots:

	/// \brief Function is triggered when button OK is pressed.
	///
	/// \return No return value.
	void ClickOk(void);

	/// \brief Function is triggered when button Cancel is pressed.
	///
	/// \return No return value.
	void ClickCancel(void);

	/// \brief Function shows select new group window.
	///
	/// \return No return value.
	void NewGroupSelect(void);

	/// \brief Function refreshes window after changing the group.
	///
	/// \return No return value.
	void NewGroupChange(void);
     
     /// \brief Function is triggered when context changed
	///
     /// \param __index - the new item index
	/// \return No return value.
     void contextChanged(int __index);

signals:

	/// \brief Function sends signal of closing window.
	///
	/// \return No return value.
	void onClose(void);
};
// -----------------------------------------------------------------------------

extern ColumnEditor_ui* ColumnEdit;
// -----------------------------------------------------------------------------

#endif
