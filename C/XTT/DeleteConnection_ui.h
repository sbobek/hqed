/**
 * \file	DeleteConnection_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges delete connection window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef DELETECONNECTIONH
#define DELETECONNECTIONH
// -----------------------------------------------------------------------------
 
#include "ui_DeleteConnection.h"
// -----------------------------------------------------------------------------
/**
* \class 	DeleteConnection_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges delete connection window.
*/
class DeleteConnection_ui : public QWidget, private Ui_DeleteConnection
{
    Q_OBJECT

public:

    /// \brief Constructor DeleteConnection_ui class.
	///
	/// \param parent Pointer to parent object.
	DeleteConnection_ui(QWidget *parent = 0);

    /// \brief Constructor DeleteConnection_ui class.
	///
	/// \param tindex Index of table in which you want to delete connection.
	/// \param rindex Index of table row from which you want to delete connection.
	/// \param cindex Index of connection you want to delete.
	/// \param parent Pointer to parent object.
	DeleteConnection_ui(int tindex, int rindex, int cindex, QWidget *parent = 0);

	int fIndex;	///< Index of edited connection.

	/// \brief Function reads titles from tables.
	///
	/// \return No return value.
	void ReadTables(void);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

	/// \brief Function is triggered when button 'Delete' is pressed - it deletes connection.
	///
	/// \return No return value.
	void ClickDelete(void);

	/// \brief Function is triggered when select table field is changed.
	///
	/// \param new_index Index of table.
	/// \return No return value.
	void ComboBoxChange(int new_index);

	/// \brief Function is triggered when table select row field is changed.
	///
	/// \param new_index Index of row.
	/// \return No return value.
	void ComboBoxChange_2(int new_index);

	/// \brief Function is triggered when select connection field is changed.
	///
	/// \param new_index Index of connection.
	/// \return No return value.
	void ComboBoxChange_3(int new_index);
};
// -----------------------------------------------------------------------------

extern DeleteConnection_ui* DelConn;
// -----------------------------------------------------------------------------

#endif
