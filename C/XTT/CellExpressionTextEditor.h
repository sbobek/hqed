#ifndef ACTIONTEXTEDITOR_H
#define ACTIONTEXTEDITOR_H

#include "QTextEdit"
#include "QMap"
#include "QStringList"
#include "QModelIndex"
#include "ExpressionToken.h"
#include "ExpressionParserContext.h"

QT_BEGIN_NAMESPACE
class QPushButton;
class QCompleter;
class QStandardItemModel;
class QGraphicsItem;
QT_END_NAMESPACE

class hExpression;
class hFunction;
class hType;
class XTT_Cell;
class XTT_Table;
class ExpressionToken;
class ParenthesisHighlighter;
class ExpressionSyntaxHighlighter;

class CellExpressionTextEditor : public QTextEdit
{
    Q_OBJECT

public:
    CellExpressionTextEditor(XTT_Cell *cell, QWidget *parent = 0);
    CellExpressionTextEditor(const ExpressionParserContext &context, QWidget *parent = 0);
    ~CellExpressionTextEditor();

    QGraphicsItem* getGraphicsItem(unsigned int &xOffset, unsigned int &yOffset);
    void setGraphicsItem(QGraphicsItem *item, unsigned int xOffset, unsigned int yOffset);

    bool parseStringToExpression();
    hExpression* result() { return m_EditedExpression; }

public slots:
    void adjustEditorSize();

protected:
    void keyPressEvent(QKeyEvent *e);
    void focusOutEvent(QFocusEvent *e);

signals:
    void editingFinished();
    void editingCancelled();

private slots:
    void focusChanged(QWidget *old, QWidget *now);
    void displayCompleter();
    void completionHighlighted(const QString &completion);
    void insertNameCompletion(const QString &completion);

private:
    QString getCurrentTokenBeginAndEnd(int &tokenBegin, int &tokenEnd);
    void replaceCurrentTokenWithCompletion(const QString &completion, const int begin, const int len);
    bool isAllowedTokenCharacter(const QChar character);
    void initialize();
    void initializeSyntaxHighlighter();
    void initializeCompleter();

    QStringList getFunctionNames();
    QStringList getAttributeNames();
    QStringList getSpecialValueNames();
    QStringList getSymbolicValueNames();

    QStringList getFunctionNamesForCompletions();
    QStringList getSpecialValueNamesForCompletions();

    ParenthesisHighlighter *m_ParenthesisHighlighter;
    ExpressionSyntaxHighlighter *m_SyntaxHighlighter;

    bool m_EditorClosing;

    ExpressionParserContext m_ParserContext;

    XTT_Cell *m_EditedCell;
    XTT_Table *m_ParentTable;
    hExpression *m_EditedExpression;

    QGraphicsItem *m_GraphicsItem;
    unsigned int m_GraphicsItemXOffset;
    unsigned int m_GraphicsItemYOffset;

    QCompleter *m_Completer;
    QStandardItemModel *m_CompletionModel;
};

#endif // ACTIONTEXTEDITOR_H
