#include "ExpressionParserError.h"

#include "ExpressionParser.h"
#include <QObject>

QString ExpressionParserError::errorTypeToString(ErrorType type)
{
    switch(type)
    {
    case ET_NO_ERROR:
    {
        return QString("No error");
    }
    case ET_EXPRESSION_EMPTY:
    {
        return QString("Expression empty");
    }
    case ET_UNKNOWN_ERROR:
    {
        return QString("Unknown error");
    }
    case ET_UNDEFINED_TOKEN:
    {
        return QString("Undefined token");
    }
    case ET_PARENTHESIS_MISMATCH:
    {
        return QString("Parenthesis mismatch");
    }
    case ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT:
    {
        return QString("Element not allowed in this context");
    }
    case ET_INVALID_RANGE_DEFINITION:
    {
        return QString("Invalid range definition");
    }
    case ET_INVALID_USE_OF_SEPARATOR:
    {
        return QString("Invalid use of separator");
    }
    case ET_MISSING_SEPARATOR:
    {
        return QString("Missing separator");
    }
    case ET_INVALID_USE_OF_FUNCTION_NAME:
    {
        return QString("Invalid use of a function name");
    }
    case ET_WRONG_ARGUMENT_COUNT:
    {
        return QString("Wrong argument count for a function or operator");
    }
    case ET_TOP_LEVEL_NOT_FUNCTION:
    {
        return QString("Top level element is not a function");
    }
    case ET_ECMA_NO_INPUT_FUNCTION:
    {
        return QString("No input function in presentation plugin has been found");
    }
    }

    return QString();
}

QString ExpressionParserError::toString()
{
    QString errorString = QObject::tr("Error encountered in expression at index ") +
            QString::number(m_LexerToken.indexInString()) + ": " + errorTypeToString(m_ErrorType);

    return errorString;
}
