/**
 * \file	AttributeEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.15
 * \brief	This file contains class definition that arranges editor attribute window. CONTROLLER
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef MYQTAPP_H
#define MYQTAPP_H
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include "ui_AttributeEditor.h"
// -----------------------------------------------------------------------------

class hType;
// -----------------------------------------------------------------------------
/**
* \class 	AttrEditor
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges editor attribute window.
*/
class AttrEditor : public QWidget, private Ui_attributeEditor
{
    Q_OBJECT

private:

    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gb1Layout;	               ///< Pointer to groupBox_1 grid layout.
    QGridLayout* gb2Layout;	               ///< Pointer to groupBox_2 grid layout.
    QGridLayout* gb3Layout;	               ///< Pointer to groupBox_3 grid layout.
    QGridLayout* gb4Layout;	               ///< Pointer to groupBox_4 grid layout.
    QGridLayout* gb5Layout;	               ///< Pointer to groupBox_5 grid layout.
    QGridLayout* gb6Layout;	               ///< Pointer to groupBox_6 grid layout.

    bool fNew;                                ///< Variable that determine whether new attribute should be create.
    int fAttrGroupIndex;	               ///< Index of group which attribute belongs to.
    int fAttrIndex;                           ///< Index of attribute that is edited.
    int fAttrAbstraction;                     ///< Indicates the allowed type of attribute abstraction
    hType* fSelectedType;                     ///< Selected type for the attribute
    QString fSelectedClb;                     ///< Selected Callback  // FISNTER

    /// \brief Function that initializes the initial values of the fileds of class
    ///
    /// \return No return value.
    void initForm(void);

    /// \brief Function that layouts the controls on the form
    ///
    /// \return No return value.
    void layoutControls(void);

public:

    /// \brief Constructor AttrEditor class.
    ///
    /// \param parent Pointer to parent object.
    AttrEditor(QWidget *parent = 0);

    /// \brief Load Callbacks Id from container to ComboBox.
    ///
    /// \return No return value.
    void LoadCombo();

    /// \brief Constructor AttrEditor class.
    ///
    /// \param attrID Index of edited attribute.
    /// \param New True when new attribute is added, false when attribute is edited.
    /// \param parent Pointer to parent object.
    AttrEditor(QString attrID, bool New, QWidget *parent = 0);

    /// \brief Constructor AttrEditor class.
    ///
    /// \param New True when new attribute is added, false when attribute is edited.
    /// \param GroupIndex Index of group from which attribute is edited.
    /// \param AttrIndex Index of edited attribute.
    /// \param parent Pointer to parent object.
    AttrEditor(bool New, int GroupIndex, int AttrIndex = -1, QWidget *parent = 0);

    /// \brief Destructor AttrEditor class.
    ~AttrEditor(void);

    /// \brief Function sets window according to parameters.
    ///
    /// \param GroupIndex Index of group.
    /// \param AttrIndex Index of attribute.
    /// \return No return value.
    void SelectedAttributeChanged(int GroupIndex, int AttrIndex);

    /// \brief Function reads attributs from the group.
    ///
    /// \return No return value.
    void ReadAttributesNames(void);

    /// \brief Function that saves the changes
    ///
    /// \return true if everything is ok otherwise false
    bool applyChanges(void);

    /// \brief Function is triggered when key is pressed.
    ///
    /// \param event Pointer to event object.
    /// \return No return value.
    void keyPressEvent(QKeyEvent*);

public slots:

    /// \brief Funtion is triggered when any Radio Button i Type Group chane value
    ///
    /// \return No return value
    void typeChange();

    /// \brief Funtion is triggered when Callback Manager button is clicked
    ///
    /// \return No return value
    void clbManager(void);

    /// \brief Function is triggered when selected index of combo box is changed.
    /// \param clbID - Content of combo - callback ID
    /// \return No return value
    void clbComboChaned(QString clbID);

    /// \brief Function is triggered after close the Callback Manager window by clicling OK button.
    /// \param id - Selected Callback Id in Callback Manager
    /// \return No return value
    void ClbManagerOk(QString id);

    /// \brief Function is triggered after close the Callback Manager window by clicling Cancel button.
    ///
    /// \return No return value
    void ClbManagerCancel();

    /// \brief Function is triggered when buttom OK is clicked.
    ///
    /// \return No return value.
    void OKClick(void);

    /// \brief Function is triggered when button apply is clicked.
    ///
    /// \return No return value.
    void applyClick(void);

    /// \brief Function is triggered when selected index of combo box is changed.
    ///
    /// \param Index Selected index of combo box.
    /// \return No return value.
    void ComboBoxChange(int Index);

    /// \brief Function is triggered when text in combo box is changed; attribute name creates acronym name automaticaly.
    ///
    /// \param _newText New text value.
    /// \return No return value.
    void comboBoxEditTextChange(QString _newText);

    /// \brief Function is triggered when selected source group button is clicked.
    ///
    /// \return No return value.
    void SrcGroupSelect(void);

    /// \brief Function is triggered when selected destiny group button is clicked.
    ///
    /// \return No return value.
    void DstGroupSelect(void);

    /// \brief Function is triggered when source group is changed.
    ///
    /// \return No return value.
    void OnSrcGroupChange(void);

    /// \brief Function is triggered when destiny group is changed.
    ///
    /// \return No return value.
    void OnDstGroupChange(void);

    /// \brief Function is triggered when the type select button is pressed
    ///
    /// \return No return value.
    void TypeSelect(void);

    /// \brief Function is triggered when argument type changed
    ///
    /// \return No return value.
    void OnTypeSelect(void);

    /// \brief Function is triggered when argument type changed
    ///
    /// \param _index - the  index of the type on the list of types
    /// \return No return value.
    void OnTypeSelect(int _index);

    /// \brief Function is triggered when the combobox item is highlighted
    ///
    /// \param __typeIndex - the index of the type
    /// \return No return value.
    void OnTypeHighlight(int __typeIndex);

    /// \brief Function is triggered when the type manager has closed
    ///
    /// \return No return value.
    void onTypeManagerClose(void);

    /// \brief Function reads the names of types
    ///
    /// \return No return value.
    void readTypesNames(void);

    /// \brief Function that sets the combobox filed according to the current type of the attribute
    ///
    /// \return No return value.
    void selectAttributeCurrentType(void);

    /// \brief Function that sets the combobox filed according to the current Callback of the attribute
    ///
    /// \return No return value.
    void selectAttributeCurrentCallback();

signals:

    /// \brief Function is triggered when button OK is clicked.
    ///
    /// \return No return value.
    void ClickOK(void);
};
// -----------------------------------------------------------------------------

extern AttrEditor* AttributeEditor;
// -----------------------------------------------------------------------------

#endif
