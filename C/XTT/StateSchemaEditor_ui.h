/**
 * \file	StateSchemaEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.12.2009 
 * \version	1.0
 * \brief	This file contains class definition arranges state schema editor windiow dialog
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef STATESCHEMAEDITOR_UI
#define STATESCHEMAEDITOR_UI
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_StateSchemaEditor.h"
// -----------------------------------------------------------------------------

class XTT_Attribute;
class XTT_State;
class XTT_States;
class XTT_Statesgroup;
class StateSchemaEditorListWidget;
// -----------------------------------------------------------------------------
/**
* \class 	StateSchemaEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.12.2009
* \brief 	This file contains class definition arranges state schema editor windiow dialog
*/
class StateSchemaEditor_ui : public QWidget, private Ui_StateSchemaEditor
{
     Q_OBJECT

private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     QGridLayout* gb1Layout;	               ///< Pointer to grid layout of groupBox1.
     QGridLayout* gb2Layout;	               ///< Pointer to grid layout of groupBox2.
     QGridLayout* gb3Layout;	               ///< Pointer to grid layout of groupBox3.
     QGridLayout* gb4Layout;	               ///< Pointer to grid layout of groupBox4.
     
     StateSchemaEditorListWidget* allAtts;   ///< Pointer to the widget that displays all attributes
     StateSchemaEditorListWidget* stsAtts;   ///< Pointer to the widget that displays only state attributes
     
     QList<XTT_Attribute*>* _allAtts;        ///< Pointer to the list of all attributes
     QList<XTT_Attribute*>* _stsAtts;        ///< Pointer to the list of state attributes
     
     XTT_Statesgroup* _stsGroup;             ///< Pointer to the edited states group
     QString _stsGroupName;                  ///< The name of the state group
     bool _isNew;                            ///< Indicates if the edited group is new

     /// \brief Function that initializes the window
     ///
     /// \return no values return
     void initWindow(void);
     
     /// \brief Function that layouts the window
     ///
     /// \return no values return
     void layoutWindow(void);
     
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public:

     /// \brief Constructor StateSchemaEditor_ui class.
     ///
     /// \param parent Pointer to parent object.
     StateSchemaEditor_ui(QWidget *parent = 0);
     
     /// \brief Constructor StateSchemaEditor_ui class.
     ///
     /// \param __group - pointer to the astates group that is edited
     /// \param __groupname - the name of the group
     /// \param parent - Pointer to parent object.
     StateSchemaEditor_ui(XTT_Statesgroup* __group, QString __groupname, QWidget *parent = 0);

     /// \brief Destructor StateSchemaEditor_ui class.
     ~StateSchemaEditor_ui(void);
     
     /// \brief The function that sets the window according to the edited state up.
     ///
     /// \return no value return
     void readStateSetup(void);

public slots:

     /// \brief Function that is triggered when user adds the attributes to the state schema
     ///
     /// \param __items - pointer to the list of indexes of the list items
     /// \return no values return
     void onAttsAddToState(QList<int>* __items);
     
     /// \brief Function that is triggered when user removes some attributes from the state schema
     ///
     /// \param __items - pointer to the list of indexes of the list items
     /// \return no values return
     void onAttsRemoveFromState(QList<int>* __items);
     
     /// \brief Function that is triggered when user click button that adds the attributes to the state schema
     ///
     /// \return no values return
     void onButtonAttsAddToStateClick(void);
     
     /// \brief Function that is triggered when user click button that removes some attributes from the state schema
     ///
     /// \return no values return
     void onButtonAttsRemoveFromStateClick(void);
     
     /// \brief Function that is triggered when filter was changed by user 
     ///
     /// \return no values return
     void filterChanged(void);
     
     /// \brief Function that is triggered when user closes the dialog with cancel button
     ///
     /// \return no values return
     void cancelClick(void);
     
     /// \brief Function that is triggered when user closes the dialog with ok button
     ///
     /// \return no values return
     void okClick(void);
     
signals:
     
     /// \brief Signal that is emitted when user closes the dialog using ok button
     void okClicked(void);
};
// -----------------------------------------------------------------------------

extern StateSchemaEditor_ui* StateSchemaEditor;
// -----------------------------------------------------------------------------

#endif
