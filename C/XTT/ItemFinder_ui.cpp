 /**
 * \file	ItemFinder_ui.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.0
 * \brief	This file contains class definition that can arrange dialog 'ItemFinder...'.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QKeyEvent>

#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_TableList.h"

#include "../../namespaces/ns_hqed.h"
#include "widgets/SearchResultListWidget.h"
#include "ItemFinder_ui.h"
// -----------------------------------------------------------------------------

ItemFinder_ui* ItemFinderWin;
// -----------------------------------------------------------------------------

// #brief Constructor ItemFinder_ui class.
//
// #param parent Pointer to parent object.
ItemFinder_ui::ItemFinder_ui(QWidget*)
{
	setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
	
	connect(toolButton, SIGNAL(clicked()), this, SLOT(performSearch()));
	connect(lineEdit, SIGNAL(textChanged(QString)), this, SLOT(phraseChange(QString)));
	
	// Create list of result object
	resultList = new SearchResultListWidget;
	resultList->setParent(groupBox_3);
     
     resize(5,5);
	formLayout = new QGridLayout;
	formLayout->addWidget(groupBox, 0, 0);
	formLayout->addWidget(groupBox_2, 1, 0);
	formLayout->addWidget(groupBox_3, 2, 0);
	formLayout->setColumnMinimumWidth(0, 290);
     hqed::setupLayout(formLayout);
	setLayout(formLayout);
	
	gb1Layout = new QGridLayout;
	gb1Layout->addWidget(lineEdit, 0, 0);
	gb1Layout->addWidget(toolButton, 0, 1);
     hqed::setupLayout(gb1Layout);
	groupBox->setLayout(gb1Layout);
	
	gb2Layout = new QGridLayout;
	gb2Layout->addWidget(toolBox, 0, 0);
     hqed::setupLayout(gb2Layout);
	groupBox_2->setLayout(gb2Layout);
	
	gb3Layout = new QGridLayout;
	gb3Layout->addWidget(resultList, 0, 0);
     hqed::setupLayout(gb3Layout);
	groupBox_3->setLayout(gb3Layout);
	
	// Layouts on pages
	p0Layout = new QGridLayout;
	p0Layout->setSpacing(0);
	p0Layout->addWidget(checkBox_13, 0, 0);
	p0Layout->addWidget(label_2, 1, 0, 1, 3);
	p0Layout->addWidget(radioButton_2, 2, 0);
	p0Layout->addWidget(radioButton, 2, 1);
	p0Layout->setColumnStretch(3, 10);
     hqed::setupLayout(p0Layout);
	toolBox->widget(0)->setLayout(p0Layout);
	
	p1Layout = new QGridLayout;
	p1Layout->setSpacing(0);
	p1Layout->addWidget(checkBox, 0, 0);
	p1Layout->addWidget(checkBox_12, 1, 0);
	p1Layout->addWidget(checkBox_2, 2, 0);
	p1Layout->addWidget(checkBox_3, 3, 0);
	p1Layout->addWidget(label, 4, 0);
	p1Layout->addWidget(checkBox_8, 4, 1);
	p1Layout->addWidget(checkBox_9, 4, 2);
	p1Layout->addWidget(checkBox_10, 4, 3);
	p1Layout->addWidget(checkBox_11, 4, 4);
	p1Layout->setColumnStretch(5, 10);
     hqed::setupLayout(p1Layout);
	toolBox->widget(1)->setLayout(p1Layout);
	
	p2Layout = new QGridLayout;
	p2Layout->setSpacing(0);
	p2Layout->addWidget(checkBox_4, 0, 0);
	p2Layout->addWidget(checkBox_5, 1, 0);
	p2Layout->addWidget(checkBox_6, 2, 0);
	p2Layout->addWidget(checkBox_7, 3, 0);
     hqed::setupLayout(p2Layout);
	toolBox->widget(2)->setLayout(p2Layout);
}
// -----------------------------------------------------------------------------

// #breif Destructor ItemFinder_ui class.
ItemFinder_ui::~ItemFinder_ui(void)
{
	delete formLayout;
	delete gb1Layout;
	delete gb2Layout;
	delete gb3Layout;
	delete p0Layout;
	delete p1Layout;
	delete p2Layout;
	
	delete resultList;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ItemFinder_ui::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Return:
               performSearch();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when search key is pressed. Function performs search of items.
//
// #return No return value.
void ItemFinder_ui::performSearch(void)
{
	// Clear previous result
	resultList->deleteAll();
	
	bool tabCriteria[] = {checkBox->isChecked(),
					checkBox_12->isChecked(),
					checkBox_2->isChecked(),
					checkBox_3->isChecked(),
					checkBox_8->isChecked(),
					checkBox_9->isChecked(),
					checkBox_10->isChecked(),
					checkBox_11->isChecked()
					};
					
	bool conCriteria[] = {checkBox_4->isChecked(),
					checkBox_5->isChecked(),
					checkBox_6->isChecked(),
					checkBox_7->isChecked()
					};
					
	bool caseSensitivity = checkBox_13->isChecked();
	int _phrasesPolicy = CONJUNCTION_POLICY;
	if(radioButton->isChecked())
		_phrasesPolicy = DISJUNCTION_POLICY;
	QString phrase = lineEdit->text();
	
	if(phrase.isEmpty())
		return;

	QStringList* tables = TableList->findTables(phrase, caseSensitivity, tabCriteria, _phrasesPolicy);
	QStringList* connections = ConnectionsList->findConnections(phrase, caseSensitivity, conCriteria, _phrasesPolicy);
	
	// Display result
	for(int t=0;t<tables->size();++t)
		resultList->add(tables->at(t));
	for(int c=0;c<connections->size();++c)
		resultList->add(connections->at(c));
		
	// Deleting dynamic alocated memory
	delete tables;
	delete connections;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the searched phrase is changed.
//
// #param QString _newPhrase - new value of searched text.
// #return No return value.
void ItemFinder_ui::phraseChange(QString /*_newPhrase*/)
{
	performSearch();
}
// -----------------------------------------------------------------------------
