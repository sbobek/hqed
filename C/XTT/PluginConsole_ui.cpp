/*
 *	   $Id: PluginConsole_ui.cpp,v 1.4.8.1 2011-09-29 09:18:04 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"

#include "../../namespaces/ns_hqed.h"

#include "PluginConsole_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor PluginEditor_ui class.
//
// #param parent Pointer to parent object.
PluginConsole_ui::PluginConsole_ui(QWidget* /*parent*/)
{
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor PluginConsole_ui class.
PluginConsole_ui::~PluginConsole_ui(void)
{
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void PluginConsole_ui::initForm(void)
{
    setupUi(this);
    hqed::centerWindow(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(sendClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeClick()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(clearClick()));
    
    textEdit->setReadOnly(true);
    comboBox->setFocus();
    
    layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void PluginConsole_ui::layoutForm(void)
{    
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(label,        0, 0, 1, 2);
     formLayout->addWidget(textEdit,     1, 0, 1, 4);
     formLayout->addWidget(label_2,      2, 0, 1, 2);
     formLayout->addWidget(comboBox,     3, 0, 1, 4);
     formLayout->addWidget(pushButton_2, 4, 0);
     formLayout->addWidget(pushButton_3, 4, 1);
     formLayout->addWidget(pushButton,   4, 3);
     
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(2, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the dialog mode
//
// #param _pluginPtr - pointer to the plugin
// #return SET_WINDOW_MODE_xxx value as a result
int PluginConsole_ui::setMode(devPlugin* __pluginPtr)
{
     // Checking parameters
     if(__pluginPtr == NULL)
          return SET_WINDOW_MODE_FAIL;
     
     // Saving parameters
     _plugin = __pluginPtr;
     
     // applaying parameters
     setWindowTitle(_plugin->name() + " console");
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when ok key is pressed.
//
// #return No return value.
void PluginConsole_ui::sendClick(void)
{
     QString text = comboBox->currentText();
     comboBox->clearEditText();
     if((comboBox->findText(text) == -1) && (!text.isEmpty()))
        comboBox->addItem(text);
     
     QString html = "<b>HQEd:</b> " + QDate::currentDate().toString("yyyy-MM-dd") + " " + QTime::currentTime().toString("H:mm:ss.zzz") + "<br/>";
     html += "<code>" + text + "</code><hr><br/>";
     addHtmlText(html);
     
     int result;
     QString response = _plugin->pluginDataExchange(text, result);
     html = "<b>" + _plugin->name() + ":</b> " + QDate::currentDate().toString("yyyy-MM-dd") + " " + QTime::currentTime().toString("H:mm:ss.zzz") + "<br/>";
     
     if(result == DEV_PLUGIN_RESULT_CODE_SUCCESS)
          html += "<code>" + response + "</code><hr>";
     if(result == DEV_PLUGIN_RESULT_CODE_ERROR)
          html += "<font color=\"#FF0000\">Error: " + _plugin->lastError() + "</font><hr>";
     addHtmlText(html);
}
// -----------------------------------------------------------------------------

// #brief Adds the given text to the dialog area
//
// #param __htmlText - html formated text that should be added to the view
// #return No return value.
void PluginConsole_ui::addHtmlText(QString __htmlText)
{
     QTextCursor tc = textEdit->textCursor();
     tc.movePosition(QTextCursor::End);
     textEdit->setTextCursor(tc);
     //textEdit->insertPlainText(__htmlText);
     textEdit->insertHtml(__htmlText);
     
     // scrolling to the end of the document
     textEdit->verticalScrollBar()->setValue(textEdit->verticalScrollBar()->maximum());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when cancel key is pressed.
//
// #return No return value.
void PluginConsole_ui::closeClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when clear key is pressed.
//
// #return No return value.
void PluginConsole_ui::clearClick(void)
{
     textEdit->clear();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void PluginConsole_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               sendClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
