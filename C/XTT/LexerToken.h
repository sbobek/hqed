#ifndef LEXERTOKEN_H
#define LEXERTOKEN_H

#include <QString>

enum LexerTokenType
{
    LTT_UNDEFINED,
    LTT_IDENTIFIER_TOKEN,
    LTT_INTEGER_TOKEN,
    LTT_NUMERIC_TOKEN,
    LTT_OPERATOR,
    LTT_SEPARATOR,
    LTT_ROUND_PARENTHESIS_OPENING,
    LTT_ROUND_PARENTHESIS_CLOSING,
    LTT_RANGE_PARENTHESIS_OPENING,
    LTT_RANGE_PARENTHESIS_CLOSING,
    LTT_SET_PARENTHESIS_OPENING,
    LTT_SET_PARENTHESIS_CLOSING,
    LTT_QUOTATION_TOKEN
};


class LexerToken
{
public:
    LexerToken()
        : m_Type(LTT_UNDEFINED), m_IndexInString(-1)
    {}

    LexerToken(const LexerTokenType type, const QString &text, const int index)
        : m_Type(type), m_Text(text), m_IndexInString(index)
    {}

    LexerTokenType type() const
    {
        return m_Type;
    }

    QString text() const
    {
        return m_Text;
    }

    int indexInString() const
    {
        return m_IndexInString;
    }

    int length() const
    {
        return m_Text.length();
    }

private:
    LexerTokenType m_Type;
    QString m_Text;
    int m_IndexInString;
};


#endif // LEXERTOKEN_H
