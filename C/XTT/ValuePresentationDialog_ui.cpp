/*
 *	  $Id: ValuePresentationDialog_ui.cpp,v 1.6 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>
#include <QTabWidget>

#include "../../M/hType.h"

#include "../../namespaces/ns_hqed.h"
#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"
#include "../Settings_ui.h"
#include "ValuePresentationScriptEditor_ui.h"
#include "ValuePresentationDialog_ui.h"
// -----------------------------------------------------------------------------

ValuePresentationDialog_ui* ValuePresentationDialog;
// -----------------------------------------------------------------------------

// #brief Constructor TextEditor_ui class.
//
// #param *parent - Pointer to parent.
ValuePresentationDialog_ui::ValuePresentationDialog_ui(QWidget* /*parent*/)
{
     initWindow();
}
// -----------------------------------------------------------------------------

// #brief Destructore TextEditor_ui class.
ValuePresentationDialog_ui::~ValuePresentationDialog_ui(void)
{
     delete sw0p0Layout;
     delete sw0p1Layout;
     delete sw0p2Layout;
     delete gb1Layout;
     delete gb2Layout;
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #return no values returns
void ValuePresentationDialog_ui::initWindow(void)
{
     setupUi(this);
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);
     
     toolButton->setIcon(QIcon(":/all_images/images/reload.png"));
     toolButton_2->setIcon(QIcon(":/all_images/images/Delete.png"));
     toolButton_3->setIcon(QIcon(":/all_images/images/texteditor_small.png"));
     toolButton_4->setIcon(QIcon(":/all_images/images/reload.png"));
     toolButton_5->setIcon(QIcon(":/all_images/images/reload.png"));
     
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(sourceIndexChange(int)));
     connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(onPluginSelectionChanged(int)));
     connect(toolButton, SIGNAL(clicked()), this, SLOT(refreshScriptList()));
     connect(toolButton_4, SIGNAL(clicked()), this, SLOT(refreshPluginList()));
     connect(toolButton_5, SIGNAL(clicked()), this, SLOT(forceRefreshPluginServices()));
     connect(toolButton_2, SIGNAL(clicked()), this, SLOT(deleteScript()));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(okClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(createNewScript()));
     connect(toolButton_3, SIGNAL(clicked()), this, SLOT(editScript()));
     
     _type = NULL;
     layoutForm();
     sourceIndexChange(0);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the mode of the dialog
//
// #param __type - pointer to the edited type
// #return window mode result type
int ValuePresentationDialog_ui::setMode(hType* __type)
{
     if(__type == NULL)
          return SET_WINDOW_MODE_FAIL;
     
     _type = __type;
     updateDialog();
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function that updates the GUI according to window mode
//
// #return no values return
void ValuePresentationDialog_ui::updateDialog(void)
{
     int psrcid = _type->presentationSourceId();
     QString psrcname = _type->presentationSourceName();
     QString psrctype = _type->presentationSourceType();

     if(psrcid == TYPE_PRESENTATION_ORIGIN)
          comboBox->setCurrentIndex(0);
     else if(psrcid == TYPE_PRESENTATION_ECMA)
     {
          comboBox->setCurrentIndex(1);
          
          // searching for script file
          int fi = comboBox_2->findText(psrcname);
          if(fi == -1)
          {
               QMessageBox::critical(this, "HQEd", "The script file \'" + psrcname + "\' cannot be found.", QMessageBox::Ok);
               return;
          }
          comboBox_2->setCurrentIndex(fi);
     }
     else if(psrcid == TYPE_PRESENTATION_DEVPC)
     {
          comboBox->setCurrentIndex(2);
          
          // searching for appropriate plugin
          int pi = comboBox_3->findText(psrcname);
          if(pi == -1)
          {
               QMessageBox::critical(this, "HQEd", "The plugin \'" + psrcname + "\' cannot be found.", QMessageBox::Ok);
               return;
          }
          comboBox_3->setCurrentIndex(pi);
          
          // searching for appropriate service
          int si = comboBox_4->findText(psrctype);
          if(si == -1)
          {
               QMessageBox::critical(this, "HQEd", "The plugin \'" + psrcname + "\' cannot be found.", QMessageBox::Ok);
               return;
          }
          comboBox_4->setCurrentIndex(si);
     }
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void ValuePresentationDialog_ui::layoutForm(void)
{
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   1, 0, 1, 3);
     formLayout->addWidget(pushButton,   2, 1);
     formLayout->addWidget(pushButton_2, 2, 2);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gb1Layout = new QGridLayout;
     gb1Layout->addWidget(comboBox, 0, 0);
     hqed::setupLayout(gb1Layout);
     groupBox->setLayout(gb1Layout);
     
     gb2Layout = new QGridLayout;
     gb2Layout->addWidget(stackedWidget, 0, 0);
     hqed::setupLayout(gb2Layout);
     groupBox_2->setLayout(gb2Layout);
     
     sw0p0Layout = new QGridLayout;
     sw0p0Layout->addWidget(label, 0, 0);
     hqed::setupLayout(sw0p0Layout);
     stackedWidget->widget(0)->setLayout(sw0p0Layout);
     
     sw0p1Layout = new QGridLayout;
     sw0p1Layout->addWidget(label_2,      0, 0);
     sw0p1Layout->addWidget(comboBox_2,   1, 0, 1, 3);
     sw0p1Layout->addWidget(toolButton,   1, 3);
     sw0p1Layout->addWidget(toolButton_3, 1, 4);
     sw0p1Layout->addWidget(toolButton_2, 1, 5);
     sw0p1Layout->addWidget(label_3,      2, 0);
     sw0p1Layout->addWidget(pushButton_3, 2, 1);
     sw0p1Layout->setRowStretch(3, 1);
     sw0p1Layout->setColumnStretch(2, 1);
     sw0p1Layout->setSpacing(4);
     hqed::setupLayout(sw0p1Layout);
     stackedWidget->widget(1)->setLayout(sw0p1Layout);
     
     sw0p2Layout = new QGridLayout;
     sw0p2Layout->addWidget(label_4,      0, 0);
     sw0p2Layout->addWidget(comboBox_3,   1, 0);
     sw0p2Layout->addWidget(toolButton_4, 1, 1);
     sw0p2Layout->addWidget(label_5,      2, 0);
     sw0p2Layout->addWidget(comboBox_4,   3, 0);
     sw0p2Layout->addWidget(toolButton_5, 3, 1);
     sw0p2Layout->setRowStretch(4, 1);
     sw0p2Layout->setColumnStretch(0, 1);
     hqed::setupLayout(sw0p2Layout);
     stackedWidget->widget(2)->setLayout(sw0p2Layout);
}
// -----------------------------------------------------------------------------

// #brief Function that lists the script files in the ecmascript directory
//
// #return No return value.
void ValuePresentationDialog_ui::listScripts(void)
{
     QString ecmadir = Settings->ecmascriptPath();
     QDir di(ecmadir);
     QFileInfoList il = di.entryInfoList(QStringList() << "*.esp", QDir::Files | QDir::Readable, QDir::Name);
     
     comboBox_2->clear();
     for(int i=0;i<il.size();++i)
          comboBox_2->addItem(il.at(i).fileName());
}
// -----------------------------------------------------------------------------

// #brief Function that lists the pligins that supports the presentation module of the protocol
//
// #return No return value.
void ValuePresentationDialog_ui::listPlugins(void)
{    
     QList<devPlugin*>* pllist = devPC->pluginsFilter(DEV_PLUGIN_SUPPORTED_MODULES_P, false);
     comboBox_3->clear();
     for(int i=0;i<pllist->size();++i)
          comboBox_3->addItem(pllist->at(i)->name());
          
     delete pllist;
}
// -----------------------------------------------------------------------------

// #brief Function that lists the presentation pligins services names
//
// #return No return value.
void ValuePresentationDialog_ui::listPluginsServices(void)
{
     QString plname = comboBox_3->currentText();
     if(plname.isEmpty())
          return;
          
     int plindex = devPC->indexOfName(plname);
     if(plindex == -1)
          return;
          
     devPlugin* plugin = devPC->plugins()->at(plindex);
     comboBox_4->clear();
     comboBox_4->addItems(plugin->presentationServices());
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes list of the scripts
//
// #return No return value.
void ValuePresentationDialog_ui::refreshScriptList(void)
{
     QString cn = comboBox_2->currentText();
     listScripts();
     int ci = comboBox_2->findText(cn);
     comboBox_2->setCurrentIndex(ci);
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes list of the plugins
//
// #return No return value.
void ValuePresentationDialog_ui::refreshPluginList(void)
{
     QString cn = comboBox_3->currentText();
     listPlugins();
     int ci = comboBox_3->findText(cn);
     comboBox_3->setCurrentIndex(ci);
}
// -----------------------------------------------------------------------------

// #brief Function that refreshes list of the plugins services
//
// #return No return value.
void ValuePresentationDialog_ui::forceRefreshPluginServices(void)
{
     QString plname = comboBox_3->currentText();
     if(plname.isEmpty())
          return;
          
     int plindex = devPC->indexOfName(plname);
     if(plindex == -1)
          return;
          
     int result;
     devPlugin* plugin = devPC->plugins()->at(plindex);
     if(!plugin->hello(result))
     {
          QMessageBox::critical(this, "HQEd", "Error while retrieving service list.\nSee the plugin event listener for more information.", QMessageBox::Ok);
          return;
     }
     
     comboBox_4->clear();
     comboBox_4->addItems(plugin->presentationServices());
}
// -----------------------------------------------------------------------------

// #brief Function that removes selected script file
//
// #return No return value.
void ValuePresentationDialog_ui::deleteScript(void)
{
     int ci = comboBox_2->currentIndex();
     if(ci == -1)
          return;
          
     QString filename = comboBox_2->itemText(ci);
     filename = Settings->ecmascriptPath() + "/" + filename;
     
     // checking if the file exists
     if(!QFile::exists(filename))
     {
          QMessageBox::critical(NULL, "HQEd", "Selected file \'" + filename + "\' does not exists.", QMessageBox::Ok);
          return;
     }
     
     // confirmation of file removing
     if(QMessageBox::warning(NULL, "HQEd", "Do you really want to remove file \'" + filename + "\'?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
         
     // file removing
     if(QFile::remove(filename))
          QMessageBox::information(NULL, "HQEd", "File \'" + filename + "\' remove successfully.", QMessageBox::Ok);
     else
          QMessageBox::critical(NULL, "HQEd", "Error while removing file \'" + filename + "\'.", QMessageBox::Ok);
     
     // refreshing file list
     listScripts();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user changes the source of presentation service
//
// #param __index - index of the new source
// #return No return value.
void ValuePresentationDialog_ui::sourceIndexChange(int __index)
{
#if QT_VERSION >= 0x040300

     if(__index == 1)
          listScripts();
     if(__index == 2)
          listPlugins();

#else

     if(__index == 1)
     {
          QMessageBox::information(this, "HQEd", "To use ECMA scripts presentation method, you need install Qt version 4.3 or higher.", QMessageBox::Ok);
          comboBox->setCurrentIndex(0);
          return;
     }

#endif

     stackedWidget->setCurrentIndex(__index);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user changes the plugin selection
//
// #param __index - index of the selected plugin
// #return No return value.
void ValuePresentationDialog_ui::onPluginSelectionChanged(int /*__index*/)
{
     listPluginsServices();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when ok button is pressed
//
// #return No return value.
void ValuePresentationDialog_ui::okClick(void)
{
     // when translation device has not been not selected
     int trdev = comboBox->currentIndex();
     if(trdev == -1)
     {
          QMessageBox::critical(this, "HQEd", "Select the translation device.", QMessageBox::Ok);
          return;
     }
     
     // setting the type parameters
     int lut[] = {TYPE_PRESENTATION_ORIGIN, TYPE_PRESENTATION_ECMA, TYPE_PRESENTATION_DEVPC};
     int psrcid = lut[trdev];
     QString psrcname = "";
     QString psrctype = "";
     
     // when ECMA script
     if(psrcid == TYPE_PRESENTATION_ECMA)
     {
          int trdevidx = comboBox_2->currentIndex();
          if(trdevidx == -1)
          {
               QMessageBox::critical(this, "HQEd", "Select a name of the translation source.", QMessageBox::Ok);
               return;
          }
          psrcname = comboBox_2->itemText(trdevidx);
     }
     
     // when DEVPC
     if(psrcid == TYPE_PRESENTATION_DEVPC)
     {
          int trdevidx = comboBox_3->currentIndex();
          int trdevtpe = comboBox_4->currentIndex();
          if(trdevidx == -1)
          {
               QMessageBox::critical(this, "HQEd", "Select a name of the translation source.", QMessageBox::Ok);
               return;
          }
          if(trdevtpe == -1)
          {
               QMessageBox::critical(this, "HQEd", "Select a name of the translation service.", QMessageBox::Ok);
               return;
          }
          
          psrcname = comboBox_3->itemText(trdevidx);
          psrctype = comboBox_4->itemText(trdevtpe);
     }
     
     // settings applying
     _type->setPresentationSourceId(psrcid);
     _type->setPresentationSourceName(psrcname);
     _type->setPresentationSourceType(psrctype);

     close();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when close button is pressed
//
// #return No return value.
void ValuePresentationDialog_ui::closeClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when create new script button is pressed
//
// #return No return value.
void ValuePresentationDialog_ui::createNewScript(void)
{
     delete ValuePresentationScriptEditor;
     ValuePresentationScriptEditor = new ValuePresentationScriptEditor_ui;
     connect(ValuePresentationScriptEditor, SIGNAL(onclose()), this, SLOT(refreshScriptList()));
     if(ValuePresentationScriptEditor->setMode(true, "") != SET_WINDOW_MODE_OK_YOU_CAN_SHOW)
     {
          QMessageBox::critical(this, "HQEd", "Script Editor dialog has entered into incorrect mode.", QMessageBox::Ok);
          return;
     }
     ValuePresentationScriptEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when edit script button is pressed
//
// #return No return value.
void ValuePresentationDialog_ui::editScript(void)
{
     int ci = comboBox_2->currentIndex();
     if(ci == -1)
          return;
     QString filename = comboBox_2->itemText(ci);
     filename = Settings->ecmascriptPath() + "/" + filename;
     
     delete ValuePresentationScriptEditor;
     ValuePresentationScriptEditor = new ValuePresentationScriptEditor_ui;
     if(ValuePresentationScriptEditor->setMode(false, filename) != SET_WINDOW_MODE_OK_YOU_CAN_SHOW)
     {
          QMessageBox::critical(this, "HQEd", "Script Editor dialog has entered into incorrect mode.", QMessageBox::Ok);
          return;
     }
     ValuePresentationScriptEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ValuePresentationDialog_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               okClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
