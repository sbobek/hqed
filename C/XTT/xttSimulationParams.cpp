/*
 *	  $Id: xttSimulationParams.cpp,v 1.2 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QHostAddress>
#include <QFile>
#include <QBuffer>

#include "../Settings_ui.h"

#include "../../namespaces/ns_hqed.h"
#include "../../M/hModel.h"
#include "../../M/hModelList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Statesgroup.h"

#include "../devPluginInterface/devPluginController.h"
#include "SimulationEditor_ui.h"
#include "xttSimulationParams.h"
//---------------------------------------------------------------------------

// #brief Construcotr of the xttSimulationParams class
xttSimulationParams::xttSimulationParams(void)
{
     defaultInit();
}
//---------------------------------------------------------------------------

// #brief Construcotr of the xttSimulationParams class
//
// #param __initialState - initial state of the simulation
xttSimulationParams::xttSimulationParams(XTT_State* __initialState)
{
     defaultInit();
     
     _usedState = __initialState;
     if(_usedState != NULL)
          _simInitState = __initialState->fullname();
}
//---------------------------------------------------------------------------

// #brief Destructor of the xttSimulationParams class
xttSimulationParams::~xttSimulationParams(void)
{
     // As far as I remember I do nothing!!!
}
//---------------------------------------------------------------------------

// #brief Function that initializes the filed of the class with default values
//
// #return no values return
void xttSimulationParams::defaultInit(void)
{
     _modelname = hqed::modelList()->activeModel()->modelID();
     _username = devPC->instanceIdentifier();
     _simtype = DEV_PLUGIN_SIM_TYPE_DDI;
     _simInitState = "";
     _usedState = NULL;
     
     defineDefaultSetOfTables();
}
//---------------------------------------------------------------------------

// #brief Function that defines the default set of tables according to the simulation type
//
// #return no values returns
void xttSimulationParams::defineDefaultSetOfTables(void)
{
     _tables.clear();
     
     if(_simtype == DEV_PLUGIN_SIM_TYPE_DDI)
     {
          QList<XTT_Table*>* to = TableList->inputTables();
          for(int i=0;i<to->size();++i)
               _tables.append(hqed::mcwp(to->at(i)->Title()));
          delete to;
     }
     
     if(_simtype == DEV_PLUGIN_SIM_TYPE_GDI)
     {
          QList<XTT_Table*>* to = TableList->rootsTables();
          for(int i=0;i<to->size();++i)
               _tables.append(hqed::mcwp(to->at(i)->Title()));
          delete to;
     }
     
     if(_simtype == DEV_PLUGIN_SIM_TYPE_TDI)
     {
          QList<XTT_Table*>* to = TableList->rootsTables();
          for(int i=0;i<to->size();++i)
               _tables.append(hqed::mcwp(to->at(i)->Title()));
          delete to;
     }
}
//---------------------------------------------------------------------------

// #brief Function that retrieves data about simulation from the simulation editor
//
// #return no values return
void xttSimulationParams::getDataFromSimulationEditor(SimulationEditor_ui* __simeditor)
{
     _tables = __simeditor->simTables();
     _simtype = __simeditor->simType();
     _simInitState = __simeditor->simState();
     _usedState = __simeditor->statePtr();
}
//---------------------------------------------------------------------------

// #brief Function that sets the pointer of the state from which the simulation starts. The NULL pointer indicates that the simulation will start from manualy defined state.
//
// #param __state - the pointer to the state from which the simulation will start
// #return no values returns
void xttSimulationParams::setUsedState(XTT_State* __state)
{
     _usedState = __state;
}
//---------------------------------------------------------------------------

// #brief Function that sets a name of the model
//
// #param __modelname - a name of the model
// #return no values returns
void xttSimulationParams::setModelName(QString __modelname)
{
     _modelname = __modelname;
}
//---------------------------------------------------------------------------

// #brief Function that sets a name of the user/client
//
// #param __uname - a name of the user/client
// #return no values returns
void xttSimulationParams::setUserName(QString __uname)
{
     _username = __uname;
}
//---------------------------------------------------------------------------

// #brief Function that sets a protocol representation of the initial state for the simulation
//
// #param __initstate - protocol representation of the initial state for the simulation
// #return no values returns
void xttSimulationParams::setInitialState(QString __initstate)
{
     _simInitState = __initstate;
}
//---------------------------------------------------------------------------

// #brief Function that sets a list and order of the tables
//
// #param __tables - a list and order of the tables
// #return no values returns
void xttSimulationParams::setTables(QStringList __tables)
{
     _tables = __tables;
}
//---------------------------------------------------------------------------

// #brief Function that sets a type of the simulation
//
// #param __simulationtype - a type of the simulation
// #return no values returns
void xttSimulationParams::setSimulationType(int __simulationtype)
{
     _simtype = __simulationtype;
}
//---------------------------------------------------------------------------

// #brief Function that returns the pointer to the state from which the simulation starts. The NULL pointer indicates that the simulation has started from manualy defined state.
//
// #return the pointer to the state from which the simulation starts
XTT_State* xttSimulationParams::usedState(void)
{
     return _usedState;
}
//---------------------------------------------------------------------------

// #brief Function that returns a name of the model
//
// #return a name of the model
QString xttSimulationParams::modelName(void)
{
     return _modelname;
}
//---------------------------------------------------------------------------

// #brief Function that returns a name of the user/client
//
// #return a name of the user/client
QString xttSimulationParams::userName(void)
{
     return _username;
}
//---------------------------------------------------------------------------

// #brief Function that returns protocol representation of the initial state for the simulation
//
// #return protocol representation of the initial state for the simulation
QString xttSimulationParams::initialState(void)
{
     return _simInitState;
}
//---------------------------------------------------------------------------

// #brief Function that returns a list and order of the tables
//
// #return a list and order of the tables
QStringList xttSimulationParams::tables(void)
{
     return _tables;
}
//---------------------------------------------------------------------------

// #brief Function that returns a type of the simulation
//
// #return a type of the simulation
int xttSimulationParams::simulationType(void)
{
     return _simtype;
}
//---------------------------------------------------------------------------

