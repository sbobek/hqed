/**
 * \file	GroupManager_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges group manager window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef GROUPMANAGER_H
#define GROUPMANAGER_H
// -----------------------------------------------------------------------------
 
#include "ui_GroupManager.h"
// -----------------------------------------------------------------------------
/**
* \class 	GroupManager_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges group manager window.
*/
class GroupManager_ui : public QWidget, private Ui_GroupManager
{
    Q_OBJECT

public:

    /// \brief Constructor GroupManager_ui class.
	///
	/// \param parent Pointer to parent object.
	GroupManager_ui(QWidget *parent = 0);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

	/// \brief Function is triggered when tree object is clicked.
	///
	/// \param item Pointer to tree object.
	/// \param col Column of Tree object that was clicked.
	/// \return No return value.
	void TreeClick(QTreeWidgetItem* item, int col);

	/// \brief Function refreshes view of tree.
	///
	/// \return No return value.
	void refreshGroupTree(void);

	/// \brief Function creates and sets new main group window.
	///
	/// \return No return value.
	void CreateNewMainGroup(void);

	/// \brief Function creates and sets new sub group window.
	///
	/// \return No return value.
	void CreateNewSubGroup(void);

	/// \brief Function shows edit group window.
	///
	/// \return No return value.
	void EditGroup(void);

	/// \brief Function deletes group that was chosen.
	///
	/// \return No return value.
	void DeleteGroup(void);
     
	/// \brief Function shows select group window.
	///
	/// \return No return value.
	void SelectNewParent(void);

	/// \brief Function changes parent for the group.
	///
	/// \return No return value.
	void ChangeParent(void);

	/// \brief Function sets group as main group.
	///
	/// \return No return value.
	void SetAsMain(void);

	/// \brief Function is triggered when group manager window is closed, it also refreshes tables view.
	///
	/// \return No return value
	void OnClose(void);
};
// -----------------------------------------------------------------------------

extern GroupManager_ui* GroupMan;
// -----------------------------------------------------------------------------

#endif
