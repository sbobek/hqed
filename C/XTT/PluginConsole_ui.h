/**
 * \file	PluginConsole_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	14.07.2009
 * \version	1.0
 * \brief	This file contains class definition that arranges plugin console window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef PLUGINCONSOLE_H
#define PLUGINCONSOLE_H
// -----------------------------------------------------------------------------

#include <QGridLayout>
 
#include "ui_PluginConsole.h"
// -----------------------------------------------------------------------------

class devPlugin;
// -----------------------------------------------------------------------------
/**
* \class 	PluginConsole_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	14.07.2009
* \brief 	Class definition that arranges plugin console window.
*/
class PluginConsole_ui : public QWidget, private Ui_PluginConsole
{
    Q_OBJECT
    
private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     devPlugin* _plugin;                     ///< Pointer to the plugin that dialog communicates with

public:

     /// \brief Constructor PluginEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	PluginConsole_ui(QWidget *parent = 0);
     
     /// \brief Destructor PluginEditor_ui class.
	~PluginConsole_ui(void);
     
     /// \brief Function that sets the dialog mode
     ///
     /// \param __pluginPtr - pointer to the plugin
     /// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(devPlugin* __pluginPtr);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
 
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Adds the given text to the dialog area
	///
	/// \param __htmlText - html formated text that should be added to the view
	/// \return No return value.
     void addHtmlText(QString __htmlText);

public slots:

     /// \brief Function is triggered when send key is pressed.
	///
	/// \return No return value.
     void sendClick(void);
     
     /// \brief Function is triggered when close key is pressed.
	///
	/// \return No return value.
     void closeClick(void);
     
     /// \brief Function is triggered when clear key is pressed.
	///
	/// \return No return value.
     void clearClick(void);
};
// -----------------------------------------------------------------------------

#endif
