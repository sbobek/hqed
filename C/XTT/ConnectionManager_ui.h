 /**
 * \file	ConnectionManager_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges connection manager window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef CONNECTIONMANAGERH
#define CONNECTIONMANAGERH
// -----------------------------------------------------------------------------
 
#include "ui_ConnectionManager.h"
// -----------------------------------------------------------------------------
/**
* \class 	ConnectionManager_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges connection manager window.
*/
class ConnectionManager_ui : public QWidget, private Ui_ConnectionManager
{
    Q_OBJECT

public:

    /// \brief Constructor ConnectionManager_ui class.
	///
	/// \param parent Pointer to parent object.
	ConnectionManager_ui(QWidget *parent = 0);

    /// \brief Constructor ConnectionManager_ui class.
	///
	/// \param tindex Index of chosen table.
	/// \param parent Pointer to parent object.
	ConnectionManager_ui(int tindex, QWidget *parent = 0);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

	/// \brief Function refreshes the window.
	///
	/// \return No return value.
	void Refresh(void);

	/// \brief Function closes the window.
	///
	/// \return No return value.
	void ClickClose(void);

	/// \brief Function is triggered when table field is changed.
	///
	/// \param new_index Index of the table.
	/// \return No return value.
	void SelectedTableChange(int new_index);

	/// \brief Function is triggered when output connection row is selected.
	///
	/// \param new_index Index of the row.
	/// \return No return value.
	void ListOfOutConnectionClick(int new_index);

	/// \brief Function is triggered when input connection row is selected.
	///
	/// \param new_index Index of the row.
	/// \return No return value.
	void ListOfInConnectionClick(int new_index);

	/// \brief Function is triggered when input connection field is changed.
	///
	/// \param new_index Index of selected combo box.
	/// \return No return value.
	void ComboBoxChange(int new_index);

	/// \brief Function is triggered when output connection field is changed.
	///
	/// \param new_index Index of selected combo box.
	/// \return No return value.
	void ComboBoxChange_2(int new_index);

	/// \brief Function is triggered when button 'Add new connection' is clicked.
	///
	/// \return No return value.
	void AddNewConnection(void);

	/// \brief Function is triggered when button 'Edit connection' is pressed - input connection.
	///
	/// \return No return value.
	void EditInConnection(void);

	/// \brief Function is triggered when button 'Edit connection' is pressed - output connection.
	///
	/// \return No return value.
	void EditOutConnection(void);

	/// \brief Function is triggered when button 'Delete connection' is pressed - input connection.
	///
	/// \return No return value.
	void DeleteInConnection(void);

	/// \brief Function is triggered when button 'Delete connection' is pressed - output connection.
	///
	/// \return No return value.
	void DeleteOutConnection(void);
};
// -----------------------------------------------------------------------------

extern ConnectionManager_ui* ConnManager;
// -----------------------------------------------------------------------------

#endif
