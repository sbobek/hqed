/**
 * \file	DeleteTable_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges delete table window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef DELETETABLE_H
#define DELETETABLE_H
// -----------------------------------------------------------------------------
 
#include "ui_DeleteTable.h"
// -----------------------------------------------------------------------------
/**
* \class 	DeleteTable_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges delete table window.
*/
class DeleteTable_ui : public QWidget, private Ui_DeleteTable
{
    Q_OBJECT

public:

    /// \brief Constructor DeleteTable_ui class.
	///
	/// \param parent Pointer to parent object.
	DeleteTable_ui(QWidget *parent = 0);

    /// \brief Constructor DeleteTable_ui class.
	///
	/// \param tableIndex Index of table you want to delete.
	/// \param parent Pointer to parent object.
	DeleteTable_ui(int tableIndex, QWidget *parent = 0);

	/// \brief Function sets window and reads information about tables.
	///
	/// \return No return value.
	void SetWindow(void);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

	/// \brief Function is triggered when table is deleted - it also updates window.
	///.
	/// \return No return value.
	void ClickDelete(void);
};
// -----------------------------------------------------------------------------

extern DeleteTable_ui* DelTable;
// -----------------------------------------------------------------------------

#endif
