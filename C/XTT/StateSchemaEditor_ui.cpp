/*
 *	   $Id: StateSchemaEditor_ui.cpp,v 1.2.4.2 2011-01-14 18:12:59 pheld Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/hSet.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../M/XTT/XTT_Attribute.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../namespaces/ns_hqed.h"

#include "widgets/StateSchemaEditorListWidget.h"
#include "StateSchemaEditor_ui.h"
// -----------------------------------------------------------------------------

StateSchemaEditor_ui* StateSchemaEditor;
// -----------------------------------------------------------------------------

// #brief Constructor StateSchemaEditor_ui class.
//
// #param parent Pointer to parent object.
StateSchemaEditor_ui::StateSchemaEditor_ui(QWidget*)
{
    initWindow();
}
// -----------------------------------------------------------------------------

// #brief Constructor StateSchemaEditor_ui class.
//
// #param __group - pointer to the astates group that is edited
// #param __groupname - the name of the group
// #param parent - Pointer to parent object.
StateSchemaEditor_ui::StateSchemaEditor_ui(XTT_Statesgroup* __group, QString __groupname, QWidget* /*parent*/)
{
     initWindow();
     
     _stsGroup = __group;
     _stsGroupName = __groupname;
     _isNew = false;
     
     readStateSetup();
}
// -----------------------------------------------------------------------------

// #brief Destructor StateSchemaEditor_ui class.
StateSchemaEditor_ui::~StateSchemaEditor_ui(void)
{
     delete gb1Layout;
     delete gb2Layout;
     delete gb3Layout;
     delete gb4Layout;
     delete formLayout;
     
     delete _allAtts;
     delete _stsAtts;
}
// -----------------------------------------------------------------------------

// #brief Function that initializes the window
//
// #return no values return
void StateSchemaEditor_ui::initWindow(void)
{
    _allAtts = new QList<XTT_Attribute*>;
    _stsAtts = new QList<XTT_Attribute*>;
    (*_allAtts) = AttributeGroups->classFilter(-1);

    setupUi(this);
    allAtts = new StateSchemaEditorListWidget(_allAtts, groupBox);
    stsAtts = new StateSchemaEditorListWidget(_stsAtts, groupBox_2);
    
    connect(allAtts, SIGNAL(elementsMoved(QList<int>*)), this, SLOT(onAttsRemoveFromState(QList<int>*)));
    connect(stsAtts, SIGNAL(elementsMoved(QList<int>*)), this, SLOT(onAttsAddToState(QList<int>*)));
    connect(toolButton, SIGNAL(clicked()), this, SLOT(onButtonAttsAddToStateClick()));
    connect(toolButton_2, SIGNAL(clicked()), this, SLOT(onButtonAttsRemoveFromStateClick()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(okClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(cancelClick()));
    connect(checkBox, SIGNAL(clicked()), this, SLOT(filterChanged()));
    connect(checkBox_2, SIGNAL(clicked()), this, SLOT(filterChanged()));
    connect(checkBox_3, SIGNAL(clicked()), this, SLOT(filterChanged()));
    
    setWindowModality(Qt::ApplicationModal);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    
    toolButton->setIcon(QIcon(":/all_images/images/rightarrow.png"));
    toolButton_2->setIcon(QIcon(":/all_images/images/leftarrow.png"));
    
    checkBox->setChecked(true);
    checkBox_2->setChecked(true);
    checkBox_3->setChecked(true);

    layoutWindow();
    
    _stsGroup = NULL;
    _isNew = true;
}
// -----------------------------------------------------------------------------

// #brief Function that layouts the window
//
// #return no values return
void StateSchemaEditor_ui::layoutWindow(void)
{    
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox_3,   0, 0, 1, 5);
     formLayout->addWidget(groupBox_4,   1, 0, 1, 5);
     formLayout->addWidget(groupBox,     2, 0, 4, 1);
     formLayout->addWidget(groupBox_2,   2, 2, 4, 3);
     formLayout->addWidget(toolButton,   3, 1);
     formLayout->addWidget(toolButton_2, 4, 1);
     formLayout->addWidget(pushButton,   6, 3);
     formLayout->addWidget(pushButton_2, 6, 4);
     formLayout->setColumnStretch(0, 1);
     formLayout->setColumnStretch(2, 1);
     formLayout->setRowStretch(2, 1);
     formLayout->setRowStretch(5, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gb1Layout = new QGridLayout;
     gb1Layout->addWidget(allAtts, 0, 0);
     hqed::setupLayout(gb1Layout);
     groupBox->setLayout(gb1Layout);
     
     gb2Layout = new QGridLayout;
     gb2Layout->addWidget(stsAtts, 0, 0);
     hqed::setupLayout(gb2Layout);
     groupBox_2->setLayout(gb2Layout);
     
     gb4Layout = new QGridLayout;
     gb4Layout->addWidget(lineEdit, 0, 0);
     hqed::setupLayout(gb4Layout);
     groupBox_4->setLayout(gb4Layout);
     
     gb3Layout = new QGridLayout;
     gb3Layout->addWidget(label,      0, 0);
     gb3Layout->addWidget(checkBox,   0, 1);
     gb3Layout->addWidget(checkBox_2, 0, 2);
     gb3Layout->addWidget(checkBox_3, 0, 3);
     gb3Layout->setColumnStretch(3, 1);
     hqed::setupLayout(gb3Layout);
     groupBox_3->setLayout(gb3Layout);
}
// -----------------------------------------------------------------------------

// #brief The function that sets the window according to the edited state up.
//
// #return no value return
void StateSchemaEditor_ui::readStateSetup(void)
{
     lineEdit->setText(_stsGroupName);
     if(_stsGroup != NULL)
     {
          QList<XTT_Attribute*>* stateAtts = _stsGroup->inputAttributes();
          for(int i=0;i<stateAtts->size();++i)
          {
               int io = _allAtts->indexOf(stateAtts->at(i));
               if(io > -1)
               {
                    _allAtts->takeAt(io);
                    _stsAtts->append(stateAtts->at(i));
               }
          }
          delete stateAtts;
     }
     
     allAtts->showList();
     stsAtts->showList();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user adds the attributes to the state schema
//
// #param __items - pointer to the list of indexes of the list items
// #return no values return
void StateSchemaEditor_ui::onAttsAddToState(QList<int>* __items)
{
     for(int i=__items->size()-1;i>=0;--i)
          _stsAtts->append(_allAtts->takeAt(__items->at(i)));
     
     allAtts->showList();
     stsAtts->showList();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user removes some attributes from the state schema
//
// #param __items - pointer to the list of indexes of the list items
// #return no values return
void StateSchemaEditor_ui::onAttsRemoveFromState(QList<int>* __items)
{
     for(int i=__items->size()-1;i>=0;--i)
          _allAtts->append(_stsAtts->takeAt(__items->at(i)));
          
     allAtts->showList();
     stsAtts->showList();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user click button that adds the attributes to the state schema
//
// #return no values return
void StateSchemaEditor_ui::onButtonAttsAddToStateClick(void)
{
     QList<int>* chosen = allAtts->chosen();
     onAttsAddToState(chosen);
     delete chosen;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user click button that removes some attributes from the state schema
//
// #return no values return
void StateSchemaEditor_ui::onButtonAttsRemoveFromStateClick(void)
{
     QList<int>* chosen = stsAtts->chosen();
     onAttsRemoveFromState(chosen);
     delete chosen;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when filter was changed by user 
//
// #return no values return
void StateSchemaEditor_ui::filterChanged(void)
{
     QList<int> newfilter;
     if(checkBox->isChecked())
          newfilter << XTT_ATT_CLASS_RO;
     if(checkBox_2->isChecked())
          newfilter << XTT_ATT_CLASS_RW;
     if(checkBox_3->isChecked())
          newfilter << XTT_ATT_CLASS_WO;
     
     stsAtts->setFilter(newfilter);
     allAtts->setFilter(newfilter);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void StateSchemaEditor_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               cancelClick();
               break;

          case Qt::Key_Return:
               okClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user closes the dialog with cancel button
//
// #return no values return
void StateSchemaEditor_ui::cancelClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user closes the dialog with ok button
//
// #return no values return
void StateSchemaEditor_ui::okClick(void)
{
     QString stsname = lineEdit->text();
     if(_isNew)
          _stsGroup = new XTT_Statesgroup(States, false);
     
     // Checking of the group is not already exist
     bool contains = States->contains(stsname);
     if((_isNew) && (contains))
     {
          QMessageBox::warning(NULL, "HQEd", "The \'" + stsname + "\' state group already exists.\nPlease enter an different name.", QMessageBox::Ok);
          return;
     }
     if((!_isNew) && (contains) && (_stsGroup != (*States)[stsname]))
     {
          QMessageBox::warning(NULL, "HQEd", "The \'" + stsname + "\' state group already exists.\nPlease enter an different name.", QMessageBox::Ok);
          return;
     }
     
     // Checking if there is at least one attribute in the schema
     if(_stsAtts->empty())
     {
          QMessageBox::warning(NULL, "HQEd", "The schema of state group should contain at least one attribute.", QMessageBox::Ok);
          return;
     }
     
     // ok when everything is ok:
     
     // name changing
     if(!_isNew)
          States->remove(_stsGroupName);
     (*States)[stsname] = _stsGroup;
     
     // When we create a new group we must also create a default state
     if(_isNew)
          _stsGroup->createdefaultState();
          
     // Refreshing attribute list in the states:
     QList<XTT_Attribute*>* stateAtts = _stsGroup->inputAttributes();
     
     // - adding new attributes
     for(int i=0;i<_stsAtts->size();++i)
          if(!stateAtts->contains(_stsAtts->at(i)))
          {
               hSet* newvalue = hSet::makeNotDefinedValue(_stsAtts->at(i)->Type());
               _stsGroup->resetInputValueTo(_stsAtts->at(i), newvalue);
               delete newvalue;
          }
               
     // - removing the old ones
     for(int i=0;i<stateAtts->size();++i)
          if(!_stsAtts->contains(stateAtts->at(i)))
               _stsGroup->removeInputAttribute(stateAtts->at(i));
               
     delete stateAtts;
     
     // Refreeshing UI
     States->refreshStatesUI();
          
     emit okClicked();
     close();
}
// -----------------------------------------------------------------------------

