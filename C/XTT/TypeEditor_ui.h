/**
 * \file	TypeEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.15
 * \brief	This file contains class definition that arranges type editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef TypeEditorH
#define TypeEditorH
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_TypeEditor.h"
// -----------------------------------------------------------------------------

class hType;
class SetEditor_ui;
// -----------------------------------------------------------------------------

/**
* \class 	TypeEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007
* \brief 	Class definition that arranges type editor window.
*/
class TypeEditor_ui : public QWidget, private Ui_TypeEditor
{
    Q_OBJECT
    
    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gp1Layout;	               ///< Pointer to groupBox1 grid layout.
    QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
    QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
    QGridLayout* gp4Layout;	               ///< Pointer to groupBox4 grid layout.
    QGridLayout* gp5Layout;	               ///< Pointer to groupBox5 grid layout.
    QGridLayout* gp6Layout;	               ///< Pointer to groupBox6 grid layout.
    QGridLayout* no_constr_page_layout;	     ///< Pointer to no_constraints_page grid layout.
    QGridLayout* numeric_page_layout;	     ///< Pointer to numeric_page grid layout.
    QGridLayout* symbolic_page_layout;	     ///< Pointer to symbolic_page grid layout.
    
    SetEditor_ui* sewin;                     ///< Poiter to the set editor window
    hType* winCurrentTypePtr;                ///< Poiter to the type that is sets up with the help dialog
    
    // The window parameters
    bool _isNew;                             ///< Denotes if the edited type is new  
    int _edit_type_index;                    ///< The index number of edited type - when isNew == false
    int _base_type_id;                       ///< The base type of new type - isNew == true
     
public:

     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param parent Pointer to parent object.
     TypeEditor_ui(QWidget *parent = 0);
     
     
     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param __isNew - Denotes if the edited type is new
     /// \param __edit_type_index - The index number of edited type - when isNew == false
     /// \param __base_type_id - The base type of new type - isNew == true
     /// \param parent Pointer to parent object.
     TypeEditor_ui(bool __isNew, int __edit_type_index, int __base_type_id, QWidget *parent = 0);
     
     /// \brief Destructor ValueEdit_ui class.
     ~TypeEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function sets the form layout
     ///
     /// \param __primitiveType - identifier of primitive type
     /// \param __contraintType - identifier of constraint type
     /// \return No return value.
     void setUpPages(int __primitiveType, int __contraintType);
     
     /// \brief Function that stores the current type that is set up by using this dialog in winCurrentTypePtr field
	///
	/// \return true if all its ok, otherwise false
     bool createCurrentWinType(void);
     
     /// \brief Function that displays the domain
	///
	/// \return No value return.
     void displayDomain(void);
     
     /// \brief Function that sets the window apperance according to given type
     ///
     /// \param __isNew - Denotes if the edited type is new
     /// \param __edit_type_index - The index number of edited type - when isNew == false
     /// \param __base_type_id - The base type of new type - isNew == true
     /// \return No value return.
     void setWindowMode(bool __isNew, int __edit_type_index, int __base_type_id);

public slots:

     /// \brief Function triggered when Close button is pressed
	///
	/// \return No return value.
     void closeClick(void);
     
     /// \brief Function triggered when presentation button is pressed
	///
	/// \return No return value.
     void presentationClick(void);
     
     /// \brief Function triggered when the name selector is changed
	///
     /// \param __index - the new index of type
	/// \return No return value.
     void typeChanged(int __index);
     
     /// \brief Function triggered when the primitiveType comboBox changed
	///
	/// \return No return value.
     void primitiveTypeChanged(int __index);
     
     /// \brief Function triggered when the constraint type comboBox changed
	///
	/// \return No return value.
     void constraintChanged(int __index);
     
     /// \brief Function triggered when the scale value is changed
	///
	/// \return No return value.
     void scaleChanged(int __newValue);
     
     /// \brief Function triggered when the length value is changed
	///
	/// \return No return value.
     void lengthChanged(int __newValue);
     
     /// \brief Function triggered when the edit domain button is pressed
	///
	/// \return No return value.
     void domainChange(void);
     
     /// \brief Function triggered when the set editor is closed with OK button
	///
	/// \return No return value.
     void domainChanged(void);
     
     /// \brief Function triggered when the save button is pressed
	///
	/// \return No return value.
     void saveClick(void);
     
signals:

     /// \brief The signal that is emited when save button is pressed
	///
	/// \return No return value.
     void saveClicked(void);
};
// -----------------------------------------------------------------------------

extern TypeEditor_ui* TypeEditor;
// -----------------------------------------------------------------------------

#endif
