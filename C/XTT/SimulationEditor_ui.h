/**
 * \file	SimulationEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	20.07.2009
 * \version	1.0
 * \brief	This file contains class definition that arranges simulation params editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef SIMULATIONEDITOR_H
#define SIMULATIONEDITOR_H
// -----------------------------------------------------------------------------

#include <QGridLayout>
 
#include "ui_SimulationParamsEditor.h"
// -----------------------------------------------------------------------------

class hSet;
class XTT_Table;
class XTT_State;
class XTT_Attribute;
class SetEditor_ui;
// -----------------------------------------------------------------------------
/**
* \class 	SimulationEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	20.07.2009
* \brief 	Class definition that arranges simulation params editor window.
*/
class SimulationEditor_ui : public QWidget, private Ui_SimulationParamsEditor
{
    Q_OBJECT
    
private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     QGridLayout* gp1Layout;	               ///< Pointer to groupBox1 grid layout.
     QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
     QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
     QGridLayout* sw1p0layout;	          ///< Pointer to stacketWidget->page0 grid layout.
     QGridLayout* sw1p1layout;	          ///< Pointer to stacketWidget->page1 grid layout.
     
     QList<XTT_Table*>* _allTables;          ///< Pointer to the tables
     QList<XTT_Table*>* _cbTables;           ///< List of tables in comboBox
     QList<XTT_Table*>* _lwTables;           ///< List of tables in listWidget
     
     QList<QPushButton*> _btns;              ///< List of edit buttons
     QList<XTT_Attribute*>* _atts;           ///< List of all attributes
     QList<hSet*>* _sets;                    ///< Pointer to the list of defined values of attributes
     SetEditor_ui* _setEditor;               ///< Pointer to the set editor window dialog
     
     QList<XTT_State*>* _states;             ///< The list of already defined states
     XTT_State* _state;                      ///< Contains the pointer to the state. It points only on the state object if the simulation has been run using already defined state
     
     int _bntIndex;                          ///< Index of the last clicked button
     
public:

     /// \brief Constructor SimulationEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	SimulationEditor_ui(QWidget *parent = 0);
     
     /// \brief Destructor SimulationEditor_ui class.
	~SimulationEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function that sets the mode of the dialog
	///
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(void);
     
     /// \brief Function that updates listWidget and comboBox w.r.t. content of _cbTables and _lwTables
	///
	/// \return no values returns
     void displayTables(void);
     
     /// \brief Function that returns list of tables that has been chosen for simulation
	///
	/// \return list of tables that has been chosen for simulation
     QStringList simTables(void);
     
     /// \brief Function that returns type of the simulation
	///
	/// \return type of the simulation
     int simType(void);
     
     /// \brief Function that returns initial state of the simulation
	///
	/// \return initial state of the simulation as state name or state definition
     QString simState(void);
     
     /// \brief return the pointer to the already defined state, that has been used to run simulation. If state has been defined manually it returns NULL
     ///
     /// \return pointer to the already defined state
     XTT_State* statePtr(void);
     
     /// \brief Function that checks simulation parameters
	///
	/// \return true if everything is correct, otherwise false
     bool verifySimParams(void);

public slots:

     /// \brief Function is triggered when start key is pressed.
	///
	/// \return No return value.
     void startClick(void);
     
     /// \brief Function is triggered when cancel key is pressed.
	///
	/// \return No return value.
     void cancelClick(void);
     
     /// \brief Function is triggered when the user change type of the state definition
	///
	/// \return No return value.
     void onStateDefTypeChanged(void);
     
     /// \brief Function is triggered when the user press add table button
	///
	/// \return No return value.
     void onAddTableClick(void);
     
     /// \brief Function is triggered when the user press remove table button
	///
	/// \return No return value.
     void onDeleteTableClick(void);
     
     /// \brief Function is triggered when the user change the selection of already defined state
	///
     /// \param __stateIndex - index of the state
	/// \return No return value.
     void onStateSelectionChanged(int __stateIndex);
     
     /// \brief Function is triggered when the user clicks edit button
	///
	/// \return No return value.
     void onEditButtonClick(void);
     
     /// \brief Function is triggered when the set editor is closed by ok button
	///
	/// \return No return value.
     void onSetEditorClose(void);
     
signals:

     /// \brief This signal is emited when window is closed with the help of Start button
	///
	/// \return No return value.
     void onStartClick(void);
};
// -----------------------------------------------------------------------------

#endif
