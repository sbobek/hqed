/*
 *	  $Id: SetCreator_ui.cpp,v 1.4.4.3 2010-12-19 23:36:35 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>
#include <QTabWidget>

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hValue.h"
#include "../../M/hDomain.h"
#include "../../M/hSetItem.h"
#include "../../M/hFunction.h"
#include "../../M/hExpression.h"
#include "../../M/hMultipleValue.h"
#include "../../M/hFunctionArgument.h"

#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Attribute.h"

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"
#include "../Settings_ui.h"
#include "widgets/SetItemSelectorWidget.h"
#include "SetCreator_ui.h"
// -----------------------------------------------------------------------------

SetCreator_ui* SetCreator;
// -----------------------------------------------------------------------------

// #brief Constructor TextEditor_ui class.
//
// #param *parent - Pointer to parent.
SetCreator_ui::SetCreator_ui(QWidget* /*parent*/)
{
     initWindow();
}
// -----------------------------------------------------------------------------

// #brief Destructore TextEditor_ui class.
SetCreator_ui::~SetCreator_ui(void)
{
     delete formLayout;
     delete listWidget;
     if(_newSet != NULL)
          delete _newSet;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #return no values returns
void SetCreator_ui::initWindow(void)
{
     setupUi(this);
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     listWidget = new SetItemSelectorWidget(this);
     
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);
     
     connect(pushButton, SIGNAL(clicked()), this, SLOT(okClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(addRangeClick()));
     connect(listWidget, SIGNAL(itemPressed(QListWidgetItem*)), this, SLOT(itemStateChanged(QListWidgetItem*)));
     
     _newSet = NULL;
     layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void SetCreator_ui::layoutForm(void)
{
     resize(5,300);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(label,        0, 0);
     formLayout->addWidget(listWidget,   1, 0, 1, 4);
     formLayout->addWidget(checkBox,     2, 2, 1, 2);
     formLayout->addWidget(pushButton_3, 2, 0);
     formLayout->addWidget(pushButton,   3, 2);
     formLayout->addWidget(pushButton_2, 3, 3);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(1, 1);
     formLayout->setAlignment(checkBox, Qt::AlignRight);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the given parameters are correct
//
// #param __type - pointer to the set type
// #param __set - pointer to the edited set
// #return true if mode is correct other wise false
bool SetCreator_ui::isModeCorrect(hType* __type, hSet* __set)
{
     if(__set == NULL)
          return false;
     if(__type == NULL)
          return false;
     if(__type->typeClass() != TYPE_CLASS_DOMAIN)
          return false;
     if(!__type->domain()->canBeExpanded())
          return false;
     if(__type->isSetCorrect(__set, true) != H_TYPE_ERROR_NO_ERROR)
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function that set the window mode
//
// #param __type - pointer to the set type
// #param __set - pointer to the edited set
// #param __singleRequired - indicates if the user can select only one value
// #return SET_WINDOW_MODE_xxx value as a result
int SetCreator_ui::setMode(hType* __type, hSet* __set, bool __singleRequired)
{
     if(!isModeCorrect(__type, __set))
          return SET_WINDOW_MODE_FAIL;
     
     _set = __set;
     _newSet = new hSet(__type);
     _domain = __type->domain();
     _singleRequired = __singleRequired;
     
     pushButton_3->setVisible(_domain->canCreateMenu(_singleRequired));
     listWidget->setMode(__singleRequired);
     if(!_singleRequired)
          label->setText("Select values:");
     if(_singleRequired)
          label->setText("Select <b>ONLY</b> one value:");
     
     // displaying values
     displaySet();
     selectItems(_set);
          
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function that set the window mode
//
// #param __cell - pointer to the edited cell
// #return SET_WINDOW_MODE_xxx value as a result
int SetCreator_ui::setMode(XTT_Cell* __cell)
{
     hType* __type = NULL;
     hSet* __set = NULL;
     bool __singleRequired = true;
     
     // checking if the second arg of the cell function is a explicite defined set
     XTT_Attribute* attr = NULL;
     XTT_Cell* cell = NULL;
     hExpression* cellexpr = NULL;
     hFunction* cellfunc = NULL;
     hFunctionArgument* cellfuncarg2 = NULL;
     hMultipleValue* cellfuncarg2value = NULL;

     cell = __cell;
     if(cell != NULL)
          attr = cell->AttrType();
     if(attr != NULL)
          __type = attr->Type();
     if(cell != NULL)
          cellexpr = cell->Content()->expressionField();
     if(cellexpr != NULL)
          cellfunc = cellexpr->function();
     if((cellfunc != NULL) && (cellfunc->argCount() == 2))
          cellfuncarg2 = cellfunc->argument(1);
     if(cellfuncarg2 != NULL)
     {
          cellfuncarg2value = cellfuncarg2->value();
          __set = cellfuncarg2value->valueField();

          if((cellfuncarg2->value()->type() != VALUE) ||
             ((cellfuncarg2->value()->type() == VALUE) && (!cellfuncarg2->value()->valueField()->hasOnlyConstantValues())))
               return SET_WINDOW_MODE_FAIL;
          //if(cellfuncarg2->valueMultiplicity() == FUNCTION_TYPE__MULTI_VALUED)
          __singleRequired = ((cellfunc->argMltplConf(1) & FUNCTION_TYPE__MULTI_VALUED) == 0);
     }
     
     return setMode(__type, __set, __singleRequired);
}
// -----------------------------------------------------------------------------

// #brief Function that displays the given set
//
// #return no values return
void SetCreator_ui::displaySet(void)
{
     hSet* expdom = _domain->expand();
     listWidget->clear();
     
     for(int i=0;i<expdom->size();++i)
     {
          if(expdom->at(i)->type() != SET_ITEM_TYPE_SINGLE_VALUE)
               continue;
          
          QListWidgetItem* newitem = new QListWidgetItem(expdom->at(i)->toString(), NULL, QListWidgetItem::UserType);
          newitem->setFlags(Qt::ItemIsEnabled  | Qt::ItemIsSelectable);
          newitem->setCheckState(Qt::Unchecked);
          listWidget->addItem(newitem);
     }
     
     delete expdom;
}
// -----------------------------------------------------------------------------

// #brief Function that select only these items that are inside the given set
//
// #param __set - pointer to the set
// #return no values return
void SetCreator_ui::selectItems(hSet* __set)
{
     hSet* expdom = _domain->expand();
     for(int i=0;i<expdom->size();++i)
     {
          if(expdom->at(i)->type() != SET_ITEM_TYPE_SINGLE_VALUE)
               continue;
          
          int contain = __set->contain(expdom->at(i));
          if(contain == H_SET_TRUE_VALUE)
               listWidget->item(i)->setCheckState(Qt::Checked);
     }
     
     delete expdom;
}
// -----------------------------------------------------------------------------

// #brief Function that updates edited set according to selected items
//
// #return  no values return
void SetCreator_ui::upadteSet(void)
{
     _set->flush();      // cleaning
     bool create_ranges = checkBox->isChecked();
     
     // searching for first selected element
     int firstsel = -1;
     for(int i=0;i<listWidget->count();++i)
          if(listWidget->item(i)->checkState() == Qt::Checked)
          {
               firstsel = i;
               break;
          }
          
     // if nothing selected
     if(firstsel == -1)
          return;
          
     hSet* expanded = _domain->expand();                             // creating exploded domain
     if(expanded->isEmpty())
          return;

     // Creating ranges
     QList<QList<int>*>* sil = new QList<QList<int>*>;                // list of ranges

     // init
     sil->append(new QList<int>);
     sil->last()->append(firstsel);

     for(int i=firstsel+1;i<listWidget->count();++i)
     {
          if(listWidget->item(i)->checkState() == Qt::Unchecked)
               continue;
               
          if((!create_ranges) || ((listWidget->item(i)->checkState() == Qt::Checked) && (i > (sil->last()->last()+1))))
               sil->append(new QList<int>);
          sil->last()->append(i);
     }
     
     // Creating a new set
     for(int i=0;i<sil->size();++i)
     {
          if(sil->at(i)->isEmpty())
               continue;
          if(sil->at(i)->size() == 1)
               _set->add(new hSetItem(_set, SET_ITEM_TYPE_SINGLE_VALUE, expanded->at(sil->at(i)->first())->fromPtr()->copyTo(), new hValue, false));
          if(sil->at(i)->size() > 1)
               _set->add(new hSetItem(_set, SET_ITEM_TYPE_RANGE, expanded->at(sil->at(i)->first())->fromPtr()->copyTo(), expanded->at(sil->at(i)->last())->fromPtr()->copyTo(), false));
     }
     
     for(int i=0;i<sil->size();++i)
          delete sil->at(i);
     delete sil;
     delete expanded;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when select range button is pressed
//
// #return No return value.
void SetCreator_ui::addRangeClick(void)
{
     QMenu* rangemenu = _domain->createRangeMenu("", "addsetitem");
     hqed::clipboardSet()->push(_newSet);
     rangemenu->exec(QCursor::pos());
     
     selectItems(_newSet);
     
     hqed::clipboardSet()->pop();
     delete rangemenu;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when ok button is pressed
//
// #return No return value.
void SetCreator_ui::okClick(void)
{
     upadteSet();
     xtt::fullVerification();
     xtt::logicalFullVerification();
     
     emit okclicked();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when close button is pressed
//
// #return No return value.
void SetCreator_ui::closeClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user clicks an item in the listBox
//
// #param __item - pointer to the clicked item
// #return No return value.
void SetCreator_ui::itemStateChanged(QListWidgetItem* __item)
{
     if(__item->checkState() == Qt::Unchecked)
          __item->setCheckState(Qt::Checked);
     else
          __item->setCheckState(Qt::Unchecked);
}
// -----------------------------------------------------------------------------

// #brief Function that returns pointer to the edited set
//
// #return  pointer to the edited set
hSet* SetCreator_ui::result(void)
{
     return _set;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param __event - Pointer to event object.
// #return No return value.
void SetCreator_ui::keyPressEvent(QKeyEvent* __event)
{
     switch(__event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               okClick();
               break;

          default:
               QWidget::keyPressEvent(__event);
     }
}
// -----------------------------------------------------------------------------
