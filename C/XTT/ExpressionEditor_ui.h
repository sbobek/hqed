/**
 * \file	ExpressionEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	10.09.2008
 * \version	1.15
 * \brief	This file contains class definition that arranges the expression editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef ExpressionEditorH
#define ExpressionEditorH
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_ExpressionEditor.h"
// -----------------------------------------------------------------------------

class XTT_Cell;

class hType;
class hSetItem;
class hFunction;
class hExpression;
class hMultipleValue;
class ValueEditor_ui;
class MultipleValueEditor_ui;
// -----------------------------------------------------------------------------

/**
* \class 	ExpressionEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	10.09.2008
* \brief 	Class definition that arranges the expression editor window.
*/
class ExpressionEditor_ui : public QWidget, private Ui_ExpressionEditor
{
    Q_OBJECT
    
    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gp1Layout;	               ///< Pointer to groupBox grid layout.
    QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
    
    // The window mode params
    hExpression* _editedExpression;          ///< The pointer to the edited expression
    int _resultMultiplicity;                 ///< Denotes if the result should be single or/and multiple value
    hType* _resultType;                      ///< The expression should return value compatible with this type
    bool _avr;                               ///< Denotes if the AVR is enabled
    bool _hidemode;                          ///< Denotes if the window works in the hide mode
    int _setClass;                           ///< Denotes the type of the set (0-set, 1-domain, 2-decision context)
    int _editedArgument;                     ///< The index of edited argument
    
    XTT_Cell* _cellPtr;     ///< The pointer to the cell that will be contain the expression. The pointer must be set only for the zero level in expression hierarchy
    XTT_Cell* _cell;                         ///< The pointer to the cell is edited. This pointer must be seted every time. It is usefull for attribute selection.
    
    QList<hFunction*> _funcsPtrs;            ///< The list of pointers to the function objects from the function list
    QList<QPushButton*> _edbtns;             ///< The list of buttons
    QList<QComboBox*> _cbxs;                 ///< The list of comboBoxes that are used to argument type selection
    
    hSetItem* _sitem;                        ///< Pointer to the item that is edited with the help of single value editor window
    ValueEditor_ui* singleValueEditor;       ///< Pointer to the window object that is the single value editor dialog
    
    hMultipleValue* _mv;                     ///< Pointer to the multivalue object that is edited with the help of multipleValueEditor
    MultipleValueEditor_ui* multipleValueEditor;  ///< Pointer to the window object that is the multiple value editor dialog
    
protected:
     
     /// \brief Function triggered when the window is going to be showed
	///
     /// \param event - pointer to the object that contains the event parameters
	/// \return No return value.
     virtual void showEvent(QShowEvent* event);
     
     /// \brief Function triggered when the window is going to be closed
	///
     /// \param event - pointer to the object that contains the event parameters
	/// \return No return value.
     virtual void closeEvent(QCloseEvent* event);
     
public:

     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param parent Pointer to parent object.
     ExpressionEditor_ui(QWidget *parent = 0);
     
     /// \brief Constructor ValueEdit_ui class.
     ~ExpressionEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent*);
     
     /// \brief Function sets the mode of window
	///
        /// \param __result_multiplicity - denotes if the result should be single or/and multi valued
        /// \param __required_type - the required type of result
        /// \param __avr - denotes if the AVR is enabled
        /// \param __edited_expression - pointer to the edited expression object
        /// \param __setClass - denotes the type of the set (0-set, 1-domain, 2-action context, 3-conditional context)
	/// \param *__cell - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(int __result_multiplicity, hType* __required_type, bool __avr, hExpression* __edited_expression, int __setClass, XTT_Cell* __cell);
     
     /// \brief Function sets the mode of window
	///
	/// \param *__cell - pointer to the cell
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(XTT_Cell* __cell);
     
     /// \brief Function checks if the current mode is correct
	///
	/// \return true if the current mode is correct, otherwise false
     bool isModeCorrect(void);
     
     /// \brief Function that sets up the window dialog according to expression parameters
	///
	/// \return No return value.
     void displayExpressionItem(void);
     
     /// \brief Function that return the result of this dialog
	///
	/// \return the result of this dialog, that is the pointer to the new expression object
     hExpression* result(void);
     
     /// \brief Function that executes action of edit button click
	///
     /// \param __btnindex - index of the clicked button
	/// \return no value returned
     void editButtonClick(int __btnindex);

public slots:

     /// \brief Function trigered when the edit button is pressed
	///
	/// \return no value returned
     void onEditButtonClick(void);
     
     /// \brief Function trigered when the type selector is changed
	///
	/// \return no value returned
     void typeComboBoxChange(int __newIndex);

     /// \brief Function trigered when the comboBox is popuped up and the item selection is changed
	///
     /// \param __index - higlighted item index
	/// \return no value returned
     void functionHighlighted(int __index);
     
     /// \brief Function trigered when the type comboBox is popuped up and the item selection is changed
	///
     /// \param __text - highlighted text
	/// \return no value returned
     void typeHighlighted(QString __text);
     
     /// \brief Function trigered when the comboBox is changed
	///
     /// \param __index - new item index
	/// \return no value returned
     void functionSelectionChanged(int __index);
     
     /// \brief Function trigered when the number of argument has cahnged
     ///
     /// \param __new_value - new number of arguments
     /// \return no value returned
     void argCountChanged(int __new_value);
     
     /// \brief Function trigered when the singleValueEditor window is closed by OK button
	///
	/// \return no value returned
     void singleValueEditorOK(void);
     
     /// \brief Function trigered when the singleValueEditor window is closed
	///
	/// \return no value returned
     void singleValueEditorClose(void);
     
     /// \brief Function trigered when the multipleValueEditor window is closed by OK button
	///
	/// \return no value returned
     void multipleValueEditorOK(void);
     
     /// \brief Function trigered when the multipleValueEditor window is closed
	///
	/// \return no value returned
     void multipleValueEditorClose(void);
     
     /// \brief Function trigered when the ok button is clicked
	///
	/// \return no value returned
     void okButtonClick(void);
     
     /// \brief Function trigered when the cancel button is clicked
	///
	/// \return no value returned
     void cancelButtonClick(void);
     
signals:

     /// \brief Signal that is emited when the window is closed
	///
	/// \return no value returned
     void windowClose(void);
     
     /// \brief Signal that is emited when the ok button is pressed
	///
	/// \return no value returned
     void okClicked(void);
};
// -----------------------------------------------------------------------------
// This class has not the global object because each object that want to use
// the window of this class must define it own instance

#endif
