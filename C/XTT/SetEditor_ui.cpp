/*
 *     $Id: SetEditor_ui.cpp,v 1.16.4.2.2.1 2011-03-16 08:58:37 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QDebug>
#include "../../M/hSet.h"
#include "../../M/hSetItem.h"
#include "../../M/hValue.h"
#include "../../M/hType.h"
#include "../../M/hDomain.h"
#include "../../M/XTT/XTT_Cell.h"

#include "../../namespaces/ns_hqed.h"
#include "ValueEditor_ui.h"
#include "SetEditor_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor SetEditor_ui class.
//
// #param parent Pointer to parent object.
SetEditor_ui::SetEditor_ui(QWidget* /*parent*/)
{
    initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor ValueEdit_ui class.
SetEditor_ui::~SetEditor_ui(void)
{
     delete formLayout;
     delete gp1Layout;
     delete gp2Layout;
     delete gp3Layout;
     delete _set;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void SetEditor_ui::initForm(void)
{
     setupUi(this);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     layoutForm();
     hqed::centerWindow(this);
     
     _type = NULL; 
     _set = NULL;          
     _cell = NULL;
     _setClass = -1;                           
     _singleValued = true;                      
     _avr = false;
     _autohide = false;
     
     acending_index = 0;
     decending_index = 1;  
     
     sort_from_index = -1;
     sort_to_index = -1;
     sort_power_index = -1;
     sort_alph_index = -1;
     edited_item = -1;
     
     toolButton_4->setIcon(QIcon(":/all_images/images/uparrow.png"));
     toolButton_5->setIcon(QIcon(":/all_images/images/downarrow.png"));
     toolButton_6->setIcon(QIcon(":/all_images/images/add_56.png"));
     toolButton_3->setIcon(QIcon(":/all_images/images/Edit1.png"));
     toolButton_2->setIcon(QIcon(":/all_images/images/Trash1.png"));
     toolButton->setIcon(QIcon(":/all_images/images/Trash2.png"));
     
     connect(pushButton, SIGNAL(clicked()), this, SLOT(okButtonClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(cancelButtonClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(simplifyClicked()));
     connect(toolButton_6, SIGNAL(clicked()), this, SLOT(itemAdd()));
     connect(toolButton_3, SIGNAL(clicked()), this, SLOT(onItemEdit()));
     connect(toolButton_2, SIGNAL(clicked()), this, SLOT(itemRemove()));
     connect(toolButton, SIGNAL(clicked()), this, SLOT(flushSet()));
     connect(toolButton_7, SIGNAL(clicked()), this, SLOT(sortClicked()));
     
     connect(toolButton_6, SIGNAL(clicked()), this, SLOT(checkSV()));
     connect(toolButton_3, SIGNAL(clicked()), this, SLOT(checkSV()));
     connect(toolButton_2, SIGNAL(clicked()), this, SLOT(checkSV()));
     connect(toolButton, SIGNAL(clicked()), this, SLOT(checkSV()));
     connect(toolButton_4, SIGNAL(clicked()), this, SLOT(itemMoveUp()));
     connect(toolButton_5, SIGNAL(clicked()), this, SLOT(itemMoveDown()));
     connect(listWidget, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(setItemDoubleClicked(QListWidgetItem*)));
     connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(sortDirectionChanged(int)));
     
     tb7icons[0] = ":/all_images/images/sort_asc.png";
     tb7icons[1] = ":/all_images/images/sort_desc.png";
     
     comboBox_2->clear();
     comboBox_2->addItem("Ascending");
     comboBox_2->addItem("Descending");
     comboBox_2->setCurrentIndex(0);
     
     vewin = new ValueEditor_ui;
     
     _type = NULL;                            
    _setClass = -1;                           
    _singleValued = false;    
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void SetEditor_ui::layoutForm(void)
{
     resize(5, 5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 3);
     formLayout->addWidget(groupBox_3,   1, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   2, 0, 1, 3);
     formLayout->addWidget(pushButton,   3, 1);
     formLayout->addWidget(pushButton_2, 3, 2);
     formLayout->setRowStretch(0, 0);
     formLayout->setRowStretch(1, 0);
     formLayout->setRowStretch(2, 1);
     formLayout->setRowStretch(3, 0);
     formLayout->setColumnStretch(0, 1);
     formLayout->setColumnStretch(1, 0);
     formLayout->setColumnStretch(2, 0);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(label,   0, 0);
     gp1Layout->addWidget(label_2, 0, 1);
     gp1Layout->addWidget(label_3, 1, 0);
     gp1Layout->addWidget(label_4, 1, 1);
     gp1Layout->addWidget(label_5, 2, 0);
     gp1Layout->addWidget(label_6, 2, 1);
     gp1Layout->setColumnStretch(0, 0);
     gp1Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);
     
     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(listWidget,   0, 0, 7, 1);
     gp2Layout->addWidget(toolButton_4, 0, 1);
     gp2Layout->addWidget(toolButton_5, 1, 1);
     gp2Layout->addWidget(toolButton_6, 3, 1);
     gp2Layout->addWidget(toolButton_3, 4, 1);
     gp2Layout->addWidget(toolButton_2, 5, 1);
     gp2Layout->addWidget(toolButton,   6, 1);
     gp2Layout->setColumnStretch(0, 1);
     gp2Layout->setRowStretch(2, 1);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);
     
     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(pushButton_3, 0, 0);
     gp3Layout->addWidget(line,         0, 1);
     gp3Layout->addWidget(comboBox,     0, 2);
     gp3Layout->addWidget(comboBox_2,   0, 3);
     gp3Layout->addWidget(toolButton_7, 0, 4);
     gp3Layout->setColumnStretch(2, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void SetEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               cancelButtonClick();
               break;

          case Qt::Key_Return:
               okButtonClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function sets the window working mode
//
// #param hType* __type - data type of the set
// #param hSet* __set - edited set
// #param int __setClass - type of the set
// #li 0-set
// #li 1-domain
// #li 2-action context
// #li 3-coditional context
// #li 4-cell in a state table
// #param bool __singleValued - denotes if the set can contains more than one value
// #param bool __avr - denotes if the attribute value reference is allowed
// #param __ptr - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
// #return SET_WINDOW_MODE_ value as a result
int SetEditor_ui::setMode(hType* __type, hSet* __set, int __setClass, bool __singleValued, bool __avr, XTT_Cell* __ptr)
{
     _type = __type;
     _set = __set->copyTo();
     _setClass = __setClass;
     _singleValued = __singleValued;
     _avr = __avr;
     _cell = __ptr;

     if(!isModeCorrect())
          return SET_WINDOW_MODE_FAIL;

     QString ptn = hpTypesInfo.ptn.at(hpTypesInfo.iopti(_type->baseType()));
     QString sc[] = {"set", "domain", "decision", "conditional", "state value"};
     QString sv = "true";

     label_2->setText(ptn);
     label_4->setText(sc[_setClass]);
     label_6->setText(sv);

     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     checkSV();

     sort_from_index = -1;
     sort_to_index = -1;
     sort_power_index = -1;
     sort_alph_index = -1;
     comboBox->clear();
     QStringList cbitms;

     if(((__setClass == 0) || (__type->baseType() != SYMBOLIC_BASED)) && (!__singleValued))
     {
          sort_from_index = cbitms.size();
          cbitms << "\'from\' value";
          sort_to_index = cbitms.size();
          cbitms << "\'to\' value";
          sort_power_index = cbitms.size();
          cbitms << "item's power";
     }
     if((__type->baseType() == SYMBOLIC_BASED) && (!__singleValued))
     {
          sort_alph_index = cbitms.size();
          cbitms << "alphabetical";
     }
     comboBox->addItems(cbitms);

     bool sortEnabled = (cbitms.size() > 0);
     comboBox->setEnabled(sortEnabled);
     comboBox_2->setEnabled(sortEnabled);
     toolButton_7->setEnabled(sortEnabled);

     // Simplifiy button
     pushButton_3->setDisabled((__setClass == 1) && (__type->baseType() == SYMBOLIC_BASED));

     if(__singleValued)
     {
          _autohide = true;
          if(_set->size() == 0)
               itemAdd();
          if(_set->size() > 0)
               itemEdit(0);
          return SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW;
     }

     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function checks if the current mode is correct
//
// #return true if the current mode is correct, otherwise false
bool SetEditor_ui::isModeCorrect(void)
{
     bool res = true;
     
     if(_type == NULL)
          res = false;
          
     if(_set == NULL)
          return false;
     
     if((_setClass != 0) && (_setClass != 1) && (_setClass != 2) && (_setClass != 3) && (_setClass != 4))
          res = false;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when OK button is pressed
//
// #return No return value.
void SetEditor_ui::okButtonClick(void)
{
     QString __msgs;
     if(!_set->setVerification(__msgs))
     {
          hqed::showMessages(__msgs);
          return;
     }
          
     emit okClicked();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when cancel button is pressed
//
// #return No return value.
void SetEditor_ui::cancelButtonClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the move up button is pressed
//
// #return No return value.
void SetEditor_ui::itemMoveUp(void)
{    
     int itemIndex = listWidget->currentRow();
     if(itemIndex < 1)
          return;
          
     _set->swapItems(itemIndex, itemIndex-1);
     
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     listWidget->setCurrentRow(itemIndex-1);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the move down button is pressed
//
// #return No return value.
void SetEditor_ui::itemMoveDown(void)
{
     int itemIndex = listWidget->currentRow();
     if((itemIndex < 0) || (itemIndex >= (listWidget->count()-1)))
          return;
          
     _set->swapItems(itemIndex, itemIndex+1);
     
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     listWidget->setCurrentRow(itemIndex+1);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the list widget is double clicked
//
// #param QListWidgetItem* - poiter to the item that is double clicked
// #return No return value.
void SetEditor_ui::setItemDoubleClicked(QListWidgetItem*)
{
     onItemEdit();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the add button is pressed
//
// #return No return value.
void SetEditor_ui::itemAdd(void)
{
     // Creating new set item
     if((_setClass != 1) || (_type->baseType() != SYMBOLIC_BASED))
          _si = new hSetItem(_set, SET_ITEM_TYPE_SINGLE_VALUE, new hValue(NOT_DEFINED), new hValue(NOT_DEFINED), false);
     if((_setClass == 1) && (_type->baseType() == SYMBOLIC_BASED))
          _si = new hSetItem(_set, SET_ITEM_TYPE_SINGLE_VALUE, new hValue("", QString::number(((hDomain*)_set)->maxNumericValue()+1)), new hValue(NOT_DEFINED), false);
     
     if(vewin != NULL)
          delete vewin;

     vewin = new ValueEditor_ui;
     connect(vewin, SIGNAL(okClicked()), this, SLOT(itemAdded()));
     connect(vewin, SIGNAL(okClicked()), this, SLOT(checkSV()));
     connect(vewin, SIGNAL(windowClosed()), this, SLOT(evCloseWindow()));
     int smr = vewin->setMode(_type, _si, _setClass, _singleValued, _avr, _cell);
     
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          delete _si;
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     vewin->show();
}    
// -----------------------------------------------------------------------------

// #brief Function triggered when the value editor is closed by use the ok button
//
// #return No return value.
void SetEditor_ui::itemAdded(void)
{
     hSetItem* result = vewin->result();
     _set->add(result);
     
     QString __msgs;
     bool verificationresult = _set->setVerification(__msgs);
     if(!verificationresult)
     {
          _set->deleteItem(_set->size()-1);
          hqed::showMessages(__msgs);
     }
     
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     
     if((_autohide) && (verificationresult))
          okButtonClick();
     if((_autohide) && (!verificationresult))
          cancelButtonClick();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the value editor is closed by use the cancel button
//
// #return No return value.
void SetEditor_ui::evCloseWindow(void)
{
     delete _si;
     _si = NULL;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the edit button is pressed
//
// #return No return value.
void SetEditor_ui::onItemEdit(void)
{
     int itemIndex = listWidget->currentRow();
     if(itemIndex == -1)
          return;
     
     itemEdit(itemIndex);
}
// -----------------------------------------------------------------------------

// #brief Function that allows for editing set item
//
// #param __itemIndex - index of the set item
// #return No return value.
void SetEditor_ui::itemEdit(int __itemIndex)
{
     _si = _set->at(__itemIndex);
     edited_item = __itemIndex;
     
     delete vewin;
     vewin = new ValueEditor_ui;
     connect(vewin, SIGNAL(okClicked()), this, SLOT(itemEdited()));
     int smr = vewin->setMode(_type, _si, _setClass, _singleValued, _avr, _cell);
     
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          delete _si;
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
     
     vewin->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the value editor is closed by use the ok button
//
// #return No return value.
void SetEditor_ui::itemEdited(void)
{
     int itemIndex = edited_item;
     if(itemIndex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Impossible to establish current item.", QMessageBox::Ok);
          return;
     }

     hSetItem* result = vewin->result();
     hSetItem* olditem = _set->takeAt(itemIndex);
     _set->insertItem(itemIndex, result);

     QString __msgs;
     bool verificationresult = _set->setVerification(__msgs);
     if(!verificationresult)
     {
          _set->deleteItem(itemIndex);
          _set->insertItem(itemIndex, olditem);
          hqed::showMessages(__msgs);
     }
     if(verificationresult)
          delete olditem;

     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     listWidget->setCurrentRow(itemIndex);

     if((_autohide) && (verificationresult))
          okButtonClick();
     if((_autohide) && (!verificationresult))
          cancelButtonClick();

     edited_item = -1;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the delete button is pressed
//
// #return No return value.
void SetEditor_ui::itemRemove(void)
{
     int itemIndex = listWidget->currentRow();
     if(itemIndex == -1)
          return;
          
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to remove item: " + _set->at(itemIndex)->toString() + "?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
          
     if(!_set->deleteItem(itemIndex))
     {
          QMessageBox::critical(NULL, "HQEd", _set->lastError(), QMessageBox::Ok);
          return;
     }
     
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
     listWidget->setCurrentRow(itemIndex);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the flush button is pressed
//
// #return No return value.
void SetEditor_ui::flushSet(void)
{
     if(_set->empty())
          return;
          
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to remove all the item from the set?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
          
     _set->flush();
     
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the sort button is pressed
//
// #return No return value.
void SetEditor_ui::sortClicked(void)
{
     int policy = comboBox->currentIndex();
     int direction = comboBox_2->currentIndex();
     
     if((policy == -1) || (direction == -1))
          return;
          
     if((_type->baseType() == SYMBOLIC_BASED) && (_setClass == 1) && (_type->domain()->ordered()))
          if(QMessageBox::question(NULL, "HQEd", "The symbolic domain is the ordered set. The sorting procedure may change the sequence of items. Would you like to continue?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
               return;

     int p = -1;
     int d = -1;
     if(policy == sort_from_index)
          p = H_SET_SORT_FROM_VALUE;
     if(policy == sort_to_index)
          p = H_SET_SORT_TO_VALUE;
     if(policy == sort_power_index)
          p = H_SET_SORT_POWER;
     if(policy == sort_alph_index)
          p = H_SET_SORT_ALPHABETICAL;
          
     if(direction == acending_index)
          d = H_SET_SORT_ASCENDING;
     if(direction == decending_index)
          d = H_SET_SORT_DESCEND;
          
     if(_set->sort(p, d) != H_SET_ERROR_NO_ERROR)
          QMessageBox::critical(NULL, "HQEd", _set->lastError(), QMessageBox::Ok);
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the sort button is pressed
//
// #return No return value.
void SetEditor_ui::simplifyClicked(void)
{
     if(_set->simplify() != H_SET_ERROR_NO_ERROR)
          QMessageBox::critical(NULL, "HQEd", _set->lastError(), QMessageBox::Ok);
     listWidget->clear();
     listWidget->addItems(_set->stringItems());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the any button that changes the count of item is pressed
//
// #return No return value.
void SetEditor_ui::checkSV(void)
{
     if(!_singleValued)
          return;
          
     bool addButtonDisabled = (_set->isSingleValue()) || (_set->size() >= 1);
     toolButton_6->setEnabled(!addButtonDisabled);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the sort direction comboBox is changed
//
// #return No return value.
void SetEditor_ui::sortDirectionChanged(int __newIndex)
{
     toolButton_7->setIcon(QIcon(tb7icons[__newIndex]));
}
// -----------------------------------------------------------------------------

// #brief Function returns the result of dialog
//
// #return pointer to the set thst has been created by using this dialog
hSet* SetEditor_ui::result(void)
{
     return _set->copyTo();
}
// -----------------------------------------------------------------------------
