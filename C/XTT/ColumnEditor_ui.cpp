/*
 *	   $Id: ColumnEditor_ui.cpp,v 1.35 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "SelectGroup_ui.h"
#include "ColumnEditor_ui.h"
#include "../../namespaces/ns_hqed.h"

#include "../../M/XTT/XTT_Attribute.h"
#include "../../M/XTT/XTT_AttributeList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
// -----------------------------------------------------------------------------

ColumnEditor_ui* ColumnEdit;
// -----------------------------------------------------------------------------

// #brief Constructor ColumnEditor_ui class.
//
// #param parent Pointer to parent object.
ColumnEditor_ui::ColumnEditor_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

// #brief Constructor ColumnEditor_ui class.
//
// #param _new Determines whether new column should be created.
// #param _path Path to attribute name.
// #param _cindex Determines which element is edited.
// #param parent Pointer to parent object.	
ColumnEditor_ui::ColumnEditor_ui(bool _new, QString _path, int _cindex, QWidget*)
{
     setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 

     connect(pushButton, SIGNAL(clicked()), this, SLOT(NewGroupSelect()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(ClickOk()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(ClickCancel()));
     connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(contextChanged(int)));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);

     fNew = _new;
     fcIndex = _cindex;
     fPath = _path;
     pushButton->setText("No group selected");
     
     int comboBox2Index[] = {0, 1, 1, 1, 2};

     // Kiedy edycja
     if(!_new)
     {
          if(fcIndex != 4)
          {
               // Extract attribute name i path
               QStringList list = _path.split(".", QString::SkipEmptyParts);

               if(list.size() == 1)
               {
                    ReadAttributes(0);

                    // Nazwa atrybutu
                    QString aname = list.at(list.size()-1);
                    
                    // Index atrybutu
                    int aindex = AttributeGroups->Group(0)->IndexOfName(aname, XTT_ATTRIBUTE_TYPE_PHYSICAL);
                    if(aindex == -1)
                    {
                         const QString content = "Critical error. Attribute \"" + aname + "\" not exists in group.";
                         QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
                    }
                    comboBox->setCurrentIndex(aindex);
               }
               if(list.size() > 1)
               {
                    // Nazwa grupy
                    QString gname = list.at(list.size()-2);

                    // Nazwa atrybutu
                    QString aname = list.at(list.size()-1);

                    // Index grupy
                    int gindex = AttributeGroups->IndexOfName(gname);
                    if(gindex == -1)
                    {
                         const QString content = "Critical error. Group \"" + gname + "\" not exists.";
                         QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
                         return;
                    }

                    // Wczytywanie atrybutow
                    ReadAttributes(gindex);

                    // Index atrybutu
                    int aindex = AttributeGroups->Group(gindex)->IndexOfName(aname, XTT_ATTRIBUTE_TYPE_PHYSICAL);
                    if(aindex == -1)
                    {
                         const QString content = "Critical error. Attribute \"" + aname + "\" not exists in group.";
                         QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
                    }
                    comboBox->setCurrentIndex(aindex);
               }
          }

          comboBox_2->setCurrentIndex(comboBox2Index[fcIndex]);
     }
     // Kiedy nowy
     if(_new)
     {
          ReadAttributes(0);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button Cancel is pressed.
//
// #return No return value.
void ColumnEditor_ui::ClickCancel(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button OK is pressed.
//
// #return No return value.
void ColumnEditor_ui::ClickOk(void)
{
     if(comboBox_2->currentIndex() == -1)
     {
          const QString content = "There is no context selected.";
          QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }
     
     int ctxs[] = {1, 4, 5, 5};
     int context = ctxs[comboBox_2->currentIndex()];

     // Sprawdzanie czy wybrany atrybut moze byc uzyty w danym kontekscie
     XTT_Attribute* attr = NULL;
     
     if(context != 5)
          attr = AttributeGroups->Attribute(pushButton->text() + "." + comboBox->currentText());

     if((context != 5) && (attr == NULL))
     {
          const QString content = "Critical error. Selected attribute not exists.";
          QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy wszystkie dane zostaly podane
     if((context != 5) && (comboBox->currentIndex() == -1))
     {
          const QString content = "There is no attribute selected.";
          QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy w wybranym kontekscie mozna uzyc danego atrybutu

     // Jezeli kontekst jest conditional i atrybut input
     //if((context != 5) && (context != 0) && (attr->Class() == XTT_ATT_CLASS_RO))
     //{
     //     const QString content = "This is a input attribue which can be use only in conditional context.";
     //     QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
     //     return;
     //}

     // Jezeli kontekst jest action a atrybut middle
     // if((context == 3) && (attr->RelationsType() == 1))
     // {
          // const QString content = "This is a middle attribue which can't be use in action context.";
          // QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          // return;
     // }
     /*
     // Jezeli kontekst nie jest action a atrybut typu output
     if((context != 3) && (attr->RelationsType() == 2))
     {
          const QString content = "This is a output attribue whitch can be use only in decision context.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     } */

     groupName = pushButton->text() + ".";
     if((groupName == "Global.") || (context == 5))
          groupName = "";
     attrName = context == 5?"*":comboBox->currentText();
     ctxIndex = context;

     emit onClose();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function shows select new group window.
//
// #return No return value.
void ColumnEditor_ui::NewGroupSelect(void)
{
     delete SelGroup;
     SelGroup = new SelectGroup_ui;
     connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(NewGroupChange()));
     SelGroup->show();
}
// -----------------------------------------------------------------------------

// #brief Function refreshes window after changing the group.
//
// #return No return value.
void ColumnEditor_ui::NewGroupChange(void)
{
     int pindex = AttributeGroups->IndexOfName(SelGroup->Result());
     if(pindex == -1)
          return;
          
     ReadAttributes(pindex);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when context changed
//
// #param __index - the new item index
// #return No return value.
void ColumnEditor_ui::contextChanged(int __index)
{
     groupBox->setEnabled(__index != 2);
}
// -----------------------------------------------------------------------------

// #brief Function reads attributs from the group.
//
// #param gindex Index of the group.
// #return No return value.
void ColumnEditor_ui::ReadAttributes(int gindex)
{
     // Sprawdzanie czy index jest poprawny
     if((int)gindex == -1)
     {
          const QString content = "There is no group selected.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     pushButton->setText(AttributeGroups->CreatePath(AttributeGroups->Group(gindex)->Name()));

     comboBox->clear();
     for(unsigned int i=0;i<AttributeGroups->Group(gindex)->Count();i++)
          if(!AttributeGroups->Group(gindex)->Item(i)->isConceptual())
               comboBox->addItem(AttributeGroups->Group(gindex)->Item(i)->Name());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ColumnEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               ClickCancel();
               break;

          case Qt::Key_Return:
               ClickOk();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
