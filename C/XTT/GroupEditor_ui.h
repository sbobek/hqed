/**
 * \file	GroupEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges group editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GROUPEDITOR_H
#define GROUPEDITOR_H
// -----------------------------------------------------------------------------
 
#include "ui_GroupEditor.h"
// -----------------------------------------------------------------------------
/**
* \class 	GroupEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges group editor window.
*/
class GroupEditor_ui : public QWidget, private Ui_GroupEditor
{
    Q_OBJECT

private:

	bool fNew;		///< Determines whether new group should be added(True) or not(False).
	int fIndex;		///< Index of edited group.
	bool fIsMain;	///< Determines whether main group should be created(True) or not(False).

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public:

    /// \brief Constructor GroupEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	GroupEditor_ui(QWidget *parent = 0);

    /// \brief Constructor GroupEditor_ui class.
	///
	/// \param _new Determines whether new group should be added(True) or not(False).
	/// \param _index Index of edited group.
	/// \param _isMain Determines whether main group should be created(True) or not(False).
	/// \param parent Pointer to parent object.
	GroupEditor_ui(bool _new, int _index, bool _isMain, QWidget *parent = 0);

	/// \brief Function sets group editor window.
	///
	/// \return No return value.
	void SetWindow(void);

public slots:

	/// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void ClickOk(void);

	/// \brief Function is triggered when button 'Cancel' is pressed.
	///
	/// \return No return value.
	void ClickCancel(void);

	/// \brief Function shows select group window.
	///
	/// \return No return value.
	void GroupSelectClick(void);

	/// \brief Function is triggered when select group window is closed.
	///
	/// \return No return value.
	void OnGroupChange(void);

signals:

	/// \brief Function closes group editor window through clicking button 'Ok'.
	///
	/// \return No return value.
	void onClose(void);
};
// -----------------------------------------------------------------------------

extern GroupEditor_ui* GroupEdit;
// -----------------------------------------------------------------------------

#endif
