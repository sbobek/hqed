/*
 *	   $Id: TableEditor_ui.cpp,v 1.35 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "ColumnEditor_ui.h"
#include "SelectGroup_ui.h"
#include "TableEditor_ui.h"
#include "../Settings_ui.h"
#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"

#include "../../M/XTT/XTT_AttributeList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
// -----------------------------------------------------------------------------

TableEditor_ui* TableEdit;
// -----------------------------------------------------------------------------

// #brief Constructor TableEditor_ui class.
//
// #param parent Pointer to parent object.
TableEditor_ui::TableEditor_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
    radioButton_3->setVisible(false);        // turning off the halt table radioButton - not supported any more in HeKatE
}
// -----------------------------------------------------------------------------

// #brief Constructor TableEditor_ui class.
//
// #param _new Determines whether new table is created(True) or not(False).
// #param _index Index of edited table.
// #param parent Pointer to parent object.
TableEditor_ui::TableEditor_ui(bool _new, int _index, QWidget*)
{
     setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
	
     connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickOk()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(ClickCancel()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(ClickApply()));
     connect(toolButton, SIGNAL(clicked()), this, SLOT(NewCol()));
     connect(toolButton_2, SIGNAL(clicked()), this, SLOT(EditCol()));
     connect(toolButton_3, SIGNAL(clicked()), this, SLOT(DeleteCol()));
     connect(toolButton_4, SIGNAL(clicked()), this, SLOT(MoveUp()));
     connect(toolButton_5, SIGNAL(clicked()), this, SLOT(MoveDown()));
     connect(toolButton_6, SIGNAL(clicked()), this, SLOT(UnDelete()));
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(SelectedTableChange(int)));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);
     radioButton_3->setVisible(false);       // turning off the halt table radioButton - not supported any more in HeKatE

     clist << "Coditional" << "Assert" << "Retract" << "Action" << "*";

     fNew = _new;
     fIndex = _index;

     if(!fNew)
          reloadTables(fIndex);
	
	// Jezeli tabela jest nowa to jako nazwe wpisujemy nazwe automatyczna
	if((fNew) && (Settings->xttCreateTableAutoName()))
		comboBox->setEditText(TableList->newBasicTableName(Settings->xttTableBaseName()));
}
// -----------------------------------------------------------------------------

// #brief Function that reloads the list of tables
//
// #param __destinationIndex - destination index of the combobox
// #return No return value.
void TableEditor_ui::reloadTables(int __destinationIndex)
{
     // Wczytywanie nazw tabel
     comboBox->clear();
     comboBox->addItems(TableList->TablesTitles());
     comboBox->setCurrentIndex(__destinationIndex);
     SelectedTableChange(__destinationIndex);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when selected table is changed.
//
// #param c_index Index of selected table.
// #return No return value.
void TableEditor_ui::SelectedTableChange(int c_index)
{
     fIndex = c_index;
     if(c_index > -1)
          SetWindow();
}
// -----------------------------------------------------------------------------

// #brief Function sets the window.
//
// #return No return value.
void TableEditor_ui::SetWindow(void)
{
     XTT_Table* tbl = TableList->Table(fIndex);
     if(tbl == NULL)
     {
          QMessageBox::information(NULL, "HQEd", "Incorrect table index.\nThe window is now in creating new table mode.", QMessageBox::Ok);
          fNew = true;
          return;
     }

     lineEdit->setText(tbl->Description());
     if(tbl->TableType() == 0)
          radioButton->setChecked(true);
     if(tbl->TableType() == 1)
          radioButton_2->setChecked(true);
     if(tbl->TableType() == 2)
          radioButton_3->setChecked(true);

     // Wczytywanie kolumn
     tableWidget->setRowCount(0);
     for(int i=0;i<tbl->ColCount();i++)
     {
          int context = tbl->ColContext(i);
          
          QString aname = "";
          
          if(context != 5)
               aname = AttributeGroups->CreatePath(tbl->ContextAtribute((unsigned int)i)) + tbl->ContextAtribute((unsigned int)i)->Name();
          if(context == 5)
               aname = "*";
          if(aname == "#NAN")
          {
               QMessageBox::critical(NULL, "HQEd", "Missing pointer.", QMessageBox::Ok);
               return;
          }
          QString actx = clist.at(tbl->ColContext(i)-1);

          // Przygotowanie elementow do wstawienia
          QTableWidgetItem* _attr = new QTableWidgetItem(aname);
          QTableWidgetItem* _cotx = new QTableWidgetItem(actx);
          QTableWidgetItem* _acti = new QTableWidgetItem("-");
          QTableWidgetItem* _crid = new QTableWidgetItem(QString::number(tbl->AbstractColID(i)));

          // Powiekszenie ilosci wierszy
          tableWidget->insertRow(i);

          // Wstawienie wartosci
          tableWidget->setItem(i, 0, _attr);
          tableWidget->setItem(i, 1, _cotx);
          tableWidget->setItem(i, 2, _acti);
          tableWidget->setItem(i, 3, _crid);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Cancel' is pressed.
//
// #return No return value.
void TableEditor_ui::ClickCancel(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function that saves the changes
//
// #return true if everything is ok, otherwise false
bool TableEditor_ui::applyChanges(void)
{
     QString title = comboBox->currentText();
     int cbCIndex = comboBox->currentIndex();

     // Sprawdzamy czy podano tytul
     if(title == "")
     {
          QMessageBox::information(NULL, "HQEd", "You have to give title for this table.", QMessageBox::Ok);
          return false;
     }
     
     // Jezeli nie wprowadzono znadej kolumny
     if(tableWidget->rowCount() == 0)
     {
          QMessageBox::information(NULL, "HQEd", "You have to define at least one column.", QMessageBox::Ok);
          return false;
     }

	
     // Sprawdznie typu tabeli, czy juz nie istnieje tabela poczatkowa, lub koncowa
     // o ile ta zdefiniowano jaka taka
     int ttype = 1;
     if(radioButton->isChecked())
     {	
		/*
          // Sprawdzanie czy tabela inicjalizacyjna juz nie istnieje
          if(((fNew) && (TableList->IndexOfType(0) > -1)) || ((!fNew) && (TableList->IndexOfType(0) > -1) && (TableList->IndexOfType(0) != comboBox->currentIndex())))
          {
               QMessageBox::information(NULL, "HQEd", "Initialization table is already exists. You can have only one initialization table.", QMessageBox::Ok);
               return false;
          }
		*/
          ttype = 0;
     }
     if(radioButton_3->isChecked())
     {
		/*
          // Sprawdzanie czy tabela koncowa juz nie istnieje
          if(((fNew) && (TableList->IndexOfType(2) > -1)) || ((!fNew) && (TableList->IndexOfType(2) > -1) && (TableList->IndexOfType(2) != comboBox->currentIndex())))
          {
               QMessageBox::information(NULL, "HQEd", "Halt table is already exists. You can have only one halt table.", QMessageBox::Ok);
               return false;
          }
		*/
          ttype = 2;
     }

     // No to teraz mozemy dodac tabele

     // Kiedy nowa
     XTT_Table* tbl;
     if(fNew)
     {
          int cc[CONTEXT_COUNT] = {0, 0, 0, 0, 0};

          TableList->Add();
          tbl = TableList->Table(TableList->Count()-1);
          tbl->setTableType(ttype);
          
          // Sprawdzamy czy wprowadzony tytul juz nie istnieje
          if(TableList->indexOfTitle(title, TableList->Count()-1) > -1)
          {
               int tindex = TableList->IndexOfID(tbl->TableId());
               if(tindex > -1)
                    TableList->Delete(tindex);
               QMessageBox::information(NULL, "HQEd", "Title is already exists. Please give another title.", QMessageBox::Ok);
               return false;
          }
          
          tbl->setTitle(title);
          tbl->setDescription(lineEdit->text());

          // Ustawianie kolumn
          for(int i=tableWidget->rowCount()-1;i>=0;i--)
          {
               // Index kontekstu
               int ctxIndex = clist.indexOf(tableWidget->item(i,1)->text());

               // Jezeli jest to pierwsz komorka z tego kontekstu to dodajemy nowy
               // kontekst w tabeli
               if(cc[ctxIndex] == 0)
                    tbl->InsertNewContext(ctxIndex+1);

               // Inaczej wstawiamy na nastepna kolumne
               else
                    tbl->InsertCol(0);

               // Zwiekszamy ilsoc wstawionych kolumn w danym kontekscie
               cc[ctxIndex]++;
          }

          // Dopisywanie atrubtow do kazdej kolumny
          for(unsigned int i=0;i<(unsigned int)tableWidget->rowCount();i++)
          {
               int ctxIndex = clist.indexOf(tableWidget->item(i,1)->text())+1;
          
               QString attr_name = tableWidget->item(i,0)->text();
               XTT_Attribute* attr = NULL;
               
               if(ctxIndex == 5)
                    attr = AttributeGroups->CreateNewActionArgument();
               if(ctxIndex != 5)
                    attr = AttributeGroups->Attribute(attr_name);
               if(attr == NULL)
               {
                    QMessageBox::critical(NULL, "HQEd", "Attribute name \"" + attr_name + "\" is incorrect.", QMessageBox::Ok);
                    return false;
               }
               tbl->setContextAtributes(i, attr);
          }
          // Odswiezenie widolu tabel
          TableList->Refresh(TableList->Count()-1);
     }

     // Kiedy edytowana nowa
     if(!fNew)
     {
          int ti = comboBox->currentIndex();
          tbl = TableList->Table(ti);

          // Ustawienie opisu i tytulu
          int oldType = tbl->TableType();
          tbl->setTableType(ttype);
          
          // Sprawdzamy czy wprowadzony tytul juz nie istnieje
          if(TableList->indexOfTitle(title, cbCIndex) > -1)
          {
               tbl->setTableType(oldType);
               QMessageBox::information(NULL, "HQEd", "Title is already exists. Please give another title.", QMessageBox::Ok);
               return false;
          }
          
          tbl->setTitle(title);
          tbl->setDescription(lineEdit->text());
		
		// Usuwanie wszystkich polaczen przychodzacych do danej tabeli jezeli jest typu FACT
		if(ttype == 0)
		{
			for(unsigned int i=0;i<tbl->RowCount();i++)
				ConnectionsList->RemoveInConnectionsWithRowID(tbl->Row(i)->RowId());
		}
		
		// Usuwanie wszystkich polaczen wychodzacych z danej tabeli jezeli jest typu HALT
		if(ttype == 2)
		{
			for(unsigned int i=0;i<tbl->RowCount();i++)
				ConnectionsList->RemoveOutConnectionsWithRowID(tbl->Row(i)->RowId());
		}

          // Zamiana kolumn
          for(int i=0;i<tableWidget->rowCount();i++)
          {
               QString path = tableWidget->item(i, 0)->text();
               QString cntx = tableWidget->item(i, 1)->text();
               QString acti = tableWidget->item(i, 2)->text();
               QString crid = tableWidget->item(i, 3)->text();
               
               int context = clist.indexOf(cntx)+1;
               int c_ca_id = crid.toInt();
               int curr_index = tbl->IndexOfColId(c_ca_id);

               XTT_Attribute* attr = NULL;
               
               if(context == 5)
                    attr = AttributeGroups->CreateNewActionArgument();
               if(context != 5)
                    attr = AttributeGroups->Attribute(path);
               if(attr == NULL)
               {
                    QMessageBox::critical(NULL, "HQEd", "Path not exists: " + path + ".", QMessageBox::Ok);
                    return false;
               }

               // Jezeli edytujemy
               if(acti == "EDIT")
               {
                    if(curr_index == -1)
                    {
                         QMessageBox::critical(NULL, "HQEd", "Unknown ID: " + crid + ".", QMessageBox::Ok);
                         return false;
                    }
                    tbl->setColContext(curr_index, context);
                    tbl->setContextAtributes(curr_index, attr);
                    tbl->Move(curr_index, i);
               }

               // Jezeli dodajemy nowa kolumne
               if(acti == "NEW")
               {
                    tbl->InsertCol(i);
                    tbl->setColContext(i, context);
                    tbl->setContextAtributes(i, attr);
               }
          }
          

          // Uaktualnienie rozmiarow kontekstu

          // Zerujemy rozmiay kontekstu
          for(unsigned int i=1;i<=CONTEXT_COUNT;i++)
               tbl->setContextSize(i, 0);
          for(int i=0;i<tableWidget->rowCount();i++)
          {
               QString cntx = tableWidget->item(i, 1)->text();
               QString acti = tableWidget->item(i, 2)->text();
               int context = clist.indexOf(cntx)+1;
               tbl->setContextSize(context, tbl->ContextSize(context)+1);
          }

          // Usuwanuie kolumn
          for(int i=0;i<tableWidget->rowCount();i++)
          {
               QString cntx = tableWidget->item(i, 1)->text();
               QString acti = tableWidget->item(i, 2)->text();
               QString crid = tableWidget->item(i, 3)->text();

               int c_ca_id = crid.toInt();
               int curr_index = tbl->IndexOfColId(c_ca_id);

               // Jezeli usuwamy kolumne
               if(acti == "DELETE")
               {
                    if(curr_index == -1)
                    {
                         QMessageBox::critical(NULL, "HQEd", "Unknown ID: " + crid + ".", QMessageBox::Ok);
                         return false;
                    }
                    tbl->DeleteCol(curr_index);
               }
          }
		
          // Odswiezenie widolu tabel
          TableList->Refresh(ti);
		
		// Jezeli usunieto wszystkie kolumny, to kasujemy rowniez i tabele
		if(tbl->ColCount() == 0)
			TableList->Delete(ti);
     }

     xtt::fullVerification();
     xtt::logicalTableVerification(tbl);
     
     emit onClose();
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Apply' is pressed.
//
// #return No return value.
void TableEditor_ui::ClickApply(void)
{
     if(!applyChanges())
          return;
          
     fNew = false;
     reloadTables(fIndex);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Ok' is pressed.
//
// #return No return value.
void TableEditor_ui::ClickOk(void)
{
     if(applyChanges())
          close();
}
// -----------------------------------------------------------------------------

// #brief Function shows edit column window in adding new column mode.
//
// #return No return value.
void TableEditor_ui::NewCol(void)
{
     if(AttributeGroups->IsEmpty())
     {
          QMessageBox::information(NULL, "HQEd", "There is no attributes defined yet.", QMessageBox::Ok);
          return;
     }

     delete ColumnEdit;
     ColumnEdit = new ColumnEditor_ui(true, "", 0);
     connect(ColumnEdit, SIGNAL(onClose()), this, SLOT(AddColumnEvent()));
     ColumnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function shows edit column window.
//
// #return No return value.
void TableEditor_ui::EditCol(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no column selected.", QMessageBox::Ok);
          return;
     }

     if(tableWidget->item(tableWidget->currentRow(), 2)->text() == "DELETE")
     {
          QMessageBox::information(NULL, "HQEd", "You can't modify column whitch is designed for remove.", QMessageBox::Ok);
          return;
     }
     
     delete ColumnEdit;
     ColumnEdit = new ColumnEditor_ui(false, tableWidget->item(tableWidget->currentRow(), 0)->text(), clist.indexOf(tableWidget->item(tableWidget->currentRow(), 1)->text()));
     connect(ColumnEdit, SIGNAL(onClose()), this, SLOT(EditColumnEvent()));
     ColumnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when new column is added to the table.
//
// #return No return value.
void TableEditor_ui::AddColumnEvent(void)
{
     // Ustalenie indexu wstawiania
     int destIndex = 0;
     int newIndex = ColumnEdit->ContextIndex();

     // Petla wyszukujaca index wstawienia bierzacej kolumny
     for(int i=0;i<tableWidget->rowCount();i++)
     {
          int currIndex = clist.indexOf(tableWidget->item(i,1)->text())+1;
          if(currIndex == -1)
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect context name.", QMessageBox::Ok);
               return;
          }
          // Kiedy napotkamy na kontekst mniejszy lub rowny to zwiekszamy pozycje wstawienia
          if(newIndex >= currIndex)
               destIndex++;
     }

     // Sprawdzanie czy juz nie ma takiej kolumny
     for(int i=0;i<tableWidget->rowCount();i++)
     {
          if((tableWidget->item(i, 0)->text() == (ColumnEdit->GroupName() + ColumnEdit->AttributeName())) && (tableWidget->item(i, 1)->text() == clist.at(ColumnEdit->ContextIndex()-1)))
          {
               //QMessageBox::information(NULL, "HQEd", "Column already exists.", QMessageBox::Ok);
               //return;
          }
     }

     // Przygotowanie elementow do wstawienia
     QTableWidgetItem* _attr = new QTableWidgetItem(ColumnEdit->GroupName() + ColumnEdit->AttributeName());
     QTableWidgetItem* _cotx = new QTableWidgetItem(clist.at(ColumnEdit->ContextIndex()-1));
     QTableWidgetItem* _acti = new QTableWidgetItem("NEW");
     QTableWidgetItem* _crid = new QTableWidgetItem("-");

     // Powiekszenie ilosci wierszy
     tableWidget->insertRow(destIndex);

     // Wstawienie wartosci
     tableWidget->setItem(destIndex, 0, _attr);
     tableWidget->setItem(destIndex, 1, _cotx);
     tableWidget->setItem(destIndex, 2, _acti);
     tableWidget->setItem(destIndex, 3, _crid);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when changes of column edit are commited.
//
// #return No return value.
void TableEditor_ui::EditColumnEvent(void)
{
     // Ustalenie indexu wstawiania
     int destIndex = 0;
     int cr = tableWidget->currentRow();
     int newIndex = ColumnEdit->ContextIndex();
     int prewIndex = clist.indexOf(tableWidget->item(tableWidget->currentRow(), 1)->text())+1;

     // Zapisanie akcji i bierzacego indexu w tabeli
     QString action = tableWidget->item(tableWidget->currentRow(), 2)->text();
     QString cindex = tableWidget->item(tableWidget->currentRow(), 3)->text();

     // Sprawdzanie czy juz nie ma takiej kolumny
     for(int i=0;i<tableWidget->rowCount();i++)
     {
          if((i != cr) && (tableWidget->item(i, 0)->text() == (ColumnEdit->GroupName() + ColumnEdit->AttributeName())) && (tableWidget->item(i, 1)->text() == clist.at(ColumnEdit->ContextIndex()-1)))
          {
               //QMessageBox::information(NULL, "HQEd", "Column already exists.", QMessageBox::Ok);
               //return;
          }
     }

     // Jezeli zadna akcja nie byla wykonywana to zaznaczamy ze jest to edit
     // Dla akcji new, nie ma znaczenoa czy jest edit bo i tak musimy dodac nowa kolumne
     if(action == "-")
          action = "EDIT";

     // Usuniecie poprzedniego wiersza
     tableWidget->removeRow(cr);

     // Jezeli nie zmieniono kontekstu to zmieniamy tylko tresc, a nie musimy zmieniac
     // miejsca kolumny
     if(newIndex == prewIndex)
          destIndex = cr;

     if(newIndex != prewIndex)
     {
          // Petla wyszukujaca index wstawienia bierzacej kolumny
          for(int i=0;i<tableWidget->rowCount();i++)
          {
               int currIndex = clist.indexOf(tableWidget->item(i,1)->text())+1;
               if(currIndex == -1)
               {
                    QMessageBox::critical(NULL, "HQEd", "Incorrect context name.", QMessageBox::Ok);
                    return;
               }
               // Kiedy napotkamy na kontekst mniejszy lub rowny to zwiekszamy pozycje wstawienia
               if(newIndex >= currIndex)
                    destIndex++;
          }
     }

     // Przygotowanie elementow do wstawienia
     QTableWidgetItem* _attr = new QTableWidgetItem(ColumnEdit->GroupName() + ColumnEdit->AttributeName());
     QTableWidgetItem* _cotx = new QTableWidgetItem(clist.at(ColumnEdit->ContextIndex()-1));
     QTableWidgetItem* _acti = new QTableWidgetItem(action);
     QTableWidgetItem* _crid = new QTableWidgetItem(cindex);

     // Powiekszenie ilosci wierszy
     tableWidget->insertRow(destIndex);

     // Wstawienie wartosci
     tableWidget->setItem(destIndex, 0, _attr);
     tableWidget->setItem(destIndex, 1, _cotx);
     tableWidget->setItem(destIndex, 2, _acti);
     tableWidget->setItem(destIndex, 3, _crid);
     tableWidget->selectRow(destIndex);
}
// -----------------------------------------------------------------------------

// #brief Function deletes column from the table.
//
// #return No return value.
void TableEditor_ui::DeleteCol(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
          return;

     if(QMessageBox::question(NULL, "HQEd", "Do you really want to remove selecetd column?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     QString action = tableWidget->item(tableWidget->currentRow(), 2)->text();

     // Jezeli jest to nowa kolumna to mozemy ja wymazac z widoku
     if(action == "NEW")
          tableWidget->removeRow(tableWidget->currentRow());

     // Jezeli nie jest nowa to zaznaczamy ja do usuniecia
     if(action != "NEW")
          tableWidget->item(tableWidget->currentRow(), 2)->setText("DELETE");
}
// -----------------------------------------------------------------------------

// #brief Function undoes deleting column.
//
// #return No return value.
void TableEditor_ui::UnDelete(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
          return;

     QString action = tableWidget->item(tableWidget->currentRow(), 2)->text();

     // Jezeli jest to nowa kolumna to mozemy ja wymazac z widoku
     if(action != "DELETE")
     {
          QMessageBox::information(NULL, "HQEd", "This column is not designed for remove.", QMessageBox::Ok);
          return;
     }

     tableWidget->item(tableWidget->currentRow(), 2)->setText("EDIT");
}
// -----------------------------------------------------------------------------

// #brief Function moves chosen row up in the table.
//
// #return No return value.
void TableEditor_ui::MoveUp(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
          return;

     // Jezeli jest to juz najwyzszy index to nie da sie go przesuwac
     if(tableWidget->currentRow() == 0)
          return;

     if(tableWidget->item(tableWidget->currentRow(), 2)->text() == "DELETE")
          return;

     // Mozemy przesunac tylko wtedy kiedy nastepny kontekst jest taki sam, aby nie mieszac kontekstow
     if(tableWidget->item(tableWidget->currentRow(), 1)->text() == tableWidget->item(tableWidget->currentRow()-1, 1)->text())
     {
          int cr = tableWidget->currentRow();
          
          QString text1 = tableWidget->item(cr, 0)->text();
          QString text2 = tableWidget->item(cr, 1)->text();
          QString text3 = tableWidget->item(cr, 2)->text();
          QString text4 = tableWidget->item(cr, 3)->text();

          if(text3 != "NEW")
               text3 = "EDIT";

          tableWidget->item(cr, 0)->setText(tableWidget->item(cr-1, 0)->text());
          tableWidget->item(cr, 1)->setText(tableWidget->item(cr-1, 1)->text());
          tableWidget->item(cr, 2)->setText(tableWidget->item(cr-1, 2)->text());
          tableWidget->item(cr, 3)->setText(tableWidget->item(cr-1, 3)->text());
          tableWidget->item(cr-1, 0)->setText(text1);
          tableWidget->item(cr-1, 1)->setText(text2);
          tableWidget->item(cr-1, 2)->setText(text3);
          tableWidget->item(cr-1, 3)->setText(text4);
          tableWidget->selectRow(cr-1);
     }
}
// -----------------------------------------------------------------------------

// #brief Function moves chosen row down in the table.
//
// #return No return value.
void TableEditor_ui::MoveDown(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
          return;

     // Jezeli jest to juz najwyzszy index to nie da sie go przesuwac
     if(tableWidget->currentRow() == (tableWidget->rowCount()-1))
          return;

     if(tableWidget->item(tableWidget->currentRow(), 2)->text() == "DELETE")
          return;

     // Mozemy przesunac tylko wtedy kiedy nastepny kontekst jest taki sam, aby nie mieszac kontekstow
     if(tableWidget->item(tableWidget->currentRow(), 1)->text() == tableWidget->item(tableWidget->currentRow()+1, 1)->text())
     {
          int cr = tableWidget->currentRow();
          
          QString text1 = tableWidget->item(cr, 0)->text();
          QString text2 = tableWidget->item(cr, 1)->text();
          QString text3 = tableWidget->item(cr, 2)->text();
          QString text4 = tableWidget->item(cr, 3)->text();

          if(text3 != "NEW")
               text3 = "EDIT";

          tableWidget->item(cr, 0)->setText(tableWidget->item(cr+1, 0)->text());
          tableWidget->item(cr, 1)->setText(tableWidget->item(cr+1, 1)->text());
          tableWidget->item(cr, 2)->setText(tableWidget->item(cr+1, 2)->text());
          tableWidget->item(cr, 3)->setText(tableWidget->item(cr+1, 3)->text());
          tableWidget->item(cr+1, 0)->setText(text1);
          tableWidget->item(cr+1, 1)->setText(text2);
          tableWidget->item(cr+1, 2)->setText(text3);
          tableWidget->item(cr+1, 3)->setText(text4);
          tableWidget->selectRow(cr+1);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return valu
void TableEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               ClickCancel();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
