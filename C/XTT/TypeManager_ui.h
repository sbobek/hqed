/**
 * \file	TypeManager_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	14.08.2008
 * \version	1.15
 * \brief	This file contains class definition that arranges the TypeManager window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef TypeManagerH
#define TypeManagerH
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include <QTableWidgetItem>

#include "ui_TypeManager.h"
// -----------------------------------------------------------------------------

/**
* \class 	TypeManager_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	14.08.2008
* \brief 	Class definition that arranges the TypeManager window.
*/
class TypeManager_ui : public QWidget, private Ui_typeManager
{
     Q_OBJECT

     QGridLayout* formLayout;	                    ///< Pointer to grid layout form.
     QGridLayout* gp1Layout;	                    ///< Pointer to groupBox1 grid layout.   

     int _selected;                               ///< The index of selected row
     
public:

     /// \brief Constructor TypeManager_ui class.
     ///
     /// \param parent Pointer to parent object.
     TypeManager_ui(QWidget *parent = 0);
     
     /// \brief Destructor TypeManager_ui class.
     ~TypeManager_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent*);
     
     /// \brief Function returns the idnex of selected row
	///
	/// \return No return value.
     int result(void);
     
public slots:

     /// \brief Function reads the informations about types and fulfill the table
	///
	/// \return No return value.
     void readTypes(void);
     
     /// \brief Function triggered when new button is pressed
	///
	/// \return No return value.
     void addNewTypeClick(void);
     
     /// \brief Function triggered when edit button is pressed
	///
	/// \return No return value.
     void editTypeClick(void);
     
     /// \brief Function triggered when delete button is pressed
	///
	/// \return No return value.
     void deleteTypeClick(void);
     
     /// \brief Function triggered when select button is pressed
	///
	/// \return No return value.
     void selectTypeClick(void);
     
     /// \brief Function triggered when close button is pressed
	///
	/// \return No return value.
     void closeClick(void);
     
     /// \brief Function triggered when the cell is double clicked
     ///
     /// \param __row - the cell's row
     /// \param __col - the cell's col
     /// \return No return value.
     void cellDoubleClicked(int __row, int __col);
     
signals:

     /// \brief Signal emited when the type is deleted
	///
	/// \return No return value.
     void onDelete(void);
     
     /// \brief Signal emited when select button is clicked
	///
	/// \return No return value.
     void onSelect(void);
     
     /// \brief Signal emited when select button is clicked
	///
	/// \return No return value.
     void onClose(void);
};
// -----------------------------------------------------------------------------

extern TypeManager_ui* TypeManager;
// -----------------------------------------------------------------------------
#endif
