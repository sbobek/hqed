#ifndef EXPRESSIONPARSERERROR_H
#define EXPRESSIONPARSERERROR_H

#include <QString>
#include "ExpressionLexer.h"
#include "ParserToken.h"

class ExpressionParserError
{
public:
    enum ErrorType {
        ET_NO_ERROR,
        ET_EXPRESSION_EMPTY,
        ET_UNKNOWN_ERROR,
        ET_UNDEFINED_TOKEN,
        ET_PARENTHESIS_MISMATCH,
        ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT,
        ET_INVALID_RANGE_DEFINITION,
        ET_INVALID_USE_OF_SEPARATOR,
        ET_MISSING_SEPARATOR,
        ET_INVALID_USE_OF_FUNCTION_NAME,
        ET_WRONG_ARGUMENT_COUNT,
        ET_TOP_LEVEL_NOT_FUNCTION,
        ET_ECMA_NO_INPUT_FUNCTION
    };

    ExpressionParserError()
        : m_ErrorType(ET_NO_ERROR), m_LexerToken(), m_ParserToken()
    {}

    ExpressionParserError(ErrorType error, LexerToken token)
        :  m_ErrorType(error), m_LexerToken(token), m_ParserToken()
    {}

    ExpressionParserError(ErrorType error, ParserToken token)
        : m_ErrorType(error), m_LexerToken(), m_ParserToken(token)
    {}

    ErrorType errorType() { return m_ErrorType; }
    LexerToken lexerToken() { return m_LexerToken; }
    ParserToken parserToken() { return m_ParserToken; }
    QString errorTypeToString(ErrorType type);
    QString toString();

private:
    ErrorType m_ErrorType;
    LexerToken m_LexerToken;
    ParserToken m_ParserToken;
};

#endif // EXPRESSIONPARSERERROR_H
