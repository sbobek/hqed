#ifndef CONDITIONALCELLEDITOR_H
#define CONDITIONALCELLEDITOR_H

#include "QWidget"

class QSizeGrip;
class QVBoxLayout;
class QGraphicsItem;
class CellExpressionTextEditor;
class XTT_Cell;
class hExpression;
class hType;

class CellEditorAdapter : public QWidget
{
    Q_OBJECT

public:
    CellEditorAdapter(XTT_Cell *cell, QWidget *parent = 0);

    void setMinimumSize(QSize size);
    void focus();
    void adjustEditorSize();

signals:
    void editingFinished();
    void editingCancelled();

public slots:
    void slotEditorFinished();
    void slotEditorCancelled();

private:
    CellExpressionTextEditor *m_CellEditor;
    XTT_Cell *m_EditedCell;
    QVBoxLayout *m_Layout;
};

#endif // CONDITIONALCELLEDITOR_H
