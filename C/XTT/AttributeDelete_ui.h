 /**
 * \file	AttributeDelete_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition arranges window that can delete attributes. CONTROLLER
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ATTRIBUTEDELETE_H
#define ATTRIBUTEDELETE_H
// -----------------------------------------------------------------------------
 
#include "ui_AttributeDelete.h"
// -----------------------------------------------------------------------------
/**
* \class 	AttributeDelete_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	This file contains class definition arranges window that can delete attributes. CONTROLLER
*/
class AttributeDelete_ui : public QWidget, private Ui_DeleteAttribute
{
    Q_OBJECT

private:

     unsigned int fGroupIndex;	///< Index of group from which attributes are to be deleted.

public:

        /// \brief Constructor AttributeDelete_ui class.
	///
	/// \param parent Pointer to parent object.
        AttributeDelete_ui(QWidget *parent = 0);

        /// \brief Constructor AttributeDelete_ui class.
	///
	/// \param GroupIndex Index of group from which attributes are to be deleted.
        /// \param parent Pointer to parent object.
        AttributeDelete_ui(int GroupIndex, QWidget *parent = 0);

	/// \brief Read attributes from the group.
	///
	/// \return No return value.
	void ReadAttributes(void);

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
    void keyPressEvent(QKeyEvent*);

public slots:

	/// \brief Delete attribute that was chosen.
	///
	/// \return No return value.
    void DeleteClick(void);

	/// \brief Show window that enable selecting group.
	///
	/// \return No return value.
	void SelectGroupClick(void);

	/// \brief Function is triggered when select group window is closed. Group is chosen at that time.
	///
	/// \return No return value.
	void OnSelectGroupChange(void);

	/// \brief Function is triggered when attribute delete window is closed. Table view is refreshed at that time.
	///
	/// \return No return value.
	void OnClose(void);
};
// -----------------------------------------------------------------------------

extern AttributeDelete_ui* AttributeDelete;
// -----------------------------------------------------------------------------

#endif
