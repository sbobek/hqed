#include "ExpressionParser.h"
#include "ExpressionLexer.h"

#include "hExpression.h"
#include <QDebug>
#include <QQueue>

#include "hType.h"
#include "hSet.h"
#include "hSetItem.h"
#include "hDomain.h"
#include "hFunction.h"
#include "hFunctionArgument.h"
#include "hFunctionList.h"
#include "XTT_Cell.h"
#include "XTT_AttributeGroups.h"
#include "hTypeList.h"
#include "hMultipleValue.h"

#include "ns_xtt.h"

ExpressionParser::ExpressionParser(const ExpressionParserContext &context)
    : m_ParserContext(context), m_ParsedExpression(0)
{
    fillIdentifierLists();
    m_Lexer.setOperatorRepresentations(m_OperatorRepresentations);
}

ExpressionParser::~ExpressionParser()
{
    doCleanup();
}

hExpression* ExpressionParser::parseExpressionString(const QString &expressionString)
{
    doCleanup();

    m_ExpressionString = expressionString;

    bool lexerOk;
    m_LexerTokens = m_Lexer.tokenize(m_ExpressionString, lexerOk);
    if(!lexerOk) {
        // Error during lexer phase
        if(m_LexerTokens.isEmpty()) {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_EXPRESSION_EMPTY, LexerToken());
            return 0;
        } else {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_UNDEFINED_TOKEN, m_LexerTokens.last());
            return 0;
        }
    }

    if(!processLexerResult()) {
        // Error during lexer result processing phase
        return 0;
    }

    if(!toRPN()) {
        // Error during shunting yard phase
        return 0;
    }

    if(!rpnToExpression()) {
        // Error during hExpression tree construction
        return 0;
    }

    return m_ParsedExpression->copyTo();
}

void ExpressionParser::doCleanup()
{
    qDeleteAll(m_ParserTokens);
    m_ParserTokens.clear();
    m_LexerTokens.clear();
    m_TokenContextStack.clear();
    for(int i = 0; i < m_RPNToExprStack.size(); i++) {
        if(m_RPNToExprStack.at(i).first) {
            delete m_RPNToExprStack.at(i).first;
        }
        if(m_RPNToExprStack.at(i).second) {
            delete m_RPNToExprStack.at(i).second;
        }
    }

    m_ParserError = ExpressionParserError();
}

void ExpressionParser::fillIdentifierLists()
{
    // Initialize operators list
    QList<hFunction*> operators = funcList->findOperators(false);
    foreach(hFunction *op, operators) {
        m_OperatorRepresentations.append(op->operatorRepresentation());
        m_OperatorObjectMapping.insert(op->operatorRepresentation(), op);
        m_OperatorPrecedence.insert(op->operatorRepresentation(), op->operatorPrecedence());
        m_OperatorLeftAssociativity.insert(op->operatorRepresentation(), op->isOperatorLeftAssociative());
    }

    // Initialize function names list
    for(int i = 0; i < funcList->size(); i++) {
        if(!funcList->at(i)->isOperator()) {
            m_FunctionNames.append(funcList->at(i)->userRepresentation());
        }
    }

    // Initialize attribute names list
    for(unsigned int i = 0; i < AttributeGroups->Count(); i++) {
        for(unsigned int j = 0; j < AttributeGroups->Group(i)->Count(); j++) {
            m_AttributeNames.append(AttributeGroups->Group(i)->AttributeNameList());
        }
    }
    m_AttributeNames.removeDuplicates();

    // Initialize special values list
    m_SpecialValues.append("null");
    m_SpecialValues.append("any");
    m_SpecialValues.append("min");
    m_SpecialValues.append("max");

    for(int i = 0; i < typeList->size(); i++) {
        hType *type = typeList->at(i);
        if(type->baseType() == SYMBOLIC_BASED) {
            foreach(QString symbolicValue, type->domain()->stringItems()) {
                m_SymbolicValueTypes.insert(symbolicValue, type);
            }
        }
    }
}

bool ExpressionParser::processLexerResult()
{
    for(int i = 0; i < m_LexerTokens.size(); i++) {
        switch(m_LexerTokens.at(i).type()) {
        case LTT_IDENTIFIER_TOKEN:
        {
            // if we're inside a function, increase its argument count
            if(inFunction()) {
                ParserToken *contextToken = m_TokenContextStack.top();
                contextToken->setArgumentCount(contextToken->functionArgumentCount() + 1);
            }

            ParserTokenType identifierType = getIdentifierType(m_LexerTokens.at(i).text());
            switch(identifierType) {
            case PTT_FUNCTION:
            {
                if(inRange() || inSet() || !m_ParserContext.functionsAllowed()) {
                    m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                    return false;
                }

                if(i <= m_LexerTokens.size() - 3) {
                    if(m_LexerTokens.at(i + 1).type() != LTT_ROUND_PARENTHESIS_OPENING) {
                        m_ParserError = ExpressionParserError(ExpressionParserError::ET_INVALID_USE_OF_FUNCTION_NAME, m_LexerTokens.at(i));
                        return false;
                    }
                } else {
                    m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                    return false;
                }

                ParserToken *token = new ParserToken(PTT_FUNCTION, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
                token->setArgumentCount(0);
                m_ParserTokens.append(token);
                m_TokenContextStack.push(token);
                break;
            }
            case PTT_ATTRIBUTE:
            {
                if(inRange() || inSet() || (!m_ParserContext.attributesAllowed() && i > 0)) {
                    m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                    return false;
                }

                ParserToken *token = new ParserToken(PTT_ATTRIBUTE, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
                m_ParserTokens.append(token);
                break;
            }
            case PTT_SYMBOLIC_VALUE:
            {
                ParserToken *token = new ParserToken(PTT_SYMBOLIC_VALUE, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
                if(inSet() || inRange()) {
                    m_TokenContextStack.top()->childrenTokens()->append(token);
                } else {
                    m_ParserTokens.append(token);
                }
                break;
            }
            case PTT_SPECIAL_VALUE:
            {
                if(((m_LexerTokens.at(i).text() == "min" || m_LexerTokens.at(i).text() == "max") && !m_ParserContext.minMaxAllowed()) ||
                    (m_LexerTokens.at(i).text() == "any" && !m_ParserContext.anyAllowed()) ||
                    (m_LexerTokens.at(i).text() == "null" && !m_ParserContext.nullAllowed())) {
                    m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                    return false;
                }

                ParserToken *token = new ParserToken(PTT_SYMBOLIC_VALUE, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
                m_ParserTokens.append(token);
                if(inSet() || inRange()) {
                    m_TokenContextStack.top()->childrenTokens()->append(token);
                }
                break;
            }
            default:
            {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_UNKNOWN_ERROR, m_LexerTokens.at(i));
                return false;
            }
            }

            break;
        }
        case LTT_ROUND_PARENTHESIS_OPENING:
        {
            if(inRange() || inSet() || !m_ParserContext.functionsAllowed()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *token = new ParserToken(PTT_PARENTHESIS_OPENING, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
            m_ParserTokens.append(token);
            // If a parenthesis is not after a function name, then it creates new context
            if(i > 0) {
                if(getIdentifierType(m_LexerTokens.at(i - 1).text()) != PTT_FUNCTION) {
                    m_TokenContextStack.append(token);
                }
            } else if(i == 0) {
                m_TokenContextStack.append(token);
            }
            break;
        }
        case LTT_ROUND_PARENTHESIS_CLOSING:
        {
            if(inRange() || inSet() || !m_ParserContext.functionsAllowed()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                return false;
            }

            if(m_TokenContextStack.isEmpty()) {
                // closing parenthesis without an opening one
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                return false;
            }

            if(inRoundParenthesis()) {
                // we're leaving "()" parenthesis pair - pop current context off stack
                m_TokenContextStack.pop();
            }

            if(inFunction()) {
                // we're inside a function
                m_TokenContextStack.pop();
            }

            ParserToken *token = new ParserToken(PTT_PARENTHESIS_CLOSING, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
            m_ParserTokens.append(token);
            break;
        }
        case LTT_SET_PARENTHESIS_OPENING:
        {
            if(inRange() || inSet()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *token = new ParserToken(PTT_SET, QString(), m_LexerTokens.at(i).indexInString());
            m_ParserTokens.append(token);
            m_TokenContextStack.push(token);
            break;
        }
        case LTT_SET_PARENTHESIS_CLOSING:
        {
            if(m_TokenContextStack.isEmpty()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                return false;
            }

            if(m_TokenContextStack.top()->type() != PTT_SET) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *parentFunction = getParentFunctionToken();
            if(parentFunction != 0) {
                parentFunction->setArgumentCount(parentFunction->functionArgumentCount() + 1);
            }

            m_TokenContextStack.pop();
            break;
        }
        case LTT_RANGE_PARENTHESIS_OPENING:
        {
            if(inRange()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *token = new ParserToken(PTT_RANGE, QString(), m_LexerTokens.at(i).indexInString());
            if(inSet()) {
                m_TokenContextStack.top()->childrenTokens()->append(token);
            } else {
                m_ParserTokens.append(token);
            }
            m_TokenContextStack.push(token);
            break;
        }
        case LTT_RANGE_PARENTHESIS_CLOSING:
        {
            if(m_TokenContextStack.isEmpty()) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                return false;
            }

            if(m_TokenContextStack.top()->type() != PTT_RANGE) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.at(i));
                return false;
            }

            if(m_TokenContextStack.pop()->childrenTokens()->size() != 2) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_INVALID_RANGE_DEFINITION, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *parentFunction = getParentFunctionToken();
            if(parentFunction != 0) {
                parentFunction->setArgumentCount(parentFunction->functionArgumentCount() + 1);
            }

            break;
        }
        case LTT_INTEGER_TOKEN:
        case LTT_NUMERIC_TOKEN:
        {
            ParserToken *token = new ParserToken(PTT_NUMERIC_VALUE, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
            if(inSet() || inRange()) {
                m_TokenContextStack.top()->childrenTokens()->append(token);
            } else if(inFunction()) {
                m_ParserTokens.append(token);
                m_TokenContextStack.top()->setArgumentCount(m_TokenContextStack.top()->functionArgumentCount() + 1);
            } else {
                m_ParserTokens.append(token);
            }
            break;
        }
        case LTT_SEPARATOR:
        {
            if(!(inFunction() || inSet() || inRange())) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_INVALID_USE_OF_SEPARATOR, m_LexerTokens.at(i));
                return false;
            }

            if(inFunction()) {
                ParserToken *token = new ParserToken(PTT_SEPARATOR, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
                m_ParserTokens.append(token);
            }
            break;
        }
        case LTT_OPERATOR :
        {
            if(inSet() || inRange() || (!m_ParserContext.functionsAllowed() && i > 1)) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_ELEMENT_NOT_ALLOWED_IN_THIS_CONTEXT, m_LexerTokens.at(i));
                return false;
            }

            ParserToken *token = new ParserToken(PTT_OPERATOR, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
            m_ParserTokens.append(token);
            break;
        }
        case LTT_QUOTATION_TOKEN:
        {
            ParserToken *token = new ParserToken(PTT_QUOTATION_VALUE, m_LexerTokens.at(i).text(), m_LexerTokens.at(i).indexInString());
            if(inSet() || inRange()) {
                m_TokenContextStack.top()->childrenTokens()->append(token);
            } else if(inFunction()) {
                m_ParserTokens.append(token);
                m_TokenContextStack.top()->setArgumentCount(m_TokenContextStack.top()->functionArgumentCount() + 1);
            } else {
                m_ParserTokens.append(token);
            }
            break;
        }
        default :
        {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_UNDEFINED_TOKEN, m_LexerTokens.at(i));
            return false;
        }
        }
    }

    if(!m_TokenContextStack.isEmpty()) {
        m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, m_LexerTokens.last());
        return false;
    }

    return true;
}

bool ExpressionParser::toRPN()
{
    m_ShuntingYardOutputQueue.clear();

    QStack<ParserToken*> operatorStack;
    // Shunting yard algorithm implementation
    foreach(ParserToken *currentToken, m_ParserTokens) {
        switch(currentToken->type())
        {
        case PTT_SYMBOLIC_VALUE :
        case PTT_INTEGER_VALUE :
        case PTT_NUMERIC_VALUE :
        case PTT_QUOTATION_VALUE :
        case PTT_SET :
        case PTT_RANGE :
        case PTT_ATTRIBUTE :
        {
            m_ShuntingYardOutputQueue.append(currentToken);
            break;
        }

        case PTT_FUNCTION:
        {
            operatorStack.push(currentToken);
            break;
        }

        case PTT_OPERATOR:
        {
            while(!operatorStack.isEmpty()) {
                ParserToken *token = operatorStack.top();
                if(token->type() == PTT_OPERATOR && ((leftAssociativity(currentToken->text())
                                                 && (precedence(currentToken->text()) <= precedence(token->text())))
                                                 || (!leftAssociativity(currentToken->text())
                                                 && (precedence(currentToken->text()) < precedence(token->text()))))) {
                    m_ShuntingYardOutputQueue.append(token);
                    operatorStack.pop();
                } else {
                    break;
                }
            }

            operatorStack.push(currentToken);
            break;
        }

        case PTT_SEPARATOR:
        {
            bool parenthesesEncountered = false;
            while(!operatorStack.isEmpty()) {
                ParserToken *token = operatorStack.top();
                if(token->type() == PTT_PARENTHESIS_OPENING) {
                    parenthesesEncountered = true;
                    break;
                } else {
                    m_ShuntingYardOutputQueue.append(token);
                    operatorStack.pop();
                }
            }

            if(!parenthesesEncountered) {
                // this should never happen
                return false;
            }

            break;
        }

        case PTT_PARENTHESIS_OPENING:
        {
            operatorStack.push(currentToken);
            break;
        }

        case PTT_PARENTHESIS_CLOSING:
        {
            bool parenthesesEncountered = false;
            while(!operatorStack.isEmpty()) {
                ParserToken *token = operatorStack.top();
                if(token->type() == PTT_PARENTHESIS_OPENING) {
                    parenthesesEncountered = true;
                    break;
                } else {
                    m_ShuntingYardOutputQueue.append(token);
                    operatorStack.pop();
                }
            }

            if(!parenthesesEncountered) {
                m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, *currentToken);
                return false;
            }

            operatorStack.pop();
            if(!operatorStack.isEmpty()) {
                if(operatorStack.top()->type() == PTT_FUNCTION) {
                    m_ShuntingYardOutputQueue.append(operatorStack.pop());
                }
            }

            break;
        }
        default:
        {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_UNDEFINED_TOKEN, *currentToken);
            return false;
        }
        }
    }

    while(!operatorStack.isEmpty()) {
        ParserToken *token = operatorStack.top();
        if(token->type() == PTT_PARENTHESIS_OPENING || token->type() == PTT_PARENTHESIS_CLOSING) {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_PARENTHESIS_MISMATCH, *token);
            return false;
        }

        m_ShuntingYardOutputQueue.append(token);
        operatorStack.pop();
    }

    return true;
}

bool ExpressionParser::rpnToExpression()
{
    foreach(ParserToken *token, m_ShuntingYardOutputQueue) {
        switch(token->type())
        {
        case PTT_OPERATOR:
        case PTT_FUNCTION:
        {
            hExpression *expression = expressionFromToken(token, 0);
            if(!expression) {
                return false;
            }
            if(m_RPNToExprStack.size() < token->functionArgumentCount() && token->type() != PTT_OPERATOR) {
                delete expression;
                return false;
            } else {
                for(int i = token->functionArgumentCount() - 1; i >= 0; i--) {
                    if(m_RPNToExprStack.isEmpty()) {
                        m_ParserError = ExpressionParserError(ExpressionParserError::ET_WRONG_ARGUMENT_COUNT, *token);
                        delete expression;
                        m_ParsedExpression = 0;
                        return false;
                    }

                    expression->function()->argument(i)->value()->valueField()->flush();
                    if(m_RPNToExprStack.top().first != 0) { // argument of hSetItem type
                        expression->function()->argument(i)->value()->valueField()->add(m_RPNToExprStack.pop().first);
                        expression->function()->argument(i)->value()->setType(VALUE);
                    } else if(m_RPNToExprStack.top().second != 0) { // argument of hMultipleValue type
                        expression->function()->argument(i)->setValue(m_RPNToExprStack.pop().second, false);
                    }
                }
            }

            hSetItem *item = new hSetItem(new hSet(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(expression), new hValue(), false);
            m_RPNToExprStack.push(QPair<hSetItem*, hMultipleValue*>(item, 0));
            break;
        }

        case PTT_ATTRIBUTE:
        {
            hMultipleValue *multipleValue = multipleValueFromParserToken(token, 0);
            if(!multipleValue) {
                return false;
            }
            m_RPNToExprStack.push(QPair<hSetItem*, hMultipleValue*>(0, multipleValue));
            break;
        }

        case PTT_SYMBOLIC_VALUE:
        case PTT_INTEGER_VALUE:
        case PTT_NUMERIC_VALUE:
        case PTT_QUOTATION_VALUE:
        case PTT_RANGE:
        {
            hSetItem *item = setItemFromParserToken(token, 0);
            if(!item) {
                return false;
            }
            m_RPNToExprStack.push(QPair<hSetItem*, hMultipleValue*>(item, 0));
            break;
        }

        case PTT_SET:
        {
            hMultipleValue *multipleValue = multipleValueFromParserToken(token, 0);
            if(!multipleValue) {
                return false;
            }
            m_RPNToExprStack.push(QPair<hSetItem*, hMultipleValue*>(0, multipleValue));
            break;
        }

        default:
        {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_UNDEFINED_TOKEN, *token);
            return false;
        }
        }
    }

    if(m_RPNToExprStack.size() == 1) {
        m_ParsedExpression = m_RPNToExprStack.top().first->fromPtr()->expressionField();
        if(!m_ParsedExpression->setRequiredType(m_ParserContext.requiredTopLevelType()) ||
           m_ParsedExpression->function() == 0) {
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_TOP_LEVEL_NOT_FUNCTION, LexerToken());
            m_ParsedExpression = 0;
            return false;
        }
    } else {
        m_ParserError = ExpressionParserError(ExpressionParserError::ET_TOP_LEVEL_NOT_FUNCTION, LexerToken());
        m_ParsedExpression = 0;
        return false;
    }

    int firstArgumentType = FUNCTION_TYPE__SINGLE_VALUED;
    if(m_ParserContext.cell()->AttrType()->multiplicity())
         firstArgumentType = FUNCTION_TYPE__MULTI_VALUED;
    firstArgumentType = firstArgumentType | hFunction::typeFunctionRepresentation(m_ParserContext.cell()->AttrType()->Type());
    hMultipleValue* firstArgumentValue = new hMultipleValue(m_ParserContext.cell()->AttrType());
    hFunctionArgument* firstArgument = new hFunctionArgument;
    firstArgument->setEditable(false);
    firstArgument->setVisible(false);
    firstArgument->setValue(firstArgumentValue, true);
    firstArgument->setType(firstArgumentType);
    firstArgument->findCompatibleTypes(typeList);

    m_ParsedExpression->function()->setArgument(0, firstArgument);

    return true;
}

ParserTokenType ExpressionParser::getIdentifierType(const QString &token)
{
    if(m_FunctionNames.contains(token)) {
        return PTT_FUNCTION;
    } else if(m_AttributeNames.contains(token)) {
        return PTT_ATTRIBUTE;
    } else if(m_SpecialValues.contains(token)) {
        return PTT_SPECIAL_VALUE;
    } else {
        return PTT_SYMBOLIC_VALUE;
    }
}

bool ExpressionParser::inFunction()
{
    if(m_TokenContextStack.isEmpty()) {
        return false;
    } else if(m_TokenContextStack.top()->type() == PTT_FUNCTION) {
        return true;
    } else {
        return false;
    }
}

bool ExpressionParser::inRange()
{
    if(m_TokenContextStack.isEmpty()) {
        return false;
    } else if(m_TokenContextStack.top()->type() == PTT_RANGE) {
        return true;
    } else {
        return false;
    }
}

bool ExpressionParser::inSet()
{
    if(m_TokenContextStack.isEmpty()) {
        return false;
    } else if(m_TokenContextStack.top()->type() == PTT_SET) {
        return true;
    } else {
        return false;
    }
}

bool ExpressionParser::inRoundParenthesis()
{
    if(m_TokenContextStack.isEmpty()) {
        return false;
    } else if(m_TokenContextStack.top()->type() == PTT_PARENTHESIS_OPENING) {
        return true;
    } else {
        return false;
    }
}

ParserToken *ExpressionParser::getParentFunctionToken()
{
    for(int i = m_TokenContextStack.size() - 1; i >= 0; i--) {
        if(m_TokenContextStack.at(i)->type() == PTT_FUNCTION) {
            return m_TokenContextStack.at(i);
        }
    }

    return 0;
}

bool ExpressionParser::leftAssociativity(const QString &op)
{
    return m_OperatorLeftAssociativity.value(op);
}

int ExpressionParser::precedence(const QString &op)
{
    return m_OperatorPrecedence.value(op);
}

hType *ExpressionParser::typeOfSymbolicValue(const QString &symbolicValue)
{
    return m_SymbolicValueTypes.value(symbolicValue);
}

hSetItem *ExpressionParser::setItemFromParserToken(ParserToken *token, hType *requiredType)
{
    hSetItem *item = NULL;

    switch(token->type())
    {
    case PTT_INTEGER_VALUE:
    case PTT_NUMERIC_VALUE:
    case PTT_SYMBOLIC_VALUE:
    {
        //@KKR: HERE ?
        qDebug() << "ExpressionParser::setItemFromParserToken: SET_ITEM_TYPE_SINGLE_VALUE = " << token->text();
        item = new hSetItem(new hSet(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(token->text()), new hValue(), false);
        break;
    }
    case PTT_RANGE:
    {
        // value definition type - SYMBOLIC / NUMERIC
        //@KKR: HERE ?
        qDebug() << "ExpressionParser::setItemFromParserToken: SET_ITEM_TYPE_RANGE = [" << token->childrenTokens()->first()->text() << ", " << token->childrenTokens()->last()->text() << "]";
        item = new hSetItem(new hSet(), SET_ITEM_TYPE_RANGE,
                            new hValue(token->childrenTokens()->first()->text(), token->childrenTokens()->first()->text()),
                            new hValue(token->childrenTokens()->last()->text(), token->childrenTokens()->last()->text()),
                            false);
        item->fromPtr()->setDefinitionType(NUM);
        item->toPtr()->setDefinitionType(NUM);
        break;
    }
    case PTT_QUOTATION_VALUE:
    {
        qDebug() << "ExpressionParser::setItemFromParserToken: SET_ITEM_TYPE_QUOTATION_VALUE = " << token->text();

        int err;
        QString text = token->text().mid(1, token->text().length() - 2);
        QString val = m_ParserContext.requiredElementType()->translate(text, err, true);

        if (!err)
            item = new hSetItem(new hSet(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(val), new hValue(), false);
        else
            m_ParserError = ExpressionParserError(ExpressionParserError::ET_ECMA_NO_INPUT_FUNCTION, *token);

        break;
    }
    default:
    {
        // token type not recognized
        item = 0;
    }
    }

    return item;
}

hMultipleValue *ExpressionParser::multipleValueFromParserToken(ParserToken *token, hType *requiredType)
{
    hMultipleValue *multipleValue = new hMultipleValue();
    multipleValue->valueField()->flush();

    switch(token->type())
    {
    case PTT_SET:
    {
        foreach(ParserToken *token, *token->childrenTokens()) {
            hSetItem *item = setItemFromParserToken(token, requiredType);
            if(item != 0) {
                multipleValue->valueField()->append(item);
                multipleValue->setType(VALUE);
            }
        }
        break;
    }
    case PTT_ATTRIBUTE:
    {
        QList<XTT_Attribute*> *attributes = AttributeGroups->Attributes();
        foreach(XTT_Attribute *attribute, *attributes) {
            if(attribute->Name() == token->text()) {
                multipleValue->setAttribute(attribute);
                multipleValue->setType(ATTRIBUTE);
                break;
            }
        }
        break;
    }
    default:
    {
        delete multipleValue;
        multipleValue = 0;
    }
    }

    return multipleValue;
}

hExpression *ExpressionParser::expressionFromToken(ParserToken *token, hType *requiredType)
{
    if(token->type() != PTT_FUNCTION && token->type() != PTT_OPERATOR) {
        return 0;
    }

    hExpression *expression = new hExpression(requiredType);
    if(token->type() == PTT_OPERATOR) {
        expression->setFunction(m_OperatorObjectMapping.value(token->text())->copyTo());
    } else {
        int idx = funcList->findFunction(token->text(), 3);
        if(idx < 0) {
            delete expression;
            return 0;
        } else {
            expression->setFunction(funcList->at(idx)->copyTo());
        }
    }

    if(token->type() == PTT_OPERATOR) {
        // handle operators that accept only one value
        if(m_OperatorObjectMapping.value(token->text())->argCount() == 1) {
            token->setArgumentCount(1);
        } else {
            token->setArgumentCount(2);
        }
    }

    if(expression->function()->setArgCount(token->functionArgumentCount()) == FUNCTION_ERROR_ARG_CNT_OUT_OF_RANGE) {
        //m_ParserError = ExpressionParserError(ExpressionParserError::ET_NOT_ENOUGH_ARGUMENTS_FOR_FUNCTION_OPERATOR)
        delete expression;
        return 0;
    }

    hType *returnType = 0;
    int returnedTypes = expression->function()->returnedTypes();
    if(returnedTypes & FUNCTION_TYPE__BOOLEAN) {
        returnType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(BOOLEAN_BASED))));
    } else if(returnedTypes & FUNCTION_TYPE__INTEGER) {
        returnType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(INTEGER_BASED))));
    } else if(returnedTypes & FUNCTION_TYPE__NUMERIC) {
        returnType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(NUMERIC_BASED))));
    } else if(returnedTypes & FUNCTION_TYPE__OSYMBL) {
        returnType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(SYMBOLIC_BASED))));
    } else if(returnedTypes & FUNCTION_TYPE__UOSYMBL) {
        returnType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(SYMBOLIC_BASED))));
    }

    expression->function()->setRequiredType(returnType);
    return expression;
}

