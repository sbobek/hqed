/*
 *     $Id: ExpressionEditor_ui.cpp,v 1.16.4.3 2010-12-20 09:50:55 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/hSet.h"
#include "../../M/hSetItem.h"
#include "../../M/hValue.h"
#include "../../M/hType.h"
#include "../../M/hTypeList.h"
#include "../../M/hFunction.h"
#include "../../M/hFunctionList.h"
#include "../../M/hMultipleValue.h"
#include "../../M/hFunctionArgument.h"
#include "../../M/hExpression.h"

#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Table.h"

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "ValueEditor_ui.h"
#include "MultipleValueEditor_ui.h"
#include "ExpressionEditor_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor ExpressionEditor_ui class.
//
// #param parent Pointer to parent object.
ExpressionEditor_ui::ExpressionEditor_ui(QWidget* /*parent*/)
{    
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
ExpressionEditor_ui::~ExpressionEditor_ui(void)
{
     delete formLayout;
     delete gp1Layout;
     delete gp2Layout;
     delete _editedExpression;

     if(_sitem != NULL)
          delete _sitem;
    
     if(_mv != NULL)
          delete _mv;
   
     if(singleValueEditor != NULL)
          delete singleValueEditor;
   
     if(multipleValueEditor != NULL)
          delete multipleValueEditor;
     
     for(;_funcsPtrs.size();)
          delete _funcsPtrs.takeLast();
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void ExpressionEditor_ui::initForm(void)
{
     setupUi(this);

     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     layoutForm();
     hqed::centerWindow(this);
     
     connect(comboBox, SIGNAL(highlighted(int)), this, SLOT(functionHighlighted(int)));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(okButtonClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(cancelButtonClick()));
     
     _setClass = -1;
     _sitem = NULL;
     _mv = NULL;
     _cell = NULL;
     _cellPtr = NULL;
     _hidemode = false;
     singleValueEditor = NULL;
     multipleValueEditor = NULL;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void ExpressionEditor_ui::layoutForm(void)
{
     resize(450,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   1, 0, 1, 3);
     formLayout->addWidget(pushButton,   2, 1);
     formLayout->addWidget(pushButton_2, 2, 2);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(label_2,  0, 0);
     gp1Layout->addWidget(comboBox, 0, 1);
     gp1Layout->addWidget(label,    0, 2);
     gp1Layout->addWidget(spinBox,  0, 3);
     gp1Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);
     
     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(tableWidget,  0, 0);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);
}
// -----------------------------------------------------------------------------

// #brief Function checks if the current mode is correct
//
// #return true if the current mode is correct, otherwise false
bool ExpressionEditor_ui::isModeCorrect(void)
{
     if((_editedExpression == NULL) || (_resultType == NULL))
          return false;
     if((_resultMultiplicity != FUNCTION_TYPE__SINGLE_VALUED) && (_resultMultiplicity != FUNCTION_TYPE__MULTI_VALUED))
          return false;
     if((_setClass != 0) && (_setClass != 1) && (_setClass != 2) && (_setClass != 3))
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function sets the mode of window
//
// #param *__cell - pointer to the cell
// #return SET_WINDOW_MODE_xxx as a result
int ExpressionEditor_ui::setMode(XTT_Cell* __cell)
{
     if(__cell == NULL)
          return SET_WINDOW_MODE_FAIL;
     if(__cell->AttrType() == NULL)
          return SET_WINDOW_MODE_FAIL;
     if(__cell->AttrType()->Type() == NULL)
          return SET_WINDOW_MODE_FAIL;
     if(__cell->Content() == NULL)
          return SET_WINDOW_MODE_FAIL;
     if(__cell->Content()->expressionField() == NULL)
          return SET_WINDOW_MODE_FAIL;
          
     // GUI setup
     if(__cell->Context() == 1)
          label_2->setText("Relation type:");
     if(__cell->Context() == 4)
          label_2->setText("Operator type:");
          
     _cellPtr = __cell;
     XTT_Table* cellParent = __cell->TableParent();
     if(cellParent == NULL)
          return SET_WINDOW_MODE_FAIL;
     
     int res_mltpl = ((__cell->Context()==5) || (__cell->AttrType()->multiplicity()))?FUNCTION_TYPE__MULTI_VALUED:FUNCTION_TYPE__SINGLE_VALUED;
     int __setClass = (((__cell->Context()==4) || (__cell->Context()==5)) && (cellParent->TableType() != 0))?2:3;

     hType* req_type = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(BOOLEAN_BASED))));

     if(__cell->Context() == 4)
          req_type = typeList->specialType(VOID_BASED);
     if(__cell->Context() == 5)
          req_type = typeList->specialType(ACTION_BASED);

     return setMode(res_mltpl, req_type, __cell->enableAVR(), __cell->Content()->expressionField(), __setClass, __cell);
}
// -----------------------------------------------------------------------------

// #brief Function sets the mode of window
//
// #param int __result_multiplicity - denotes if the result should be single or/and multi valued
// #param hType* __required_type - the required type of result
// #param bool __avr - denotes if the AVR is enabled
// #param hExpression* __edited_expression - pointer to the edited expression object
// #param int __setClass - denotes the type of the set (0-set, 1-domain, 2-action context, 3-conditional context)
// #param *__cell - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
// #return SET_WINDOW_MODE_xxx as a result
int ExpressionEditor_ui::setMode(int __result_multiplicity, hType* __required_type, bool __avr, hExpression* __edited_expression, int __setClass, XTT_Cell* __cell)
{
     _hidemode = false;
     _resultMultiplicity = __result_multiplicity;
     _resultType = __required_type;
     _avr = __avr;
     _editedExpression = __edited_expression->copyTo();
     _setClass = __setClass;
     _cell = __cell;
     
     if(!isModeCorrect())
          return SET_WINDOW_MODE_FAIL;

     _editedExpression->setRequiredType(_resultType);
     _editedExpression->setAsMemberOfDomain(_setClass == 1);

     // Setting up the form
     _funcsPtrs = funcList->findFunctions(_resultType, _resultMultiplicity, true);
     comboBox->clear();
     for(int i=0;i<_funcsPtrs.size();++i)
          comboBox->addItem(_funcsPtrs.at(i)->userRepresentation());
          
     // Display item
     displayExpressionItem();

     connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(argCountChanged(int)));
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(functionSelectionChanged(int)));

     // when is only one function and nothing is selected
     if((_funcsPtrs.size() > 0) && (comboBox->currentIndex() == -1))
     {
          comboBox->setCurrentIndex(0);
          functionSelectionChanged(0);
     }

     // checking if the edit button should be called automatically
     if((comboBox->count() == 1) &&
        (_editedExpression->function() != NULL))  // can be null in case of Action cells
     {
          int enabledcount = 0;
          int enabledindex = -1;
          int facnt = _editedExpression->function()->argCount();
          for(int i=0;i<facnt;++i)
               if(_editedExpression->function()->argument(i)->isEditable())
               {
                    enabledcount++;
                    enabledindex = i;
               }
          if((enabledcount == 1) && (enabledindex > -1))
          {
               _hidemode = true;
               editButtonClick(enabledindex);
               return SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW;
          }
     }

     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function that sets up the window dialog according to expression parameters
//
// #return No return value.
void ExpressionEditor_ui::displayExpressionItem(void)
{
     spinBox->setEnabled(true);
     if(_editedExpression->function() == NULL)
     {
          comboBox->setCurrentIndex(-1);
          spinBox->setEnabled(false);
          return;
     }

     // If the expression object has already defined function object
     int currentFunctionObject = -1;
     for(int i=0;i<_funcsPtrs.size();++i)
          if(_funcsPtrs.at(i)->internalRepresentation() == _editedExpression->function()->internalRepresentation())
          {
               currentFunctionObject = i;
               break;
          }
     comboBox->setCurrentIndex(currentFunctionObject);
 
     // If function does not exists
     if(currentFunctionObject == -1)
     {
          spinBox->setEnabled(false);
          return;
     }

     // Setting up the arguments
     int facnt = _editedExpression->function()->argCount();
     int minargcount = _editedExpression->function()->minArgCount();
     int maxargcount = _editedExpression->function()->maxArgCount();
     if(maxargcount < 0)
          maxargcount = 0x7FFFFFFF;
     spinBox->setMinimum(minargcount);
     spinBox->setMaximum(maxargcount);
     spinBox->setValue(facnt);
     spinBox->setVisible(minargcount != maxargcount);
     label->setVisible(minargcount != maxargcount);

     argCountChanged(facnt);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ExpressionEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               cancelButtonClick();
               break;

          case Qt::Key_Return:
               okButtonClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the type comboBox is popuped up and the item selection is changed
//
// #param QString __text - highlighted text
// #return no value returned
void ExpressionEditor_ui::typeHighlighted(QString __text)
{
     QString text = "";
     
     // Searching for type
     int ti = typeList->indexOfName(__text);
     if(ti > -1)
          text = typeList->at(ti)->typeInfo(true);
     
     QToolTip::showText(QCursor::pos(), text);
}
// -----------------------------------------------------------------------------

void ExpressionEditor_ui::functionHighlighted(int __index)
{
     QString text = _funcsPtrs.at(__index)->description();
     QToolTip::showText(QCursor::pos(), text);
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the comboBox is changed
//
// #param int __index - new item index
// #return no value returned
void ExpressionEditor_ui::functionSelectionChanged(int __index)
{
     spinBox->setEnabled(__index != -1);

     if(__index == -1)
          return;
         
     _editedExpression->setFunction(_funcsPtrs.at(__index)->copyTo());

     int oval = spinBox->value();
     int minac = _funcsPtrs.at(__index)->minArgCount();
     int maxac = _funcsPtrs.at(__index)->maxArgCount();
     int cac = _funcsPtrs.at(__index)->argCount();

     spinBox->setMinimum(minac);
     if(maxac < 0)
          spinBox->setMaximum(0x7FFFFFFF);
     if(maxac >= 0)
          spinBox->setMaximum(maxac);
     spinBox->setValue(cac);

     if(oval == cac)
          argCountChanged(cac);
          
     label->setVisible(minac != maxac);
     spinBox->setVisible(minac != maxac);
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the number of argument has cahnged
//
// #param int __new_value - new number of arguments
// #return no value returned
void ExpressionEditor_ui::argCountChanged(int __new_value)
{
     QStringList col_hdrs = QStringList() << "Argument name" << "Argument type" << "Current value" << "Action";
     QStringList row_hdrs;
     
     for(int i=0;i<__new_value;++i)
           row_hdrs << QString::number(i+1);

     tableWidget->setRowCount(__new_value);
     tableWidget->setColumnCount(4);
     tableWidget->setColumnWidth(3, 50);
     
     tableWidget->setHorizontalHeaderLabels(col_hdrs);
     tableWidget->setVerticalHeaderLabels(row_hdrs);
     
     // Creating new instances of buttons
     while(_edbtns.size() < __new_value)
     {
          QPushButton* npb = new QPushButton("Edit...");
          _edbtns.append(npb);
          connect(npb, SIGNAL(clicked()), this, SLOT(onEditButtonClick()));

          QComboBox* ncb = new QComboBox(NULL);
          _cbxs.append(ncb);
          connect(ncb, SIGNAL(currentIndexChanged(int)), this, SLOT(typeComboBoxChange(int)));
          connect(ncb, SIGNAL(highlighted(QString)), this, SLOT(typeHighlighted(QString)));
          
          int row = _edbtns.size() - 1;
          tableWidget->setCellWidget(row, 1, _cbxs.at(row));
          tableWidget->setCellWidget(row, 3, _edbtns.at(row));
          tableWidget->setRowHeight(row, 20);
     }
     
     // Removing not necessary instances of buttons
     while(_edbtns.size() > __new_value)
     {
          delete _edbtns.takeLast();
          delete _cbxs.takeLast();
     }

     if(!_editedExpression->setArgumentCount(__new_value))
          QMessageBox::critical(NULL, "HQEd", _editedExpression->lastError(), QMessageBox::Ok);
        
     // When we edit the cell
     if(_cellPtr != NULL)
     {
          // Creating the new instance of the argument
          int first_argument_type = FUNCTION_TYPE__SINGLE_VALUED;
          if(_cellPtr->AttrType()->multiplicity())
               first_argument_type = FUNCTION_TYPE__MULTI_VALUED;
          first_argument_type = first_argument_type | hFunction::typeFunctionRepresentation(_cellPtr->AttrType()->Type());
          hMultipleValue* first_argument_value = new hMultipleValue(_cellPtr->AttrType());
          hFunctionArgument* first_argument = new hFunctionArgument;
          first_argument->setEditable(false);
          first_argument->setVisible(false);
          first_argument->setValue(first_argument_value, true);
          first_argument->setType(first_argument_type);
          first_argument->findCompatibleTypes(typeList);
          
          delete first_argument_value;

          if(_editedExpression->function()->setArgument(0, first_argument) != FUNCTION_ERROR_SUCCESS)
               QMessageBox::critical(NULL, "HQEd", _editedExpression->function()->lastError(), QMessageBox::Ok);
     }
          
     // Displaying the names, types and values of the arguments
     for(int i=0;i<__new_value;++i)
     {    
          // Name of the argument
          QString arg_name = _editedExpression->function()->argumentName(i);
          QTableWidgetItem* itm = new QTableWidgetItem(arg_name);
          tableWidget->setItem(i, 0, itm);
          
          // Types of the argument
          _cbxs.at(i)->clear();
          for(int t=0;t<_editedExpression->function()->argument(i)->compatibleTypes()->size();++t)
               _cbxs.at(i)->addItem(_editedExpression->function()->argument(i)->compatibleTypes()->at(t)->name());
          _cbxs.at(i)->setCurrentIndex(_editedExpression->function()->argument(i)->currentTypeIndex());
          
          // Values of the arguments
          QString str_arg_value = _editedExpression->function()->argument(i)->value()->toString();
          itm = new QTableWidgetItem(str_arg_value);
          tableWidget->setItem(i, 2, itm);
          
          _edbtns.at(i)->setEnabled(_editedExpression->function()->argument(i)->isEditable());
          _cbxs.at(i)->setEnabled((_editedExpression->function()->argument(i)->isEditable()) && (_cellPtr == NULL));
     }
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the type selector is changed
//
// #return no value returned
void ExpressionEditor_ui::typeComboBoxChange(int __newIndex)
{
     QWidget* w = QApplication::focusWidget();
     int cb_index = _cbxs.indexOf((QComboBox*)w);
     if(cb_index == -1)
          return;
     
     int argIndex = cb_index;
     int tpeIndex = __newIndex;
     
     if((_editedExpression == NULL) || (_editedExpression->function() == NULL))
          return;
         
     bool check_contraints = ((_setClass != 1) && (!_editedExpression->function()->argument(argIndex)->isAVRused()));
     if(!_editedExpression->function()->argument(argIndex)->setValueParentType(_editedExpression->function()->argument(argIndex)->compatibleTypes()->at(tpeIndex), check_contraints))
     {
          _cbxs.at(cb_index)->setCurrentIndex(_editedExpression->function()->argument(argIndex)->currentTypeIndex());
          QMessageBox::critical(NULL, "HQEd", _editedExpression->function()->argument(argIndex)->lastError(), QMessageBox::Ok);
          return;
     }
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the edit button is pressed
//
// #return no value returned
void ExpressionEditor_ui::onEditButtonClick(void)
{
     QWidget* w = QApplication::widgetAt(QCursor::pos());
     int index = _edbtns.indexOf((QPushButton*)w);
     editButtonClick(index);
}
// -----------------------------------------------------------------------------

// #brief Function that executes action of edit button click
//
// #param __btnindex - index of the clicked button
// #return no value returned
void ExpressionEditor_ui::editButtonClick(int __btnindex)
{
     if((__btnindex < 0) || (__btnindex >= _editedExpression->function()->argCount()))
          return;
     
     _editedArgument = __btnindex;
     
     // Executing value editor dialog
     int vm = _editedExpression->function()->argument(__btnindex)->valueMultiplicity();
     if(vm == FUNCTION_RESULT_TYPE_NOT_DEFINED)
     {
          QMessageBox::critical(NULL, "Expression Editor", "Not defined argument multiplicity.", QMessageBox::Ok);
          return;
     }
     
     bool sngl_req = (vm == FUNCTION_TYPE__SINGLE_VALUED);

     // Editor of singlevalued arguments
     if(sngl_req)
     {
          hType* __type = _editedExpression->function()->argument(__btnindex)->value()->parentType();
          _sitem = _editedExpression->function()->argument(__btnindex)->value()->valueField()->at(0)->copyTo();

          int __setClass = _setClass==1?0:_setClass;
          bool __avr = _avr;

          if(singleValueEditor != NULL)
               delete singleValueEditor;
          singleValueEditor = new ValueEditor_ui;

          connect(singleValueEditor, SIGNAL(okClicked()), this, SLOT(singleValueEditorOK()));
          connect(singleValueEditor, SIGNAL(windowClosed()), this, SLOT(singleValueEditorClose()));

          int smr = singleValueEditor->setMode(__type, _sitem, __setClass, sngl_req, __avr, _cell);
          if(smr == SET_WINDOW_MODE_FAIL)
          {
               QMessageBox::critical(NULL, "Expression Editor", "Incorrect value editor mode.", QMessageBox::Ok);
               delete _sitem;
               return;
          }
          if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
               return;

          singleValueEditor->show();
     }
     
     // Editor of multivalued arguments
     if(!sngl_req)
     {
          hType* __type = _editedExpression->function()->argument(__btnindex)->value()->parentType();
          _mv = _editedExpression->function()->argument(__btnindex)->value()->copyTo();

          int __setClass = _setClass==1?0:_setClass;
          bool __avr = _avr;

          if(multipleValueEditor != NULL)
               delete multipleValueEditor;
          multipleValueEditor = new MultipleValueEditor_ui;

          connect(multipleValueEditor, SIGNAL(okClicked()), this, SLOT(multipleValueEditorOK()));
          connect(multipleValueEditor, SIGNAL(windowClosed()), this, SLOT(multipleValueEditorClose()));

          int smr = multipleValueEditor->setMode(__type, _mv, __setClass, sngl_req, __avr, _cell);
          if(smr == SET_WINDOW_MODE_FAIL)
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect value editor mode.", QMessageBox::Ok);
               delete _sitem;
               return;
          }
          if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
               return;

          multipleValueEditor->show();
     }
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the singleValueEditor window is closed by OK button
//
// #return no value returned
void ExpressionEditor_ui::singleValueEditorOK(void)
{
     hSetItem* result = singleValueEditor->result();
     _editedExpression->function()->argument(_editedArgument)->value()->valueField()->flush();
     _editedExpression->function()->argument(_editedArgument)->value()->valueField()->add(result);
     _editedExpression->function()->argument(_editedArgument)->value()->setType(VALUE);
     
     tableWidget->item(_editedArgument, 2)->setText(_editedExpression->function()->argument(_editedArgument)->value()->toString());
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the singleValueEditor window is closed
//
// #return no value returned
void ExpressionEditor_ui::singleValueEditorClose(void)
{
     delete _sitem;
     _sitem = NULL;
     if(_hidemode)
          okButtonClick();
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the multipleValueEditor window is closed by OK button
//
// #return no value returned
void ExpressionEditor_ui::multipleValueEditorOK(void)
{
     hMultipleValue* result = multipleValueEditor->result();
     _editedExpression->function()->argument(_editedArgument)->setValue(result, false);

     tableWidget->item(_editedArgument, 2)->setText(_editedExpression->function()->argument(_editedArgument)->value()->toString());
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the multipleValueEditor window is closed
//
// #return no value returned
void ExpressionEditor_ui::multipleValueEditorClose(void)
{
     delete _mv;
     _mv = NULL;
     if(_hidemode)
          okButtonClick();
}
// -----------------------------------------------------------------------------

// #brief Function that return the result of this dialog
//
// #return the result of this dialog, that is the pointer to the new expression object
hExpression* ExpressionEditor_ui::result(void)
{    
     return _editedExpression->copyTo();
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the ok button is clicked
//
// #return no value returned
void ExpressionEditor_ui::okButtonClick(void)
{
     if(comboBox->currentIndex() == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "No function selected.", QMessageBox::Ok);
          return;
     }

     // Verification of the expression
     if(_setClass != 1)
     {
          QString __ver_msgs = "";
          if(!_editedExpression->expressionVerification(__ver_msgs))
          {
               QMessageBox::critical(NULL, "HQEd", "Expression error: " + __ver_msgs, QMessageBox::Ok);
               return;
          }
     }

     // If the expression should be placed in the cell
     if(_cellPtr != NULL)
     {
          _cellPtr->Content()->setExpression(result());
          xtt::fullVerification();
          xtt::logicalTableVerification(_cellPtr->TableParent());
     }

     emit okClicked();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function trigered when the cancel button is clicked
//
// #return no value returned
void ExpressionEditor_ui::cancelButtonClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the window is going to be showed
//
// #param QCloseEvent* event - pointer to the object that contains the event parameters
// #return No return value.
void ExpressionEditor_ui::showEvent(QShowEvent* /*event*/)
{
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the window is going to be closed
//
// #param QCloseEvent* event - pointer to the object that cntains the event parameters
// #return No return value.
void ExpressionEditor_ui::closeEvent(QCloseEvent* /*event*/)
{
     emit windowClose();
}
// -----------------------------------------------------------------------------
