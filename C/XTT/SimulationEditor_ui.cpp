/*
 *	   $Id: SimulationEditor_ui.cpp,v 1.9.8.1 2011-06-13 17:02:13 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../namespaces/ns_hqed.h"
#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"
#include "../Settings_ui.h"

#include "../../M/hSet.h"
#include "../../M/hMultipleValue.h"
#include "../../M/hMultipleValueResult.h"
#include "../../M/XTT/XTT_Row.h"
#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Attribute.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "xttSimulationParams.h"
#include "SetEditor_ui.h"
#include "SimulationEditor_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor UnusedAtrtribute_ui class.
//
// #param parent Pointer to parent object.
SimulationEditor_ui::SimulationEditor_ui(QWidget*)
{
    initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor PluginEditor_ui class.
SimulationEditor_ui::~SimulationEditor_ui(void)
{
     delete sw1p1layout;
     delete sw1p0layout;
     delete gp3Layout;
     delete gp2Layout;
     delete gp1Layout;
     delete formLayout;

     if(_cbTables != NULL)
          delete _cbTables;
     if(_lwTables != NULL)
          delete _lwTables;
     if(_allTables != NULL)
          delete _allTables;
     if(_atts != NULL)
          delete _atts;
     if(_states != NULL)
          delete _states;
     if(_sets != NULL)
     {
          for(;_sets->size();)
               delete _sets->takeFirst();
          delete _sets;
     }
     for(;_btns.size();)
          delete _btns.takeFirst();
     if(_setEditor != NULL)
          delete _setEditor;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void SimulationEditor_ui::initForm(void)
{
    setupUi(this);
    hqed::centerWindow(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    setWindowModality(Qt::ApplicationModal);
    setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(onDeleteTableClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(onAddTableClick()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(startClick()));
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(cancelClick()));
    connect(radioButton_4, SIGNAL(clicked()), this, SLOT(onStateDefTypeChanged()));
    connect(radioButton_5, SIGNAL(clicked()), this, SLOT(onStateDefTypeChanged()));
    connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(onStateSelectionChanged(int)));
    
    _allTables = NULL;
    _cbTables = NULL;
    _lwTables = NULL;
    _atts = NULL;
    _sets = NULL;
    _setEditor = NULL;
    _state = NULL;
    _states = NULL;
    _bntIndex = -1;

    layoutForm();
    onStateDefTypeChanged();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the mode of the dialog
//
// #return SET_WINDOW_MODE_xxx value as a result
int SimulationEditor_ui::setMode(void)
{
     // Reading tables
     _cbTables = new QList<XTT_Table*>;
     _lwTables = new QList<XTT_Table*>;
    
     _allTables = TableList->tablesOrder();
     hqed::invertList(_allTables);
     for(int i=0;i<_allTables->size();++i)
          _lwTables->append(_allTables->at(i));
     displayTables();
     
     // Reading states
     _states = States->states();
     comboBox_2->addItems(States->names());
     
     // Reading attributes
     _sets = new QList<hSet*>;
     _atts = AttributeGroups->Attributes();
     tableWidget_2->setRowCount(0);
     tableWidget_2->setColumnWidth(1, 50);
     tableWidget_2->setColumnWidth(2, 50);
     tableWidget_2->setColumnWidth(3, 40);
     for(int i=0;i<_atts->size();++i)
     {    
          QPushButton* npb = new QPushButton("Edit...");
          _btns.append(npb);
          connect(npb, SIGNAL(clicked()), this, SLOT(onEditButtonClick()));
          
          tableWidget_2->setRowCount(i+1);
          tableWidget_2->setRowHeight(i, 18);
          hMultipleValueResult hmvr = _atts->at(i)->Value()->value();
          if(!(bool)hmvr)
               _sets->append(new hSet(_atts->at(i)->Type()));
           if((bool)hmvr)
               _sets->append(hmvr.toSet(true));
          
          tableWidget_2->setItem(i, 0, new QTableWidgetItem(_atts->at(i)->Path()));
          tableWidget_2->setItem(i, 1, new QTableWidgetItem(_sets->at(i)->toString()));
          tableWidget_2->setCellWidget(i, 2, npb);
     }
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void SimulationEditor_ui::layoutForm(void)
{    
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   1, 0, 1, 3);
     formLayout->addWidget(groupBox_3,   2, 0, 1, 3);
     formLayout->addWidget(pushButton_3, 3, 1);
     formLayout->addWidget(pushButton_4, 3, 2);
     formLayout->setRowStretch(0, 1);
     formLayout->setRowStretch(2, 2);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);

     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(listWidget,   0, 0, 2, 1);
     gp1Layout->addWidget(comboBox,     2, 0);
     gp1Layout->addWidget(pushButton,   1, 1);
     gp1Layout->addWidget(pushButton_2, 2, 1);
     gp1Layout->setRowStretch(0, 1);
     gp1Layout->setColumnStretch(0, 1);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);

     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(radioButton,   0, 0);
     gp2Layout->addWidget(radioButton_2, 0, 1);
     gp2Layout->addWidget(radioButton_3, 0, 2);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);

     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(radioButton_5, 0, 0);
     gp3Layout->addWidget(radioButton_4, 0, 1);
     gp3Layout->addWidget(stackedWidget, 1, 0, 1, 3);
     gp3Layout->setRowStretch(1, 1);
     gp3Layout->setColumnStretch(2, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);

     sw1p0layout = new QGridLayout;
     sw1p0layout->addWidget(comboBox_2,  0, 0, 1, 2);
     sw1p0layout->addWidget(label_2,     1, 0);
     sw1p0layout->addWidget(tableWidget, 2, 0, 1, 2);
     sw1p0layout->setRowStretch(2, 1);
     sw1p0layout->setColumnStretch(1, 1);
     hqed::setupLayout(sw1p0layout);
     stackedWidget->widget(0)->setLayout(sw1p0layout);

     sw1p1layout = new QGridLayout;
     sw1p1layout->addWidget(label_4,       0, 0);
     sw1p1layout->addWidget(tableWidget_2, 1, 0, 1, 2);
     sw1p1layout->setRowStretch(1, 1);
     sw1p1layout->setColumnStretch(1, 1);
     hqed::setupLayout(sw1p1layout);
     stackedWidget->widget(1)->setLayout(sw1p1layout);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the user clicks edit button
//
// #return No return value.
void SimulationEditor_ui::onEditButtonClick(void)
{
     QWidget* w = QApplication::widgetAt(QCursor::pos());
     _bntIndex = _btns.indexOf((QPushButton*)w);
     if(_bntIndex == -1)
          return;
     
     _setEditor = new SetEditor_ui;
     connect(_setEditor, SIGNAL(okClicked()), this, SLOT(onSetEditorClose()));
     int smr = _setEditor->setMode(_atts->at(_bntIndex)->Type(), _sets->at(_bntIndex), 3, !_atts->at(_bntIndex)->multiplicity(), false, NULL);
     
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect mode of set editor.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
     
     _setEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the set editor is closed by ok button
//
// #return No return value.
void SimulationEditor_ui::onSetEditorClose(void)
{
     if(_bntIndex == -1)
          return;
          
     _setEditor->disconnect();
     hSet* result = _setEditor->result();
     delete _sets->at(_bntIndex);
     _sets->replace(_bntIndex, result);
     tableWidget_2->item(_bntIndex, 1)->setText(_sets->at(_bntIndex)->toString());
}
// -----------------------------------------------------------------------------

// #brief Function that updates listWidget and comboBox w.r.t. content of _cbTables and _lwTables
//
// #return no values returns
void SimulationEditor_ui::displayTables(void)
{
     int cbii = comboBox->currentIndex();
     int lwii = listWidget->currentRow();
     
     comboBox->clear();
     listWidget->clear();
     
     for(int i=0;i<_cbTables->size();++i)
          comboBox->addItem(_cbTables->at(i)->TableId() + ": " + _cbTables->at(i)->Title());
     for(int i=0;i<_lwTables->size();++i)
          listWidget->addItem(_lwTables->at(i)->TableId() + ": " + _lwTables->at(i)->Title());
          
     cbii = cbii < comboBox->count() ? cbii : comboBox->count()-1;
     lwii = lwii < listWidget->count() ? lwii : listWidget->count()-1;
     
     comboBox->setCurrentIndex(cbii);
     listWidget->setCurrentRow(lwii);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the user press add table button
//
// #return No return value.
void SimulationEditor_ui::onAddTableClick(void)
{
     int cbii = comboBox->currentIndex();
     
     if(cbii == -1)
          return;
          
     _lwTables->append(_cbTables->at(cbii));
     _cbTables->removeAt(cbii);
     displayTables();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the user press remove table button
//
// #return No return value.
void SimulationEditor_ui::onDeleteTableClick(void)
{
     int lwii = listWidget->currentRow();
     
     if(lwii == -1)
          return;
          
     _cbTables->append(_lwTables->at(lwii));
     _lwTables->removeAt(lwii);
     displayTables();
}
// -----------------------------------------------------------------------------

// #brief Function that checks simulation parameters
//
// #return true if everything is correct, otherwise false
bool SimulationEditor_ui::verifySimParams(void)
{
     if(_lwTables->size() == 0)
     {
          QMessageBox::critical(NULL, "HQEd", "Select at least one table.", QMessageBox::Ok);
          return false;
     }
     
     if(_atts->size() != _sets->size())
     {
          QMessageBox::critical(NULL, "HQEd", "Critical error, contact with author.", QMessageBox::Ok);
          return false;
     }
     
     if(simType() == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Please, select simulation type.", QMessageBox::Ok);
          return false;
     }
     
     if((radioButton_5->isChecked()) && (comboBox_2->currentIndex() == -1))
     {
          QMessageBox::critical(NULL, "HQEd", "Please, select state from the list.", QMessageBox::Ok);
          return false;
     }
     
     if(radioButton_4->isChecked())
     {
          int nes = 0;
          for(int i=0;i<_sets->size();++i)
               if(!_sets->at(i)->isEmpty())
                    nes++;
          if(nes == 0)
          {
               QMessageBox::critical(NULL, "HQEd", "You must define at least one value of an attribute.", QMessageBox::Ok);
               return false;
          }
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when ok key is pressed.
//
// #return No return value.
void SimulationEditor_ui::startClick(void)
{
     if(!verifySimParams())
          return;
          
     emit onStartClick();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when cancel key is pressed.
//
// #return No return value.
void SimulationEditor_ui::cancelClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the user change type of the state definition
//
// #return No return value.
void SimulationEditor_ui::onStateDefTypeChanged(void)
{
     if(radioButton_5->isChecked())
     {
          stackedWidget->setCurrentIndex(0);
          
          int ii = comboBox_2->currentIndex();
          if((ii >= 0) && (ii < _states->size()))
               _state = _states->at(ii);
     }
     if(radioButton_4->isChecked())
     {
          stackedWidget->setCurrentIndex(1);
          _state = NULL;
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the user change the selection of already defined state
//
// #param __stateIndex - index of the state
// #return No return value.
void SimulationEditor_ui::onStateSelectionChanged(int __stateIndex)
{
     if(__stateIndex == -1)
          return;
          
     int stind = __stateIndex;
     
     // cleaning table
     while(tableWidget->rowCount())
     {
          tableWidget->removeRow(0);
     }
     
     _state = _states->at(stind);
     QList<XTT_Attribute*>* atts = _state->inputAttributes();
     for(int a=0;a<atts->size();++a)
     {
          XTT_Attribute* att = atts->at(a);
          if(att == NULL)
          {
               QMessageBox::critical(NULL, "HQEd", "Undefined attribute in state \'" + _state->fullname() + "\'.", QMessageBox::Ok);
               return;
          }
          
          int rc = tableWidget->rowCount();
          tableWidget->setRowCount(rc+1);
          tableWidget->setRowHeight(rc, 18);
          
          QString aname = att->Path();
          QString avalue = _state->inputSetAt(att)->toString();
          
          tableWidget->setItem(rc, 0, new  QTableWidgetItem(aname));
          tableWidget->setItem(rc, 1, new  QTableWidgetItem(avalue));
     }
     delete atts;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void SimulationEditor_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               cancelClick();
               break;

          case Qt::Key_Return:
               startClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that returns list of tables that has been chosen for simulation
//
// #return list of tables that has been chosen for simulation
QStringList SimulationEditor_ui::simTables(void)
{
     QStringList result;
     for(int i=0;i<_lwTables->size();++i)
          result << hqed::mcwp(_lwTables->at(i)->Title());

     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns type of the simulation
//
// #return type of the simulation
int SimulationEditor_ui::simType(void)
{
     if(radioButton->isChecked())
          return DEV_PLUGIN_SIM_TYPE_DDI;
     if(radioButton_2->isChecked())
          return DEV_PLUGIN_SIM_TYPE_GDI;
     if(radioButton_3->isChecked())
          return DEV_PLUGIN_SIM_TYPE_TDI;
          
     return -1;
}
// -----------------------------------------------------------------------------

// #brief Function that returns initial state of the simulation
//
// #return initial state of the simulation as state name or state definition
QString SimulationEditor_ui::simState(void)
{
     QString res = "";

     // Already defined state
     if(radioButton_5->isChecked())
          res = hqed::mcwp(comboBox_2->currentText());
     
     // User defined state
     if(radioButton_4->isChecked())
     {
          res = "[";
          // Creating user defined state
          for(int i=0;i<_atts->size();++i)
          {
               QString obrace = "";
               QString cbrace = "";
               
               //qDebug() << _sets->at(i)->toString() << ", " << _sets->at(i)->isNotDefinedValue();
               if((_sets->at(i)->isEmpty()) || (_sets->at(i)->isNotDefinedValue()))
                    continue;
               if(_atts->at(i)->multiplicity())
               {
                    obrace = "[";
                    cbrace = "]";
               }
               
               res = res + "[" + hqed::mcwp(_atts->at(i)->Path()) + ", " + obrace + _sets->at(i)->toProlog() + cbrace + "], ";
          }
          if(res.size() >= 2)
               res = res.remove(res.size()-2, 2);      // removing last colon
          res = res + "]";
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief return the pointer to the already defined state, that has been used to run simulation. If state has been defined manually it returns NULL
//
// #return pointer to the already defined state
XTT_State* SimulationEditor_ui::statePtr(void)
{
     return _state;
}
// -----------------------------------------------------------------------------
