#ifndef SYNTAXHIGHLIGHTER_H
#define SYNTAXHIGHLIGHTER_H

#include <QSyntaxHighlighter>
#include <QHash>
#include <QTextCharFormat>

QT_BEGIN_NAMESPACE
class QTextDocument;
QT_END_NAMESPACE

class ExpressionSyntaxHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    ExpressionSyntaxHighlighter(QTextDocument *parent = 0);

    void setAttributeNames(const QStringList &names)
    {
        m_AttributeNames = names;
    }

    void setFunctionNames(const QStringList &names)
    {
        m_FunctionNames = names;
    }

    void setSymbolicNames(const QStringList &names)
    {
        m_SymbolicNames = names;
    }

    void setSpecialNames(const QStringList &names)
    {
        m_SpecialNames = names;
    }

    void refreshRules();

protected:
    void highlightBlock(const QString &text);

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> m_HighlightingRules;

    QTextCharFormat m_AttributeFormat;
    QTextCharFormat m_FunctionFormat;
    QTextCharFormat m_SymbolicFormat;
    QTextCharFormat m_SpecialFormat;
    QTextCharFormat m_NumericFormat;

    QStringList m_AttributeNames;
    QStringList m_FunctionNames;
    QStringList m_SymbolicNames;
    QStringList m_SpecialNames;
};

#endif // SYNTAXHIGHLIGHTER_H
