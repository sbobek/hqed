#ifndef EXPRESSIONPARSER_H
#define EXPRESSIONPARSER_H

#include <QStringList>
#include <QStack>
#include <QQueue>
#include <QMap>
#include <QHash>

#include "ExpressionLexer.h"
#include "ParserToken.h"
#include "XTT_Cell.h"
#include "ExpressionParserError.h"
#include "ExpressionParserContext.h"


class hExpression;
class hType;
class hFunction;
class XTT_Cell;
class hFunctionList;
class hTypeList;
class hSetItem;
class XTT_AttributeGroups;


class ExpressionParser
{
public:
    ExpressionParser(const ExpressionParserContext &context);
    ~ExpressionParser();

    hExpression* parseExpressionString(const QString &expressionString);
    ExpressionParserError getError() { return m_ParserError; }

private:
    void doCleanup();
    void fillIdentifierLists();
    bool processLexerResult();
    bool toRPN();
    bool rpnToExpression();

    ParserTokenType getIdentifierType(const QString &token);

    bool inFunction();
    bool inRange();
    bool inSet();
    bool inRoundParenthesis();
    ParserToken *getParentFunctionToken();

    bool leftAssociativity(const QString &op);
    int precedence(const QString &op);

    hType *typeOfSymbolicValue(const QString &symbolicValue);

    hSetItem *setItemFromParserToken(ParserToken *token, hType *requiredType);
    hMultipleValue *multipleValueFromParserToken(ParserToken *token, hType *requiredType);
    hExpression *expressionFromToken(ParserToken *token, hType *requiredType);

    ExpressionParserContext m_ParserContext;
    QString m_ExpressionString;
    hExpression *m_ParsedExpression;

    ExpressionLexer m_Lexer;
    ExpressionParserError m_ParserError;

    QList<LexerToken> m_LexerTokens;
    QList<ParserToken*> m_ParserTokens;
    QStack<ParserToken*> m_TokenContextStack;
    QQueue<ParserToken*> m_ShuntingYardOutputQueue;
    QStack<QPair<hSetItem*, hMultipleValue*> > m_RPNToExprStack;

    QStringList m_OperatorRepresentations;
    QMap<QString, hFunction*> m_OperatorObjectMapping;
    QMap<QString, int> m_OperatorPrecedence;
    QMap<QString, bool> m_OperatorLeftAssociativity;

    QHash<QString, hType*> m_SymbolicValueTypes;

    QStringList m_FunctionNames;
    QStringList m_AttributeNames;
    QStringList m_SpecialValues;
};

extern hFunctionList *funcList;
extern hTypeList *typeList;
extern XTT_AttributeGroups *AttributeGroups;

#endif // EXPRESSIONPARSER_H
