/*
 *	   $Id: CellEditor_ui.cpp,v 1.36 2010-01-08 19:47:03 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QCursor>
#include <QKeyEvent>

#include "TableEditor_ui.h"
#include "ExpressionEditor_ui.h"
#include "widgets/MyGraphicsScene.h"

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"
#include "../MainWin_ui.h"
#include "../Settings_ui.h"

#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/hType.h"
#include "../../M/hMultipleValue.h"

#include "CellEditor_ui.h"
// -----------------------------------------------------------------------------

CellEditor_ui* CellEdit;
// -----------------------------------------------------------------------------

// #brief Constructor CellEditor_ui class.
//
// #param parent Pointer to parent object.
CellEditor_ui::CellEditor_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);
    
    connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(SpinButtonChange(int)));
    connect(tableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(TableDoubleClick(int, int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    
    _expr_editor = NULL;
}
// -----------------------------------------------------------------------------

// #brief Constructor CellEditor_ui class.
//
// #param tableIndex Index of table that was read.
// #param parent Pointer to parent object.
CellEditor_ui::CellEditor_ui(int tableIndex, QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);
    
    connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(SpinButtonChange(int)));
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(TableChange(int))); 
    connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickOK()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(EditTable()));
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(applyClick()));
    connect(toolButton, SIGNAL(clicked()), this, SLOT(MoveDown()));
    connect(toolButton_2, SIGNAL(clicked()), this, SLOT(MoveUp()));
    connect(tableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(TableDoubleClick(int, int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    
    _expr_editor = NULL;
    reloadTables(tableIndex);
}
// -----------------------------------------------------------------------------

// #brief Function reads tables.
//
// #return No return value.
void CellEditor_ui::ReadTabels(void)
{
     // Wczytywanie informacji o tabelach
     comboBox->clear();

     for(unsigned int i=0;i<TableList->Count();i++)
     {
          QString title = TableList->Table(i)->Title();
          QString desc = TableList->Table(i)->Description();
          QString line = title;
          if(desc != "")
               line += " - [" + desc + "]";
          comboBox->addItem(line);
     }
}
// -----------------------------------------------------------------------------

// #brief Function updates window when chosen table is changed.
//
// #param itemIndex Index of chosen table.
// #return No return value.
void CellEditor_ui::TableChange(int itemIndex)
{
     if(itemIndex == -1)
          return;

     XTT_Table* tbl = TableList->Table(itemIndex);

     // Ustawienie liczby wierszy i kolumn
     tableWidget->setColumnCount((int)tbl->ColCount());
     spinBox->setValue((int)tbl->RowCount());

     // Ustawianie naglowkow kolumn
     QTableWidgetItem** _attr = new QTableWidgetItem*[tbl->ColCount()];
     for(int i=0;i<tbl->ColCount();i++)
     {
          int context = tbl->ColContext(i);
          QString aname = "";
          if(context != 5)
               aname = AttributeGroups->CreatePath(tbl->ContextAtribute((unsigned int)i)) + tbl->ContextAtribute((unsigned int)i)->Name();
          if(aname == "#NAN")
          {
               QMessageBox::critical(NULL, "HQEd", "Missing pointer.", QMessageBox::Ok);
               return;
          }
          // Ustalanie contekstu i dodanie znacznika kontekstu
          QStringList ctx_smbl;
          ctx_smbl << "(?) " << "(+) " << "(-) " << "(->) " << "*";
          aname = ctx_smbl.at(tbl->ColContext(i)-1) + aname;

          _attr[i] = new QTableWidgetItem(aname);
          tableWidget->setHorizontalHeaderItem(i, _attr[i]);
     }

     // Ustawianie tresci komorek
     QStringList ctx, atype;
     ctx << "conditional" << "assert" << "retract" << "action" << "*";
     for(unsigned int i=0;i<tbl->RowCount();i++)
     {
          for(unsigned int j=0;j<tbl->Row(i)->Length();j++)
          {
               QTableWidgetItem* _content = new QTableWidgetItem(tbl->Row(i)->Cell(j)->Content()->toString());
               int context = tbl->ColContext(j);
               XTT_Attribute* attr = NULL;
               if(context != 5)
                    attr = tbl->ContextAtribute(j);
               QString tt = "";
               tt = "Column information:\n\tContext: " + ctx.at(context-1);
               if(context != 5)
               {
                    tt += "\n\tArgument name: " + attr->Name();
                    tt += "\n\tArgument acronym: " + attr->Acronym();
                    tt += "\n\tArgument type: " + attr->Type()->name();
                    tt += "\n\tArgument domain: " + attr->ConstraintsRepresentation();
               }
               tt += "\nCell information:\n\tContent: \"" + tbl->Row(i)->Cell(j)->Content()->toString() + "\"";
               _content->setToolTip(tt);

               tableWidget->setItem(i, j, _content);
          }
     }

     // Ustawianie wymiarow kolumn
     for(int j=0;j<tbl->ColCount();j++)
          tableWidget->setColumnWidth((int)j, tbl->ColWidth(j));

     // Ustawianie wymiarow wierszy
     for(unsigned int j=0;j<tbl->RowCount();j++)
          tableWidget->setRowHeight((int)j, tbl->RowHeight(j));
}
// -----------------------------------------------------------------------------

// #brief Function that reloads the list of tables
//
// #param __destinationIndex - destination index of the combobox
// #return No return value.
void CellEditor_ui::reloadTables(int __destinationIndex)
{
    ReadTabels();
    comboBox->setCurrentIndex(__destinationIndex);
    TableChange(__destinationIndex);
}
// -----------------------------------------------------------------------------

// #brief Function shows edit table window. The table for editing was chosen in combo box.
//
// #return No return value.
void CellEditor_ui::EditTable(void)
{
     if(comboBox->currentIndex() == -1)
     {
          const QString str2 = "There is no table selected.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }


     delete TableEdit;
     TableEdit = new TableEditor_ui(false, comboBox->currentIndex());
     connect(TableEdit, SIGNAL(onClose()), this, SLOT(EditTableEvent()));
     TableEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when edited table is commited.
//
// #return No return value.
void CellEditor_ui::EditTableEvent(void)
{
     int cc = comboBox->currentIndex();
     ReadTabels();
     comboBox->setCurrentIndex(cc);
}
// -----------------------------------------------------------------------------

// #brief Function that saves the changes
//
// #return true if everything is ok otherwise false
bool CellEditor_ui::applyChanges(void)
{
     if(comboBox->currentIndex() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no table selected.", QMessageBox::Ok);
          return false;
     }

     // Zapisanie wskaznika do tabeli
     XTT_Table* tbl = TableList->Table(comboBox->currentIndex());

     // Alokacja pamieci na komorki
     for(;tbl->RowCount() < (unsigned int)tableWidget->rowCount();)
          tbl->AddRow();
     for(;tbl->RowCount() > (unsigned int)tableWidget->rowCount();)
          tbl->DeleteRow(tbl->RowCount()-1);

     // Sprawdzanie poprawnosci wpisanych formul
     /*for(int i=0;i<tableWidget->rowCount();i++)
           for(int j=0;j<tableWidget->columnCount();j++)
           {
               // Jezeli nie poprawny formula
               if((!tbl->Row(i)->Cell(j)->Analyze(tableWidget->item(i, j)->text())) && (Settings->xttShowErrorCellMessage()))
               {
                    // Jezeli chcemy poprawic
                    if(QMessageBox::warning(NULL, "HQEd", tbl->Row(i)->Cell(j)->CellError() + "\nClick \"Retry\" for improve.\nClick \"Ignore\" for continue.", QMessageBox::Retry | QMessageBox::Ignore) == QMessageBox::Retry)
                    {
                         tableWidget->clearSelection();
                         tableWidget->item(i, j)->setSelected(true);
                         return;
                    }
               }
           }*/


     // Ustawianie wymiarow kolumn
     for(int j=0;j<tableWidget->columnCount();j++)
          tbl->setColWidth(j, tableWidget->columnWidth(j));

     // Ustawianie wymiarow wierszy
     for(int j=0;j<tableWidget->rowCount();j++)
          tbl->setRowHeight(j, tableWidget->rowHeight(j));

     // Odswiezenie widoku tabel
     TableList->Refresh(comboBox->currentIndex());
     
     xtt::fullVerification();
     xtt::logicalTableVerification(tbl);
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button OK is pressed.
//
// #return No return value.
void CellEditor_ui::ClickOK(void)
{
     if(applyChanges())
          close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button apply is clicked.
//
// #return No return value.
void CellEditor_ui::applyClick(void)
{
     if(!applyChanges())
          return;
          
     reloadTables(comboBox->currentIndex());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when spin object is changed. It changes row number of table.
//
// #param rowCount Row number of table.
// #return No return value.
void CellEditor_ui::SpinButtonChange(int rowCount)
{
     int cc = tableWidget->rowCount();

     // Usuwanie nadmiarowycj komorek
     if(cc > rowCount)
     {
          for(int i=rowCount;i<cc;i++)
               for(int j=0;j<tableWidget->columnCount();j++)
                    delete tableWidget->item(i, j);
     }

     tableWidget->setRowCount(rowCount);

     if(cc >= rowCount)
          return;

     if(comboBox->currentIndex() == -1)
          return;
     XTT_Table* tbl = TableList->Table(comboBox->currentIndex());
     
     // Memory allocating for the new rows
     for(int i=tbl->RowCount();i<rowCount;++i)
          tbl->AddRow();
     
     // Jezeli zwiekszamy to musimy zaalokowac miejsce na nowe komorki
     for(int i=cc;i<rowCount;i++)
     {
          for(int j=0;j<tableWidget->columnCount();j++)
          {
               QTableWidgetItem* _content = new QTableWidgetItem("");
               tableWidget->setItem(i, j, _content);
          }
		tableWidget->setRowHeight(i, Settings->xttDefaultRowHeight());
     }

     // Wypiasnie zawartosci komorek, jezeli w zakresie
     if(((unsigned int)rowCount <= tbl->RowCount()) && (rowCount > 0))
     {
          for(unsigned int j=0;j<tbl->Row(rowCount-1)->Length();j++)
          {
               QTableWidgetItem* _content = new QTableWidgetItem(tbl->Row(rowCount-1)->Cell(j)->Content()->toString());
               tableWidget->setItem(rowCount-1, j, _content);
			tableWidget->setRowHeight(j, tbl->RowHeight(j));
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Function moves one row down in the table.
//
// #return No return value.
void CellEditor_ui::MoveDown(void)
{
     int currRow = tableWidget->currentRow();
     int currCol = tableWidget->currentColumn();
     int tableIndex = comboBox->currentIndex();

     if(tableIndex == -1)
          return;

     if(TableList->Table(tableIndex)->ExchangeRows(currRow, currRow+1))
     {
          tableWidget->clearSelection();
          TableChange(tableIndex);
          tableWidget->setCurrentCell(currRow+1, currCol);
     }
}
// -----------------------------------------------------------------------------

// #brief Function moves one row up in the table.
//
// #return No return value.
void CellEditor_ui::MoveUp(void)
{
     int currRow = tableWidget->currentRow();
     int currCol = tableWidget->currentColumn();
     int tableIndex = comboBox->currentIndex();

     if(tableIndex == -1)
          return;

     if(TableList->Table(tableIndex)->ExchangeRows(currRow, currRow-1))
     {
          tableWidget->clearSelection();
          TableChange(tableIndex);
          tableWidget->setCurrentCell(currRow-1, currCol);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when table cell is double clicked.
//
// #param row Row of the table.
// #param col Column of the table.
// #return No return value.
void CellEditor_ui::TableDoubleClick(int row, int col)
{
     int tableIndex = comboBox->currentIndex();

     if(tableIndex == -1)
          return;
          
     XTT_Cell* cell = TableList->Table(tableIndex)->Row(row)->Cell(col);
          
     if(_expr_editor != NULL)
          delete _expr_editor;
          
     _expr_editor = new ExpressionEditor_ui;
     QObject::connect(_expr_editor, SIGNAL(okClicked()), MainWin->xtt_scene, SLOT(update()));
     QObject::connect(_expr_editor, SIGNAL(windowClose()), this, SLOT(expressionEditorWindowClosed()));
     
     int smr = _expr_editor->setMode(cell);
     if(smr == SET_WINDOW_MODE_FAIL)
          return;
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
          
     _expr_editor->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the expression editor is closed
//
// #return No return value.
void CellEditor_ui::expressionEditorWindowClosed(void)
{
     if(_expr_editor != NULL)
     {
          delete _expr_editor;
          _expr_editor = NULL;
     }
          
     // Refresh view
     int tableIndex = comboBox->currentIndex();
     if(tableIndex == -1)
          return;
          
     TableChange(tableIndex);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void CellEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               ClickOK();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
