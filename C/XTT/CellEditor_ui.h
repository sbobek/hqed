 /**
 * \file	CellEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges cell table editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef CELLEDITORH
#define CELLEDITORH
// -----------------------------------------------------------------------------
 
#include "ui_CellEditor.h"
#include <QMouseEvent>
// -----------------------------------------------------------------------------

class ExpressionEditor_ui;
// -----------------------------------------------------------------------------
/**
* \class 	CellEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges cell table editor window.
*/
class CellEditor_ui : public QWidget, private Ui_CellEditor
{
private:

    Q_OBJECT
    
    ExpressionEditor_ui* _expr_editor;  ///< Pointer to the expresion editor window

public:

     /// \brief Constructor CellEditor_ui class.
     ///
     /// \param parent Pointer to parent object.
     CellEditor_ui(QWidget *parent = 0);

     /// \brief Constructor CellEditor_ui class.
     ///
     /// \param tableIndex Index of table that was read.
     /// \param parent Pointer to parent object.
     CellEditor_ui(int tableIndex, QWidget *parent = 0);

     /// \brief Function reads tables.
     ///
     /// \return No return value.
     void ReadTabels(void);
     
     /// \brief Function that reloads the list of tables
     ///
     /// \param __destinationIndex - destination index of the combobox
     /// \return No return value.
     void reloadTables(int __destinationIndex);
     
     /// \brief Function that saves the changes
     ///
     /// \return true if everything is ok otherwise false
     bool applyChanges(void);

     /// \brief Function is triggered when key is pressed.
     ///
     /// \param event Pointer to event object.
     /// \return No return value.
     void keyPressEvent(QKeyEvent* event);

public slots:

	/// \brief Function is triggered when button OK is pressed.
	///
	/// \return No return value.
	void ClickOK(void);
     
     /// \brief Function is triggered when button apply is clicked.
	///
	/// \return No return value.
     void applyClick(void);

        /// \brief Function updates window when chosen table is changed.
	///
	/// \param itemIndex Index of chosen table.
	/// \return No return value.
	void TableChange(int itemIndex);

	/// \brief Function is triggered when spin object is changed. It changes row number of table.
	///
	/// \param rowCount Row number of table.
	/// \return No return value.
	void SpinButtonChange(int rowCount);

	/// \brief Function shows edit table window. The table for editing was chosen in combo box.
	///
	/// \return No return value.
	void EditTable(void);

	/// \brief Function is triggered when edited table is commited.
	///
	/// \return No return value.
	void EditTableEvent(void);

	/// \brief Function moves one row down in the table.
	///
	/// \return No return value.
	void MoveDown(void);

	/// \brief Function moves one row up in the table.
	///
	/// \return No return value.
	void MoveUp(void);

	/// \brief Function is triggered when table cell is double clicked.
	///
	/// \param row Row of the table.
	/// \param col Column of the table.
	/// \return No return value.
	void TableDoubleClick(int row, int col);
     
     /// \brief Function is triggered when the expression editor is closed
	///
	/// \return No return value.
     void expressionEditorWindowClosed(void);
};
// -----------------------------------------------------------------------------

extern CellEditor_ui* CellEdit;
// -----------------------------------------------------------------------------

#endif
