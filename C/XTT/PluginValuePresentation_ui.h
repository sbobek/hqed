 /**
 * \file      ValuePresentationScriptEditor_ui.h
 * \author    Krzysztof Kaczor kinio4444@gmail.com
 * \date      11.01.2010
 * \version   1.0
 * \brief     This file contains class definition arranges PluginValuePresentation window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef PLUGINVALUEPRESENTATION_UI
#define PLUGINVALUEPRESENTATION_UI
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_PluginValuePresentation.h"
// -----------------------------------------------------------------------------

class devPlugin;
// -----------------------------------------------------------------------------

/**
* \class      PluginValuePresentation_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       11.01.2010
* \brief      This class arranges PluginValuePresentation window
*/
class PluginValuePresentation_ui : public QWidget, public Ui_PluginValuePresentation
{
    Q_OBJECT

private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     devPlugin* _plugin;                     ///< Pointer to the plugin
     
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function that lists the services from the plugin
	///
	/// \return No return value.
     void listServices(void);

public:

     /// \brief Constructor PluginValuePresentation_ui class.
     ///
     /// \param *parent - Pointer to parent.
     PluginValuePresentation_ui(QWidget *parent = 0);

     /// \brief Destructore PluginValuePresentation_ui class.
     ~PluginValuePresentation_ui(void);

     /// \brief Function that sets the initial values of the window class
     ///
     /// \return no values returns
     void initWindow(void);
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
     /// \brief Function that sets the mode of the dialog
     ///
     /// \param __plugin - pointer to the plugin
     /// \return window mode result type
     int setMode(devPlugin* __plugin);
     
public slots:
     
     /// \brief Function that is triggered when translate button is pressed
     ///
     /// \return No return value.
     void translateClick(void);
     
     /// \brief Function that is triggered when close button is pressed
     ///
     /// \return No return value.
     void closeClick(void);
};
// -----------------------------------------------------------------------------

#endif
