#ifndef EXPRESSIONTOKEN_H
#define EXPRESSIONTOKEN_H

#include "QString"

class ExpressionToken
{
public:
    enum TOKEN_TYPE
    {
        TT_UNKNOWN = 0,
        TT_CONSTANT_VALUE = 1,
        TT_ATTRIBUTE = 2,
        TT_FUNCTION = 3,
        TT_OPERATOR = 4,
        TT_SEPARATOR = 5,
        TT_LEFT_PARENTHESIS = 6,
        TT_RIGHT_PARENTHESIS = 7
    };

    ExpressionToken(const QString &text = QString())
    {
        m_Text = text;
        m_Type = TT_UNKNOWN;
        m_ArgCount = -1;
    }
    ~ExpressionToken() {};

    void setText(const QString &text) { m_Text = text; }
    void setType(TOKEN_TYPE type) { m_Type = type; }
    void setArgCount(int argCount) { m_ArgCount = argCount; }

    QString text() const { return m_Text; }
    TOKEN_TYPE type() const { return m_Type; }
    int argCount() const { return m_ArgCount; }

private:
    QString m_Text;
    TOKEN_TYPE m_Type;
    int m_ArgCount;
};

#endif // EXPRESSIONTOKEN_H
