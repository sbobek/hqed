#ifndef EXPRESSIONLEXER_H
#define EXPRESSIONLEXER_H

#include <QList>
#include <QRegExp>
#include <QStringList>

#include "LexerToken.h"

class ExpressionLexer
{
public:
    ExpressionLexer();

    QList<LexerToken> tokenize(const QString &expression, bool &ok);

    void setOperatorRepresentations(const QStringList &representations)
    {
        m_OperatorRepresentations = representations;
    }

    QStringList operatorRepresentations() const
    {
        return m_OperatorRepresentations;
    }

private:
    bool parseIfIdentifier();
    bool parseIfInteger();
    bool parseIfNumeric();
    bool parseIfParenthesis();
    bool parseIfOperator();
    bool parseIfSeparator();
    bool parseIfWhitespace();
    bool parseIfQuotation();

    QRegExp m_IdentifierTokenRegExp;
    QRegExp m_IntegerTokenRegExp;
    QRegExp m_NumericTokenRegExp;
    QRegExp m_WhitespaceRegExp;
    QRegExp m_QuotationTokenRegExp;

    QList<LexerToken> m_Tokens;
    int m_CurrentIndex;
    QString m_ExpressionString;

    QStringList m_OperatorRepresentations;
};

#endif // EXPRESSIONLEXER_H
