/*
 *     $Id: TypeManager_ui.cpp,v 1.14 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/hTypeList.h"
#include "../../M/hType.h"
#include "../../M/hDomain.h"

#include "../../namespaces/ns_hqed.h"

#include "TypeEditor_ui.h"
#include "TypeManager_ui.h"
// -----------------------------------------------------------------------------

TypeManager_ui* TypeManager;
// -----------------------------------------------------------------------------

// #brief Constructor TypeManager_ui class.
//
// #param parent Pointer to parent object.
TypeManager_ui::TypeManager_ui(QWidget* /*parent*/)
{
    initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor TypeManager_ui class.
TypeManager_ui::~TypeManager_ui(void)
{
     delete formLayout;
     delete gp1Layout;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void TypeManager_ui::initForm(void)
{
     setupUi(this);
     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     layoutForm();
     hqed::centerWindow(this);
     
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(addNewTypeClick()));
     connect(pushButton_4, SIGNAL(clicked()), this, SLOT(editTypeClick()));
     connect(pushButton_5, SIGNAL(clicked()), this, SLOT(deleteTypeClick()));
     
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(selectTypeClick()));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(closeClick()));
     
     connect(tableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(cellDoubleClicked(int, int)));
     
     readTypes();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void TypeManager_ui::layoutForm(void)
{
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,     0, 0, 1, 6);
     formLayout->addWidget(pushButton_3, 1, 0);
     formLayout->addWidget(pushButton_4, 1, 1);
     formLayout->addWidget(pushButton_5, 1, 2);
     formLayout->addWidget(pushButton_2, 1, 4);
     formLayout->addWidget(pushButton ,  1, 5);
     formLayout->setRowStretch(0, 1);
     formLayout->setColumnStretch(3, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(tableWidget, 0, 0);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void TypeManager_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               selectTypeClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the cell is double clicked
//
// #param int __row - the cell's row
// #param int __col - the cell's col
// #return No return value.
void TypeManager_ui::cellDoubleClicked(int /*__row*/, int /*__col*/)
{
     editTypeClick();
}
// -----------------------------------------------------------------------------

// #brief Function reads the informations about types and fulfill the table
//
// #return No return value.
void TypeManager_ui::readTypes(void)
{
     /*while(tableWidget->rowCount() > 0)
          tableWidget->removeRow(0);
     while(tableWidget->columnCount() > 0)
          tableWidget->removeColumn(0);*/
          
     int row_count = typeList->size()-hpTypesInfo.ptc;
     int col_count = 8;
     QStringList col_headers_labels;
     QStringList row_headers_labels = QStringList() << "Name" << "ID" << "Description" << "Base type" << "Constraints" << "Domain" << "Length" << "Scale";
     
     tableWidget->setRowCount(row_count);
     tableWidget->setColumnCount(col_count);
     
     for(int i=hpTypesInfo.ptc;i<typeList->size();++i)
     {
          int row = i-hpTypesInfo.ptc;
          int pti =  hpTypesInfo.iopti(typeList->at(i)->baseType());
          QString ptn = hpTypesInfo.ptn.at(pti);
          
          int ci = hpTypesInfo.ioci(typeList->at(i)->typeClass());
          QString cn = hpTypesInfo.cn.at(ci);
     
          tableWidget->setItem(row, 0, new QTableWidgetItem(typeList->at(i)->name()));
          tableWidget->setItem(row, 1, new QTableWidgetItem(typeList->at(i)->id()));
          tableWidget->setItem(row, 2, new QTableWidgetItem(typeList->at(i)->description()));
          tableWidget->setItem(row, 3, new QTableWidgetItem(ptn));
          tableWidget->setItem(row, 4, new QTableWidgetItem(cn));

          tableWidget->setItem(row, 5, new QTableWidgetItem(""));
          if(typeList->at(i)->typeClass() == TYPE_CLASS_DOMAIN)
          {
               QString itm_cnt = "not ordered\n";
               if(typeList->at(i)->domain()->ordered())
                    itm_cnt = "ordered\n";
               itm_cnt += typeList->at(i)->domain()->toString();
               tableWidget->item(row, 5)->setText(itm_cnt);
          }
               
          tableWidget->setItem(row, 6, new QTableWidgetItem(""));
          tableWidget->setItem(row, 7, new QTableWidgetItem(""));
          if((typeList->at(i)->baseType() == INTEGER_BASED) || (typeList->at(i)->baseType() == NUMERIC_BASED))
          {
               tableWidget->item(row, 6)->setText(QString::number(typeList->at(i)->length()));
               tableWidget->item(row, 7)->setText(QString::number(typeList->at(i)->scale()));
          }
     }
     tableWidget->setHorizontalHeaderLabels(row_headers_labels);
}
// -----------------------------------------------------------------------------

// #brief Function returns the idnex of selected row
//
// #return No return value.
int TypeManager_ui::result(void)
{
     return _selected;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when new button is pressed
//
// #return No return value.
void TypeManager_ui::addNewTypeClick(void)
{
     delete TypeEditor;
     TypeEditor = new TypeEditor_ui(true, -1, -1);
     connect(TypeEditor, SIGNAL(saveClicked()), this, SLOT(readTypes()));
     TypeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when edit button is pressed
//
// #return No return value.
void TypeManager_ui::editTypeClick(void)
{
     int currentIndex = tableWidget->currentRow();
     if(currentIndex == -1)
          return;
     
     delete TypeEditor;
     TypeEditor = new TypeEditor_ui(false, currentIndex, -1);
     connect(TypeEditor, SIGNAL(saveClicked()), this, SLOT(readTypes()));
     TypeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when delete button is pressed
//
// #return No return value.
void TypeManager_ui::deleteTypeClick(void)
{
     int currentIndex = tableWidget->currentRow();
     if(currentIndex == -1)
          return;
          
     int li = hpTypesInfo.ptc + currentIndex;
     if(li >= typeList->size())
          return;
          
     if(QMessageBox::information(this, "HQEd", "Do you really want to remove type \'" + typeList->at(li)->name() + "\'?"  , QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)   
          return;
          
     typeList->remove(li);
     emit onDelete();
     readTypes();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when select button is pressed
//
// #return No return value.
void TypeManager_ui::selectTypeClick(void)
{
     int currentIndex = tableWidget->currentRow();
     if(currentIndex == -1)
          return;
          
     _selected = currentIndex;
     emit onSelect();
     closeClick();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when close button is pressed
//
// #return No return value.
void TypeManager_ui::closeClick(void)
{    
     emit onClose();
     close();
}
// -----------------------------------------------------------------------------
