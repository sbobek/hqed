/**
 * \file	SelectGroup_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.15
 * \brief	This file contains class definition that arranges select group window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef SELECTGROUP_H
#define SELECTGROUP_H
// -----------------------------------------------------------------------------
 
#include "ui_SelectGroup.h"
// -----------------------------------------------------------------------------
/**
* \class 	SelectGroup_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007
* \brief 	Class definition that arranges select group window.
*/
class SelectGroup_ui : public QWidget, private Ui_SelectGroup
{
    Q_OBJECT

private:

     QString res_name;	///< Name of selected group.

public:

    /// \brief Constructor SelectGroup_ui class.
	///
	/// \param parent Pointer to parent object.
	SelectGroup_ui(QWidget *parent = 0);

 	/// \brief Function gets name of selected group.
	///
	/// \return Name of selected group.
	QString Result(void) { return res_name; }

 	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

 	/// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void OkClick(void);

 	/// \brief Function is object tree is clicked.
	///
	/// \param item Pointer to tree object.
	/// \param col Number of clicked column.
	/// \return No return value.
	void TreeClick(QTreeWidgetItem* item, int col);

signals:

 	/// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void ResultOK(void);
};
// -----------------------------------------------------------------------------

extern SelectGroup_ui* SelGroup;
// -----------------------------------------------------------------------------

#endif
