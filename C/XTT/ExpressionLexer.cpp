#include "ExpressionLexer.h"
#include "QDebug"

ExpressionLexer::ExpressionLexer()
{
    m_IdentifierTokenRegExp = QRegExp("[a-zA-Z_][a-zA-Z0-9_]{0,256}");
    m_IntegerTokenRegExp = QRegExp("\\d+(?=\\s|$)");
    m_NumericTokenRegExp = QRegExp("\\b(\\d+)(\\.\\d+)*\\b");
    m_WhitespaceRegExp = QRegExp("\\s+");
    m_QuotationTokenRegExp = QRegExp("\"[^\"]*\"");
}

QList<LexerToken> ExpressionLexer::tokenize(const QString &expression, bool &ok)
{
    m_ExpressionString = expression;

    m_Tokens.clear();
    m_CurrentIndex = 0;

    do {
        if(parseIfWhitespace()) {}
        else if(parseIfIdentifier()) {}
        else if(parseIfInteger()) {}
        else if(parseIfNumeric()) {}
        else if(parseIfParenthesis()) {}
        else if(parseIfOperator()) {}
        else if(parseIfSeparator()) {}
        else if(parseIfQuotation()) {}
        else {
            // Unknown token encountered
            ok = false;
            return m_Tokens;
        }

    } while(m_CurrentIndex != m_ExpressionString.length());

    ok = true;
    return m_Tokens;
}

bool ExpressionLexer::parseIfIdentifier()
{
    // check for identifier at current position
    int firstIndexFound = m_ExpressionString.indexOf(m_IdentifierTokenRegExp, m_CurrentIndex);
    if(firstIndexFound == m_CurrentIndex) {
        int tokenLength = m_IdentifierTokenRegExp.matchedLength();
        m_Tokens.append(LexerToken(LTT_IDENTIFIER_TOKEN, m_ExpressionString.mid(firstIndexFound, tokenLength), m_CurrentIndex));
        m_CurrentIndex = firstIndexFound + tokenLength;
        qDebug() << "bool ExpressionLexer::parseIfIdentifier(): " << m_ExpressionString << ", mid: " << m_ExpressionString.mid(firstIndexFound, tokenLength);
        return true;
    } else {
        return false;
    }
}

bool ExpressionLexer::parseIfInteger()
{
    // check for integer at current position
    int firstIndexFound = m_ExpressionString.indexOf(m_IntegerTokenRegExp, m_CurrentIndex);
    if(firstIndexFound == m_CurrentIndex) {
        int tokenLength = m_IntegerTokenRegExp.matchedLength();
        m_Tokens.append(LexerToken(LTT_INTEGER_TOKEN, m_ExpressionString.mid(firstIndexFound, tokenLength), m_CurrentIndex));
        m_CurrentIndex = firstIndexFound + tokenLength;
        qDebug() << "bool ExpressionLexer::parseIfInteger(): " << m_ExpressionString << ", mid: " << m_ExpressionString.mid(firstIndexFound, tokenLength);
        return true;
    } else {
        return false;
    }
}

bool ExpressionLexer::parseIfNumeric()
{
    // check for numerical value at current position
    int firstIndexFound = m_ExpressionString.indexOf(m_NumericTokenRegExp, m_CurrentIndex);
    if(firstIndexFound == m_CurrentIndex) {
        int tokenLength = m_NumericTokenRegExp.matchedLength();
        m_Tokens.append(LexerToken(LTT_NUMERIC_TOKEN, m_ExpressionString.mid(firstIndexFound, tokenLength), m_CurrentIndex));
        m_CurrentIndex = firstIndexFound + tokenLength;
        qDebug() << "bool ExpressionLexer::parseIfNumeric(): " << m_ExpressionString << ", mid: " << m_ExpressionString.mid(firstIndexFound, tokenLength);
        return true;
    } else {
        return false;
    }
}

bool ExpressionLexer::parseIfParenthesis()
{
    QChar charAtCurrentIndex = m_ExpressionString.at(m_CurrentIndex);

    switch(charAtCurrentIndex.toAscii())
    {
    case '(' :
    {
        m_Tokens.append(LexerToken(LTT_ROUND_PARENTHESIS_OPENING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    case ')' :
    {
        m_Tokens.append(LexerToken(LTT_ROUND_PARENTHESIS_CLOSING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    case '[' :
    {
        m_Tokens.append(LexerToken(LTT_RANGE_PARENTHESIS_OPENING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    case ']' :
    {
        m_Tokens.append(LexerToken(LTT_RANGE_PARENTHESIS_CLOSING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    case '{' :
    {
        m_Tokens.append(LexerToken(LTT_SET_PARENTHESIS_OPENING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    case '}' :
    {
        m_Tokens.append(LexerToken(LTT_SET_PARENTHESIS_CLOSING, charAtCurrentIndex, m_CurrentIndex));
        ++m_CurrentIndex;
        return true;
    }

    default:
    {
        return false;
    }
    }
}

bool ExpressionLexer::parseIfOperator()
{
    // check for operator at current position
    bool operatorRecognized = false;
    QString longestMatchingOperator;
    int firstIndexFound;
    foreach(QString operatorRepresentation, m_OperatorRepresentations) {
        firstIndexFound = m_ExpressionString.indexOf(operatorRepresentation, m_CurrentIndex);
        if(firstIndexFound == m_CurrentIndex) {
            if(operatorRepresentation.length() > longestMatchingOperator.length()) {
                longestMatchingOperator = operatorRepresentation;
            }
            operatorRecognized = true;
        }
    }

    if(operatorRecognized) {
        qDebug() << "bool ExpressionLexer::parseIfOperator(): " << m_ExpressionString << ", mid: " << longestMatchingOperator;
        m_Tokens.append(LexerToken(LTT_OPERATOR, longestMatchingOperator, m_CurrentIndex));
        m_CurrentIndex += longestMatchingOperator.length();
    }

    return operatorRecognized;
}

bool ExpressionLexer::parseIfSeparator()
{
    if(m_ExpressionString.at(m_CurrentIndex) == ',' || m_ExpressionString.at(m_CurrentIndex) == ';') {
        m_Tokens.append(LexerToken(LTT_SEPARATOR, m_ExpressionString.at(m_CurrentIndex), m_CurrentIndex));
        ++m_CurrentIndex;
        qDebug() << "bool ExpressionLexer::parseIfOperator(): " << m_ExpressionString << ", mid: " << m_ExpressionString.at(m_CurrentIndex);
        return true;
    } else {
        return false;
    }
}

bool ExpressionLexer::parseIfWhitespace()
{
    // consume whitespaces at current position
    int firstIndexFound = m_ExpressionString.indexOf(m_WhitespaceRegExp, m_CurrentIndex);
    if(firstIndexFound == m_CurrentIndex) {
        m_CurrentIndex = firstIndexFound + m_WhitespaceRegExp.matchedLength();
        return true;
    } else {
        return false;
    }
}

bool ExpressionLexer::parseIfQuotation()
{
    // check for quotation at current position
    int firstIndexFound = m_ExpressionString.indexOf(m_QuotationTokenRegExp, m_CurrentIndex);
    if(firstIndexFound == m_CurrentIndex) {
        int tokenLength = m_QuotationTokenRegExp.matchedLength();
        m_Tokens.append(LexerToken(LTT_QUOTATION_TOKEN, m_ExpressionString.mid(firstIndexFound, tokenLength), m_CurrentIndex));
        m_CurrentIndex = firstIndexFound + tokenLength;
        qDebug() << "bool ExpressionLexer::parseIfQuotation(): " << m_ExpressionString << ", mid: " << m_ExpressionString.mid(firstIndexFound, tokenLength);
        return true;
    } else {
        return false;
    }
}
