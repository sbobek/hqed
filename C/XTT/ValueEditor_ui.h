/**
 * \file	ValueEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	07.08.2008
 * \version	1.15
 * \brief	This file contains class definition that arranges the value editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef ValueEditorH
#define ValueEditorH
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include <QResizeEvent>

#include <functional>
#include "ui_ValueEditor.h"
// -----------------------------------------------------------------------------

class hType;
class hSet;
class hSetItem;
class hValue;
class XTT_Cell;

class ExpressionEditor_ui;
class SymbolicValueSelector_ui;
// -----------------------------------------------------------------------------

/**
* \class 	ValueEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	07.08.2008
* \brief 	Class definition that arranges the value editor window.
*/
class ValueEditor_ui : public QWidget, private Ui_ValueEditor
{
    Q_OBJECT
    
    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
    QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
    QGridLayout* gp4Layout;	               ///< Pointer to groupBox3 grid layout.
    QGridLayout* sw1page0;	               ///< Pointer to page0 grid layout.
    QGridLayout* sw1page1;	               ///< Pointer to page1 grid layout.
    QGridLayout* sw1page2;	               ///< Pointer to page2 grid layout.
    QGridLayout* sw2page0;	               ///< Pointer to page0 grid layout.
    QGridLayout* sw2page1;	               ///< Pointer to page1 grid layout.
    QGridLayout* sw2page2;	               ///< Pointer to page2 grid layout.
    
    hType* _type;                            ///< The type of values
    hSetItem* _sitem;                        ///< Edited item
    XTT_Cell* _cell;                         ///< Pointer to the edited cell
    int _setClass;                           ///< Denotes the type of the set (0-set, 1-domain, 2-decision context)
    bool _singleRequired;                    ///< Denotes if only single value is required
    bool _avr;                               ///< Denotes if the attribute value reference is allowed
    bool _autohide;                          ///< Denotes if the window works in the hide mode
    
    // Window configuration
    int vdt_cv;                              ///< ComboBox index - value definition type - constant value
    int vdt_avr;                             ///< ComboBox index - value definition type - AVR
    int vdt_exp;                             ///< ComboBox index - value definition type - expression
    
    // Specify values index in ComboBoxes
    int sv_get;                              ///< Allow for input new value
    int sv_nd;                               ///< Not defined value
    int sv_any;                              ///< Any value
    int sv_min;                              ///< Min value (only integer and numeric)
    int sv_max;                              ///< Max value (only integer and numeric)
    
    ExpressionEditor_ui* expressionEditor;   ///< Pointer to the expression editor window
    SymbolicValueSelector_ui* symValSelector;///< Pointer to the sybolic value selector

    /// \brief Generic function to get value from user.
    ///
    /// \param initVal - value to display as default.
    /// \param getValue - function that will be used to retrieve value from user when no ECMAScript presentation plugin is set.
    /// \param fromString - function that converts QString to type T given as template parameter.
    /// \param toString - function that converts type T given as template parameter to QString.
    template<typename T>
    T getValueFromInput(
         T initVal,
         std::function<T(T, bool&)> getValue,
         std::function<T(QString)> fromString,
         std::function<QString(T)> toString,
         bool& ok);

protected:

     /// \brief Function that checks if the defined range is correct
     ///
     /// \return true when range is correct, or value is defined as single. Otherwise false.
     bool checkRangeCorrectness(void);

     /// \brief Function triggered when the size of the window is changing.
     ///
     /// \param event - the object that contains the event parameters
     /// \return No return value.
     virtual void resizeEvent(QResizeEvent* event); 

     /// \brief Function triggered when the window is going to be closed
     ///
     /// \param event - pointer to the object that cntains the event parameters
     /// \return No return value.
     virtual void closeEvent(QCloseEvent* event);
     
public:

     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param parent Pointer to parent object.
     ValueEditor_ui(QWidget *parent = 0);
     
     /// \brief Destructor ValueEdit_ui class.
     ~ValueEditor_ui(void);
     
     /// \brief Function sets up the window properties
     ///
     /// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
     ///
     /// \param event Pointer to event object.
     /// \return No return value.
     void keyPressEvent(QKeyEvent*);
     
     /// \brief Function sets the window working mode
     ///
     /// \param *__type - data type of the set
     /// \param *__sitem - edited set item
     /// \param __setClass - type of the set
     /// \li 0-set
     /// \li 1-domain
     /// \li 2-action context
     /// \li 3-coditional context
     /// \param __singleRequired - denotes if the set can contains more than one value
     /// \param __avr - denotes if the attribute value reference is allowed
     /// \param *__cell - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
     /// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, hSetItem* __sitem, int __setClass, bool __singleRequired, bool __avr, XTT_Cell* __cell);
     
     /// \brief Function checks if the current mode is correct
     ///
     /// \return true if the current mode is correct, otherwise false
     bool isModeCorrect(void);
     
     /// \brief Function triggered when type of specify value change
     ///
     /// \param __spIndex - new specify value index, if -2 that means the window is closed by ok button
     /// \param *__value - poiter to the value
     /// \param __which - denotes which value has been changed
     /// \return true if all the operation are succefully, otherwise false
     bool spValueChanged(int __spIndex, hValue* __value, int __which);
     
     /// \brief Function returns the item that has been changed by using this dialog
     ///
     /// \return Pointer to the changed object.
     hSetItem* result(void);
     
     /// \brief Function calls the SymbolicValueSelector widget
     ///
     /// \param __type - the type of the value
     /// \param __setClass - the class of the edited set
     /// \param __value - value that we want to edit
     /// \return no values returns
     void symbolicValueSelector(hType* __type, int __setClass, hValue* __value);
     
     /// \brief Function calls the SymbolicValueSelector widget with default arguments
     ///
     /// \param __which - denotes which value is edited
     /// \return no values returns
     void symbolicValueSelector(int __which);

public slots:

     /// \brief Function that is triggered when type of the value is changed
     ///
     /// \return No return value.
     void typeSelect(void);
     
     /// \brief Function triggered when OK button is pressed
     ///
     /// \return No return value.
     void okButtonClick(void);
     
     /// \brief Function triggered when cancel button is pressed
     ///
     /// \return No return value.
     void cancelButtonClick(void);
     
     /// \brief Function triggered when get value button is clicked
     ///
     /// \return No return value.
     void getFromValueClick(void);
     
     /// \brief Function triggered when get value button is clicked
     ///
     /// \return No return value.
     void getToValueClick(void);
     
     /// \brief Function triggered when type of value definition has changed
     ///
     /// \param __newIndex - new item index
     /// \return No return value.
     void typeOfValFromDefChanged(int __newIndex);
     
     /// \brief Function triggered when type of value definition has changed
     ///
     /// \param __newIndex - new item index
     /// \return No return value.
     void typeOfValToDefChanged(int __newIndex);
     
     /// \brief Function triggered when type of specify value changed
     ///
     /// \param __newIndex - new specify value index
     /// \return No return value.
     void spFromValueChanged(int __newIndex);
     
     /// \brief Function triggered when type of specify value changed
     ///
     /// \param __newIndex - new specify value index
     /// \return No return value.
     void spToValueChanged(int __newIndex);
     
     /// \brief Function triggered when select attribute button for "from" value is pressed
     ///
     /// \return No return value.
     void valFromAttributeSelectionClick(void);
     
     /// \brief Function triggered when the attribute manager is closed with select button
     ///
     /// \return No return value.
     void valFromAttributeSelectionFinish(void);
     
     /// \brief Function triggered when select attribute button for "to" value is pressed
     ///
     /// \return No return value.
     void valToAttributeSelectionClick(void);
     
     /// \brief Function triggered when the attribute manager is closed with select button
     ///
     /// \return No return value.
     void valToAttributeSelectionFinish(void);
     
     /// \brief Function triggered when edit expression for "from" value is pressed
     ///
     /// \return No return value.
     void valFromExprDefinitionClick(void);
     
     /// \brief Function triggered when the expression editor is closed with ok button
     ///
     /// \return No return value.
     void valFromExprDefinitionFinish(void);
     
     /// \brief Function triggered when the expression editor is closed
     ///
     /// \return No return value.
     void valFromExprDefinitionWindowClosed(void);
     
     /// \brief Function triggered when edit expression for "to" value is pressed
     ///
     /// \return No return value.
     void valToExprDefinitionClick(void);
     
     /// \brief Function triggered when the expression editor is closed with ok button
     ///
     /// \return No return value.
     void valToExprDefinitionFinish(void);
     
     /// \brief Function triggered when the expression editor is closed
     ///
     /// \return No return value.
     void valToExprDefinitionWindowClosed(void);
     
     /// \brief Function triggered when the sybolicValueSelector is closed by ok button
     ///
     /// \return No return value.
     void symValSelectorApply(void);
     
     /// \brief Function triggered when the sybolicValueSelector is closed
     ///
     /// \return No return value.
     void symValSelectorClose(void);
     
     /// \brief Function that sets the window according to the item parameters and values
     ///
     /// \param *__item - pointer to the item that mast be displayed. If it is NULL the item stored in _sitem will be displayed
     /// \return No return value.
     void displayItem(hSetItem* __item = NULL);
     
signals:

     /// \brief Signal emited when ok button is pressed
     ///
     /// \return No return value.
     void okClicked(void);
     
     /// \brief Signal emited when cancel button is pressed
     ///
     /// \return No return value.
     void windowClosed(void);
};
// -----------------------------------------------------------------------------
// This class has not the global object because each object that want to use
// the window of this class must define it own instance

#endif
