#ifndef PARENTHESISHIGHLIGHTER_H
#define PARENTHESISHIGHLIGHTER_H

#include <QObject>
#include <QVector>
#include <QPair>
#include <QTextCursor>
#include <QTextCharFormat>

QT_BEGIN_NAMESPACE
class QTextEdit;
class QTextDocument;
QT_END_NAMESPACE

class ParenthesisHighlighter : public QObject
{
    Q_OBJECT

public:
    ParenthesisHighlighter(QTextEdit *editor);

public slots:
    void highlightMatchingParenthesis();

private:
    void removeFormattings();
    void highlightIndexes(const int a, const int b);

    int getOpeningParenthesisTypeIndex(QChar character);
    int getClosingParenthesisTypeIndex(QChar character);

    QTextEdit *m_Editor;
    QTextDocument *m_Document;
    QVector<QPair<QChar, QChar> > m_ParenthesisTypes;
    QList<QPair<QTextCursor, QTextCharFormat> > m_OldCursorsWithFormats;
};

#endif // PARENTHESISHIGHLIGHTER_H
