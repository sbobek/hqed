#include "ParenthesisHighlighter.h"
#include <QTextDocument>
#include <QTextEdit>

ParenthesisHighlighter::ParenthesisHighlighter(QTextEdit *editor)
    : QObject(editor), m_Editor(editor), m_Document(editor->document())
{
    m_ParenthesisTypes << QPair<QChar, QChar>('(', ')')
            << QPair<QChar, QChar>('{', '}')
            << QPair<QChar, QChar>('[', ']');
}

void ParenthesisHighlighter::highlightMatchingParenthesis()
{
    removeFormattings();

    int cursorPosition = m_Editor->textCursor().position();
    if(cursorPosition < 0) {
        return;
    }

    QString documentString = m_Document->toPlainText();
    if(documentString.size() < 2) {
        return;
    }

    QChar charBeforeCursor;
    QChar charAfterCursor;
    if(cursorPosition == 0) { // before string
        charBeforeCursor = QChar::Null;
        charAfterCursor = documentString.at(cursorPosition);
    } else if(cursorPosition == documentString.size()) { // after string
        charBeforeCursor = documentString.at(cursorPosition - 1);
        charAfterCursor = QChar::Null;
    } else { // in the middle
        charBeforeCursor = documentString.at(cursorPosition - 1);
        charAfterCursor = documentString.at(cursorPosition);
    }

    int level = 0;
    int openingParenthesisType = getOpeningParenthesisTypeIndex(charAfterCursor);
    int closingParenthesisType = getClosingParenthesisTypeIndex(charBeforeCursor);

    if(openingParenthesisType != -1) {
        for(int i = cursorPosition + 1; i < documentString.size(); i++) {
            if(openingParenthesisType == getClosingParenthesisTypeIndex(documentString.at(i)) && level == 0) {
                //qDebug() << "Closing parenthesis found at location " << i;
                highlightIndexes(i, cursorPosition);
                break;
            } else if(openingParenthesisType == getOpeningParenthesisTypeIndex(documentString.at(i))) {
                level++;
            } else if(openingParenthesisType == getClosingParenthesisTypeIndex(documentString.at(i))) {
                level--;
            }
        }
    }

    level = 0;
    if(closingParenthesisType != -1) {
        for(int i = cursorPosition - 2; i >= 0; --i) {
            if(closingParenthesisType == getOpeningParenthesisTypeIndex(documentString.at(i)) && level == 0) {
                //qDebug() << "Opening parenthesis found at location " << i;
                highlightIndexes(i, cursorPosition - 1);
                break;
            } else if(closingParenthesisType == getClosingParenthesisTypeIndex(documentString.at(i))) {
                level++;
            } else if(closingParenthesisType == getOpeningParenthesisTypeIndex(documentString.at(i))) {
                level--;
            }
        }
    }
}

void ParenthesisHighlighter::removeFormattings()
{
    QTextCursor cursor(m_Document);
    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor, 1);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 1);
    QTextCharFormat defaultFormat;
    cursor.setCharFormat(defaultFormat);

    QPair<QTextCursor, QTextCharFormat> oldCursorAndFormat;
    foreach(oldCursorAndFormat, m_OldCursorsWithFormats) {
        oldCursorAndFormat.first.setCharFormat(oldCursorAndFormat.second);
    }

    m_OldCursorsWithFormats.clear();
}

void ParenthesisHighlighter::highlightIndexes(const int a, const int b)
{
    QTextCharFormat highlightCharFormat;
    //highlightCharFormat.setUnderlineStyle(QTextCharFormat::DotLine);
    QBrush highlightBrush(QColor(173, 216, 230, 200));
    highlightCharFormat.setBackground(highlightBrush);

    QTextCursor cursor(m_Document);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, a);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 1);
    m_OldCursorsWithFormats.append(QPair<QTextCursor, QTextCharFormat>(cursor, cursor.charFormat()));
    cursor.setCharFormat(highlightCharFormat);

    cursor.movePosition(QTextCursor::Start, QTextCursor::MoveAnchor);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, b);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, 1);
    m_OldCursorsWithFormats.append(QPair<QTextCursor, QTextCharFormat>(cursor, cursor.charFormat()));
    cursor.setCharFormat(highlightCharFormat);
}

int ParenthesisHighlighter::getOpeningParenthesisTypeIndex(QChar character)
{
    for(int i = 0; i < m_ParenthesisTypes.size(); i++) {
        if(character == m_ParenthesisTypes.at(i).first) {
            return i;
        }
    }

    return -1;
}

int ParenthesisHighlighter::getClosingParenthesisTypeIndex(QChar character)
{
    for(int i = 0; i < m_ParenthesisTypes.size(); i++) {
        if(character == m_ParenthesisTypes.at(i).second) {
            return i;
        }
    }

    return -1;
}


