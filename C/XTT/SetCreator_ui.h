 /**
 * \file      SetCreator_ui.h
 * \author    Krzysztof Kaczor kinio4444@gmail.com
 * \date      3.11.2009
 * \version   1.0
 * \brief     This file contains class definition arranges SetCreator window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef SETCREATOR_H
#define SETCREATOR_H
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_SetCreator.h"
// -----------------------------------------------------------------------------

class hSet;
class hType;
class hDomain;
class XTT_Cell;
class QListWidgetItem;
class SetItemSelectorWidget;
// -----------------------------------------------------------------------------

/**
* \class      SetCreator_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       3.11.2009
* \brief      This class arranges SetCreator window
*/
class SetCreator_ui : public QWidget, public Ui_SetCreator
{
    Q_OBJECT

private:

     SetItemSelectorWidget* listWidget;      ///< The list of the set items
     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     hDomain* _domain;                       ///< Domain of allowed values
     hSet* _set;                             ///< Edited set
     hSet* _newSet;                          ///< Set that contains new elements
     bool _singleRequired;                   ///< Indicates if user can select only one value
     
     /// \brief Function that checks if the given parameters are correct
     ///
     /// \param __type - pointer to the set type
     /// \param __set - pointer to the edited set
     /// \return true if mode is correct other wise false
     bool isModeCorrect(hType* __type, hSet* __set);
     
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param __event - Pointer to event object.
	/// \return No return value.
	virtual void keyPressEvent(QKeyEvent* __event);

public:

     /// \brief Constructor SetCreator_ui class.
     ///
     /// \param *parent - Pointer to parent.
     SetCreator_ui(QWidget *parent = 0);

     /// \brief Destructore SetCreator_ui class.
     ~SetCreator_ui(void);

     /// \brief Function that sets the initial values of the window class
     ///
     /// \return no values returns
     void initWindow(void);
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
     /// \brief Function that set the window mode
     ///
     /// \param __type - pointer to the set type
     /// \param __set - pointer to the edited set
     /// \param __singleRequired - indicates if the user can select only one value
     /// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, hSet* __set, bool __singleRequired);
     
     /// \brief Function that set the window mode
     ///
     /// \param __cell - pointer to the edited cell
     /// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(XTT_Cell* __cell);
     
     /// \brief Function that displays the given set
     ///
     /// \return no values return
     void displaySet(void);
     
     /// \brief Function that select only these items that are inside the given set
     ///
     /// \param __set - pointer to the set
     /// \return no values return
     void selectItems(hSet* __set);
     
     /// \brief Function that updates edited set according to selected items
     ///
     /// \return  no values return
     void upadteSet(void);
     
     /// \brief Function that returns pointer to the edited set
     ///
     /// \return  pointer to the edited set
     hSet* result(void);
     
public slots:
     
     /// \brief Function that is triggered when select range button is pressed
     ///
     /// \return No return value.
     void addRangeClick(void);
     
     /// \brief Function that is triggered when ok button is pressed
     ///
     /// \return No return value.
     void okClick(void);
     
     /// \brief Function that is triggered when close button is pressed
     ///
     /// \return No return value.
     void closeClick(void);
     
     /// \brief Function that is triggered when user clicks an item in the listBox
     ///
     /// \param __item - pointer to the clicked item
     /// \return No return value.
     void itemStateChanged(QListWidgetItem* __item);
     
signals:

     /// \brief Signal that is emited when user clicks ok button
     ///
     /// \return No return value.
     void okclicked(void);
};
// -----------------------------------------------------------------------------

extern SetCreator_ui* SetCreator;
// -----------------------------------------------------------------------------

#endif
