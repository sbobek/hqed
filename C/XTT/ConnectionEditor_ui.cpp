/*
 *	   $Id: ConnectionEditor_ui.cpp,v 1.32 2009-10-26 18:25:57 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_ConnectionsList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"
#include "widgets/MyGraphicsScene.h"
#include "TableEditor_ui.h"
#include "ConnectionEditor_ui.h"
#include "../MainWin_ui.h"
// -----------------------------------------------------------------------------

ConnectionEditor_ui* ConnEdit;
// -----------------------------------------------------------------------------

// #brief Constructor ConnectionEditor_ui class.
//
// #param parent Pointer to parent object.
ConnectionEditor_ui::ConnectionEditor_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickOK()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

// #brief Constructor AttrEditor class.
//
// #param _new Determines whether new connection should be created(true) or not(false).
// #param _index Index of connection.
// #param parent Pointer to parent object.
ConnectionEditor_ui::ConnectionEditor_ui(bool _new, int _index, QWidget*)
{
     setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
	
     connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickOK()));
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));
     connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_3(int)));
     connect(comboBox_5, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_5(int)));
     connect(comboBox_6, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_6(int)));
     connect(comboBox_7, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_7(int)));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);

     fNew = _new;
     fIndex = _index;

     groupBox_4->setDisabled(fNew);

     // Wczytywanie nazw tabel do comboboxow
     comboBox->addItems(TableList->TablesTitles());
     comboBox_3->addItems(TableList->TablesTitles());
     comboBox_5->addItems(TableList->TablesTitles());

     if(!fNew)
          setUpWindow();
     if(fNew)
          comboBox->setCurrentIndex(_index);
}
// -----------------------------------------------------------------------------

// #brief Function sets up window parameters.
//
// #return No return value.
void ConnectionEditor_ui::setUpWindow(void)
{
     XTT_Connections* conn = ConnectionsList->Connection(fIndex);
     if(conn == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Invalid connection index.", QMessageBox::Ok);
          return;
     }

     QString tid = conn->SrcTable();
     QString rid = conn->SrcRow();

     // Wyszukiwanie indexow
     int tindex = TableList->IndexOfID(tid);
     if(tindex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Invalid table ID.", QMessageBox::Ok);
          return;
     }
     int rindex = -1;
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
          if(TableList->Table(tindex)->Row(i)->RowId() == rid)
          {
               rindex = i;
               break;
          }
     if(rindex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Invalid row ID.", QMessageBox::Ok);
          return;
     }

     comboBox_5->setCurrentIndex(tindex);
     comboBox_6->setCurrentIndex(rindex);
	
	QList<XTT_Connections*>* cl = ConnectionsList->_IndexOfOutConnections(tid, rid);
	comboBox_7->setCurrentIndex(cl->indexOf(conn));
	delete cl;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when combo box in the table is changed.
//
// #param new_index Selected index of combo box.
// #return No return value.
void ConnectionEditor_ui::ComboBoxChange(int new_index)
{
     comboBox_2->clear();
     
     // Pobranie indexu tabeli
     int tindex = new_index;
     if(tindex == -1)
          return;

     // Wczytywanie wierszy
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
          comboBox_2->addItem(QString::number(i+1) + ". Row ID = " + TableList->Table(tindex)->Row(i)->RowId());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select table field is changed.
//
// #param new_index Selected index of combo box.
// #return No return value.
void ConnectionEditor_ui::ComboBoxChange_3(int new_index)
{
     comboBox_4->clear();
     
     // Pobranie indexu tabeli
     int tindex = new_index;
     if(tindex == -1)
          return;

     // Wczytywanie wierszy
     comboBox_4->addItem("Table: " + TableList->Table(tindex)->Title() + ". Table ID: " + TableList->Table(tindex)->TableId());
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
          comboBox_4->addItem(QString::number(i+1) + ". Row ID: " + TableList->Table(tindex)->Row(i)->RowId());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select table field is changed.
//
// #param new_index Selected index of combo box.
// #return No return value.
void ConnectionEditor_ui::ComboBoxChange_5(int new_index)
{
     if(fNew)
          return;
          
     comboBox_6->clear();
     
     // Pobranie indexu tabeli
     int tindex = new_index;
     if(tindex == -1)
          return;

     // Wczytywanie wierszy
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
     {
          QString comment = " - CONNECTED";
          QString tid = TableList->Table(tindex)->TableId();
          QString rid = TableList->Table(tindex)->Row(i)->RowId();
          if(!ConnectionsList->ExistsOutConnections(tid, rid))
               comment = " - NOT CONNECTED";

          comboBox_6->addItem(QString::number(i+1) + ". Row ID = " + TableList->Table(tindex)->Row(i)->RowId() + comment);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select row field is changed.
//
// #param new_index Selected index of combo box.
// #return No return value.
void ConnectionEditor_ui::ComboBoxChange_6(int new_index)
{
     if((fNew) || (new_index == -1))
          return;

	comboBox_7->clear();
     int cti = comboBox_5->currentIndex();
     int cri = new_index;

     // Jezeli nie poprawne indexy to blokujemy elemnty
     if((cti == -1) || (cri == -1))
          return;

     // Pobranie i elementow
     QString tid = TableList->Table(cti)->TableId();
     QString rid = TableList->Table(cti)->Row(cri)->RowId();
	
	// Wypisanie wszystkich polaczen wychodzacych z danego wiersza
	QList<XTT_Connections*>* conn_list = ConnectionsList->_IndexOfOutConnections(tid, rid);
	for(int c=0;c<conn_list->size();++c)
		comboBox_7->addItem(conn_list->at(c)->ConnId() + "; " + conn_list->at(c)->Label());
	delete conn_list;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select connection id is changed.
//
// #param new_index Selected index of combo box.
// #return No return value.
void ConnectionEditor_ui::ComboBoxChange_7(int new_index)
{
     if((fNew) || (new_index == -1))
          return;

     int cti = comboBox_5->currentIndex();
     int cri = comboBox_6->currentIndex();
	
	// Wczytywanie identyfikatora polaczenia
	QString text = comboBox_7->itemText(new_index);
	QStringList tlist = text.split(";", QString::SkipEmptyParts, Qt::CaseSensitive);
	if(tlist.size() == 0)
		return;
	QString cid = tlist.at(0);

     // Odblokowanie elementow
     pushButton->setEnabled(true);
     groupBox->setEnabled(true);
     groupBox_2->setEnabled(true);
     groupBox_3->setEnabled(true);

     // Jezeli nie poprawne indexy to blokujemy elemnty
     if((cti == -1) || (cri == -1) || (cid == ""))
     {
          pushButton->setDisabled(true);
          groupBox->setDisabled(true);
          groupBox_2->setDisabled(true);
          groupBox_3->setDisabled(true);
     }

     // Pobranie i elementow
     //int tid = TableList->Table(cti)->TableId();
     //int rid = TableList->Table(cti)->Row(cri)->RowId();
     int conn_index = ConnectionsList->IndexOfID(cid);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          pushButton->setDisabled(true);
          groupBox->setDisabled(true);
          groupBox_2->setDisabled(true);
          groupBox_3->setDisabled(true);
          return;
     }

     // Wyszukiwanie indexow elemetow polaczenia
     QString dtid = ConnectionsList->Connection(conn_index)->DesTable();
     QString drid = ConnectionsList->Connection(conn_index)->DesRow();
     int dti = TableList->IndexOfID(dtid);
     int dri = -1;
     if(dti == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect table ID.", QMessageBox::Ok);
          return;
     }
     // Wyszukiwanie indexu wiersza
     for(unsigned int i=0;i<TableList->Table(dti)->RowCount();i++)
          if(TableList->Table(dti)->Row(i)->RowId() == drid)
          {
               dri = i;
               break;
          }
     if((dri == -1) && (!drid.isEmpty()))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect row ID.", QMessageBox::Ok);
          return;
     }

     // Wpisujemy wsrtosci w pola okienka
     comboBox->setCurrentIndex(cti);
     comboBox_2->setCurrentIndex(cri);
     comboBox_3->setCurrentIndex(dti);
     comboBox_4->setCurrentIndex(dri+1);
     lineEdit->setText(ConnectionsList->Connection(conn_index)->Label());
     lineEdit_2->setText(ConnectionsList->Connection(conn_index)->Description());
     checkBox->setChecked(ConnectionsList->Connection(conn_index)->IsCut());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button OK is pressed.
//
// #return No return value.
void ConnectionEditor_ui::ClickOK(void)
{
     int stindex = comboBox->currentIndex();
     int srindex = comboBox_2->currentIndex();
     int dtindex = comboBox_3->currentIndex();
     int drindex = comboBox_4->currentIndex();

     int ctindex = comboBox_5->currentIndex();
     int crindex = comboBox_6->currentIndex();

     if(stindex == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no source table selected.", QMessageBox::Ok);
          return;
     }

     if(dtindex == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no destination table selected.", QMessageBox::Ok);
          return;
     }

     if(srindex == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no source row selected.", QMessageBox::Ok);
          return;
     }

     if(drindex == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no destination row selected.", QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy polaczenie nie wychodz z tabeli koncowej
     if(TableList->Table(stindex)->TableType() == 2)
     {
          QMessageBox::information(NULL, "HQEd", "Selected source table is halt table whitch can't have connection outside.", QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy polaczenie nie przychodzi do tabeli poczatkowej
     if(TableList->Table(dtindex)->TableType() == 0)
     {
          QMessageBox::information(NULL, "HQEd", "Selected destination table is initial table whitch can't have connection inside.", QMessageBox::Ok);
          return;
     }
	
	// Wczytanie identyfikatorow
     QString stid = TableList->Table(stindex)->TableId();
     QString srid = TableList->Table(stindex)->Row(srindex)->RowId();
     QString dtid = TableList->Table(dtindex)->TableId();
     QString drid = "";
     if(drindex > 0)
          drid = TableList->Table(dtindex)->Row(drindex-1)->RowId();
	
	// Sprawdzanie czy nie istnieje juz polaczenie z tego miejsca
     QList<int>* l = ConnectionsList->IndexOfOutConnections(stid, srid);
     int s = l->size();
     delete l;
     if(((fNew) && (s > 0)) ||
       ((!fNew) && (s > 0) && ((stindex != ctindex) || (srindex != crindex))))
     {
          QMessageBox::information(NULL, "HQEd", "Connection from this row is already exists. You can't create it.", QMessageBox::Ok);
          return;
     }

     // Zapisanie ustawien
     if(fNew)
     {
          ConnectionsList->Add();
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setCut(checkBox->isChecked());
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setLabel(lineEdit->text());
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setDescription(lineEdit_2->text());
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setSrcTable(stid);
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setDesTable(dtid);
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setSrcRow(srid);
          ConnectionsList->Connection(ConnectionsList->Count()-1)->setDesRow(drid);
		
		// Odswiezenie widoku polaczenia
		ConnectionsList->Connection(ConnectionsList->Count()-1)->Reposition();
		ConnectionsList->Connection(ConnectionsList->Count()-1)->Repaint();
		MainWin->xtt_scene->update();
     }

	QString prevSrcTableID = "";		// ID poprzedniej tabeli zrodlowej
	QString prevDesTableID = "";		// ID poprzedniej tabeli docelowej
     if(!fNew)
     {
          //QString ctid = TableList->Table(ctindex)->TableId();
          //QString crid = TableList->Table(ctindex)->Row(crindex)->RowId();
		
		// Wczytywanie identyfikatora polaczenia
		QString text = comboBox_7->itemText(comboBox_7->currentIndex());
		QStringList tlist = text.split(";", QString::SkipEmptyParts, Qt::CaseSensitive);
		if(tlist.size() == 0)
		{
			QMessageBox::critical(NULL, "HQEd", "Can't read connection ID.", QMessageBox::Ok);
               return;
		}
		QString cid = tlist.at(0);
          int conn_index =  ConnectionsList->IndexOfID(cid);

          if(conn_index == -1)
          {
               QMessageBox::critical(NULL, "HQEd", "Selected connection not exists.", QMessageBox::Ok);
               return;
          }
		
		prevSrcTableID = ConnectionsList->Connection(conn_index)->SrcTable();
		prevDesTableID = ConnectionsList->Connection(conn_index)->DesTable();
          ConnectionsList->Connection(conn_index)->setCut(checkBox->isChecked());
          ConnectionsList->Connection(conn_index)->setLabel(lineEdit->text());
          ConnectionsList->Connection(conn_index)->setDescription(lineEdit_2->text());
          ConnectionsList->Connection(conn_index)->setSrcTable(stid);
          ConnectionsList->Connection(conn_index)->setDesTable(dtid);
          ConnectionsList->Connection(conn_index)->setSrcRow(srid);
          ConnectionsList->Connection(conn_index)->setDesRow(drid);
		
		// Odswiezenie widoku polaczenia
		ConnectionsList->Connection(conn_index)->Reposition();
		ConnectionsList->Connection(conn_index)->Repaint();
		MainWin->xtt_scene->update();
     }
	
	// Analiza wszystkich poalczaen tabeli zrodlowej poprzedniej
	if(prevSrcTableID != "")
		TableList->AnalyzeTableConnections(prevSrcTableID);
		
	// Analiza wszystkich poalczaen tabeli docelowej poprzedniej
	if((prevDesTableID != "") && (prevDesTableID != prevSrcTableID))
		TableList->AnalyzeTableConnections(prevDesTableID);
	
	// Analiza wszystkich poalczaen tabeli zrodlowej docelowej
	TableList->AnalyzeTableConnections(stid);
	TableList->AnalyzeTableConnections(dtid);

     xtt::fullVerification();
     emit OnClose();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ConnectionEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               ClickOK();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
