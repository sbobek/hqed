/*
 *	   $Id: ConnectionManager_ui.cpp,v 1.32 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_ConnectionsList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"

#include "../../namespaces/ns_hqed.h"
#include "../../namespaces/ns_xtt.h"
#include "DeleteConnection_ui.h"
#include "ConnectionEditor_ui.h"
#include "TableEditor_ui.h"
#include "ConnectionManager_ui.h"
// -----------------------------------------------------------------------------

ConnectionManager_ui* ConnManager;
// -----------------------------------------------------------------------------

// #brief Constructor ConnectionManager_ui class.
//
// #param parent Pointer to parent object.
ConnectionManager_ui::ConnectionManager_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(ClickClose()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(AddNewConnection()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(EditInConnection()));
    connect(pushButton_5, SIGNAL(clicked()), this, SLOT(EditOutConnection()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(DeleteInConnection()));
    connect(pushButton_6, SIGNAL(clicked()), this, SLOT(DeleteOutConnection()));
    connect(listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(SelectedTableChange(int)));
    connect(listWidget_2, SIGNAL(currentRowChanged(int)), this, SLOT(ListOfOutConnectionClick(int)));
    connect(listWidget_3, SIGNAL(currentRowChanged(int)), this, SLOT(ListOfInConnectionClick(int)));
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));
    connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_2(int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);

    // Wczytanie tabel
    listWidget->addItems(TableList->TablesTitles());
}
// -----------------------------------------------------------------------------

// #brief Constructor ConnectionManager_ui class.
//
// #param tindex Index of chosen table.
// #param parent Pointer to parent object.
ConnectionManager_ui::ConnectionManager_ui(int tindex, QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(ClickClose()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(AddNewConnection()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(EditInConnection()));
    connect(pushButton_5, SIGNAL(clicked()), this, SLOT(EditOutConnection()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(DeleteInConnection()));
    connect(pushButton_6, SIGNAL(clicked()), this, SLOT(DeleteOutConnection()));
    connect(listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(SelectedTableChange(int)));
    connect(listWidget_2, SIGNAL(currentRowChanged(int)), this, SLOT(ListOfOutConnectionClick(int)));
    connect(listWidget_3, SIGNAL(currentRowChanged(int)), this, SLOT(ListOfInConnectionClick(int)));
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));
    connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_2(int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);

    // Wczytanie tabel
    listWidget->addItems(TableList->TablesTitles());
    listWidget->setCurrentRow(tindex);
}
// -----------------------------------------------------------------------------

// #brief Function refreshes the window.
//
// #return No return value.
void ConnectionManager_ui::Refresh(void)
{
     int lw1 = listWidget->currentRow();
     int lw2 = listWidget_2->currentRow();
     int lw3 = listWidget_3->currentRow();
     int cb1 = comboBox->currentIndex();
     int cb2 = comboBox_2->currentIndex();

     SelectedTableChange(lw1);

     listWidget->setCurrentRow(lw1);
     listWidget_2->setCurrentRow(lw2);
     listWidget_3->setCurrentRow(lw3);
     comboBox->setCurrentIndex(cb1);
     comboBox_2->setCurrentIndex(cb2);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when table field is changed.
//
// #param new_index Index of the table.
// #return No return value.
void ConnectionManager_ui::SelectedTableChange(int new_index)
{
     listWidget_2->clear();
     listWidget_3->clear();
     
     // Pobranie indexu tabeli
     int tindex = new_index;
     if(tindex == -1)
          return;

     // Polaczenia przychodzace do tabeli
     QString tid = TableList->Table(tindex)->TableId(), 
             rid = "";
     QString comment = " - CONNECTED";
     if(!ConnectionsList->ExistsInConnections(tid, ""))
          comment = " - NOT CONNECTED";
     listWidget_3->addItem("Table: " + TableList->Table(tindex)->Title() + ". Table ID = " + TableList->Table(tindex)->TableId() + comment);
          
     // Wczytywanie wierszy
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
     {
          // Polaczenia wychodzace
          comment = " - CONNECTED";
          rid = TableList->Table(tindex)->Row(i)->RowId();
          if(!ConnectionsList->ExistsOutConnections(tid, rid))
               comment = " - NOT CONNECTED";
          listWidget_2->addItem(QString::number(i+1) + ". Row ID = " + TableList->Table(tindex)->Row(i)->RowId() + comment);
  
          comment = " - CONNECTED";
          if(!ConnectionsList->ExistsInConnections(tid, rid))
               comment = " - NOT CONNECTED";
          listWidget_3->addItem(QString::number(i+1) + ". Row ID = " + TableList->Table(tindex)->Row(i)->RowId() + comment);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when output connection row is selected.
//
// #param new_index Index of the row.
// #return No return value.
void ConnectionManager_ui::ListOfOutConnectionClick(int new_index)
{
     comboBox_2->clear();

     // Pobranie indexu tabeli i wiersza
     int tindex = listWidget->currentRow();
     int rindex = new_index;

     if((tindex == -1) || (rindex == -1))
          return;

     QString tid = TableList->Table(tindex)->TableId();
     QString rid = TableList->Table(tindex)->Row(rindex)->RowId();

     // Wczytywanie polaczen
     for(unsigned int i=0;i<ConnectionsList->Count();i++)
     {
          if((ConnectionsList->Connection(i)->SrcTable() == tid) && (ConnectionsList->Connection(i)->SrcRow() == rid))
          {
               QString item = ConnectionsList->Connection(i)->ConnId() + "; " + ConnectionsList->Connection(i)->Label();
               comboBox_2->addItem(item);
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when input connection row is selected.
//
// #param new_index Index of the row.
// #return No return value.
void ConnectionManager_ui::ListOfInConnectionClick(int new_index)
{
     comboBox->clear();

     // Pobranie indexu tabeli i wiersza
     int tindex = listWidget->currentRow();
     int rindex = new_index;

     if((tindex == -1) || (rindex == -1))
          return;

     QString tid = TableList->Table(tindex)->TableId();
     QString rid = "";
     if(rindex > 0)
          rid = TableList->Table(tindex)->Row(rindex-1)->RowId();

     // Wczytywanie polaczen
     for(unsigned int i=0;i<ConnectionsList->Count();i++)
     {
          if((ConnectionsList->Connection(i)->DesTable() == tid) && (ConnectionsList->Connection(i)->DesRow() == rid))
          {
               QString item = ConnectionsList->Connection(i)->ConnId() + "; " + ConnectionsList->Connection(i)->Label();
               comboBox->addItem(item);
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when input connection field is changed.
//
// #param new_index Index of selected combo box.
// #return No return value.
void ConnectionManager_ui::ComboBoxChange(int new_index)
{
     groupBox_4->setEnabled(true);
     if(new_index == -1)
     {
          groupBox_4->setDisabled(true);
          return;
     }

     // Pobranie i elemento
     QString conn_id = comboBox->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          groupBox_4->setDisabled(true);
          return;
     }

     // Wyszukiwanie indexow elemetow polaczenia
     QString stid = ConnectionsList->Connection(conn_index)->SrcTable();
     QString srid = ConnectionsList->Connection(conn_index)->SrcRow();
     QString dtid = ConnectionsList->Connection(conn_index)->DesTable();
     QString drid = ConnectionsList->Connection(conn_index)->DesRow();
     int dti = TableList->IndexOfID(dtid);
     int sti = TableList->IndexOfID(stid);
     int dri = -1;
     int sri = -1;
     if((dti == -1) || (sti == -1))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect table ID.", QMessageBox::Ok);
          return;
     }
     
     // Wyszukiwanie indexu wiersza z ktorego wychodz polaczenie
     for(unsigned int i=0;i<TableList->Table(dti)->RowCount();i++)
          if(TableList->Table(dti)->Row(i)->RowId() == drid)
          {
               dri = i;
               break;
          }
     if((dri == -1) && (!drid.isEmpty()))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect destination row ID.", QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu wiersza do ktorego wchodzi polaczenie
     for(unsigned int i=0;i<TableList->Table(sti)->RowCount();i++)
          if(TableList->Table(sti)->Row(i)->RowId() == srid)
          {
               sri = i;
               break;
          }
     if(sri == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect source row ID.", QMessageBox::Ok);
          return;
     }

     // Wpisujemy wsrtosci w pola okienka
     QString cut = "No";
     if(ConnectionsList->Connection(conn_index)->IsCut())
          cut = "Yes";

     label_8->setText(TableList->Table(sti)->Title());
     label_9->setText(QString::number(sri+1) + ". Row ID = " + TableList->Table(sti)->Row(sri)->RowId());
     label_10->setText(TableList->Table(dti)->Title());
     if(dri > -1)
          label_11->setText(QString::number(dri+1) + ". Row ID = " + TableList->Table(dti)->Row(dri)->RowId());
     if(dri == -1)
          label_11->setText("rule-table connection");
     label_12->setText(ConnectionsList->Connection(conn_index)->Label());
     label_13->setText(ConnectionsList->Connection(conn_index)->Description());
     label_14->setText(cut);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when output connection field is changed.
//
// #param new_index Index of selected combo box.
// #return No return value.
void ConnectionManager_ui::ComboBoxChange_2(int new_index)
{
     groupBox_5->setEnabled(true);
     if(new_index == -1)
     {
          groupBox_5->setDisabled(true);
          return;
     }

     // Pobranie i elemento
     QString conn_id = comboBox_2->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          groupBox_5->setDisabled(true);
          return;
     }

     // Wyszukiwanie indexow elemetow polaczenia
     QString stid = ConnectionsList->Connection(conn_index)->SrcTable();
     QString srid = ConnectionsList->Connection(conn_index)->SrcRow();
     QString dtid = ConnectionsList->Connection(conn_index)->DesTable();
     QString drid = ConnectionsList->Connection(conn_index)->DesRow();
     int dti = TableList->IndexOfID(dtid);
     int sti = TableList->IndexOfID(stid);
     int dri = -1;
     int sri = -1;
     if((dti == -1) || (sti == -1))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect table ID.", QMessageBox::Ok);
          return;
     }
     
     // Wyszukiwanie indexu wiersza z ktorego wychodz polaczenie
     for(unsigned int i=0;i<TableList->Table(dti)->RowCount();i++)
          if(TableList->Table(dti)->Row(i)->RowId() == drid)
          {
               dri = i;
               break;
          }
     if((dri == -1) && (!drid.isEmpty()))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect destination row ID.", QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu wiersza do ktorego wchodzi polaczenie
     for(unsigned int i=0;i<TableList->Table(sti)->RowCount();i++)
          if(TableList->Table(sti)->Row(i)->RowId() == srid)
          {
               sri = i;
               break;
          }
     if(sri == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect source row ID.", QMessageBox::Ok);
          return;
     }

     // Wpisujemy wsrtosci w pola okienka
     QString cut = "No";
     if(ConnectionsList->Connection(conn_index)->IsCut())
          cut = "Yes";

     label_22->setText(TableList->Table(sti)->Title());
     label_23->setText(QString::number(sri+1) + ". Row ID = " + TableList->Table(sti)->Row(sri)->RowId());
     label_24->setText(TableList->Table(dti)->Title());
     if(dri > -1)
          label_25->setText(QString::number(dri+1) + ". Row ID = " + TableList->Table(dti)->Row(dri)->RowId());
     if(dri == -1)
          label_25->setText("rule-table connection");
     label_26->setText(ConnectionsList->Connection(conn_index)->Label());
     label_27->setText(ConnectionsList->Connection(conn_index)->Description());
     label_28->setText(cut);
}
// -----------------------------------------------------------------------------

// #brief Function closes the window.
//
// #return No return value.
void ConnectionManager_ui::ClickClose(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Add new connection' is clicked.
//
// #return No return value.
void ConnectionManager_ui::AddNewConnection(void)
{
     int index = listWidget->currentRow();
     if(index == -1)
          index = 0;

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(true, index);
     connect(ConnEdit, SIGNAL(OnClose()), this, SLOT(Refresh()));
     ConnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Edit connection' is pressed - input connection.
//
// #return No return value.
void ConnectionManager_ui::EditInConnection(void)
{
     QString conn_id = comboBox->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect connection selected.", QMessageBox::Ok);
          return;
     }

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(false, conn_index);
     connect(ConnEdit, SIGNAL(OnClose()), this, SLOT(Refresh()));
     ConnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Edit connection' is pressed - output connection.
//
// #return No return value.
void ConnectionManager_ui::EditOutConnection(void)
{
     QString conn_id = comboBox_2->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect connection selected.", QMessageBox::Ok);
          return;
     }

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(false, conn_index);
     connect(ConnEdit, SIGNAL(OnClose()), this, SLOT(Refresh()));
     ConnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Delete connection' is pressed - input connection.
//
// #return No return value.
void ConnectionManager_ui::DeleteInConnection(void)
{
     // Wczytywanie indexu polaczenia
     QString conn_id = comboBox->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect connection selected.", QMessageBox::Ok);
          return;
     }
     
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to delete selected connection?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Skasowanie polaczenia
     ConnectionsList->Delete(conn_index);

     // Odswiezenie widoku
     xtt::fullVerification();
     Refresh();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Delete connection' is pressed - output connection.
//
// #return No return value.
void ConnectionManager_ui::DeleteOutConnection(void)
{
     // Wczytywanie indexu polaczenia
     QString conn_id = comboBox_2->currentText().split(";").at(0);
     int conn_index = ConnectionsList->IndexOfID(conn_id);

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect connection selected.", QMessageBox::Ok);
          return;
     }
     
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to delete selected connection?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Skasowanie polaczenia
     ConnectionsList->Delete(conn_index);

     // Odswiezenie widoku
     xtt::fullVerification();
     Refresh();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ConnectionManager_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               ClickClose();
               break;

          case Qt::Key_Return:
               AddNewConnection();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
