/**
 * \file	PluginEventListner_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	14.07.2009
 * \version	1.0
 * \brief	This file contains class definition that arranges plugin console window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef PLUGINEVENTLISTENER_H
#define PLUGINEVENTLISTENER_H
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_PluginEventListner.h"
// -----------------------------------------------------------------------------

class devPlugin;
// -----------------------------------------------------------------------------
/**
* \class 	PluginEventListner_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	14.07.2009
* \brief 	Class definition that arranges plugin console window.
*/
class PluginEventListner_ui : public QWidget, private Ui_PluginEventListner
{
    Q_OBJECT
    
private:

    QGridLayout* formLayout;	               ///< Pointer to grid layout form.

    bool _listeningEnabled;                 ///< Indicates if the listening is active

public:

    /// \brief Constructor PluginEditor_ui class.
    ///
    /// \param parent Pointer to parent object.
    PluginEventListner_ui(QWidget *parent = 0);

    /// \brief Destructor PluginEditor_ui class.
    ~PluginEventListner_ui(void);

    /// \brief Function sets up the window properties
    ///
    /// \return No return value.
    void initForm(void);

    /// \brief Function sets the form layout
    ///
    /// \return No return value.
    void layoutForm(void);

    /// \brief Function is triggered when key is pressed.
    ///
    /// \param event Pointer to event object.
    /// \return No return value.
    void keyPressEvent(QKeyEvent* event);

public slots:

    /// \brief Function is triggered when disable listening key is pressed.
    ///
    /// \param __state - new state of the button
    /// \return No return value.
    void onDiableStateChangeClick(bool __state);

    /// \brief Function is triggered when clear key is pressed.
    ///
    /// \return No return value.
    void clearClick(void);

    /// \brief Function is triggered when copy key is pressed.
    ///
    /// \return No return value.
    void copyClick(void);

    /// \brief Function is triggered when new plugin event occurs.
    ///
    /// \param __msgType - type of the event
    /// \param __message - event message
    /// \return No return value.
    void onNewEvent(int __msgType, QString __message);
};
// -----------------------------------------------------------------------------

extern PluginEventListner_ui* pluginEventListener;
// -----------------------------------------------------------------------------

#endif
