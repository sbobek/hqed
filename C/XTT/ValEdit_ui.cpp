/*
 *	   $Id: ValEdit_ui.cpp,v 1.32 2010-01-08 19:47:05 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../namespaces/ns_hqed.h"
#include "../Settings_ui.h"
#include "ValEdit_ui.h"
// -----------------------------------------------------------------------------

ValueEdit_ui* ValEdit;
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
//
// #param parent Pointer to parent object.
ValueEdit_ui::ValueEdit_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png"));
    hqed::centerWindow(this);
    
    connect(toolButton_3, SIGNAL(clicked()), this, SLOT(AddVal()));
    connect(toolButton_2, SIGNAL(clicked()), this, SLOT(RplVal()));
    connect(toolButton, SIGNAL(clicked()), this, SLOT(DelVal()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(Accept()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
//
// #param line String of input values.
// #param parent Pointer to parent object.
ValueEdit_ui::ValueEdit_ui(QString line, QWidget*)
{
	setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
	
	connect(toolButton_3, SIGNAL(clicked()), this, SLOT(AddVal()));
	connect(toolButton_2, SIGNAL(clicked()), this, SLOT(RplVal()));
	connect(toolButton, SIGNAL(clicked()), this, SLOT(DelVal()));
	connect(pushButton, SIGNAL(clicked()), this, SLOT(Accept()));
	setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);

	values = line.split(";", QString::SkipEmptyParts);
	listWidget->clear();
	
	for(int i=0;i<values.size();i++)
	{
		QString nv = values.at(i);
		
		// Wstepna obrobka tekstu
		// Usuwanie spacji na koncach
		for(;(nv.length() > 0) && (nv[0] == ' ');)
	          nv = nv.remove(0, 1);
		for(;(nv.length() > 0) && (nv[nv.length()-1] == ' ');)
	          nv = nv.remove(nv.length()-1, 1);
			
		if(nv == "")
			continue;

	     listWidget->addItem(nv);
	}
	
	// Jeszcze nalezy przekopiowac poprawione wartosci do zmiennej values
	values.clear();
	for(int i=0;i<listWidget->count();i++)
		values.append(listWidget->item(i)->text());
}
// -----------------------------------------------------------------------------

// #brief Function adds new value.
//
// #return No return value.
void ValueEdit_ui::AddVal(void)
{
     if(lineEdit->text() == "")
          return;
		
	// Opcje zastepowania
	QString spaceReplacer = Settings->spaceReplacer();
		
	QStringList newValues = lineEdit->text().split(";", QString::SkipEmptyParts);
	
	for(int i=0;i<newValues.size();i++)
	{
		QString nv = newValues.at(i);
		
		// Wstepna obrobka tekstu
		// Usuwanie spacji na koncach
		for(;(nv.length() > 0) && (nv[0] == ' ');)
	          nv = nv.remove(0, 1);
		for(;(nv.length() > 0) && (nv[nv.length()-1] == ' ');)
	          nv = nv.remove(nv.length()-1, 1);
			
		// Zamiana spacji na zamiennik
		for(;nv.indexOf(" ") > -1;)
			nv = nv.replace(nv.indexOf(" "), 1, spaceReplacer);
			
		if(nv == "")
			continue;
	
	     if(values.indexOf(nv) > -1)
	     {
	          QMessageBox::information(NULL, "HQEd", "Value " + nv + " is already exists.", QMessageBox::Ok);
	          return;
	     }

	     values.append(nv);
	}
	
     listWidget->clear();
     listWidget->addItems(values);
     lineEdit->setText("");
}
// -----------------------------------------------------------------------------

// #brief Function deletes value.
//
// #return No return value.
void ValueEdit_ui::DelVal(void)
{
     if(listWidget->currentRow() == -1)
          return;

     values.removeAt(listWidget->currentRow());
     listWidget->clear();
     listWidget->addItems(values);
}
// -----------------------------------------------------------------------------

// #brief Function replies selected value.
//
// #return No return value.
void ValueEdit_ui::RplVal(void)
{
     if(listWidget->currentRow() == -1)
          return;

     if(lineEdit->text() == "")
          return;
		
	// Opcje zastepowania
	QString spaceReplacer = Settings->spaceReplacer();
		
	QStringList newValues = lineEdit->text().split(";", QString::SkipEmptyParts);
	
	for(int i=0;i<newValues.size();i++)
	{
		QString nv = newValues.at(i);
		
		// Wstepna obrobka tekstu
		// Usuwanie spacji na koncach
		for(;(nv.length() > 0) && (nv[0] == ' ');)
	          nv = nv.remove(0, 1);
		for(;(nv.length() > 0) && (nv[nv.length()-1] == ' ');)
	          nv = nv.remove(nv.length()-1, 1);
			
		// Zamiana spacji na zamiennik
		for(;nv.indexOf(" ") > -1;)
			nv = nv.replace(nv.indexOf(" "), 1, spaceReplacer);
			
		if(nv == "")
			continue;
		
	     if((values.indexOf(nv) > -1) && (values.indexOf(nv) != listWidget->currentRow()))
		{
			QMessageBox::information(NULL, "HQEd", "Value " + nv + " is already exists.", QMessageBox::Ok);
			return;
		}
		
		if(i == 0)
			values.replace(listWidget->currentRow(), nv);
		if(i > 0)
			values.insert(listWidget->currentRow()+i, nv);
	}

     listWidget->clear();
     listWidget->addItems(values);
     lineEdit->setText("");
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Ok' is pressed.
//
// #return No return value.
void ValueEdit_ui::Accept(void)
{
     close();
     emit ClickOK();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ValueEdit_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               Accept();
               break;

          case Qt::Key_Delete:
               DelVal();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
