/*
 *	   $Id: GroupEditor_ui.cpp,v 1.32 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "SelectGroup_ui.h"
#include "GroupEditor_ui.h"

#include "../../namespaces/ns_hqed.h"

#include "../../M/XTT/XTT_AttributeList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
// -----------------------------------------------------------------------------

GroupEditor_ui* GroupEdit;
// -----------------------------------------------------------------------------

// #brief Constructor GroupEditor_ui class.
//
// #param parent Pointer to parent object.
GroupEditor_ui::GroupEditor_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

// #brief Constructor GroupEditor_ui class.
//
// #param _new Determines whether new group should be added(True) or not(False).
// #param _index Index of edited group.
// #param _isMain Determines whether main group should be created(True) or not(False).
// #param parent Pointer to parent object.
GroupEditor_ui::GroupEditor_ui(bool _new, int _index, bool _isMain, QWidget*)
{
     setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png")); 

     //connect(pushButton, SIGNAL(clicked()), this, SLOT(CreateNewMainGroup()));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickOk()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(ClickCancel()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(GroupSelectClick()));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);

     fNew = _new;
     fIndex = _index;
     fIsMain = _isMain;

     SetWindow();
}
// -----------------------------------------------------------------------------

// #brief Function sets group editor window.
//
// #return No return value.
void GroupEditor_ui::SetWindow(void)
{
     // Gdy ne tworzymy nowej grupy to wypisujemy jej watosci pol
     if(!fNew)
     {
          if((fIndex < 0) || (fIndex >= (int)AttributeGroups->Count()))
          {
               const QString content = "Incorrect index is given.\nThe required group index: " + QString::number(fIndex, 10);
               QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
               return;
          }

          pushButton_3->setText(AttributeGroups->CreatePath(AttributeGroups->Group(fIndex)->Name()));
          lineEdit->setText(AttributeGroups->Group(fIndex)->Name());
          lineEdit_2->setText(AttributeGroups->Group(fIndex)->Description());
     }

     if((fIsMain) && (fNew))
     {
          pushButton_3->setEnabled(fIndex != 0);
          pushButton_3->setText("Main Level");
     }
     if(!fIsMain)
     {
          if((fIndex < 0) || (fIndex >= (int)AttributeGroups->Count()))
          {
               const QString content = "Incorrect index is given.\nThe required group index: " + QString::number(fIndex, 10);
               QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
               return;
          }
     }

     if((!fIsMain) || (!fNew))
     {
          pushButton_3->setEnabled(fIndex != 0);
          pushButton_3->setText(AttributeGroups->CreatePath(AttributeGroups->Group(fIndex)->Name()));
     }
}
// -----------------------------------------------------------------------------

// #brief Function shows select group window.
//
// #return No return value.
void GroupEditor_ui::GroupSelectClick(void)
{
     delete SelGroup;
     SelGroup = new SelectGroup_ui;
     connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(OnGroupChange()));
     SelGroup->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select group window is closed.
//
// #return No return value.
void GroupEditor_ui::OnGroupChange(void)
{
     int pindex = AttributeGroups->IndexOfName(SelGroup->Result());
     if(pindex == -1)
          return;

     if(pindex == 0)
     {
          const QString content = "You can't modify Global group properties.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     fIndex = pindex;
     SetWindow();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Cancel' is pressed.
//
// #return No return value.
void GroupEditor_ui::ClickCancel(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Ok' is pressed.
//
// #return No return value.
void GroupEditor_ui::ClickOk(void)
{
     // Sprawdzanie czy wprowadzno poprawne dane
     QString nname = lineEdit->text().toLower();
     QString ndesc = lineEdit_2->text().toLower();
     QString ngroup = pushButton_3->text().split(".", QString::SkipEmptyParts, Qt::CaseInsensitive).last();

     if((nname == "") || (nname == XTT_Attribute::_any_operator_) || (nname == XTT_Attribute::_not_definded_operator_))
     {
          const QString content = "Incorrect name is given.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Sprawdzanie czy podana juz nie istnieje
     if(((fNew) && (AttributeGroups->IndexOfName(nname) > -1)) ||
        ((!fNew) && (AttributeGroups->IndexOfName(nname) > -1) && (AttributeGroups->IndexOfName(nname) != fIndex)))
     {
          const QString content = "The name is already exists.\nPlease, enter another name.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Wskaznik do ustawianej grupy
     XTT_AttributeList* ptr;

     // Dodawanie nowej grupy
     if(fNew)
     {
          AttributeGroups->Add();
          ptr = AttributeGroups->Group(AttributeGroups->Count()-1);
     }

     // Gdy tylko edycja to pobieramy wskaznik
     if(!fNew)
     {
          ptr = AttributeGroups->Group(fIndex);
     }

     // Kiedy nowa i glowna to:
     if((fIsMain) && (fNew))
          ptr->setLevel(0);

     // Kiedy nowa i dziecko to ustawiamy jej rodzica
     if((!fIsMain) && (fNew))
     {
          int g_index = AttributeGroups->IndexOfName(ngroup);

          if(g_index == -1)
          {
               const QString content = "Application can't find parent group.";
               QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);
               return;
          }
          ptr->setLevel(AttributeGroups->Group(g_index)->Level()+1);
          ptr->setParentId(AttributeGroups->Group(g_index)->ID());
     }

     // Ustawienie nazwy itp
     ptr->setName(nname);
     ptr->setDescription(ndesc);

     emit onClose();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void GroupEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               ClickCancel();
               break;

          case Qt::Key_Return:
               ClickOk();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
