 /**
 * \file	ConnectionEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges connection editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */



#ifndef CONNECTIONEDITORH
#define CONNECTIONEDITORH
// -----------------------------------------------------------------------------
 
#include "ui_ConnectionEditor.h"
// -----------------------------------------------------------------------------
/**
* \class 	ConnectionEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007 
* \brief 	Class definition that arranges connection editor window.
*/
class ConnectionEditor_ui : public QWidget, private Ui_ConnectionEditor
{
    Q_OBJECT

public:

    /// \brief Constructor ConnectionEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	ConnectionEditor_ui(QWidget *parent = 0);

    /// \brief Constructor AttrEditor class.
	///
	/// \param _new Determines whether new connection should be created(true) or not(false).
	/// \param _index Index of connection.
	/// \param parent Pointer to parent object.
	ConnectionEditor_ui(bool _new, int _index, QWidget *parent = 0);

	bool fNew;	///< Determines creating new connection.

	int fIndex;	///< Index of edited connection.

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent*);

public slots:

	/// \brief Function is triggered when button OK is pressed.
	///
	/// \return No return value.
	void ClickOK(void);

	/// \brief Function sets up window parameters.
	///
	/// \return No return value.
	void setUpWindow(void);

	/// \brief Function is triggered when combo box in the table is changed.
	///
	/// \param new_index Selected index of combo box.
	/// \return No return value.
	void ComboBoxChange(int new_index);

	/// \brief Function is triggered when select table field is changed.
	///
	/// \param new_index Selected index of combo box.
	/// \return No return value.
	void ComboBoxChange_3(int new_index);

	/// \brief Function is triggered when select table field is changed.
	///
	/// \param new_index Selected index of combo box.
	/// \return No return value.
	void ComboBoxChange_5(int new_index);

	/// \brief Function is triggered when select row field is changed.
	///
	/// \param new_index Selected index of combo box.
	/// \return No return value.
	void ComboBoxChange_6(int new_index);

	/// \brief Function is triggered when select connection id is changed.
	///
	/// \param new_index Selected index of combo box.
	/// \return No return value.
	void ComboBoxChange_7(int new_index);

signals:

	/// \brief Function sends signal of closing window.
	///
	/// \return No return value.
	void OnClose(void);
};
// -----------------------------------------------------------------------------

extern ConnectionEditor_ui* ConnEdit;
// -----------------------------------------------------------------------------

#endif
