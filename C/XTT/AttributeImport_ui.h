/**
 * \file	AttributeImport_ui.h
 * \author	Marcin Koziej m-key88@o2.pl
 * \date 	05.07.2011
 * \version	1.0
 * \brief	This file contains class definition that can arrange dialog 'AttributeImport'.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef ATTRIBUTEIMPORT_H
#define ATTRIBUTEIMPORT_H
// -----------------------------------------------------------------------------

#include <QDialog>
#include <QListWidgetItem>
#include <QDomElement>
#include <QGridLayout>
// -----------------------------------------------------------------------------

namespace Ui {
class AttributeImport;
}
// -----------------------------------------------------------------------------

/**
* \class 	AttributeImport
* \author	Marcin Koziej m-key88@o2.pl
* \date 	23.06.2011
* \brief 	Class definition that arranges import attribute window.
*/
class AttributeImport : public QDialog
{
     Q_OBJECT

public:

     /// \brief Constructor AttributeImport class.
     ///
     /// \param parent Pointer to parent object.
     /// \param cT List of confict types.
     /// \param cA List of conflict attributes.
     explicit AttributeImport(QWidget *parent, QList<QDomElement> cT, QList<QDomElement> cA);

     /// \brief Destructor AttributeImport class.
     ~AttributeImport();

     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);

     /// \brief Function returns selected types.
     ///
     /// \return List of selected types.
     QList<QDomElement> getSTypes(void);

     /// \brief Function returns unselected types.
     ///
     /// \return List of unselected types.
     QList<QDomElement> getUTypes(void);

     /// \brief Function returns selected attributes.
     ///
     /// \return List of selected attributes.
     QList<QDomElement> getSAttrs(void);

     /// \brief Function returns unselected attributes.
     ///
     /// \return List of unselected attributes.
     QList<QDomElement> getUAttrs(void);

     /// \brief Function returns choice for selected items.
     ///
     /// \return Combobox choice QString.
     QString getSelChoice(void);

     /// \brief Function returns choice for unselected items.
     ///
     /// \return Combobox choice QString.
     QString getOthChoice(void);

private:
     Ui::AttributeImport *ui;         ///< Pointer to the form.
     QList<QDomElement> conTypes,     ///< Variable that contains conflict types.
     conAttrs,     ///< Variable that contains conflict attributes.
     selTypes,     ///< Variable that contains selected types.
     unselTypes,   ///< Variable that contains unselected types.
     selAttrs,     ///< Variable that contains selected attributes.
     unselAttrs;   ///< Variable that contains unselected attributes.
     QString selChoice;               ///< Variable that stores choice for selected items.
     QString othChoice;               ///< Variable that stores choice for unselected items.
     QGridLayout* formLayout;

public slots:

     /// \brief Slot processed after pressing OK button.
     ///
     /// \return No return value.
     void confirm();
};
// -----------------------------------------------------------------------------

#endif // ATTRIBUTEIMPORT_H
