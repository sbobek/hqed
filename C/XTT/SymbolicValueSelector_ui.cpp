/*
 *     $Id: SymbolicValueSelector_ui.cpp,v 1.6.4.1 2010-11-10 02:02:44 llk Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../namespaces/ns_hqed.h"

#include "../../M/hType.h"
#include "../../M/hValue.h"
#include "../../M/hDomain.h"
#include "../../M/hSetItem.h"
#include "../../M/hValueResult.h"

#include "SymbolicValueSelector_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor SymbolicValueSelector_ui class.
//
// #param parent Pointer to parent object.
SymbolicValueSelector_ui::SymbolicValueSelector_ui(QWidget* /*parent*/)
{
    initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor SymbolicValueSelector_ui class.
SymbolicValueSelector_ui::~SymbolicValueSelector_ui(void)
{
     delete gb3Layout;
     delete gb2Layout;
     delete gb1Layout;
     delete swp2Layout;
     delete swp1Layout;
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void SymbolicValueSelector_ui::initForm(void)
{
     setupUi(this);
     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     layoutForm();
     hqed::centerWindow(this);
     
     stackedWidget->setCurrentIndex(0);
     
     connect(pushButton, SIGNAL(clicked()), this, SLOT(okClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeClick()));
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void SymbolicValueSelector_ui::layoutForm(void)
{
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,      0, 0, 1, 3);
     formLayout->addWidget(stackedWidget, 1, 0, 1, 3);
     formLayout->addWidget(pushButton,    2, 1);
     formLayout->addWidget(pushButton_2,  2, 2);
     formLayout->setRowStretch(0, 1);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     swp1Layout = new QGridLayout;
     swp1Layout->addWidget(groupBox_2, 0, 0);
     swp1Layout->setRowStretch(0, 1);
     swp1Layout->setColumnStretch(0, 1);
     hqed::setupLayout(swp1Layout);
     stackedWidget->widget(0)->setLayout(swp1Layout);
     
     gb1Layout = new QGridLayout;
     gb1Layout->addWidget(comboBox, 0, 0);
     gb1Layout->setRowStretch(0, 1);
     gb1Layout->setColumnStretch(0, 1);
     hqed::setupLayout(gb1Layout);
     groupBox->setLayout(gb1Layout);
     
     swp2Layout = new QGridLayout;
     swp2Layout->addWidget(groupBox_3, 0, 0);
     swp2Layout->setRowStretch(0, 1);
     swp2Layout->setColumnStretch(0, 1);
     hqed::setupLayout(swp2Layout);
     stackedWidget->widget(1)->setLayout(swp2Layout);
     
     gb2Layout = new QGridLayout;
     gb2Layout->addWidget(spinBox, 0, 0);
     gb2Layout->setRowStretch(0, 1);
     gb2Layout->setColumnStretch(0, 1);
     hqed::setupLayout(gb2Layout);
     groupBox_2->setLayout(gb2Layout);
     
     gb3Layout = new QGridLayout;
     gb3Layout->addWidget(radioButton,   0, 1);
     gb3Layout->addWidget(radioButton_2, 0, 2);
     gb3Layout->setColumnStretch(0, 1);
     gb3Layout->setColumnStretch(3, 1);
     hqed::setupLayout(gb3Layout);
     groupBox_3->setLayout(gb3Layout);
}
// -----------------------------------------------------------------------------

// #brief Function that checks the correctness of the recived parameters
//
// #return true if ok, otherwise false
bool SymbolicValueSelector_ui::checkMode(void)
{
     if((_value == NULL) || (_type == NULL))
          return false;

     if(_type->typeClass() != TYPE_CLASS_DOMAIN)
          return false;
          
     if(_type->baseType() != SYMBOLIC_BASED)
          return false;
          
     if(_type->domain() == NULL)
          return false;
          
     if(!_type->domain()->ordered())
          return false;
     
     hValueResult hvr = _value->value();
     if(!(bool)hvr)
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form according to the recieved parameters
//
// #param __type - Pointer to type of the value object
// #param __setClass - the type of the set
// #li 0-set
// #li 1-domain
// #li 2-action context
// #li 3-coditional context
// #param __value - Pointer to the edited value
// #return SET_WINDOW_MODE_xxx value as a result
int SymbolicValueSelector_ui::setMode(hType* __type, int __setClass, hValue* __value)
{
     _type = __type;
     _setClass = __setClass;
     _value = __value;
     
     if(!checkMode())
          return SET_WINDOW_MODE_FAIL;
     
     setWindowTitle("Enter value");
     groupBox->setTitle("Enter symbolic value:");
     comboBox->setEditable(_setClass == 1);
     
     if(_setClass != 1)
     {
          stackedWidget->setCurrentIndex(1);
          radioButton->setChecked(_value->definitionType() == SYM);
          radioButton_2->setChecked(_value->definitionType() == NUM);
          comboBox->addItems(_type->domain()->stringItems());
          setWindowTitle("Select value");
          groupBox->setTitle("Select symbolic/numeric value:");
          
          // Setting the current index of the comboBox
          hValueResult hvr = _value->value();
          if(_value->definitionType() == SYM)
               comboBox->setCurrentIndex(_type->domain()->contain((QString)hvr));
          if(_value->definitionType() == NUM)
               comboBox->setCurrentIndex(_type->domain()->symbolicEquivalent(((QString)hvr).toInt()));
     }
     
     if(_setClass == 1)
     {
          ValueDefinitionType dt = _value->definitionType();
          
          _value->setDefinitionType(SYM);
          hValueResult hsvr = _value->value();
          _value->setDefinitionType(NUM);
          hValueResult hnvr = _value->value();
          
          _value->setDefinitionType(dt);
          
          comboBox->setEditText(QString(hsvr));
          spinBox->setValue((QString(hnvr)).toInt());
     }
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void SymbolicValueSelector_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               okClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that updates the current value object according to user setting
//
// #return true on succes, otherwise false
bool SymbolicValueSelector_ui::apply(void)
{
     // When edited object is not from the domain
     if(_setClass != 1)
     {
          int valIndex = comboBox->currentIndex();
          if(valIndex == -1)
          {
               QMessageBox::information(NULL, "HQEd", "Value not selected.", QMessageBox::Ok);
               return false;
          }
          
          // When user sets the symbolic representation
          if(radioButton->isChecked())
          {
               _value->setDefinitionType(SYM);
               _type->domain()->at(valIndex)->valuePtr()->copyTo(_value);
          }
          
          // When user sets the numeric representation
          if(radioButton_2->isChecked())
          {
               _value->setDefinitionType(NUM);
               
               hValueResult hvr = _type->domain()->at(valIndex)->value();
               if(!(bool)hvr)
               {
                    QMessageBox::critical(NULL, "HQEd", "Error while calculating value: \'" + _type->domain()->at(valIndex)->toString() + "\'.\nError message: " + (QString)hvr + ".", QMessageBox::Ok);
                    return false;
               }
               
               int i_numrep;
               double d_numrep;
               if(_type->mapToNumeric((QString)hvr, d_numrep, true) != H_TYPE_ERROR_NO_ERROR)
               {
                    QMessageBox::critical(NULL, "HQEd", "Error while converting value \'" + (QString)hvr + "\' to numeric representation.\nError message: " + _type->lastError() + "", QMessageBox::Ok);
                    return false;
               }
               i_numrep = (int)d_numrep;
               
               _value->setValue(QString::number(i_numrep));
               _value->setType(VALUE);
          }
     }
     
     // When edited object is from the domain
     if(_setClass == 1)
     {
          _value->setDefinitionType(SYM);
          _value->setValue(comboBox->currentText());
          _value->setDefinitionType(NUM);
          _value->setValue(QString::number(spinBox->value()));
          
          _value->setDefinitionType(SYM);
          _value->setType(VALUE);
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when select button is pressed
//
// #return No return value.
void SymbolicValueSelector_ui::okClick(void)
{
     if(!apply())
          return;
     
     emit onOk();
     closeClick();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when close button is pressed
//
// #return No return value.
void SymbolicValueSelector_ui::closeClick(void)
{    
     emit onClose();
     close();
}
// -----------------------------------------------------------------------------
