/*
 *     $Id: MultipleValueEditor_ui.cpp,v 1.13.4.1 2010-12-19 23:36:35 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QInputDialog>

#include "../../M/XTT/XTT_Attribute.h"

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hSetItem.h"
#include "../../M/hValue.h"
#include "../../M/hFunction.h"
#include "../../M/hExpression.h"
#include "../../M/hMultipleValue.h"

#include "../../namespaces/ns_hqed.h"

#include "SetEditor_ui.h"
#include "ExpressionEditor_ui.h"
#include "AttributeManager_ui.h"
#include "MultipleValueEditor_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor ValueEditor_ui class.
//
// #param parent Pointer to parent object.
MultipleValueEditor_ui::MultipleValueEditor_ui(QWidget* /*parent*/)
{
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor ValueEdit_ui class.
MultipleValueEditor_ui::~MultipleValueEditor_ui(void)
{
     delete _mv;
     
     delete formLayout;
     delete gp3Layout;
     delete sw1page0;
     delete sw1page1;
     delete sw1page2;
     
     if(expressionEditor != NULL)
          delete expressionEditor;
     if(setEditor != NULL)
          delete setEditor;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void MultipleValueEditor_ui::initForm(void)
{
     setupUi(this);

     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     layoutForm();
     hqed::centerWindow(this);
     
     _type = NULL;                            
     _mv = NULL;                       
     _cell = NULL;                       
     _setClass = -1;                           
     _singleRequired = true;                    
     _avr = false;    

     setEditor = NULL;
     expressionEditor = NULL;
     
     // Initialize window configuration parameters
     vdt_cv = -1;                              
     vdt_avr = -1;                             
     vdt_exp = -1;     
     
     sv_get = -1;                              
     sv_nd = -1;                               
     sv_any = -1;                                             

     connect(pushButton, SIGNAL(clicked()), this, SLOT(getValueClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(valAttributeSelectionClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(okButtonClick()));
     connect(pushButton_4, SIGNAL(clicked()), this, SLOT(cancelButtonClick()));
     connect(pushButton_5, SIGNAL(clicked()), this, SLOT(valExprDefinitionClick()));
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(typeOfValDefChanged(int)));
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void MultipleValueEditor_ui::layoutForm(void)
{
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox_3,   0, 0, 1, 3);
     formLayout->addWidget(pushButton_3, 1, 1);
     formLayout->addWidget(pushButton_4, 1, 2);
     formLayout->setRowStretch(2, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(label,         0, 0);
     gp3Layout->addWidget(comboBox,      0, 1);
     gp3Layout->addWidget(stackedWidget, 1, 0, 1, 2);
     gp3Layout->setColumnStretch(0, 0);
     gp3Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);
     
     sw1page0 = new QGridLayout;
     sw1page0->addWidget(label_7,    0, 0);
     sw1page0->addWidget(comboBox_3, 0, 1);
     sw1page0->addWidget(pushButton, 0, 2);
     sw1page0->addWidget(label_2,    1, 0);
     sw1page0->addWidget(label_3,    1, 1, 1, 2);
     sw1page0->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page0);
     stackedWidget->widget(0)->setLayout(sw1page0);
     
     sw1page1 = new QGridLayout;
     sw1page1->addWidget(label_4,      0, 0);
     sw1page1->addWidget(pushButton_2, 0, 2);
     sw1page1->addWidget(label_5,      1, 0, 1, 3);
     sw1page1->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page1);
     stackedWidget->widget(1)->setLayout(sw1page1);
     
     sw1page2 = new QGridLayout;
     sw1page2->addWidget(label_9,      0, 0);
     sw1page2->addWidget(pushButton_5, 0, 2);
     sw1page2->addWidget(label_10,     1, 0, 1, 3);
     sw1page2->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page2);
     stackedWidget->widget(2)->setLayout(sw1page2);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void MultipleValueEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               cancelButtonClick();
               break;

          case Qt::Key_Return:
               okButtonClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the size of the window is changing.
//
// #param QResizeEvent* event - the object that contains the event parameters
// #return No return value.
void MultipleValueEditor_ui::resizeEvent(QResizeEvent* event) 
{
     if(event->size().height() != event->oldSize().height())
          event->ignore();

     QWidget::resizeEvent(event);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when OK button is pressed
//
// #return No return value.
void MultipleValueEditor_ui::okButtonClick(void)
{
     if(!spValueChanged(-2, _mv))
          return;

     emit okClicked();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when cancel button is pressed
//
// #return No return value.
void MultipleValueEditor_ui::cancelButtonClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the window is going to be closed
//
// #param QCloseEvent* event - pointer to the object that cntains the event parameters
// #return No return value.
void MultipleValueEditor_ui::closeEvent(QCloseEvent* /*event*/)
{
     emit windowClosed();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the window according to the item parameters and values
//
// #param hMultipleValue* __item - pointer to the item that mast be displayed. If it is NULL the item stored in _sitem will be displayed
// #return No return value.
void MultipleValueEditor_ui::displayItem(hMultipleValue* __item)
{
     hMultipleValue* __sitem = __item;
     if(__sitem == NULL)
          __sitem = _mv;

     // Display setItem
     int idxs[] = {vdt_cv, vdt_cv, vdt_cv, vdt_avr, vdt_exp};
     int svidx[] = {sv_any, sv_nd};
     QString svstrs[] = {XTT_Attribute::_any_operator_, XTT_Attribute::_not_definded_operator_};

     comboBox->setCurrentIndex(idxs[__sitem->type()]);
     label_3->setText(__sitem->valueField()->toString());
     label_5->setText(__sitem->attributeFieldToString());
     label_10->setText(__sitem->expressionField()->toString());

     for(int i=0;i<2;++i)
     {
          if((__sitem->valueField()->toString() == svstrs[i]) && (svidx[i] > -1) && (comboBox_3->currentIndex() != svidx[i]))
               comboBox_3->setCurrentIndex(svidx[i]);
     } 
     //pushButton->setEnabled(comboBox_3->currentIndex() == sv_get);
}
// -----------------------------------------------------------------------------

// #brief Function sets the window working mode
//
// #param *__type - data type of the set
// #param *__mv - pointer to the edited item
// #param __setClass - type of the set
// #li 0-set
// #li 1-domain
// #li 2-action context
// #li 3-coditional context
// #param __singleRequired - denotes if the set can contains more than one value
// #param __avr - denotes if the attribute value reference is allowed
// #param *__ptr - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
// #return SET_WINDOW_MODE_xxx value as a result
int MultipleValueEditor_ui::setMode(hType* __type, hMultipleValue* __mv, int __setClass, bool __singleRequired, bool __avr, XTT_Cell* __ptr)
{
     _type = __type;
     _mv = __mv->copyTo();
     _setClass = __setClass;
     _singleRequired = (__singleRequired) || ((_setClass == 1) && (_type->baseType() == SYMBOLIC_BASED));
     _avr = __avr;
     _cell = __ptr;
     if(!isModeCorrect())
          return SET_WINDOW_MODE_FAIL;

     // Initializing the list of type of value definitions
     QStringList vdts = QStringList() << "Constant value";
     vdt_cv = 0;              
     if(((_setClass == 0) || (_setClass == 2)) && 
        (_avr))
     {    
          vdt_avr = vdts.size();
          vdts << "Attribute reference";
     }
     
     if((_setClass == 0) || (_setClass == 2))
     {
          vdt_exp = vdts.size();
          vdts << "Expression";
     }

     comboBox->clear();
     comboBox->addItems(vdts);
     comboBox->setCurrentIndex(vdt_cv);

     // Initializing the list of  specified values
     QStringList svl = QStringList() << "Set";
     sv_get = 0;  

     // FIXME - is this condition correct ???
     if(_setClass != 1)// && (_singleRequired))
     {
          sv_nd = svl.size();    
          svl << XTT_Attribute::_not_definded_operator_;
          sv_any = svl.size();
          svl << XTT_Attribute::_any_operator_;
     }

     comboBox_3->clear();
     comboBox_3->addItems(svl);
     comboBox_3->setCurrentIndex(sv_get);

     displayItem();

     // Connecting slots and signals
     connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(spValueChanged(int))); 

     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function checks if the current mode is correct
//
// #return true if the current mode is correct, otherwise false
bool MultipleValueEditor_ui::isModeCorrect(void)
{
     bool res = true;
     
     if(_type == NULL)
          res = false;
          
     if(_mv == NULL)
          return false;
          
     if(_mv->parentType() == NULL)
          return false;
          
     if((_setClass != 0) && (_setClass != 1) && (_setClass != 2) && (_setClass != 3))
          res = false;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when get value button is clicked
//
// #return No return value.
void MultipleValueEditor_ui::getValueClick(void)
{
     comboBox_3->setCurrentIndex(sv_get);
     
     if(setEditor != NULL)
          delete setEditor;
     setEditor = new SetEditor_ui;
     
     connect(setEditor, SIGNAL(okClicked()), this, SLOT(setChanged()));

     // When the setItem is not defined correctly
     if(_mv->valueField() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined value object.", QMessageBox::Ok);
          return;
     }
     
     // When the setItem is not defined correctly
     if(_mv->valueField()->parentType() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined value parent set object.", QMessageBox::Ok);
          return;
     }
     
     // Setting the apropriate window working mode
     int smr = setEditor->setMode(_type, _mv->valueField(), 3/*_setClass*/, _singleRequired, _avr, _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     setEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when set editor is closed with ok button
//
// #return No return value.
void MultipleValueEditor_ui::setChanged(void)
{
     hSet* result = setEditor->result();
     hMultipleValue* new_mv = new hMultipleValue(result);
     
     if(!spValueChanged(sv_get, new_mv))
     {
          delete new_mv;
          return;
     }
          
     // If the set is ok
     if(_mv != NULL)
          delete _mv;
     _mv = new_mv;
     
     // Refresh the view
     displayItem();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of value definition has changed
//
// #param int __newIndex - nowy index
// #return No return value.
void MultipleValueEditor_ui::typeOfValDefChanged(int __newIndex)
{
     if(__newIndex == vdt_cv)
          stackedWidget->setCurrentIndex(0);
     if(__newIndex == vdt_avr)
          stackedWidget->setCurrentIndex(1);
     if(__newIndex == vdt_exp)
          stackedWidget->setCurrentIndex(2);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of specify value changed
//
// #param int __newIndex - new specify value index
// #return No return value.
void MultipleValueEditor_ui::spValueChanged(int __newIndex)
{
     hMultipleValue* old_mv = _mv;

     pushButton->setEnabled(__newIndex == sv_get);
     
     // If op:nd
     if(__newIndex == sv_nd)
          _mv = new hMultipleValue(NOT_DEFINED, _type);
          
     // If op:any
     if(__newIndex == sv_any)
          _mv = new hMultipleValue(ANY_VALUE, _type);
          
     // If set
     if(__newIndex == sv_get)      // We do not call the "spValueChanged" function - first the button should be preessed
          return;
          
     // Deleting old object of mv. This instruction must be called here because in here we are sure that _mv pointer
     // is allocated. In case of "if(__newIndex == sv_get)" the pointer is not allocated and that's why is shouldn't be deleted
     if(old_mv != NULL)
          delete old_mv;
          
     spValueChanged(__newIndex, _mv);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when select attribute button is pressed
//
// #return No return value.
void MultipleValueEditor_ui::valAttributeSelectionClick(void)
{
     if(AttrManager != NULL)
          delete AttrManager;

     AttrManager = new AttributeManager_ui;

     connect(AttrManager, SIGNAL(onSelectClose()), this, SLOT(valAttributeSelectionFinish()));

     // When the setItem is not defined correctly
     if(_mv == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }

     // Setting the apropriate window working mode
     int smr = AttrManager->setMode(_type, _setClass, _singleRequired, _mv->attributeField(), _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     AttrManager->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the attribute manager is closed with select button
//
// #return No return value.
void MultipleValueEditor_ui::valAttributeSelectionFinish(void)
{
     _mv->setAttribute(AttrManager->selectionResult());
     _mv->setType(ATTRIBUTE);
     
     label_5->setText(_mv->attributeFieldToString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when edit expression for value is pressed
//
// #return No return value.
void MultipleValueEditor_ui::valExprDefinitionClick(void)
{
     if(expressionEditor != NULL)
          delete expressionEditor;
     expressionEditor = new ExpressionEditor_ui;
     
     connect(expressionEditor, SIGNAL(okClicked()), this, SLOT(valExprDefinitionFinish()));
     connect(expressionEditor, SIGNAL(windowClose()), this, SLOT(valExprDefinitionWindowClosed()));

     // When the setItem is not defined correctly
     if(_mv->expressionField() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }
     
     // Calculating the result multiplicity
     int res_multiplicity = FUNCTION_TYPE__SINGLE_VALUED;
     if(!_singleRequired)
          res_multiplicity = FUNCTION_TYPE__MULTI_VALUED;
          
     // Setting the apropriate window working mode
     int smr = expressionEditor->setMode(res_multiplicity, _type, _avr, _mv->expressionField(), _setClass, _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     expressionEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed with ok button
//
// #return No return value.
void MultipleValueEditor_ui::valExprDefinitionFinish(void)
{
     delete _mv->expressionField();
     _mv->setExpression(expressionEditor->result());
     _mv->setType(EXPRESSION);
     
     label_10->setText(_mv->expressionField()->toString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed
//
// #return No return value.
void MultipleValueEditor_ui::valExprDefinitionWindowClosed(void)
{    
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of specify value change
//
// #param int __spIndex - new specify value index, if -2 that means the window is closed by ok button
// #param hMultipleValue* __value - poiter to the multiple value
// #return true if all the operation are succefully, otherwise false
bool MultipleValueEditor_ui::spValueChanged(int __spIndex, hMultipleValue* __value)
{
     if(__spIndex == -1)
          return true;

     if(__value == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "The value is not defined.", QMessageBox::Ok);
          displayItem();
          return false;
     }

     hMultipleValue* newMultiValue = __value->copyTo();
     bool checkConstraints = (_setClass != 1) && (!__value->isAVRused());

     if(_type->isSetCorrect(newMultiValue->value().toSet(false), checkConstraints) != H_TYPE_ERROR_NO_ERROR)
     {
          QMessageBox::critical(NULL, "HQEd", _type->lastError(), QMessageBox::Ok);
          displayItem();
          return false;
     }

     delete newMultiValue;
     displayItem(__value);

     return true;
}
// -----------------------------------------------------------------------------

// #brief Function returns the item that has been changed by using this dialog
//
// #return Pointer to the changed object.
hMultipleValue* MultipleValueEditor_ui::result(void)
{
     return _mv->copyTo();
}
// -----------------------------------------------------------------------------
