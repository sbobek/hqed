/**
 * \file	PluginEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	10.07.2009
 * \version	1.0
 * \brief	This file contains class definition that arranges plugin editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef PLUGINEDITOR_H
#define PLUGINEDITOR_H
// -----------------------------------------------------------------------------

#define PLUGINDIALOGEDITORMODE_NEW 0
#define PLUGINDIALOGEDITORMODE_EDT 1
// -----------------------------------------------------------------------------

#include <QGridLayout>
 
#include "ui_PluginEditor.h"
// -----------------------------------------------------------------------------
/**
* \class 	PluginEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	10.07.2009
* \brief 	Class definition that arranges plugin editor window.
*/
class PluginEditor_ui : public QWidget, private Ui_PluginEditor
{
    Q_OBJECT
    
private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     QGridLayout* gp1Layout;	               ///< Pointer to groupBox1 grid layout.
     QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
     QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
     QGridLayout* gp4Layout;	               ///< Pointer to groupBox4 grid layout.
     QGridLayout* gp5Layout;	               ///< Pointer to groupBox5 grid layout.
     QGridLayout* sw1p0layout;	          ///< Pointer to stacketWidget->page0 grid layout.
     QGridLayout* sw1p1layout;	          ///< Pointer to stacketWidget->page1 grid layout.
     
     int _mode;                              ///< Indicates the dialog mode (0-defining new plugin, 1-editing plugin)
     int _plindex;                           ///< Index of the editing plugin

public:

     /// \brief Constructor PluginEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	PluginEditor_ui(QWidget *parent = 0);
     
     /// \brief Destructor PluginEditor_ui class.
	~PluginEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function returns the mode of the dialog
	///
	/// \return the mode of the dialog
     /// \li 0 - create new plugin
     /// \li 1 - edit plugin
     int mode(void);
     
     /// \brief Function returns the index of the edited plugin
	///
	/// \return the index of the edited plugin
     int plindex(void);
     
     /// \brief Function that allow for setting mode of the dialog
	///
     /// \param __mode - the mode of the dialog
     /// \param __plindex - index of the edited plugin
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(int __mode, int __plindex);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function that verifies the entered parameters
	///
	/// \return true on success otherwise false
     bool verifyParams(void);
     
     /// \brief Function that returns the entered plugin name
	///
	/// \return the entered plugin name as string
     QString plname(void);
     
     /// \brief Function that returns the entered plugin address
	///
	/// \return the entered plugin address as string
     QString pladdress(void);
     
     /// \brief Function that returns the entered plugin process params
	///
	/// \return the entered plugin address as string
     QString plparams(void);
     
     /// \brief Function that returns the entered plugin connection timeout
	///
	/// \return the entered plugin connection timeout
     int pltimeout(void);
     
     /// \brief Function that returns the entered plugin connection port
	///
	/// \return the entered plugin connection port
     int plportnumber(void);
     
     /// \brief Function that returns the plugin configuration
	///
	/// \return the plugin configuration
     int plconfiguration(void);

public slots:

     /// \brief Function is triggered when ok key is pressed.
	///
	/// \return No return value.
     void okClick(void);
     
     /// \brief Function is triggered when cancel key is pressed.
	///
	/// \return No return value.
     void cancelClick(void);
     
     /// \brief Function is triggered when select file key is pressed.
	///
	/// \return No return value.
     void selectFileClick(void);

     /// \brief Function is triggered when modules selection has been changed
     ///
     /// \param __state - the new state of the selected item
     /// \return No return value.
     void modulesSelectionChanged(int __state);
     
signals:

     /// \brief This signal is emited when window is closed with the help of OK button
	///
	/// \return No return value.
     void onOkClose(void);
};
// -----------------------------------------------------------------------------

extern PluginEditor_ui* PluginEditor;
// -----------------------------------------------------------------------------

#endif
