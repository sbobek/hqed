/*

 *	   $Id: AttributeEditor_ui.cpp,v 1.39.8.9 2011-07-04 18:32:20 hqedclb Exp $

 *

 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>

 *

 *     Copyright (C) 2006-9 by the HeKatE Project

 *

 *     HQEd has been develped within the HeKatE Project,

 *     see http://hekate.ia.agh.edu.pl

 *

 *     This file is part of HQEd.

 *

 *     HQEd is free software: you can redistribute it and/or modify

 *     it under the terms of the GNU General Public License as published by

 *     the Free Software Foundation, either version 3 of the License, or

 *     (at your option) any later version.

 *

 *     HQEd is distributed in the hope that it will be useful,

 *     but WITHOUT ANY WARRANTY; without even the implied warranty of

 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the

 *     GNU General Public License for more details.

 *

 *     You should have received a copy of the GNU General Public License

 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.

 *

 */



#include <QMessageBox>

#include <QtGui>

#include <QDialog>

#include <QObject>

#include <QKeyEvent>



#include "ValEdit_ui.h"

#include "AttributeEditor_ui.h"

#include "CallbackManager_ui.h"

#include "TypeManager_ui.h"

#include "SelectGroup_ui.h"

#include "../Settings_ui.h"

#include "../MainWin_ui.h"



#include "../../namespaces/ns_hqed.h"

#include "../../namespaces/ns_xtt.h"



#include "../../M/hType.h"

#include "../../M/hTypeList.h"

#include "../../M/hMultipleValue.h"

#include "../../M/XTT/XTT_TableList.h"

#include "../../M/XTT/XTT_Attribute.h"

#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../M/XTT/XTT_CallbackContainer.h"

// -----------------------------------------------------------------------------



AttrEditor* AttributeEditor;

// -----------------------------------------------------------------------------



// #brief Constructor AttrEditor class.

//

// #param parent Pointer to parent object.

AttrEditor::AttrEditor(QWidget*)

{

    initForm();

}

// -----------------------------------------------------------------------------



// #brief Constructor AttrEditor class.

//

// #param attrID Index of edited attribute.

// #param New True when new attribute is added, false when attribute is edited.

// #param parent Pointer to parent object.

AttrEditor::AttrEditor(QString attrID, bool New, QWidget*)

{

    initForm();

    

    int GroupIndex = -1;

    int AttrIndex = -1;

    for(unsigned int g=0;g<AttributeGroups->Count();++g)

    {

        int aindex = AttributeGroups->Group(g)->IndexOfID(attrID, fAttrAbstraction);

        if(aindex > -1)

        {

            GroupIndex = g;

            AttrIndex = aindex;

            break;

        }

    }



    if((GroupIndex == -1) || (AttrIndex == -1))

    {

        QMessageBox::critical(NULL, "HQEd", "Cannot find attribute id \'" + attrID + "\'", QMessageBox::Ok);

        return;

    }



    // Zapisanie parametrow edycji

    fNew = New;

    fAttrGroupIndex = GroupIndex;

    fAttrIndex = AttrIndex;



    pushButton_3->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    pushButton_4->setText(AttributeGroups->CreatePath(fAttrGroupIndex));



    readTypesNames();

    ReadAttributesNames();

    comboBox->setCurrentIndex(AttrIndex);

    SelectedAttributeChanged(fAttrGroupIndex, fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Constructor AttrEditor class.

//

// #param New True when new attribute is added, false when attribute is edited.

// #param GroupIndex Index of group from which attribute is edited.

// #param AttrIndex Index of edited attribute.

// #param parent Pointer to parent object.

AttrEditor::AttrEditor(bool New, int GroupIndex, int AttrIndex, QWidget*)

{

    initForm();



    // Zapisanie parametrow edycji

    fNew = New;

    fAttrGroupIndex = GroupIndex;

    fAttrIndex = AttrIndex;

    //AttributeGroups->Group(fAttrGroupIndex)->indexTranslate(AttrIndex, XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL, fAttrAbstraction);



    pushButton_3->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    pushButton_4->setText(AttributeGroups->CreatePath(fAttrGroupIndex));



    readTypesNames();

    ReadAttributesNames();

    comboBox->setCurrentIndex(AttrIndex);

    SelectedAttributeChanged(fAttrGroupIndex, fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Destructor AttrEditor class.

AttrEditor::~AttrEditor(void)

{

    delete gb1Layout;

    delete gb2Layout;

    delete gb3Layout;

    delete gb4Layout;

    delete gb5Layout;

    delete gb6Layout;

    delete formLayout;

}

// -----------------------------------------------------------------------------



// #brief Function that initializes the initial values of the fileds of class

//

// #return No return value.

void AttrEditor::initForm(void)

{

    fNew = true;

    fAttrGroupIndex = -1;

    fAttrIndex = -1;

    fSelectedType = NULL;

    fAttrAbstraction = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL;

    if((MainWin == NULL) || (MainWin->currentMode() == XTT_MODE))

        fAttrAbstraction = XTT_ATTRIBUTE_TYPE_PHYSICAL;



    setupUi(this);

    setWindowIcon(QIcon(":/all_images/images/mainico.png"));

    hqed::centerWindow(this);

    setWindowFlags(Qt::WindowMaximizeButtonHint);



    // signals/slots mechanism in action

    connect(pushButton, SIGNAL(clicked()), this, SLOT(OKClick()));

    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(SrcGroupSelect()));

    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(DstGroupSelect()));

    connect(pushButton_5, SIGNAL(clicked()), this, SLOT(TypeSelect()));

    connect(pushButton_6, SIGNAL(clicked()), this, SLOT(applyClick()));

    connect(radioButton_7, SIGNAL(clicked()), this, SLOT(typeChange()));

    connect(radioButton_8, SIGNAL(clicked()), this, SLOT(typeChange()));

    connect(radioButton_9, SIGNAL(clicked()), this, SLOT(typeChange()));

    connect(radioButton, SIGNAL(clicked()), this, SLOT(typeChange()));

    connect(clbButton, SIGNAL(clicked()),this, SLOT(clbManager()));

    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));

    connect(comboBox, SIGNAL(editTextChanged(QString)), this, SLOT(comboBoxEditTextChange(QString)));

    connect(comboBox_2, SIGNAL(highlighted(int)), this, SLOT(OnTypeHighlight(int)));

    connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(OnTypeSelect(int)));

    connect(clbCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(clbComboChaned(QString)));



    radioButton_2->setText(XTT_Attribute::_class_attribute_.at(0));

    radioButton_3->setText(XTT_Attribute::_class_attribute_.at(1));



    layoutControls();

}

// -----------------------------------------------------------------------------



// #brief Function that layouts the controls on the form

//

// #return No return value.

void AttrEditor::layoutControls(void)
{
    //resize(5,5);

    formLayout = new QGridLayout;
    formLayout->addWidget(groupBox_5,   0, 0, 1, 4);
    formLayout->addWidget(groupBox,     1, 0, 1, 4);
    formLayout->addWidget(groupBox_6,   2, 0, 1, 4);
    formLayout->addWidget(groupBox_4,   3, 0, 1, 4);
    formLayout->addWidget(groupBox_2,   4, 0, 1, 4);
    formLayout->addWidget(groupBox_3,   5, 0, 1, 4);
    formLayout->addWidget(pushButton,   6, 1);
    formLayout->addWidget(pushButton_2, 6, 2);
    formLayout->addWidget(pushButton_6, 6, 3);
    formLayout->setRowStretch(3, 1);
    formLayout->setColumnStretch(0, 1);
    hqed::setupLayout(formLayout);
    setLayout(formLayout);

    gb5Layout = new QGridLayout;
    gb5Layout->addWidget(label_6,      0, 0);
    gb5Layout->addWidget(label_7,      0, 1);
    gb5Layout->addWidget(pushButton_3, 1, 0);
    gb5Layout->addWidget(pushButton_4, 1, 1);
    hqed::setupLayout(gb5Layout);
    groupBox_5->setLayout(gb5Layout);

    gb1Layout = new QGridLayout;
    gb1Layout->addWidget(label,      0, 0);
    gb1Layout->addWidget(label_2,    0, 2);
    gb1Layout->addWidget(comboBox,   1, 0, 1, 2);
    gb1Layout->addWidget(lineEdit,   1, 2, 1, 2);
    gb1Layout->addWidget(label_3,    2, 0);
    gb1Layout->addWidget(lineEdit_2, 3, 0, 1, 4);
    gb1Layout->setColumnStretch(1, 1);
    //gb1Layout->setColumnStretch(3, 1);
    hqed::setupLayout(gb1Layout);
    groupBox->setLayout(gb1Layout);

    gb6Layout = new QGridLayout;
    gb6Layout->addWidget(radioButton_7, 0, 1);
    gb6Layout->addWidget(radioButton_8, 0, 2);
    gb6Layout->addWidget(radioButton_9, 0, 3);
    gb6Layout->addWidget(radioButton,   0, 4);
    gb6Layout->setColumnStretch(0, 1);
    gb6Layout->setColumnStretch(5, 1);
    hqed::setupLayout(gb6Layout);
    groupBox_6->setLayout(gb6Layout);

    gb4Layout = new QGridLayout;
    gb4Layout->addWidget(label_9,      0, 0);
    gb4Layout->addWidget(clbCombo,     0, 1);
    gb4Layout->addWidget(clbButton,    0, 2);
    gb4Layout->addWidget(label_10,     1, 0);
    gb4Layout->addWidget(clbLabel,     2, 0, 1, 3);
    gb4Layout->setColumnStretch(1, 1);
    gb4Layout->setRowStretch(2, 1);
    hqed::setupLayout(gb4Layout);
    groupBox_4->setLayout(gb4Layout);

    gb2Layout = new QGridLayout;
    gb2Layout->addWidget(label_8,      0, 0);
    gb2Layout->addWidget(comboBox_2,   0, 1);
    gb2Layout->addWidget(pushButton_5, 0, 2);
    gb2Layout->addWidget(label_4,      1, 0);
    gb2Layout->addWidget(label_5,      2, 0, 1, 3);
    gb2Layout->setColumnStretch(1, 1);
    gb2Layout->setRowStretch(2, 1);
    hqed::setupLayout(gb2Layout);
    groupBox_2->setLayout(gb2Layout);

    gb3Layout = new QGridLayout;
    gb3Layout->addWidget(radioButton_2, 0, 1);
    gb3Layout->addWidget(radioButton_3, 0, 2);
    gb3Layout->setColumnStretch(0, 1);
    gb3Layout->setColumnStretch(3, 1);
    hqed::setupLayout(gb3Layout);
    groupBox_3->setLayout(gb3Layout);

    LoadCombo();
}
// -----------------------------------------------------------------------------


// #brief Function is triggered when selected index of combo box is changed.

//

// #param Index Selected index of combo box.

// #return No return value.

void AttrEditor::ComboBoxChange(int Index)

{

    fAttrIndex = Index;

    SelectedAttributeChanged(fAttrGroupIndex, fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when text in combo box is changed; attribute name creates acronym name automaticaly.

//

// #param _newText New text value.

// #return No return value.

void AttrEditor::comboBoxEditTextChange(QString _newText)

{

    // Jezeli nie mamy tworzyc automatycznej nazwy atrybutu

    if(!Settings->xttCreateAutoAcronym())

        return;



    // Jezeli string pusty to nic nie wstawiamy nic nie zmieniamy

    if(_newText == "")

        return;



    QString newAcronymBaseName = _newText;



    QString dgname = pushButton_4->text().split(".", QString::SkipEmptyParts).last();         // Nowa grupa

    int dgindex = AttributeGroups->IndexOfName(dgname);                                       // Index nowej grupy

    int sgai = fAttrIndex;                    										// Index atrybutu w starej grupie



    lineEdit->setText(AttributeGroups->createAcronym(_newText, fAttrGroupIndex, dgindex, sgai));

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when the type select button is pressed

//

// #return No return value.

void AttrEditor::TypeSelect(void)

{

    delete TypeManager;

    TypeManager = new TypeManager_ui;

    connect(TypeManager, SIGNAL(onSelect()), this, SLOT(OnTypeSelect()));

    connect(TypeManager, SIGNAL(onClose()), this, SLOT(onTypeManagerClose()));

    connect(TypeManager, SIGNAL(onDelete()), this, SLOT(selectAttributeCurrentType()));

    TypeManager->show();

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when the type manager has closed

//

// #return No return value.

void AttrEditor::onTypeManagerClose(void)

{    

    readTypesNames();

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when argument type changed

//

// #return No return value.

void AttrEditor::OnTypeSelect(void)

{

    int result = TypeManager->result() + hpTypesInfo.ptc;

    OnTypeSelect(result);



    if(result < 0)

        result = 0;

    if(comboBox_2->count() <= result)

        result = comboBox_2->count()-1;



    comboBox_2->setCurrentIndex(result);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when argument type changed

//

// #param _index - the  index of the type on the list of types

// #return No return value.

void AttrEditor::OnTypeSelect(int _index)

{

    if(_index == -1)

        return;



    int index = _index;

    fSelectedType = typeList->at(index);



    // Info o typie

    if(fSelectedType != NULL)

        label_5->setText(fSelectedType->typeInfo(true));

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when the combobox item is highlighted

//

// #param __typeIndex - the index of the type

// #return No return value.

void AttrEditor::OnTypeHighlight(int __typeIndex)

{

    if((__typeIndex < 0 ) || (__typeIndex >= typeList->size()))

        return;



    QString text = typeList->at(__typeIndex)->typeInfo(true);

    QToolTip::showText(QCursor::pos(), text);

}

// -----------------------------------------------------------------------------



// #brief Function reads attributs from the group.

//

// #return No return value.

void AttrEditor::ReadAttributesNames(void)

{

    if(fNew)

        return;



    comboBox->clear();

    comboBox->addItems(AttributeGroups->Group(fAttrGroupIndex)->AttributeNameList(fAttrAbstraction));

    comboBox->setCurrentIndex(fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Function reads the names of types

//

// #return No return value.

void AttrEditor::readTypesNames(void)

{

    int cb2ii = comboBox_2->currentIndex();



    comboBox_2->clear();

    comboBox_2->addItems(typeList->names(true));



    int dii = cb2ii;

    if(dii < 0)

        dii = 0;

    if(comboBox_2->count() <= dii)

        dii = comboBox_2->count()-1;



    comboBox_2->setCurrentIndex(dii);

}

// -----------------------------------------------------------------------------



// #brief Function sets window according to parameters.

//

// #param GroupIndex Index of group.

// #param AttrIndex Index of attribute.

// #return No return value.

void AttrEditor::SelectedAttributeChanged(int GroupIndex, int AttrIndex)

{

    pushButton_3->setEnabled(!fNew);

    label_6->setEnabled(!fNew);



    // Sprawdzenie poprawnosci indexow

    if((GroupIndex == -1) || (AttrIndex == -1))

        return;



    // Ustawianie okienka zgodnie z parametrami atrybutu

    XTT_Attribute* attr = AttributeGroups->Group(GroupIndex)->Item(AttrIndex, fAttrAbstraction);

    lineEdit->setText(attr->Acronym());

    lineEdit_2->setText(attr->Description());

    if(attr->Class() == XTT_ATT_CLASS_RO)

        radioButton_7->setChecked(true);

    if(attr->Class() == XTT_ATT_CLASS_RW)

        radioButton_8->setChecked(true);

    if(attr->Class() == XTT_ATT_CLASS_WO)

        radioButton_9->setChecked(true);

    if(attr->Class() == XTT_ATT_CLASS_ST)

        radioButton->setChecked(true);



    if(attr->multiplicity() == false)

        radioButton_2->setChecked(true);

    if(attr->multiplicity() == true)

        radioButton_3->setChecked(true);



    selectAttributeCurrentType();

    LoadCombo();

    selectAttributeCurrentCallback();

}

// -----------------------------------------------------------------------------



// #brief Function that sets the combobox filed according to the current type of the attribute

//

// #return No return value.

void AttrEditor::selectAttributeCurrentType(void)

{

    // Info o typie

    fSelectedType = AttributeGroups->Group(fAttrGroupIndex)->Item(fAttrIndex, fAttrAbstraction)->Type();

    int ioid = typeList->indexOfID(fSelectedType->id());

    comboBox_2->setCurrentIndex(ioid);

}

// -----------------------------------------------------------------------------



// #brief Function that sets the combobox filed according to the current Callback of the attribute

//

// #return No return value.

void AttrEditor::selectAttributeCurrentCallback()

{

    fSelectedClb = AttributeGroups->Group(fAttrGroupIndex)->Item(fAttrIndex, fAttrAbstraction)->CallbackId();



    int id = clbCombo->findText(fSelectedClb);

    clbCombo->setCurrentIndex(id);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when selected source group button is clicked.

//

// #return No return value.

void AttrEditor::SrcGroupSelect(void)

{

    delete SelGroup;

    SelGroup = new SelectGroup_ui;

    connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(OnSrcGroupChange()));

    SelGroup->show();

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when source group is changed.

//

// #return No return value.

void AttrEditor::OnSrcGroupChange(void)

{

    int pindex = AttributeGroups->IndexOfName(SelGroup->Result());

    if(pindex == -1)

        return;



    fAttrGroupIndex = pindex;

    pushButton_3->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    pushButton_4->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    ReadAttributesNames();

    SelectedAttributeChanged(fAttrGroupIndex, fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when selected destiny group button is clicked.

//

// #return No return value.

void AttrEditor::DstGroupSelect(void)

{

    delete SelGroup;

    SelGroup = new SelectGroup_ui;

    connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(OnDstGroupChange()));

    SelGroup->show();

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when destiny group is changed.

//

// #return No return value.

void AttrEditor::OnDstGroupChange(void)

{

    int pindex = AttributeGroups->IndexOfName(SelGroup->Result());

    if(pindex == -1)

        return;



    pushButton_4->setText(AttributeGroups->CreatePath(pindex));

    comboBoxEditTextChange(comboBox->currentText());

}

// -----------------------------------------------------------------------------



// #brief Function that saves the changes

//

// #return true if everything is ok otherwise false

bool AttrEditor::applyChanges(void)

{

    // WALIDACJA DANYCH

    // Index edytowanego atrybutu

    int curr_index = comboBox->currentIndex();



    // Kiedy zaden atrybut nie zostal wybrany

    if((curr_index == -1) && (!fNew))

    {

        const QString content = "There is no attrbiute selected.";

        QMessageBox::warning(NULL, "HQEd", content, QMessageBox::Ok);

        return false;

    }



    // Wczytanie nowej nazwy i usuniecie spacji

    QString atrrNewName = comboBox->currentText();                                            // Nowa nazwa atrybutu

    for(;atrrNewName.contains(" ");)

        atrrNewName = atrrNewName.remove(atrrNewName.indexOf(" "), 1);



    if((MainWin->currentMode() == XTT_MODE) && (atrrNewName.length() > 0) && (atrrNewName.data()[0].isUpper()))

    {

        const QString content = "You try to define conceptual attribute which name starts with capital letter.\nConceptual attributes are not allowed in XTT mode, plese change the name of the attribute.";

        QMessageBox::warning(NULL, "HQEd", content, QMessageBox::Ok);

        return false;

    }



    // Wczytanie nowego akronimu i usuniecie spacji

    QString atrrNewAcronym = lineEdit->text();                                                // Nowy akronim atrybutu

    for(;atrrNewAcronym.contains(" ");)

        atrrNewAcronym = atrrNewAcronym.remove(atrrNewAcronym.indexOf(" "), 1);



    // Sprawdzanie czy podana nazwa juz nie istnieje

    QString sgname = pushButton_3->text().split(".", QString::SkipEmptyParts).last();         // Stara grupa

    QString dgname = pushButton_4->text().split(".", QString::SkipEmptyParts).last();         // Nowa grupa

    int sgindex = AttributeGroups->IndexOfName(sgname);                                       // Index starej grupy

    int dgindex = AttributeGroups->IndexOfName(dgname);                                       // Index nowej grupy



    if((sgindex == -1) || (dgindex == -1))

    {

        const QString content = "Incorrect source/destination group.";

        QMessageBox::critical(NULL, "HQEd", content, QMessageBox::Ok);

        return false;

    }

    int sgai = fAttrIndex;                                                                    // Index atrybutu w starej grupie

    int dgai = AttributeGroups->Group(dgindex)->IndexOfName(atrrNewName, fAttrAbstraction);                     // Index atrybutu w nowej grupie

    int dgaci = AttributeGroups->Group(dgindex)->IndexOfAcronym(atrrNewName, fAttrAbstraction);                 // Sprawdzanie czy nie istnieje taki akronim



    if(((fNew) && ((dgai > -1) || (dgaci > -1))) ||

            ((!fNew) && (dgai > -1) && (sgindex == dgindex) && (sgai != dgai)) ||

            ((!fNew) && (dgai > -1) && (sgindex != dgindex)) ||

            ((!fNew) && (dgaci > -1) && (sgindex == dgindex) && (sgai != dgaci)) ||

            ((!fNew) && (dgaci > -1) && (sgindex != dgindex)))

    {

        const QString content = "The name/acronym like new name is already exists in this group.\nPlease give another name.";

        QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);

        return false;

    }



    // Sprawdzanie czy podany akronim juz nie istnieje

    dgai = AttributeGroups->Group(dgindex)->IndexOfAcronym(atrrNewAcronym, fAttrAbstraction);                   // Index atrybutu w nowej grupie

    dgaci = AttributeGroups->Group(dgindex)->IndexOfName(atrrNewAcronym, fAttrAbstraction);                     // Sprawdzanie czy nie istnieje taka nazwa jak akronim



    if(((fNew) && ((dgai > -1) || (dgaci > -1))) ||

            ((!fNew) && (dgai > -1) && (sgindex == dgindex) && (sgai != dgai)) ||

            ((!fNew) && (dgai > -1) && (sgindex != dgindex)) ||

            ((!fNew) && (dgaci > -1) && (sgindex == dgindex) && (sgai != dgaci)) ||

            ((!fNew) && (dgaci > -1) && (sgindex != dgindex)))

    {

        const QString content = "The name/acronym like acronym is already exists in this group.\nPlease give another acronym.";

        QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);

        return false;

    }



    // Sprawdzanie czy podano wszystkie wymagane dane

    // Czy podano nazwe

    if((atrrNewName == "") ||

            (atrrNewName.toLower() == XTT_Attribute::_not_definded_operator_) ||

            (atrrNewName.toLower() == XTT_Attribute::_any_operator_))

    {

        QMessageBox::information(NULL, "HQEd", "Icorrect name.", QMessageBox::Ok);

        return false;

    }

    // Czy podano akronim

    if((atrrNewAcronym == "") ||

            (atrrNewAcronym.toLower() == XTT_Attribute::_not_definded_operator_) ||

            (atrrNewAcronym.toLower() == XTT_Attribute::_any_operator_))

    {

        QMessageBox::information(NULL, "HQEd", "Incorrect acronym.", QMessageBox::Ok);

        return false;

    }



    // Ustalanie adresu atrybutu

    XTT_Attribute* attr;



    if(!fNew)

    {

        // Zapisanie nowego indexu

        fAttrIndex = comboBox->currentIndex();



        // Kiedy index jest niepoprawny to tworzymy nowy argument

        if((fAttrIndex < 0) || (fAttrIndex >= (int)AttributeGroups->Group(fAttrGroupIndex)->Count()))

            fNew = true;



        // Jezeli wszystko ok to pobieramy adres z pojemnika

        if(!fNew)

            attr = AttributeGroups->Group(fAttrGroupIndex)->Item(fAttrIndex, fAttrAbstraction);

    }



    // Kiedy tworzymy nowy atrybut

    if(fNew)

        attr = new XTT_Attribute(AttributeGroups->Group(fAttrGroupIndex)->NextId());



    attr->setName(atrrNewName);                                                // Ustawienie nazwy atrybutu

    attr->setAcronym(atrrNewAcronym);                                          // Ustawienie akronimu atrybutu

    attr->setDescription(lineEdit_2->text());                                  // Ustawienie opisu atrybutu

    attr->setType(fSelectedType);                                              // Ustawienie typu atrybutu

    attr->setCallbackId(fSelectedClb);                                         // Ustawianie id Callbacku

    if(radioButton_7->isChecked())

        attr->setClass(XTT_ATT_CLASS_RO);

    if(radioButton_8->isChecked())

        attr->setClass(XTT_ATT_CLASS_RW);

    if(radioButton_9->isChecked())

        attr->setClass(XTT_ATT_CLASS_WO);

    if(radioButton->isChecked())

        attr->setClass(XTT_ATT_CLASS_ST);



    // Ustawienie wartosci domyslnej, jezeli sie nie uda to nie zamykamy jeszcze okienka

    //if(!attr->setDefaultValue(""))

    //     return;

    attr->setMultiplicity(radioButton_3->isChecked());



    // Gdy nastepuje zmiana grupy to trzeba przeniesc atrybut

    if((!fNew) && (sgindex != dgindex))

    {

        // Dodawanie do nowej grupy

        AttributeGroups->Group(dgindex)->Add(attr);



        // Usuwanie ze starej grupy

        int gIndex = AttributeGroups->Group(sgindex)->indexTranslate(sgai, fAttrAbstraction, XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL);

        AttributeGroups->Group(sgindex)->Delete(gIndex, false);                 // Usuwanie atrybutu

    }

    // Dodanie atrybutu tylko gdy jest on nowy

    if(fNew)

        AttributeGroups->Group(dgindex)->Add(attr);



    // Sortowanie atrybutow

    AttributeGroups->Group(dgindex)->sort();



    // Odswiezenie widoku tabel

    QString errs = "";

    if(!hqed::sendEventMessage(&errs, XTT_SIGNAL_ATTRIBUTE_CHANGED, attr))

        hqed::showMessages(errs);

    xtt::fullVerification();

    xtt::logicalFullVerification();

    TableList->RefreshAll();



    // Emisja sygnalu

    emit ClickOK();



    fNew = false;

    fAttrIndex = AttributeGroups->Group(dgindex)->IndexOfID(attr->Id(), fAttrAbstraction);

    fAttrGroupIndex = dgindex;



    return true;

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when object OK is clicked.

//

// #return No return value.

void AttrEditor::OKClick(void)

{

    // Zamkniecie okienka jezeli wszystko ok

    if(applyChanges())

        close();

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when button apply is clicked.

//

// #return No return value.

void AttrEditor::applyClick(void)

{

    if(!applyChanges())

        return;



    pushButton_3->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    pushButton_4->setText(AttributeGroups->CreatePath(fAttrGroupIndex));

    int ai = fAttrIndex;

    readTypesNames();

    ReadAttributesNames();

    comboBox->setCurrentIndex(ai);

    SelectedAttributeChanged(fAttrGroupIndex, fAttrIndex);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when key is pressed.

//

// #param event Pointer to event object.

// #return No return value.

void AttrEditor::keyPressEvent(QKeyEvent *event)

{

    switch (event->key())

    {

    case Qt::Key_Escape:

        close();

        break;

    case Qt::Key_Return:

        OKClick();

        break;

    default:

        QWidget::keyPressEvent(event);

    }

}

// -----------------------------------------------------------------------------



// #brief Funtion is triggered when Callback Manager button is clicked

//

// #return No return valude

void AttrEditor::clbManager()

{

    delete ClbManager;

    ClbManager = new CallbackManager(fSelectedClb);

    connect(ClbManager, SIGNAL(okClick(QString)), this, SLOT(ClbManagerOk(QString)));

    connect(ClbManager,SIGNAL(cancelClicked()), this, SLOT(ClbManagerCancel()));

    ClbManager->show();

}

// -----------------------------------------------------------------------------



// #brief Load Callbacks Ids from container to ComboBox.

//

// #return No return value.

void AttrEditor::LoadCombo()

{

    int size;

    size = CallbackContainer->size();

    clbCombo->clear();

    clbCombo->addItem("");

    for(int i = 0; i<size; i++)

    {

        if(radioButton_7->isChecked() && (CallbackContainer->get(i)->getType() == "in"))

            clbCombo->addItem(CallbackContainer->get(i)->getID());

        if(radioButton_9->isChecked() && (CallbackContainer->get(i)->getType() == "out"))

            clbCombo->addItem(CallbackContainer->get(i)->getID());

        if(radioButton_8->isChecked() && (CallbackContainer->get(i)->getType() == "inout"))

            clbCombo->addItem(CallbackContainer->get(i)->getID());

        if(radioButton->isChecked())

            clbCombo->addItem(CallbackContainer->get(i)->getID());

    }

}

// -----------------------------------------------------------------------------



// #brief Function is triggered when selected index of combo box is changed.

//

// #param Content of combo - callback ID

// #return No return valude

void AttrEditor::clbComboChaned(QString clbID)

{

    if(clbID == "")

    {

        clbLabel->hide();

    }

    else

    {

        clbLabel->setText(CallbackContainer->get(CallbackContainer->getByID(clbID))->callbackInfo(true));

        clbLabel->show();

    }

    fSelectedClb = clbID;

}

// -----------------------------------------------------------------------------



// #brief Function is triggered after close the Callback Manager window by clicling OK button.

//

// #param Selected Callback Id in Callback Manager

// #return No return value

void AttrEditor::ClbManagerOk(QString id)

{

    LoadCombo();

    int index = clbCombo->findText(id);

    clbCombo->setCurrentIndex(index);

}

// -----------------------------------------------------------------------------



// #brief Function is triggered after close the Callback Manager window by clicking Cancel button.

//

// #return No return value

void AttrEditor::ClbManagerCancel()

{

    QString Clb = clbCombo->currentText();

    LoadCombo();

    int id = clbCombo->findText(Clb);

    clbCombo->setCurrentIndex(id);

}

// -----------------------------------------------------------------------------



// #brief Funtion is triggered when any Radio Button i Type Group chane value

//

// #return No return value

void AttrEditor::typeChange()

{

    LoadCombo();

}

