 /**
 * \file      ValuePresentationDialog_ui.h
 * \author    Krzysztof Kaczor kinio4444@gmail.com
 * \date      23.10.2009
 * \version   1.0
 * \brief     This file contains class definition arranges ValuePresentation window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef VALUEPRESENTATIONDIALOG_H
#define VALUEPRESENTATIONDIALOG_H
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_ValuePresentationDialog.h"
// -----------------------------------------------------------------------------

class hType;
// -----------------------------------------------------------------------------

/**
* \class      ValuePresentationDialog_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       23.10.2009
* \brief      This class arranges ValuePresentation window
*/
class ValuePresentationDialog_ui : public QWidget, public Ui_ValuePresentationDialog
{
    Q_OBJECT

private:

     hType* _type;                           ///< Pointer to the edited type

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     QGridLayout* gb1Layout;	               ///< Pointer to groupBox grid layout.
     QGridLayout* gb2Layout;	               ///< Pointer to groupBox_2 grid layout.
     QGridLayout* sw0p0Layout;	          ///< Pointer to stacket widget page 0 grid layout
     QGridLayout* sw0p1Layout;	          ///< Pointer to stacket widget page 1 grid layout
     QGridLayout* sw0p2Layout;	          ///< Pointer to stacket widget page 2 grid layout
     
     /// \brief Function that lists the script files in the ecmascript directory
     ///
     /// \return No return value.
     void listScripts(void);
     
     /// \brief Function that lists the pligins that supports the presentation module of the protocol
     ///
     /// \return No return value.
     void listPlugins(void);
     
     /// \brief Function that lists the presentation pligins services names
     ///
     /// \return No return value.
     void listPluginsServices(void);
     
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public:

     /// \brief Constructor ValuePresentationDialog_ui class.
     ///
     /// \param *parent - Pointer to parent.
     ValuePresentationDialog_ui(QWidget *parent = 0);

     /// \brief Destructore ValuePresentationDialog_ui class.
     ~ValuePresentationDialog_ui(void);

     /// \brief Function that sets the initial values of the window class
     ///
     /// \return no values returns
     void initWindow(void);
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
     /// \brief Function that sets the mode of the dialog
     ///
     /// \param __type - pointer to the edited type
     /// \return window mode result type
     int setMode(hType* __type);
     
     /// \brief Function that updates the GUI according to window mode
     ///
     /// \return no values return
     void updateDialog(void);
     
public slots:

     /// \brief Function that is triggered when user changes the source of presentation service
     ///
     /// \param __index - index of the new source
     /// \return No return value.
     void sourceIndexChange(int __index);
     
     /// \brief Function that is triggered when user changes the plugin selection
     ///
     /// \param __plindex - index of the selected plugin
     /// \return No return value.
     void onPluginSelectionChanged(int __plindex);
     
     /// \brief Function that refreshes list of the scripts
     ///
     /// \return No return value.
     void refreshScriptList(void);
     
     /// \brief Function that refreshes list of the plugins
     ///
     /// \return No return value.
     void refreshPluginList(void);
     
     /// \brief Function that refreshes list of the plugins services
     ///
     /// \return No return value.
     void forceRefreshPluginServices(void);
     
     /// \brief Function that removes selected script file
     ///
     /// \return No return value.
     void deleteScript(void);
     
     /// \brief Function that is triggered when ok button is pressed
     ///
     /// \return No return value.
     void okClick(void);
     
     /// \brief Function that is triggered when close button is pressed
     ///
     /// \return No return value.
     void closeClick(void);
     
     /// \brief Function that is triggered when create new script button is pressed
     ///
     /// \return No return value.
     void createNewScript(void);
     
     /// \brief Function that is triggered when edit script button is pressed
     ///
     /// \return No return value.
     void editScript(void);
};
// -----------------------------------------------------------------------------

extern ValuePresentationDialog_ui* ValuePresentationDialog;
// -----------------------------------------------------------------------------

#endif
