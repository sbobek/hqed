/*
 *	   $Id: SelectGroup_ui.cpp,v 1.32 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "SelectGroup_ui.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
// -----------------------------------------------------------------------------

SelectGroup_ui* SelGroup;
// -----------------------------------------------------------------------------

// #brief Constructor SelectGroup_ui class.
//
// #param parent Pointer to parent object.
SelectGroup_ui::SelectGroup_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 

    connect(pushButton, SIGNAL(clicked()), this, SLOT(OkClick()));
    connect(treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(TreeClick(QTreeWidgetItem*, int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);

    treeWidget->insertTopLevelItems(0, AttributeGroups->CreateTree());
    res_name = "";
}
// -----------------------------------------------------------------------------

// #brief Function is object tree is clicked.
//
// #param item Pointer to tree object.
// #param col Number of clicked column.
// #return No return value.
void SelectGroup_ui::TreeClick(QTreeWidgetItem* item, int col)
{
     // Pobranie nazwy gkliknietej grupy
     res_name = item->text(col);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Ok' is pressed.
//
// #return No return value.
void SelectGroup_ui::OkClick(void)
{
     // Emisja syganlu OK
     emit ResultOK();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void SelectGroup_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               OkClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
