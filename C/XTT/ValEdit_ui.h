/**
 * \file	ValEdit_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.15
 * \brief	This file contains class definition that arranges list edit window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef VALUEEDIT_H
#define VALUEEDIT_H
// -----------------------------------------------------------------------------
 
#include "ui_EditList.h"
// -----------------------------------------------------------------------------

#include <QStringList>
#include <QString>
// -----------------------------------------------------------------------------
/**
* \class 	ValueEdit_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007
* \brief 	Class definition that arranges list edit window.
*/
class ValueEdit_ui : public QWidget, private Ui_EditList
{
    Q_OBJECT

public:

    /// \brief Constructor ValueEdit_ui class.
	///
	/// \param parent Pointer to parent object.
	ValueEdit_ui(QWidget *parent = 0);

    /// \brief Constructor ValueEdit_ui class.
	///
	/// \param line String of input values.
	/// \param parent Pointer to parent object.
	ValueEdit_ui(QString line, QWidget *parent = 0);

	QStringList values;	///< List of values.

 	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);

public slots:

 	/// \brief Function adds new value.
	///
	/// \return No return value.
	void AddVal(void);

 	/// \brief Function deletes value.
	///
	/// \return No return value.
	void DelVal(void);

 	/// \brief Function replies selected value.
	///
	/// \return No return value.
	void RplVal(void);

 	/// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void Accept(void);

signals:

 	/// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void ClickOK(void);
};
// -----------------------------------------------------------------------------

extern ValueEdit_ui* ValEdit;
// -----------------------------------------------------------------------------

#endif
