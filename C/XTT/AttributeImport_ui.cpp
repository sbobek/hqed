/**
 * \file	AttributeImport_ui.cpp
 * \author	Marcin Koziej m-key88@o2.pl
 * \date 	05.07.2011
 * \version	1.0
 * \brief	This file contains class definition that can arrange dialog 'AttributeImport'.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include "AttributeImport_ui.h"
#include "ui_AttributeImport.h"
#include "../../namespaces/ns_hqed.h"
// -----------------------------------------------------------------------------

// #brief Constructor AttributeImport class.
   //
   // #param parent Pointer to parent object.
   // #param cT List of confict types.
   // #param cA List of conflict attributes.
AttributeImport::AttributeImport(QWidget *parent, QList<QDomElement> cT, QList<QDomElement> cA) :
    QDialog(parent),
    ui(new Ui::AttributeImport),
    conTypes(cT),
    conAttrs(cA),
    selTypes(conTypes),
    unselTypes(conTypes),
    selAttrs(conAttrs),
    unselAttrs(conAttrs)
{
    ui->setupUi(this);
    for (int i = 0; i < conTypes.size(); ++i)
              ui->listWidget->addItem(conTypes.at(i).attribute("name", "") + " (" + conTypes.at(i).attribute("id", "") + ")");
    for (int i = 0; i < conAttrs.size(); ++i)
              ui->listWidget->addItem(conAttrs.at(i).attribute("name", "") + " (" + conAttrs.at(i).attribute("id", "") + ")");
    connect(ui->OKButton, SIGNAL(clicked()), this, SLOT(confirm()));
    layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor AttributeImport class.
AttributeImport::~AttributeImport()
{
    delete ui;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void AttributeImport::layoutForm(void)
{
    formLayout = new QGridLayout;
    formLayout->addWidget(ui->listWidget, 0, 0, 1, 4);
    formLayout->addWidget(ui->label, 1, 0);
    formLayout->addWidget(ui->selected, 1, 1, 1, 3);
    formLayout->addWidget(ui->label_2, 2, 0);
    formLayout->addWidget(ui->others, 2, 1, 1, 3);
    formLayout->addWidget(ui->OKButton, 3, 1);
    formLayout->addWidget(ui->CancelButton, 3, 2);
    formLayout->setSpacing(5);
    formLayout->setMargin(10);
    hqed::setupLayout(formLayout);
    setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Slot processed after pressing OK button.
//
// #return No return value.
void AttributeImport::confirm()
{
    QList<QListWidgetItem *> temp = ui->listWidget->selectedItems();
    for(int i = conTypes.size() - 1; i >= 0; --i)
    {
        bool found = false;
        for(int j = 0; j < temp.size(); ++j)
        {
            QRegExp rxlen("\\((.+)\\)");
            rxlen.indexIn((*temp.at(j)).text());
            if(conTypes.at(i).attribute("id", "") == rxlen.cap(1))
                found = true;
        }
        if(found == false)
            this->selTypes.removeAt(i);
        else
            this->unselTypes.removeAt(i);
    }

    for(int i = conAttrs.size() - 1; i >= 0; --i)
    {
        bool found = false;
        for(int j = 0; j < temp.size(); ++j)
        {
            QRegExp rxlen("\\((.+)\\)");
            rxlen.indexIn((*temp.at(j)).text());
            if(conAttrs.at(i).attribute("id", "") == rxlen.cap(1))
                found = true;
        }
        if(found == false)
            this->selAttrs.removeAt(i);
        else
            this->unselAttrs.removeAt(i);
    }

    this->selChoice = ui->selected->currentText();
    this->othChoice = ui->others->currentText();
    this->accept();
}
// -----------------------------------------------------------------------------

// #brief Function returns selected types.
   //
   // #return List of selected types.
QList<QDomElement> AttributeImport::getSTypes(void)
{
    return this->selTypes;
}
// -----------------------------------------------------------------------------

// #brief Function returns unselected types.
   //
   // #return List of unselected types.
QList<QDomElement> AttributeImport::getUTypes(void)
{
    return this->unselTypes;
}
// -----------------------------------------------------------------------------

// #brief Function returns selected attributes.
   //
   // #return List of selected attributes.
QList<QDomElement> AttributeImport::getSAttrs(void)
{
    return this->selAttrs;
}
// -----------------------------------------------------------------------------

// #brief Function returns unselected attributes.
   //
   // #return List of unselected attributes.
QList<QDomElement> AttributeImport::getUAttrs(void)
{
    return this->unselAttrs;
}
// -----------------------------------------------------------------------------

// #brief Function returns choice for selected items.
   //
   // #return Combobox choice QString.
QString AttributeImport::getSelChoice(void)
{
    return this->selChoice;
}
// -----------------------------------------------------------------------------

// #brief Function returns choice for unselected items.
   //
   // #return Combobox choice QString.
QString AttributeImport::getOthChoice(void)
{
    return this->othChoice;
}

