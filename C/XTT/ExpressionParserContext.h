#ifndef EXPRESSIONPARSERCONTEXT_H
#define EXPRESSIONPARSERCONTEXT_H

class hType;
class XTT_Cell;

class ExpressionParserContext
{
public:
    ExpressionParserContext();
    ExpressionParserContext(XTT_Cell *cell);

    void contextForCell(XTT_Cell *cell);
    void decisionCellContext(hType *elementType);
    void actionCellContext(hType *elementType);
    void stateCellContext(hType *elementType);

    XTT_Cell *cell()
    {
        return m_Cell;
    }

    hType *requiredTopLevelType()
    {
        return m_RequiredTopLevelType;
    }

    hType *requiredElementType()
    {
        return m_RequiredElementType;
    }

    bool functionsAllowed()
    {
        return m_FunctionsAllowed;
    }

    bool attributesAllowed()
    {
        return m_AttributesAllowed;
    }

    bool minMaxAllowed()
    {
        return m_MinMaxAllowed;
    }

    bool anyAllowed()
    {
        return m_AnyAllowed;
    }

    bool nullAllowed()
    {
        return m_NullAllowed;
    }


private:
    void createDefaultContext();

    hType *m_RequiredTopLevelType;
    hType *m_RequiredElementType; // or QList<hType>?

    XTT_Cell *m_Cell;

    bool m_FunctionsAllowed;
    bool m_AttributesAllowed;
    bool m_MinMaxAllowed;
    bool m_AnyAllowed;
    bool m_NullAllowed;
};

#endif // EXPRESSIONPARSERCONTEXT_H
