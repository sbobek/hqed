#include "CellExpressionTextEditor.h"

#include "QMessageBox"
#include "QKeyEvent"
#include "QToolTip"
#include "QCompleter"
#include "QStandardItemModel"
#include "QGraphicsView"
#include "QScrollBar"
#include "QGraphicsItem"
#include "QFontMetrics"
#include "QTimer"

#include "M/hFunctionList.h"
#include "M/hType.h"
#include "M/hTypeList.h"
#include "M/hFunction.h"
#include "M/hSet.h"
#include "M/hSetItem.h"
#include "M/hExpression.h"
#include "M/hFunctionArgument.h"
#include "M/hValue.h"
#include "M/hMultipleValue.h"
#include "M/XTT/XTT_Cell.h"
#include "M/XTT/XTT_AttributeGroups.h"
#include "C/Settings_ui.h"

#include "namespaces/ns_xtt.h"

#include "ExpressionParser.h"
#include "ParenthesisHighlighter.h"
#include "ExpressionSyntaxHighlighter.h"
#include "ExpressionParserContext.h"
#include "CompletionModelBuilder.h"


CellExpressionTextEditor::CellExpressionTextEditor(XTT_Cell *cell, QWidget *parent)
    : QTextEdit(parent)
{
    m_EditedCell = cell;
    m_EditedExpression = cell->Content()->expressionField()->copyTo();
    m_ParserContext = ExpressionParserContext(cell);

    initialize();
    initializeCompleter();
}

CellExpressionTextEditor::CellExpressionTextEditor(const ExpressionParserContext &context, QWidget *parent)
    : QTextEdit(parent)
{
    m_ParserContext = context;
}

CellExpressionTextEditor::~CellExpressionTextEditor()
{}

QGraphicsItem* CellExpressionTextEditor::getGraphicsItem(unsigned int &xOffset, unsigned int &yOffset)
{
    xOffset = m_GraphicsItemXOffset;
    yOffset = m_GraphicsItemYOffset;

    return m_GraphicsItem;
}

void CellExpressionTextEditor::setGraphicsItem(QGraphicsItem *item, unsigned int xOffset, unsigned int yOffset)
{
    m_GraphicsItem = item;
    m_GraphicsItemXOffset = xOffset;
    m_GraphicsItemYOffset = yOffset;
}

bool CellExpressionTextEditor::parseStringToExpression()
{
    // Prepend expression with the attribute name
    QString expressionString = m_EditedCell->AttrType()->Name() + toPlainText();

    ExpressionParser parser(m_ParserContext);
    hExpression *resultingExpression = parser.parseExpressionString(expressionString);
    if(!resultingExpression) {
        // error encountered
        QMessageBox::critical(0, tr("Error in the expression"), parser.getError().toString());
        setFocus();
        return false;
    }

    resultingExpression->function()->argument(0)->setVisible(false);
    if(m_EditedCell)
    {
        m_EditedCell->Content()->setExpression(resultingExpression);
        xtt::fullVerification();
        xtt::logicalTableVerification(m_EditedCell->TableParent());
    }

    setFocus();
    return true;
}

void CellExpressionTextEditor::adjustEditorSize()
{
    QFontMetrics fontMetrics(font());
    QRect textBoundingRect = fontMetrics.boundingRect(toPlainText());
    resize(QSize(textBoundingRect.width() + 25, textBoundingRect.height()));
}

void CellExpressionTextEditor::keyPressEvent(QKeyEvent *e)
{
    if(m_Completer->popup()->isVisible()) {
        // Ignore following keys if completer popup is visible
        switch(e->key())
        {
        case Qt::Key_Enter:
        case Qt::Key_Return:
        case Qt::Key_Escape:
        case Qt::Key_Tab:
        case Qt::Key_Backtab:
            e->ignore();
            return;
        default:
            break;
        }
    }

    if(e->key() == Qt::Key_Return || e->key() == Qt::Key_Enter) {
        m_EditorClosing = true;
        if(parseStringToExpression()) {
            emit editingFinished();
            return;
        } else {
            return;
        }
    } else if(e->key() == Qt::Key_Escape) {
        m_EditorClosing = true;
        emit editingCancelled();
        return;
    } else if(e->key() == Qt::Key_Tab || e->key() == Qt::Key_Backtab) {
        e->ignore();
        return;
    }

    bool ctrlOrShift = e->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
    if(ctrlOrShift && e->text().isEmpty()) {
        return;
    }

    if(((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_Space) ||
       ((e->modifiers() & Qt::ControlModifier) && e->key() == Qt::Key_E)) {
        displayCompleter();
        return;
    }

    QTextEdit::keyPressEvent(e);
    int tokenBegin, tokenEnd;
    if((getCurrentTokenBeginAndEnd(tokenBegin, tokenEnd).size() > 2) &&
        e->key() != Qt::Key_Left && e->key() != Qt::Key_Right) {
        // FIXME - this is a dirty walkaround for a bug described at:
        // https://bugreports.qt-project.org/browse/QTBUG-13658
        QTimer::singleShot(0, this, SLOT(displayCompleter()));
    }
}

void CellExpressionTextEditor::focusOutEvent(QFocusEvent *e)
{
    Q_UNUSED(e);

    if(m_EditorClosing) {
        return;
    }

    if(parseStringToExpression()) {
        emit editingFinished();
    } else {
        emit editingCancelled();
    }
}

void CellExpressionTextEditor::focusChanged(QWidget *old, QWidget *now)
{
    Q_UNUSED(old);
    Q_UNUSED(now);
}

void CellExpressionTextEditor::displayCompleter()
{
    int tokenBegin, tokenEnd;
    QString currentToken = getCurrentTokenBeginAndEnd(tokenBegin, tokenEnd);
    m_Completer->setCompletionPrefix(currentToken);
    m_Completer->popup()->setCurrentIndex(m_CompletionModel->index(0, 0));

    if(m_Completer->popup()->isVisible()) {
        return;
    }

    QRect cr = cursorRect();
    cr.setWidth(m_Completer->popup()->sizeHintForColumn(0) + m_Completer->popup()->verticalScrollBar()->width());
    m_Completer->complete(cr);

    QGraphicsView *view = m_GraphicsItem->scene()->views().first();
    QPoint globalViewPos = view->mapToGlobal(QPoint(0, 0));
    QPoint newPos = view->mapFromScene(m_GraphicsItem->scenePos().x() + m_GraphicsItemXOffset,
                                       m_GraphicsItem->scenePos().y() + m_GraphicsItemYOffset);
    QPoint finalPosition = QPoint(newPos.x() + globalViewPos.x() + cr.x(), newPos.y() + globalViewPos.y() + cr.height() + 8 + cr.y());
    m_Completer->popup()->move(finalPosition);
}

void CellExpressionTextEditor::completionHighlighted(const QString &completion)
{
    Q_UNUSED(completion);
}

void CellExpressionTextEditor::insertNameCompletion(const QString &completion)
{
    int tokenBegin, tokenEnd;
    QString currentToken = getCurrentTokenBeginAndEnd(tokenBegin, tokenEnd);
    replaceCurrentTokenWithCompletion(completion, tokenBegin, currentToken.length());
}

QString CellExpressionTextEditor::getCurrentTokenBeginAndEnd(int &tokenBegin, int &tokenEnd)
{
    int currentIndex = textCursor().position();
    QString expression = toPlainText();
    if(expression.isEmpty()) {
        tokenBegin = tokenEnd = 0;
        return QString();
    }

    if(currentIndex > 0) {
        tokenBegin = currentIndex - 1;
    } else {
        tokenBegin = 0;
    }

    while(tokenBegin > 0) {
        if(!isAllowedTokenCharacter(expression.at(tokenBegin))) {
            tokenBegin++;
            break;
        }
        tokenBegin--;
    }


    tokenEnd = currentIndex;
    while(tokenEnd < expression.size()) {
        if(!isAllowedTokenCharacter(expression.at(tokenEnd))) {
            tokenEnd--;
            break;
        }
        tokenEnd++;
    }

    if(tokenBegin < 0 || tokenEnd < 0 || ((tokenEnd - tokenBegin) <= 0)) {
        return QString();
    }

    return expression.mid(tokenBegin, tokenEnd - tokenBegin + 1);
}

void CellExpressionTextEditor::replaceCurrentTokenWithCompletion(const QString &completion, const int begin, const int len)
{
    QTextCursor cursor(document());
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::MoveAnchor, begin);
    cursor.movePosition(QTextCursor::NextCharacter, QTextCursor::KeepAnchor, len);


    int completionEndIndex = completion.indexOf('/');
    if(completionEndIndex > 0) {
        cursor.insertText(completion.left(completionEndIndex));
    } else {
        cursor.insertText(completion);
    }
}

bool CellExpressionTextEditor::isAllowedTokenCharacter(const QChar character)
{
    if((character > 47 && character < 58) ||   // [0-9]
       (character > 64 && character < 91) ||   // [A-Z]
       (character > 96 && character < 123)||   // [a-z]
        character == 95) {                     // _
        return true;
    } else {
        return false;
    }
}

void CellExpressionTextEditor::initialize()
{
    m_ParenthesisHighlighter = new ParenthesisHighlighter(this);
    connect(this, SIGNAL(cursorPositionChanged()), m_ParenthesisHighlighter, SLOT(highlightMatchingParenthesis()));
    connect(this, SIGNAL(textChanged()), this, SLOT(adjustEditorSize()));

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    setFont(Settings->xttTableFont());
    setPlainText(m_EditedExpression->toString());

    QTextCursor cursor = textCursor();
    cursor.movePosition(QTextCursor::End);
    setTextCursor(cursor);

    m_EditorClosing = false;

    initializeSyntaxHighlighter();
}

void CellExpressionTextEditor::initializeSyntaxHighlighter()
{
    m_SyntaxHighlighter = new ExpressionSyntaxHighlighter(document());

    m_SyntaxHighlighter->setFunctionNames(getFunctionNames());
    m_SyntaxHighlighter->setAttributeNames(getAttributeNames());
    m_SyntaxHighlighter->setSpecialNames(getSpecialValueNames());
    m_SyntaxHighlighter->setSymbolicNames(getSymbolicValueNames());

    m_SyntaxHighlighter->refreshRules();
}

void CellExpressionTextEditor::initializeCompleter()
{
    m_Completer = new QCompleter(this);
    m_Completer->setCompletionMode(QCompleter::PopupCompletion);
    m_Completer->setCaseSensitivity(Qt::CaseSensitive);
    m_Completer->setWidget(this);

    m_Completer->popup()->setFont(Settings->xttTableFont());

    CompletionModelBuilder completionBuilder;

    completionBuilder.setSpecialValues(getSpecialValueNamesForCompletions());

    completionBuilder.setFunctionNames(getFunctionNamesForCompletions());

    if(m_ParserContext.attributesAllowed()) {
        completionBuilder.setAttributeNames(getAttributeNames());
    }

    completionBuilder.setSymbolicValues(*typeList);

    m_CompletionModel = new QStandardItemModel(this);
    completionBuilder.buildModel(m_CompletionModel);
    m_Completer->setModel(m_CompletionModel);

    connect(m_Completer, SIGNAL(highlighted(QString)), this, SLOT(completionHighlighted(QString)));
    connect(m_Completer, SIGNAL(activated(QString)), this, SLOT(insertNameCompletion(QString)));
}

QStringList CellExpressionTextEditor::getFunctionNames()
{
    QStringList names;
    for(int i = 0; i < funcList->size(); i++) {
        QString functionName = funcList->at(i)->userRepresentation();
            names.append(functionName);
    }

    return names;
}

QStringList CellExpressionTextEditor::getAttributeNames()
{
    QStringList names;
    for(unsigned int i = 0; i < AttributeGroups->Count(); i++) {
        for(unsigned int j = 0; j < AttributeGroups->Group(i)->Count(); j++) {
            names.append(AttributeGroups->Group(i)->AttributeNameList());
        }
    }
    names.removeDuplicates();

    return names;
}

QStringList CellExpressionTextEditor::getSpecialValueNames()
{
    QStringList names;

    names.append("null");
    names.append("any");
    names.append("min");
    names.append("max");

    return names;
}

QStringList CellExpressionTextEditor::getSymbolicValueNames()
{
    return QStringList();
}

QStringList CellExpressionTextEditor::getFunctionNamesForCompletions()
{
    QStringList names;
    for(int i = 0; i < funcList->size(); i++) {
        if((m_ParserContext.requiredTopLevelType()->baseType() == BOOLEAN_BASED)
                && (funcList->at(i)->returnedTypes() & FUNCTION_TYPE__BOOLEAN)) {
            names.append(funcList->at(i)->userRepresentation());
        }
    }
    return names;
}

QStringList CellExpressionTextEditor::getSpecialValueNamesForCompletions()
{
    QStringList names;

    if(m_ParserContext.nullAllowed()) {
        names.append("null");
    }

    if(m_ParserContext.anyAllowed()) {
        names.append("any");
    }

    if(m_ParserContext.minMaxAllowed()) {
        names.append("min");
        names.append("max");
    }

    return names;
}


