 /**
 * \file      ValuePresentationScriptEditor_ui.h
 * \author    Krzysztof Kaczor kinio4444@gmail.com
 * \date      08.01.2010
 * \version   1.0
 * \brief     This file contains class definition arranges ValuePresentationScriptEditor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef VALUEPRESENTATIONSCRIPTEDITOR_UI
#define VALUEPRESENTATIONSCRIPTEDITOR_UI
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_ValuePresentationScriptEditor.h"
// -----------------------------------------------------------------------------

class TextEditor_ui;
// -----------------------------------------------------------------------------

/**
* \class      ValuePresentationScriptEditor_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       08.01.2010
* \brief      This class arranges ValuePresentationScriptEditor window
*/
class ValuePresentationScriptEditor_ui : public QWidget, public Ui_ValuePresentationScriptEditor
{
    Q_OBJECT

private:

     QGridLayout* formLayout;	               ///< Pointer to grid layout form.
     QGridLayout* gb1Layout;	               ///< Pointer to groupBox grid layout.
     QGridLayout* gb2Layout;	               ///< Pointer to groupBox_2 grid layout.
     TextEditor_ui* editor;                  ///< Pointer to the texteditor
     bool _new;                              ///< Indicates if the edited file is a new file
     QString _filename;                      ///< The name of the file
     
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent* event);
     
     /// \brief Function is triggered when window is closed.
	///
	/// \param event - Pointer to close event object.
	/// \return No return value.
     void closeEvent(QCloseEvent* event);

public:

     /// \brief Constructor ValuePresentationDialog_ui class.
     ///
     /// \param *parent - Pointer to parent.
     ValuePresentationScriptEditor_ui(QWidget *parent = 0);

     /// \brief Destructore ValuePresentationDialog_ui class.
     ~ValuePresentationScriptEditor_ui(void);

     /// \brief Function that sets the initial values of the window class
     ///
     /// \return no values returns
     void initWindow(void);
     
     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
     /// \brief Function that sets the mode of the dialog
     ///
     /// \param __createnew - indicates if the edited script is a new script
     /// \param __filename - defines the file name of the script
     /// \return window mode result type
     int setMode(bool __createnew, QString __filename);
     
     /// \brief Function that updates the GUI according to window mode
     ///
     /// \return no values return
     void updateDialog(void);
     
public slots:
     
     /// \brief Function that is triggered when test button is pressed
     ///
     /// \return No return value.
     void testClick(void);
     
     /// \brief Function that is triggered when the editor window is closed
     ///
     /// \return No return value.
     void onEditorClose(void);
     
signals:

     /// \brief Signal that is emited when edior is closed
     ///
     /// \return no values return
     void onclose(void);
};
// -----------------------------------------------------------------------------

extern ValuePresentationScriptEditor_ui* ValuePresentationScriptEditor;
// -----------------------------------------------------------------------------

#endif
