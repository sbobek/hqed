/*
 *	   $Id: CallbackManager_ui.cpp,v 1.1.2.11 2011-07-05 06:49:28 kkr Exp $
 *
 *     Implementation by �ukasz Finster, Sebastian Witowski
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QComboBox>
#include <QXmlStreamWriter>
#include <QGridLayout>

#include "CallbackManager_ui.h"
#include "ui_CallbackManager.h"
#include "../../namespaces/ns_hqed.h"
// -----------------------------------------------------------------------------

CallbackManager* ClbManager;
// -----------------------------------------------------------------------------

// #brief Callback Manager constructor
CallbackManager::CallbackManager(QString clbId, QWidget *parent) :
     QWidget(parent),
    ui(new Ui::CallbackManager)
{
     ui->setupUi(this);
     highlighter = new HSyntaxHighlighter(ui->textEdit);
     highlighter->loadConfig(Settings->configurationPath() + "/" + "htepl.ini");
     QButtonGroup *type = new QButtonGroup(this);
     QButtonGroup *attr = new QButtonGroup(this);
     type->addButton(ui->inoutRadio);
     type->addButton(ui->inRadio);
     type->addButton(ui->outRadio);
     attr->addButton(ui->simpleRadio);
     attr->addButton(ui->generalRadio);
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     layoutForm();
     MODE = "NORMAL";
     libFile = CallbackContainer->getLibFile();
     loadCombo();
     int index = ui->idCombo->findText(clbId);
     ui->idCombo->setCurrentIndex(index);
     ChangeContent(clbId);
     this->setWindowTitle("Callback Manager");
     connect(ui->idCombo, SIGNAL(currentIndexChanged(QString)), this, SLOT(ComboChanged(QString)));
     connect(ui->applyButton, SIGNAL(clicked()), this, SLOT(ApplyClicked()));
     connect(ui->okButton, SIGNAL(clicked()), this, SLOT(okClicked()));
     connect(ui->newButton, SIGNAL(clicked()), this, SLOT(NewClicked()));
     connect(ui->cancelButton, SIGNAL(clicked()), this, SLOT(cancelClick()));
     connect(ui->deleteButton, SIGNAL(clicked()), this, SLOT(deleteClick()));
     connect(ui->editButton, SIGNAL(clicked()), this, SLOT(editClick()));
}
// -----------------------------------------------------------------------------

// #brief Callback manager destructor
CallbackManager::~CallbackManager()
{
     delete ui;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void CallbackManager::layoutForm()
{
    formLayout = new QGridLayout;
    QGridLayout *gb1Layout = new QGridLayout;
    gb1Layout->addWidget(ui->inRadio,       0,  0);
    gb1Layout->addWidget(ui->outRadio,      0,  1);
    gb1Layout->addWidget(ui->inoutRadio,    0,  2);

    QGridLayout *gb2Layout = new QGridLayout;
    gb2Layout->addWidget(ui->simpleRadio,   0,  0);
    gb2Layout->addWidget(ui->generalRadio,  0,  1);

    formLayout->addWidget(ui->label,        0,  0);
    formLayout->addWidget(ui->idCombo,      0,  1);
    formLayout->addWidget(ui->okButton,     0,  2);
    formLayout->addWidget(ui->applyButton,  0,  3);
    formLayout->addWidget(ui->cancelButton, 0,  4);
    formLayout->addWidget(ui->label_2,      1,  0);
    formLayout->addWidget(ui->nameEdit,     1,  1);
    formLayout->addWidget(ui->newButton,    1,  2);
    formLayout->addWidget(ui->editButton,   1,  3);
    formLayout->addWidget(ui->deleteButton, 1,  4);
    formLayout->addWidget(ui->label_3,      2,  0);
    formLayout->addLayout(gb1Layout,        2,  1);
    formLayout->addWidget(ui->checkBox,     2,  3);
    formLayout->addWidget(ui->label_4,      3,  0);
    formLayout->addLayout(gb2Layout,        3,  1);
    formLayout->addWidget(ui->label_5,      4,  0);
    formLayout->addWidget(ui->textEdit,     5,  0,  6,  5);

    hqed::setupLayout(formLayout);
    setLayout(formLayout);
}

// -----------------------------------------------------------------------------

// #brief Function loads data and adds callbacks ids to combo box
//
// #return No return value
void CallbackManager::loadCombo()
{
     ui->idCombo->clear();
     int size;
     size = CallbackContainer->size();
     for(int i = 0; i<size; i++)
          ui->idCombo->addItem(CallbackContainer->get(i)->getID());
     ui->idCombo->setEditable(false);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when text in combo box is changed;
//
// #param New callback id
// #return No return value
void CallbackManager::ComboChanged(QString id)
{
     ChangeContent(id);
}
// -----------------------------------------------------------------------------

// #brief Function loads callback data and show in the window
//
// #param new callback id
// #return No return value
void CallbackManager::ChangeContent(QString id)
{
     if(MODE != "NEW" && id != "" && id != " ")
     {
          int no = CallbackContainer->getByID(id);
          ui->nameEdit->setText(CallbackContainer->get(no)->getName());
          ui->nameEdit->setReadOnly(true);
          ui->textEdit->setPlainText(CallbackContainer->get(no)->getDef());
          ui->textEdit->setReadOnly(true);
          ui->checkBox->setChecked(CallbackContainer->get(no)->getSave());

          ui->inRadio->setChecked(CallbackContainer->get(no)->getType() == "in");
          ui->inRadio->setEnabled(false);
          ui->inoutRadio->setChecked(CallbackContainer->get(no)->getType() == "inout");
          ui->inoutRadio->setEnabled(false);
          ui->outRadio->setChecked(CallbackContainer->get(no)->getType() == "out");
          ui->outRadio->setEnabled(false);

          ui->generalRadio->setChecked(CallbackContainer->get(no)->getAtt() == "general");
          ui->generalRadio->setEnabled(false);
          ui->simpleRadio->setChecked(CallbackContainer->get(no)->getAtt() == "simple");
          ui->simpleRadio->setEnabled(false);

          ui->checkBox->setEnabled(false);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered after clock on New button, it let to edit data of new callback
//
// #return No return value
void CallbackManager::NewCallback()
{
     MODE = "NEW";
     this->setWindowTitle("New Callback...");

     ui->inoutRadio->setEnabled(true);
     ui->inRadio->setEnabled(true);
     ui->outRadio->setEnabled(true);
     ui->simpleRadio->setEnabled(true);
     ui->generalRadio->setEnabled(true);

     ui->idCombo->clear();
     ui->idCombo->addItem(CallbackContainer->generateID());
     ui->nameEdit->setReadOnly(false);
     ui->nameEdit->clear();
     ui->textEdit->setReadOnly(false);
     ui->textEdit->clear();
     ui->checkBox->setEnabled(true);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered after click on Aplly button
//
// #return No return value
void CallbackManager::ApplyClicked()
{
     ApplyChanges();
}
// -----------------------------------------------------------------------------

// #brief Function saves changes to callback container
//
// #return No return value
void CallbackManager::ApplyChanges()
{
     if(ui->idCombo->currentText().size() == 0 || ui->nameEdit->text().size() == 0 || ui->textEdit->toPlainText().size() == 0)
     {
          QMessageBox::about(this, tr("Error!"),tr("Wypelnij wszystkie pola"));
          return;
     }

     if(MODE == "NEW" && !(CallbackContainer->exist(ui->idCombo->currentText())))
   {
        Save();
        return;
   }

     if(MODE == "NEW" && CallbackContainer->exist(ui->idCombo->currentText()))
     {
          QMessageBox::StandardButton ret;
          ret = QMessageBox::warning(this, tr("Callback Manager"),
                                           tr("Callback with this ID already exists.\n"
                                               "Do you want to overwrite?"),
                                           QMessageBox::Save | QMessageBox::Discard | QMessageBox::Cancel);
          if (ret == QMessageBox::Save)
          {
               Save();
               return;
          }

          if(ret == QMessageBox::Discard)
          {
             loadCombo();
               return;
          }

          if(ret == QMessageBox::Cancel)
          {
               return;
          }
     }

     if(MODE == "EDIT")
     {
          Save();
     }
     this->setWindowTitle("Callback Manager");
}
// -----------------------------------------------------------------------------

// #brief Function is Saves changes of callback data to callback container
//
// #return No return value
void CallbackManager::Save()
{
     if(CallbackContainer->exist(ui->idCombo->currentText()))
     {
          int id = CallbackContainer->getByID(ui->idCombo->currentText());
          CallbackContainer->del(id);

          XTT_Callback *newclb;
          newclb = new XTT_Callback;
          newclb->setID(ui->idCombo->currentText());
          newclb->setName(ui->nameEdit->text());

          if(ui->inoutRadio->isChecked())
               newclb->setType("inout");
          if(ui->inRadio->isChecked())
               newclb->setType("in");
          if(ui->outRadio->isChecked())
               newclb->setType("out");

          newclb->setAtt((ui->generalRadio->isChecked())?"general":"simple");
          newclb->setDef(ui->textEdit->toPlainText());
          newclb->setSave(ui->checkBox->isChecked());
          CallbackContainer->add(*newclb);

          changed = true;
          MODE = "NORMAL";
          QString cId = ui->idCombo->currentText();
          loadCombo();
          int index = ui->idCombo->findText(cId);
          ui->idCombo->setCurrentIndex(index);
          return;
     }
     else
     {
          XTT_Callback *newclb;
          newclb = new XTT_Callback;
          newclb->setID(ui->idCombo->currentText());
          newclb->setName(ui->nameEdit->text());

          if(ui->inoutRadio->isChecked())
               newclb->setType("inout");
          if(ui->inRadio->isChecked())
               newclb->setType("in");
          if(ui->outRadio->isChecked())
               newclb->setType("out");

          newclb->setAtt((ui->generalRadio->isChecked())?"general":"simple");
          newclb->setDef(ui->textEdit->toPlainText());
          newclb->setSave(ui->checkBox->isChecked());
          CallbackContainer->add(*newclb);
          changed = true;
          MODE = "NORMAL";
          QString cId = ui->idCombo->currentText();
          loadCombo();
          int index = ui->idCombo->findText(cId);
          ui->idCombo->setCurrentIndex(index);
          return;
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when ok Button is clicked
//
// #return No return value
void CallbackManager::okClicked()
{
     if(MODE == "NORMAL")
     {
          if(changed == true) SaveToLibrary();
          this->close();
          emit okClick(ui->idCombo->currentText());
     }

     if(MODE == "EDIT" || MODE == "NEW")
     {
          ApplyChanges();
          if(changed == true) SaveToLibrary();
          emit okClick(ui->idCombo->currentText());
          this->close();
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when new button is clicked
//
// #return No return value
void CallbackManager::NewClicked()
{
     NewCallback();
}

// -----------------------------------------------------------------------------

// #brief Function is triggered when cancel button is clicked
//
// #return No return value
void CallbackManager::cancelClick()
{
     if(MODE == "NEW" || MODE == "EDIT")
     {
          MODE = "NORMAL";
          this->setWindowTitle("Callback Manager");
          loadCombo();
          return;
     }

     if(MODE == "NORMAL")
     {
          emit cancelClicked();
          if(changed == true) SaveToLibrary();
          this->close();
     }
}

// -----------------------------------------------------------------------------

// #brief Function is triggered when delete button clicked
//
// #return No return value
void CallbackManager::deleteClick()
{
     if(MODE == "NORMAL")
     {
          QString id = ui->idCombo->currentText();
          int index = CallbackContainer->getByID(id);
          QMessageBox::StandardButton pot;
          pot = QMessageBox::warning(this, tr("Callback Manager"),
                                           tr("Do you really want to delete callback?"),
                                           QMessageBox::Yes | QMessageBox::Cancel);
          if(pot == QMessageBox::Yes)
          {
               CallbackContainer->del(index);
               changed = true;
               loadCombo();
          }
          if(pot == QMessageBox::Cancel)

               return;
     }
}
// -----------------------------------------------------------------------------

// #brief Function saves callbacks to callback librery in xml format
//
// #return No return value
void CallbackManager::SaveToLibrary()
{
     QFile file(libFile);
     if (!file.open(QFile::WriteOnly | QFile::Text))
     {
          QMessageBox::warning(this, QObject::tr("HQEd"),
                                    QObject::tr("Can not open Callback Library file:\n%1.")
                                    .arg(file.errorString()));
     }
     QXmlStreamWriter xmlWriter(&file);
     CallbackContainer->allToXML(&xmlWriter);
     file.close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered afet edit button click, it let to edit selected callback data
//
// #return No return value
void CallbackManager::editClick()
{
     MODE = "EDIT";
     this->setWindowTitle("Editing Callback...");
     ui->idCombo->setEditable(false);
     ui->nameEdit->setReadOnly(false);

     ui->textEdit->setReadOnly(false);
     ui->inoutRadio->setEnabled(true);
     ui->inRadio->setEnabled(true);
     ui->outRadio->setEnabled(true);
     ui->simpleRadio->setEnabled(true);
     ui->generalRadio->setEnabled(true);
     ui->checkBox->setEnabled(true);
}
// -----------------------------------------------------------------------------
