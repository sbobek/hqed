/*
 *	   $Id: AttributeManager_ui.cpp,v 1.33 2010-01-08 19:47:03 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QTreeWidgetItem>

#include "../MainWin_ui.h"
#include "SelectGroup_ui.h"
#include "AttributeManager_ui.h"
#include "AttributeEditor_ui.h"
#include "SelectGroup_ui.h"

#include "widgets/AttributeTableWidgetItem.h"
#include "widgets/AttributeGroupsTreeWidget.h"
#include "widgets/AttributeManagerTableWidget.h"

#include "../../namespaces/ns_hqed.h"
         
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hFunction.h"
#include "../../M/hMultipleValue.h"
// -----------------------------------------------------------------------------

AttributeManager_ui* AttrManager;
// -----------------------------------------------------------------------------

// #brief Constructor AttributeManager_ui class.
//
// #param parent Pointer to parent object.
AttributeManager_ui::AttributeManager_ui(QWidget*)
{
    setupUi(this);
    
    tableWidget = new AttributeManagerTableWidget(groupBox);
    treeWidget = new AttributeGroupsTreeWidget(groupBox_2);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);

    connect(pushButton, SIGNAL(clicked()), this, SLOT(AddNewAttribute()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(EditAttribute()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(DeleteClick()));
    connect(pushButton_5, SIGNAL(clicked()), this, SLOT(NewGroupSelect()));
    connect(pushButton_6, SIGNAL(clicked()), this, SLOT(ReadAttributes()));
    connect(pushButton_7, SIGNAL(clicked()), this, SLOT(selectClick()));
    
    connect(tableWidget, SIGNAL(doubleClicked(void)), this, SLOT(EditAttribute(void)));
    connect(tableWidget, SIGNAL(rmbActionNewClicked()), this, SLOT(AddNewAttribute()));
    connect(tableWidget, SIGNAL(rmbActionEditClicked()), this, SLOT(EditAttribute()));
    connect(tableWidget, SIGNAL(rmbActionDeleteClicked()), this, SLOT(DeleteClick()));
    connect(tableWidget, SIGNAL(rmbActionSelectClicked()), this, SLOT(selectClick()));
    connect(tableWidget, SIGNAL(rmbActionChangeGroupClicked()), this, SLOT(NewGroupSelect()));
    connect(tableWidget, SIGNAL(rmbActionRefreshClicked()), this, SLOT(ReadAttributes()));
    
    connect(treeWidget, SIGNAL(itemClicked(QTreeWidgetItem*, int)), this, SLOT(TreeClick(QTreeWidgetItem*, int)));
    connect(treeWidget, SIGNAL(attributeMoved()), this, SLOT(ReadAttributes()));
    
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    treeWidget->insertTopLevelItems(0, AttributeGroups->CreateTree());

     _type = NULL;                           
     _setClass = -1;                         
     _sreq = false; 
     _currentAttribute = NULL;       
     _useFilter = false; 
     
    _selectedAttribute = NULL;
    fGroupIndex = 0;
    layoutForm();
    ReadAttributes();

    // signals/slots mechanism in action
    //connect(pushButton, SIGNAL(clicked()), this, SLOT(DeleteClick()));
    //connect(pushButton_3, SIGNAL(clicked()), this, SLOT(SelectGroupClick()));
}
// -----------------------------------------------------------------------------

// #brief Destructor AttributeManager_ui class.
AttributeManager_ui::~AttributeManager_ui(void)
{
     delete gb1Layout;
     delete gb2Layout;
     delete formLayout;
     delete treeWidget;
     delete tableWidget;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void AttributeManager_ui::layoutForm(void)
{
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox_2,   0, 0, 1, 4);
     formLayout->addWidget(groupBox,     0, 4, 1, 5);
     formLayout->addWidget(pushButton,   1, 0);
     formLayout->addWidget(pushButton_2, 1, 1);
     formLayout->addWidget(pushButton_3, 1, 2);
     formLayout->addWidget(pushButton_5, 1, 3, 1, 2);
     formLayout->addWidget(pushButton_7, 1, 6);
     formLayout->addWidget(pushButton_6, 1, 7);
     formLayout->addWidget(pushButton_4, 1, 8);
     formLayout->setRowStretch(0, 1);
     formLayout->setColumnStretch(5, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gb1Layout = new QGridLayout;
     gb1Layout->addWidget(tableWidget, 0, 0);
     hqed::setupLayout(gb1Layout);
     groupBox->setLayout(gb1Layout);
     
     gb2Layout = new QGridLayout;
     gb2Layout->addWidget(treeWidget, 0, 0);
     hqed::setupLayout(gb2Layout);
     groupBox_2->setLayout(gb2Layout);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the window mode
//
// #param *__type - pointer to the type of attribute
// #param __setClass - the class of defined set (0-set, 1-domain, 2-decision context, 3-conditional context)
// #param __singleRequired - determines if the multiplicity of argument value must be single (true) or multiple (false)
// #param *__currentAttribute - the pointer to the current attribute in the expression
// #param *__cell - the pointer to the edited cell.
// #param __useFilter - determines whether given filter must be used
// #return SET_WINDOW_MODE_xxx as a result
int AttributeManager_ui::setMode(hType* __type, int __setClass, bool __singleRequired, XTT_Attribute* __currentAttribute, XTT_Cell* __cell, bool __useFilter)
{
     // Checking for mode correctness
     if(__type == NULL)
          return SET_WINDOW_MODE_FAIL;
     if((__setClass != 0) && (__setClass != 1) && (__setClass != 2) && (__setClass != 3))
          return SET_WINDOW_MODE_FAIL;

     setWindowModality(Qt::ApplicationModal);
     tableWidget->disconnect(SIGNAL(doubleClicked()));
     connect(tableWidget, SIGNAL(doubleClicked()), this, SLOT(selectClick()));
     
     _type = __type;                           
     _setClass = __setClass;       // 0 - coditional context, 2 - action context                         
     _sreq = __singleRequired; 
     _cell = __cell;       
     _currentAttribute = __currentAttribute;       
     _useFilter = __useFilter; 

     // Searching for the attribute
     if(_currentAttribute != NULL)
     {
          XTT_AttributeList* dest_list = AttributeGroups->AttributeGroup(_currentAttribute);
          if(dest_list != NULL)
               fGroupIndex = (unsigned int)AttributeGroups->IndexOfID(dest_list->ID());
     }
     
     ReadAttributes();
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when you click the tree object.
//
// #param item Pointer to tree object.
// #param col Number of column that was clicked.
// #return No return value.
void AttributeManager_ui::TreeClick(QTreeWidgetItem* item, int col)
{
     // Zapisanie indexu kliknietej grupy
     fGroupIndex = AttributeGroups->IndexOfName(item->text(col));
     ReadAttributes();
}
// -----------------------------------------------------------------------------

// #brief Function reads information about attributes belnogs to the group.
//
// #return No return value.
void AttributeManager_ui::ReadAttributes(void)
{
     tableWidget->setRowCount(0);

     // Sprawdzanie czy index jest poprawny
     if((int)fGroupIndex == -1)
     {
          const QString content = "There is no group selected.";
          QMessageBox::information(NULL, "HQEd", content, QMessageBox::Ok);
          return;
     }

     // Wczytywanie informacji
     XTT_Table* cellParent = TableList->findCell(_cell);
     QList<XTT_Attribute*> *calist = NULL, 
                           *aalist = NULL;
     QList<XTT_Attribute*> parentAtts;
     
     if(cellParent != NULL)
     {
          calist = cellParent->ContextAtributes(1);
          aalist = cellParent->ContextAtributes(4);
          parentAtts = hqed::mergeListContents(*calist, *aalist);
          delete calist;
          delete aalist;
     }
     int row_selection = -1;
     for(unsigned int i=0;i<AttributeGroups->Group(fGroupIndex)->Count();i++)
     {
          if(_useFilter)
          {
               int type_characteristics = hFunction::typeFunctionRepresentation(_type);
               if(((hFunction::typeFunctionRepresentation(AttributeGroups->Group(fGroupIndex)->Item(i)->Type()) & type_characteristics) == 0) ||      // If incompatible types 
                   ((_sreq) && (_sreq == AttributeGroups->Group(fGroupIndex)->Item(i)->multiplicity())) ||                                            // If single is required and multiplicity is incopatible. This condition allow to select single attribute in case we want general.
                   ((cellParent != NULL) && (!parentAtts.contains(AttributeGroups->Group(fGroupIndex)->Item(i))))
                 )  
                         continue;
          }
          if((MainWin->currentMode() == XTT_MODE) && (AttributeGroups->Group(fGroupIndex)->Item(i)->isConceptual()))
               continue;
          
          if(AttributeGroups->Group(fGroupIndex)->Item(i) == _currentAttribute)
               row_selection = tableWidget->rowCount();
          
          int row_index = tableWidget->rowCount();
          tableWidget->setRowCount(tableWidget->rowCount()+1);
          
          QString type_name = "Type name: " + AttributeGroups->Group(fGroupIndex)->Item(i)->Type()->name();
          if(hpTypesInfo.ptn.indexOf(AttributeGroups->Group(fGroupIndex)->Item(i)->Type()->name()) == -1)
               type_name += "\nBase type: " + hpTypesInfo.ptn.at(hpTypesInfo.iopti(AttributeGroups->Group(fGroupIndex)->Item(i)->Type()->baseType()));

          QString val = "";
          hMultipleValueResult mvr = AttributeGroups->Group(fGroupIndex)->Item(i)->Value()->value();
          
          if(!(bool)mvr)
               val = (QString)mvr;
          if((bool)mvr)
               val = mvr.toSet(false)->toString(); //mvr.toSet()->toString() + " = " + mvr.toSet()->toVals();
               
          QString attID = AttributeGroups->Group(fGroupIndex)->Item(i)->Id();
          AttributeTableWidgetItem* _name = new AttributeTableWidgetItem(AttributeGroups->Group(fGroupIndex)->Item(i)->Name(), attID);
          AttributeTableWidgetItem* _aronym = new AttributeTableWidgetItem(AttributeGroups->Group(fGroupIndex)->Item(i)->Acronym(), attID);
          AttributeTableWidgetItem* _desc = new AttributeTableWidgetItem(AttributeGroups->Group(fGroupIndex)->Item(i)->Description(), attID);
          AttributeTableWidgetItem* _class = new AttributeTableWidgetItem(XTT_Attribute::classString(AttributeGroups->Group(fGroupIndex)->Item(i)->Class()), attID);
          AttributeTableWidgetItem* _type = new AttributeTableWidgetItem(type_name, attID);
          AttributeTableWidgetItem* _conrepr = new AttributeTableWidgetItem(AttributeGroups->Group(fGroupIndex)->Item(i)->ConstraintsRepresentation(), attID);
          AttributeTableWidgetItem* _val = new AttributeTableWidgetItem(val, attID);

          tableWidget->setItem(row_index, 0, _name);
          tableWidget->setItem(row_index, 1, _aronym);
          tableWidget->setItem(row_index, 2, _desc);
          tableWidget->setItem(row_index, 3, _class);
          tableWidget->setItem(row_index, 4, _type);
          tableWidget->setItem(row_index, 5, _conrepr);
          tableWidget->setItem(row_index, 6, _val);
     }
     
     if(row_selection > -1)
          tableWidget->setRangeSelected(QTableWidgetSelectionRange(row_selection, 0, row_selection, tableWidget->columnCount()-1), true);
}
// -----------------------------------------------------------------------------

// #brief Function shows edit attribute window.
//
// #return No return value.
void AttributeManager_ui::AddNewAttribute(void)
{
     delete AttributeEditor;
     AttributeEditor = new AttrEditor(true, fGroupIndex);

     connect(AttributeEditor, SIGNAL(ClickOK()), this, SLOT(ReadAttributes()));
     
     AttributeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function shows edit attribute window.
//
// #return No return va
void AttributeManager_ui::EditAttribute(void)
{
     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no row selected.", QMessageBox::Ok);
          return;
     }

     int attAbstraction = XTT_ATTRIBUTE_TYPE_CONCEPTUAL | XTT_ATTRIBUTE_TYPE_PHYSICAL;
     if(MainWin->currentMode() == XTT_MODE)
          attAbstraction = XTT_ATTRIBUTE_TYPE_PHYSICAL;
     
     QString name = tableWidget->item(tableWidget->currentRow(), 0)->text();
     int index = AttributeGroups->Group(fGroupIndex)->IndexOfName(name, attAbstraction);

     delete AttributeEditor;
     AttributeEditor = new AttrEditor(false, fGroupIndex, index);

     connect(AttributeEditor, SIGNAL(ClickOK()), this, SLOT(ReadAttributes()));

     AttributeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function deletes attribute that was chosen.
//
// #return No return value.
void AttributeManager_ui::DeleteClick(void)
{
     // Kiedy nie jest zaznaczony zaden wiersz
     if((int)fGroupIndex == -1)
          return;

     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no row selected.", QMessageBox::Ok);
          return;
     }

     QString name = tableWidget->item(tableWidget->currentRow(), 0)->text();
     int index = AttributeGroups->Group(fGroupIndex)->IndexOfName(name);

     // Gdy nie znaleziono
     if(index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Can't find selected attribute.", QMessageBox::Ok);
          return;
     }

     // Potwierdzenie usuniecia
     const QString content = "Do you realy want to delete attribute named " + name + "?";
     if(QMessageBox::warning(NULL, "HQEd", content, QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Inaczej kasujemy obiekt
     hqed::sendEventMessage(NULL, XTT_SIGNAL_ATTRIBUTE_REMOVED, AttributeGroups->Group(fGroupIndex)->Item(index));
     AttributeGroups->Group(fGroupIndex)->Delete(index);

     // Odswiezenie widoku
     ReadAttributes();

     // Odswiezenie widoku tabel
     TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function shows select group window.
//
// #return No return value.
void AttributeManager_ui::NewGroupSelect(void)
{
     // Kiedy nie jest zaznaczony zaden wiersz
     if((int)fGroupIndex == -1)
          return;

     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no row selected.", QMessageBox::Ok);
          return;
     }
     
     QString name = tableWidget->item(tableWidget->currentRow(), 0)->text();
     int fAttrIndex = AttributeGroups->Group(fGroupIndex)->IndexOfName(name);

     // Gdy nie znaleziono
     if(fAttrIndex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Can't find selected attribute.", QMessageBox::Ok);
          return;
     }
     
     delete SelGroup;
     SelGroup = new SelectGroup_ui;
     connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(NewGroupChange()));
     SelGroup->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when you change the group of attribute.
//
// #return No return value.
void AttributeManager_ui::NewGroupChange(void)
{
     QString name = tableWidget->item(tableWidget->currentRow(), 0)->text();
     int fAttrIndex = AttributeGroups->Group(fGroupIndex)->IndexOfName(name);
     int pindex = AttributeGroups->IndexOfName(SelGroup->Result());
     
     // Gdy nie znaleziono
     if(fAttrIndex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Can't find selected attribute.", QMessageBox::Ok);
          return;
     }
     if(pindex == -1)
          return;

     QString attid = AttributeGroups->Group(fGroupIndex)->Item(fAttrIndex)->Id();
     int moveresult = AttributeGroups->moveAttribute(attid, pindex);
     
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_NAME_OR_ACRONYM_EXISTS)
          QMessageBox::critical(NULL, "HQEd", "The name or acronym already exists in the destination group.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_ATTRIBUTE)
          QMessageBox::critical(NULL, "HQEd", "Attribute cannot be found in it's group.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_SOURCE_GROUP)
          QMessageBox::critical(NULL, "HQEd", "The parent group of attribute cannot be found.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_DESTINATION_GROUP)
          QMessageBox::critical(NULL, "HQEd", "Can't find destination group.", QMessageBox::Ok);

     // Odswiezenie widoku
     ReadAttributes();

     // Odswiezenie widoku tabel
     TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when the select button is clicked
//
// #return No return value.
void AttributeManager_ui::selectClick(void)
{
     // Kiedy nie jest zaznaczony zaden wiersz
     if((int)fGroupIndex == -1)
          return;

     // Wczytywanie indexu atrybutu
     if(tableWidget->currentRow() == -1)
     {
          QMessageBox::information(NULL, "HQEd", "There is no row selected.", QMessageBox::Ok);
          return;
     }

     QString name = tableWidget->item(tableWidget->currentRow(), 0)->text();
     int index = AttributeGroups->Group(fGroupIndex)->IndexOfName(name);

     // Gdy nie znaleziono
     if(index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Can't find selected attribute.", QMessageBox::Ok);
          return;
     }

     // Inaczej kasujemy obiekt
     _selectedAttribute = AttributeGroups->Group(fGroupIndex)->Item(index);
     
     emit onSelectClose();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void AttributeManager_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               EditAttribute();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that returns the attribute selection result
//
// #return the attribute selection result
XTT_Attribute* AttributeManager_ui::selectionResult(void)
{
     return _selectedAttribute;
}
// -----------------------------------------------------------------------------
