/*
 *	   $Id: AttributeDelete_ui.cpp,v 1.33 2010-01-08 19:47:03 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "SelectGroup_ui.h"
#include "AttributeDelete_ui.h"

#include "../../namespaces/ns_hqed.h"

#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
// -----------------------------------------------------------------------------

AttributeDelete_ui* AttributeDelete;
// -----------------------------------------------------------------------------

// #brief Constructor AttributeDelete_ui class.
//
// #param parent Pointer to parent object.
AttributeDelete_ui::AttributeDelete_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);

    // signals/slots mechanism in action
    connect(pushButton, SIGNAL(clicked()), this, SLOT(DeleteClick()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(SelectGroupClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(OnClose()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
}
// -----------------------------------------------------------------------------

// #brief Constructor AttributeDelete_ui class.
//
// #param GroupIndex Index of group from which attributes are to be deleted.
// #param parent Pointer to parent object.
AttributeDelete_ui::AttributeDelete_ui(int GroupIndex, QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    hqed::centerWindow(this);

    // signals/slots mechanism in action
    connect(pushButton, SIGNAL(clicked()), this, SLOT(DeleteClick()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(SelectGroupClick()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(OnClose()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);

    // Wczytywanie atrybutow
    if((GroupIndex >= 0) && (GroupIndex < (int)AttributeGroups->Count()))     
    {
          pushButton_3->setText(AttributeGroups->CreatePath(GroupIndex));
          fGroupIndex = (unsigned int)GroupIndex;
          ReadAttributes();
    }
}
// -----------------------------------------------------------------------------

// #brief Read attributes from the group.
//
// #return No return value.
void AttributeDelete_ui::ReadAttributes(void)
{
     listWidget->clear();
     listWidget->addItems(AttributeGroups->Group(fGroupIndex)->AttributeNameList());
}
// -----------------------------------------------------------------------------

// #brief Show window that enable selecting group.
//
// #return No return value.
void AttributeDelete_ui::SelectGroupClick(void)
{
     delete SelGroup;
     SelGroup = new SelectGroup_ui;
     connect(SelGroup, SIGNAL(ResultOK()), this, SLOT(OnSelectGroupChange()));
     SelGroup->show();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select group window is closed. Group is chosen at that time.
//
// #return No return value.
void AttributeDelete_ui::OnSelectGroupChange(void)
{
     int pindex = AttributeGroups->IndexOfName(SelGroup->Result());

     if(pindex == -1)
          return;

     fGroupIndex = pindex;
     pushButton_3->setText(AttributeGroups->CreatePath(fGroupIndex));
     ReadAttributes();
}
// -----------------------------------------------------------------------------

// #brief Delete attribute that was chosen.
//
// #return No return value.
void AttributeDelete_ui::DeleteClick(void)
{
     // Kiedy nie jest zaznaczony zaden wiersz
     if(listWidget->currentRow() == -1)
          return;

     // Pobranie nazwy atrybutu
     QString current = listWidget->currentItem()->text();

     // Potwierdzenie usuniecia
     const QString content = "Do you realy want to delete attribute named " + current + "?";
     if(QMessageBox::warning(NULL, "HQEd", content, QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Wyszukiwanie indexu wybranego obiektu w pojemniku
     int index = AttributeGroups->Group(fGroupIndex)->IndexOfName(current);

     // Gdy nie znaleziono
     if(index == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Can't find selected attribute.", QMessageBox::Ok);
          return;
     }

     // Inaczej kasujemy obiekt
     hqed::sendEventMessage(NULL, XTT_SIGNAL_ATTRIBUTE_REMOVED, AttributeGroups->Group(fGroupIndex)->Item(index));
     AttributeGroups->Group(fGroupIndex)->Delete(index);

     // Odswiezenie widoku
     ReadAttributes();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when attribute delete window is closed. Table view is refreshed at that time.
//
// #return No return value.
void AttributeDelete_ui::OnClose(void)
{
     TableList->RefreshAll();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void AttributeDelete_ui::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               OnClose();
               break;

          case Qt::Key_Return:
               DeleteClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
