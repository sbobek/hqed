/*
 *     $Id: ValueEditor_ui.cpp,v 1.16.4.4.2.1 2011-03-16 08:58:37 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QInputDialog>

#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Attribute.h"

#include "../../C/XTT/AttributeManager_ui.h"
#include "../../C/XTT/ExpressionEditor_ui.h"
#include "../../C/XTT/SymbolicValueSelector_ui.h"

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hDomain.h"
#include "../../M/hSetItem.h"
#include "../../M/hValue.h"
#include "../../M/hFunction.h"
#include "../../M/hExpression.h"

#include "../../namespaces/ns_hqed.h"

#include "ValueEditor_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor ValueEditor_ui class.
//
// #param parent Pointer to parent object.
ValueEditor_ui::ValueEditor_ui(QWidget* /*parent*/)
{
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor ValueEdit_ui class.
ValueEditor_ui::~ValueEditor_ui(void)
{
     delete _sitem;
     
     delete formLayout;
     delete gp2Layout;
     delete gp3Layout;
     delete gp4Layout;
     delete sw1page0;
     delete sw1page1;
     delete sw1page2;
     delete sw2page0;
     delete sw2page1;
     delete sw2page2;
     
     if(expressionEditor != NULL)
          delete expressionEditor;
          
     if(symValSelector != NULL)
          delete symValSelector;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void ValueEditor_ui::initForm(void)
{
     setupUi(this);

     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     groupBox_4->setVisible(false);
     setSizePolicy(QSizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum));
     layoutForm();
     hqed::centerWindow(this);
     
     _type = NULL;                            
     _sitem = NULL;                       
     _cell = NULL;                       
     _setClass = -1;                           
     _singleRequired = true;                    
     _avr = false;
     _autohide = false;

     symValSelector = NULL;
     expressionEditor = NULL;
     
     // Initialize window configuration parameters
     vdt_cv = -1;                              
     vdt_avr = -1;                             
     vdt_exp = -1;     
     
     sv_get = -1;                              
     sv_nd = -1;                               
     sv_any = -1;                              
     sv_min = -1;                           
     sv_max = -1;                  

     connect(radioButton_4, SIGNAL(clicked()), this, SLOT(typeSelect()));
     connect(radioButton_5, SIGNAL(clicked()), this, SLOT(typeSelect()));
     connect(pushButton, SIGNAL(clicked()), this, SLOT(getFromValueClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(getToValueClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(okButtonClick()));
     connect(pushButton_4, SIGNAL(clicked()), this, SLOT(cancelButtonClick()));
     connect(pushButton_5, SIGNAL(clicked()), this, SLOT(valFromExprDefinitionClick()));
     connect(pushButton_6, SIGNAL(clicked()), this, SLOT(valToExprDefinitionClick()));
     connect(pushButton_7, SIGNAL(clicked()), this, SLOT(valFromAttributeSelectionClick()));
     connect(pushButton_8, SIGNAL(clicked()), this, SLOT(valToAttributeSelectionClick()));
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(typeOfValFromDefChanged(int)));
     connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(typeOfValToDefChanged(int)));
      
     radioButton_4->setChecked(true);
     groupBox_2->setVisible(false);
     typeSelect();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void ValueEditor_ui::layoutForm(void)
{
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox_3,   0, 0, 1, 3);
     formLayout->addWidget(groupBox_2,   1, 0, 1, 3);
     formLayout->addWidget(groupBox_4,   2, 0, 1, 3);
     formLayout->addWidget(pushButton_3, 3, 1);
     formLayout->addWidget(pushButton_4, 3, 2);
     formLayout->setRowStretch(4, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(radioButton_4, 0, 0);
     gp2Layout->addWidget(radioButton_5, 0, 3);
     gp2Layout->setColumnStretch(0, 1);
     gp2Layout->setColumnStretch(1, 0);
     gp2Layout->setColumnStretch(2, 0);
     gp2Layout->setColumnStretch(3, 1);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);
     
     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(label,         0, 0);
     gp3Layout->addWidget(comboBox,      0, 1);
     gp3Layout->addWidget(stackedWidget, 1, 0, 1, 2);
     gp3Layout->setColumnStretch(0, 0);
     gp3Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);
     
     gp4Layout = new QGridLayout;
     gp4Layout->addWidget(label_5,         0, 0);
     gp4Layout->addWidget(comboBox_2,      0, 1);
     gp4Layout->addWidget(stackedWidget_2, 1, 0, 1, 2);
     gp4Layout->setColumnStretch(0, 0);
     gp4Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp4Layout);
     groupBox_4->setLayout(gp4Layout);
     
     sw1page0 = new QGridLayout;
     sw1page0->addWidget(label_7,    0, 0);
     sw1page0->addWidget(comboBox_3, 0, 1);
     sw1page0->addWidget(pushButton, 0, 2);
     sw1page0->addWidget(label_2,    1, 0);
     sw1page0->addWidget(label_3,    1, 1, 1, 2);
     sw1page0->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page0);
     stackedWidget->widget(0)->setLayout(sw1page0);
     
     sw1page1 = new QGridLayout;
     sw1page1->addWidget(label_13,     0, 0);
     sw1page1->addWidget(pushButton_7, 0, 2);
     sw1page1->addWidget(label_14,     1, 0, 1, 3);
     sw1page1->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page1);
     stackedWidget->widget(1)->setLayout(sw1page1);
     
     sw1page2 = new QGridLayout;
     sw1page2->addWidget(label_9,      0, 0);
     sw1page2->addWidget(pushButton_5, 0, 2);
     sw1page2->addWidget(label_10,     1, 0, 1, 3);
     sw1page2->setColumnStretch(1, 1);
     hqed::setupLayout(sw1page2);
     stackedWidget->widget(2)->setLayout(sw1page2);
     
     sw2page0 = new QGridLayout;
     sw2page0->addWidget(label_8,      0, 0);
     sw2page0->addWidget(comboBox_4,   0, 1);
     sw2page0->addWidget(pushButton_2, 0, 2);
     sw2page0->addWidget(label_4,      1, 0);
     sw2page0->addWidget(label_6,      1, 1, 1, 2);
     sw2page0->setColumnStretch(1, 1);
     hqed::setupLayout(sw2page0);
     stackedWidget_2->widget(0)->setLayout(sw2page0);
     
     sw2page1 = new QGridLayout;
     sw2page1->addWidget(label_15,     0, 0);
     sw2page1->addWidget(pushButton_8, 0, 2);
     sw2page1->addWidget(label_16,     1, 0, 1, 3);
     sw2page1->setColumnStretch(1, 1);
     hqed::setupLayout(sw2page1);
     stackedWidget_2->widget(1)->setLayout(sw2page1);
     
     sw2page2 = new QGridLayout;
     sw2page2->addWidget(label_11,     0, 0);
     sw2page2->addWidget(pushButton_6, 0, 2);
     sw2page2->addWidget(label_12,     1, 0, 1, 3);
     sw2page2->setColumnStretch(1, 1);
     hqed::setupLayout(sw2page2);
     stackedWidget_2->widget(2)->setLayout(sw2page2);
}
// -----------------------------------------------------------------------------

// #brief Function calls the SymbolicValueSelector widget
//
// #param __type - the type of the value
// #param __setClass - the class of the edited set
// #param __value - value that we want to edit
// #return no values returns
void ValueEditor_ui::symbolicValueSelector(hType* __type, int __setClass, hValue* __value)
{
     if(symValSelector != NULL)
          delete symValSelector;
     
     symValSelector = new SymbolicValueSelector_ui;
     connect(symValSelector, SIGNAL(onOk()), this, SLOT(symValSelectorApply()));
     connect(symValSelector, SIGNAL(onClose()), this, SLOT(symValSelectorClose()));
     int smr = symValSelector->setMode(__type, __setClass, __value);
     
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "The mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
     
     symValSelector->show();
}
// -----------------------------------------------------------------------------

// #brief Function calls the SymbolicValueSelector widget with default arguments
//
// #param __which - denotes which value is edited
// #return no values returns
void ValueEditor_ui::symbolicValueSelector(int __which)
{
     hValue* value = NULL;
     
     if(__which == 0)
          value = _sitem->fromPtr();
     if(__which == 1)
          value = _sitem->toPtr();

     symbolicValueSelector(_type, _setClass, value);
}
// -----------------------------------------------------------------------------

// #brief Function that checks if the defined range is correct
//
// #return true when range is correct, or value is defined as single. Otherwise false.
bool ValueEditor_ui::checkRangeCorrectness(void)
{
     bool multipleVal = radioButton_5->isChecked();
     if(!multipleVal)
          return true;
          
     if(_sitem->isExpressionUsed())
          return false;
          
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ValueEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               cancelButtonClick();
               break;

          case Qt::Key_Return:
               okButtonClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the size of the window is changing.
//
// #param QResizeEvent* event - the object that contains the event parameters
// #return No return value.
void ValueEditor_ui::resizeEvent(QResizeEvent* event) 
{
     if(event->size().height() != event->oldSize().height())
          event->ignore();

     QWidget::resizeEvent(event);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when type of the value is changed
//
// #return No return value.
void ValueEditor_ui::typeSelect(void)
{
     if(!checkRangeCorrectness())
     {
          QMessageBox::critical(NULL, "HQEd", "The expressions are not allowed in range definitions. Fix the error and try again.", QMessageBox::Ok);
          radioButton_4->setChecked(true);
          return;
     }
     
     bool multipleVal = radioButton_5->isChecked();
     
     if(!multipleVal)
          groupBox_3->setTitle("Value:");
     if(multipleVal)
          groupBox_3->setTitle("From:");
     
     groupBox_4->setEnabled(multipleVal);
     groupBox_4->setVisible(multipleVal);
     resize(width()-1, 10);
     adjustSize();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when OK button is pressed
//
// #return No return value.
void ValueEditor_ui::okButtonClick(void)
{
     if(!checkRangeCorrectness())
     {
          QMessageBox::critical(NULL, "HQEd", "The expressions are not allowed in range definitions.", QMessageBox::Ok);
          return;
     }

     //int itype = radioButton_4->isChecked()?SET_ITEM_TYPE_SINGLE_VALUE:SET_ITEM_TYPE_RANGE;
     if(!spValueChanged(-2, NULL, 2))
          return;

     emit okClicked();
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when cancel button is pressed
//
// #return No return value.
void ValueEditor_ui::cancelButtonClick(void)
{    
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the window is going to be closed
//
// #param QCloseEvent* event - pointer to the object that cntains the event parameters
// #return No return value.
void ValueEditor_ui::closeEvent(QCloseEvent* /*event*/)
{
     emit windowClosed();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the window according to the item parameters and values
//
// #param hSetItem* __item - pointer to the item that mast be displayed. If it is NULL the item stored in _sitem will be displayed
// #return No return value.
void ValueEditor_ui::displayItem(hSetItem* __item)
{
     hSetItem* __sitem = __item;
     if(__sitem == NULL)
          __sitem = _sitem;

     radioButton_4->setChecked((__sitem->type() == SET_ITEM_TYPE_SINGLE_VALUE) || (_singleRequired));
     radioButton_5->setChecked((__sitem->type() == SET_ITEM_TYPE_RANGE) && (!_singleRequired));
     radioButton_5->setEnabled(!_singleRequired);
     groupBox_2->setVisible(!_singleRequired);
     if(_singleRequired)
          radioButton_4->setChecked(true);
     typeSelect();

     // Display setItem
     int idxs[] = {vdt_cv, vdt_cv, vdt_cv, vdt_avr, vdt_exp};
     int svidx[] = {sv_min, sv_max, sv_any, sv_nd};
     QString svstrs[] = {XTT_Attribute::_min_operator_, XTT_Attribute::_max_operator_, XTT_Attribute::_any_operator_, XTT_Attribute::_not_definded_operator_};

     comboBox->setCurrentIndex(idxs[__sitem->fromPtr()->type()]);
     label_3->setText(__sitem->strFrom());
     label_14->setText(__sitem->fromPtr()->attributeFieldToString());
     label_10->setText(__sitem->fromPtr()->expressionField()->toString());
     comboBox_2->setCurrentIndex(idxs[__sitem->toPtr()->type()]);
     label_6->setText(__sitem->strTo());
     label_16->setText(__sitem->toPtr()->attributeFieldToString());
     label_12->setText(__sitem->toPtr()->expressionField()->toString());

     for(int i=0;i<4;++i)
     {
          if((__sitem->strFrom() == svstrs[i]) && (svidx[i] > -1) && (comboBox_3->currentIndex() != svidx[i]))
               comboBox_3->setCurrentIndex(svidx[i]);
          if((__sitem->strTo() == svstrs[i]) && (svidx[i] > -1) && (comboBox_4->currentIndex() != svidx[i]))
               comboBox_4->setCurrentIndex(svidx[i]);
     }
}
// -----------------------------------------------------------------------------

// #brief Function sets the window working mode
//
// #param *__type - data type of the set
// #param *__sitem - edited set item
// #param int __setClass - type of the set
// #li 0-set
// #li 1-domain
// #li 2-action context
// #li 3-coditional context
// #li 4-cell in a state table
// #param __singleRequired - denotes if the set can contains more than one value
// #param __avr - denotes if the attribute value reference is allowed
// #param *__cell - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
// #return SET_WINDOW_MODE_xxx value as a result
int ValueEditor_ui::setMode(hType* __type, hSetItem* __sitem, int __setClass, bool __singleRequired, bool __avr, XTT_Cell* __cell)
{
     _type = __type;
     _sitem = __sitem->copyTo();
     _setClass = __setClass;
     _cell = __cell;
     _singleRequired = (__singleRequired) ||                                                                                                     // When single value is required
                       ((_setClass == 1) && (_type->baseType() == SYMBOLIC_BASED)) ||                                                            // When define symbolic domain 
                       ((_type->baseType() == SYMBOLIC_BASED) && (_type->typeClass() != TYPE_CLASS_DOMAIN)) ||                                   // When symbolic type has not domain (then there is no numeric representation)
                       ((_type->baseType() == SYMBOLIC_BASED) && (_type->typeClass() == TYPE_CLASS_DOMAIN) && (!_type->domain()->ordered()));    // When symbolic type has not ordered domain
     _avr = __avr;
     if(!isModeCorrect())
          return SET_WINDOW_MODE_FAIL;

     // Initializing the list of type of value definitions
     QStringList vdts = QStringList() << "Constant value";

     vdt_cv = 0;              
     if(((_setClass == 0) || (_setClass == 2)) &&
        (_avr))
     {    
          vdt_avr = vdts.size();
          vdts << "Attribute reference";
     }
     
     if((_setClass == 0) || (_setClass == 2))
     {
          vdt_exp = vdts.size();
          vdts << "Expression";
     }

     comboBox->clear();
     comboBox_2->clear();
     comboBox->addItems(vdts);
     comboBox_2->addItems(vdts);
     comboBox->setCurrentIndex(vdt_cv);
     comboBox_2->setCurrentIndex(vdt_cv);
     comboBox->setVisible(comboBox->count() > 1);
     comboBox_2->setVisible(comboBox_2->count() > 1);
     label->setVisible(comboBox->count() > 1);
     label_5->setVisible(comboBox_2->count() > 1);

     // Initializing the list of  specified values
     QStringList svl = QStringList() << "Get value";
     sv_get = 0;  

     if((_type->baseType() == INTEGER_BASED) || (_type->baseType() == NUMERIC_BASED))
     {
          sv_min = svl.size();    
          svl << XTT_Attribute::_min_operator_;
          sv_max = svl.size();
          svl << XTT_Attribute::_max_operator_;
     }

     // FIXME - is this condition correct ???
     if((_setClass != 1) && (_setClass != 2) && (_setClass != 4))
     {
          sv_nd = svl.size();    
          svl << XTT_Attribute::_not_definded_operator_;
          sv_any = svl.size();
          svl << XTT_Attribute::_any_operator_;
     }

     comboBox_3->clear();
     comboBox_4->clear();
     comboBox_3->addItems(svl);
     comboBox_4->addItems(svl);
     comboBox_3->setCurrentIndex(sv_get);
     comboBox_4->setCurrentIndex(sv_get);  

     displayItem();
     resize(5,5);

     // Connecting slots and signals
     connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(spFromValueChanged(int)));
     connect(comboBox_4, SIGNAL(currentIndexChanged(int)), this, SLOT(spToValueChanged(int)));
     
     // When the set class is a symbolic domain, then we enter to the hide mode.
     if(((__setClass == 1) /*|| (__setClass == 2)*/ || (__setClass == 4)) &&
        (_type->baseType() == SYMBOLIC_BASED))
     {
          _autohide = true;
          getFromValueClick();
          return SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW;
     }
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function checks if the current mode is correct
//
// #return true if the current mode is correct, otherwise false
bool ValueEditor_ui::isModeCorrect(void)
{
     bool res = true;
     
     if(_type == NULL)
          res = false;
          
     if(_sitem == NULL)
          return false;
          
     if(_sitem->type() == SET_ITEM_TYPE_NOT_DEFINED)
          return false;
          
     if((_setClass != 0) && (_setClass != 1) && (_setClass != 2) && (_setClass != 3) && (_setClass != 4))
          res = false;
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when get value button is clicked
//
// #return No return value.
void ValueEditor_ui::getFromValueClick(void)
{
     comboBox_3->setCurrentIndex(sv_get);
     spValueChanged(sv_get, _sitem->fromPtr(), 0);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when get value button is clicked
//
// #return No return value.
void ValueEditor_ui::getToValueClick(void)
{
     comboBox_4->setCurrentIndex(sv_get);
     spValueChanged(sv_get, _sitem->toPtr(), 1);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of value definition has changed
//
// #param int __newIndex - nowy index
// #return No return value.
void ValueEditor_ui::typeOfValFromDefChanged(int __newIndex)
{
     if((__newIndex == vdt_cv) && (vdt_cv != -1))
          stackedWidget->setCurrentIndex(0);
     if((__newIndex == vdt_avr) && (vdt_avr != -1))
          stackedWidget->setCurrentIndex(1);
     if((__newIndex == vdt_exp) && (vdt_exp != -1))
          stackedWidget->setCurrentIndex(2);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of value definition has changed
//
// #param int __newIndex - new item index
// #return No return value.
void ValueEditor_ui::typeOfValToDefChanged(int __newIndex)
{
     if((__newIndex == vdt_cv) && (vdt_cv != -1))
          stackedWidget_2->setCurrentIndex(0);
     if((__newIndex == vdt_avr) && (vdt_avr != -1))
          stackedWidget_2->setCurrentIndex(1);
     if((__newIndex == vdt_exp) && (vdt_exp != -1))
          stackedWidget_2->setCurrentIndex(2);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of specify value changed
//
// #param int __newIndex - new specify value index
// #return No return value.
void ValueEditor_ui::spFromValueChanged(int __newIndex)
{
     if(__newIndex == sv_get)      // We do not call the "spValueChanged" function - first the button should be preessed
          return;
          
     spValueChanged(__newIndex, _sitem->fromPtr(), 0);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of specify value changed
//
// #param int __newIndex - new specify value index
// #return No return value.
void ValueEditor_ui::spToValueChanged(int __newIndex)
{
     if(__newIndex == sv_get)      // We do not call the "spValueChanged" function - first the button should be preessed
          return;
          
     spValueChanged(__newIndex, _sitem->toPtr(), 1);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when select attribute button for "from" value is pressed
//
// #return No return value.
void ValueEditor_ui::valFromAttributeSelectionClick(void)
{
     if(AttrManager != NULL)
          delete AttrManager;

     AttrManager = new AttributeManager_ui;

     connect(AttrManager, SIGNAL(onSelectClose()), this, SLOT(valFromAttributeSelectionFinish()));

     // When the setItem is not defined correctly
     if(_sitem->fromPtr() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }

     // Setting the apropriate window working mode
     int smr = AttrManager->setMode(_type, _setClass, true, _sitem->fromPtr()->attributeField(), _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     AttrManager->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the attribute manager is closed with select button
//
// #return No return value.
void ValueEditor_ui::valFromAttributeSelectionFinish(void)
{
     _sitem->fromPtr()->setAttribute(AttrManager->selectionResult());
     _sitem->fromPtr()->setType(ATTRIBUTE);
     
     label_14->setText(_sitem->fromPtr()->attributeFieldToString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when select attribute button for "to" value is pressed
//
// #return No return value.
void ValueEditor_ui::valToAttributeSelectionClick(void)
{
     if(AttrManager != NULL)
          delete AttrManager;

     AttrManager = new AttributeManager_ui;

     connect(AttrManager, SIGNAL(onSelectClose()), this, SLOT(valToAttributeSelectionFinish()));

     // When the setItem is not defined correctly
     if(_sitem->toPtr() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }

     // Setting the apropriate window working mode
     int smr = AttrManager->setMode(_type, _setClass, true, _sitem->toPtr()->attributeField(), _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     AttrManager->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the attribute manager is closed with select button
//
// #return No return value.
void ValueEditor_ui::valToAttributeSelectionFinish(void)
{
     _sitem->toPtr()->setAttribute(AttrManager->selectionResult());
     _sitem->toPtr()->setType(ATTRIBUTE);
     _sitem->setType(SET_ITEM_TYPE_RANGE);
     
     label_16->setText(_sitem->toPtr()->attributeFieldToString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when edit expression for "from" value is pressed
//
// #return No return value.
void ValueEditor_ui::valFromExprDefinitionClick(void)
{
     if(expressionEditor != NULL)
          delete expressionEditor;

     expressionEditor = new ExpressionEditor_ui;

     connect(expressionEditor, SIGNAL(okClicked()), this, SLOT(valFromExprDefinitionFinish()));
     connect(expressionEditor, SIGNAL(windowClose()), this, SLOT(valFromExprDefinitionWindowClosed()));

     // When the setItem is not defined correctly
     if(_sitem->fromPtr() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined setItem->from object.", QMessageBox::Ok);
          return;
     }
     
     // When the setItem is not defined correctly
     if(_sitem->fromPtr()->expressionField() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }

     // Setting the apropriate window working mode
     int smr = expressionEditor->setMode(FUNCTION_TYPE__SINGLE_VALUED, _type, _avr, _sitem->fromPtr()->expressionField(), _setClass, _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     expressionEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed with ok button
//
// #return No return value.
void ValueEditor_ui::valFromExprDefinitionFinish(void)
{   
     // delete _sitem->fromPtr()->expressionField(); //
     _sitem->fromPtr()->setExpression(expressionEditor->result());
     _sitem->fromPtr()->setType(EXPRESSION);    
     
     label_10->setText(_sitem->fromPtr()->expressionField()->toString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed
//
// #return No return value.
void ValueEditor_ui::valFromExprDefinitionWindowClosed(void)
{    
}
// -----------------------------------------------------------------------------

// #brief Function triggered when edit expression for "to" value is pressed
//
// #return No return value.
void ValueEditor_ui::valToExprDefinitionClick(void)
{
     if(expressionEditor != NULL)
          delete expressionEditor;

     expressionEditor = new ExpressionEditor_ui;

     connect(expressionEditor, SIGNAL(okClicked()), this, SLOT(valToExprDefinitionFinish()));
     connect(expressionEditor, SIGNAL(windowClose()), this, SLOT(valToExprDefinitionWindowClosed()));

     // When the setItem is not defined correctly
     if(_sitem->toPtr() == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Not defined expression object.", QMessageBox::Ok);
          return;
     }

     // Setting the apropriate window working mode
     int smr = expressionEditor->setMode(FUNCTION_TYPE__SINGLE_VALUED, _type, _avr, _sitem->toPtr()->expressionField(), _setClass, _cell);
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;

     expressionEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed with ok button
//
// #return No return value.
void ValueEditor_ui::valToExprDefinitionFinish(void)
{
     _sitem->toPtr()->setExpression(expressionEditor->result());
     _sitem->toPtr()->setType(EXPRESSION);
     _sitem->setType(SET_ITEM_TYPE_RANGE);
     
     label_12->setText(_sitem->toPtr()->expressionField()->toString());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the expression editor is closed
//
// #return No return value.
void ValueEditor_ui::valToExprDefinitionWindowClosed(void)
{
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the sybolicValueSelector is closed
//
// #return No return value.
void ValueEditor_ui::symValSelectorClose(void)
{
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the sybolicValueSelector is closed by ok button
//
// #return No return value.
void ValueEditor_ui::symValSelectorApply(void)
{
     spValueChanged(-2, NULL, 2);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when type of specify value change
//
// #param int __spIndex - new specify value index, if -2 that means the window is closed by ok button
// #param hValue* __value - poiter to the value
// #param int __which - denotes which value has been changed
// #return true if all the operation are succefully, otherwise false
bool ValueEditor_ui::spValueChanged(int __spIndex, hValue* __value, int __which)
{
     if(__spIndex == -1)
          return true;
          
     int destType = -1;
     QString newValue = "";
      
     // If MIN value
     if(__spIndex == sv_min)
     {
          destType = VALUE;
          newValue = QString::number(NUMERIC_INFIMUM);
     }
     
     // If MAX value
     if(__spIndex == sv_max)
     {
          destType = VALUE;
          newValue = QString::number(NUMERIC_SUPREMUM);
     }
     
     // If op:nd
     if(__spIndex == sv_nd)
     {
          destType = NOT_DEFINED;
          newValue = XTT_Attribute::_not_definded_operator_;
     }
     
     // If op:any
     if(__spIndex == sv_any)
     {
          destType = ANY_VALUE;
          newValue = XTT_Attribute::_any_operator_;
     }

     // If get value pressed
     if(__spIndex == sv_get)
     {
          hValueResult hvr = __value->value();
          if(!(bool)hvr)
               QMessageBox::critical(NULL, "HQEd", "Error during calculation " + __value->toString() + ".\nError message: " + (QString)hvr + ".", QMessageBox::Ok);
     
          // If set is defined
          if((_setClass == 0) || (_setClass == 2) || (_setClass == 3) || (_setClass == 4))
          {
               if((__spIndex == sv_get) && (_type->baseType() == BOOLEAN_BASED))
               {
                    bool ok;
                    int vals[] = {BOOLEAN_FALSE, BOOLEAN_TRUE};
                    QStringList si = QStringList() << XTT_Attribute::_false_operator_ << XTT_Attribute::_true_operator_;
                    int i_init_val = ((QString)hvr).toInt()==BOOLEAN_TRUE?1:0;
                    QString getval = QInputDialog::getItem(this, "Select value", "Select value", si, i_init_val, false, &ok, Qt::Dialog);
                    
                    if(ok)
                    {
                         int val_index = si.indexOf(getval);
                         if(val_index == -1)
                              return false;
                         newValue = QString::number(vals[val_index]);
                    }
                    if(!ok)
                         return false;
               }
               
               if((__spIndex == sv_get) && (_type->baseType() == INTEGER_BASED))
               {
                    bool ok;
                    int init_val = __value->type()==VALUE?((QString)hvr).toInt():0;

                    int iv = getValueFromInput<int>(
                         init_val,
                         [this](int initVal, bool& ok)->int {
                              return QInputDialog::getInteger(this, "Get value", "New value:", initVal, NUMERIC_INFIMUM, NUMERIC_SUPREMUM, 1, &ok, Qt::Dialog);
                         },
                         [](QString str)->int { return str.toInt(); },
                         [](int val)->QString { return QString::number(val); },
                         ok);

                    if(ok) {
                         newValue = QString::number(iv);
                    }
                    else {
                         return false;
                    }
               }
               
               if((__spIndex == sv_get) && (_type->baseType() == NUMERIC_BASED))
               {
                    bool ok;
                    double init_val = __value->type()==VALUE?((QString)hvr).toDouble():0.;

                    int dv = getValueFromInput<double>(
                         init_val,
                         [this](double initVal, bool& ok)->double {
                              return QInputDialog::getDouble(this, "Get value", "New value:", initVal, NUMERIC_INFIMUM, NUMERIC_SUPREMUM, _type->allowedDecimalLength(), &ok, Qt::Dialog);
                         },
                         [](QString str)->double { return str.toDouble(); },
                         [this](double val)->QString { return QString::number(val, 'f', _type->allowedDecimalLength()); },
                         ok);

                    if(ok) {
                         newValue = QString::number(dv, 'f', _type->allowedDecimalLength());
                    }
                    else {
                         return false;
                    }
               }
               
               if((__spIndex == sv_get) && (_type->baseType() == SYMBOLIC_BASED))
               {
                    bool ok;
                    QString init_val = __value->type()==VALUE?((QString)hvr):"";
                    QString getval = "";
                    if(_type->typeClass() != TYPE_CLASS_DOMAIN)
                         // @KKR: HERE ?
                         getval = getValueFromInput<QString>(
                              init_val,
                              [this](QString initVal, bool& ok)->QString {
                                   return QInputDialog::getText(this, "Get value", "New value:", QLineEdit::Normal, initVal, &ok, Qt::Dialog);
                              },
                              [](QString str)->QString { return str; },
                              [](QString val)->QString { return val; },
                              ok);

                    if(_type->typeClass() == TYPE_CLASS_DOMAIN)
                    {
                         // @KKR: tutaj nie ponieważ wartości są wybierane z listy, która może być renderowana przez plugin output
                         if(!_type->domain()->ordered())
                         {
                              QStringList si = _type->domain()->stringItems();
                              int i_init_val = si.indexOf(init_val);
                              getval = QInputDialog::getItem(this, "Select value", "Select value", si, i_init_val, false, &ok, Qt::Dialog);
                         }
                         
                         if(_type->domain()->ordered())
                         {
                              symbolicValueSelector(__which);
                              return true;
                         }
                    }
                    if(ok)
                         newValue = getval;
                    if(!ok)
                         return false;
               }
              
               // Checking for any operator in decision context
               if((_setClass == 2) && (newValue.toLower() == XTT_Attribute::_any_operator_.toLower()))
               {
                    QMessageBox::critical(NULL, "HQEd", "\'" + XTT_Attribute::_any_operator_ + "\' operator is not allowed in decision context.", QMessageBox::Ok);
                    return false;
               }        
               
               // Checking for any operator in state table
               if((_setClass == 4) && (newValue.toLower() == XTT_Attribute::_any_operator_.toLower()))
               {
                    QMessageBox::critical(NULL, "HQEd", "\'" + XTT_Attribute::_any_operator_ + "\' operator is not allowed in state table.", QMessageBox::Ok);
                    return false;
               }
          }
          
          // If domain is defined
          if(_setClass == 1)
          {
               // @KKR Tutaj nie ponieważ tutaj definiuje się dziedzina, która nie może mieć renderowania
               if((__spIndex == sv_get) && (_type->baseType() == BOOLEAN_BASED))
               {
                    bool ok;
                    int vals[] = {BOOLEAN_FALSE, BOOLEAN_TRUE};
                    QStringList si = QStringList() << XTT_Attribute::_false_operator_ << XTT_Attribute::_true_operator_;
                    int i_init_val = ((QString)hvr).toInt()==BOOLEAN_TRUE?1:0;
                    QString getval = QInputDialog::getItem(this, "Select value", "Select value", si, i_init_val, false, &ok, Qt::Dialog);
                    
                    if(ok)
                    {
                         int val_index = si.indexOf(getval);
                         if(val_index == -1)
                              return false;
                         newValue = QString::number(vals[val_index]);
                    }
                    if(!ok)
                         return false;
               }
          
               if((__spIndex == sv_get) && (_type->baseType() == INTEGER_BASED))
               {
                    bool ok;
                    int init_val = __value->type()==VALUE?((QString)hvr).toInt():0;
                    int iv = QInputDialog::getInteger(this, "Get value", "New value:", init_val, NUMERIC_INFIMUM, NUMERIC_SUPREMUM, 1, &ok, Qt::Dialog);
                    if(ok)
                         newValue = QString::number(iv);
                    if(!ok)
                         return false;
               }
               
               if((__spIndex == sv_get) && (_type->baseType() == NUMERIC_BASED))
               {
                    bool ok;
                    double init_val = __value->type()==VALUE?((QString)hvr).toDouble():0.;
                    double dv = QInputDialog::getDouble(this, "Get value", "New value:", init_val, NUMERIC_INFIMUM, NUMERIC_SUPREMUM, _type->allowedDecimalLength(), &ok, Qt::Dialog);
                    if(ok)
                         newValue = QString::number(dv, 'f', _type->allowedDecimalLength());
                    if(!ok)
                         return false;
               }
               
               if((__spIndex == sv_get) && (_type->baseType() == SYMBOLIC_BASED))
               {
                    bool ok;
                    QString init_val = __value->type()==VALUE?((QString)hvr):"";
                    QString getval = "";
                    if((_type->typeClass() != TYPE_CLASS_DOMAIN) || (!_type->domain()->ordered()))
                         getval = QInputDialog::getText(this, "Get value", "New value:", QLineEdit::Normal, init_val, &ok, Qt::Dialog);
                    if((_type->typeClass() == TYPE_CLASS_DOMAIN) && (_type->domain()->ordered()))
                    {
                         symbolicValueSelector(__which);
                         return true;
                    }
                    if(ok)
                         newValue = getval;
                    if(!ok)
                         return false;
               }
               
               // Checking for not defined or any value operator
               if(newValue.toLower() == XTT_Attribute::_not_definded_operator_.toLower())
               {
                    QMessageBox::critical(NULL, "HQEd", "\'" + XTT_Attribute::_not_definded_operator_ + "\' operator is not allowed as domain element.", QMessageBox::Ok);
                    return false;
               }
               if(newValue.toLower() == XTT_Attribute::_any_operator_.toLower())
               {
                    QMessageBox::critical(NULL, "HQEd", "\'" + XTT_Attribute::_any_operator_ + "\' operator is not allowed as domain element.", QMessageBox::Ok);
                    return false;
               }                  
          }
     }
     
     int itype = radioButton_4->isChecked()?SET_ITEM_TYPE_SINGLE_VALUE:SET_ITEM_TYPE_RANGE;
     bool checkConstraints = ((_setClass != 1) && (!_sitem->isAVRused()));
     hValue *__newFrom = NULL, *__newTo = NULL, *__newValue;
     __newValue = new hValue(newValue);
     
     //QMessageBox::critical(NULL, "Value Editor", "Checking value: " + newValue, QMessageBox::Ok);

     if(__which == 0)
     {
          __newFrom = __newValue;
          __newTo = new hValue(_sitem->toPtr());
     }
     if(__which == 1)
     {
          __newFrom = new hValue(_sitem->fromPtr());
          __newTo = __newValue;
     }
     if(__which == 2)
     {
          __newFrom = new hValue(_sitem->fromPtr());
          __newTo = new hValue(_sitem->toPtr());
     }
     
     int iic = _type->isItemCorrect(itype, __newFrom, __newTo, checkConstraints);
     if(iic != H_TYPE_ERROR_NO_ERROR)
     {
          QMessageBox::critical(NULL, "HQEd", _type->lastError(), QMessageBox::Ok);
          
          // When incorrect extreme values
          if(iic == H_TYPE_ERROR_INCOORECT_EXTREME_VALUES)
               _sitem->setType(SET_ITEM_TYPE_SINGLE_VALUE);
               
          displayItem();
          return false;
     }
     
     // There are two reasons why this operations canot be executed when the window is going to be closed:
     // 1. The _sitem's setValue or setRange functions have not effect - the values have been fixed before
     // 2. In case of hValue that has type other than VALUE i.e. EXRPRESSION or ATTRIBUTE, the type will be changed and fixed to current value of expression or attribute !!!
     if(__spIndex != -2)
     {
          if(itype == SET_ITEM_TYPE_SINGLE_VALUE) 
               _sitem->setValue(__newFrom, true);
          if(itype == SET_ITEM_TYPE_RANGE) 
               _sitem->setRange(__newFrom, __newTo, true);
     }

     delete __newFrom;
     delete __newTo;

     _sitem->setType(itype);
     displayItem();
     
     // When window works isn autohide mode
     if(_autohide)
     {
          emit okClicked();
          close();
     }
     
     return true;
}
// -----------------------------------------------------------------------------

template<typename T>
T ValueEditor_ui::getValueFromInput(
     T initVal,
     std::function<T(T, bool&)> getValue,
     std::function<T(QString)> fromString,
     std::function<QString(T)> toString,
     bool& ok) {

     auto throwIfFalse = [](bool b)->void {
          if (!b) {
               throw false;
          }
     };

     T retVal;

     try {
          bool available =
               _type->translateScriptAvailable(true)
            && _type->translateScriptAvailable(false);

          if (available && _type->presentationSourceId() == TYPE_PRESENTATION_ECMA) {
               int code;

               QString init_val_str = _type->translate(toString(initVal), code, false);
               throwIfFalse(code == 0);

               QString txt = QInputDialog::getText(this, "Get value", "New value:", QLineEdit::Normal, init_val_str, &ok, Qt::Dialog);
               throwIfFalse(ok);

               txt = _type->translate(txt, code, true);
               throwIfFalse(code == 0);

               retVal = fromString(txt);
          }
          else {
               retVal = getValue(initVal, ok);
               throwIfFalse(ok);
          }
     } catch(...) {
          retVal = initVal;
          ok = false;
     }

     return retVal;
}

// #brief Function returns the item that has been changed by using this dialog
//
// #return Pointer to the changed object.
hSetItem* ValueEditor_ui::result(void)
{
     return _sitem->copyTo();
}
// -----------------------------------------------------------------------------
