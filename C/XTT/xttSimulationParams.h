/**
 * \file	xttSimulationParams.h
 * \author      Krzysztof Kaczor kinio4444@gmail.com
 * \date 	03.12.2009 
 * \version	1.0
 * \brief	This file contains class definitioan that manages with parameters of simulation
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HXTTSIMULATIONPARAMSH
#define HXTTSIMULATIONPARAMSH
//---------------------------------------------------------------------------

#include <QList>
#include <QMenu>
#include <QObject>
#include <QIODevice>
#include <QProcess>
#include <QAction>
#include <QAbstractSocket>
//--------------------------------------------------------------------------

#define DEV_PLUGIN_SIM_TYPE_DDI 0
#define DEV_PLUGIN_SIM_TYPE_GDI 1
#define DEV_PLUGIN_SIM_TYPE_TDI 2
//--------------------------------------------------------------------------

class XTT_State;
class SimulationEditor_ui;
//--------------------------------------------------------------------------

/**
* \class 	xttSimulationParams
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	03.12.2009 
* \brief 	Class definitioan that manages with parameters of simulation
*/
class xttSimulationParams : public QObject
{
	Q_OBJECT

private:

     XTT_State* _usedState;
     QString _modelname;
     QString _username;
     QString _simInitState;
     QStringList _tables;
     int _simtype;
     
     /// \brief Function that initializes the filed of the class with default values
     ///
     /// \return no values return
     void defaultInit(void);

public:

     /// \brief Construcotr of the xttSimulationParams class
     xttSimulationParams(void);
     
     /// \brief Construcotr of the xttSimulationParams class
     ///
     /// \param __initialState - initial state of the simulation
     xttSimulationParams(XTT_State* __initialState);
     
     /// \brief Destructor of the xttSimulationParams class
     ~xttSimulationParams(void);
     
     /// \brief Function that retrieves data about simulation from the simulation editor
     ///
     /// \return no values return
     void getDataFromSimulationEditor(SimulationEditor_ui* __simeditor);
     
     /// \brief Function that returns the pointer to the state from which the simulation starts. The NULL pointer indicates that the simulation has started from manualy defined state.
     ///
     /// \return the pointer to the state from which the simulation starts
     XTT_State* usedState(void);
     
     /// \brief Function that returns a name of the model
     ///
     /// \return a name of the model
     QString modelName(void);
     
     /// \brief Function that returns a name of the user/client
     ///
     /// \return a name of the user/client
     QString userName(void);
     
     /// \brief Function that returns protocol representation of the initial state for the simulation
     ///
     /// \return protocol representation of the initial state for the simulation
     QString initialState(void);
     
     /// \brief Function that returns a list and order of the tables
     ///
     /// \return a list and order of the tables
     QStringList tables(void);
     
     /// \brief Function that returns a type of the simulation
     ///
     /// \return a type of the simulation
     int simulationType(void);
     
     
     /// \brief Function that sets the pointer of the state from which the simulation starts. The NULL pointer indicates that the simulation will start from manualy defined state.
     ///
     /// \param __state - the pointer to the state from which the simulation will start
     /// \return no values returns
     void setUsedState(XTT_State* __state);
     
     /// \brief Function that sets a name of the model
     ///
     /// \param __modelname - a name of the model
     /// \return no values returns
     void setModelName(QString __modelname);
     
     /// \brief Function that sets a name of the user/client
     ///
     /// \param __uname - a name of the user/client
     /// \return no values returns
     void setUserName(QString __uname);
     
     /// \brief Function that sets a protocol representation of the initial state for the simulation
     ///
     /// \param __initstate - protocol representation of the initial state for the simulation
     /// \return no values returns
     void setInitialState(QString __initstate);
     
     /// \brief Function that sets a list and order of the tables
     ///
     /// \param __tables - a list and order of the tables
     /// \return no values returns
     void setTables(QStringList __tables);
     
     /// \brief Function that sets a type of the simulation
     ///
     /// \param __simulationtype - a type of the simulation
     /// \return no values returns
     void setSimulationType(int __simulationtype);
     
     /// \brief Function that defines the default set of tables according to the simulation type
     ///
     /// \return no values returns
     void defineDefaultSetOfTables(void);
};
//---------------------------------------------------------------------------

#endif
