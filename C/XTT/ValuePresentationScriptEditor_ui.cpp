/*
 *	  $Id: ValuePresentationScriptEditor_ui.cpp,v 1.2.4.1 2010-11-19 08:17:05 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>
#include <QTabWidget>

#include "../../M/hType.h"

#include "../../namespaces/ns_hqed.h"
#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"
#include "../Settings_ui.h"
#include "../TextEditor_ui.h"
#include "ValuePresentationScriptEditor_ui.h"
// -----------------------------------------------------------------------------

ValuePresentationScriptEditor_ui* ValuePresentationScriptEditor;
// -----------------------------------------------------------------------------

// #brief Constructor TextEditor_ui class.
//
// #param *parent - Pointer to parent.
ValuePresentationScriptEditor_ui::ValuePresentationScriptEditor_ui(QWidget* /*parent*/)
{
     initWindow();
}
// -----------------------------------------------------------------------------

// #brief Destructore TextEditor_ui class.
ValuePresentationScriptEditor_ui::~ValuePresentationScriptEditor_ui(void)
{
     delete gb1Layout;
     delete gb2Layout;
     delete formLayout;
     delete editor;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #return no values returns
void ValuePresentationScriptEditor_ui::initWindow(void)
{
     setupUi(this);
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);
     
     toolButton->setIcon(QIcon(":/all_images/images/greenPlay.png"));
     connect(toolButton, SIGNAL(clicked()), this, SLOT(testClick()));
     
     label->setText(hqed::processAppVariables(label->text()));
     label_2->setText(hqed::processAppVariables(label_2->text()));
     label_5->setText(hqed::processAppVariables(label_5->text()));

     editor = new TextEditor_ui;
     connect(editor, SIGNAL(onclose()), this, SLOT(onEditorClose()));
     editor->setMode(TE_MODE_SCRIPT_EDIT);
     layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the mode of the dialog
//
// #param __createnew - indicates if the edited script is a new script
// #param __filename - defines the file name of the script
// #return window mode result type
int ValuePresentationScriptEditor_ui::setMode(bool __createnew, QString __filename)
{
     if((!__createnew) && (!QFile::exists(__filename)))
          return SET_WINDOW_MODE_FAIL;

     _new = __createnew;
     _filename = __filename;
     updateDialog();
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function that updates the GUI according to window mode
//
// #return no values return
void ValuePresentationScriptEditor_ui::updateDialog(void)
{
     if(!_new)
          editor->openFile(0, _filename);
     editor->useEcmaMode();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void ValuePresentationScriptEditor_ui::layoutForm(void)
{
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox,   0, 0);
     formLayout->addWidget(editor,     1, 0);
     formLayout->addWidget(groupBox_2, 2, 0);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gb1Layout = new QGridLayout;
     gb1Layout->addWidget(label, 0, 0);
     hqed::setupLayout(gb1Layout);
     groupBox->setLayout(gb1Layout);
     
     gb2Layout = new QGridLayout;
     gb2Layout->addWidget(label_2,           0, 0, 1, 5);
     gb2Layout->addWidget(inputRadioButton,  1, 0);
     gb2Layout->addWidget(outputRadioButton, 1, 1);
     gb2Layout->addWidget(label_5,           1, 2);
     gb2Layout->addWidget(lineEdit,          1, 3);
     gb2Layout->addWidget(toolButton,        1, 4);
     gb2Layout->addWidget(label_3,           2, 0);
     gb2Layout->addWidget(label_4,           2, 1, 1, 4);
     gb2Layout->setColumnStretch(0, 1);
     gb2Layout->setColumnStretch(1, 2);
     gb2Layout->setColumnStretch(3, 2);
     hqed::setupLayout(gb2Layout);
     groupBox_2->setLayout(gb2Layout);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when close button is pressed
//
// #return No return value.
void ValuePresentationScriptEditor_ui::testClick(void)
{
     QString scriptcontent = "";
     QString value = lineEdit->text();
     
     if(editor->documents().size() > 0)
          scriptcontent = editor->documents().at(0)->doc()->toPlainText();
     
     int ok;
     bool isInput;

     if(inputRadioButton->isChecked())
          isInput = true;
     else
          isInput = false;

     label_4->setText(hType::translateScript(scriptcontent, value, ok, isInput));
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when window is closed.
//
// #param event - Pointer to close event object.
// #return No return value.
void ValuePresentationScriptEditor_ui::closeEvent(QCloseEvent* /*event*/)
{
     emit onclose();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when the editor window is closed
//
// #return No return value.
void ValuePresentationScriptEditor_ui::onEditorClose(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void ValuePresentationScriptEditor_ui::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
