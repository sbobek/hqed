/*
 *	  $Id: PluginValuePresentation_ui.cpp,v 1.1.2.2 2010-10-13 14:14:42 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>
#include <QTabWidget>

#include "../../M/hType.h"

#include "../../namespaces/ns_hqed.h"
#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"
#include "../Settings_ui.h"
#include "../TextEditor_ui.h"
#include "PluginValuePresentation_ui.h"
// -----------------------------------------------------------------------------

// #brief Constructor PluginValuePresentation_ui class.
//
// #param *parent - Pointer to parent.
PluginValuePresentation_ui::PluginValuePresentation_ui(QWidget* /*parent*/)
{
     initWindow();
}
// -----------------------------------------------------------------------------

// #brief Destructore PluginValuePresentation_ui class.
PluginValuePresentation_ui::~PluginValuePresentation_ui(void)
{
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #return no values returns
void PluginValuePresentation_ui::initWindow(void)
{
     setupUi(this);
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);
     
     connect(pushButton, SIGNAL(clicked()), this, SLOT(translateClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeClick()));
     
     layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function that lists the services from the plugin
//
// #return No return value.
void PluginValuePresentation_ui::listServices(void)
{
     comboBox->addItems(_plugin->presentationServices());
}
// -----------------------------------------------------------------------------

// #brief Function that sets the mode of the dialog
//
// #param __plugin - pointer to the plugin
// #return window mode result type
int PluginValuePresentation_ui::setMode(devPlugin* __plugin)
{
     if((__plugin == NULL) && (devPC->indexOfDevice(__plugin->device()) == -1))
          return SET_WINDOW_MODE_FAIL;
          
     _plugin = __plugin;
     
     // service listing
     listServices();
     
     return SET_WINDOW_MODE_OK_YOU_CAN_SHOW;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void PluginValuePresentation_ui::layoutForm(void)
{
     formLayout = new QGridLayout;
     formLayout->addWidget(label,        0, 0);
     formLayout->addWidget(comboBox,     1, 0);
     formLayout->addWidget(label_2,      2, 0);
     formLayout->addWidget(lineEdit,     3, 0);
     formLayout->addWidget(label_3,      4, 0);
     formLayout->addWidget(lineEdit_2,   5, 0);
     formLayout->addWidget(pushButton,   4, 1);
     formLayout->addWidget(pushButton_2, 5, 1);
     formLayout->setRowStretch(6, 1);
     formLayout->setColumnStretch(0, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when translate button is pressed
//
// #return No return value.
void PluginValuePresentation_ui::translateClick(void)
{
     int iresult;
     QString sname = comboBox->currentText();
     QString value = lineEdit->text();
     
     lineEdit_2->clear();
     QString sresult = _plugin->valueTranslation(sname, value, iresult);
     if(iresult != DEV_PLUGIN_RESULT_CODE_SUCCESS)
     {
          QMessageBox::critical(this, "HQEd", "Error during value translating:\nPlugin error: " + sresult, QMessageBox::Ok);
          return;
     }
     
     lineEdit_2->setText(sresult);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when close button is pressed
//
// #return No return value.
void PluginValuePresentation_ui::closeClick(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void PluginValuePresentation_ui::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Return:
               translateClick();
               break;
               
          case Qt::Key_Escape:
               close();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
