#include <QtGui>

#include "ExpressionSyntaxHighlighter.h"

ExpressionSyntaxHighlighter::ExpressionSyntaxHighlighter(QTextDocument *parent)
    : QSyntaxHighlighter(parent)
{
    m_AttributeFormat.setForeground(Qt::darkBlue);

    m_FunctionFormat.setForeground(Qt::blue);
    m_FunctionFormat.setFontItalic(true);

    m_SymbolicFormat.setForeground(Qt::darkGreen);

    m_SpecialFormat.setForeground(Qt::darkGreen);
    m_SpecialFormat.setFontItalic(true);

    m_NumericFormat.setForeground(Qt::darkMagenta);
}

void ExpressionSyntaxHighlighter::refreshRules()
{
    m_HighlightingRules.clear();

    // Add the rule for numeric values
    HighlightingRule rule;
    rule.pattern = QRegExp("\\d+");
    rule.format = m_NumericFormat;
    m_HighlightingRules.append(rule);

    foreach(const QString &name, m_AttributeNames) {
        rule.pattern = QRegExp("\\b" + name + "\\b");
        rule.format = m_AttributeFormat;
        m_HighlightingRules.append(rule);
    }

    foreach(const QString &name, m_FunctionNames) {
        rule.pattern = QRegExp("\\b" + name + "(?=\\()");
        rule.format = m_FunctionFormat;
        m_HighlightingRules.append(rule);
    }

    foreach(const QString &name, m_SymbolicNames) {
        rule.pattern = QRegExp("\\b" + name + "\\b");
        rule.format = m_SymbolicFormat;
        m_HighlightingRules.append(rule);
    }

    foreach(const QString &name, m_SpecialNames) {
        rule.pattern = QRegExp("\\b" + name + "\\b");
        rule.format = m_SpecialFormat;
        m_HighlightingRules.append(rule);
    }
}

void ExpressionSyntaxHighlighter::highlightBlock(const QString &text)
{
    foreach (const HighlightingRule &rule, m_HighlightingRules) {
        QRegExp expression(rule.pattern);
        int index = expression.indexIn(text);
        while (index >= 0) {
            int length = expression.matchedLength();
            setFormat(index, length, rule.format);
            index = expression.indexIn(text, index + length);
        }
    }
}
