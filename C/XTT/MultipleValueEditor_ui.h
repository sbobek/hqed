/**
 * \file	MultipleValueEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	16.09.2008
 * \version	1.0
 * \brief	This file contains class definition that arranges the multiple value editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef MultipleValueEditorH
#define MultipleValueEditorH
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include <QResizeEvent>

#include "ui_MultipleValueEditor.h"
// -----------------------------------------------------------------------------

class hType;
class hSet;
class hSetItem;
class hValue;
class hMultipleValue;
class XTT_Cell;

class SetEditor_ui;
class ExpressionEditor_ui;
// -----------------------------------------------------------------------------

/**
* \class 	MultipleValueEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	07.08.2008
* \brief 	Class definition that arranges the multiple value editor window.
*/
class MultipleValueEditor_ui : public QWidget, private Ui_MultipleValueEditor
{
    Q_OBJECT
    
    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
    QGridLayout* sw1page0;	               ///< Pointer to page0 grid layout.
    QGridLayout* sw1page1;	               ///< Pointer to page0 grid layout.
    QGridLayout* sw1page2;	               ///< Pointer to page1 grid layout.
    
    hType* _type;                            ///< The type of value
    hMultipleValue* _mv;                     ///< Edited item
    XTT_Cell* _cell;                         ///< Pointer to the edited cell
    int _setClass;                           ///< Denotes the type of the set (0-set, 1-domain, 2-decision context)
    bool _singleRequired;                    ///< Denotes if only single value is required
    bool _avr;                               ///< Denotes if the attribute value reference is allowed
    
    // Window configuration
    int vdt_cv;                              ///< ComboBox index - value definition type - constant value
    int vdt_avr;                             ///< ComboBox index - value definition type - AVR
    int vdt_exp;                             ///< ComboBox index - value definition type - expression
    
    // Specify values index in ComboBoxes
    int sv_get;                              ///< Allow for input new value
    int sv_nd;                               ///< Not defined value
    int sv_any;                              ///< Any value
    
    SetEditor_ui* setEditor;                 ///< Pointer to the set editor window
    ExpressionEditor_ui* expressionEditor;   ///< Pointer to the expression editor window
    
protected:

     /// \brief Function triggered when the size of the window is changing.
     ///
     /// \param event - the object that contains the event parameters
     /// \return No return value.
     virtual void resizeEvent(QResizeEvent* event); 

     /// \brief Function triggered when the window is going to be closed
     ///
     /// \param event - pointer to the object that cntains the event parameters
     /// \return No return value.
     virtual void closeEvent(QCloseEvent* event);
     
public:

     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param parent Pointer to parent object.
     MultipleValueEditor_ui(QWidget *parent = 0);
     
     /// \brief Destructor ValueEdit_ui class.
     ~MultipleValueEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
	void keyPressEvent(QKeyEvent*);
     
     /// \brief Function sets the window working mode
	///
     /// \param *__type - data type of the set
     /// \param *__mv - pointer to the edited item
     /// \param __setClass - type of the set
     /// \li 0-set
     /// \li 1-domain
     /// \li 2-action context
     /// \li 3-coditional context
     /// \param __singleRequired - denotes if the set can contains more than one value
     /// \param __avr - denotes if the attribute value reference is allowed
     /// \param *__ptr - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
	/// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, hMultipleValue* __mv, int __setClass, bool __singleRequired, bool __avr, XTT_Cell* __ptr);
     
     /// \brief Function checks if the current mode is correct
	///
	/// \return true if the current mode is correct, otherwise false
     bool isModeCorrect(void);
     
     /// \brief Function triggered when type of specify value change
     ///
     /// \param __spIndex - new specify value index, if -2 that means the window is closed by ok button
     /// \param *__value - poiter to the value
     /// \return true if all the operation are succefully, otherwise false
     bool spValueChanged(int __spIndex, hMultipleValue* __value);
     
     /// \brief Function returns the item that has been changed by using this dialog
	///
	/// \return Pointer to the changed object.
     hMultipleValue* result(void);

public slots:
     
     /// \brief Function triggered when OK button is pressed
	///
	/// \return No return value.
     void okButtonClick(void);
     
     /// \brief Function triggered when cancel button is pressed
	///
	/// \return No return value.
     void cancelButtonClick(void);
     
     /// \brief Function triggered when get value button is clicked
	///
	/// \return No return value.
     void getValueClick(void);
     
     /// \brief Function triggered when set editor is closed with ok button
	///
	/// \return No return value.
     void setChanged(void);
     
     /// \brief Function triggered when type of value definition has changed
	///
     /// \param __newIndex - new item index
	/// \return No return value.
     void typeOfValDefChanged(int __newIndex);
     
     /// \brief Function triggered when type of specify value changed
	///
     /// \param __newIndex - new specify value index
	/// \return No return value.
     void spValueChanged(int __newIndex);
     
     /// \brief Function triggered when select attribute button is pressed
     ///
     /// \return No return value.
     void valAttributeSelectionClick(void);
     
     /// \brief Function triggered when the attribute manager is closed with select button
     ///
     /// \return No return value.
     void valAttributeSelectionFinish(void);
     
     /// \brief Function triggered when edit expression for "from" value is pressed
	///
	/// \return No return value.
     void valExprDefinitionClick(void);
     
     /// \brief Function triggered when the expression editor is closed with ok button
	///
	/// \return No return value.
     void valExprDefinitionFinish(void);
     
     /// \brief Function triggered when the expression editor is closed
	///
	/// \return No return value.
     void valExprDefinitionWindowClosed(void);
     
     /// \brief Function that sets the window according to the item parameters and values
	///
     /// \param __item - pointer to the item that mast be displayed. If it is NULL the item stored in _sitem will be displayed
	/// \return No return value.
     void displayItem(hMultipleValue* __item = NULL);
     
signals:

     /// \brief Signal emited when ok button is pressed
	///
	/// \return No return value.
     void okClicked(void);
     
     /// \brief Signal emited when cancel button is pressed
	///
	/// \return No return value.
     void windowClosed(void);
};
// -----------------------------------------------------------------------------
// This class has not the global object because each object that want to use
// the window of this class must define it own instance

#endif
