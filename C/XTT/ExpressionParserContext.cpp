#include "ExpressionParserContext.h"

#include "XTT_Cell.h"
#include "hType.h"
#include "hTypeList.h"

extern hTypeList *typeList;

ExpressionParserContext::ExpressionParserContext()
{
    createDefaultContext();
}

ExpressionParserContext::ExpressionParserContext(XTT_Cell *cell)
{
    m_Cell = cell;
    contextForCell(cell);
}

void ExpressionParserContext::contextForCell(XTT_Cell *cell)
{
    switch(cell->Context())
    {
    case 1: // decision context
    {
        decisionCellContext(cell->AttrType()->Type());
        break;
    }
    case 4: // action context
    {
        actionCellContext(cell->AttrType()->Type());
        break;
    }
    default:
    {
        createDefaultContext();
    }
    }
}

void ExpressionParserContext::decisionCellContext(hType *elementType)
{
    m_RequiredElementType = elementType;
    m_RequiredTopLevelType = typeList->at(typeList->indexOfName(hpTypesInfo.ptn.at(hpTypesInfo.iopti(BOOLEAN_BASED))));

    m_FunctionsAllowed = false;
    m_AttributesAllowed = false;
    m_MinMaxAllowed = true;
    m_AnyAllowed = true;
    m_NullAllowed = true;
}

void ExpressionParserContext::actionCellContext(hType *elementType)
{
    m_RequiredElementType = elementType;
    m_RequiredTopLevelType = typeList->specialType(VOID_BASED);

    m_FunctionsAllowed = true;
    m_AttributesAllowed = true;
    m_MinMaxAllowed = true;
    m_AnyAllowed = true;
    m_NullAllowed = true;
}

void ExpressionParserContext::stateCellContext(hType *elementType)
{
    m_RequiredElementType = elementType;
    // top level type

    m_FunctionsAllowed = false;
    m_AttributesAllowed = false;
    m_MinMaxAllowed = true;
    m_AnyAllowed = true;
    m_NullAllowed = true;
}

void ExpressionParserContext::createDefaultContext()
{
    m_RequiredTopLevelType = 0;
    m_RequiredElementType = 0;

    m_FunctionsAllowed = true;
    m_AttributesAllowed = true;
    m_MinMaxAllowed = true;
    m_AnyAllowed = true;
    m_NullAllowed = true;
}


