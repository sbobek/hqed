/*
 *	   $Id: MyLineEdit.cpp,v 1.2 2010-01-08 19:47:12 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QCompleter>
#include <QKeyEvent>
#include <QString>
#include <QWidget>
#include <QLineEdit>
#include <QTextCursor>
#include <QScrollBar>
#include <QObject>
#include <QtGui>

#include "MyLineEdit.h"
//---------------------------------------------------------------------------

// #brief Constructor MyLineEdit class.
//
// #param parent Pointer to parent object.
MyLineEdit::MyLineEdit(QWidget* parent)
     :QLineEdit(parent), c(0)
{
}
// -----------------------------------------------------------------------------

// #brief Destructor MyLineEdit class.
MyLineEdit::~MyLineEdit(void)
{
	if(c)
		delete c;
}
// -----------------------------------------------------------------------------

// #brief Function sets active prompt(completer).
//
// #param completer Pointer to prompt(completer).
// #return No return value.
void MyLineEdit::setCompleter(QCompleter *completer)
{
     if(completer)
          QObject::disconnect(c, 0, this, 0);

     c = completer;

     if (!c)
         return;

     c->setWidget(this);
     c->setCompletionMode(QCompleter::PopupCompletion);
     c->setCaseSensitivity(Qt::CaseInsensitive);
     QObject::connect(completer, SIGNAL(activated(const QString&)), this, SLOT(insertCompletion(const QString&)));
}
// -----------------------------------------------------------------------------

// #brief Function gets active prompt(completer).
//
// #return Pointer to active prompt(completer).
QCompleter* MyLineEdit::completer(void) const
{
     return c;
}
// -----------------------------------------------------------------------------

// #brief Function gets edited text.
//
// #return Edited text.
QString MyLineEdit::textUnderCursor(void) const
{
     // Kod alternatywny
     /*int cp = ((QLineEdit*)this)->cursorPosition();
     ((QLineEdit*)this)->cursorWordBackward(true);
     res = selectedText();
     //((QLineEdit*)this)->cursorWordForward(false);
     ((QLineEdit*)this)->setCursorPosition(cp); */
     
     QString res = "";
     static QString eow("~!@#$%^&*()+{}|:\"?,/;'[]\\-= ");
     int cp = ((QLineEdit*)this)->cursorPosition();
     QString t = text();

     while((cp > 0) && (!eow.contains(t[cp-1])))
     {
          res = t[cp-1] + res;
          cp--;
     }
     
     return res;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void MyLineEdit::keyPressEvent(QKeyEvent *e)
{
     if ((c) && (c->popup()->isVisible()))
     {
          switch (e->key())
          {
               case Qt::Key_Enter:
               case Qt::Key_Return:
               case Qt::Key_Escape:
               case Qt::Key_Tab:
               case Qt::Key_Backtab:
                    e->ignore();
                    return;
               default:
                    break;
          }
     }

     // Sprawdzanie czy nacisniety klawisz to nie jest skrot pokazania wszystkich podpowiedzi
     bool isShortcut = (e->key() == Qt::Key_Down);
     if ((!c) || (!isShortcut))
         QLineEdit::keyPressEvent(e);

     const bool ctrlOrShift = e->modifiers() & (Qt::ControlModifier | Qt::ShiftModifier);
     if ((!c) || (ctrlOrShift && e->text().isEmpty()))
         return;

     // Znaczniki konczace slowo
     static QString eow("~!@#$%^&*()+{}|:\"?,/;'[]\\-=");
     bool hasModifier = (e->modifiers() != Qt::NoModifier) && !ctrlOrShift;
     QString completionPrefix = textUnderCursor();

     if((!isShortcut) && ((hasModifier) || (e->text().isEmpty()) || (completionPrefix.length() < 1) || (eow.contains(e->text().right(1)))))
     {
         c->popup()->hide();
         return;
     }

     // Sprawdzanie czy rozpoczynajace sie slowo mozna uzupelnic
     if(completionPrefix != c->completionPrefix())
     {
         c->setCompletionPrefix(completionPrefix);
         c->popup()->setCurrentIndex(c->completionModel()->index(0, 0));
     }

     // Pozycja pokazania elementu
     QRect cr = QRect(0, 10, 10, 10);
     cr.setWidth(c->popup()->sizeHintForColumn(0) + c->popup()->verticalScrollBar()->sizeHint().width());

     // Wywolanie listy
     c->complete(cr);    
}
// -----------------------------------------------------------------------------

// #brief Function inserts a word from completer list.
//
// #param completion Reference to inserting word.
// #return No return value.
void MyLineEdit::insertCompletion(const QString& completion)
{
     int cp = cursorPosition();
     int offset = 0;
     QString part1 = text().mid(0, cp);
     QString part2 = text().mid(cp, text().length()-cp);
     int extra = completion.length() - c->completionPrefix().length();
     //tc.movePosition(QTextCursor::Left);
     //tc.movePosition(QTextCursor::EndOfWord);
     part1 = part1 + completion.right(extra) + part2;
     setText(part1);

     // Jezeli wstawiamy funckje to musimy cofnac kursor o jeden do tylu
     if((completion.length() > 0) && (completion[completion.length()-1] == ')'))
          offset = -1;
     if((completion.length() > 1) && (completion[completion.length()-2] == ','))
          offset = -2;

     setCursorPosition(cp+extra+offset);
}
// -----------------------------------------------------------------------------
