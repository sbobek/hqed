/**
 * \file	StateGraphicsScene.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.11.2009
 * \version	1.0
 * \brief	This file contains class definition that derives element on which scene is drawn.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef STATEGRAPHICSSCENEH
#define STATEGRAPHICSSCENEH
// -----------------------------------------------------------------------------

#include <QGraphicsScene>
#include "MyGraphicsScene.h"
// -----------------------------------------------------------------------------

class QGraphicsView;
class QGraphicsItem;
// -----------------------------------------------------------------------------
/**
* \class 	StateGraphicsScene
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.11.2009
* \brief 	Class definition that derives element on which scene is drawn.
*/
class StateGraphicsScene : public MyGraphicsScene
{
    Q_OBJECT  
	
protected:
     
     /// \brief Function is triggered when RMB is cliced
	///
	/// \param contextMenuEvent - pointer to object with event parameters.
	/// \return No return value.
	void contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent);
     
public:

     /// \brief Constructor MyGraphicsScene class.
	///
	/// \param oParent Pointer to parent object.
	StateGraphicsScene(QObject* oParent = 0);

	/// \brief Destructor MyGraphicsScene class.
	~StateGraphicsScene(void);
     
public slots:

     /// \brief Function adapts scene size to the size of elements.
	///
	/// \return No return value.
	void AdaptSceneSize(void);
};
// -----------------------------------------------------------------------------
#endif
