/**
 * \file	AttributeManagerTableWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 26.10.2009
 * \version	1.0
 * \brief	This file contains class definition that is a Attribute table.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef TABLELISTWIDGETH
#define TABLELISTWIDGETH
// -----------------------------------------------------------------------------

#include <QAction>
#include <QTableWidget>
// -----------------------------------------------------------------------------
/**
* \class 	AttributeManagerTableWidget
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	26.10.2009
* \brief 	Class definition that is controll presents cell error list.
*/
class AttributeManagerTableWidget : public QTableWidget
{
     Q_OBJECT
     
private:

     QAction* actionNew;           ///< Pointer to the action new button in rmb context menu
     QAction* actionEdit;          ///< Pointer to the action edit button in rmb context menu
     QAction* actionDelete;        ///< Pointer to the action delete button in rmb context menu
     QAction* actionSelect;        ///< Pointer to the action select button in rmb context menu
     QAction* actionChaneGroup;    ///< Pointer to the action change group button in rmb context menu
     QAction* actionRefresh;       ///< Pointer to the action refresh button in rmb context menu
     
     QPoint _dragStartPosition;    ///< Used to save the start point of the drag

protected:

     /// \brief Function that is triggered when on RMB click
     ///
     /// \param __event - pointer to thr event object
     /// \return no values returns
     void contextMenuEvent(QContextMenuEvent* __event);
     
     /// \brief Function that is triggered when user clicks twice on widget
     ///
     /// \param __event - pointer to the event object
     /// \return no values returns
     void mouseDoubleClickEvent(QMouseEvent* __event);
     
     /// \brief Function that is triggered when user presses a mouse button on the widget area
     ///
     /// \param __event - pointer to the event object
     /// \return no values returns
     void mousePressEvent(QMouseEvent* __event);
     
     /// \brief Function that is triggered when user moves mouse on the widget area
     ///
     /// \param __event - pointer to the event object
     /// \return no values returns
     void mouseMoveEvent(QMouseEvent* __event);

public:

     /// \brief Constructor AttributeManagerTableWidget class.
     ///
     /// \return parent Pointer to parent object.
     AttributeManagerTableWidget(QWidget* parent = 0);
     
     /// \brief Sets up all the object properities
     ///
     /// \return no values return
     void initObject(void);
     
     /// \brief Function that creates the context menu actions
     ///
     /// \return no values return
     void createActions(void);
     
public slots:
     
     /// \brief Function that is triggered when user triggers new action from the context menu
     ///
     /// \return no values return
     void onRmbActionNewClicked(void);
     
     /// \brief Function that is triggered when user triggers edit action from the context menu
     ///
     /// \return no values return
     void onRmbActionEditClicked(void);
     
     /// \brief Function that is triggered when user triggers delete action from the context menu
     ///
     /// \return no values return
     void onRmbActionDeleteClicked(void);
     
     /// \brief Function that is triggered when user triggers changeGroup action from the context menu
     ///
     /// \return no values return
     void onRmbActionChangeGroupClicked(void);
     
     /// \brief Function that is triggered when user triggers select action from the context menu
     ///
     /// \return no values return
     void onRmbActionSelectClicked(void);
     
     /// \brief Function that is triggered when user triggers refresh action from the context menu
     ///
     /// \return no values return
     void onRmbActionRefreshClicked(void);
     
signals:

     /// \brief Signal that is emitted when user clicks twice on the widget
     ///
     /// \return no values return
     void doubleClicked(void);
     
     /// \brief Signal that is emitted when user triggers new action from the context menu
     ///
     /// \return no values return
     void rmbActionNewClicked(void);
     
     /// \brief Signal that is emitted when user triggers edit action from the context menu
     ///
     /// \return no values return
     void rmbActionEditClicked(void);
     
     /// \brief Signal that is emitted when user triggers delete action from the context menu
     ///
     /// \return no values return
     void rmbActionDeleteClicked(void);
     
     /// \brief Signal that is emitted when user triggers changeGroup action from the context menu
     ///
     /// \return no values return
     void rmbActionChangeGroupClicked(void);
     
     /// \brief Signal that is emitted when user triggers select action from the context menu
     ///
     /// \return no values return
     void rmbActionSelectClicked(void);
     
     /// \brief Signal that is emitted when user triggers refresh action from the context menu
     ///
     /// \return no values return
     void rmbActionRefreshClicked(void);
};
// -----------------------------------------------------------------------------

#endif
