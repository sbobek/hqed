/**
 * \file	MyLineEdit.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	27.05.2007
 * \version	1.15
 * \brief	This file contains class definition that is controll that edits text.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef MyLineEditH
#define MyLineEditH
//---------------------------------------------------------------------------

class QCompleter;
class QKeyEvent;
class QString;
class QWidget;
//---------------------------------------------------------------------------

#include <QLineEdit>
//---------------------------------------------------------------------------
/**
* \class 	MyLineEdit
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	27.05.2007
* \brief 	Class definition that is controll that edits text.
*/
class MyLineEdit : public QLineEdit
{
     Q_OBJECT

private:

	QCompleter *c;	///< Pointer to prompt(completer).

protected:

	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return value.
        void keyPressEvent(QKeyEvent* event);

public:

    /// \brief Constructor MyLineEdit class.
	///
	/// \param parent Pointer to parent object.
	MyLineEdit(QWidget *parent = 0);

	/// \brief Destructor MyLineEdit class.
	~MyLineEdit(void);

	/// \brief Function gets edited text.
	///
	/// \return Edited text.
	QString textUnderCursor(void) const;

	/// \brief Function gets active prompt(completer).
	///
	/// \return Pointer to active prompt(completer).
	QCompleter* completer(void) const;

	/// \brief Function sets active prompt(completer).
	///
	/// \param completer Pointer to prompt(completer).
	/// \return No return value.
	void setCompleter(QCompleter* completer);

private slots:

	/// \brief Function inserts a word from completer list.
	///
	/// \param completion Reference to inserting word.
	/// \return No return value.
	void insertCompletion(const QString &completion);
};
//---------------------------------------------------------------------------
#endif
