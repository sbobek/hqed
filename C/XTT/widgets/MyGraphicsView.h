/**
 * \file	MyGraphicsView.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	12.10.2007
 * \version	1.15
 * \brief	This file contains class definition that derives element that is screen on which scene is drawn.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef MYGRAPHICSVIEWH
#define MYGRAPHICSVIEWH
// -----------------------------------------------------------------------------

#include <QGraphicsView>
#include "../../widgets/GSceneView.h"
#include "../../widgets/hBaseScene.h"
// -----------------------------------------------------------------------------

class QGraphicsView;
// -----------------------------------------------------------------------------
/**
* \class 	MyGraphicsView
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	12.10.2007
* \brief 	Class definition that derives element that is screen on which scene is drawn.
*/
class MyGraphicsView : public GSceneView
{
    Q_OBJECT
public:

    /// \brief Constructor MyGraphicsView class.
    ///
    /// \param oParent Pointer to parent object.
    MyGraphicsView(QWidget* oParent = 0);

    /// \brief Constructs a MyGraphicsView and sets the visualized scene.
    ///
    /// \param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
    /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
    MyGraphicsView(hBaseScene* __scene, QWidget* __parent = 0);

    /// \brief Function is triggered when file is dragged above the window, it also checks file type.
    ///
    /// \param event Parameters of dragged file.
    /// \return No return value.
    void dragEnterEvent(QDragEnterEvent* event);

    /// \brief Function is triggered when file is dropped on the window.
    ///
    /// \param event Parameters of dropped file.
    /// \return No return value.
    void dropEvent(QDropEvent* event);

    /// \brief Function is triggered when dragged file is moved above the window.
    ///
    /// \param event Parameters of dragged file.
    /// \return No return value.
    void dragMoveEvent(QDragMoveEvent* event);

    /// \brief Function is triggered when mouse wheel is changed, file is still dragged.
    ///
    /// \param event Parameters of dragged file.
    /// \return No return value.
    void wheelEvent(QWheelEvent* event);
};
// -----------------------------------------------------------------------------
#endif
