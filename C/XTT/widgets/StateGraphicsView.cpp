/*
 *	   $Id: StateGraphicsView.cpp,v 1.2.4.2 2011-06-06 15:33:33 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "StateGraphicsScene.h"
#include "StateGraphicsView.h"
// -----------------------------------------------------------------------------

// #brief Constructor StateGraphicsView class.
//
// #param oParent Pointer to parent object.
StateGraphicsView::StateGraphicsView(QWidget* oParent) 
: MyGraphicsView(oParent)
{	
	setAcceptDrops(true);
}
// -----------------------------------------------------------------------------

// #brief Constructs a StateGraphicsView and sets the visualized scene.
//
// #param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
StateGraphicsView::StateGraphicsView(hBaseScene* __scene, QWidget* __parent)
: MyGraphicsView(__scene, __parent)
{
     setAcceptDrops(true);
}
// -----------------------------------------------------------------------------
