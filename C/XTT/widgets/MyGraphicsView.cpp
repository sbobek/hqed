/*
 *	   $Id: MyGraphicsView.cpp,v 1.4.4.2 2011-06-06 15:33:33 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "MyGraphicsScene.h"
#include "MyGraphicsView.h"
// -----------------------------------------------------------------------------

// #brief Constructor MyGraphicsView class.
//
// #param oParent Pointer to parent object.
MyGraphicsView::MyGraphicsView(QWidget* oParent) 
: GSceneView(oParent)
{	
	setAcceptDrops(true);
}
// -----------------------------------------------------------------------------

// #brief Constructs a MyGraphicsView and sets the visualized scene.
//
// #param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
MyGraphicsView::MyGraphicsView(hBaseScene* __scene, QWidget* __parent)
: GSceneView(__scene, __parent)
{
     setAcceptDrops(true);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dragged above the window, it also checks file type.
//
// #param event Parameters of dragged file.
// #return No return value.
void MyGraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
     if(event->mimeData()->hasFormat("text/uri-list"))
		event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dropped on the window.
//
// #param event Parameters of dropped file.
// #return No return value.
void MyGraphicsView::dropEvent(QDropEvent *event)
{
	QList<QUrl> urlList = event->mimeData()->urls();
	if(urlList.size() > 0)
	{
		  QString url = urlList.at(0).toLocalFile();
		  MainWin->openFile(url);
	}

     event->acceptProposedAction();
} 
// -----------------------------------------------------------------------------

// #brief Function is triggered when dragged file is moved above the window.
//
// #param event Parameters of dragged file.
// #return No return value.
void MyGraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when mouse wheel is changed, file is still dragged.
//
// #param event Parameters of dragged file.
// #return No return value.
void MyGraphicsView::wheelEvent(QWheelEvent* event)
{
	if(!Settings->useZoomMouseWhell(MainWin->currentMode()))
     {
          QGraphicsView::wheelEvent(event);
		return;
     }
	
	if(event->delta() < 0)
		((MyGraphicsScene*)scene())->zin();
	if(event->delta() > 0)
		((MyGraphicsScene*)scene())->zout();
     //scene->scaleView(pow((double)2, -event->delta() / 960.0));
}
// -----------------------------------------------------------------------------
