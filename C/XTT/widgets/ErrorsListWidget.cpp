/*
 *	   $Id: ErrorsListWidget.cpp,v 1.4 2010-01-08 19:47:11 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QList>
#include <QWidget>
#include <QString>
#include <QListWidget>
#include <QObject>
#include <QtGui>
#include <QMessageBox>
#include <QDialog>

#include "../../../M/XTT/XTT_Row.h"
#include "../../../M/XTT/XTT_Cell.h"
#include "../../../M/XTT/XTT_Table.h"
#include "../../../M/XTT/XTT_States.h"
#include "../../../M/XTT/XTT_TableList.h"
#include "../../../M/XTT/XTT_Statesgroup.h"

#include "../../../V/XTT/GTable.h"
#include "../../../V/XTT/GConnection.h"

#include "../../../namespaces/ns_hqed.h"

#include "ErrorsListWidget.h"
//---------------------------------------------------------------------------

// #brief Constructor ErrorsListWidget class.
//
// #param parent Pointer to parent object.
ErrorsListWidget::ErrorsListWidget(QWidget* parent)
     :QListWidget(parent)
{
	connect(this, SIGNAL(currentRowChanged(int)), this, SLOT(onRowChange(int)));
	connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClick(QListWidgetItem*)));

	icons[0] = ":/all_images/images/critical.png";
	icons[1] = ":/all_images/images/warning.png";
	icons[2] = ":/all_images/images/information.png";
	msgTypes[0] = "error: ";
	msgTypes[1] = "warning: ";
	msgTypes[2] = "information: ";
     
     errorlevels[0] = "Syntax ";
     errorlevels[1] = "Logic ";
     errorlevels[2] = "Other ";

	// Dodanie informacji o liczbie bledow
	int n = fTablesIDs.size();
	if(n == 0)
	{
		addItem(msgTypes[2] + tr("No errors found."));
		item(count()-1)->setIcon(QIcon(icons[2]));
	}
}
// -----------------------------------------------------------------------------

// #brief Separates the messages from the string
//
// #param __msg - string that contain a number of messages, that should be separated.
// #param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
// #return no values return
void ErrorsListWidget::parseMessages(QString __msg, int __messageLevel)
{
     int pos = __msg.indexOf(hqed::errorIndicator(NEW_MESSAGE_INDICATOR));
     while(pos > -1)
     {
          QString stid = hqed::retrieveMessageParts(__msg, MESSAGE_PART_TABLE_ID);
          QString scid = hqed::retrieveMessageParts(__msg, MESSAGE_PART_CELL_ID);
          QString smt = hqed::retrieveMessageParts(__msg, MESSAGE_PART_MSGTYPE);
          QString stc = hqed::retrieveMessageParts(__msg, MESSAGE_PART_MORIGIN);
          QString msg = hqed::retrieveMessageParts(__msg, MESSAGE_PART_MESSAGE);       
          
          pos = __msg.indexOf(hqed::errorIndicator(END_MESSAGE_INDICATOR));
          __msg = __msg.remove(0, pos+hqed::errorIndicator(END_MESSAGE_INDICATOR).size());

          QString _tid = stid;
          QString _cid = scid;
          int _mt = smt.toInt();
          int _tc = stc.toInt();
          
          if((_tc == 2) && (_tid == "") && (_cid == ""))
               add(__messageLevel, msg, _mt, _tc);
          else
               add(__messageLevel, _tid, _cid, msg, _mt, _tc);
               
          pos = __msg.indexOf(hqed::errorIndicator(NEW_MESSAGE_INDICATOR));
     }
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when uer clicks the RMB on the widget area
//
// #param __event - pointer to the object that holds the event parameters
// #return no values return
void ErrorsListWidget::contextMenuEvent(QContextMenuEvent* __event)
{
     QMenu rmb("Context Menu");
     QAction* cpyall = rmb.addAction("Copy all to clipboard");
     QAction* cpysel = rmb.addAction("Copy selected to clipboard");
     rmb.addSeparator();
     QAction* selall = rmb.addAction("Select all");
     QAction* invsel = rmb.addAction("Invert selection");
     
     connect(selall, SIGNAL(triggered()), this, SLOT(onSelectAll()));
     connect(invsel, SIGNAL(triggered()), this, SLOT(onInvertSelection()));
     connect(cpysel, SIGNAL(triggered()), this, SLOT(onCopySelected()));
     connect(cpyall, SIGNAL(triggered()), this, SLOT(onCopyAll()));
     
     rmb.exec(__event->globalPos());
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the select all button from the context menu
//
// #return No return value.
void ErrorsListWidget::onSelectAll(void)
{
     for(int i=0;i<count();++i)
          item(i)->setSelected(true);
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the invert selection button from the context menu
//
// #return No return value.
void ErrorsListWidget::onInvertSelection(void)
{
     for(int i=0;i<count();++i)
          item(i)->setSelected(!item(i)->isSelected());
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the copy all button from the context menu
//
// #return No return value.
void ErrorsListWidget::onCopyAll(void)
{
     QStringList strs;
     for(int i=0;i<count();++i)
          strs << item(i)->text();
          
     QApplication::clipboard()->setText(strs.join("\n"));
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the copy selected button from the context menu
//
// #return No return value.
void ErrorsListWidget::onCopySelected(void)
{
     QStringList strs;
     QList<QListWidgetItem *> selitm = selectedItems();
     
     for(int i=0;i<selitm.size();++i)
          strs << selitm.at(i)->text();
          
     QApplication::clipboard()->setText(strs.join("\n"));
}
// -----------------------------------------------------------------------------

// #brief Function adds new element to the list.
//
// #param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
// #param _err Error message.
// #param _msgType Type of message
// #li 0 - error.
// #li 1 - warning.
// #li 2 - information.
// #param _tc Origin of added element.
// #li 0 - Added element comes from cell.
// #li 1 - Added element comes from connection.
// #li 2 - Added element comes from type or attribute
// #li 3 - Added element comes from table
// #li 4 - Added element comes from table header
// #li 5 - Added element comes from row
// #li 6 - Added element comes from stategroup
// #li 7 - Added element comes from state
// #li 8 - Added element comes from single cell of the state
// #return No return value.
void ErrorsListWidget::add(int __messageLevel, QString _err, int _msgType, int _tc)
{
     if((__messageLevel < 0) || (__messageLevel >= MESSAGES_TYPES_COUNT))
          __messageLevel = MESSAGES_TYPES_COUNT-1;                    // always other
	if((_msgType < 0) || (_msgType >= ERRORS_TYPES_COUNT))
		_msgType = 0;
		
	// Usuwanie ostatniego obiektu ktorym jest info o liczbie bledow
	delete takeItem(count()-1);
	
	fTCs.append(_tc);
	fTablesIDs.append("");
	fCellsIDs.append("");
	fCellsErrors.append(errorlevels[__messageLevel] + msgTypes[_msgType] + _err);
	fCells.append(NULL);
	fELs.append(__messageLevel);
     
	addItem(_err);
	item(count()-1)->setIcon(QIcon(icons[_msgType]));
	
	// Dodanie informacji o liczbie bledow
	int n = fTablesIDs.size();
	addItem(msgTypes[1] + tr("%n error(s) found.", "", n));
	item(count()-1)->setIcon(QIcon(icons[1]));
	QFont ifont = item(count()-1)->font();
	ifont.setBold(true);
	item(count()-1)->setFont(ifont);
	scrollToItem(item(count()-1));
}
// -----------------------------------------------------------------------------

// #brief Function adds new element to the list.
//
// #param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
// #param _tid Table id.
// #param _cid Cell id.
// #param _err Error message.
// #param _msgType Type of message
// #li 0 - error.
// #li 1 - warning.
// #li 2 - information.
// #param _tc Origin of added element.
// #li 0 - Added element comes from cell.
// #li 1 - Added element comes from connection.
// #li 2 - Added element comes from type or attribute
// #li 3 - Added element comes from table
// #li 4 - Added element comes from table header
// #li 5 - Added element comes from row
// #li 6 - Added element comes from stategroup
// #li 7 - Added element comes from state
// #li 8 - Added element comes from single cell of the state
// #return No return value.
void ErrorsListWidget::add(int __messageLevel, QString _tid, QString _cid, QString _err, int _msgType, int _tc)
{
     if((__messageLevel < 0) || (__messageLevel >= MESSAGES_TYPES_COUNT))
          __messageLevel = MESSAGES_TYPES_COUNT-1;               // always other
	if((_msgType < 0) || (_msgType >= ERRORS_TYPES_COUNT))
		_msgType = 0;
	
	// Wyszukanie nazwy tabeli
     __messageLevel = __messageLevel>2?2:__messageLevel;
	int tindex = -1;
     QString tname = "";
     
     // table name
     if((_tc == 0) || (_tc == 1) || (_tc == 3) || (_tc == 4) || (_tc == 5))
     {
          tindex = TableList->IndexOfID(_tid);
     	if(tindex == -1)
     		return;
          tname = TableList->Table(tindex)->Title();
     }
     
     // state group name
     if(_tc == 6)
     {
          tname = _tid;
     }
     
     // state name
     if(_tc == 7)
     {
          tname = _tid;
     }
     
     // state cell
     if(_tc == 8)
     {
          tname = _tid;
     }

	// Wyszukanie numeru wiersza i kolumny gdzie znajduje sie komorka
	int rindex = -1, cindex = -1;
     if((_tc == 0) || (_tc == 1) || (_tc == 5))
     {
     	for(unsigned int r=0;r<TableList->Table(tindex)->RowCount();r++)
     		if(TableList->Table(tindex)->Row(r)->indexOfCellID(_cid) > -1)
     		{	
     			rindex = (int)r;
     			cindex = TableList->Table(tindex)->Row(r)->indexOfCellID(_cid);
     			break;
     		}
     	if((rindex == -1) || (cindex == -1))
     		return;
     }

	// Usuwanie ostatniego obiektu ktorym jest info o liczbie bledow
	delete takeItem(count()-1);

	fTCs.append(_tc);
	fTablesIDs.append(_tid);
	fCellsIDs.append(_cid);
	fCellsErrors.append(_err);
     fELs.append(__messageLevel);
     if((_tc == 0) || (_tc == 1) || (_tc == 5))
          fCells.append(TableList->Table(tindex)->Row(rindex)->Cell(cindex));
     else
          fCells.append(NULL);

     if(_tc == 0)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in table \'" + tname + "\' row " + QString::number(rindex+1) + ", col " + QString::number(cindex+1) + ": " + _err);
     if(_tc == 1)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in table \'" + tname + "\' row " + QString::number(rindex+1) + ": " + _err);
     if(_tc == 2)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + _err);
     if(_tc == 3)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in table \'" + tname + "\': " + _err);
     if(_tc == 4)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in table \'" + tname + "\': " + _err);
     if(_tc == 5)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in table \'" + tname + "\' row " + QString::number(rindex+1) + ": " + _err);
     if(_tc == 6)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in stategroup \'" + tname + "\': " + _err);
     if(_tc == 7)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in state \'" + tname + "\': " + _err);
     if(_tc == 8)
          addItem(errorlevels[__messageLevel] + msgTypes[_msgType] + "in state \'" + tname + "\': " + _err);

     item(count()-1)->setIcon(QIcon(icons[_msgType]));
	
	// Dodanie informacji o liczbie bledow
	int n = fTablesIDs.size();
	addItem(msgTypes[1] + tr("%n error(s) found.", "", n));
	item(count()-1)->setIcon(QIcon(icons[1]));
	QFont ifont = item(count()-1)->font();
	ifont.setBold(true);
	item(count()-1)->setFont(ifont);
	scrollToItem(item(count()-1));
}
// -----------------------------------------------------------------------------

// #brief Function deletes element from list.
//
// #param _tid Table id.
// #param _cid Cell id.
// #param _tc Origin of added element.
// #li 0 - Added element comes from cell.
// #li 1 - Added element comes from connection.
// #return Result of deleting element of table.
// #li True - element was deleted successfully.
// #li False - element was not deleted.
bool ErrorsListWidget::deleteItem(QString _tid, QString _cid, int _tc)
{
     for(int i=fTCs.size()-1;i>=0;--i)
     {
          if((fTCs.at(i) != _tc) || (fTablesIDs.at(i) != _tid) || (fCellsIDs.at(i) != _cid))
               continue;
               
          // Usuwanie z list
     	fTCs.removeAt(i);
     	fCells.removeAt(i);
     	fTablesIDs.removeAt(i);
     	fCellsIDs.removeAt(i);
     	fCellsErrors.removeAt(i);
          fELs.removeAt(i);
          
          // Usuwanie z obiektu
          delete takeItem(i);
     }
	
	// Usuwanie ostatniego obiektu ktorym jest info o liczbie bledow
	delete takeItem(count()-1);
	
	// Dodanie informacji o liczbie bledow
	int n = fTablesIDs.size();
	if(n == 0)
	{
		addItem(msgTypes[2] + tr("No errors found."));
		item(count()-1)->setIcon(QIcon(icons[2]));
	}
	if(n > 0)
	{
		addItem(msgTypes[1] + tr("%n error(s) found.", "", n));
		item(count()-1)->setIcon(QIcon(icons[1]));
	}
	
	return true;
}
// -----------------------------------------------------------------------------

// #brief Function deletes only those messages, that satisfy filter conditions. Filter conditions are satisfied when all conditions are true.
//
// #param __errorType - type of the error that should be removed. When -1 this parameter is omited.
// #param __tid - id of the table that should be removed. When "" this parameter is omited.
// #return No return value.
void ErrorsListWidget::deleteFilter(int __errorType, QString __tid)
{
     for(int i=fTCs.size()-1;i>=0;--i)
     {
          if(((__errorType != -1) && (__errorType != fELs.at(i))) || 
             ((__tid != "") && (__tid != fTablesIDs.at(i))))
               continue;
          
          // Usuwanie z list
     	fTCs.removeAt(i);
     	fCells.removeAt(i);
     	fTablesIDs.removeAt(i);
     	fCellsIDs.removeAt(i);
     	fCellsErrors.removeAt(i);
          fELs.removeAt(i);
          
          // Usuwanie z obiektu
          delete takeItem(i);
     }
	
	// Usuwanie ostatniego obiektu ktorym jest info o liczbie bledow
	delete takeItem(count()-1);
	
	// Dodanie informacji o liczbie bledow
	int n = fTablesIDs.size();
	if(n == 0)
	{
		addItem(msgTypes[2] + tr("No errors found."));
		item(count()-1)->setIcon(QIcon(icons[2]));
	}
	if(n > 0)
	{
		addItem(msgTypes[1] + tr("%n error(s) found.", "", n));
		item(count()-1)->setIcon(QIcon(icons[1]));
	}
}
// -----------------------------------------------------------------------------

// #brief Function deletes all elements from list.
//
// #return No return value.
void ErrorsListWidget::deleteAll(void)
{
	fTCs.clear();				
	fTablesIDs.clear();			
	fCellsIDs.clear();			
	fCells.clear();			
	fCellsErrors.clear();		
	fELs.clear();		
	
	clear();
	addItem(msgTypes[2] + tr("No errors found."));
	item(count()-1)->setIcon(QIcon(icons[2]));
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when row selection is changed.
//
// #param _newRow Index of changed row.
// #return No return value.
void ErrorsListWidget::onRowChange(int _newRow)
{	
	int currSize = fTCs.size();
	
	if((_newRow < 0) || (_newRow >= currSize))
		return;
	
     // Clear selection of elements
	for(int i=0;i<fTCs.size();i++)
     {
          int tindex = TableList->IndexOfID(fTablesIDs.at(i));
                    
          if(fTCs.at(i) == 0)                     // Clear selection of cells
               fCells.at(i)->setSelected(false);
          if((fTCs.at(i) == 1) && (tindex != -1)) // Clear selection of connections
          {
               int row = TableList->Table(tindex)->indexOfRow(fCells.at(i));
               if(row == -1)
                    return;
               QList<XTT_Connections*>* clist = TableList->Table(tindex)->Row(row)->OutConnection();
               for(int c=0;c<clist->size();++c)
                    clist->at(c)->gConnection()->ClearSelection();
               delete clist;
          }
          if((fTCs.at(i) == 3) && (tindex != -1)) // Clear selection of tables
               TableList->Table(tindex)->GraphicTable()->setTableSelection(false);
          if((fTCs.at(i) == 4) && (tindex != -1)) // Clear selection of headers
               TableList->Table(tindex)->GraphicTable()->selectHeader(fCellsIDs.at(i).toInt(), false);
          if((fTCs.at(i) == 5) && (tindex != -1)) // Clear selection of rows
          {
               int row = TableList->Table(tindex)->indexOfRow(fCells.at(i));
               if(row == -1)
                    return;
               TableList->Table(tindex)->Row(row)->setSelected(false);
          }
          if(fTCs.at(i) == 6)                     // Clear selection of stategroups
               States->selectStatesgroup(fTablesIDs.at(i), false);
          if(fTCs.at(i) == 7)                     // Clear selection of states
               States->selectState(fTablesIDs.at(i), false);
          if(fTCs.at(i) == 8)                     // Clear selection of states cells
               States->selectCell(fTablesIDs.at(i), fCellsIDs.at(i), false);
     }
		
	if(_newRow == currSize)
		return;
          
     // Cell selection
     if(fTCs.at(_newRow) == 0)
          fCells.at(_newRow)->setSelected();
          
     // Connection selection
     if(fTCs.at(_newRow) == 1)
     {
          int tindex = TableList->IndexOfID(fTablesIDs.at(_newRow));
          if(tindex == -1)
     		return;
          int row = TableList->Table(tindex)->indexOfRow(fCells.at(_newRow));
          if(row == -1)
               return;
          QList<XTT_Connections*>* clist = TableList->Table(tindex)->Row(row)->OutConnection();
          for(int c=0;c<clist->size();++c)
               clist->at(c)->gConnection()->SelectAll();
          delete clist;
     }
     
     // Table selection
     if(fTCs.at(_newRow) == 3)
     {
          int tindex = TableList->IndexOfID(fTablesIDs.at(_newRow));
     	if(tindex == -1)
     		return;
          TableList->Table(tindex)->GraphicTable()->setTableSelection(true);
     }
     
     // Header selection
     if(fTCs.at(_newRow) == 4)
     {
          int tindex = TableList->IndexOfID(fTablesIDs.at(_newRow));
     	if(tindex == -1)
     		return;
          TableList->Table(tindex)->GraphicTable()->selectHeader(fCellsIDs.at(_newRow).toInt(), true);
     }
     
     // Row selection
     if(fTCs.at(_newRow) == 5)
     {
          int tindex = TableList->IndexOfID(fTablesIDs.at(_newRow));
     	if(tindex == -1)
     		return;
          int row = TableList->Table(tindex)->indexOfRow(fCells.at(_newRow));
          if(row == -1)
               return;
          TableList->Table(tindex)->Row(row)->setSelected(true);
     }
     
     // Stategroup selection
     if(fTCs.at(_newRow) == 6)
     {
          States->selectStatesgroup(fTablesIDs.at(_newRow), true);
     }
     
     // State selection
     if(fTCs.at(_newRow) == 7)
     {
          States->selectState(fTablesIDs.at(_newRow), true);
     }
     
     // Cell state selection
     if(fTCs.at(_newRow) == 8)
     {
          States->selectCell(fTablesIDs.at(_newRow), fCellsIDs.at(_newRow), true);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when item of list is clicked.
//
// #param itm Pointer to clicked item.
// #return No return value.
void ErrorsListWidget::itemClick(QListWidgetItem* itm)
{
	onRowChange(row(itm));
}
// -----------------------------------------------------------------------------
