/**
 * \file	ExecuteMessagesListWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	27.11.2007 
 * \version	1.15
 * \brief	This file contains class definition that is controll presents list of messages shown during start of model.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef EXECUTEMESSAGESLISTWIDGETH
#define EXECUTEMESSAGESLISTWIDGETH
// -----------------------------------------------------------------------------

class QWidget;
class QListWidget;
// -----------------------------------------------------------------------------

#include <QList>
#include <QListWidget>

#include "../../../M/XTT/XTT_Row.h"
#include "../../../M/XTT/XTT_Cell.h"
#include "../../../M/XTT/XTT_Table.h"
// -----------------------------------------------------------------------------
/**
* \class 	ExecMsgListWidget
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	27.11.2007 
* \brief 	Class definition that is controll presents list of messages shown during start of model.
*/
class ExecMsgListWidget : public QListWidget
{
    Q_OBJECT

private:

     QTimer* _timer;

	QList<QString> fTablesIDs;		///< List of table identifiers.
	QList<QString> fRowIDs;			///< List of row identifiers.
	QList<QString> fCellsIDs;		///< List of cell identifiers.
	QList<int> fMessagesTypes;	///< List of types of messages.

	QList<XTT_Cell*> fCells;	///< List of pointers to cells.
	QList<XTT_Row*> fRows;		///< List of pointers to rows.
	QList<XTT_Table*> fTables;	///< List of pointers to tables.
	QList<QString> fMessages;	///< List of messages.

	QString icons[3];			///< Paths to icons.
	QString msgTypes[3];		///< Types of messages.
	
	bool fShowInfo;				///< Determines whether information should be shown.
	bool fShowError;			///< Determines whether errors should be shown.
	bool fShowWrng;				///< Determines whether wornings should be shown.
     
protected:

     /// \brief Function that is triggered when uer clicks the RMB on the widget area
	///
	/// \param __event - pointer to the object that holds the event parameters
     /// \return no values return
     void contextMenuEvent(QContextMenuEvent* __event);
	
public:
	
     /// \brief Constructor ExecMsgListWidget class.
	///
	/// \param parent Pointer to parent object.
	ExecMsgListWidget(QWidget * parent = 0);
	
	/// \brief Function adds new element to the list.
	///
	/// \param _tid Table id.
	/// \param _rid Row id.
	/// \param _cid Cell id.
	/// \param _err Error message.
	/// \param _msgType Type of message
	/// \li 0 - error.
	/// \li 1 - warning.
	/// \li 2 - information.
	/// \return No return value.
	void add(QString _tid, QString _rid, QString _cid, QString _err, int _msgType = 0);
	
	/// \brief Function deletes all elements from list.
	///
	/// \return No return value.
	void deleteAll(void);
	
	/// \brief Function composes content of error message - it adds parameters connected to error.
	///
	/// \param _index Index of message.
	/// \return No return value.
	QString messageContent(int _index);
     
     /// \brief Function that returns the pointer to the timer
	///
	/// \return pointer to the timer
     QTimer* timer(void);
	
public slots:

	/// \brief Function is triggered when selected row is changed.
	///
	/// \param _newRow Index of row.
	/// \return No return value.
	void onRowChange(int _newRow);

	/// \brief Function is triggered when item is clicked.
	///
	/// \param itm Pointer to clicked element.
	/// \return No return value.
	void itemClick(QListWidgetItem* itm);

	/// \brief Function determines whether error messages should be shown.
	///
	/// \param _val Decision.
	/// \li True - error message will be shown.
	/// \li False - error message will not be shown.
	/// \return No return value.
	void showErrors(bool _val);

	/// \brief Function determines whether worning messages should be shown.
	///
	/// \param _val Decision.
	/// \li True - worning message will be shown.
	/// \li False - worning message will not be shown.
	/// \return No return value.
	void showWarnings(bool);

	/// \brief Function determines whether information messages should be shown.
	///
	/// \param _val Decision.
	/// \li True - information message will be shown.
	/// \li False - information message will not be shown.
	/// \return No return value.
	void showInformations(bool);

	/// \brief Function shows messages according to the filter.
	///
	/// \return No return value.
	void applyFilter(void);
     
     /// \brief Function called on timer->timeOut signal
	///
	/// \return No return value.
     void onTimer(void);
     
     /// \brief Function called when play button is pressed
	///
	/// \return No return value.
     void onPlay(void);
     
     /// \brief Function called when stop button is pressed
	///
	/// \return No return value.
     void onStop(void);
     
     /// \brief Function called when pause button is pressed
	///
	/// \return No return value.
     void onPause(void);
     
     /// \brief Function called when stepNext button is pressed
	///
	/// \return No return value.
     void onStepNext(void);
     
     /// \brief Function called when stepBack button is pressed
	///
	/// \return No return value.
     void onStepBack(void);
     
     /// \brief Function called when begin button is pressed
	///
	/// \return No return value.
     void onFirstMsg(void);
     
     /// \brief Function called when end button is pressed
	///
	/// \return No return value.
     void onLastMsg(void);
     
     /// \brief Function called when interval button is pressed
	///
	/// \return No return value.
     void onIntervalChange(void);
     
     /// \brief Function called when user press the select all button from the context menu
	///
	/// \return No return value.
     void onSelectAll(void);
     
     /// \brief Function called when user press the invert selection button from the context menu
	///
	/// \return No return value.
     void onInvertSelection(void);
     
     /// \brief Function called when user press the copy all button from the context menu
	///
	/// \return No return value.
     void onCopyAll(void);
     
     /// \brief Function called when user press the copy selected button from the context menu
	///
	/// \return No return value.
     void onCopySelected(void);
     
signals:

     /// \brief Signal emited when interval of the timer has been changed successfully
	///
	/// \return No return value.
     void intervalChanged(int __interval);
};
// -----------------------------------------------------------------------------

#endif
