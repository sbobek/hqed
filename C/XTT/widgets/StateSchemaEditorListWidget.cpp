/*
 *	   $Id: StateSchemaEditorListWidget.cpp,v 1.2.4.2 2011-01-14 18:12:59 pheld Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../../M/XTT/XTT_Attribute.h"
#include "../../../M/XTT/XTT_AttributeGroups.h"

#include "../../Settings_ui.h"

#include "StateSchemaEditorListWidget.h"
// -----------------------------------------------------------------------------

// #brief Constructor StateSchemaEditorListWidget class.
//
// #param __parent - parent of thr widget
StateSchemaEditorListWidget::StateSchemaEditorListWidget(QWidget* __parent)
: QListWidget(__parent)
{
     initObject();
}
// -----------------------------------------------------------------------------

// #brief Constructor AttributeGroupsTreeWidget class.
//
// #param __attlist - pointer to the list of attributes that the element displays
// #param __parent - parent of thr widget
StateSchemaEditorListWidget::StateSchemaEditorListWidget(QList<XTT_Attribute*>* __attlist, QWidget* __parent)
: QListWidget(__parent)
{
     initObject();
     _attlist = __attlist;
     showList();
}
// -----------------------------------------------------------------------------

// #brief Function that initializes the object parameters
//
// #return No return value.
void StateSchemaEditorListWidget::initObject(void)
{
    setSelectionMode(QAbstractItemView::ExtendedSelection);
     _attlist = NULL;
     _filter = QList<int>() << XTT_ATT_CLASS_RO << XTT_ATT_CLASS_RW << XTT_ATT_CLASS_WO << XTT_ATT_CLASS_ST;
     setAcceptDrops(true);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the list of indexes of chosen items
//
// #return the pointer to the list that contains the indexes of chosen items
QList<int>* StateSchemaEditorListWidget::chosen(void)
{
     QList<int>* result = new QList<int>;
     for(int i=0;i<count();++i)
          if(item(i)->isSelected())
               result->append(item(i)->statusTip().toInt());
               
     return result;
}
// -----------------------------------------------------------------------------

// #brief Function that redisplays the atrribute list
//
// #return no values return
void StateSchemaEditorListWidget::showList(void)
{
     clear();
     if(_attlist == NULL)
          return;
          
     QString (XTT_Attribute::*reffunc)(void) = Settings->attsReferenceByAcronyms() ? &XTT_Attribute::Acronym : &XTT_Attribute::Name;
     for(int i=0;i<_attlist->size();++i)
     {
          if(!_filter.contains((int)_attlist->at(i)->Class()))
              continue;
              
          QListWidgetItem* item = new QListWidgetItem(QIcon(":/all_images/images/NewAttribute.png"), (_attlist->at(i)->*reffunc)(), this, QListWidgetItem::UserType);
          item->setFlags(Qt::ItemIsEnabled  | Qt::ItemIsSelectable /*| Qt::ItemIsUserCheckable*/);
//          item->setCheckState(Qt::Unchecked);
          item->setStatusTip(QString::number(i));
          addItem(item);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that sets the filter of the widget
//
// #param __filter - the list of filter elements
// #return no values return
void StateSchemaEditorListWidget::setFilter(QList<int>& __filter)
{
     _filter = __filter;
     showList();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dragged above the window, it also checks file type.
//
// #param __event Parameters of dragged file.
// #return No return value.
void StateSchemaEditorListWidget::dragEnterEvent(QDragEnterEvent* __event)
{
     if((__event->mimeData()->hasFormat("text/plain")) && (__event->source() != this))
		__event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dropped on the window.
//
// #param __event - Parameters of dropped file.
// #return No return value.
void StateSchemaEditorListWidget::dropEvent(QDropEvent* __event)
{
     // Collecting data
     QString items = __event->mimeData()->text();
     QStringList indexlist = items.split(";", QString::SkipEmptyParts);
     
     QList<int>* sig = new QList<int>;
     for(int i=0;i<indexlist.size();++i)
     {
          bool convok;
          int val = indexlist.at(i).toInt(&convok);
          if(!convok)
               continue;
               
          sig->append(val);
     }
     emit elementsMoved(sig);
     delete sig;
     
     __event->acceptProposedAction();
} 
// -----------------------------------------------------------------------------

// #brief Function is triggered when dragged file is moved above the window.
//
// #param __event Parameters of dragged file.
// #return No return value.
void StateSchemaEditorListWidget::dragMoveEvent(QDragMoveEvent* __event)
{
     if((__event->mimeData()->hasFormat("text/plain")) && (__event->source() != this))
          __event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user presses a mouse button on the widget area
//
// #param __event - pointer to thr event object
// #return no values returns
void StateSchemaEditorListWidget::mousePressEvent(QMouseEvent* __event)
{
     if (__event->button() == Qt::LeftButton)
         _dragStartPosition = __event->pos();
         
     QListWidget::mousePressEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user moves mouse on the widget area
//
// #param __event - pointer to the event object
// #return no values returns
void StateSchemaEditorListWidget::mouseMoveEvent(QMouseEvent* __event)
{
     if(!(__event->buttons() & Qt::LeftButton))
          return;
     if ((__event->pos() - _dragStartPosition).manhattanLength() < QApplication::startDragDistance())
          return;
     if(itemAt(__event->pos()) == NULL)
          return;

     QList<int>* clist = chosen();
     QString mimetext = "";
     if(clist->size() > 0)
          mimetext = QString::number(clist->at(0));
     for(int i=1;i<clist->size();++i)
          mimetext += ";" + QString::number(clist->at(i));
     delete clist;
          
     QDrag* drag = new QDrag(this);
     QMimeData* mimeData = new QMimeData;

     mimeData->setText(mimetext);
     drag->setMimeData(mimeData);

#if QT_VERSION >= 0x040300
     drag->exec();
#elif QT_VERSION < 0x040300
     drag->start();
#endif
}
// -----------------------------------------------------------------------------

