/*
 *	   $Id: AttributeManagerTableWidget.cpp,v 1.5.4.1 2010-11-19 08:17:05 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QList>
#include <QWidget>
#include <QString>
#include <QTableWidget>
#include <QObject>
#include <QtGui>
#include <QMessageBox>
#include <QDialog>

#include "AttributeTableWidgetItem.h"
#include "AttributeManagerTableWidget.h"
//---------------------------------------------------------------------------

// #brief Constructor AttributeManagerTableWidget class.
//
// #param parent Pointer to parent object.
AttributeManagerTableWidget::AttributeManagerTableWidget(QWidget* parent)
     :QTableWidget(parent)
{
     initObject();
}
// -----------------------------------------------------------------------------

// #brief Sets up all the object properities
//
// #param no values return
void AttributeManagerTableWidget::initObject(void)
{
     setObjectName(QString::fromUtf8("tableWidget"));
     setWindowModality(Qt::NonModal);
     setGeometry(QRect(11, 25, 325, 208));
     setEditTriggers(QAbstractItemView::NoEditTriggers);
     setSelectionMode(QAbstractItemView::SingleSelection);
     setSelectionBehavior(QAbstractItemView::SelectRows);
     setTextElideMode(Qt::ElideNone);
     setGridStyle(Qt::SolidLine);
     setSortingEnabled(false);
     setRowCount(0);
     setColumnCount(7);

     QTableWidgetItem *__colItem = new QTableWidgetItem();
     __colItem->setText(QApplication::translate("attributeManager", "Name", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(0, __colItem);

     QTableWidgetItem *__colItem1 = new QTableWidgetItem();
     __colItem1->setText(QApplication::translate("attributeManager", "Acronym", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(1, __colItem1);

     QTableWidgetItem *__colItem2 = new QTableWidgetItem();
     __colItem2->setText(QApplication::translate("attributeManager", "Description", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(2, __colItem2);

     QTableWidgetItem *__colItem3 = new QTableWidgetItem();
     __colItem3->setText(QApplication::translate("attributeManager", "Class", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(3, __colItem3);

     QTableWidgetItem *__colItem4 = new QTableWidgetItem();
     __colItem4->setText(QApplication::translate("attributeManager", "Type", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(4, __colItem4);

     QTableWidgetItem *__colItem5 = new QTableWidgetItem();
     __colItem5->setText(QApplication::translate("attributeManager", "Domain", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(5, __colItem5);

     QTableWidgetItem *__colItem6 = new QTableWidgetItem();
     __colItem6->setText(QApplication::translate("attributeManager", "Current Value", 0, QApplication::UnicodeUTF8));
     setHorizontalHeaderItem(6, __colItem6);
}
// -----------------------------------------------------------------------------

// #brief Function that creates the context menu actions
//
// #return no values return
void AttributeManagerTableWidget::createActions(void)
{
     actionNew = new QAction(QIcon(":/all_images/images/AttributeAdd.png"), "Add new...", NULL);
     actionNew->setStatusTip("Allows for creating a new attribute");
     connect(actionNew, SIGNAL(triggered()), this, SLOT(onRmbActionNewClicked()));
     
     actionEdit = new QAction(QIcon(":/all_images/images/Edit1.png"), "Edit...", NULL);
     actionEdit->setStatusTip("Allows for editing an attribute");
     connect(actionEdit, SIGNAL(triggered()), this, SLOT(onRmbActionEditClicked()));
     
     actionDelete = new QAction(QIcon(":/all_images/images/AttributeDel.png"), "Delete", NULL);
     actionDelete->setStatusTip("Allows for deleting an attribute");
     connect(actionDelete, SIGNAL(triggered()), this, SLOT(onRmbActionDeleteClicked()));
     
     actionSelect = new QAction("Select", NULL);
     actionSelect->setStatusTip("Allows for editing an attribute");
     connect(actionSelect, SIGNAL(triggered()), this, SLOT(onRmbActionSelectClicked()));
     
     actionChaneGroup = new QAction("Change group...", NULL);
     actionChaneGroup->setStatusTip("Allows for change attribute group");
     connect(actionChaneGroup, SIGNAL(triggered()), this, SLOT(onRmbActionChangeGroupClicked()));
     
     actionRefresh = new QAction(QIcon(":/all_images/images/reload.png"), "Refresh list", NULL);
     actionRefresh->setStatusTip("Refreshes the list of attributes");
     connect(actionRefresh, SIGNAL(triggered()), this, SLOT(onRmbActionRefreshClicked()));
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when on RMB click
//
// #param __event - pointer to thr event object
// #return no values returns
void AttributeManagerTableWidget::contextMenuEvent(QContextMenuEvent* __event)
{
     QPoint rmbGlobalPos = __event->globalPos();
     QPoint rmbLocalPos = __event->pos();
     bool emptyPlace = itemAt(rmbLocalPos) == NULL ? true : false;
     
     createActions();
     
     QMenu rmbMenu(NULL);
     rmbMenu.addAction(actionNew);
     if(!emptyPlace)
     {
          rmbMenu.addAction(actionEdit);
          rmbMenu.addAction(actionDelete);
          rmbMenu.addSeparator();
          rmbMenu.addAction(actionSelect);
          rmbMenu.addSeparator();
          rmbMenu.addAction(actionChaneGroup);
     }
     rmbMenu.addSeparator();
     rmbMenu.addAction(actionRefresh);
     
     rmbMenu.exec(rmbGlobalPos);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user clicks twice on widget
//
// #param __event - pointer to thr event object
// #return no values returns
void AttributeManagerTableWidget::mouseDoubleClickEvent(QMouseEvent* /*__event*/)
{
     emit doubleClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user presses a mouse button on the widget area
//
// #param __event - pointer to thr event object
// #return no values returns
void AttributeManagerTableWidget::mousePressEvent(QMouseEvent* __event)
{
     if (__event->button() == Qt::LeftButton)
         _dragStartPosition = __event->pos();
         
     QTableWidget::mousePressEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user moves mouse on the widget area
//
// #param __event - pointer to the event object
// #return no values returns
void AttributeManagerTableWidget::mouseMoveEvent(QMouseEvent* __event)
{
     if(!(__event->buttons() & Qt::LeftButton))
          return;
     if ((__event->pos() - _dragStartPosition).manhattanLength() < QApplication::startDragDistance())
          return;

     QString attID = "";
     AttributeTableWidgetItem* cellitem = (AttributeTableWidgetItem*)itemAt(_dragStartPosition);
     if(cellitem != 0)
          attID = cellitem->attributeID();
          
     QDrag* drag = new QDrag(this);
     QMimeData* mimeData = new QMimeData;

     mimeData->setText(attID);
     drag->setMimeData(mimeData);

#if QT_VERSION >= 0x040300
     drag->exec();
#elif QT_VERSION < 0x040300
     drag->start();
#endif

}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers new action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionNewClicked(void)
{
     emit rmbActionNewClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers edit action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionEditClicked(void)
{
     emit rmbActionEditClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers delete action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionDeleteClicked(void)
{
     emit rmbActionDeleteClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers changeGroup action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionChangeGroupClicked(void)
{
     emit rmbActionChangeGroupClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers select action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionSelectClicked(void)
{
     emit rmbActionSelectClicked();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user triggers refresh action from the context menu
//
// #param no values return
void AttributeManagerTableWidget::onRmbActionRefreshClicked(void)
{
     emit rmbActionRefreshClicked();
}
// -----------------------------------------------------------------------------
