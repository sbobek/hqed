/*
 *	   $Id: AttributeGroupsTreeWidget.cpp,v 1.2 2010-01-08 19:47:11 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../../M/XTT/XTT_AttributeGroups.h"

#include "AttributeGroupsTreeWidget.h"
// -----------------------------------------------------------------------------

// #brief Constructor AttributeGroupsTreeWidget class.
//
// #param __parent - parent of thr widget
AttributeGroupsTreeWidget::AttributeGroupsTreeWidget(QWidget* __parent)
: QTreeWidget(__parent)
{
     setColumnCount(1);
     setHeaderLabels(QStringList() << "Groups");
     setAnimated(true);
     setAcceptDrops(true);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dragged above the window, it also checks file type.
//
// #param __event Parameters of dragged file.
// #return No return value.
void AttributeGroupsTreeWidget::dragEnterEvent(QDragEnterEvent* __event)
{
     if(__event->mimeData()->hasFormat("text/plain"))
		__event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when file is dropped on the window.
//
// #param __event - Parameters of dropped file.
// #return No return value.
void AttributeGroupsTreeWidget::dropEvent(QDropEvent* __event)
{
     QTreeWidgetItem* group = itemAt(__event->pos());
     if(group == NULL)
          return;
     
     // Collecting data
     QString attid = __event->mimeData()->text();
     QString grpname =  group->text(0);
     int destgrpindex = AttributeGroups->IndexOfName(grpname);

     // Checking data correctenss
     if(destgrpindex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Cannot find destination group \'" + grpname + "\'.", QMessageBox::Ok);
          return;
     }
     
     // Moving attribute
     int moveresult = AttributeGroups->moveAttribute(attid, destgrpindex);
     AttributeGroups->Group(destgrpindex)->sort();
     
     // Error handling
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_NAME_OR_ACRONYM_EXISTS)
          QMessageBox::critical(NULL, "HQEd", "The name or acronym already exists in the destination group.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_ATTRIBUTE)
          QMessageBox::critical(NULL, "HQEd", "Attribute cannot be found in it's group.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_SOURCE_GROUP)
          QMessageBox::critical(NULL, "HQEd", "The parent group of attribute cannot be found.", QMessageBox::Ok);
     if(moveresult == XTT_MOVE_ATTRIBUTE_RESULT_CANT_FIND_DESTINATION_GROUP)
          QMessageBox::critical(NULL, "HQEd", "Can't find destination group.", QMessageBox::Ok);
     
     __event->acceptProposedAction();
     
     emit attributeMoved();
} 
// -----------------------------------------------------------------------------

// #brief Function is triggered when dragged file is moved above the window.
//
// #param __event Parameters of dragged file.
// #return No return value.
void AttributeGroupsTreeWidget::dragMoveEvent(QDragMoveEvent* __event)
{
     QTreeWidgetItem* group = itemAt(__event->pos());
     clearSelection();
     
     if((__event->mimeData()->hasFormat("text/plain")) && (group != NULL))
     {
          __event->acceptProposedAction();
          group->setSelected(true);
     }
}
// -----------------------------------------------------------------------------

