/**
 * \file	AttributeGroupsTreeWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	13.11.2009
 * \version	1.0
 * \brief	This file contains class definition that derives element that displays the tree of the xtt attribute groups
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef ATTRIBUTEGROUPSTREEWIDGETH
#define ATTRIBUTEGROUPSTREEWIDGETH
// -----------------------------------------------------------------------------

#include <QTreeWidget>
#include <QTreeWidgetItem>
// -----------------------------------------------------------------------------

/**
* \class 	AttributeTableWidgetItem
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	13.11.2009
* \brief 	Class definition that derives element that displays the tree of the xtt attribute groups
*/
class AttributeGroupsTreeWidget : public QTreeWidget
{
     Q_OBJECT
     
protected:

     /// \brief Function is triggered when file is dragged above the window, it also checks file type.
     ///
     /// \param __event Parameters of dragged file.
     /// \return No return value.
     void dragEnterEvent(QDragEnterEvent* __event);
     
     /// \brief Function is triggered when file is dropped on the window.
     ///
     /// \param __event - Parameters of dropped file.
     /// \return No return value.
     void dropEvent(QDropEvent* __event);
     
     /// \brief Function is triggered when dragged file is moved above the window.
     ///
     /// \param event Parameters of dragged file.
     /// \return No return value.
     void dragMoveEvent(QDragMoveEvent *event);
     
public:

     /// \brief Constructor AttributeGroupsTreeWidget class.
     ///
     /// \param __parent - parent of thr widget
     AttributeGroupsTreeWidget(QWidget* __parent = 0);
     
signals:

     /// \brief Signal that is emited when user moves the attribute to the another group using drag&drop technique
     ///
     /// \return no values return
     void attributeMoved(void);
};
// -----------------------------------------------------------------------------
#endif
