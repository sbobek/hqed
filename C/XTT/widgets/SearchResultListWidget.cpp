/*
 *	   $Id: SearchResultListWidget.cpp,v 1.2 2010-01-08 19:47:12 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QList>
#include <QWidget>
#include <QString>
#include <QListWidget>
#include <QObject>
#include <QtGui>
#include <QMessageBox>
#include <QDialog>

#include "../../../V/XTT/GTable.h"
#include "../../../V/XTT/GConnection.h"

#include "../../../M/XTT/XTT_AttributeGroups.h"
#include "../../../M/XTT/XTT_TableList.h"
#include "../../../M/XTT/XTT_Table.h"
#include "../../../M/XTT/XTT_Row.h"
#include "../../../M/XTT/XTT_Cell.h"

#include "../../../M/hMultipleValue.h"

#include "SearchResultListWidget.h"
//---------------------------------------------------------------------------

// #brief Constructor SearchResultListWidget class.
//
// #param *parent Pointer to parent.
SearchResultListWidget::SearchResultListWidget(QWidget* parent)
     :QListWidget(parent)
{
	connect(this, SIGNAL(currentItemChanged(QListWidgetItem*, QListWidgetItem*)), this, SLOT(onCurrentItemChange(QListWidgetItem*, QListWidgetItem*)));
	connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(onItemClick(QListWidgetItem*)));
	
	fItems = new QStringList;

	icons[0] = ":/all_images/images/NewTabele_16.png";
	icons[1] = ":/all_images/images/New Connection_16.png";
}
// -----------------------------------------------------------------------------

// #brief Destructore SearchResultListWidget class.
SearchResultListWidget::~SearchResultListWidget(void)
{
	delete fItems;
}
// -----------------------------------------------------------------------------

// #brief Adds new element to search results
//
// #param QString _item - String which contains params about result
// #return No return value.
void SearchResultListWidget::add(QString _item)
{
	fItems->append(_item);
	
	// Show no display
	addItem(translateResult(_item));
}
// -----------------------------------------------------------------------------

// #brief Translates the result on the string for show
//
// #param QString _res - search result as string
// #return Item for display
QListWidgetItem* SearchResultListWidget::translateResult(QString _res)
{
	QListWidgetItem* item = new QListWidgetItem;
	QString itemText = "";

	QStringList paramsList = _res.split(";");
	
	// If xtt_table
	if(paramsList.at(0) == "XTT_TABLE")
	{
		item->setIcon(QIcon(icons[0]));
		QString tid = paramsList.at(2);
		int tind = TableList->IndexOfID(tid);
		if(tind == -1)
		{
			item->setText("Can't find table id " + tid);
			return item;
		}
		
		// If found in title
		if(paramsList.at(1) == "TITLE")
			itemText = "Found in table " + TableList->Table(tind)->Title() + " title: " + TableList->Table(tind)->Title();
		
		// If found in description
		if(paramsList.at(1) == "DESCRIPTION")
			itemText = "Found in table " + TableList->Table(tind)->Title() + " description: " + TableList->Table(tind)->Description();
			
		// If found in table header
		if(paramsList.at(1) == "HEADER")
		{
			int col = paramsList.at(3).toInt();
			XTT_Attribute* ca = TableList->Table(tind)->ContextAtribute((unsigned int)col);
			QString shortPath = AttributeGroups->CreatePath(ca) + ca->Acronym();
			QString longPath = AttributeGroups->CreatePath(ca) + ca->Name();
			
			itemText = "Found in table " + TableList->Table(tind)->Title() + " header: column " + QString::number(col+1) + ", header values: " + longPath + " OR " + shortPath;
		}
		
		// If found in table cell
		if(paramsList.at(1) == "CELL")
		{
			int row = paramsList.at(3).toInt();
			int col = paramsList.at(4).toInt();
			QString cnt = TableList->Table(tind)->Row(row)->Cell(col)->Content()->toString();
			
			itemText = "Found in " + TableList->Table(tind)->Title() + " table's cell: row " + QString::number(row+1) + ", col " + QString::number(col+1) + ":"  + cnt;
		}
	}
	
	// If xtt_connection
	if(paramsList.at(0) == "XTT_CONNECTION")
	{
		item->setIcon(QIcon(icons[1]));
		QString cid = paramsList.at(2);
		int cind = ConnectionsList->IndexOfID(cid);
		if(cind == -1)
		{
			item->setText("Can't find connection id " + cid);
			return item;
		}
		
		// If found in label
		if(paramsList.at(1) == "LABEL")
			itemText = "Found in connection " + ConnectionsList->Connection(cind)->Label() + " label: " + ConnectionsList->Connection(cind)->Label();
		
		// If found in description
		if(paramsList.at(1) == "DESCRIPTION")
			itemText = "Found in connection " + ConnectionsList->Connection(cind)->Label() + " description: " + ConnectionsList->Connection(cind)->Description();
			
		// If found in connection's source table
		if(paramsList.at(1) == "SRC_TABLE")
		{
			QString tid = paramsList.at(3);
			int tind = TableList->IndexOfID(tid);
			
			if(tind > -1)
				itemText = "Found in connection " + ConnectionsList->Connection(cind)->Label() + " source table title: " + TableList->Table(tind)->Title();
		}
		
		// If found in connection's destination table
		if(paramsList.at(1) == "DES_TABLE")
		{
			QString tid = paramsList.at(3);
			int tind = TableList->IndexOfID(tid);
			
			if(tind > -1)
				itemText = "Found in connection " + ConnectionsList->Connection(cind)->Label() + " destination table title: " + TableList->Table(tind)->Title();
		}
	}
	
	item->setText(itemText);
	return item;
}
// -----------------------------------------------------------------------------

// #brief Deletes selected item
//
// #param int _ind - index of selected item
// #return true if the item has been removed, otherwise false
bool SearchResultListWidget::deleteItem(int _ind)
{	
	if((_ind < 0) || (_ind >= count()))
		return false;

	fItems->removeAt(_ind);
     
     #if QT_VERSION < 0x040300
     takeItem(_ind); 
     #endif
     
     #if QT_VERSION >= 0x040300
	removeItemWidget(item(_ind));
     #endif
     
	return true;
}
// -----------------------------------------------------------------------------

// #brief Clears results
//
// #return No return value.
void SearchResultListWidget::deleteAll(void)
{	
	// Clear the lastest selection
	int ci = row(currentItem());
	if(ci != -1)
	{
		QString _item = fItems->at(ci);
		selectItem(_item, false);
	}

	fItems->clear();
	clear();
}
// -----------------------------------------------------------------------------

// #brief Method triggered when the item is clicked
//
// #param QListWidgetItem* _item - pointer to clicked item
// #return No return value.
void SearchResultListWidget::onItemClick(QListWidgetItem* _item)
{
	int _newRow = row(_item);
	
	// Selecting new item
	if(_newRow != -1)
	{
		QString item = fItems->at(_newRow);
		selectItem(item, true);
	}
}
// -----------------------------------------------------------------------------

// #brief Method triggered when the current row of object changed
//
// #param QListWidgetItem* current - current item
// #param QListWidgetItem* previous - previous item
// #return No return value.
void SearchResultListWidget::onCurrentItemChange(QListWidgetItem* current, QListWidgetItem* previous)
{	
	int _prvRow = row(previous);
	int _newRow = row(current);
	
	// If the new row is -1 that means the object has been cleared, so there is no previous item on the list
	if(_newRow == -1)
		return;
	
	//QMessageBox::warning(this, tr("HQEd"), "_prvRow = " + QString::number(_prvRow) + ", _newRow = " + QString::number(_newRow));

	// Clear the lastest selection
	if(_prvRow != -1)
	{
		QString _item = fItems->at(_prvRow);
		selectItem(_item, false);
	}

	// Selecting new item
	if(_newRow != -1)
	{
		QString _item = fItems->at(_newRow);
		selectItem(_item, true);
	}
}
// -----------------------------------------------------------------------------

// #brief Selects given item
//
// #param QString _item - item to select
// #param bool _selStat - selection status
// #return No return value.
void SearchResultListWidget::selectItem(QString _item, bool _selStat)
{
	QStringList paramsList = _item.split(";");
	
	// If xtt_table
	if(paramsList.at(0) == "XTT_TABLE")
	{
		QString tid = paramsList.at(2);
		int tind = TableList->IndexOfID(tid);
		if(tind == -1)
			return;
		
		// If found in title ot description
		if((paramsList.at(1) == "TITLE") || (paramsList.at(1) == "DESCRIPTION"))
		{
			TableList->Table(tind)->GraphicTable()->setTableSelection(_selStat);
			if(_selStat)
				TableList->Table(tind)->GraphicTable()->ensureVisible();
		}
			
		// If found in table header
		if(paramsList.at(1) == "HEADER")
		{
			int col = paramsList.at(3).toInt();
			TableList->Table(tind)->GraphicTable()->selectHeader(col, _selStat);
		}
		
		// If found in table cell
		if(paramsList.at(1) == "CELL")
		{
			int row = paramsList.at(3).toInt();
			int col = paramsList.at(4).toInt();
			
			TableList->Table(tind)->Row(row)->Cell(col)->setSelected(_selStat);
		}
	}
	
	// If xtt_connection
	if(paramsList.at(0) == "XTT_CONNECTION")
	{
		QString cid = paramsList.at(2);
		int cind = ConnectionsList->IndexOfID(cid);
		if(cind == -1)
			return;
		
		// If found in title ot description
		if((paramsList.at(1) == "LABEL") || (paramsList.at(1) == "DESCRIPTION") || (paramsList.at(1) == "SRC_TABLE") || (paramsList.at(1) == "DES_TABLE"))
		{
			if(_selStat)
				ConnectionsList->Connection(cind)->gConnection()->SelectAll();
			if(!_selStat)
				ConnectionsList->Connection(cind)->gConnection()->ClearSelection();
		}
	}
}
// -----------------------------------------------------------------------------
