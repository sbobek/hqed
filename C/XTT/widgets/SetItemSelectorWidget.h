/**
 * \file	SetItemSelectorWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	04.11.2009
 * \version	1.0
 * \brief	This file contains class definition that is controll the set item selector list
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef SETITEMSELECTORWIDGETH
#define SETITEMSELECTORWIDGETH
// -----------------------------------------------------------------------------

class QWidget;
// -----------------------------------------------------------------------------

#include <QList>
#include <QListWidget>
// -----------------------------------------------------------------------------
/**
* \class 	SetItemSelectorWidget
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	09.11.2007 
* \brief 	Class definition that is controll the set item selector list
*/
class SetItemSelectorWidget : public QListWidget
{
    Q_OBJECT
    
private:

     bool _singleRequired;

protected:

     /// \brief Function that is triggered when user requests for context menu
	///
	/// \param __event - Pointer to event handler object.
     /// \return no values return
     void contextMenuEvent(QContextMenuEvent* __event);
     
     /// \brief Function that is triggered on mouse event
	///
	/// \param __event - Pointer to event handler object.
     /// \return no values return
     void mousePressEvent(QMouseEvent* __event);
    
public:
	
     /// \brief Constructor SetItemSelectorWidget class.
	///
	/// \param parent Pointer to parent object.
	SetItemSelectorWidget(QWidget * parent = 0);
     
     /// \brief set the mode of the widget
	///
	/// \param __singleRequired - indicates if only one value can be selected
     /// \return no values return
     void setMode(bool __singleRequired);
     
public slots:

     /// \brief Function that selects the selected items
	///
	/// \return no values return
     void selectSelected(void);
     
     /// \brief Function that selects the all items
	///
	/// \return no values return
     void selectAll(void);
     
     /// \brief Function that deselects the selected items
	///
	/// \return no values return
     void deselectSelected(void);
     
     /// \brief Function that deselects the all items
	///
	/// \return no values return
     void deselectAll(void);
     
     /// \brief Function that inverts selection of the selected items
	///
	/// \return no values return
     void invertSelectedState(void);
     
     /// \brief Function that inverts selection of the all items
	///
	/// \return no values return
     void invertAllStates(void);
};
// -----------------------------------------------------------------------------

#endif
