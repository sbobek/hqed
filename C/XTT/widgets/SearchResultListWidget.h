 /**
 * \file	SearchResultListWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007 
 * \version	1.15
 * \brief	This file contains class definition that arranges editor attribute window. CONTROLLER
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef SEARCHRESULTLISTWIDGETH
#define SEARCHRESULTLISTWIDGETH
// -----------------------------------------------------------------------------

class QWidget;
class QListWidget;
// -----------------------------------------------------------------------------

#include <QList>
#include <QListWidget>
// -----------------------------------------------------------------------------

class SearchResultListWidget : public QListWidget
{
    Q_OBJECT

private:

	QStringList* fItems;			// Search results list
	QString icons[2];				// Icons paths
	
public:
	
	/// \brief Constructor SearchResultListWidget class.
	///
	/// \param *parent Pointer to parent.
	SearchResultListWidget(QWidget * parent = 0);
	
	/// \brief Destructore SearchResultListWidget class.
	~SearchResultListWidget(void);
	
	/// \brief Adds new element to search results
	///
        /// \param _item - String which contains params about result
	/// \return No return value.
	void add(QString _item);
	
	/// \brief Deletes selected item
	///
        /// \param _ind - index of selected item
	/// \return true if the item has been removed, otherwise false
	bool deleteItem(int _ind);
	
	/// \brief Clears results
	///
	/// \return No return value.
	void deleteAll(void);
	
	/// \brief Translates the result on the string for show
	///
        /// \param _res - search result as string
	/// \return Item for display
	QListWidgetItem* translateResult(QString _res);
	
	/// \brief Selects given item
	///
        /// \param _item - item to select
        /// \param _selStat - selection status
	/// \return No return value.
	void selectItem(QString _item, bool _selStat);
	
public slots:

	/// \brief Method triggered when the current row of object changed
	///
        /// \param current - current item
        /// \param previous - previous item
	/// \return No return value.
	void onCurrentItemChange(QListWidgetItem* current, QListWidgetItem* previous);
	
	/// \brief Method triggered when the item is clicked
	///
        /// \param _item - pointer to clicked item
	/// \return No return value.
	void onItemClick(QListWidgetItem* _item);
};
// -----------------------------------------------------------------------------

#endif
