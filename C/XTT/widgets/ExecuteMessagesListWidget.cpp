/*
 *	   $Id: ExecuteMessagesListWidget.cpp,v 1.3 2010-01-08 19:47:12 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QList>
#include <QWidget>
#include <QString>
#include <QListWidget>
#include <QObject>
#include <QtGui>
#include <QMessageBox>
#include <QDialog>

#include "../../../M/XTT/XTT_TableList.h"
#include "../../../M/XTT/XTT_Table.h"
#include "../../../M/XTT/XTT_Row.h"
#include "../../../M/XTT/XTT_Cell.h"
#include "../../../V/XTT/GTable.h"
#include "ExecuteMessagesListWidget.h"
//---------------------------------------------------------------------------

// #brief Constructor ExecMsgListWidget class.
//
// #param parent Pointer to parent object.
ExecMsgListWidget::ExecMsgListWidget(QWidget* parent)
     :QListWidget(parent)
{
     _timer = new QTimer(this);
     _timer->setInterval(1000);
     _timer->stop();
     
     connect(this, SIGNAL(currentRowChanged(int)), this, SLOT(onRowChange(int)));
	connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClick(QListWidgetItem*)));
	connect(_timer, SIGNAL(timeout()), this, SLOT(onTimer()));
     
	icons[0] = ":/all_images/images/critical.png";
	icons[1] = ":/all_images/images/warning.png";
	icons[2] = ":/all_images/images/information.png";
	msgTypes[0] = "Error: ";
	msgTypes[1] = "Warning: ";
	msgTypes[2] = "Information: ";
	
	fShowInfo = true;				
	fShowError = true;				
	fShowWrng = true;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when uer clicks the RMB on the widget area
//
// #param __event - pointer to the object that holds the event parameters
// #return no values return
void ExecMsgListWidget::contextMenuEvent(QContextMenuEvent* __event)
{
     QMenu rmb("Context Menu");
     QAction* cpyall = rmb.addAction("Copy all to clipboard");
     QAction* cpysel = rmb.addAction("Copy selected to clipboard");
     rmb.addSeparator();
     QAction* selall = rmb.addAction("Select all");
     QAction* invsel = rmb.addAction("Invert selection");
     
     connect(selall, SIGNAL(triggered()), this, SLOT(onSelectAll()));
     connect(invsel, SIGNAL(triggered()), this, SLOT(onInvertSelection()));
     connect(cpysel, SIGNAL(triggered()), this, SLOT(onCopySelected()));
     connect(cpyall, SIGNAL(triggered()), this, SLOT(onCopyAll()));
     
     rmb.exec(__event->globalPos());
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the select all button from the context menu
//
// #return No return value.
void ExecMsgListWidget::onSelectAll(void)
{
     for(int i=0;i<count();++i)
          item(i)->setSelected(true);
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the invert selection button from the context menu
//
// #return No return value.
void ExecMsgListWidget::onInvertSelection(void)
{
     for(int i=0;i<count();++i)
          item(i)->setSelected(!item(i)->isSelected());
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the copy all button from the context menu
//
// #return No return value.
void ExecMsgListWidget::onCopyAll(void)
{
     QStringList strs;
     for(int i=0;i<count();++i)
          strs << item(i)->text();
          
     QApplication::clipboard()->setText(strs.join("\n"));
}
// -----------------------------------------------------------------------------

// #brief Function called when user press the copy selected button from the context menu
//
// #return No return value.
void ExecMsgListWidget::onCopySelected(void)
{
     QStringList strs;
     QList<QListWidgetItem *> selitm = selectedItems();
     
     for(int i=0;i<selitm.size();++i)
          strs << selitm.at(i)->text();
          
     QApplication::clipboard()->setText(strs.join("\n"));
}
// -----------------------------------------------------------------------------

// #brief Function adds new element to the list.
//
// #param _tid Table id.
// #param _rid Row id.
// #param _cid Cell id.
// #param _err Error message.
// #param _msgType Type of message
// #li 0 - error.
// #li 1 - warning.
// #li 2 - information.
// #return No return value.
void ExecMsgListWidget::add(QString _tid, QString _rid, QString _cid, QString _err, int _msgType)
{
	if((_msgType < 0) || (_msgType > 2))
		return;
		
	// Usuwanie ostatniego obiektu ktorym jest info o liczbie bledow
	delete takeItem(count()-1);
	
	XTT_Cell* cell = NULL;
	XTT_Row* row = NULL;
	XTT_Table* table = NULL;
	QString tname = "";
	
	// Wyszukanie tabeli
	int tindex = TableList->IndexOfID(_tid);
	if(tindex > -1)
	{
		table = TableList->Table(tindex);
		tname = table->Title();
		fTablesIDs.append(_tid);
	}
	
	// Tak czy owak dodoajemy wskaznik do tabeli, aby ilosc elementow w liscie pokrywala sie z liczba wiadomosci
	fTables.append(table);
	if(tindex == -1)
		fTablesIDs.append("");	
	
	// Wyszukanie numeru wiersza gdzie znajduje sie komorka
	int rindex = -1, cindex = -1;
	if((tindex > -1) && (table != NULL))
	{
		for(unsigned int r=0;r<table->RowCount();r++)
			if(table->Row(r)->RowId() == _rid)
			{	
				rindex = (int)r;
				row = table->Row(rindex);
				break;
			}
			
		if(rindex > -1)
			fRowIDs.append(_rid);
		if(rindex == -1)
			fRowIDs.append("");
	}
	if((tindex == -1) || (table == NULL))
		fRowIDs.append("");
	fRows.append(row);
	
	// Wyszukanie numeru kolumny gdzie znajduje sie komorka
	if((_cid != "") && (rindex > -1) && (table != NULL))
	{
		cindex = row->indexOfCellID(_cid);
		cell = row->Cell(cindex);
		
		if(cindex > -1)
			fCellsIDs.append(_cid);
		if(cindex == -1)
			fCellsIDs.append("");
	}
	if((_cid == "") || (rindex == -1) || (table == NULL))
		fCellsIDs.append("");
	fCells.append(cell);
	
	fMessages.append(_err);
	fMessagesTypes.append(_msgType);
	
	// Wyswietlenie informacji
	applyFilter();
}
// -----------------------------------------------------------------------------

// #brief Function deletes all elements from list.
//
// #return No return value.
void ExecMsgListWidget::deleteAll(void)
{
	clear();
	fTablesIDs.clear();		
	fRowIDs.clear();
	fCellsIDs.clear();			
	fMessagesTypes.clear();		
	fCells.clear();		
	fRows.clear();
	fTables.clear();		
	fMessages.clear();	
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when selected row is changed.
//
// #param _newRow Index of row.
// #return No return value.
void ExecMsgListWidget::onRowChange(int _newRow)
{	
	if((_newRow < 0) || (_newRow >= (count()-1)))
		return;
	
	int currSize = fCells.size();
	
	/// Index z listy wiadomosci
	int newRow = -1;
	for(int i=0;i<fMessages.size();i++)
		if(((fShowError) && (fMessagesTypes.at(i) == 0)) || ((fShowWrng) && (fMessagesTypes.at(i) == 1)) || ((fShowInfo) && (fMessagesTypes.at(i) == 2)))
		{
			newRow++;
			if(_newRow == newRow)
			{
				newRow = i;
				break;
			}
		}
	
	if((newRow < 0) || (newRow >= currSize))
		return;
	
	// Usuwanie zaznaczenia z tabel
	for(int i=0;i<fTables.size();i++)
		if(fTablesIDs.at(i) != "")
			fTables.at(i)->GraphicTable()->setTableSelection(false);
			
	// Usuwanie zaznaczenia z komorek i wierszy
	for(int i=0;i<fRows.size();i++)
		if(fRowIDs.at(i) != "")
			fRows.at(i)->setSelected(false);
	//for(int i=0;i<fCells.size();i++)
	//	fCells.at(i)->setSelected(false);
		
	if(fCellsIDs.at(newRow) != "")
		fCells.at(newRow)->setSelected();
		
	if((fRowIDs.at(newRow) != "") && (fCellsIDs.at(newRow) == ""))
		fRows.at(newRow)->setSelected();
		
	if((fTablesIDs.at(newRow) != "") && (fRowIDs.at(newRow) == "") && (fCellsIDs.at(newRow) == ""))
	{	
		fTables.at(newRow)->GraphicTable()->setTableSelection(true);
		fTables.at(newRow)->GraphicTable()->ensureVisible();
	}
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when item is clicked.
//
// #param itm Pointer to clicked element.
// #return No return value.
void ExecMsgListWidget::itemClick(QListWidgetItem* itm)
{
	onRowChange(row(itm));
}
// -----------------------------------------------------------------------------

// #brief Function determines whether error messages should be shown.
//
// #param _val Decision.
// #li True - error message will be shown.
// #li False - error message will not be shown.
// #return No return value.
void ExecMsgListWidget::showErrors(bool _val)
{
	fShowError = _val;
	applyFilter();
}
// -----------------------------------------------------------------------------

// #brief Function determines whether worning messages should be shown.
//
// #param _val Decision.
// #li True - worning message will be shown.
// #li False - worning message will not be shown.
// #return No return value.
void ExecMsgListWidget::showWarnings(bool _val)
{
	fShowWrng = _val;
	applyFilter();
}
// -----------------------------------------------------------------------------

// #brief Function determines whether information messages should be shown.
//
// #param _val Decision.
// #li True - information message will be shown.
// #li False - information message will not be shown.
// #return No return value.
void ExecMsgListWidget::showInformations(bool _val)
{
	fShowInfo = _val;
	applyFilter();
}
// -----------------------------------------------------------------------------

// #brief Function shows messages according to the filter.
//
// #return No return value.
void ExecMsgListWidget::applyFilter(void)
{	
	setCurrentRow(-1);
	clear();
	int typesCount[3] = {0, 0, 0};
	
	for(int i=0;i<fMessages.size();i++)
	{
		typesCount[fMessagesTypes.at(i)]++;
		if(((fShowError) && (fMessagesTypes.at(i) == 0)) || ((fShowWrng) && (fMessagesTypes.at(i) == 1)) || ((fShowInfo) && (fMessagesTypes.at(i) == 2)))
		{
			addItem(messageContent(i));
			item(count()-1)->setIcon(QIcon(icons[fMessagesTypes.at(i)]));
		}
	}
		
	// Dodanie informacji o liczbie bledow
	// Ustalanie koloru czcionki
	QColor colors[3] = {Qt::red, Qt::blue, Qt::darkGreen};
	QColor fontColor;
	int lastIcon = 2;
	for(int i=2;i>=0;i--)
		if(typesCount[i] > 0)
		{
			fontColor = colors[i];
			lastIcon = i;
		}
	
	int n = fMessages.size();
	addItem(msgTypes[2] + QString::number(n) + " messages(s) found:\t" + QString::number(typesCount[0]) + " error(s),\t" + QString::number(typesCount[1]) + " warnings(s),\t"  + QString::number(typesCount[2]) + " informations(s).");
	QFont ifont = item(count()-1)->font();
	item(count()-1)->setIcon(QIcon(icons[lastIcon]));
	item(count()-1)->setFont(ifont);
	item(count()-1)->setForeground(QBrush(fontColor));
	scrollToItem(item(count()-1));
	
	setCurrentRow(0);
}
// -----------------------------------------------------------------------------

// #brief Function composes content of error message - it adds parameters connected to error.
//
// #param _index Index of message.
// #return No return value.
QString ExecMsgListWidget::messageContent(int _index)
{
	if((_index < 0) || (_index >= fMessages.size()))
		return "";
		
		
	// Wyszukanie numeru wiersza gdzie znajduje sie komorka
	int rindex = -1, cindex = -1;
	if(fTablesIDs.at(_index) != "")
	{
		for(unsigned int r=0;r<fTables.at(_index)->RowCount();r++)
			if(fTables.at(_index)->Row(r)->RowId() == fRowIDs.at(_index))
			{	
				rindex = (int)r;
				break;
			}
	}
	
	// Wyszukanie numeru kolumny gdzie znajduje sie komorka
	if((rindex > -1) && (fTablesIDs.at(_index) != ""))
		cindex = fRows.at(_index)->indexOfCellID(fCellsIDs.at(_index));

	// Ustalanie tresci wiadomosci
	QString messageItem = msgTypes[fMessagesTypes.at(_index)];
	
	if(fTablesIDs.at(_index) != "")
		messageItem += "in table \'" + fTables.at(_index)->Title() + "\'";
	if(fRowIDs.at(_index) != "")
		messageItem += ", row " + QString::number(rindex+1);
	if(fCellsIDs.at(_index) != "")
		messageItem += ", col " + QString::number(cindex+1);
	messageItem += ": " + fMessages.at(_index);
	
	return messageItem;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the pointer to the timer
//
// #return pointer to the timer
QTimer* ExecMsgListWidget::timer(void)
{
     return _timer;
}
// -----------------------------------------------------------------------------

// #brief Function called on timer->timeOut signal
//
// #return No return value.
void ExecMsgListWidget::onTimer(void)
{
     int cr = currentRow();
     if((cr < 0) || (cr >= (count()-1)))
     {
          _timer->stop();
          return;
     }
     
     setCurrentRow(cr+1);
}
// -----------------------------------------------------------------------------

// #brief Function called when play button is pressed
//
// #return No return value.
void ExecMsgListWidget::onPlay(void)
{
     _timer->start();
}
// -----------------------------------------------------------------------------

// #brief Function called when stop button is pressed
//
// #return No return value.
void ExecMsgListWidget::onStop(void)
{
     setCurrentRow(0);
     _timer->stop();
}
// -----------------------------------------------------------------------------

// #brief Function called when pause button is pressed
//
// #return No return value.
void ExecMsgListWidget::onPause(void)
{
     if(_timer->isActive())
          _timer->stop();
     else if(!_timer->isActive())
          _timer->start();
}
// -----------------------------------------------------------------------------

// #brief Function called when stepNext button is pressed
//
// #return No return value.
void ExecMsgListWidget::onStepNext(void)
{
     int cr = currentRow();
     if((cr < 0) || (cr >= (count()-1)))
          return;
     
     _timer->stop();
     setCurrentRow(cr+1);
}
// -----------------------------------------------------------------------------

// #brief Function called when stepBack button is pressed
//
// #return No return value.
void ExecMsgListWidget::onStepBack(void)
{
     int cr = currentRow();
     if((cr < 0) || (cr >= (count()-1)))
          return;
     
     _timer->stop();
     setCurrentRow(cr-1);
}
// -----------------------------------------------------------------------------

// #brief Function called when begin button is pressed
//
// #return No return value.
void ExecMsgListWidget::onFirstMsg(void)
{
     setCurrentRow(0);
     _timer->stop();
}
// -----------------------------------------------------------------------------

// #brief Function called when end button is pressed
//
// #return No return value.
void ExecMsgListWidget::onLastMsg(void)
{
     if(count() < 2)
          return;
     
     setCurrentRow(count()-2);
     _timer->stop();
}
// -----------------------------------------------------------------------------

// #brief Function called when interval button is pressed
//
// #return No return value.
void ExecMsgListWidget::onIntervalChange(void)
{
     bool ok;
     int cv = _timer->interval()/10;
     int newVal = QInputDialog::getInteger (this, "Set interval", "New interval (1<sup>-2</sup> s)", cv, 1, 10000, 1, &ok, Qt::Tool | Qt::MSWindowsFixedSizeDialogHint | Qt::WindowShadeButtonHint);
     
     if(!ok)
          return;
          
     _timer->setInterval(newVal*10);
     emit intervalChanged(_timer->interval());
}
// -----------------------------------------------------------------------------
