/**
 * \file	StateGraphicsView.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.11.2009
 * \version	1.0
 * \brief	This file contains class definition that derives element that is screen on which scene is drawn.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef STATEGRAPHICSVIEWH
#define STATEGRAPHICSVIEWH
// -----------------------------------------------------------------------------

#include "../../widgets/hBaseScene.h"
#include "MyGraphicsView.h"
// -----------------------------------------------------------------------------
/**
* \class 	StateGraphicsView
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.11.2009
* \brief 	Class definition that derives element that is screen on which scene is drawn.
*/
class StateGraphicsView : public MyGraphicsView
{
    Q_OBJECT
     
public:

    /// \brief Constructor StateGraphicsView class.
	///
	/// \param oParent Pointer to parent object.
	StateGraphicsView(QWidget* oParent = 0);
     
     /// \brief Constructs a StateGraphicsView and sets the visualized scene.
     ///
     /// \param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
     StateGraphicsView(hBaseScene* __scene, QWidget* __parent = 0);
};
// -----------------------------------------------------------------------------
#endif
