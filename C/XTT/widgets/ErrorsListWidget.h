/**
 * \file	ErrorsListWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	09.11.2007 
 * \version	1.15
 * \brief	This file contains class definition that is controll presents cell error list.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef ERRORSLISTWIDGETH
#define ERRORSLISTWIDGETH
// -----------------------------------------------------------------------------

class QWidget;
class QListWidget;

// -----------------------------------------------------------------------------

#define ERROR_TYPE 0
#define WARNING_TYPE 1
#define INFORMATION_TYPE 2

#define MESSAGES_TYPES_COUNT 3
#define ERRORS_TYPES_COUNT 3
// -----------------------------------------------------------------------------

#include <QList>
#include <QListWidget>

#include "../../../M/XTT/XTT_Cell.h"
// -----------------------------------------------------------------------------
/**
* \class 	ErrorsListWidget
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	09.11.2007 
* \brief 	Class definition that is controll presents cell error list.
*/
class ErrorsListWidget : public QListWidget
{
    Q_OBJECT

private:

	/// List of senders identifiers.
	/// \li 0 - table and cell.
	/// \li 1 - connections.
	QList<int> fTCs;
     
     /// List of error levels
	QList<int> fELs;


	QList<QString> fTablesIDs;		///< List of table identifiers.
	QList<QString> fCellsIDs;		///< List of cell identifiers.
	QList<QString> fCellsErrors;	     ///< List of errors.
	QList<XTT_Cell*> fCells;		     ///< List of pointers to cells.

	
	QString icons[MESSAGES_TYPES_COUNT];	     ///< Paths to icons.
	QString msgTypes[MESSAGES_TYPES_COUNT];	     ///< Types of messages.
     QString errorlevels[ERRORS_TYPES_COUNT];     ///< The error levels
     
     /// \brief Function adds new element to the list.
     ///
     /// \param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
     /// \param _err Error message.
     /// \param _msgType Type of message
     /// \li 0 - error.
     /// \li 1 - warning.
     /// \li 2 - information.
     /// \param _tc Origin of added element.
     /// \li 0 - Added element comes from cell.
     /// \li 1 - Added element comes from connection.
     /// \li 2 - Added element comes from type or attribute
     /// \li 3 - Added element comes from table
     /// \li 4 - Added element comes from table header
     /// \li 5 - Added element comes from row
     /// \return No return value.
     void add(int __messageLevel, QString _err, int _msgType = 0, int _tc = 2);
	
	/// \brief Function adds new element to the list.
	///
     /// \param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
	/// \param _tid Table id.
	/// \param _cid Cell id.
	/// \param _err Error message.
	/// \param _msgType Type of message
	/// \li 0 - error.
	/// \li 1 - warning.
	/// \li 2 - information.
	/// \param _tc Origin of added element.
	/// \li 0 - Added element comes from cell.
	/// \li 1 - Added element comes from connection.
     /// \li 2 - Added element comes from type or attribute
     /// \li 3 - Added element comes from table
     /// \li 4 - Added element comes from table header
     /// \li 5 - Added element comes from row
	/// \return No return value.
	void add(int __messageLevel, QString _tid, QString _cid, QString _err, int _msgType = 0, int _tc = 0);
     
protected:
     
     /// \brief Function that is triggered when uer clicks the RMB on the widget area
	///
	/// \param __event - pointer to the object that holds the event parameters
     /// \return no values return
     void contextMenuEvent(QContextMenuEvent* __event);
	
public:
	
     /// \brief Constructor ErrorsListWidget class.
	///
	/// \param parent Pointer to parent object.
	ErrorsListWidget(QWidget * parent = 0);
     
     /// \brief Separates the messages from the string
	///
	/// \param __msg - string that contain a number of messages, that should be separated.
	/// \param __messageLevel - indicates the message level. HQed generates SYNTAX_ERROR level and HeaRT generates LOGIC_ERROR level.
     /// \return no values return
     void parseMessages(QString __msg, int __messageLevel = 0);
	
	/// \brief Function deletes element from list.
	///
	/// \param _tid Table id.
	/// \param _cid Cell id.
	/// \param _tc Origin of added element.
	/// \li 0 - Added element comes from cell.
	/// \li 1 - Added element comes from connection.
	/// \return Result of deleting element of table.
	/// \li True - element was deleted successfully.
	/// \li False - element was not deleted.
	bool deleteItem(QString _tid, QString _cid, int _tc = 0);
	
	/// \brief Function deletes all elements from list.
	///
	/// \return No return value.
	void deleteAll(void);
     
     /// \brief Function deletes only those messages, that satisfy filter conditions. Filter conditions are satisfied when all conditions are true.
	///
     /// \param __errorType - type of the error that should be removed. When -1 this parameter is omited.
     /// \param __tid - id of the table that should be removed.  When "" this parameter is omited.
	/// \return No return value.
	void deleteFilter(int __errorType = -1, QString __tid = "");
	
public slots:

	/// \brief Function is triggered when row selection is changed.
	///
	/// \param _newRow Index of changed row.
	/// \return No return value.
	void onRowChange(int _newRow);
	
	/// \brief Function is triggered when item of list is clicked.
	///
	/// \param itm Pointer to clicked item.
	/// \return No return value.
	void itemClick(QListWidgetItem* itm);
     
     /// \brief Function called when user press the select all button from the context menu
	///
	/// \return No return value.
     void onSelectAll(void);
     
     /// \brief Function called when user press the invert selection button from the context menu
	///
	/// \return No return value.
     void onInvertSelection(void);
     
     /// \brief Function called when user press the copy all button from the context menu
	///
	/// \return No return value.
     void onCopyAll(void);
     
     /// \brief Function called when user press the copy selected button from the context menu
	///
	/// \return No return value.
     void onCopySelected(void);
};
// -----------------------------------------------------------------------------

#endif
