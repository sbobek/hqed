/*
 *	   $Id: MyGraphicsScene.cpp,v 1.5.4.3 2012-01-02 04:48:33 rkl Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "MyGraphicsScene.h"
#include "namespaces/ns_hqed.h"
#include "M/hModelList.h"
#include "M/hModel.h"
#include "M/XTT/XTT_TableList.h"
#include "M/XTT/XTT_Table.h"
#include "V/XTT/GTable.h"
#include "V/XTT/GRect.h"
// -----------------------------------------------------------------------------

// #brief Constructor MyGraphicsScene class.
//
// #param oParent Pointer to parent object.
MyGraphicsScene::MyGraphicsScene(QObject* oParent) : hBaseScene(oParent)
{	
	fSelectedConnectionID = "";
	fSelectedTableID = "";
    fFocusTableID = "";
    fFocusCellID = "";
    ignoreKeyboard = false;
}
// -----------------------------------------------------------------------------

// #brief Destructor MyGraphicsScene class.
MyGraphicsScene::~MyGraphicsScene(void)
{
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when mouse cursor is moved.
//
// #param mouseEvent Pointer to object with event parameters.
// #return No return value.
void MyGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
    // Jezeli mamy jakis nowy przedmiot to jest on priorytetm do tego aby zawsze byl widoczny
    if((fNewItem) && (items().contains(fNewItem)))
    {
        fNewItem->setPos(mouseEvent->scenePos());
        if(Settings->xttEnabledAutocentering())
            for(int i=0;i<views().count();i++)
                views().at(i)->ensureVisible(fNewItem);
    }
		
    // Jezeli cos chwycilismy myszka
    if((!fNewItem) && (mouseGrabberItem()))
    {
        if(Settings->xttEnabledAutocentering())
            for(int i=0;i<views().count();i++)
                views().at(i)->ensureVisible(mouseGrabberItem());
    }

    QGraphicsScene::mouseMoveEvent(mouseEvent);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when mouse is clicked on the scene.
//
// #param mouseEvent Pointer to object with event parameters.
// #return No return value.
void MyGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{   
	if(fNewItem)
		resetNewItem();

    if(!mouseGrabberItem())
    {
        fFocusTableID = "";
        fFocusCellID = "";
        fSelectedTableID = "";
    }

	QGraphicsScene::mousePressEvent(mouseEvent);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when RMB is cliced
//
// #param contextMenuEvent - pointer to object with event parameters.
// #return No return value.
void MyGraphicsScene::contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent)
{
     QPointF scenepos = contextMenuEvent->scenePos();
     if(items(scenepos).isEmpty())
     {
          QMenu rmbMenu;
          
          QAction* addTable = rmbMenu.addAction(QIcon(":/all_images/images/NewTabele.png"), "Add new table...");
          addTable->setStatusTip("Allows for creating a new table");
          connect(addTable, SIGNAL(triggered()), MainWin, SLOT(TableEditorShow()));
          
          QAction* tableLayout = rmbMenu.addAction("Tables layout");
          tableLayout->setStatusTip("Layout tables");
          connect(tableLayout, SIGNAL(triggered()), MainWin, SLOT(tablesArrangment()));
          
          rmbMenu.addSeparator();
          QAction* hideTables = rmbMenu.addAction("Tables collapsing");
          hideTables->setCheckable(true);
          hideTables->setChecked(Settings->xttTableAutoHide());
          connect(hideTables, SIGNAL(triggered()), Settings, SLOT(changeXttTableAutoHideStatus()));
          
          QAction* showInitTables = rmbMenu.addAction("Display state tables");
          showInitTables->setCheckable(true);
          showInitTables->setChecked(Settings->xttShowInitialTables());
          connect(showInitTables, SIGNAL(triggered()), Settings, SLOT(changeXttInitialTableHideStatus()));
          
          QAction* showAttsAcronyms = rmbMenu.addAction("Attribute reference as acronym");
          showAttsAcronyms->setCheckable(true);
          showAttsAcronyms->setChecked(Settings->attsReferenceByAcronyms());
          connect(showAttsAcronyms, SIGNAL(triggered()), Settings, SLOT(changeXttAttributeReferenceTypeStatus()));
          
          rmbMenu.addSeparator();
          rmbMenu.addMenu(MainWin->getMenu(2));
          
          rmbMenu.addSeparator();
          rmbMenu.addMenu(MainWin->getMenu(5));
          QAction* zoomByWheel = rmbMenu.addAction("Zoom by wheel");
          zoomByWheel->setCheckable(true);
          zoomByWheel->setChecked(Settings->useZoomMouseWhell(MainWin->currentMode()));
          connect(zoomByWheel, SIGNAL(triggered()), Settings, SLOT(changeXttScrollByWheelStatus()));
          
          rmbMenu.addSeparator();
          QAction* printModel = rmbMenu.addAction(QIcon(":/all_images/images/print.png"), "Print");
          printModel->setStatusTip("Allows for printing current model view");
          connect(printModel, SIGNAL(triggered()), MainWin, SLOT(printModel()));
          
          rmbMenu.addSeparator();
          rmbMenu.addMenu(MainWin->getMenu(14));
          
          rmbMenu.exec(QCursor::pos(), addTable);
          return;
     }
     
     QGraphicsScene::contextMenuEvent(contextMenuEvent);
}
// -----------------------------------------------------------------------------

void MyGraphicsScene::keyPressEvent(QKeyEvent* event)
{
    if(fFocusTableID.isEmpty() || fFocusCellID.isEmpty())
    {
        hBaseScene::keyPressEvent(event);
        return;
    }

    XTT_Table *table = hqed::modelList()->activeModel()->xttTableList()->_IndexOfID(fFocusTableID);
    XTT_Cell *cell = table->cell(fFocusCellID);
    if((table == NULL) || (cell == NULL))
        return;

    int idx = cell->CellIdx();
    int maxIdx = table->RowCount() * table->ColCount();

    bool tableToggle = event->modifiers() == Qt::ControlModifier && event->key() == Qt::Key_Tab;
    bool cellToggle = event->key() == Qt::Key_Tab;

    if(!event->text().isEmpty() && !tableToggle && !cellToggle)
    {
        hBaseScene::keyPressEvent(event);
        return;
    }

    if(tableToggle)
    {
        XTT_TableList *tableList = hqed::modelList()->activeModel()->xttTableList();
        unsigned int idx = tableList->IndexOfID(fFocusTableID);
        if(++idx >= tableList->Count())
            idx = 0;

        XTT_Table *nextFocus = tableList->Table(idx);
        if(nextFocus->RowCount() == 0 ||
           nextFocus->ColCount() == 0)
        {
            return;
        }

        fFocusTableID = nextFocus->TableId();
        fSelectedTableID = fFocusTableID;
        fFocusCellID = nextFocus->cell(1)->CellId();

        views().first()->centerOn(nextFocus->Localization());
        clearSelection();
        nextFocus->GraphicTable()->setTableSelection(true);
        nextFocus->GraphicTable()->baseRect()->setSelected(true);
        setFocusItem(nextFocus->GraphicTable()->baseRect());
        update(sceneRect());

        return;
    }

    if(!ignoreKeyboard)
    {
        int newFocusIdx;
        switch(event->key())
        {
        case Qt::Key_Left :
            {
                newFocusIdx = idx - 1;
                break;
            }

        case Qt::Key_Tab:
        case Qt::Key_Right :
            {
                newFocusIdx = idx + 1;
                break;
            }

        case Qt::Key_Up :
            {
                newFocusIdx = idx - table->ColCount();
                break;
            }

        case Qt::Key_Down :
            {
                newFocusIdx = idx + table->ColCount();
                break;
            }
        }

        if(newFocusIdx < 1 || newFocusIdx > maxIdx)
            return;
        else
            fFocusCellID = XTT_Cell::idPrefix() + QString::number(newFocusIdx);

        update();
        return;
    }

    update();
    hBaseScene::keyPressEvent(event);
}
