/*
 *	   $Id: StateGraphicsScene.cpp,v 1.2 2010-01-08 19:47:12 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "StateGraphicsScene.h"
// -----------------------------------------------------------------------------

// #brief Constructor StateGraphicsScene class.
//
// #param oParent Pointer to parent object.
StateGraphicsScene::StateGraphicsScene(QObject* oParent) : MyGraphicsScene(oParent)
{	
}
// -----------------------------------------------------------------------------

// #brief Destructor StateGraphicsScene class.
StateGraphicsScene::~StateGraphicsScene(void)
{
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when RMB is cliced
//
// #param contextMenuEvent - pointer to object with event parameters.
// #return No return value.
void StateGraphicsScene::contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent)
{
     QPointF scenepos = contextMenuEvent->scenePos();
     if(items(scenepos).isEmpty())
     {
          QMenu rmbMenu;
          QMenu rmbView("View");
          rmbView.setTearOffEnabled(true);
          
          QAction* addStateGroup = rmbMenu.addAction("Add new state group...");
          addStateGroup->setStatusTip("Allows for creating a new state group");
          connect(addStateGroup, SIGNAL(triggered()), MainWin, SLOT(onAddNewStateGroup()));
          QAction* addStatesLayout = rmbMenu.addAction("States layout");
          connect(addStatesLayout, SIGNAL(triggered()), MainWin, SLOT(statesArrangment()));
          rmbMenu.addSeparator();
          
          QAction* showAttsAcronyms = rmbMenu.addAction("Attribute reference as acronym");
          showAttsAcronyms->setCheckable(true);
          showAttsAcronyms->setChecked(Settings->attsReferenceByAcronyms());
          connect(showAttsAcronyms, SIGNAL(triggered()), Settings, SLOT(changeXttAttributeReferenceTypeStatus()));
          rmbMenu.addSeparator();
          
          rmbMenu.addMenu(MainWin->getMenu(3));
          rmbMenu.addSeparator();
          
          QAction* zinAction = rmbView.addAction(QIcon(":/all_images/images/ZoomIn.png"), "Zoom in");
          connect(zinAction, SIGNAL(triggered()), this, SLOT(zin()));
          QAction* zoutAction = rmbView.addAction(QIcon(":/all_images/images/ZoomOut.png"), "Zoom out");
          connect(zoutAction, SIGNAL(triggered()), this, SLOT(zout()));
          rmbView.addSeparator();
          QAction* moveItemsLeft = rmbView.addAction(QIcon(":/all_images/images/leftarrow.png"), "Move all left");
          connect(moveItemsLeft, SIGNAL(triggered()), this, SLOT(MoveItemsLeft()));
          QAction* moveItemsRight = rmbView.addAction(QIcon(":/all_images/images/rightarrow.png"), "Move all right");
          connect(moveItemsRight, SIGNAL(triggered()), this, SLOT(MoveItemsRight()));
          QAction* moveItemsUp = rmbView.addAction(QIcon(":/all_images/images/uparrow.png"), "Move all up");
          connect(moveItemsUp, SIGNAL(triggered()), this, SLOT(MoveItemsUp()));
          QAction* moveItemsDown = rmbView.addAction(QIcon(":/all_images/images/downarrow.png"), "Move all down");
          connect(moveItemsDown, SIGNAL(triggered()), this, SLOT(MoveItemsDown()));
          rmbView.addSeparator();
          QAction* adaptscene = rmbView.addAction(QIcon(":/all_images/images/AdaptSceneSize.png"), "Adapt scene size");
          connect(adaptscene, SIGNAL(triggered()), this, SLOT(AdaptSceneSize()));
          rmbView.addSeparator();
          rmbView.addMenu(MainWin->getMenu(3));
          rmbView.addSeparator();
          rmbView.addMenu(MainWin->getMenu(6));
          rmbMenu.addMenu(&rmbView);
          
          QAction* zoomByWheel = rmbMenu.addAction("Zoom by wheel");
          zoomByWheel->setCheckable(true);
          zoomByWheel->setChecked(Settings->useZoomMouseWhell(MainWin->currentMode()));
          connect(zoomByWheel, SIGNAL(triggered()), Settings, SLOT(changeXttScrollByWheelStatus()));
          rmbMenu.addSeparator();
          
          rmbMenu.addSeparator();
          rmbMenu.addMenu(MainWin->getMenu(14));
          
          rmbMenu.exec(QCursor::pos(), addStateGroup);
          return;
     }
     
     QGraphicsScene::contextMenuEvent(contextMenuEvent);
}
// -----------------------------------------------------------------------------

// #brief Function adapts scene size to the size of elements.
//
// #return No return value.
void StateGraphicsScene::AdaptSceneSize(void)
{
     //int egdelen = 300;
     //setSceneRect(-egdelen, -egdelen, egdelen*2, egdelen*2);
     MyGraphicsScene::AdaptSceneSize();
}
// -----------------------------------------------------------------------------
