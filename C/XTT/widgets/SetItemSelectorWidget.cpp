/*
 *	   $Id: SetItemSelectorWidget.cpp,v 1.3 2010-01-08 19:47:12 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
 
#include <QList>
#include <QWidget>
#include <QString>
#include <QListWidget>
#include <QObject>
#include <QtGui>
#include <QMessageBox>
#include <QDialog>

#include "SetItemSelectorWidget.h"
//---------------------------------------------------------------------------

// #brief Constructor SetItemSelectorWidget class.
//
// #param parent Pointer to parent object.
SetItemSelectorWidget::SetItemSelectorWidget(QWidget* parent)
     :QListWidget(parent)
{
     _singleRequired = true;
}
// -----------------------------------------------------------------------------

// #brief set the mode of the widget
//
// #param __singleRequired - indicates if only one value can be selected
// #return no values return
void SetItemSelectorWidget::setMode(bool __singleRequired)
{
     _singleRequired = __singleRequired;
     if(__singleRequired)
          setSelectionMode(QAbstractItemView::SingleSelection);
     if(!__singleRequired)
          setSelectionMode(QAbstractItemView::ExtendedSelection);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered on mouse event
//
// #param __event - Pointer to event handler object.
// #return no values return
void SetItemSelectorWidget::mousePressEvent(QMouseEvent* __event)
{
     if(__event->button() == Qt::RightButton)
          return;
     if((__event->button() == Qt::LeftButton) && (itemAt(__event->pos()) != NULL) && (_singleRequired))
          deselectAll();
     
     QListWidget::mousePressEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user requests for context menu
//
// #param __event - Pointer to event handler object.
// #return no values return
void SetItemSelectorWidget::contextMenuEvent(QContextMenuEvent* __event)
{
     QMenu cm;
     
     QAction* selsel = cm.addAction("Check selected");
     QAction* deselsel = cm.addAction("Uncheck selected");
     QAction* invsel = cm.addAction("Invert selected");
     
     connect(selsel, SIGNAL(triggered()), this, SLOT(selectSelected()));
     connect(deselsel, SIGNAL(triggered()), this, SLOT(deselectSelected()));
     connect(invsel, SIGNAL(triggered()), this, SLOT(invertSelectedState()));
     
     if(!_singleRequired)
     {
          cm.addSeparator();
          QAction* selall = cm.addAction("Check all");
          QAction* deselall = cm.addAction("Uncheck all");
          QAction* invall = cm.addAction("Invert all");
          
          connect(selall, SIGNAL(triggered()), this, SLOT(selectAll()));
          connect(deselall, SIGNAL(triggered()), this, SLOT(deselectAll()));
          connect(invall, SIGNAL(triggered()), this, SLOT(invertAllStates()));
     }

     cm.exec(__event->globalPos());
     
     QWidget::contextMenuEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the selected items
//
// #return no values return
void SetItemSelectorWidget::selectSelected(void)
{
     QList<QListWidgetItem*> si = selectedItems();
     for(int i=0;i<si.size();++i)
          si.at(i)->setCheckState(Qt::Checked);
}
// -----------------------------------------------------------------------------

// #brief Function that deselects the selected items
//
// #return no values return
void SetItemSelectorWidget::deselectSelected(void)
{
     QList<QListWidgetItem*> si = selectedItems();
     for(int i=0;i<si.size();++i)
          si.at(i)->setCheckState(Qt::Unchecked);
}
// -----------------------------------------------------------------------------

// #brief Function that inverts selection of the selected items
//
// #return no values return
void SetItemSelectorWidget::invertSelectedState(void)
{
     QList<QListWidgetItem*> si = selectedItems();
     for(int i=0;i<si.size();++i)
          si.at(i)->setCheckState(si.at(i)->checkState() == Qt::Checked ? Qt::Unchecked : Qt::Checked);
}
// -----------------------------------------------------------------------------

// #brief Function that selects the all items
//
// #return no values return
void SetItemSelectorWidget::selectAll(void)
{
     for(int i=0;i<count();++i)
          item(i)->setCheckState(Qt::Checked);
}
// -----------------------------------------------------------------------------

// #brief Function that deselects the all items
//
// #return no values return
void SetItemSelectorWidget::deselectAll(void)
{
     for(int i=0;i<count();++i)
          item(i)->setCheckState(Qt::Unchecked);
}
// -----------------------------------------------------------------------------

// #brief Function that inverts selection of the all items
//
// #return no values return
void SetItemSelectorWidget::invertAllStates(void)
{
     for(int i=0;i<count();++i)
          item(i)->setCheckState(item(i)->checkState() == Qt::Checked ? Qt::Unchecked : Qt::Checked);
}
// -----------------------------------------------------------------------------
