/*
 *	   $Id: AttributeTableWidgetItem.cpp,v 1.2 2010-01-08 19:47:11 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "AttributeTableWidgetItem.h"
// -----------------------------------------------------------------------------

// #brief Constructor AttributeTableWidgetItem class.
//
// #param __attributeID - id of the attribute
// #param __type - the type of the item
AttributeTableWidgetItem::AttributeTableWidgetItem(QString __attributeID, int __type)
: QTableWidgetItem(__type)
{
     _attributeID = __attributeID;
}
// -----------------------------------------------------------------------------

// #brief Constructor AttributeTableWidgetItem class.
//
// #param __text - the text that wil be displaed in the item
// #param __attributeID - id of the attribute
// #param __type - the type of the item
AttributeTableWidgetItem::AttributeTableWidgetItem(QString __text, QString __attributeID, int __type)
: QTableWidgetItem(__text, __type)
{
     _attributeID = __attributeID;
}
// -----------------------------------------------------------------------------

// #brief Constructor AttributeTableWidgetItem class.
//
// #param __icon - the icon (picture) that wil be displaed in the item
// #param __text - the text that wil be displaed in the item
// #param __attributeID - id of the attribute
// #param __type - the type of the item
AttributeTableWidgetItem::AttributeTableWidgetItem(QIcon __icon, QString __text, QString __attributeID, int __type)
: QTableWidgetItem(__icon, __text, __type)
{
     _attributeID = __attributeID;
}
// -----------------------------------------------------------------------------

// #brief Constructor AttributeTableWidgetItem class.
//
// #param __other - copying contructor
AttributeTableWidgetItem::AttributeTableWidgetItem(AttributeTableWidgetItem& __other)
: QTableWidgetItem(__other)
{
     _attributeID = __other._attributeID;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the id of the attribute
//
// #return the id of the attribute
QString AttributeTableWidgetItem::attributeID(void)
{
     return _attributeID;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the id of the attribute
//
// #param newID - the new id of the attribute
// #return no values return
void AttributeTableWidgetItem::setAttributeID(QString __newID)
{
     _attributeID = __newID;
}
// -----------------------------------------------------------------------------
