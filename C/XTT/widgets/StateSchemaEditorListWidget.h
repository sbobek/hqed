/**
 * \file	StateSchemaEditorListWidget.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	21.12.2009
 * \version	1.0
 * \brief	This file contains class definition that derives element that displays the list of attributes
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef STATESCHEMAEDITORLISTWIDGET
#define STATESCHEMAEDITORLISTWIDGET
// -----------------------------------------------------------------------------

#include <QListWidget>
// ----------------------------------------------------------------------------

class XTT_Attribute;
// -----------------------------------------------------------------------------

/**
* \class 	StateSchemaEditorListWidget
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	21.12.2009
* \brief 	Class definition that derives element that displays the tree of the list of attributes
*/
class StateSchemaEditorListWidget : public QListWidget
{
     Q_OBJECT
     
private:

     QList<int> _filter;                ///< List that contains the elements of the filter
     QPoint _dragStartPosition;         ///< Used to save the start point of the drag
     QList<XTT_Attribute*>* _attlist;   ///< List of attributes

     /// \brief Function that initializes the object parameters
     ///
     /// \return No return value.
     void initObject(void);
     
protected:

     /// \brief Function is triggered when file is dragged above the window, it also checks file type.
     ///
     /// \param __event Parameters of dragged file.
     /// \return No return value.
     void dragEnterEvent(QDragEnterEvent* __event);
     
     /// \brief Function is triggered when file is dropped on the window.
     ///
     /// \param __event - Parameters of dropped file.
     /// \return No return value.
     void dropEvent(QDropEvent* __event);
     
     /// \brief Function is triggered when dragged file is moved above the window.
     ///
     /// \param event Parameters of dragged file.
     /// \return No return value.
     void dragMoveEvent(QDragMoveEvent *event);
     
     /// \brief Function that is triggered when user presses a mouse button on the widget area
     ///
     /// \param __event - pointer to the event object
     /// \return no values returns
     void mousePressEvent(QMouseEvent* __event);
     
     /// \brief Function that is triggered when user moves mouse on the widget area
     ///
     /// \param __event - pointer to the event object
     /// \return no values returns
     void mouseMoveEvent(QMouseEvent* __event);
     
public:

     /// \brief Constructor AttributeGroupsTreeWidget class.
	///
	/// \param __parent - parent of thr widget
	StateSchemaEditorListWidget(QWidget* __parent = 0);
     
     /// \brief Constructor AttributeGroupsTreeWidget class.
	///
	/// \param __attlist - pointer to the list of attributes that the element displays
	/// \param __parent - parent of thr widget
	StateSchemaEditorListWidget(QList<XTT_Attribute*>* __attlist, QWidget* __parent = 0);
     
     /// \brief Function that returns the list of indexes of chosen items
	///
	/// \return the pointer to the list that contains the indexes of chosen items
     QList<int>* chosen(void);
     
     /// \brief Function that sets the filter of the widget
	///
     /// \param __filter - the list of filter elements
	/// \return no values return
     void setFilter(QList<int>& __filter);
     
public slots:

     /// \brief Function that redisplays the atrribute list
	///
	/// \return no values return
     void showList(void);
     
signals:
     
     /// \brief Signal emited when the object receives the items from the second list
     ///
     /// \param __listindex - pointer to the list of indexes of moved elements
     /// \return no values return
     void elementsMoved(QList<int>* __listindex);
};
// -----------------------------------------------------------------------------
#endif
