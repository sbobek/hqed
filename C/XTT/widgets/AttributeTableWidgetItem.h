/**
 * \file	AttributeTableWidgetItem.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	12.11.2009
 * \version	1.0
 * \brief	This file contains class definition that derives element that a item of the tableWidget in attributeManager
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef ATTRBUTETABLEWIDGETITEMH
#define ATTRBUTETABLEWIDGETITEMH
// -----------------------------------------------------------------------------

#include <QTableWidgetItem>
// -----------------------------------------------------------------------------

/**
* \class 	AttributeTableWidgetItem
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	13.11.2009
* \brief 	Class definition that derives element that a item of the tableWidget in attributeManager.
*/
class AttributeTableWidgetItem : public QTableWidgetItem
{
    
private:

     QString _attributeID;
     
public:

     /// \brief Constructor AttributeTableWidgetItem class.
     ///
     /// \param __attributeID - id of the attribute
     /// \param __type - the type of the item
     AttributeTableWidgetItem(QString __attributeID, int __type = QTableWidgetItem::Type);
     
     /// \brief Constructor AttributeTableWidgetItem class.
     ///
     /// \param __text - the text that wil be displaed in the item
     /// \param __attributeID - id of the attribute
     /// \param __type - the type of the item
     AttributeTableWidgetItem(QString __text, QString __attributeID, int __type = QTableWidgetItem::Type);
     
     /// \brief Constructor AttributeTableWidgetItem class.
     ///
     /// \param __icon - the icon (picture) that wil be displaed in the item
     /// \param __text - the text that wil be displaed in the item
     /// \param __attributeID - id of the attribute
     /// \param __type - the type of the item
     AttributeTableWidgetItem(QIcon __icon, QString __text, QString __attributeID, int __type = QTableWidgetItem::Type);
     
     /// \brief Constructor AttributeTableWidgetItem class.
     ///
     /// \param __other - copying contructor
     AttributeTableWidgetItem(AttributeTableWidgetItem& __other);
     
     /// \brief Function that returns the id of the attribute
     ///
     /// \return the id of the attribute
     QString attributeID(void);
     
     /// \brief Function that sets the id of the attribute
     ///
     /// \param __newID - the new id of the attribute
     /// \return no values return
     void setAttributeID(QString __newID);
};
// -----------------------------------------------------------------------------
#endif
