/**
 * \file	MyGraphicsScene.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	12.10.2007
 * \version	1.15
 * \brief	This file contains class definition that derives element on which scene is drawn.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef MYGRAPHICSSCENEH
#define MYGRAPHICSSCENEH
// -----------------------------------------------------------------------------

#include "C/widgets/hBaseScene.h"
// -----------------------------------------------------------------------------

class QGraphicsView;
class QGraphicsItem;
// -----------------------------------------------------------------------------
/**
* \class 	MyGraphicsScene
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	12.10.2007
* \brief 	Class definition that derives element on which scene is drawn.
*/
class MyGraphicsScene : public hBaseScene
{
    Q_OBJECT
    
private:

    QString fSelectedTableID;		///< Identifier of selected table.
    QString fSelectedConnectionID;	///< Identifier of selected connection.
    QString fFocusTableID;          ///< Identifier of the focused table
    QString fFocusCellID;           ///< Identifier of the focused cell

protected:

    /// \brief Function is triggered when mouse cursor is moved.
    ///
    /// \param mouseEvent Pointer to object with event parameters.
    /// \return No return value.
    void mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent);

    /// \brief Function is triggered when mouse is clicked on the scene.
    ///
    /// \param mouseEvent Pointer to object with event parameters.
    /// \return No return value.
    void mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent);

    /// \brief Function is triggered when RMB is cliced
    ///
    /// \param contextMenuEvent - pointer to object with event parameters.
    /// \return No return value.
    void contextMenuEvent(QGraphicsSceneContextMenuEvent* contextMenuEvent);

    void keyPressEvent(QKeyEvent* event);

public:
    bool ignoreKeyboard;

    /// \brief Constructor MyGraphicsScene class.
    ///
    /// \param oParent Pointer to parent object.
    MyGraphicsScene(QObject* oParent = 0);

    /// \brief Destructor MyGraphicsScene class.
    ~MyGraphicsScene(void);

    /// \brief Function sets selected table
    ///
    /// \param _id Identifier of table.
    /// \return No return value.
    void setSelectedTableID(QString _id){ fSelectedTableID = _id; }

    /// \brief Function gets identifier of selected table.
    ///
    /// \return Identifier of selected table.
    QString SelectedTableID(void){ return fSelectedTableID; }

    /// \brief Function sets selected connection.
    ///
    /// \param _id Identifier of connection.
    /// \return No return value.
    void setSelectedConnectionID(QString _id){ fSelectedConnectionID = _id; }

    /// Zwraca wartosc aktualnie wybranego polaczenia

    /// \brief Function gets identifier of selected connection.
    ///
    /// \return Identifier of selected connection.
    QString SelectedConnectionID(void){ return fSelectedConnectionID; }

    /// \brief Function setting identifier of table that has focus.
    void setFocusTableID(QString _id) { fFocusTableID = _id; fSelectedTableID = _id; }

    /// \brief Function returning identifier of the table that has focus.
    ///
    /// \return Identifier of table that has focus.
    QString FocusTableID(void) { return fFocusTableID; }

    void setFocusCellID(QString _id) { fFocusCellID = _id; }
    QString FocusCellID(void) { return fFocusCellID; }
};
// -----------------------------------------------------------------------------
#endif
