/*
 *	   $Id: DeleteConnection_ui.cpp,v 1.33 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_ConnectionsList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"

#include "../../namespaces/ns_hqed.h"
#include "TableEditor_ui.h"
#include "DeleteConnection_ui.h"
// -----------------------------------------------------------------------------

DeleteConnection_ui* DelConn;
// -----------------------------------------------------------------------------

// #brief Constructor DeleteConnection_ui class.
//
// #param parent Pointer to parent object.
DeleteConnection_ui::DeleteConnection_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickDelete()));
    connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));
    connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_2(int)));
    connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_3(int)));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);

    ReadTables();
}
// -----------------------------------------------------------------------------

// #brief Constructor DeleteConnection_ui class.
//
// #param tindex Index of table in which you want to delete connection.
// #param rindex Index of table row from which you want to delete connection.
// #param cindex Index of connection you want to delete.
// #param parent Pointer to parent object.
DeleteConnection_ui::DeleteConnection_ui(int tindex, int rindex, int cindex, QWidget*)
{
	setupUi(this);
	setWindowIcon(QIcon(":/all_images/images/mainico.png"));
    
	connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickDelete()));
	connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange(int)));
	connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_2(int)));
	connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(ComboBoxChange_3(int)));
	setWindowFlags(Qt::WindowMaximizeButtonHint);
     hqed::centerWindow(this);
    
	fIndex = cindex;

	ReadTables();
	comboBox->setCurrentIndex(tindex);
	comboBox_2->setCurrentIndex(rindex);
    
	XTT_Connections* currconn = ConnectionsList->Connection(cindex);
	if(currconn == NULL)
		return;
	
	QList<XTT_Connections*>* cl = ConnectionsList->_IndexOfOutConnections(currconn->SrcTable(), currconn->SrcRow());
	comboBox_3->setCurrentIndex(cl->indexOf(currconn));
	delete cl;
}
// -----------------------------------------------------------------------------

// #brief Function reads titles from tables.
//
// #return No return value.
void DeleteConnection_ui::ReadTables(void)
{
     comboBox->clear();
     comboBox->addItems(TableList->TablesTitles());
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select table field is changed.
//
// #param new_index Index of table.
// #return No return value.
void DeleteConnection_ui::ComboBoxChange(int new_index)
{
     if(new_index == -1)
          return;

     comboBox_2->clear();
     
     // Pobranie indexu tabeli
     int tindex = new_index;
     if(tindex == -1)
          return;

     // Wczytywanie wierszy
     for(unsigned int i=0;i<TableList->Table(tindex)->RowCount();i++)
     {
          QString comment = " - CONNECTED";
          QString tid = TableList->Table(tindex)->TableId();
          QString rid = TableList->Table(tindex)->Row(i)->RowId();
          if(!ConnectionsList->ExistsOutConnections(tid, rid))
               comment = " - NOT CONNECTED";

          comboBox_2->addItem(QString::number(i+1) + ". Row ID = " + TableList->Table(tindex)->Row(i)->RowId() + comment);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when table select row field is changed.
//
// #param new_index Index of row.
// #return No return value.
void DeleteConnection_ui::ComboBoxChange_2(int new_index)
{
     if(new_index == -1)
     {
          pushButton->setEnabled(false);
          return;
     }

     int cti = comboBox->currentIndex();
     int cri = new_index;

     // Jezeli nie poprawne indexy to blokujemy elemnty
     if((cti == -1) || (cri == -1))
          pushButton->setDisabled(true);

     // Pobranie i elementow
     QString tid = TableList->Table(cti)->TableId();
     QString rid = TableList->Table(cti)->Row(cri)->RowId();
     //int conn_index = fIndex;
	
	// Wypisywanie polaczen z danego wiersza
	comboBox_3->clear();
	QList<XTT_Connections*>* conn_list = ConnectionsList->_IndexOfOutConnections(tid, rid);
	for(int c=0;c<conn_list->size();++c)
		comboBox_3->addItem(conn_list->at(c)->ConnId() + "; " + conn_list->at(c)->Label());
	delete conn_list;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when select connection field is changed.
//
// #param new_index Index of connection.
// #return No return value.
void DeleteConnection_ui::ComboBoxChange_3(int new_index)
{
     if(new_index == -1)
     {
          pushButton->setEnabled(false);
          return;
     }

     int cti = comboBox->currentIndex();
     int cri = comboBox_2->currentIndex();
     int cci = new_index;

     // Odblokowanie elementow
     pushButton->setEnabled(true);

     // Jezeli nie poprawne indexy to blokujemy elemnty
     if((cti == -1) || (cri == -1) || (cci == -1))
          pushButton->setDisabled(true);

     // Pobranie i elementow
     //int tid = TableList->Table(cti)->TableId();
     //int rid = TableList->Table(cti)->Row(cri)->RowId();
	
	// Wczytywanie identyfikatora polaczenia
	QString text = comboBox_3->itemText(new_index);
	QStringList tlist = text.split(";", QString::SkipEmptyParts, Qt::CaseSensitive);
	if(tlist.size() == 0)
		return;
	QString cid = tlist.at(0);
     int conn_index = ConnectionsList->IndexOfID(cid);
     fIndex = conn_index;

     // Sprawdzanie czy dane polaczenie istnieje
     if(conn_index == -1)
     {
          pushButton->setDisabled(true);
          return;
     }

     // Wyszukiwanie indexow elemetow polaczenia
     QString dtid = ConnectionsList->Connection(conn_index)->DesTable();
     QString drid = ConnectionsList->Connection(conn_index)->DesRow();
     int dti = TableList->IndexOfID(dtid);
     int dri = -1;
     if(dti == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect table ID.", QMessageBox::Ok);
          return;
     }
     // Wyszukiwanie indexu wiersza
     for(unsigned int i=0;i<TableList->Table(dti)->RowCount();i++)
          if(TableList->Table(dti)->Row(i)->RowId() == drid)
          {
               dri = i;
               break;
          }
     if((dri == -1) && (!drid.isEmpty()))
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect row ID.", QMessageBox::Ok);
          return;
     }

     // Wpisujemy wsrtosci w pola okienka
     QString cut = "No";
     if(ConnectionsList->Connection(conn_index)->IsCut())
          cut = "Yes";

     label_6->setText(TableList->Table(dti)->Title());
     if(dri == -1)
          label_9->setText("No destination rule. This is Rule-Table connection.");
     if(dri > -1)
          label_9->setText(QString::number(dri+1) + ". Row ID = " + TableList->Table(dti)->Row(dri)->RowId());
     label_10->setText(ConnectionsList->Connection(conn_index)->Label());
     label_11->setText(ConnectionsList->Connection(conn_index)->Description());
     label_12->setText(cut);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when button 'Delete' is pressed - it deletes connection.
//
// #return No return value.
void DeleteConnection_ui::ClickDelete(void)
{
     if((fIndex < 0) || ((unsigned int)fIndex >= ConnectionsList->Count()))
     {
          QMessageBox::critical(NULL, "HQEd", "There is no connections selected.", QMessageBox::Ok);
          return;
     }

     if(QMessageBox::question(NULL, "HQEd", "Do you really want to delete selected connection?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Skasowanie polaczenia
     ConnectionsList->Delete(fIndex);

     // Ponowne wczytanie tabeli
     ReadTables();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void DeleteConnection_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               ClickDelete();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
