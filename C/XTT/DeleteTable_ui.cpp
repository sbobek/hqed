/*
 *	   $Id: DeleteTable_ui.cpp,v 1.32 2010-01-08 19:47:04 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"

#include "../../namespaces/ns_hqed.h"
#include "DeleteTable_ui.h"
// -----------------------------------------------------------------------------

DeleteTable_ui* DelTable;
// -----------------------------------------------------------------------------

// #brief Constructor DeleteTable_ui class.
//
// #param parent Pointer to parent object.
DeleteTable_ui::DeleteTable_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickDelete()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
    
    SetWindow();
}
// -----------------------------------------------------------------------------

// #brief Constructor DeleteTable_ui class.
//
// #param tableIndex Index of table you want to delete.
// #param parent Pointer to parent object.
DeleteTable_ui::DeleteTable_ui(int tableindex, QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(clicked()), this, SLOT(ClickDelete()));
    setWindowFlags(Qt::WindowMaximizeButtonHint);
    hqed::centerWindow(this);
    
    SetWindow();
    listWidget->setCurrentRow(tableindex);
}
// -----------------------------------------------------------------------------

// #brief Function sets window and reads information about tables.
//
// #return No return value.
void DeleteTable_ui::SetWindow(void)
{
     // Wczytywanie informacji o tabelach
     listWidget->clear();

     for(unsigned int i=0;i<TableList->Count();i++)
     {
          QString title = TableList->Table(i)->Title();
          QString desc = TableList->Table(i)->Description();
          QString line = title;
          if(desc != "")
               line += " - [" + desc + "]";
          listWidget->addItem(line);
     }
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when table is deleted - it also updates window.
//
// #param event Pointer to event object.
// #return No return value.
void DeleteTable_ui::ClickDelete(void)
{
     int row = listWidget->currentRow();

     // Jezeli nic nie wybrane
     if(row == -1)
          return;

     // Potwierdzenie usuniecia
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to remove selected table?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No) 
          return;

     // Usuwanie
     TableList->Delete(row);

     // Odswiezenie okienka
     SetWindow();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void DeleteTable_ui::keyPressEvent(QKeyEvent *event)
{
     switch (event->key())
     {
          case Qt::Key_Escape:
               close();
               break;

          case Qt::Key_Return:
               ClickDelete();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
