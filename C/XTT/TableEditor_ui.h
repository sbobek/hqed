/**
 * \file	TableEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.15
 * \brief	This file contains class definition that arranges table editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef TABLEEDITOR_H
#define TABLEEDITOR_H
// -----------------------------------------------------------------------------
 
#include "ui_TableEditor.h"
// -----------------------------------------------------------------------------
/**
* \class 	TableEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	28.04.2007
* \brief 	Class definition that arranges table editor window.
*/
class TableEditor_ui : public QWidget, private Ui_TableEditor
{
    Q_OBJECT

private:

     bool fNew;		///< Determines whether new table is created(True) or not(False).
     int fIndex;	///< Index of edited table.

     QStringList clist;	///< List of context names.

public:

    /// \brief Constructor TableEditor_ui class.
	///
	/// \param parent Pointer to parent object.
	TableEditor_ui(QWidget *parent = 0);

    /// \brief Constructor TableEditor_ui class.
	///
	/// \param _new Determines whether new table is created(True) or not(False).
	/// \param _index Index of edited table.
	/// \param parent Pointer to parent object.
	TableEditor_ui(bool _new, int _index, QWidget *parent = 0);
     
     /// \brief Function that reloads the list of tables
	///
     /// \param __destinationIndex - destination index of the combobox
	/// \return No return value.
     void reloadTables(int __destinationIndex);

	/// \brief Function sets the window.
	///
	/// \return No return value.
	void SetWindow(void);
     
     /// \brief Function that saves the changes
	///
	/// \return true if everything is ok, otherwise false
     bool applyChanges(void);

 	/// \brief Function is triggered when key is pressed.
	///
	/// \param event Pointer to event object.
	/// \return No return valu
	void keyPressEvent(QKeyEvent* event);

public slots:

 	/// \brief Function is triggered when button 'Apply' is pressed.
	///
	/// \return No return value.
	void ClickApply(void);
     
     /// \brief Function is triggered when button 'Ok' is pressed.
	///
	/// \return No return value.
	void ClickOk(void);

 	/// \brief Function is triggered when button 'Cancel' is pressed.
	///
	/// \return No return value.
	void ClickCancel(void);

 	/// \brief Function shows edit column window in adding new column mode.
	///
	/// \return No return value.
	void NewCol(void);

 	/// \brief Function shows edit column window.
	///
	/// \return No return value.
	void EditCol(void);

 	/// \brief Function is triggered when new column is added to the table.
	///
	/// \return No return value.
	void AddColumnEvent(void);

 	/// \brief Function is triggered when changes of column edit are commited.
	///
	/// \return No return value.
	void EditColumnEvent(void);

 	/// \brief Function deletes column from the table.
	///
	/// \return No return value.
	void DeleteCol(void);

 	/// \brief Function undoes deleting column.
	///
	/// \return No return value.
	void UnDelete(void);

 	/// \brief Function moves chosen row up in the table.
	///
	/// \return No return value.
	void MoveUp(void);

 	/// \brief Function moves chosen row down in the table.
	///
	/// \return No return value.
	void MoveDown(void);

 	/// \brief Function is triggered when selected table is changed.
	///
	/// \param c_index Index of selected table.
	/// \return No return value.
	void SelectedTableChange(int c_index);

signals:

 	/// \brief Function is triggered when window is closed.
	///
	/// \return No return value.
	void onClose(void);
};
// -----------------------------------------------------------------------------

extern TableEditor_ui* TableEdit;
// -----------------------------------------------------------------------------

#endif
