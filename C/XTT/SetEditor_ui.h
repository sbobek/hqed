/**
 * \file	SetEditor_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	07.08.2008
 * \version	1.15
 * \brief	This file contains class definition that arranges the set editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */


#ifndef SetEditorH
#define SetEditorH
// -----------------------------------------------------------------------------

#include <QGridLayout>

#include "ui_SetEditor.h"
// -----------------------------------------------------------------------------

class hSet;
class hSetItem;
class hType;
class XTT_Cell;
class ValueEditor_ui;
// -----------------------------------------------------------------------------

/**
* \class 	SetEditor_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	07.08.2008
* \brief 	Class definition that arranges the set editor window.
*/
class SetEditor_ui : public QWidget, private Ui_SetEditor
{
    Q_OBJECT
    
    QGridLayout* formLayout;	               ///< Pointer to grid layout form.
    QGridLayout* gp1Layout;	               ///< Pointer to groupBox1 grid layout.
    QGridLayout* gp2Layout;	               ///< Pointer to groupBox2 grid layout.
    QGridLayout* gp3Layout;	               ///< Pointer to groupBox3 grid layout.
    
    // Fields that defines the working mode
    hType* _type;                            ///< The type of the set
    hSet* _set;                              ///< Pointer to the edited set
    XTT_Cell* _cell;                         ///< Pointer to the edited cell
    int _setClass;                           ///< Denotes the type of the set (0-set, 1-domain, 2-decision context)
    bool _singleValued;                      ///< Denotes if the set can have more than one value
    bool _avr;                               ///< Denotes if the attribute value reference is allowed
    bool _autohide;                          ///< Denotes if the window works in the hide mode
    
    // Window configuration
    QString tb7icons[2];                     ///< Icon paths for toolButton_7
    int acending_index;                      ///< Index of value in comboBox_2
    int decending_index;                     ///< Index of value in comboBox_2
    int sort_from_index;                     ///< Index of value in comboBox. Sorting with from value.
    int sort_to_index;                       ///< Index of value in comboBox. Sorting with to value.
    int sort_power_index;                    ///< Index of value in comboBox. Sorting with power value.
    int sort_alph_index;                     ///< Index of value in comboBox. Sorting alphabetical
    int edited_item;                         ///< Index of the edited item
    
    hSetItem* _si;                           ///< The pointer to the edited set item
    ValueEditor_ui* vewin;                   ///< The pointer to the ValueEdit window
    
protected:

     /// \brief Function is triggered when key is pressed.
	///
	/// \param __event - Pointer to event object.
	/// \return No return value.
	virtual void keyPressEvent(QKeyEvent* __event);
    
public:

     /// \brief Constructor ValueEdit_ui class.
     ///
     /// \param parent Pointer to parent object.
     SetEditor_ui(QWidget *parent = 0);
     
     /// \brief Destructor ValueEdit_ui class.
     ~SetEditor_ui(void);
     
     /// \brief Function sets up the window properties
	///
	/// \return No return value.
     void initForm(void);
     
     /// \brief Function sets the form layout
	///
	/// \return No return value.
     void layoutForm(void);
     
     /// \brief Function sets the window working mode
	///
     /// \param *__type - data type of the set
     /// \param *__set - edited set
     /// \param __setClass - type of the set
     /// \li 0-set
     /// \li 1-domain
     /// \li 2-action context
     /// \li 3-coditional context
     /// \param __singleValued - denotes if the set can contains more than one value
     /// \param __avr - denotes if the attribute value reference is allowed
     /// \param __ptr - pointer to the cell that is edited. If cell is not edited then __ptr is equal to NULL.
     /// \return SET_WINDOW_MODE_xxx value as a result
     int setMode(hType* __type, hSet* __set, int __setClass, bool __singleValued, bool __avr, XTT_Cell* __ptr);
     
     /// \brief Function that allows for editing set item
     ///
     /// \param __itemIndex - index of the set item
     /// \return No return value.
     void itemEdit(int __itemIndex);
     
     /// \brief Function checks if the current mode is correct
	///
	/// \return true if the current mode is correct, otherwise false
     bool isModeCorrect(void);
     
     /// \brief Function returns the result of dialog
	///
	/// \return pointer to the set thst has been created by using this dialog
     hSet* result(void);

public slots:

     /// \brief Function triggered when OK button is pressed
	///
	/// \return No return value.
     void okButtonClick(void);
     
     /// \brief Function triggered when cancel button is pressed
	///
	/// \return No return value.
     void cancelButtonClick(void);
     
     /// \brief Function triggered when the move up button is pressed
	///
	/// \return No return value.
     void itemMoveUp(void);
     
     /// \brief Function triggered when the move down button is pressed
	///
	/// \return No return value.
     void itemMoveDown(void);

     /// \brief Function triggered when the list widget is double clicked
     ///
     /// \param ptr - poiter to the item that is double clicked
     /// \return No return value.
     void setItemDoubleClicked(QListWidgetItem* ptr);
     
     /// \brief Function triggered when the add button is pressed
	///
	/// \return No return value.
     void itemAdd(void);
     
     /// \brief Function triggered when the edit button is pressed
	///
	/// \return No return value.
     void onItemEdit(void);
     
     /// \brief Function triggered when the value editor is closed by use the ok button
	///
	/// \return No return value.
     void itemAdded(void);
     
     /// \brief Function triggered when the value editor is closed by use the ok button
	///
	/// \return No return value.
     void itemEdited(void);
     
     /// \brief Function triggered when the value editor is closed by use the cancel button
	///
	/// \return No return value.
     void evCloseWindow(void);
     
     /// \brief Function triggered when the delete button is pressed
	///
	/// \return No return value.
     void itemRemove(void);
     
     /// \brief Function triggered when the flush button is pressed
	///
	/// \return No return value.
     void flushSet(void);
     
     /// \brief Function triggered when the sort button is pressed
	///
	/// \return No return value.
     void sortClicked(void);
     
     /// \brief Function triggered when the sort button is pressed
	///
	/// \return No return value.
     void simplifyClicked(void);
     
     /// \brief Function triggered when the any button that changes the count of item is pressed
	///
	/// \return No return value.
     void checkSV(void);
     
     /// \brief Function triggered when the sort direction comboBox is changed
	///
	/// \return No return value.
     void sortDirectionChanged(int __newIndex);

signals:

     /// \brief Signal emited when ok button is pressed
	///
	/// \return No return value.
     void okClicked(void);
};
// -----------------------------------------------------------------------------
// This class has not the global object because each object that want to use
// the window of this class must define it own instance

#endif
