/*
 *	   $Id: PluginEventListner_ui.cpp,v 1.8.4.4 2011-06-06 15:33:33 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QListWidgetItem>

#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"

#include "../../namespaces/ns_hqed.h"

#include "PluginEventListner_ui.h"
// -----------------------------------------------------------------------------

PluginEventListner_ui* pluginEventListener;
// -----------------------------------------------------------------------------

// #brief Constructor PluginEditor_ui class.
//
// #param parent Pointer to parent object.
PluginEventListner_ui::PluginEventListner_ui(QWidget* /*parent*/)
{
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor PluginEventListner_ui class.
PluginEventListner_ui::~PluginEventListner_ui(void)
{
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void PluginEventListner_ui::initForm(void)
{
    setupUi(this);
    hqed::centerWindow(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
    
    connect(pushButton, SIGNAL(toggled(bool)), this, SLOT(onDiableStateChangeClick(bool)));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(clearClick()));
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(copyClick()));
    
    checkBox->setChecked(true);
    checkBox_2->setChecked(false);
    checkBox_3->setChecked(false);
    
    _listeningEnabled = true;
    layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void PluginEventListner_ui::layoutForm(void)
{    
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(label,        0, 0, 1, 2);
     formLayout->addWidget(checkBox,     0, 4);
     formLayout->addWidget(checkBox_2,   0, 5);
     formLayout->addWidget(checkBox_3,   0, 6);
     formLayout->addWidget(listWidget,   1, 0, 1, 7);
     formLayout->addWidget(pushButton,   2, 0);
     formLayout->addWidget(pushButton_2, 2, 1);
     formLayout->addWidget(pushButton_4, 2, 2);
     
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(3, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when disable listening key is pressed.
//
// #param __state - new state of the button
// #return No return value.
void PluginEventListner_ui::onDiableStateChangeClick(bool /*__state*/)
{
     _listeningEnabled = !_listeningEnabled;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when clear key is pressed.
//
// #return No return value.
void PluginEventListner_ui::clearClick(void)
{
     listWidget->clear();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when copy key is pressed.
//
// #return No return value.
void PluginEventListner_ui::copyClick(void)
{
     QString text;
     QList<QListWidgetItem*> si = listWidget->selectedItems();
     for(int i=0;i<si.size();++i)
          text += si.at(i)->text() + "\n";
     QApplication::clipboard()->setText(text);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when new plugin event occurs.
//
// #param __msgType - type of the event
// #param __message - event message
// #return No return value.
void PluginEventListner_ui::onNewEvent(int __msgType, QString __message)
{
     if(!_listeningEnabled)
          return;
     if((__msgType < 0) || (__msgType > 2))
          __msgType = 2;
     if((__msgType == 0)  && (!checkBox->isChecked()))
          return;
     if((__msgType == 1)  && (!checkBox_2->isChecked()))
          return;
     if((__msgType == 2)  && (!checkBox_3->isChecked()))
          return;
     
     QColor bkgcolors[] = {QColor(255, 200, 200), QColor(255, 255, 200), QColor(200, 255, 200), QColor(255, 255, 255)};
     //QColor bkgcolors[] = {QColor(255, 255, 255), QColor(255, 255, 255), QColor(255, 255, 255), QColor(255, 255, 255)};
     QString eventname [] = {"Error", "Warning", "Information", ""};           
     QString iconpaths[] = {":/all_images/images/critical_small.png",
                                 ":/all_images/images/warning_small.png",
                                 ":/all_images/images/information_small.png",
                                 ""};   
     QString message = __msgType == 3 ? __message : QTime::currentTime().toString("H:mm:ss.zzz") + " " + eventname[__msgType] + " from " + __message;
     
     QListWidgetItem* newItem = new QListWidgetItem;
     newItem->setIcon(QIcon(iconpaths[__msgType]));
     newItem->setText(message);
     newItem->setBackground(QBrush(bkgcolors[__msgType]));
     
     listWidget->addItem(newItem);
     listWidget->scrollToItem(newItem);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void PluginEventListner_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------
