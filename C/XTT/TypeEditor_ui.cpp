/*
 *	   $Id: TypeEditor_ui.cpp,v 1.18.4.1 2010-10-04 09:15:11 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include "../../M/hSet.h"
#include "../../M/hSetItem.h"
#include "../../M/hDomain.h"
#include "../../M/hTypeList.h"
#include "../../M/hType.h"
#include "../../M/hValue.h"

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "../Settings_ui.h"
#include "ValuePresentationDialog_ui.h"
#include "SetEditor_ui.h"
#include "TypeEditor_ui.h"
// -----------------------------------------------------------------------------

TypeEditor_ui* TypeEditor;
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
//
// #param parent Pointer to parent object.
TypeEditor_ui::TypeEditor_ui(QWidget*)
{
     initForm();
}
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
//
// #param bool __isNew - Denotes if the edited type is new  
// #param int __edit_type_index - The index number of edited type - when isNew == false
// #param int __base_type_id - The base type of new type - isNew == true
// #param QWidget* parent Pointer to parent object.
TypeEditor_ui::TypeEditor_ui(bool __isNew, int __edit_type_index, int __base_type_id, QWidget*)
{
     initForm();
     setWindowMode(__isNew, __edit_type_index, __base_type_id);
}
// -----------------------------------------------------------------------------

// #brief Destructor ValueEdit_ui class.
TypeEditor_ui::~TypeEditor_ui(void)
{
     delete formLayout;
     delete gp1Layout;
     delete gp2Layout;
     delete gp3Layout;
     delete gp4Layout;
     delete gp5Layout;
     delete gp6Layout;
     delete no_constr_page_layout;
     delete numeric_page_layout;
     delete symbolic_page_layout;
     
     delete winCurrentTypePtr;
     delete sewin;
}
// -----------------------------------------------------------------------------

// #brief Function sets up the window properties
//
// #return No return value.
void TypeEditor_ui::initForm(void)
{
     setupUi(this);
     setWindowIcon(QIcon(":/all_images/images/mainico.png")); 
     setWindowFlags(Qt::Window | Qt::WindowMaximizeButtonHint);
     setWindowModality(Qt::ApplicationModal);
     layoutForm();
     hqed::centerWindow(this);
     
     toolButton_2->setIcon(QIcon(":/all_images/images/Edit1.png"));
     toolButton_5->setIcon(QIcon(":/all_images/images/Edit1.png"));
     
     connect(pushButton, SIGNAL(clicked()), this, SLOT(closeClick()));
     connect(pushButton_2, SIGNAL(clicked()), this, SLOT(saveClick()));
     connect(pushButton_3, SIGNAL(clicked()), this, SLOT(presentationClick()));
     
     connect(comboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(primitiveTypeChanged(int)));
     connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged(int)));
     connect(comboBox_3, SIGNAL(currentIndexChanged(int)), this, SLOT(constraintChanged(int)));
     connect(spinBox, SIGNAL(valueChanged(int)), this, SLOT(lengthChanged(int)));
     connect(spinBox_2, SIGNAL(valueChanged(int)), this, SLOT(scaleChanged(int)));
     connect(toolButton_2, SIGNAL(clicked()), this, SLOT(domainChange()));
     connect(toolButton_5, SIGNAL(clicked()), this, SLOT(domainChange()));
     
     spinBox->setRange(0, 20);
     spinBox_2->setRange(0, 19);
     
     comboBox->clear();
     comboBox->addItems(hpTypesInfo.ptn);
     comboBox_3->clear();
     comboBox_3->addItems(hpTypesInfo.cn);
     
     sewin = new SetEditor_ui;
     winCurrentTypePtr = new hType(typeList);
     
     // Sets the default window mode
     setWindowMode(true, -1, -1);
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void TypeEditor_ui::layoutForm(void)
{
     resize(5, 5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(groupBox_2,   0, 0, 2, 1);
     formLayout->addWidget(pushButton_3, 0, 1);
     formLayout->addWidget(groupBox_4,   2, 0);
     formLayout->addWidget(groupBox,     3, 0);
     formLayout->addWidget(groupBox_3,   4, 0, 3, 1);
     formLayout->addWidget(pushButton_2, 5, 1);
     formLayout->addWidget(pushButton,   6, 1);
     //formLayout->setRowMinimumHeight(0, groupBox_2->height());
     formLayout->setRowMinimumHeight(2, groupBox_4->height());
     formLayout->setRowMinimumHeight(3, groupBox->height());
     //formLayout->setRowMinimumHeight(3, groupBox_3->height());
     formLayout->setRowStretch(0, 0);
     formLayout->setRowStretch(1, 0);
     formLayout->setRowStretch(2, 0);
     formLayout->setRowStretch(3, 0);
     formLayout->setRowStretch(4, 1);
     formLayout->setRowStretch(5, 0);
     formLayout->setRowStretch(6, 0);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
     
     gp1Layout = new QGridLayout;
     gp1Layout->addWidget(comboBox, 0, 0);
     hqed::setupLayout(gp1Layout);
     groupBox->setLayout(gp1Layout);
     
     gp2Layout = new QGridLayout;
     gp2Layout->addWidget(comboBox_2, 0, 0);
     hqed::setupLayout(gp2Layout);
     groupBox_2->setLayout(gp2Layout);
     
     gp3Layout = new QGridLayout;
     gp3Layout->addWidget(label,         0, 0);
     gp3Layout->addWidget(comboBox_3,    0, 1);
     gp3Layout->addWidget(stackedWidget, 1, 0, 1, 2);
     gp3Layout->setColumnStretch(0, 0);
     gp3Layout->setColumnStretch(1, 1);
     hqed::setupLayout(gp3Layout);
     groupBox_3->setLayout(gp3Layout);
     
     gp4Layout = new QGridLayout;
     gp4Layout->addWidget(lineEdit, 0, 0);
     hqed::setupLayout(gp4Layout);
     groupBox_4->setLayout(gp4Layout);
     
     no_constr_page_layout = new QGridLayout;
     no_constr_page_layout->addWidget(label_2, 0, 0, Qt::AlignVCenter | Qt::AlignHCenter);
     hqed::setupLayout(no_constr_page_layout);
     stackedWidget->widget(0)->setLayout(no_constr_page_layout);
     
     numeric_page_layout = new QGridLayout;
     numeric_page_layout->addWidget(checkBox,   0, 0, 1, 2);
     numeric_page_layout->addWidget(label_3,    1, 0);
     numeric_page_layout->addWidget(spinBox,    1, 1, 1, 1);
     numeric_page_layout->addWidget(label_4,    1, 3);
     numeric_page_layout->addWidget(spinBox_2,  1, 4);
     numeric_page_layout->addWidget(groupBox_5, 2, 0, 1, 5);
     numeric_page_layout->setColumnStretch(2, 1);
     numeric_page_layout->setColumnStretch(4, 1);
     hqed::setupLayout(numeric_page_layout);
     stackedWidget->widget(1)->setLayout(numeric_page_layout);
     
     gp5Layout = new QGridLayout;
     gp5Layout->addWidget(listWidget,   0, 0, 2, 1);
     gp5Layout->addWidget(toolButton_2, 0, 1);
     gp5Layout->setColumnStretch(0, 1);
     gp5Layout->setRowStretch(1, 1);
     hqed::setupLayout(gp5Layout);
     groupBox_5->setLayout(gp5Layout);
     
     symbolic_page_layout = new QGridLayout;
     symbolic_page_layout->addWidget(checkBox_2, 0, 0);
     symbolic_page_layout->addWidget(groupBox_6, 1, 0, 1, 2);
     symbolic_page_layout->setColumnStretch(1, 1);
     hqed::setupLayout(symbolic_page_layout);
     stackedWidget->widget(2)->setLayout(symbolic_page_layout);
     
     gp6Layout = new QGridLayout;
     gp6Layout->addWidget(listWidget_2, 0, 0, 2, 1);
     gp6Layout->addWidget(toolButton_5, 0, 1);
     gp6Layout->setColumnStretch(0, 1);
     gp6Layout->setRowStretch(1, 1);
     hqed::setupLayout(gp6Layout);
     groupBox_6->setLayout(gp6Layout);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void TypeEditor_ui::keyPressEvent(QKeyEvent* event)
{
     switch(event->key())
     {
          case Qt::Key_Escape:
               closeClick();
               break;

          case Qt::Key_Return:
               saveClick();
               break;

          default:
               QWidget::keyPressEvent(event);
     }
}
// -----------------------------------------------------------------------------

// #brief Constructor ValueEdit_ui class.
//
// #param bool __isNew - Denotes if the edited type is new  
// #param int __edit_type_index - The index number of edited type - when isNew == false
// #param int __base_type_id - The base type of new type - isNew == true
// #return No value return.
void TypeEditor_ui::setWindowMode(bool __isNew, int __edit_type_index, int __base_type_id)
{
     comboBox_2->disconnect();
     comboBox_2->clear();
     comboBox_2->addItems(typeList->names(false));

     int pti = hpTypesInfo.iopti(__base_type_id);
     
     if(__isNew)
     {
          comboBox_2->setCurrentIndex(-1);
          if(pti > -1)
               comboBox->setCurrentIndex(pti);
     }
          
     if((!__isNew) && (__edit_type_index >= 0))
     {
          int ti = __edit_type_index + hpTypesInfo.ptc;
          int ci = hpTypesInfo.ioci(typeList->at(ti)->typeClass());
          pti = hpTypesInfo.iopti(typeList->at(ti)->baseType());
          
          if(comboBox_2->currentIndex() != __edit_type_index)
               comboBox_2->setCurrentIndex(__edit_type_index);
          lineEdit->setText(typeList->at(ti)->description());
          comboBox->setCurrentIndex(pti);
          comboBox_3->setCurrentIndex(ci);
          spinBox->setValue(typeList->at(ti)->length());
          spinBox_2->setValue(typeList->at(ti)->scale());
          if((typeList->at(ti)->typeClass() == TYPE_CLASS_DOMAIN) && (typeList->at(ti)->baseType() == SYMBOLIC_BASED))
               checkBox_2->setChecked(typeList->at(ti)->domain()->ordered());
          
          createCurrentWinType();
          winCurrentTypePtr->setID(typeList->at(ti)->id());
          winCurrentTypePtr->setPresentationSourceId(typeList->at(ti)->presentationSourceId());
          winCurrentTypePtr->setPresentationSourceName(typeList->at(ti)->presentationSourceName());
          winCurrentTypePtr->setPresentationSourceType(typeList->at(ti)->presentationSourceType());
          if(typeList->at(ti)->typeClass() == TYPE_CLASS_DOMAIN)
          {
               winCurrentTypePtr->domain()->flush();
               winCurrentTypePtr->domain()->setParentType(winCurrentTypePtr);
               typeList->at(ti)->domain()->copyToDomain(winCurrentTypePtr->domain());
               displayDomain();
          }
     }
     
     _isNew = __isNew;
     _edit_type_index = __edit_type_index;
     _base_type_id = __base_type_id;
     
     connect(comboBox_2, SIGNAL(currentIndexChanged(int)), this, SLOT(typeChanged(int)));
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the primitiveType comboBox changed
//
// #return No return value.
void TypeEditor_ui::primitiveTypeChanged(int __index)
{
     int ci = comboBox_3->currentIndex();
     if((__index < 0) || (__index >= hpTypesInfo.ptc) || (ci < 0) || (ci >= hpTypesInfo.cc))
          return;
          
     setUpPages(hpTypesInfo.ptids[__index], hpTypesInfo.cids[ci]);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the constraint type comboBox changed
//
// #return No return value.
void TypeEditor_ui::constraintChanged(int __index)
{
     int ci = comboBox->currentIndex();
     if((__index < 0) || (__index >= hpTypesInfo.cc) || (ci < 0) || (ci >= hpTypesInfo.ptc))
          return;
          
     setUpPages(hpTypesInfo.ptids[ci], hpTypesInfo.cids[__index]);
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #param int __primitiveType - identifier of primitive type
// #param int __contraintType - identifier of constraint type
// #return No return value.
void TypeEditor_ui::setUpPages(int __primitiveType, int __contraintType)
{
     comboBox_3->setEnabled(true);

     // When the constraints are not defined or if it is boolen based type
     if((__contraintType == NO_TYPE_CLASS) || (__primitiveType == BOOLEAN_BASED))
     {
          stackedWidget->setCurrentIndex(0);
     }
          
     if(__primitiveType == BOOLEAN_BASED)
     {
          stackedWidget->setCurrentIndex(0);
          comboBox_3->setCurrentIndex(hpTypesInfo.ioci(NO_TYPE_CLASS));
          comboBox_3->setEnabled(false);
     }
          
     if(__contraintType == TYPE_CLASS_DOMAIN)
     {
          if(__primitiveType == INTEGER_BASED)
          {
               spinBox_2->setValue(0);
               spinBox_2->setEnabled(false);
               stackedWidget->setCurrentIndex(1);
          }
          
          if(__primitiveType == NUMERIC_BASED)
          {
               spinBox_2->setEnabled(true);
               stackedWidget->setCurrentIndex(1);
          }
          
          if(__primitiveType == SYMBOLIC_BASED)
          {
               stackedWidget->setCurrentIndex(2);
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the name selector is changed
//
// #param int __index - the new index of type
// #return No return value.
void TypeEditor_ui::typeChanged(int __index)
{
     if(__index == -1)
          return;
          
     setWindowMode(false, __index, -1);
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the scale value is changed
//
// #return No return value.
void TypeEditor_ui::scaleChanged(int __newValue)
{
     int lenVal = spinBox->value();
     int sclVal = __newValue;
     
     if(sclVal >= lenVal)
          spinBox->setValue(sclVal+1);
          
     createCurrentWinType();
     displayDomain();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the length value is changed
//
// #return No return value.
void TypeEditor_ui::lengthChanged(int __newValue)
{
     int lenVal = __newValue;
     int sclVal = spinBox_2->value();
     
     if(lenVal <= sclVal)
          spinBox_2->setValue((lenVal-1)<0?0:(lenVal-1));
          
     createCurrentWinType();
     displayDomain();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when Close button is pressed
//
// #return No return value.
void TypeEditor_ui::closeClick(void)
{
     /*createCurrentWinType();
     
     QString getval1 = QInputDialog::getText(this, "Get value", "Value:", QLineEdit::Normal, "", NULL, Qt::Dialog);
     QString getval = QInputDialog::getText(this, "Get value", "Value:", QLineEdit::Normal, "", NULL, Qt::Dialog);
     
     int cntidx = winCurrentTypePtr->domain()->contain(getval);
     bool cnt = (cntidx > -1);
     QString p1msg = "Domain contains: ";
     if(cnt)   p1msg += "true";
     if(!cnt)   p1msg += "false";
     p1msg += ", item index = " + QString::number(cntidx);
     
     QString p2msg = "Numeric representation: ";
     double val;
     int res = winCurrentTypePtr->mapToNumeric(getval, val);
     if(res == H_TYPE_ERROR_NO_ERROR)
          p2msg += "YES nr = " + QString::number(val);
     if(res != H_TYPE_ERROR_NO_ERROR)
          p2msg += "NO, error = " + winCurrentTypePtr->lastError();
          
     QString p3msg = "Symbolic representation: ";
     QString sr;
     res = winCurrentTypePtr->mapFromNumeric(getval, sr);
     if(res == H_TYPE_ERROR_NO_ERROR)
          p3msg += "YES symbolic = " + sr;
     if(res != H_TYPE_ERROR_NO_ERROR)
          p3msg += "NO, error = " + winCurrentTypePtr->lastError();
          
     QString p4msg = "Previous value not compatible with domain: ";
     res = winCurrentTypePtr->typePreviousValue(getval, sr);
     if(res == H_TYPE_ERROR_NO_ERROR)
          p4msg += "YES prev value = " + sr;
     if(res != H_TYPE_ERROR_NO_ERROR)
          p4msg += "NO, error = " + winCurrentTypePtr->lastError();
          
     QString p5msg = "Next value not compatible with domain: ";
     res = winCurrentTypePtr->typeNextValue(getval, sr);
     if(res == H_TYPE_ERROR_NO_ERROR)
          p5msg += "YES next value = " + sr;
     if(res != H_TYPE_ERROR_NO_ERROR)
          p5msg += "NO, error = " + winCurrentTypePtr->lastError();
          
     hSetItem* itm = new hSetItem(winCurrentTypePtr->domain(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(getval), NULL, false);
     QString p6msg = "Domain after substraction: " + itm->toString() + ": ";
     QList<hSetItem*>* tmp;
     for(int i=0;i<winCurrentTypePtr->domain()->size();++i)
     {
          tmp = winCurrentTypePtr->domain()->at(i)->sub(itm);
          if(tmp != NULL)
          {    
               for(int j=0;j<tmp->size();++j)
                    p6msg  = p6msg + tmp->at(j)->toString() + " U ";
          }
          if(tmp == NULL)
               p6msg = p6msg + "\n" + winCurrentTypePtr->domain()->at(i)->lastError() + "\n";
     }
     
     hSetItem* itm1 = new hSetItem(winCurrentTypePtr->domain(), SET_ITEM_TYPE_SINGLE_VALUE, new hValue(getval1), new hValue(getval), false);
     int cnt_dom = winCurrentTypePtr->domain()->contain(itm1);
     hSet* set2 = NULL;
     //if(cnt_dom == H_SET_TRUE_VALUE)
          set2 = winCurrentTypePtr->domain()->sub(itm1);
     QString p7msg = "Domain after substraction: " + itm1->toString() + " = ";
     
     if(set2 != NULL)
          p7msg += set2->toString();
     if((set2 == NULL) && (cnt_dom == H_SET_TRUE_VALUE))
          p7msg += " error " + winCurrentTypePtr->domain()->lastError();
     if((set2 == NULL) && (cnt_dom == H_SET_FALSE_VALUE))
          p7msg += " error out of domain";
     if((set2 == NULL) && (cnt_dom == H_SET_OTHER_OBJECT_ERROR))
          p7msg += " error " + winCurrentTypePtr->domain()->lastError();
          
     QString p8msg = "Items power:\n";
     for(int i=0;i<winCurrentTypePtr->domain()->size();++i)
     {
          int ok;
          QString n = "";
          quint64 power = winCurrentTypePtr->domain()->itemPower(i, &ok);
          if(ok != H_SET_ERROR_NO_ERROR)
                n = winCurrentTypePtr->domain()->lastError();
          if(ok == H_SET_ERROR_NO_ERROR)
               n = QString::number(power);
          p8msg += "Item: " + winCurrentTypePtr->domain()->at(i)->toString() + ": " + n + "\n";
     }
     
     QMessageBox::information(NULL, "HQEd", p1msg + "\n" + p2msg + "\n" + p3msg + "\n" + p4msg + "\n" + p5msg + "\n" + p6msg + "\n" + p7msg + "\n" + p8msg, QMessageBox::Ok);*/
     
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when presentation button is pressed
//
// #return No return value.
void TypeEditor_ui::presentationClick(void)
{
     delete ValuePresentationDialog;
     ValuePresentationDialog = new ValuePresentationDialog_ui(this);
     if(ValuePresentationDialog->setMode(winCurrentTypePtr) == SET_WINDOW_MODE_OK_YOU_CAN_SHOW)
          ValuePresentationDialog->show();
}
// -----------------------------------------------------------------------------

// #brief Function that stores the current type that is set up by using this dialog in winCurrentTypePtr field
//
// #return true if all its ok, otherwise false
bool TypeEditor_ui::createCurrentWinType(void)
{   
     int pti = comboBox->currentIndex();
     if(pti == -1)
          return false;
          
     int ci = comboBox_3->currentIndex();
     if(ci == -1)
          return false;
          
     winCurrentTypePtr->setName(comboBox_2->currentText());
     winCurrentTypePtr->setDescription(lineEdit->text());
          
     if(winCurrentTypePtr->baseType() != hpTypesInfo.ptids[pti])
          winCurrentTypePtr->domain()->flush();
   
     bool checkItems = (winCurrentTypePtr->length() != spinBox->value()) || (winCurrentTypePtr->scale() != spinBox_2->value());
     
     winCurrentTypePtr->setType(hpTypesInfo.ptids[pti]);
     winCurrentTypePtr->setClass(hpTypesInfo.cids[ci]);
     winCurrentTypePtr->setLength(spinBox->value());
     winCurrentTypePtr->setScale(spinBox_2->value());
     
     if(winCurrentTypePtr->baseType() == SYMBOLIC_BASED)
          winCurrentTypePtr->domain()->setOrdered(checkBox_2->isChecked());
     
     if((hpTypesInfo.cids[ci] == TYPE_CLASS_DOMAIN) && (checkItems))
     {
          for(int i=0;i<winCurrentTypePtr->domain()->size();++i)
               if(winCurrentTypePtr->isItemCorrect(winCurrentTypePtr->domain()->at(i), false) != H_TYPE_ERROR_NO_ERROR)
               {
                    QMessageBox::critical(NULL, "HQEd", "The domain item: " + winCurrentTypePtr->domain()->at(i)->toString() + " is incorrect.\nError message: " + winCurrentTypePtr->lastError(), QMessageBox::Ok);
                    return false;
               }
     }
     
     return true;
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the edit domain button is pressed
//
// #return No return value.
void TypeEditor_ui::domainChange(void)
{
     int pti = comboBox->currentIndex();
     if(pti == -1)
          return;
     
     // Create new instance of set editor
     delete sewin;
     sewin = new SetEditor_ui;
     
     if(!createCurrentWinType())
     {
          QMessageBox::critical(NULL, "HQEd", "Current type is incorrect. Check the values of the parameters: primitive type and constraints.", QMessageBox::Ok);
          return;
     }
     
     // Renumbering items
     if((winCurrentTypePtr->baseType() == SYMBOLIC_BASED) && (winCurrentTypePtr->domain()->ordered()))
     {
          if(!winCurrentTypePtr->domain()->updateItemsNumbering())
          {
               QMessageBox::critical(NULL, "HQEd", winCurrentTypePtr->domain()->lastError(), QMessageBox::Ok);
               return;
          }
     }
     
     connect(sewin, SIGNAL(okClicked()), this, SLOT(domainChanged()));
     int smr = sewin->setMode(winCurrentTypePtr, winCurrentTypePtr->domain(), 1, false, false, NULL);
     
     if(smr == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is not correct.", QMessageBox::Ok);
          delete winCurrentTypePtr;
          return;
     }
     if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
          
     sewin->show();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the set editor is closed with OK button
//
// #return No return value.
void TypeEditor_ui::domainChanged(void)
{
     hSet* result = sewin->result();
     
     winCurrentTypePtr->domain()->flush();
     for(int i=0;i<result->size();++i)
     {
          hSetItem* itm = result->at(i)->copyTo();
          if(winCurrentTypePtr->domain()->add(itm) != H_SET_ERROR_NO_ERROR)
               QMessageBox::information(NULL, "HQEd", "Error adding element: " + itm->toString() + "\Error message: " + winCurrentTypePtr->domain()->lastError(), QMessageBox::Ok);
     }
     winCurrentTypePtr->domain()->setParentType(winCurrentTypePtr);
     
     // Update numbering
     if((winCurrentTypePtr->baseType() == SYMBOLIC_BASED) && (winCurrentTypePtr->domain()->ordered()))
     {
          if(!winCurrentTypePtr->domain()->updateNumbering())
          {
               QMessageBox::critical(NULL, "HQEd", winCurrentTypePtr->domain()->lastError(), QMessageBox::Ok);
               return;
          }
     }

     delete result;
     displayDomain();
}
// -----------------------------------------------------------------------------

// #brief Function that displays the domain
//
// #return No value return.
void TypeEditor_ui::displayDomain(void)
{
     listWidget->clear();
     listWidget_2->clear();
     QListWidget* destListWidget = listWidget;
     if(winCurrentTypePtr->baseType() == SYMBOLIC_BASED)
          destListWidget = listWidget_2;
     destListWidget->addItems(winCurrentTypePtr->domain()->stringItems());
}
// -----------------------------------------------------------------------------

// #brief Function triggered when the save button is pressed
//
// #return No return value.
void TypeEditor_ui::saveClick(void)
{    
     if(!createCurrentWinType())
          return;
         
     // Checking the name
     QString name = comboBox_2->currentText();
     if(name == "")
     {
          QMessageBox::information(NULL, "HQEd", "The name canot be empty.", QMessageBox::Ok);
          return;
     }
     if(((_isNew) && (typeList->indexOfName(name) > -1)) ||
        ((!_isNew) && (typeList->indexOfName(name) > -1) && (typeList->indexOfName(name) != (hpTypesInfo.ptc + _edit_type_index))))
     {    
          QMessageBox::information(NULL, "HQEd", "The name is already exists. Type another name.", QMessageBox::Ok);
          return;
     }
     
     if(_isNew)
     {
          typeList->add(winCurrentTypePtr->copyTo());
          _edit_type_index = typeList->size() - hpTypesInfo.ptc - 1;;
     }
     
     if(!_isNew)
     {
          hType* _old = typeList->takeAt(hpTypesInfo.ptc + _edit_type_index);
          hType* _new = winCurrentTypePtr->copyTo();
          typeList->insert(hpTypesInfo.ptc + _edit_type_index, _new);
          typeList->updateTypePointer(_old, _new);
          delete _old;
          
          // Test of types verification
          QString msg = "";
          if((!xtt::fullVerification(msg)) && (Settings->xttShowErrorMessages()))
               hqed::showMessages(msg);
          xtt::logicalFullVerification();
     }
     
     setWindowMode(false, _edit_type_index, -1);
     emit saveClicked();
}
// -----------------------------------------------------------------------------
