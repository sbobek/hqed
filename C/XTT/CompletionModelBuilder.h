#ifndef COMPLETIONMODELBUILDER_H
#define COMPLETIONMODELBUILDER_H

#include <QList>
#include <QStringList>
#include <QStandardItemModel>

class hType;

class CompletionModelBuilder
{
public:
    CompletionModelBuilder()
    {}

    void setSymbolicValues(const QList<hType*> &types)
    {
        m_SymbolicTypes = types;
    }

    void setSpecialValues(const QStringList &special)
    {
        m_SpecialValues = special;
    }

    void setOperatorRepresentations(const QStringList &operators)
    {
        m_OperatorRepresentations = operators;
    }

    void setFunctionNames(const QStringList &functions)
    {
        m_FunctionNames = functions;
    }

    void setAttributeNames(const QStringList &attributes)
    {
        m_AttributeNames = attributes;
    }

    void buildModel(QStandardItemModel *model);

private:
    QList<hType*> m_SymbolicTypes;
    QStringList m_SpecialValues;
    QStringList m_OperatorRepresentations;
    QStringList m_FunctionNames;
    QStringList m_AttributeNames;
};

#endif // COMPLETIONMODELBUILDER_H
