#include "CompletionModelBuilder.h"

#include "hType.h"
#include "hDomain.h"

#include "QIcon"

void CompletionModelBuilder::buildModel(QStandardItemModel *model)
{
    if(!model) {
        return;
    }

    model->clear();

    QIcon orangeIcon(":/all_images/images/boxOrange.png");
    QIcon blueIcon(":/all_images/images/boxBlue.png");
    QIcon greenIcon(":/all_images/images/boxGreen.png");
    QIcon purpleIcon(":/all_images/images/boxPurple.png");

    foreach(const QString op, m_OperatorRepresentations) {
        QStandardItem *item = new QStandardItem(orangeIcon, op);
        model->appendRow(item);
    }

    foreach(const QString function, m_FunctionNames) {
        QStandardItem *item = new QStandardItem(orangeIcon, function);
        model->appendRow(item);
    }

    foreach(hType *type, m_SymbolicTypes) {
        if(type->baseType() == SYMBOLIC_BASED) {
            foreach(const QString value, type->domain()->stringItems()) {
                QStandardItem *item = new QStandardItem(blueIcon, value);
                model->appendRow(item);
            }
        }
    }

    foreach(const QString specialValue, m_SpecialValues) {
        QStandardItem *item = new QStandardItem(greenIcon, specialValue);
        model->appendRow(item);
    }

    foreach(const QString attribute, m_AttributeNames) {
        QStandardItem *item = new QStandardItem(purpleIcon, attribute);
        model->appendRow(item);
    }
}


