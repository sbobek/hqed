/**
 * \file	hMenuAction.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	31.10.2009
 * \version	1.0
 * \brief	This file contains class definition of the specialized menu action button.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HMENUACTIONH
#define HMENUACTIONH
// -----------------------------------------------------------------------------

#include <QAction>
// -----------------------------------------------------------------------------

class hSet;
class hType;
class XTT_Attribute;
class hMultipleValue;
class ExpressionEditor_ui;
// -----------------------------------------------------------------------------
/**
* \class 	hMenuAction
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	31.10.2009 
* \brief 	This class definies specialized menu action button/
*/
class hMenuAction : public QAction
{
    Q_OBJECT
    
private:

    QString _action;                   ///< The string that contains the name of the action as well as parameters of the action
    ExpressionEditor_ui* exprEditor;   ///< Pointer to the expression editor dialog

    /// \brief Function that constructs the object and sets up the object parameters
    ///
    /// \return no values return
    void initObject(void);

public:

    /// \brief Constructor hMenuAction class.
    ///
    /// \param __action - string that contains the name of the action and its parameters
    /// \param parent Pointer to parent object.
    hMenuAction(QString __action, QObject *parent);

    /// \brief Constructor hMenuAction class.
    ///
    /// \param __action - string that contains the name of the action and its parameters
    /// \param __title - button title
    /// \param parent Pointer to parent object.
    hMenuAction(QString __action, const QString& __title, QObject *parent);

    /// \brief Constructor hMenuAction class.
    ///
    /// \param __action - string that contains the name of the action and its parameters
    /// \param __icon - icon on the button
    /// \param __title - button title
    /// \param parent Pointer to parent object.
    hMenuAction(QString __action, const QIcon& __icon, const QString& __title, QObject *parent);

    /// \brief Destructor hMenuAction class.
    virtual ~hMenuAction(void);

    /// \brief Function that allows for changing action executed by this button
    ///
    /// \param __action - string that contains the name of the action and its parameters
    /// \return no values return
    void setAction(QString __action);

public slots:

    /// \brief Function that is triggered when the action is clicked
    ///
    /// \return no values return
    void onClick(void);

    /// \brief Function that is triggered when the
    ///
    /// \return no values return
    void onMouseOver(void);

    /// \brief Function shows cell contents window.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \param __colIndex - index of the column in the given table
    /// \return No return value.
    void editCell(QString __tableId, int __rowIndex, int __colIndex);

    /// \brief Function shows new cell content editor
    ///
    /// \param __tableId - table id
    /// \param ri - index of the row in the given table
    /// \param ci - index of the column in the given table
    /// \return No return value.
    void newRulesEditor(QString __tableId, int ri, int ci);

    /// \brief Function shows table editor window
    ///
    /// \param __tableId - table id
    /// \return No return value.
    void editTable(QString __tableId);

    /// \brief Function shows cell editor window
    ///
    /// \param __tableId - table id
    /// \return No return value.
    void cellEditor(QString __tableId);

    /// \brief Function called when cell editor is closed using the OK button
    ///
    /// \return No return value.
    void cellEditorConfirm(void);

    /// \brief Function called on cell editor close event
    ///
    /// \return No return value.
    void cellEditorClose(void);

    /// \brief Function that changes the operator in the given cell
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \param __colIndex - index of the column in the given table
    /// \param __operatorinternalname - the internal representation of the operator
    /// \return No return value.
    void changeCellOperator(QString __tableId, int __rowIndex, int __colIndex, QString __operatorinternalname);

    /// \brief Function that sets the content of the cell to default
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \param __colIndex - index of the column in the given table
    /// \return No return value.
    void resetCellContent(QString __tableId, int __rowIndex, int __colIndex);

    /// \brief Function moves one row up.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void moveRowUp(QString __tableId, int __rowIndex);

    /// \brief Function moves one row down.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void moveRowDown(QString __tableId, int __rowIndex);

    /// \brief Function inserts one row before another.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void insertRowBefore(QString __tableId, int __rowIndex);

    /// \brief Function inserts one row after another.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void insertRowAfter(QString __tableId, int __rowIndex);

    /// \brief Function add new row the end  of the table
    ///
    /// \param __tableId - table id
    /// \return No return value.
    void addNewRow(QString __tableId);

    /// \brief Function adds a new state to the group
    ///
    /// \param __statebasename - basename of the state
    /// \return No return value.
    void addNewState(QString __statebasename);

    /// \brief Function copies one row before another.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void copyRowBefore(QString __tableId, int __rowIndex);

    /// \brief Function copies one row after another.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void copyRowAfter(QString __tableId, int __rowIndex);

    /// \brief Function that add a new row that is a copy of the current one
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void addCopyOfRow(QString __tableId, int __rowIndex);

    /// \brief Function that add a new state that is a copy of the current one
    ///
    /// \param __statebasename - basename of the state
    /// \param __stateIndex - index of state in the group
    /// \return No return value.
    void addCopyOfState(QString __statebasename, int __stateIndex);

    /// \brief Function removes current row.
    ///
    /// \param __tableId - table id
    /// \param __rowIndex - index of the row in the given table
    /// \return No return value.
    void deleteRow(QString __tableId, int __rowIndex);

    /// \brief Function removes given state
    ///
    /// \param __statebasename - base name of the state groups from which the state should be removed
    /// \param __stateIndex - index of the state in the given state group
    /// \return No return value.
    void deleteState(QString __statebasename, int __stateIndex);

    /// \brief Function removes current column.
    ///
    /// \param __tableId - table id
    /// \param __colIndex - index of the column in the given table
    /// \return No return value.
    void deleteCol(QString __tableId, int __colIndex);

    /// \brief Function that loads the given state into xtt model
    ///
    /// \param __statebasename - base name of the state groups from which the state should be removed
    /// \param __stateIndex - index of the state in the given state group
    /// \return No return value.
    void loadState(QString __statebasename, int __stateIndex);

    /// \brief Function that executes the model simulation using given engine
    ///
    /// \param __enginetype - the type of the engine: hqed, devplugin
    /// \param __enginename - the name of the engine
    /// \param __simulationtype - the type of the simulation
    /// \param __statebasename - base name of the state groups from which the simulation will be executed
    /// \param __stateIndex - index of the state in the given state group
    /// \return No return value.
    void runSimulation(QString __enginetype, QString __enginename, QString __simulationtype, QString __statebasename, int __stateIndex);

    /// \brief Function removes attributes from the given state group
    ///
    /// \param __statebasename - base name of the state
    /// \param __attr - pointer to the attribute that schould be removed
    /// \return No return value.
    void deleteInputStateCol(QString __statebasename, XTT_Attribute* __attr);

    /// \brief Function removes the current table
    ///
    /// \param __tableId - table id
    /// \return No return value.
    void deleteTable(QString __tableId);

    /// \brief Function that removes the group of states
    ///
    /// \param __basename - name of the group
    /// \return No return value.
    void deleteStatesgroup(QString __basename);

    /// \brief Function shows edit attribute window.
    ///
    /// \param __tableId - table id
    /// \param __colIndex - index of the column in the given table
    /// \return No return value.
    void editAttribute(QString __tableId, int __colIndex);

    /// \brief Function shows edit attribute window.
    ///
    /// \param __xttattribute - pointer to the attribute
    /// \return No return value.
    void editAttribute(XTT_Attribute* __xttattribute);

    /// \brief Function shows edit state schema dialog window.
    ///
    /// \param __statebasename - the name of the state group
    /// \return No return value.
    void editStateSchema(QString __statebasename);

    /// \brief Function that adds a range to the set
    ///
    /// \param __setPtr - pointer to the set
    /// \param __from - from value
    /// \param __to - to value
    /// \return No return value.
    void addSetRange(hSet* __setPtr, QString __from, QString __to);

    /// \brief Function that adds a range to the MultipleValue and sets it to value set
    ///
    /// \param __setPtr - pointer to the MultipleValue
    /// \param __from - from value
    /// \param __to - to value
    /// \return No return value.
    void addMultipleValueRange(hMultipleValue* __setPtr, QString __from, QString __to);

    /// \brief Function executes setCreator dialog
    ///
    /// \param __type - pointer to the set type
    /// \param __set - pointer to the set that we want to edit
    /// \param __singlerequired - indicates if the set can conatin only one value
    /// \return No return value.
    void setCreator(hType* __type, hSet* __set, bool __singlerequired);

    /// \brief Function that is trgered when user clicks ok in the setCreator
    ///
    /// \return No return value.
    void setCreatorOkClicked(void);

    /// \brief Function that removes given item from the given set
    ///
    /// \param __setPtr - pointer to the set
    /// \param __itemIndex - index of the item
    /// \return No return value.
    void removeSetItem(hSet* __setPtr, int __itemIndex);

    /// \brief Function shows edit attribute window.
    ///
    /// \param __tableId - table id
    /// \param __colIndex - index of the column in the given table
    /// \param __criterion - idicates the criterion of table sorting
    /// \return No return value.
    void sortTable(QString __tableId, int __colIndex, int __criterion);

signals:

    /// \brief Signal that is emitted when set createro window is clicked with OK button
    ///
    /// \return No return value.
    void setCreatorConfirmed(void);
};
// -----------------------------------------------------------------------------
#endif
