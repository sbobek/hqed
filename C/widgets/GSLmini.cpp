/*
 *	  $Id: GSLmini.cpp,v 1.2.4.1 2010-10-04 09:15:11 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "GSLmini.h"
// -----------------------------------------------------------------------------

// #brief Constructs a GSLmini
//
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
GSLmini::GSLmini(QWidget* __parent) 
: QGraphicsView(__parent)
{
	fScaleRatio = 1.2;
     fLockRectangle = false;
     setInteractive(false);
}
// -----------------------------------------------------------------------------

// #brief Constructs a GSLmini and sets the visualized scene.
//
// #param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
GSLmini::GSLmini(QGraphicsScene* __scene, QWidget* __parent) 
: QGraphicsView(__scene, __parent)
{
	fScaleRatio = 1.2;
     fLockRectangle = false;
     setInteractive(false);
}
// -----------------------------------------------------------------------------

// #brief Descrutor of the GSLmini class
GSLmini::~GSLmini()
{
}
// -----------------------------------------------------------------------------

// #brief Draws "miniFrame" rectangle on the miniature view foreground.
//
// #param __painter - the canvas where the foreground will be painted
// #param __rect - Exposed rectangle.
// #return no value return
void GSLmini::drawForeground(QPainter* __painter, const QRectF& /*__rect*/)
{
	QLinearGradient brush(fMiniRectangle.left(), fMiniRectangle.top(), fMiniRectangle.right(), fMiniRectangle.bottom());
	brush.setColorAt(0., QColor(100,100,100,150));
	//brush.setColorAt(0., QColor(100,100,100,150));
     brush.setColorAt(0.9, QColor(255,255,255,150));
     __painter->setPen(QPen(QBrush(QColor(Qt::black)), __painter->pen().widthF()));
     __painter->setBrush(QBrush(brush));

     __painter->drawRect(fMiniRectangle);
}
// -----------------------------------------------------------------------------

// #brief Sets the rectangle which marks on the miniature the state of main view.
//
// #param __rect - Rectangle to set.
// #return no value return
void GSLmini::setMiniRectangle(const QRectF& __rect)
{
     if(fLockRectangle)
          return;
	fMiniRectangle = __rect;
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when you press mouse button.
//
// #param __event - the pointer to the object that holds the data of mouse event
// #return no value return
void GSLmini::mousePressEvent(QMouseEvent* __event)
{
	moveRect(__event->x(), __event->y());
}
// -----------------------------------------------------------------------------

// #param Implementation of virtual function which is call when you move mouse.
//
// #param __event - the pointer to the object that holds the data of mouse event
// #return no value return
void GSLmini::mouseMoveEvent(QMouseEvent* __event)
{
	if(!(__event->buttons() & Qt::LeftButton)) 
          return;
	moveRect(__event->x(), __event->y());
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when you use wheel.
//
// #param __event - the pointer to the object that holds the data of mouse event
// #return no value return
void GSLmini::wheelEvent(QWheelEvent* __event)
{
	int ev;

	QPointF center;

	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	ev = __event->delta();
	center = getCenter();

	if(ev < 0)
     {
		fMiniRectangle.setHeight(fMiniRectangle.height()*fScaleRatio);
		fMiniRectangle.setWidth(fMiniRectangle.width()*fScaleRatio);
	}
	else
     {
		fMiniRectangle.setHeight(fMiniRectangle.height()/fScaleRatio);
		fMiniRectangle.setWidth(fMiniRectangle.width()/fScaleRatio);
	}

	setTransformationAnchor(QGraphicsView::NoAnchor);

	setRectPosition(center.x(), center.y());
	update();
	emit mouseAction(fMiniRectangle);
}
// -----------------------------------------------------------------------------

// #brief Reimplementation of update function.
//
// #return no value return
void GSLmini::update(void)
{
	fitInView(sceneRect(), Qt::KeepAspectRatio);
	QGraphicsView::update();
}
// -----------------------------------------------------------------------------

// #brief Sets scene of GSceneLocalizator.
//
// #param __scene - Pointer to scene.
// #return no value return
void GSLmini::setScene(QGraphicsScene* __scene)
{
	QGraphicsView::setScene(__scene);
}
// -----------------------------------------------------------------------------

// #brief Sets view rectangle position.
//
// #param __x - X coordinate of center point - in scene coordinates.
// #param __y - Y coordinate of center point - in scene coordinates.
// #return No value returns
void GSLmini::setRectPosition(qreal __x, qreal __y)
{
     QRectF mrtmp = fMiniRectangle;
     
     mrtmp.moveCenter(QPointF(__x, fMiniRectangle.center().y()));
	//if(sceneRect().contains(mrtmp))
          fMiniRectangle.moveCenter(QPointF(__x, fMiniRectangle.center().y()));
     mrtmp.moveCenter(QPointF(fMiniRectangle.center().x(), __y));
     //if(sceneRect().contains(mrtmp))
          fMiniRectangle.moveCenter(QPointF(fMiniRectangle.center().x(), __y));
}
// -----------------------------------------------------------------------------

// #brief Sets view rectangle position.
//
// #param __x - X coordinate of center point - in view coordinates.
// #param __y - Y coordinate of center point - in view coordinates.
// #return no values return
void GSLmini::moveRect(int __x, int __y)
{
	QPointF sceneP;

	sceneP = mapToScene(__x, __y);

	setRectPosition(sceneP.x(), sceneP.y());
     fLockRectangle = true;
     update();
	emit mouseAction(fMiniRectangle);
     fLockRectangle = false;
}
// -----------------------------------------------------------------------------

// #brief returns the point that is the center of the rectangle
//
// #return returns the point that is the center of the rectangle
QPointF GSLmini::getCenter(void)
{
	return fMiniRectangle.center();
}
// -----------------------------------------------------------------------------
