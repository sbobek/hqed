/*
 *	  $Id: GSceneView.cpp,v 1.2.4.2 2011-06-06 15:33:33 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QWidget>

#include "GSceneView.h"
// -----------------------------------------------------------------------------

// #brief Constructs a GSceneView
//
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
GSceneView::GSceneView(QWidget* __parent) 
: QGraphicsView(__parent) 
{
	fViewMode = Normal;
	fScaleRatio = 1.2;
	setGeometry(0, 0, 100, 100);

	connect(this->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(sliderMove()));
	connect(this->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(sliderMove()));
}
// -----------------------------------------------------------------------------

// #brief Constructs a GSceneView and sets the visualized scene.
//
// #param __scene - Pointer to ardGraphicsScene object which hBaseScene displays.
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
GSceneView::GSceneView(hBaseScene* __scene, QWidget* __parent)
     : QGraphicsView(__scene, __parent)
{
     connectScene(__scene);
     initScene();
}
// -----------------------------------------------------------------------------

// #brief Destructor.
GSceneView::~GSceneView() 
{
}
// -----------------------------------------------------------------------------

// \brief Initializes scene
//
// \return no value reurns.
void GSceneView::initScene(void)
{
     fViewMode = Normal;
     fScaleRatio = 1.2;
     setGeometry(0,0,100,100);

     //connect(this->horizontalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(sliderMove()));
     //connect(this->verticalScrollBar(), SIGNAL(valueChanged(int)), this, SLOT(sliderMove()));
}
// -----------------------------------------------------------------------------

// #brief Stes the given scene in the view
//
// #param __scene - pointer to the scene
// #return no value reurns.
void GSceneView::connectScene(hBaseScene* __scene)
{
     QGraphicsScene* currentScene = scene();
     if(currentScene != NULL)
          this->disconnect(currentScene);

     connect(this, SIGNAL(viewChanged()), __scene, SLOT(AdaptSceneSize()));
     setScene(__scene);
}
// -----------------------------------------------------------------------------

// #brief Rotates GSceneLocalizer view.
// #param __angle - A angle of rotation.
//
// #return no value reurns.
void GSceneView::rotate(qreal __angle)
{
	QGraphicsView::rotate(__angle);
	emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Sets the mode of GSceneView.
//
// #param __mod - Mode to set (enum parameter).
// #return no value reurns.
void GSceneView::setViewMode(viewMode __mod)
{
	fViewMode = __mod;
	if(__mod == GSceneView::Normal) 
     {
		setDragMode(QGraphicsView::NoDrag);
		this->setCursor(Qt::ArrowCursor);
	}
	if(__mod == GSceneView::HandDrag) 
     {
		setDragMode(QGraphicsView::ScrollHandDrag);
	}
	if(__mod == GSceneView::Zoom) 
     {
		setDragMode(QGraphicsView::NoDrag);
		this->setCursor(QCursor(QPixmap(":/Icons/ZoomIn.png"), 5, 5));
	}
}
// -----------------------------------------------------------------------------

// #brief Returns the area of a scene that is visible in View.
//
// #return the area of a scene that is visible in View.
QRectF GSceneView::getViewRect(void)
{
	float left, right, top, bottom;
	QRectF rect;

     int w = 12;
	left = mapToScene(w,w).x();
	right = mapToScene(width()-w, height()-w).x();
	top = mapToScene(w,w).y();
	bottom = mapToScene(width()-w, height()-w).y();
	rect = QRectF(left, top, right-left, bottom-top);

	return rect;
}
// -----------------------------------------------------------------------------

// #brief Zooms in the main view.
//
// #param __sR - Scale ratio.
// #return no value reurns.
void GSceneView::zoomIn(float __sR)
{
	scale(__sR, __sR);
	emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Zooms out the main view.
//
// #param __sR - Scale ratio.
// #return no value reurns.
void GSceneView::zoomOut(float __sR)
{
	scale(1./__sR, 1./__sR);
	emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Reimplementation of update function.
//
// #return no value reurns.
void GSceneView::update(void)
{
	QGraphicsView::update();
	emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when you move mouse over the object.
//
// #param __event - the pointer to the object that holds all the event parameters
// #return no value reurns.
void GSceneView::mouseMoveEvent(QMouseEvent* __event)
{
	QGraphicsView::mouseMoveEvent(__event);
	if((__event->buttons() & Qt::LeftButton) && (fViewMode == GSceneView::HandDrag)) 
          emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when you use wheel.
//
// #param __event - the pointer to the object that holds all the event parameters
// #return no value reurns.
void GSceneView::wheelEvent(QWheelEvent* __event)
{
	int ev;

	setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
	ev = __event->delta();
     
	if(ev > 0) 
          zoomIn(fScaleRatio);
	else 
          zoomOut(fScaleRatio);

	setTransformationAnchor(QGraphicsView::NoAnchor);
	emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when you press mouse button.
//
// #param __event - the pointer to the object that holds all the event parameters
// #return no value reurns.
void GSceneView::mousePressEvent(QMouseEvent* __event)
{
	if(fViewMode == Zoom)
     {
		setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
		if(__event->button()==Qt::LeftButton) 
               zoomIn(fScaleRatio);
		if(__event->button()==Qt::RightButton) 
               zoomOut(fScaleRatio);
		setTransformationAnchor(QGraphicsView::NoAnchor);
	}
	else 
          QGraphicsView::mousePressEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Implementation of virtual function which is call when the widget size changed.
//
// #param __event - the pointer to the object that holds all the event parameters
// #return no value reurns.
void GSceneView::resizeEvent(QResizeEvent* __event)
{
	update();
	QWidget::resizeEvent(__event);
     emit viewChanged();
     emit viewChanged(getViewRect());
}
// -----------------------------------------------------------------------------

// #brief Sets the visible area of the scene in view to __rect
//
// #param rect Rectangle to set as view rectangle.
// #return no value reurns.
void GSceneView::setViewRect(QRectF  __rect)
{
	fitInView(__rect, Qt::KeepAspectRatio);
	QGraphicsView::update();
}
// -----------------------------------------------------------------------------

// #brief The function that is called when slider is changed
// 
// #return no value reurns.
void GSceneView::sliderMove(void)
{
	update();
}
// -----------------------------------------------------------------------------
