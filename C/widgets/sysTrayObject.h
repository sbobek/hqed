 /**
 * \file	sysTrayObject.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	29.01.2008 
 * \version	1.0
 * \brief	This file defines class that is derived from an element being reprezentation of program in system resourece.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef SYSTRAYOBJECTH
#define SYSTRAYOBJECTH
// -----------------------------------------------------------------------------

#include <QSystemTrayIcon>
// -----------------------------------------------------------------------------

class QAction;
// -----------------------------------------------------------------------------
/**
* \class 	sysTrayObject
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	29.01.2008
* \brief 	This is derived class from an element being reprezentation of program in system resourece.
*/
class sysTrayObject : public QSystemTrayIcon
{
	Q_OBJECT
    
	QString fLastTip;	///< String containing last found tip.
    
	QMenu* trayIconMenu;	///< Pointer to menu of system resource.
	
	QAction* trayMenu_netxTip;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_restore;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_minimize;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_maximize;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_options;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_about;	///< Pointer to instruction from menu of system resource.
	QAction* trayMenu_quit;		///< Pointer to instruction from menu of system resource.
     
public:

     /// \brief Constructor sysTrayObject class.
     ///
     /// \param oParent Pointer to parent object.
     sysTrayObject(QWidget* oParent = 0);
	
     /// \brief Destructor About_ui class.
     ~sysTrayObject(void);
	
	/// \brief Show random tip about using program.
	///
	/// \param _appMode Defines mode of program.
	/// \return No return value.
	void drawTip(int _appMode);
	
	/// \brief Show last random tip about using program.
	///
	/// \return No return value.
	void showDrawedTip(void);
	
	
public slots:

	/// \brief Show next random tip about using program.
	///
	/// \return No return value.
	void onNextTip(void);
	
	/// \brief Show window in normal size.
	///
	/// \return No return value.
	void onRestore(void);
	
	/// \brief Show window minimized.
	///
	/// \return No return value.
	void onMinimize(void);
	
	/// \brief Show window maximized.
	///
	/// \return No return value.
	void onMaximize(void);
	
	/// \brief Show configuration window.
	///
	/// \return No return value.
	void onOptions(void);
	
	/// \brief Show window containing information about program.
	///
	/// \return No return value.
	void onAbout(void);
	
	/// \brief Close window of program.
	///
	/// \return No return value.
	void onQuit(void);
	
	/// \brief Show message-box containig tip.
	///
	/// \return No return value.
	void onMessageClick(void);
	
	/// \brief Set eneble of context menu.
	///
	/// \return No return value.
	void onShowMenu(void);
	
	/// \brief Set reason for activating icon in system resource.
	///
	/// \param reason Reason for hiding icon in system resource.
	/// \return No return value.
	void onActivate(QSystemTrayIcon::ActivationReason reason);
};
// -----------------------------------------------------------------------------
#endif
