/**
 * \file	sysTrayObject.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	29.01.2008 
 * \version	1.0
 * \brief	This file defines class that is derived from an element being reprezentation of program in system resourece.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>

#include <QTime>
#include <QAction>

#include "../About_ui.h"
#include "../MainWin_ui.h"
#include "sysTrayObject.h"
// -----------------------------------------------------------------------------

// #brief Constructor sysTrayObject class.
//
// #param parent Pointer to parent object.
sysTrayObject::sysTrayObject(QWidget* oParent) : QSystemTrayIcon(oParent)
{	
	setIcon(QIcon(":/all_images/images/mainico.png"));
	setToolTip(AboutWin->appVersion());
	
	// Create menu.
	trayIconMenu = new QMenu(NULL);
	
	trayMenu_netxTip = new QAction(QIcon(":/all_images/images/blub.png"), tr("&Next tip"), NULL);
	trayMenu_minimize = new QAction(QIcon(":/all_images/images/minimize.png"), tr("&Minimize"), NULL);
	trayMenu_maximize = new QAction(QIcon(":/all_images/images/maximize.png"), tr("Ma&ximize"), NULL);
	trayMenu_restore = new QAction(QIcon(":/all_images/images/restore.png"), tr("&Restore"), NULL);
	trayMenu_options = new QAction(QIcon(":/all_images/images/settings.png"), tr("&Options"), NULL);
	trayMenu_about = new QAction(QIcon(":/all_images/images/About.png"), tr("&About"), NULL);
	trayMenu_quit = new QAction(QIcon(":/all_images/images/Quit.png"), tr("&Quit"), NULL);
	
	QFont nextTipFont = trayMenu_netxTip->font();
	nextTipFont.setBold(true);
	trayMenu_netxTip->setFont(nextTipFont);
	
	trayIconMenu->addAction(trayMenu_netxTip);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(trayMenu_minimize);
	trayIconMenu->addAction(trayMenu_maximize);
	trayIconMenu->addAction(trayMenu_restore);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(trayMenu_options);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(trayMenu_about);
	trayIconMenu->addSeparator();
	trayIconMenu->addAction(trayMenu_quit);
	
	connect(trayMenu_minimize, SIGNAL(triggered()), this, SLOT(onMinimize()));
	connect(trayMenu_maximize, SIGNAL(triggered()), this, SLOT(onMaximize()));
	connect(trayMenu_restore, SIGNAL(triggered()), this, SLOT(onRestore()));
	connect(trayMenu_options, SIGNAL(triggered()), this, SLOT(onOptions()));
	connect(trayMenu_about, SIGNAL(triggered()), this, SLOT(onAbout()));
	connect(trayMenu_quit, SIGNAL(triggered()), this, SLOT(onQuit()));
	connect(trayMenu_netxTip, SIGNAL(triggered()), this, SLOT(onNextTip()));
	connect(this, SIGNAL(messageClicked()), this, SLOT(onMessageClick()));
	connect(trayIconMenu, SIGNAL(aboutToShow()), this, SLOT(onShowMenu()));
	connect(this, SIGNAL(activated(QSystemTrayIcon::ActivationReason)), this, SLOT(onActivate(QSystemTrayIcon::ActivationReason)));
	
	setContextMenu(trayIconMenu);
}
// -----------------------------------------------------------------------------

// #brief Destructor About_ui class.
sysTrayObject::~sysTrayObject(void)
{
	delete trayMenu_restore;
	delete trayMenu_minimize;
	delete trayMenu_maximize;
	delete trayMenu_options;
	delete trayMenu_about;
	delete trayMenu_quit;
	delete trayMenu_netxTip;
	delete trayIconMenu;
}
// -----------------------------------------------------------------------------

// #brief Show random tip about using program.
//
// #param _appMode Defines mode of program.
// #return No return value.
void sysTrayObject::drawTip(int _appMode)
{
	QStringList ardTips;
	QStringList xttTips;
	QStringList othersTips;
	
	ardTips << "If you want to open the file quickly, use drag-and-drop technique. The type of file will be regognized automatically";
	ardTips << "If you want to move the whole branch, choose the root of branch, press ctrl button and move it.";
	ardTips << "If you want to shorten/extend level, press the plus/minus button associated with the appropriate level.";
	ardTips << "If you want to switch to XTT mode, press the F12 button.";
	ardTips << "To open the file from command line, just type: \"hqed filename\". The type of file will be regognized automatically";
	
	xttTips << "If you want to open the file quickly, use drag-and-drop technique. The type of file will be regognized automatically";
	xttTips << "If you want to switch to ARD mode, press the F11 button.";
	xttTips << "To open the file from command line, just type: \"hqed filename\". The type of file will be regognized automatically";
	xttTips << "In order to create a new connection between rows, you can drag an appropriate input/output pin on the other an appropriate output/input pin.";
	xttTips << "If you want to check which of defined attributes are not used, choose the action: Atributes->Unused attributes...";
	
	othersTips << "Others Tip 1";
	othersTips << "Others Tip 2";
	othersTips << "Others Tip 3";
	othersTips << "Others Tip 4";
	
	// Random tip.
	qsrand((unsigned int)QTime::currentTime().msec());
	if(_appMode == ARD_MODE)
		fLastTip = ardTips.at(qrand() % ardTips.size());
	if(_appMode == XTT_MODE)
		fLastTip = xttTips.at(qrand() % xttTips.size());
	if(_appMode == -1)
		fLastTip = othersTips.at(qrand() % othersTips.size());
}
// -----------------------------------------------------------------------------

// #brief Show last random tip about using program.
//
// #return No return value.
void sysTrayObject::showDrawedTip(void)
{
	int durationTime = 10000;
	QString title = "Tool Tip";
	showMessage(title, fLastTip, Information, durationTime);
}
// -----------------------------------------------------------------------------

// #brief Show message-box containig tip.
//
// #return No return value.
void sysTrayObject::onMessageClick(void)
{
	QMessageBox::information(NULL, "HQEd", fLastTip, QMessageBox::Ok);
}
// -----------------------------------------------------------------------------

// #brief Show next random tip about using program.
//
// #return No return value.
void sysTrayObject::onNextTip(void)
{
	drawTip(MainWin->currentMode());
	showDrawedTip();
}
// -----------------------------------------------------------------------------

// #brief Show window in normal size.
//
// #return No return value.
void sysTrayObject::onRestore(void)
{
	MainWin->showNormal();
}
// -----------------------------------------------------------------------------

// #brief Show window minimized.
//
// #return No return value.
void sysTrayObject::onMinimize(void)
{
	MainWin->showMinimized();
}
// -----------------------------------------------------------------------------

// #brief Show window maximized.
//
// #return No return value.
void sysTrayObject::onMaximize(void)
{	
	MainWin->showMaximized();
}
// -----------------------------------------------------------------------------

// #brief Show configuration window.
//
// #return No return value.
void sysTrayObject::onOptions(void)
{
	MainWin->showConfigurationDialog();
}
// -----------------------------------------------------------------------------

// #brief Show window containing information about program.
//
// #return No return value.
void sysTrayObject::onAbout(void)
{
	MainWin->ShowAboutWin();
}
// -----------------------------------------------------------------------------

// #brief Close window of program.
//
// #return No return value.
void sysTrayObject::onQuit(void)
{
	MainWin->CloseForm();
}
// -----------------------------------------------------------------------------

// #brief Set eneble of context menu.
//
// #return No return value.
void sysTrayObject::onShowMenu(void)
{
	trayMenu_minimize->setEnabled(!MainWin->isMinimized());
	trayMenu_maximize->setEnabled(!MainWin->isMaximized());
	trayMenu_restore->setEnabled(MainWin->isMinimized() || MainWin->isMaximized());
}
// -----------------------------------------------------------------------------

// #brief Set reason for activating icon in system resource.
//
// #param reason Reason for hiding icon in system resource.
// #return No return value.
void sysTrayObject::onActivate(QSystemTrayIcon::ActivationReason reason)
{
	switch(reason) 
	{
		case QSystemTrayIcon::Trigger:
			contextMenu()->exec(QCursor::pos()); 
		break;
		
		case QSystemTrayIcon::DoubleClick:
		case QSystemTrayIcon::MiddleClick:
			onNextTip();
		break;
		
		default:
			contextMenu()->exec(QCursor::pos()); 
		break;
     }
}
// -----------------------------------------------------------------------------
