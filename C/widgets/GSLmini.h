 /**
 * \file	GSLmini.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.01.2009
 * \version	1.0
 * \brief	This file contains declaration of GSLmini class.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GSLMINI_H_
#define GSLMINI_H_
// -----------------------------------------------------------------------------

#include <QtGui>
#include <QtGui/QWidget>
// -----------------------------------------------------------------------------

/**
* \class 	GSLmini
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.01.2009
* \brief 	This class provides miniature view for GSceneLocalizator widget.
*/
class GSLmini : public QGraphicsView 
{
private:
     
	Q_OBJECT
     
     bool fLockRectangle;                ///< The flag variable that is used to locking the size of the rectangle
     QRectF fMiniRectangle;              ///< Rectangle which marks on the miniature the state of main view.
	float fScaleRatio;                  ///< Scale ratio parameter for scaling and zooming.

     /// \brief Sets view rectangle position.
     ///
     /// \param __x - X coordinate of center point - in scene coordinates.
     /// \param __y - Y coordinate of center point - in scene coordinates.
     /// \return No value returns
	void setRectPosition(qreal __x, qreal __y);

     /// \brief Sets view rectangle position.
     ///
     /// \param __x - X coordinate of center point - in view coordinates.
     /// \param __y - Y coordinate of center point - in view coordinates.
     /// \return no values return
	void moveRect(int __x, int __y);

     /// \brief returns the point that is the center of the rectangle
     ///
     /// \return returns the point that is the center of the rectangle
	QPointF getCenter(void);

public:

     /// \brief Constructs a GSLmini
     ///
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
	GSLmini(QWidget* __parent);

     /// \brief Constructs a GSLmini and sets the visualized scene.
     ///
     /// \param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
	GSLmini(QGraphicsScene* __scene, QWidget* __parent);

     /// \brief Descrutor of the GSLmini class
	~GSLmini(void);

     /// \brief Draws "miniFrame" rectangle on the miniature view foreground.
     ///
     /// \param __painter - the canvas where the foreground will be painted
     /// \param __rect - Exposed rectangle.
     /// \return no value return
	void drawForeground(QPainter* __painter, const QRectF& __rect);

     /// \brief Sets the rectangle which marks on the miniature the state of main view.
     ///
     /// \param __rect - Rectangle to set.
     /// \return no value return
	void setMiniRectangle(const QRectF& __rect);
      
     /// \brief Implementation of virtual function which is call when you press mouse button.
     ///
     /// \param __event - the pointer to the object that holds the data of mouse event
     /// \return no value return
	void mousePressEvent(QMouseEvent* __event);

     /// \brief Implementation of virtual function which is call when you move mouse.
     ///
     /// \param __event - the pointer to the object that holds the data of mouse event
     /// \return no value return
	void mouseMoveEvent(QMouseEvent* __event);

     /// \brief Implementation of virtual function which is call when you use wheel.
     ///
     /// \param __event - the pointer to the object that holds the data of mouse event
     /// \return no value return
	void wheelEvent(QWheelEvent* __event);

     /// \brief Reimplementation of update function.
     ///
     /// \return no value return
	void update(void);

     /// \brief Sets scene of GSceneLocalizator.
     ///
     /// \param __scene - Pointer to scene.
     /// \return no value return
	void setScene(QGraphicsScene* __scene);

signals:

     /// \brief The signal that is emited when the mouse action change the rectangle (movement, zoom, etc)
     ///
     /// \param __rect - the rect
     /// \return no value return
	void mouseAction(QRectF __rect);
};
// -----------------------------------------------------------------------------

#endif
