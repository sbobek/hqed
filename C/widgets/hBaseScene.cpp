/*
 *	   $Id: hBaseScene.cpp,v 1.1.2.1 2011-06-03 14:03:27 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QGraphicsView>

#include "../Settings_ui.h"
#include "../MainWin_ui.h"
#include "hBaseScene.h"
// -----------------------------------------------------------------------------

// #brief Constructor hBaseScene class.
//
// #param __parent - Pointer to parent object.
hBaseScene::hBaseScene(QObject* __parent) : QGraphicsScene(__parent)
{
     setNewItem(NULL);
     fCurrentAngle = 0;
}
// -----------------------------------------------------------------------------

// #brief Function sets object on the scene.
//
// #param _item Pointer to object that is set on the scene.
// #return No return value.
void hBaseScene::setNewItem(QGraphicsItem* _item)
{
     fNewItem = _item;
}
// -----------------------------------------------------------------------------

// #brief Function gets object that is set on the scene.
//
// #return NPointer to object that is set on the scene.
QGraphicsItem* hBaseScene::newItem(void)
{
     return fNewItem;
}
// -----------------------------------------------------------------------------

// #brief Function resets pointer to the object that is set on the scene.
//
// #return No return value.
void hBaseScene::resetNewItem(void)
{
     setNewItem(NULL);
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when angle of scene rotation is changed.
//
// #param Angle value.
// #return Angle of scene rotation.
void hBaseScene::Rotate(int angle)
{
     QList<QGraphicsView*> views_list = views();

     for(int i=0;i<views_list.count();i++)
          views_list.at(i)->rotate(angle-fCurrentAngle);

     fCurrentAngle = angle;
     //rotateAngleEdit->setValue(angle);
}
// -----------------------------------------------------------------------------

// #brief Function zoom in on the scene.
//
// #return No return value.
void hBaseScene::zin(void)
{
     double factor = (Settings->zoomFactorValue(MainWin->currentMode())/100.0)+1.0;
     scaleView(factor);
}
// -----------------------------------------------------------------------------

// #brief Function zoom out on the scene.
//
// #return No return value
void hBaseScene::zout(void)
{
     double factor = (Settings->zoomFactorValue(MainWin->currentMode())/100.0)+1.0;
     scaleView(1/factor);
}
// -----------------------------------------------------------------------------

// #brief Function zoom scene.
//
// #param scaleFactor Zoom value.
// #return Angle of scene rotation.
void hBaseScene::scaleView(qreal scaleFactor)
{
     QList<QGraphicsView*> views_list = views();

     for(int i=0;i<views_list.count();i++)
     {
          qreal factor = views_list.at(i)->matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
          if (factor < 0.007 || factor > 100)
               continue;
          views_list.at(i)->scale(scaleFactor, scaleFactor);
     }
}
// -----------------------------------------------------------------------------

// #brief Function adapts scene size to the size of elements.
//
// #return No return value.
void hBaseScene::AdaptSceneSize(void)
{
     const int Margin = 50;

     QList<QGraphicsItem*> items_list = items();

     float minLeft = 0;
     float minTop = 0;
     float maxLeft = 0;
     float maxTop = 0;

     for(int i=0;i<items_list.size();i++)
     {
          float minl = items_list.at(i)->x();
          float maxl = items_list.at(i)->x() + items_list.at(i)->boundingRect().width();
          float mint = items_list.at(i)->y();
          float maxt = items_list.at(i)->y() + items_list.at(i)->boundingRect().height();

          if(minLeft > minl)
               minLeft = minl;
          if(minTop > mint)
               minTop = mint;
          if(maxLeft < maxl)
               maxLeft = maxl;
          if(maxTop < maxt)
               maxTop = maxt;
     }

     minLeft -= Margin;
     minTop -= Margin;
     maxLeft += Margin;
     maxTop += Margin;

     QList<QGraphicsView*> v = views();
     for(int i=0;i<v.size();++i)
          v.at(i)->setSceneRect(minLeft, minTop, maxLeft-minLeft, maxTop-minTop);
}
// -----------------------------------------------------------------------------

// #brief Function moves elements on the scene up.
//
// #return No return value.
void hBaseScene::MoveItemsUp(void)
{
     int moveDistance = Settings->xttMoveElementsDistance();
     setSceneRect(sceneRect().x(), sceneRect().y()+moveDistance, sceneRect().width(), sceneRect().height()-moveDistance);
}
// -----------------------------------------------------------------------------

// #brief Function moves elements on the scene down.
//
// #return No return value.
void hBaseScene::MoveItemsDown(void)
{
     int moveDistance = Settings->xttMoveElementsDistance();
     setSceneRect(sceneRect().x(), sceneRect().y()-moveDistance, sceneRect().width(), sceneRect().height()+moveDistance);
}
// -----------------------------------------------------------------------------

// #brief Function moves elements on the scene right.
//
// #return No return value.
void hBaseScene::MoveItemsRight(void)
{
     int moveDistance = Settings->xttMoveElementsDistance();
     setSceneRect(sceneRect().x()-moveDistance, sceneRect().y(), sceneRect().width()+moveDistance, sceneRect().height());
}
// -----------------------------------------------------------------------------

// #brief Function moves elements on the scene left.
//
// #return No return value.
void hBaseScene::MoveItemsLeft(void)
{
     int moveDistance = Settings->xttMoveElementsDistance();
     setSceneRect(sceneRect().x()+moveDistance, sceneRect().y(), sceneRect().width()-moveDistance, sceneRect().height());
}
// -----------------------------------------------------------------------------
