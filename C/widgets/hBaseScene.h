/**
 * \file	hBaseScene.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	03.06.2011
 * \version	1.0
 * \brief	This file contains class definition that is a base GraphicsScene for each scene used in the code.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
// -----------------------------------------------------------------------------

#ifndef HBASESCENE_H
#define HBASESCENE_H
// -----------------------------------------------------------------------------

#include <QGraphicsScene>
// -----------------------------------------------------------------------------

/**
* \class 	hBaseScene
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	03.06.2011
* \brief 	Class definition that is a base GraphicsScene for each scene used in the code.
*/
class hBaseScene : public QGraphicsScene
{
     Q_OBJECT
protected:

     int fCurrentAngle;			     ///< Angle of scene rotation.
     QGraphicsItem* fNewItem;	          ///< Defines object that has to be set on the scene.

public:

     /// \brief Constructor hBaseScene class.
     ///
     /// \param __parent - Pointer to parent object.
     explicit hBaseScene(QObject* __parent = 0);

     /// \brief Function sets object on the scene.
     ///
     /// \param _item Pointer to object that is set on the scene.
     /// \return No return value.
     void setNewItem(QGraphicsItem* _item);

     /// \brief Function gets object that is set on the scene.
     ///
     /// \return NPointer to object that is set on the scene.
     QGraphicsItem* newItem(void);

     /// \brief Function resets pointer to the object that is set on the scene.
     ///
     /// \return No return value.
     void resetNewItem(void);

     /// \brief Function gets angle of scene rotation.
     ///
     /// \return Angle of scene rotation.
     int currentAngle(void){ return fCurrentAngle; }

     /// \brief Function zoom scene.
     ///
     /// \param scaleFactor Zoom value.
     /// \return Angle of scene rotation.
     void scaleView(qreal scaleFactor);

public slots:

     /// \brief Function is triggered when angle of scene rotation is changed.
     ///
     /// \param Angle value.
     /// \return Angle of scene rotation.
     void Rotate(int angle);

     /// \brief Function zoom out on the scene.
     ///
     /// \return No return value.
     void zout(void);

     /// \brief Function zoom in on the scene.
     ///
     /// \return No return value.
     void zin(void);

     /// \brief Function updates current angle of scene rotation.
     ///
     /// \param _ca Angle value.
     /// \return No return value.
     void setCurrentAngle(int _ca){ fCurrentAngle = _ca; }

     /// \brief Function adapts scene size to the size of elements.
     ///
     /// \return No return value.
     virtual void AdaptSceneSize(void);

     /// \brief Function moves elements on the scene right.
     ///
     /// \return No return value.
     void MoveItemsRight(void);

     /// \brief Function moves elements on the scene left.
     ///
     /// \return No return value.
     void MoveItemsLeft(void);

     /// \brief Function moves elements on the scene up.
     ///
     /// \return No return value.
     void MoveItemsUp(void);

     /// \brief Function moves elements on the scene down.
     ///
     /// \return No return value.
     void MoveItemsDown(void);
};
// -----------------------------------------------------------------------------

#endif // HBASESCENE_H
