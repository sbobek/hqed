/*
 *	  $Id: GSceneLocalizator.cpp,v 1.2 2010-01-08 19:47:30 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include "GSceneLocalizator.h"
// -----------------------------------------------------------------------------

// #brief Constructs of the GSceneLocalizator class.
//
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
GSceneLocalizator::GSceneLocalizator(QWidget* __parent)
: QDialog(__parent)
{
     fUi.setupUi(this);
     //setWindowFlags(Qt::Tool);

     fMiniView = new GSLmini(this);

     fVerticalLayout = new QVBoxLayout(this);
     fVerticalLayout->setSpacing(0);
     fVerticalLayout->setMargin(0);
     fVerticalLayout->setObjectName(QString::fromUtf8("VerticalLayout"));

     fVerticalLayout->addWidget(fMiniView);

     connect(fMiniView, SIGNAL(mouseAction(QRectF)), this, SLOT(ifMousePressed(QRectF)));
}
// -----------------------------------------------------------------------------

// #brief Destructor.
GSceneLocalizator::~GSceneLocalizator()
{
}
// -----------------------------------------------------------------------------

// #brief Sets scene of GSceneLocalizator.
//
// #param __scene - Pointer to scene.
// #return no value return
void GSceneLocalizator::setScene(QGraphicsScene* __scene)
{
	fMiniView->setScene(__scene);
}
// -----------------------------------------------------------------------------

// #brief Reimplementation of resizeEvent.
//
// #param __event - the pointer to the object that holds all the parameters/information about resize event
// #return no value return
void GSceneLocalizator::resizeEvent(QResizeEvent* __event)
{
	fMiniView->update();
	QWidget::resizeEvent(__event);
}
// -----------------------------------------------------------------------------

// #brief Sets view rectangle.
//
// #param __rect - Actual view rectangle.
// #return no value return
void GSceneLocalizator::setFrameRect(QRectF __rect)
{
	fMiniView->setMiniRectangle(__rect);
	fMiniView->update();
}
// -----------------------------------------------------------------------------

// #brief Emits external signal when you click on miniature view.
//
// #param __rect - Actual view rectangle.
// #return no value return
void GSceneLocalizator::ifMousePressed(QRectF __rect)
{
	emit framePositionChanged(__rect);
}
// -----------------------------------------------------------------------------
