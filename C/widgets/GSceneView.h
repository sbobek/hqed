 /**
 * \file	GSceneView.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.01.2009
 * \version	1.0
 * \brief	This file contains declaration of GSceneView class.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GSCENEVIEW_H_
#define GSCENEVIEW_H_
// -----------------------------------------------------------------------------

#include <QtGui>
#include <QtGui/QWidget>
#include <QGraphicsView>
// -----------------------------------------------------------------------------

#include "C/ARD/widgets/ardGraphicsScene.h"
#include "C/XTT/widgets/MyGraphicsScene.h"
// -----------------------------------------------------------------------------

/**
* \class 	GSceneView
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.01.2009
* \brief 	Contains definitions and methods implementations of GSceneView class.
*/
class GSceneView : public QGraphicsView
{

public:

     /// \brief This enums describes the mode of GSceneView.
     enum viewMode
     {
          Normal = 1,    ///<Normal Normal mode.
          HandDrag = 2,  ///<HandDrag Hand draging mode. Lets you to grab the view and drag it. Its lets you navigate the scene.
          Zoom = 3       ///<Zoom Zooming mode. Lets you to zoom view clicking or by wheel.
     };
     
private:

	Q_OBJECT
     
     viewMode fViewMode;                ///< Actually set mode.
	float fScaleRatio;                 ///< Scale ratio parameter for scaling and zooming.
     
protected:

     /// \brief Implementation of virtual function which is call when you move mouse over the object.
     ///
     /// \param __event - the pointer to the object that holds all the event parameters
     /// \return no value reurns.
     void mouseMoveEvent(QMouseEvent* __event);

     /// \brief Implementation of virtual function which is call when you use wheel.
     ///
     /// \param __event - the pointer to the object that holds all the event parameters
     /// \return no value reurns.
     void wheelEvent(QWheelEvent* __event);

     /// \brief Implementation of virtual function which is call when you press mouse button.
     ///
     /// \param __event - the pointer to the object that holds all the event parameters
     /// \return no value reurns.
     void mousePressEvent(QMouseEvent* __event);

     /// \brief Implementation of virtual function which is call when the widget size changed.
     ///
     /// \param __event - the pointer to the object that holds all the event parameters
     /// \return no value reurns.
     void resizeEvent(QResizeEvent* __event);

public:

     /// \brief Constructs a GSceneView
     ///
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
     GSceneView(QWidget* __parent = 0);

     /// \brief Constructs a GSceneView and sets the visualized scene.
     ///
     /// \param __scene - Pointer to QGraphicsScene object which ardGraphicsScene displays.
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
     GSceneView(hBaseScene* __scene, QWidget* __parent = 0);

     /// \brief Destructor.
     ~GSceneView(void);

     /// \brief Initializes scene
     ///
     /// \return no value reurns.
     void initScene(void);

     /// \brief Rotates GSceneLocalizer view.
     /// \param __angle - A angle of rotation.
     ///
     /// \return no value reurns.
     void rotate(qreal __angle);

     /// \brief Sets the mode of GSceneView.
     ///
     /// \param __mod - Mode to set (enum parameter).
     /// \return no value reurns.
     void setViewMode(viewMode __mod);
   
     /// \brief Returns the area of a scene that is visible in View.
     ///
     /// \return the area of a scene that is visible in View.
     QRectF getViewRect(void);

     /// \brief Zooms in the main view.
     ///
     /// \param __sR - Scale ratio.
     /// \return no value reurns.
     void zoomIn(float __sR);

     /// \brief Zooms out the main view.
     ///
     /// \param __sR - Scale ratio.
     /// \return no value reurns.
     void zoomOut(float __sR);

     /// \brief Reimplementation of update function.
     ///
     /// \return no value reurns.
     void update(void);

     /// \brief Stes the given scene in the view
     ///
     /// \param __scene - pointer to the scene
     /// \return no value reurns.
     void connectScene(hBaseScene* __scene);

public slots:

     /// \brief Sets the visible area of the scene in view to __rect
     ///
     /// \param __rect Rectangle to set as view rectangle.
     /// \return no value reurns.
     void setViewRect(QRectF __rect);
     
     /// \brief The function that is called when slider is changed
     /// 
     /// \return no value reurns.
     void sliderMove(void);

signals:

     /// \brief Signal emited when view changed.
     ///
     /// \param __rect - Actual view rectangle.
     /// \return no value reurns.
     void viewChanged(QRectF __rect);

     /// \brief Signal emited when view changed.
     ///
     /// \return no value reurns.
     void viewChanged(void);
};
// -----------------------------------------------------------------------------
#endif
