 /**
 * \file	GSceneLocalizator.h
 * \author Krzysztof Kaczor kinio4444@gmail.com
 * \date 	17.01.2009
 * \version	1.0
 * \brief	This file contains declaration of GSceneLocalizator class that is a widget on which the navigation scene is placed.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef GSCENELOCALIZATOR_H
#define GSCENELOCALIZATOR_H
// -----------------------------------------------------------------------------

#include <QtGui/QWidget>
#include <QtGui/QDialog>

#include "ui_GSceneLocalizator.h"
#include "GSLmini.h"
// -----------------------------------------------------------------------------

/**
* \class 	GSceneLocalizator
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	17.01.2009
* \brief 	This class provides a widget on which the navigation scene is placed.
*/
class GSceneLocalizator : public QDialog
{
private:

    Q_OBJECT
    
    Ui::GSceneLocalizatorClass fUi;     ///< The pointer to the user interface class of this widget
    QLayout* fVerticalLayout;           ///< Main widget layout.
    GSLmini* fMiniView;                 ///< The scene that displays the mini view that is used for navigation
    QRectF fFrameRect;                  ///< View rectangle.

public:

     /// \brief Constructs of the GSceneLocalizator class.
     ///
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
     GSceneLocalizator(QWidget* __parent = 0);

     /// \brief Destructor.
     ~GSceneLocalizator(void);

     /// \brief Sets scene of GSceneLocalizator.
     ///
     /// \param __scene - Pointer to scene.
     /// \return no value return
     void setScene(QGraphicsScene* __scene);

     /// \brief Reimplementation of resizeEvent.
     ///
     /// \param __event - the pointer to the object that holds all the parameters/information about resize event
     /// \return no value return
     void resizeEvent(QResizeEvent* __event);

public slots:

     /// \brief Sets view rectangle.
     ///
     /// \param __rect - Actual view rectangle.
     /// \return no value return
     void setFrameRect(QRectF __rect);

     /// \brief Emits external signal when you click on miniature view.
     ///
     /// \param __rect - Actual view rectangle.
     /// \return no value return
     void ifMousePressed(QRectF __rect);

signals:

     /// \brief Signal emited when position of size of view rectangle changed.
     ///
     /// \param __rect - the area where the frame is
     /// \return no value return
     void framePositionChanged(QRectF __rect);
};
// -----------------------------------------------------------------------------

#endif
