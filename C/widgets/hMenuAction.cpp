 /**
 * \file	hMenuAction.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	31.10.2009
 * \version	1.0
 * \brief	This file contains class definition of the hMenuAction class methods
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QMenu>
#include <QIcon>
#include <QObject>
#include <QAction>
#include <QCheckBox>
#include <QToolTip>
#include <QMessageBox>

#include "../../M/hSet.h"
#include "../../M/hType.h"
#include "../../M/hValue.h"
#include "../../M/hSetItem.h"
#include "../../M/hTypeList.h"
#include "../../M/hFunction.h"
#include "../../M/hFunctionList.h"
#include "../../M/hMultipleValue.h"

#include "../../M/XTT/XTT_Cell.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Column.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Attribute.h"
#include "../../M/XTT/XTT_Statesgroup.h"
#include "../../M/XTT/XTT_AttributeGroups.h"

#include "../../C/MainWin_ui.h"
#include "../../C/XTT/SetCreator_ui.h"
#include "../../C/XTT/CellEditor_ui.h"
#include "../../C/XTT/TableEditor_ui.h"
#include "../../C/XTT/AttributeEditor_ui.h"
#include "../../C/XTT/ExpressionEditor_ui.h"
#include "../../C/XTT/xttSimulationParams.h"
#include "../../C/XTT/StateSchemaEditor_ui.h"
#include "../../C/XTT/widgets/MyGraphicsScene.h"

#include "../../V/XTT/GTable.h"

#include "../../namespaces/ns_xtt.h"
#include "../../namespaces/ns_hqed.h"

#include "../devPluginInterface/devPlugin.h"
#include "../devPluginInterface/devPluginController.h"

#include "hMenuAction.h"
// -----------------------------------------------------------------------------

// #brief Constructor hMenuAction class.
//
// #param __action - string that contains the name of the action and its parameters
// #param parent Pointer to parent object.
hMenuAction::hMenuAction(QString __action, QObject* parent)
: QAction(parent)
{
     _action = __action;
     initObject();
}
// -----------------------------------------------------------------------------

// #brief Constructor hMenuAction class.
//
// #param __action - string that contains the name of the action and its parameters
// #param __title - button title
// #param parent Pointer to parent object.
hMenuAction::hMenuAction(QString __action, const QString& __title, QObject *parent)
: QAction(__title, parent)
{
     _action = __action;
     initObject();
}
// -----------------------------------------------------------------------------

// #brief Constructor hMenuAction class.
//
// #param __action - string that contains the name of the action and its parameters
// #param __icon - icon on the button
// #param __title - button title
// #param parent Pointer to parent object.
hMenuAction::hMenuAction(QString __action, const QIcon& __icon, const QString& __title, QObject *parent)
: QAction(__icon, __title, parent)
{
     _action = __action;
     initObject();
}
// -----------------------------------------------------------------------------

// #brief Destructor hMenuAction class.
hMenuAction::~hMenuAction(void)
{
     if(exprEditor != NULL)
          delete exprEditor;
}
// -----------------------------------------------------------------------------

// #brief Function that constructs the object and sets up the object parameters
//
// #return no values return
void hMenuAction::initObject(void)
{
     exprEditor = NULL;
     connect(this, SIGNAL(triggered()), this, SLOT(onClick()));
     connect(this, SIGNAL(hovered()), this, SLOT(onMouseOver()));
}
// -----------------------------------------------------------------------------

// #brief Function that allows for changing action executed by this button
//
// #param __action - string that contains the name of the action and its parameters
// #param no values return
void hMenuAction::setAction(QString __action)
{
     _action = __action;
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when the mouse cursor enter thr action area
//
// #return no values return
void hMenuAction::onMouseOver(void)
{
     QStringList params = _action.split(":");
     if(params.isEmpty())
          return;
     
     QString command = params.at(0);
     if(!command.compare("changecelloperator", Qt::CaseInsensitive))
     {
          QString operatorname = params.at(4);
          int operindex = funcList->indexOf(operatorname, true);
          if(operindex == -1)
               return;
          
          QPoint cpos = QCursor::pos();
          QPoint tran = QPoint(40, -20);
          cpos += tran;
          QToolTip::showText(cpos, funcList->at(operindex)->description());
     }
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when the action is clicked
//
// #return no values return
void hMenuAction::onClick(void)
{
     QStringList params = _action.split(":");
     if(params.isEmpty())
          return;
     
     QString command = params.at(0);
     if(!command.compare("neweditor", Qt::CaseInsensitive))
     {
         QString tableid = params.at(1);
         int ri = params.at(2).toInt();
         int ci = params.at(3).toInt();
         newRulesEditor(tableid, ri, ci);
     }
     else if(!command.compare("expressioneditor", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          int ci = params.at(3).toInt();
          editCell(tableid, ri, ci);
     }
     else if(!command.compare("edittable", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          editTable(tableid);
     }
     else if(!command.compare("celleditor", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          cellEditor(tableid);
     }
     else if(!command.compare("changecelloperator", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          int ci = params.at(3).toInt();
          QString operatorname = params.at(4);
          changeCellOperator(tableid, ri, ci, operatorname);
     }
     else if(!command.compare("resetcellcontent", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          int ci = params.at(3).toInt();
          resetCellContent(tableid, ri, ci);
     }
     else if(!command.compare("insertrowbefore", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          insertRowBefore(tableid, ri);
     }
     else if(!command.compare("insertrowafter", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          insertRowAfter(tableid, ri);
     }
     else if(!command.compare("addnewrow", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          addNewRow(tableid);
     }
     else if(!command.compare("stateaddnewrow", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          addNewState(statebasename);
     }
     else if(!command.compare("copyrowbefore", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          copyRowBefore(tableid, ri);
     }
     else if(!command.compare("copyrowafter", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          copyRowAfter(tableid, ri);
     }
     else if(!command.compare("addcopyofrow", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          addCopyOfRow(tableid, ri);
     }
     else if(!command.compare("addcopyofstate", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          int ri = params.at(2).toInt();
          addCopyOfState(statebasename, ri);
     }
     else if(!command.compare("moverowup", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          moveRowUp(tableid, ri);
     }
     else if(!command.compare("moverowdown", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          moveRowDown(tableid, ri);
     }
     else if(!command.compare("deleterow", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ri = params.at(2).toInt();
          deleteRow(tableid, ri);
     }
     else if(!command.compare("deletecol", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ci = params.at(2).toInt();
          deleteCol(tableid, ci);
     }
     else if(!command.compare("deletestate", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          int ri = params.at(2).toInt();
          deleteState(statebasename, ri);
     }
     else if(!command.compare("loadstate", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          int ri = params.at(2).toInt();
          loadState(statebasename, ri);
     }
     else if(!command.compare("starthqedsimulation", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          int ri = params.at(2).toInt();
          runSimulation("hqed", "hqed", "default", statebasename, ri);
     }
     else if(!command.compare("startdevpluginsimulation", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          int ri = params.at(2).toInt();
          QString enginename = params.at(3);
          QString simtype = params.at(4);
          runSimulation("devplugin", enginename, simtype, statebasename, ri);
     }
     else if(!command.compare("stateinputdeletecol", Qt::CaseInsensitive))
     {
          QString statebasename = params.at(1);
          XTT_Attribute* attr = hqed::clipboardXttAttribute()->top();
          deleteInputStateCol(statebasename, attr);
     }
     else if(!command.compare("deletetable", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          deleteTable(tableid);
     }
     else if(!command.compare("deletestategroup", Qt::CaseInsensitive))
     {
          QString satesgroup = params.at(1);
          deleteStatesgroup(satesgroup);
     }
     else if(!command.compare("editattribute", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ci = params.at(2).toInt();
          editAttribute(tableid, ci);
     }
     else if(!command.compare("stateeditattribute", Qt::CaseInsensitive))
     {
          if(hqed::clipboardXttAttribute()->isEmpty())
          {
               QMessageBox::critical(NULL, "HQEd", "No attribute selected.\nCurrent file: " + QString(__FILE__) + "\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
               return;
          }
          XTT_Attribute* attPtr = hqed::clipboardXttAttribute()->top();
          editAttribute(attPtr);
     }
     else if(!command.compare("stateeditschema", Qt::CaseInsensitive))
     {
          QString xttstategroupname = params.at(1);
          editStateSchema(xttstategroupname);
     }
     else if(!command.compare("addsetitem", Qt::CaseInsensitive))
     {
          if(hqed::clipboardSet()->isEmpty())
          {
               QMessageBox::critical(NULL, "HQEd", "No set selected.\nCurrent file: " + QString(__FILE__) + "\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
               return;
          }
          hSet* setPtr = hqed::clipboardSet()->top();
          QString from = params.at(1);
          QString to = params.at(2);
          addSetRange(setPtr, from, to);
     }
     else if(!command.compare("addmultiplevaluerange", Qt::CaseInsensitive))
     {
          if(hqed::clipboardMultipleValue()->isEmpty())
          {
               QMessageBox::critical(NULL, "HQEd", "No value selected.\nCurrent file: " + QString(__FILE__) + "\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
               return;
          }
          hMultipleValue* setPtr = hqed::clipboardMultipleValue()->top();
          QString from = params.at(1);
          QString to = params.at(2);
          addMultipleValueRange(setPtr, from, to);
     }
     else if(!command.compare("setcreator", Qt::CaseInsensitive))
     {
          if((hqed::clipboardType()->isEmpty()) || (hqed::clipboardSet()->isEmpty()))
          {
               QMessageBox::critical(NULL, "HQEd", "No type or set selected.\nCurrent file: " + QString(__FILE__) + "\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
               return;
          }
          hType* typePtr = hqed::clipboardType()->top();
          hSet* setPtr = hqed::clipboardSet()->top();
          QString sr = params.at(1);
          setCreator(typePtr, setPtr, sr.compare("true", Qt::CaseInsensitive) == 0);
     }
     else if(!command.compare("setremoveitem", Qt::CaseInsensitive))
     {
          if(hqed::clipboardSet()->isEmpty())
          {
               QMessageBox::critical(NULL, "HQEd", "No set selected.\nCurrent file: " + QString(__FILE__) + "\nCurrent line: " + QString::number(__LINE__), QMessageBox::Ok);
               return;
          }
          hSet* setPtr = hqed::clipboardSet()->top();
          int index = params.at(1).toInt();
          removeSetItem(setPtr, index);
     }
     else if(!command.compare("changetablesortcriterion", Qt::CaseInsensitive))
     {
          QString tableid = params.at(1);
          int ci = params.at(2).toInt();
          int sc = params.at(3).toInt();
          sortTable(tableid, ci, sc);
     }
     else
     {
          QMessageBox::critical(NULL, "HQEd", "hMenuAction::onClick\nNot supported command: " + command + "\nReceived string: " + _action, QMessageBox::Ok);
     }
}
// -----------------------------------------------------------------------------

// #brief Function shows cell contents window.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #param __colIndex - index of the column in the given table
// #return No return value.
void hMenuAction::editCell(QString __tableId, int __rowIndex, int __colIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
     
	if(__colIndex >= 0)
	{
	     XTT_Cell* cell = NULL;
	     cell = fParent->Row(__rowIndex)->Cell(__colIndex);
          
          if(exprEditor == NULL)
          {
               exprEditor = new ExpressionEditor_ui(NULL);
               connect(exprEditor, SIGNAL(windowClose()), this, SLOT(cellEditorClose()));
               connect(exprEditor, SIGNAL(okClicked()), this, SLOT(cellEditorConfirm()));
          }
          
          int smr = exprEditor->setMode(cell);
          if(smr == SET_WINDOW_MODE_FAIL)
          {
               QMessageBox::critical(NULL, "HQEd", "Incorrect expression editor mode.", QMessageBox::Ok);
               return;
          }
          if(smr == SET_WINDOW_MODE_OK_YOU_CAN_SHOW)
               exprEditor->show();
	}
}
//---------------------------------------------------------------------------

// #brief Function shows cell contents window.
//
// #param __tableId - table id
// #param ri - index of the row in the given table
// #param ci - index of the column in the given table
// #return No return value.
void hMenuAction::newRulesEditor(QString __tableId, int /*ri*/, int /*ci*/)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;

     if(exprEditor == NULL)
     {
          exprEditor = new ExpressionEditor_ui(NULL);
          connect(exprEditor, SIGNAL(windowClose()), this, SLOT(cellEditorClose()));
          connect(exprEditor, SIGNAL(okClicked()), this, SLOT(cellEditorConfirm()));
     }

     // TODO:

     exprEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function shows table editor window
//
// #param __tableId - table id
// #return No return value.
void hMenuAction::editTable(QString __tableId)
{
     int fParentIndex = TableList->IndexOfID(__tableId);
     if(fParentIndex == -1)
          return;
          
	delete TableEdit;
     TableEdit = new TableEditor_ui(false, fParentIndex, NULL);
     TableEdit->show();
}
//---------------------------------------------------------------------------

// #brief Function shows cell editor window
//
// #param __tableId - table id
// #return No return value.
void hMenuAction::cellEditor(QString __tableId)
{
     int fParentIndex = TableList->IndexOfID(__tableId);
     if(fParentIndex == -1)
          return;
          
	delete CellEdit;
     CellEdit = new CellEditor_ui(fParentIndex, NULL);
     CellEdit->show();
}
//---------------------------------------------------------------------------

// #brief Function called when cell editor is closed using the OK button
//
// #return No return value.
void hMenuAction::cellEditorConfirm(void)
{
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function called on cell editor close event
//
// #return No return value.
void hMenuAction::cellEditorClose(void)
{
}
//---------------------------------------------------------------------------

// #brief Function that changes the operator in the given cell
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #param __colIndex - index of the column in the given table
// #param __operatorinternalname - the internal representation of the operator
// #return No return value.
void hMenuAction::changeCellOperator(QString __tableId, int __rowIndex, int __colIndex, QString __operatorinternalname)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     if(__colIndex >= 0)
	{
	     XTT_Cell* cell = fParent->Row(__rowIndex)->Cell(__colIndex);
          cell->setCellOperator(__operatorinternalname);
          xtt::fullVerification();
          xtt::logicalFullVerification();
	}
}
//---------------------------------------------------------------------------

// #brief Function that sets the content of the cell to default
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #param __colIndex - index of the column in the given table
// #return No return value.
void hMenuAction::resetCellContent(QString __tableId, int __rowIndex, int __colIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     if(__colIndex >= 0)
	     fParent->Row(__rowIndex)->Cell(__colIndex)->createDefaultContent();
}
//---------------------------------------------------------------------------

// #brief Function moves one row up.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::moveRowUp(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	fParent->ExchangeRows(__rowIndex, __rowIndex-1);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function moves one row up.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::moveRowDown(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	fParent->ExchangeRows(__rowIndex, __rowIndex+1);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function moves one row up.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::insertRowAfter(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	fParent->insertRow(__rowIndex+1);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function moves one row up.
//
// #param __tableId - table id
// #return No return value.
void hMenuAction::addNewRow(QString __tableId)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     fParent->insertRow(fParent->RowCount());
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function adds a new state to the group
//
// #param __statebasename - basename of the state
// #return No return value.
void hMenuAction::addNewState(QString __statebasename)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }

     XTT_Statesgroup* destinationgroup = (*States)[__statebasename];
     destinationgroup->createdefaultState();
}
//---------------------------------------------------------------------------

// #brief Function add new row the end  of the table
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::insertRowBefore(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	fParent->insertRow(__rowIndex);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function copies one row before another.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::copyRowBefore(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     fParent->copyRow(__rowIndex, __rowIndex);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function copies one row after another.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::copyRowAfter(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     fParent->copyRow(__rowIndex, __rowIndex+1);
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function that add a new row that is a copy of the current one
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::addCopyOfRow(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     fParent->copyRow(__rowIndex, fParent->RowCount());
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function that add a new state that is a copy of the current one
//
// #param __statebasename - basename of the state
// #param __stateIndex - index of state in the group
// #return No return value.
void hMenuAction::addCopyOfState(QString __statebasename, int __stateIndex)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     
     XTT_Statesgroup* destinationgroup = (*States)[__statebasename];
     destinationgroup->createCopyOfState(__stateIndex);
}
//---------------------------------------------------------------------------

// #brief Function removes current row.
//
// #param __tableId - table id
// #param __rowIndex - index of the row in the given table
// #return No return value.
void hMenuAction::deleteRow(QString __tableId, int __rowIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	if(QMessageBox::question(NULL, "HQEd", "Are you really want to remove selected row?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		return;
	
	fParent->DeleteRow(__rowIndex);
     xtt::fullVerification();
     xtt::logicalTableVerification(fParent);
}
//---------------------------------------------------------------------------

// #brief Function removes current column.
//
// #param __tableId - table id
// #param __colIndex - index of the column in the given table
// #return No return value.
void hMenuAction::deleteCol(QString __tableId, int __colIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	if(QMessageBox::question(NULL, "HQEd", "Are you really want to remove selected column?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
		return;
		
	fParent->DeleteCol(__colIndex);
	if(fParent->ColCount() == 0)
		TableList->Delete(TableList->IndexOfID(__tableId));
     xtt::fullVerification();
     xtt::logicalTableVerification(fParent);
}
//---------------------------------------------------------------------------

// #brief Function that loads the given state into xtt model
//
// #param __statebasename - base name of the state groups from which the state should be removed
// #param __stateIndex - index of the state in the given state group
// #return No return value.
void hMenuAction::loadState(QString __statebasename, int __stateIndex)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     
     XTT_Statesgroup* stsgrp = (*States)[__statebasename];
     
     if(stsgrp->loadState(__stateIndex))
          QMessageBox::information(NULL, "HQEd", "The state has been loaded successfully.", QMessageBox::Ok);
     else
          QMessageBox::critical(NULL, "HQEd", "The state cannot be loaded successfully.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that executes the model simulation using given engine
//
// #param __enginetype - the type of the engine: hqed, devplugin
// #param __enginename - the name of the engine
// #param __simulationtype - the type of the simulation
// #param __statebasename - base name of the state groups from which the simulation will be executed
// #param __stateIndex - index of the state in the given state group
// #return No return value.
void hMenuAction::runSimulation(QString __enginetype, QString __enginename, QString __simulationtype, QString __statebasename, int __stateIndex)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     
     XTT_Statesgroup* stsgrp = (*States)[__statebasename];
     XTT_State* state = stsgrp->stateAt(__stateIndex);
     
     if(state == NULL)
     {
          QMessageBox::critical(NULL, "HQEd", "Cannot find state \'" + QString::number(__stateIndex) + "\' in the \'" + __statebasename + "\' group.", QMessageBox::Ok);
          return;
     }
     if(!state->loadState())
     {
          QMessageBox::critical(NULL, "HQEd", "The state cannot be loaded successfully.", QMessageBox::Ok);
          return;
     }
     
     // if state is loaded then we must to run simulation
     
     // hqed simulation
     if(!__enginetype.compare("hqed", Qt::CaseInsensitive))
     {
          xtt::run(state);
          return;
     }     
     // devplugin simulation
     if(!__enginetype.compare("devplugin", Qt::CaseInsensitive))
     {
          // search for plugin
          int pluginindex = devPC->indexOfName(__enginename);
          if(pluginindex == -1)
          {    
               QMessageBox::critical(NULL, "HQEd", "The plugin \'" + __enginename + "\' cannot be found.", QMessageBox::Ok);
               return;
          }
          devPlugin* plugin = devPC->plugins()->at(pluginindex);
          
          // Setup simulation parameters
          int simtype = DEV_PLUGIN_SIM_TYPE_DDI;
          xttSimulationParams simparams(state);
          if(!__simulationtype.compare("ddi", Qt::CaseInsensitive))
               simtype = DEV_PLUGIN_SIM_TYPE_DDI;
          if(!__simulationtype.compare("gdi", Qt::CaseInsensitive))
               simtype = DEV_PLUGIN_SIM_TYPE_GDI;
          if(!__simulationtype.compare("tdi", Qt::CaseInsensitive))
               simtype = DEV_PLUGIN_SIM_TYPE_TDI;
          
          simparams.setSimulationType(simtype);
          simparams.defineDefaultSetOfTables();
          plugin->startSimulation(simparams);
          
          return;
     }
}
//---------------------------------------------------------------------------

// #brief Function removes given state
//
// #param __statebasename - base name of the state groups from which the state should be removed
// #param __stateIndex - index of the state in the given state group
// #return No return value.
void hMenuAction::deleteState(QString __statebasename, int __stateIndex)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     
     if(QMessageBox::warning(NULL, "HQEd", "Do you really want to remove selected state?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
     
     XTT_Statesgroup* stsgrp = (*States)[__statebasename];
     stsgrp->removeState(__stateIndex);
}
//---------------------------------------------------------------------------

// #brief Function removes attributes from the given state group
//
// #param __statebasename - base name of the state
// #param __attr - pointer to the attribute that schould be removed
// #return No return value.
void hMenuAction::deleteInputStateCol(QString __statebasename, XTT_Attribute* __attr)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     if(__attr == NULL)
          return;
          
     if(QMessageBox::warning(NULL, "HQEd", "Do you really want to remove initial values of \'" + __attr->Name() + "\' attribute?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
     
     XTT_Statesgroup* stsgrp = (*States)[__statebasename];
     stsgrp->removeInputAttribute(__attr);
}
//---------------------------------------------------------------------------

// #brief Function removes the current table
//
// #param __tableId - table id
// #return No return value.
void hMenuAction::deleteTable(QString __tableId)
{
     int fParentIndex = TableList->IndexOfID(__tableId);
     if(fParentIndex == -1)
          return;

     if(QMessageBox::warning(NULL, "HQEd", "Do you really want to remove selected table?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     if(MainWin->xtt_scene->SelectedTableID() == __tableId)
          MainWin->xtt_scene->setSelectedTableID("");
     TableList->Delete(fParentIndex);
}
//---------------------------------------------------------------------------

// #brief Function that removes the group of states
//
// #param __basename - name of the group
// #return No return value.
void hMenuAction::deleteStatesgroup(QString __basename)
{
     if(!States->contains(__basename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __basename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     
     if(QMessageBox::warning(NULL, "HQEd", "Do you really want to remove the group of states \'" + __basename + "\'?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     delete States->take(__basename);
}
//---------------------------------------------------------------------------

// #brief Function shows edit attribute window.
//
// #param __tableId - table id
// #param __colIndex - index of the column in the given table
// #return No return value.
void hMenuAction::editAttribute(QString __tableId, int __colIndex)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
	if(AttributeGroups->IsEmpty())
     {
          const QString str2 = "There is no attributes defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     delete AttributeEditor;
     // AttributeEditor = new AttrEditor(fParent->Row(fRow)->Cell(__colIndex)->AttrType()->Id(), false);
     AttributeEditor = new AttrEditor(fParent->ContextAtribute(__colIndex)->Id(), false);
     AttributeEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function shows edit attribute window.
//
// #param __xttattribute - pointer to the attribute
// #return No return value.
void hMenuAction::editAttribute(XTT_Attribute* __xttattribute)
{       
	if(AttributeGroups->IsEmpty())
     {
          const QString str2 = "There is no attributes defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     delete AttributeEditor;
     // AttributeEditor = new AttrEditor(fParent->Row(fRow)->Cell(__colIndex)->AttrType()->Id(), false);
     AttributeEditor = new AttrEditor(__xttattribute->Id(), false);
     AttributeEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function shows edit state schema dialog window.
//
// #param __statebasename - the name of the state group
// #return No return value.
void hMenuAction::editStateSchema(QString __statebasename)
{
     if(!States->contains(__statebasename))
     {
          QMessageBox::critical(NULL, "HQEd", "The \'" + __statebasename + "\' state group connot be found.", QMessageBox::Ok);
          return;
     }
     XTT_Statesgroup* destinationgroup = (*States)[__statebasename];
     
     delete StateSchemaEditor;
     StateSchemaEditor = new StateSchemaEditor_ui(destinationgroup, __statebasename);
     StateSchemaEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function that adds a range to the set
//
// #param __setPtr - pointer to the set
// #param __from - from value
// #param __to - to value
// #return No return value.
void hMenuAction::addSetRange(hSet* __setPtr, QString __from, QString __to)
{
     if(__setPtr == NULL)
          return;
          
     __setPtr->add(new hSetItem(__setPtr, SET_ITEM_TYPE_RANGE, new hValue(__from), new hValue(__to), false));
     
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function that adds a range to the MultipleValue and sets it to value set
//
// #param __setPtr - pointer to the MultipleValue
// #param __from - from value
// #param __to - to value
// #return No return value.
void hMenuAction::addMultipleValueRange(hMultipleValue* __setPtr, QString __from, QString __to)
{
     if(__setPtr == NULL)
          return;
          
     if((__setPtr->type() != VALUE) || (__setPtr->isAVRused()) || (__setPtr->isExpressionUsed()))
     {
          __setPtr->setType(VALUE);
          __setPtr->valueField()->flush();
     }
     addSetRange(__setPtr->valueField(), __from, __to);
}
//---------------------------------------------------------------------------

// #brief Function executes setCreator dialog
//
// #param __type - pointer to the set type
// #param __set - pointer to the set that we want to edit
// #param __singlerequired - indicates if the set can conatin only one value
// #return No return value.
void hMenuAction::setCreator(hType* __type, hSet* __set, bool __singlerequired)
{
     delete SetCreator;
     SetCreator = new SetCreator_ui;
     connect(SetCreator, SIGNAL(okclicked()), this, SLOT(setCreatorOkClicked()));

     if(SetCreator->setMode(__type, __set, __singlerequired) == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Incorrect set creator mode.", QMessageBox::Ok);
          return;
     }
     SetCreator->show();
}
//---------------------------------------------------------------------------

// #brief Function that is trgered when user clicks ok in the setCreator
//
// #return No return value.
void hMenuAction::setCreatorOkClicked(void)
{
     emit setCreatorConfirmed();

     MainWin->xtt_scene->update();
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function that removes given item from the given set
//
// #param __setPtr - pointer to the set
// #param __itemIndex - index of the item
// #return No return value.
void hMenuAction::removeSetItem(hSet* __setPtr, int __itemIndex)
{
     if(__setPtr == NULL)
          return;
     if((__itemIndex < 0) || (__itemIndex >= __setPtr->size()))
          return;
          
     __setPtr->deleteItem(__itemIndex);
     
     MainWin->xtt_scene->update();
     xtt::fullVerification();
     xtt::logicalFullVerification();
}
//---------------------------------------------------------------------------

// #brief Function shows edit attribute window.
//
// #param __tableId - table id
// #param __colIndex - index of the column in the given table
// #param __criterion - idicates the criterion of table sorting
// #return No return value.
void hMenuAction::sortTable(QString __tableId, int __colIndex, int __criterion)
{
     XTT_Table* fParent = TableList->_IndexOfID(__tableId);
     if(fParent == NULL)
          return;
          
     if((__colIndex < 0) || (__colIndex >= fParent->ColCount()))
          return;
          
     fParent->column(__colIndex)->setSortCriterion(__criterion);
     
     QString msg;
     if(!fParent->sortRules(__colIndex, XTT_COLUMN_SORT_ASCENDING, msg))
          QMessageBox::critical(NULL, "HQEd", "Sorting error:\n" + msg, QMessageBox::Ok);
}
//---------------------------------------------------------------------------

