/**
 * \file	About_ui.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	28.04.2007
 * \version	1.0
 * \brief	This file contains class definition that can arrange dialog 'About...'.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QKeyEvent>

#include "../namespaces/ns_hqed.h"

#include "About_ui.h"
// -----------------------------------------------------------------------------

About_ui* AboutWin;
// -----------------------------------------------------------------------------

// #brief Constructor About_ui class.
//
// #param parent Pointer to parent object.
About_ui::About_ui(QWidget*)
{
    setupUi(this);
    setWindowIcon(QIcon(":/all_images/images/mainico.png"));
    
    setWindowModality(Qt::ApplicationModal);
    setWindowFlags(Qt::SplashScreen);
    hqed::centerWindow(this);

    // Zapisanie wersji programu
    fAppVersion = hqed::hqedName();
    label->setText(fAppVersion);
    label_7->setText(fAppVersion);
    
    QString licenceInfo = "<font size=\"4\"><b>Implementation by Krzysztof Kaczor</b></font><br><font size=\"4\">kinio4444@gmail.com</font><br><font size=\"4\">kk@agh.edu.pl</font>";
    
    licenceInfo += "<br><br><b>Copyright (C) 2006-9 by the HeKatE Project</b>";
    licenceInfo += "<br>HQEd has been develped within the <b>HeKatE Project</b>, see <a href=\"http://hekate.ia.agh.edu.pl\">http://hekate.ia.agh.edu.pl</a>";

    licenceInfo += "<br><br>This file is part of <b>HQEd</b>.";

    licenceInfo += "<br><br><u>HQEd is free software:</u> you can redistribute it and/or modify ";
    licenceInfo += "it under the terms of the GNU General Public License as published by ";
    licenceInfo += "the Free Software Foundation, either version 3 of the License, or ";
    licenceInfo += "(at your option) any later version.";

    licenceInfo += "<br><br>HQEd is distributed in the hope that it will be useful, ";
    licenceInfo += "but WITHOUT ANY WARRANTY; without even the implied warranty of ";
    licenceInfo += "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the ";
    licenceInfo += "GNU General Public License for more details.";

    licenceInfo += "<br><br>You should have received a copy of the GNU General Public License";
    licenceInfo += "along with HQEd.  If not, see <a href=\"http://www.gnu.org/licenses/\">http://www.gnu.org/licenses/</a>.";

    textEdit->setWordWrapMode(QTextOption::WordWrap);
    textEdit->setHtml(licenceInfo);
}
// -----------------------------------------------------------------------------

// #brief Destructor About_ui class.
About_ui::~About_ui(void)
{
    delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void About_ui::keyPressEvent(QKeyEvent *event)
{
    switch(event->key())
    {
    case Qt::Key_Escape:
        close();
        break;

    case Qt::Key_Return:
        close();
        break;

    default:
        QWidget::keyPressEvent(event);
    }
}
// -----------------------------------------------------------------------------

// #brief Get current application version.
//
// #return QString Information about current application version.
QString About_ui::appVersion(void)
{
    return fAppVersion;
}
// -----------------------------------------------------------------------------
