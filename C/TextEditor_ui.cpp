/*
 *	  $Id: TextEditor_ui.cpp,v 1.9.4.2.2.4 2011-06-16 14:59:12 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPainter>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>
#include <QTabWidget>

#include "../namespaces/ns_xtt.h"
#include "../namespaces/ns_hqed.h"
#include "../M/hModelList.h"
#include "../M/hModel.h"
#include "../V/vasXML.h"
#include "../V/vasProlog.h"

#include "te/sh/HSyntaxHighlighter.h"
#include "Settings_ui.h"
#include "TextEditor_ui.h"
// -----------------------------------------------------------------------------

TextEditor_ui* TextEditor;
// -----------------------------------------------------------------------------

// #brief Constructor TextEditor_ui class.
//
// #param *parent - Pointer to parent.
TextEditor_ui::TextEditor_ui(QMainWindow* /*parent*/)
{
     initWindow();
}
// -----------------------------------------------------------------------------

// #brief Destructor TextEditor_ui class.
TextEditor_ui::~TextEditor_ui(void)
{
     if(printer)
          delete printer;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #return no values returns
void TextEditor_ui::initWindow(void)
{
     setupUi(this);
     
     setAcceptDrops(true);
     setDockNestingEnabled(true);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);
     
     createStatusBar();
     createToolBars();
     
     tabs = new QTabWidget;
     adoc = -1;
     setCentralWidget(tabs);
     
     connect(actionNew, SIGNAL(triggered()), this, SLOT(newClick()));
     connect(actionOpen, SIGNAL(triggered()), this, SLOT(openFileDialog()));
     connect(actionReload, SIGNAL(triggered()), this, SLOT(reload()));
     
     connect(actionUndo, SIGNAL(triggered()), this, SLOT(undo()));
     connect(actionRedo, SIGNAL(triggered()), this, SLOT(redo()));
     connect(actionCopy, SIGNAL(triggered()), this, SLOT(copy()));
     connect(actionCut, SIGNAL(triggered()), this, SLOT(cut()));
     connect(actionPaste, SIGNAL(triggered()), this, SLOT(paste()));
     connect(actionDelete, SIGNAL(triggered()), this, SLOT(deleteSelection()));
     connect(actionSelect_all, SIGNAL(triggered()), this, SLOT(selectAll()));
     connect(actionClear_all, SIGNAL(triggered()), this, SLOT(clear()));
     connect(actionPrint, SIGNAL(triggered()), this, SLOT(printDoc()));
     connect(actionPrint_setup, SIGNAL(triggered()), this, SLOT(printSetup()));
     connect(actionSave, SIGNAL(triggered()), this, SLOT(save()));
     connect(actionSave_as, SIGNAL(triggered()), this, SLOT(saveAs()));
     connect(actionSave_all, SIGNAL(triggered()), this, SLOT(saveAll()));
     connect(actionClose_this_document, SIGNAL(triggered()), this, SLOT(closeDoc()));
     connect(actionClose_editor, SIGNAL(triggered()), this, SLOT(closeForm()));
     connect(actionClose_all_documents_2, SIGNAL(triggered()), this, SLOT(closeAllDocs()));
     connect(actionClose_all_documents_except_current_2, SIGNAL(triggered()), this, SLOT(closeAllDocsExceptCurrent()));
     
     connect(actionWrap_lines, SIGNAL(triggered()), this, SLOT(onWrapLineClick()));
     connect(actionOverwrite_mode, SIGNAL(triggered()), this, SLOT(useOverwriteMode()));
     connect(actionInsert_mode, SIGNAL(triggered()), this, SLOT(useInsertMode()));
     
     connect(actionText, SIGNAL(triggered()), this, SLOT(useTextMode()));
     connect(actionPROLOG, SIGNAL(triggered()), this, SLOT(usePrologMode()));
     connect(actionHML, SIGNAL(triggered()), this, SLOT(useHMLmode()));
     connect(actionHMR, SIGNAL(triggered()), this, SLOT(useHMRmode()));
     connect(actionEcma, SIGNAL(triggered()), this, SLOT(useEcmaMode()));
     connect(actionMode_configuration, SIGNAL(triggered()), this, SLOT(modeConfig()));
     
     connect(actionDisplay_HML_code, SIGNAL(triggered()), this, SLOT(viewXmlCode()));
     connect(actionDisplay_PROLOG_code, SIGNAL(triggered()), this, SLOT(viewPrologCode()));
     
     connect(tabs, SIGNAL(currentChanged(int)), this, SLOT(currentDocIndexChanged(int)));
     
     // Adding shortcuts
     actionNew->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_N));
     actionOpen->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_O));
     actionReload->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_R));
     actionUndo->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Z));
     actionRedo->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_Y));
     actionCopy->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_C));
     actionCut->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_X));
     actionPaste->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_V));
     actionSelect_all->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_A));
     actionPrint->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));
     actionPrint_setup->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_P));
     actionSave->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_S));
     actionSave_all->setShortcut(QKeySequence(Qt::CTRL + Qt::SHIFT + Qt::Key_S));
     actionClose_this_document->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_W));
     actionMode_configuration->setShortcut(QKeySequence(Qt::Key_F12));
     actionText->setShortcut(QKeySequence(Qt::Key_F6));
     actionPROLOG->setShortcut(QKeySequence(Qt::Key_F7));
     actionHML->setShortcut(QKeySequence(Qt::Key_F8));
     actionHMR->setShortcut(QKeySequence(Qt::Key_F9));
     actionEcma->setShortcut(QKeySequence(Qt::Key_F10));
     actionWrap_lines->setShortcut(QKeySequence(Qt::Key_F2));
     
     // initializing printer
     printer = 0;
     
     // creating new empty doc
     newClick();
     adoc = 0;
     useTextMode();
     useInsertMode();
     
     setMode(TE_MODE_NORMAL_EDIT);
}
// -----------------------------------------------------------------------------

// #brief Create toolbars.
//
// #return No return value.
void TextEditor_ui::createToolBars(void)
{
     fileToolBar = addToolBar(tr("File"));
     fileToolBar->addAction(actionClose_editor);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionNew);
     fileToolBar->addAction(actionOpen);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionReload);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionSave);
     fileToolBar->addAction(actionSave_all);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionClose_this_document);
     fileToolBar->addAction(actionClose_all_documents_2);
     
     undoredoToolBar = addToolBar(tr("Undo/Redo"));
     undoredoToolBar->addAction(actionUndo);
     undoredoToolBar->addAction(actionRedo);
     
     ccpToolBar = addToolBar(tr("Copy/Cut/Paste"));
     ccpToolBar->addAction(actionCut);
     ccpToolBar->addAction(actionCopy);
     ccpToolBar->addAction(actionPaste);
     
     printToolBar = addToolBar(tr("Print"));
     printToolBar->addAction(actionPrint);
     printToolBar->addAction(actionPrint_setup);
     
     cfgToolBar = addToolBar(tr("Configuration"));
     cfgToolBar->addAction(actionMode_configuration);
}
// -----------------------------------------------------------------------------

// #brief Create statusbar.
//
// #return No return value.
void TextEditor_ui::createStatusBar(void)
{
     statusBar()->showMessage(tr("Ready"));
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user switch beetween tabs
//
// #param __index - index of the current tab
// #return No return value.
void TextEditor_ui::currentDocIndexChanged(int __index)
{
     actionReload->setEnabled(__index != -1);
     actionSave->setEnabled(__index != -1);
     actionSave_as->setEnabled(__index != -1);
     actionPrint->setEnabled(__index != -1);
     actionClose_this_document->setEnabled(__index != -1);
     actionUndo->setEnabled(__index != -1);
     actionRedo->setEnabled(__index != -1);
     actionCopy->setEnabled(__index != -1);
     actionCut->setEnabled(__index != -1);
     actionDelete->setEnabled(__index != -1);
     actionSelect_all->setEnabled(__index != -1);
     actionClear_all->setEnabled(__index != -1);

     adoc = __index;
     
     if(__index == -1)
          return;

     actionSave->disconnect();
     actionUndo->disconnect();
     actionRedo->disconnect();
     connect(actionSave, SIGNAL(triggered()), this, SLOT(save()));
     connect(docs.at(adoc)->doc()->document(), SIGNAL(modificationChanged(bool)), actionSave, SLOT(setEnabled(bool)));
     connect(actionUndo, SIGNAL(triggered()), this, SLOT(undo()));
     connect(docs.at(adoc)->doc()->document(), SIGNAL(undoAvailable(bool)), actionUndo, SLOT(setEnabled(bool)));
     connect(actionRedo, SIGNAL(triggered()), this, SLOT(redo()));
     connect(docs.at(adoc)->doc()->document(), SIGNAL(redoAvailable(bool)), actionRedo, SLOT(setEnabled(bool)));
     
     actionSave->setEnabled(docs.at(adoc)->doc()->document()->isModified());
     actionUndo->setEnabled(docs.at(adoc)->doc()->document()->isUndoAvailable());
     actionRedo->setEnabled(docs.at(adoc)->doc()->document()->isRedoAvailable());

     onSwitchDocs();
}
// -----------------------------------------------------------------------------

// #brief Function that is treggered when user switches between documents
//
// #return No return value.
void TextEditor_ui::onSwitchDocs(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_TEXT)
          useTextMode();
     if(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_PROLOG)
          usePrologMode();
     if(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_HML)
          useHMLmode();
     if(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_HMR)
          useHMRmode();
     if(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_ECMA)
          useEcmaMode();
          
     if(docs.at(adoc)->doc()->overwriteMode())
          useOverwriteMode();
     else if(!docs.at(adoc)->doc()->overwriteMode())
          useInsertMode();
          
     if((docs.at(adoc)->doc()->lineWrapMode() == QTextEdit::NoWrap) && (actionWrap_lines->isChecked()))
          actionWrap_lines->setChecked(false);
     else if((docs.at(adoc)->doc()->lineWrapMode() == QTextEdit::WidgetWidth) && (!actionWrap_lines->isChecked()))
          actionWrap_lines->setChecked(true);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when state of the modification has been changed
//
// #param __modified - new state of modification
// #return No return value.
void TextEditor_ui::modificationStateChanged(bool __modified)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(__modified)
          tabs->setTabText(adoc, createTabTitle(docs.at(adoc)->fileName()) + "*");
     if(!__modified)
          tabs->setTabText(adoc, createTabTitle(docs.at(adoc)->fileName()));
}
// -----------------------------------------------------------------------------

// #brief Handle event of dragging file on window area and check it data tape.
//
// #param event Pointer to parameters of dragging data on form.
// #return No return value.
void TextEditor_ui::dragEnterEvent(QDragEnterEvent* event)
{
     if(event->mimeData()->hasFormat("text/uri-list"))
          event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Handle event of dropping file on window area and check it data tape.
//
// #param event - Pointer to parameters of dropping data on form.
// #return No return value.
void TextEditor_ui::dropEvent(QDropEvent* event)
{
     QList<QUrl> urlList = event->mimeData()->urls();
     for(int i=0;i<urlList.size();++i)
     {
          QString url = urlList.at(i).toLocalFile();
          if(QFile::exists(url))
          {
               newClick();
               openFile(docs.size()-1, url);
          }
     }

     event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function that returns suffix of the file
//
// #param __filename - given file name
// #return suffix of the file
QString TextEditor_ui::fileSuffix(QString __filename)
{
     QFileInfo fi(__filename);
     QString base = fi.baseName();
     return fi.suffix().toLower();
}
// -----------------------------------------------------------------------------

// #brief function that returns the editor mode
//
// #return the editor mode
int TextEditor_ui::mode(void)
{
     return _mode;
}
// -----------------------------------------------------------------------------

// #brief Returns the list of documents
//
// #return the list of documents
QList<TextEditorDoc*>& TextEditor_ui::documents(void)
{
     return docs;
}
// -----------------------------------------------------------------------------

// #brief Function that sets the dialog working mode
//
// #param __mode - mode identifier
// #return no values returns
void TextEditor_ui::setMode(int __mode)
{
     if((__mode == TE_MODE_NORMAL_EDIT) && (__mode == TE_MODE_SCRIPT_EDIT))
          return;
          
     _mode = __mode;
     actionNew->setVisible(_mode == TE_MODE_NORMAL_EDIT);
     actionOpen->setVisible(_mode == TE_MODE_NORMAL_EDIT);
     actionClose_this_document->setVisible(_mode == TE_MODE_NORMAL_EDIT);
     actionClose_all_documents_2->setVisible(_mode == TE_MODE_NORMAL_EDIT);
     actionDisplay_HML_code->setVisible(_mode == TE_MODE_NORMAL_EDIT);
     actionDisplay_PROLOG_code->setVisible(_mode == TE_MODE_NORMAL_EDIT);
}
// -----------------------------------------------------------------------------

// #brief Function that creates tab title acording to the given file name
//
// #param __filename - given file name
// #return tab title
QString TextEditor_ui::createTabTitle(QString __filename)
{
     QFileInfo fi(__filename);
     QString base = fi.baseName();
     QString csuf = fileSuffix(__filename);
     QString dispfn = base;
     if(!csuf.isEmpty())
          dispfn = base + "." + csuf;
          
     return dispfn;
}
// -----------------------------------------------------------------------------

// #brief Open file dialog.
//
// #return No return value.
void TextEditor_ui::openFileDialog(void)
{
     QString fileName = QFileDialog::getOpenFileName(this,
                                 tr("Open file"),
                                 Settings->projectPath(),
                                 tr("All files (*)"));
     
     newClick();
     openFile(docs.count()-1, fileName);
}
// -----------------------------------------------------------------------------

// #brief Opens a file.
//
// #param __tabIndex - index of tab where file will be loaded
// #param _fileName - File name to open.
// #return No return value.
void TextEditor_ui::openFile(int __tabIndex, QString _fileName)
{
     if((__tabIndex < 0) || (__tabIndex >= docs.size()))
          return;
          
     // Gdy nie wybrano zandego pliku
     if(_fileName.isEmpty())
          return;
     if(!QFile::exists(_fileName))
     {
          QMessageBox::critical(this, "HQEd", "File " + _fileName + " does not exists.");
          return;
     }

     // Czytanie pliku
     QFile file(_fileName);
     if(!file.open(QFile::ReadOnly | QFile::Text))
     {
          QMessageBox::critical(this, tr("HQEd"), tr("Cannot read file %1:\n%2.").arg(_fileName).arg(file.errorString()));
          return;
     }

     QString fc = QString(file.readAll());
     docs.at(__tabIndex)->doc()->setPlainText(fc);
     docs.at(__tabIndex)->setFileName(_fileName);
     docs.at(__tabIndex)->setIsFile(true);
     tabs->setTabText(__tabIndex, createTabTitle(_fileName));

     QString csuf = fileSuffix(_fileName);
     if(csuf == "hml")
          useHMLmode();
     if(csuf == "pl")
          usePrologMode();
}
// -----------------------------------------------------------------------------

// #brief Saves the content of the given doc to file
//
// #param __tabIndex - index of the doc
// #param _fileName - name of the file where content will be saved
// #return No return value.
void TextEditor_ui::saveDoc(int __tabIndex, QString _fileName)
{
     if((__tabIndex < 0) || (__tabIndex >= docs.size()))
          return;
          
     // Zapis danych do pliku
     QFile file(_fileName);
     if(!file.open(QFile::WriteOnly | QFile::Text))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(_fileName).arg(file.errorString()));
         return;
     }
     
     file.write(docs.at(__tabIndex)->doc()->toPlainText().toAscii());
     docs.at(__tabIndex)->setIsFile(true);
     docs.at(__tabIndex)->setFileName(_fileName);
     docs.at(__tabIndex)->doc()->document()->setModified(false);
}
// -----------------------------------------------------------------------------

// #brief Closes given document
//
// #param __tabIndex - index of the doc to be closed
// #return No return value.
void TextEditor_ui::closeDoc(int __tabIndex)
{
     if((__tabIndex < 0) || (__tabIndex >= docs.size()))
          return;
          
     delete docs.takeAt(__tabIndex);
     tabs->removeTab(__tabIndex);
}
// -----------------------------------------------------------------------------

// #brief reloads document from file
//
// #return No return value.
void TextEditor_ui::reload(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to reload the content of the file from hard disk?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     if(docs.at(adoc)->isFile())
          openFile(adoc, docs.at(adoc)->fileName());
}
// -----------------------------------------------------------------------------

// #brief Save file as.
//
// #return No return value.
void TextEditor_ui::saveAs(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     QString path = QDir::currentPath();
     if(mode() == TE_MODE_SCRIPT_EDIT)
          path = Settings->ecmascriptPath();
     
     QStringList filters;
     if(mode() == TE_MODE_SCRIPT_EDIT)
          filters << "ECMA script presentation (*.esp)";
     filters << "All files (*)";

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
                                 tr("Save project as"),
                                 path,
                                 filters.at(0));

     // Gdy nie wybrano zandego pliku
     if(fileName.isEmpty())
          return;
          
     saveDoc(adoc, fileName);
}
// -----------------------------------------------------------------------------

// #brief Save changes.
//
// #return No return value.
void TextEditor_ui::save(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(!docs.at(adoc)->isFile())
     {
          saveAs();
          return;
     }
     
     saveDoc(adoc, docs.at(adoc)->fileName());
}
// -----------------------------------------------------------------------------

// #brief Save all changes in all docs
//
// #return No return value.
void TextEditor_ui::saveAll(void)
{
     int adocbkp = adoc;
     for(int i=0;i<docs.size();++i)
     {
          adoc = i;
          save();
     }
     adoc = adocbkp;
}
// -----------------------------------------------------------------------------

// #brief Create new dosument.
//
// #return No return value.
void TextEditor_ui::newClick(void)
{
     docs.append(new TextEditorDoc);
     tabs->addTab(docs.last()->doc(), QIcon(":/all_images/images/texteditor_small.png"), docs.last()->fileName());
     tabs->setCurrentIndex(tabs->count()-1);
     connect(docs.last()->doc()->document(), SIGNAL(modificationChanged(bool)), this, SLOT(modificationStateChanged(bool)));
}
// -----------------------------------------------------------------------------

// #brief Removes all the data from current document
//
// #return No return value.
void TextEditor_ui::clear(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(QMessageBox::question(this, "HQEd", "Do you really want to clear all the text?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
          
     docs.at(adoc)->doc()->clear();
}
// -----------------------------------------------------------------------------

// #brief Closes current docuemnt
//
// #return No return value.
void TextEditor_ui::closeDoc(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(docs.at(adoc)->changed())
     {
          QMessageBox::StandardButton btn = QMessageBox::warning(NULL, "HQEd", "You are going to close modified document \'" + createTabTitle(docs.at(adoc)->fileName()) + "\'.\nDo you want to save changes?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel);
          if(btn == QMessageBox::Cancel)
               return;
          if(btn == QMessageBox::Yes)
               save();
     }
          
     closeDoc(adoc);
     
     if(docs.size() == 0)
          newClick();
}
// -----------------------------------------------------------------------------

// #brief Closes all documents
//
// #param __confirm - determines confirmation of the action
// #return No return value.
void TextEditor_ui::closeAllDocs(bool /*__confirm*/)
{
     if(QMessageBox::warning(NULL, "HQEd", "You are going to close all the documents?\nDo you really want to continue?", QMessageBox::Yes | QMessageBox::Cancel) == QMessageBox::Cancel)
          return;
     
     for(int i=docs.size()-1;i>=0;--i)
     {
          adoc = i;
          closeDoc();
     }
}
// -----------------------------------------------------------------------------

// #brief Closes all documents except current
//
// #return No return value.
void TextEditor_ui::closeAllDocsExceptCurrent(void)
{
     if(QMessageBox::warning(NULL, "HQEd", "You are going to close all the documents except \'" + createTabTitle(docs.at(adoc)->fileName()) + "\'?\nDo you really want to continue?", QMessageBox::Yes | QMessageBox::Cancel) == QMessageBox::Cancel)
          return;
     
     int adocbkp = adoc;
     for(int i=docs.size()-1;i>=0;--i)
     {
          if(i == adocbkp)
               continue;
          
          adoc = i;
          closeDoc();
     }
}
// -----------------------------------------------------------------------------

// #brief Updates the state of checked actions
//
// #return No return value.
void TextEditor_ui::updateModeChecked(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     actionText->setChecked(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_TEXT);
     actionPROLOG->setChecked(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_PROLOG);
     actionHML->setChecked(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_HML);
     actionHMR->setChecked(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_HMR);
     actionEcma->setChecked(docs.at(adoc)->currentMode() == TEXT_EDITOR_MODE_ECMA);
}
// -----------------------------------------------------------------------------

// #brief Switch application to Text mode.
//
// #return No return value.
void TextEditor_ui::useTextMode(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->setCurrentMode(TEXT_EDITOR_MODE_TEXT);
     updateModeChecked();
}
// -----------------------------------------------------------------------------

// #brief Switch application to PROLOG mode.
//
// #return No return value.
void TextEditor_ui::usePrologMode(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->setCurrentMode(TEXT_EDITOR_MODE_PROLOG);
     updateModeChecked();
}
// -----------------------------------------------------------------------------

// #brief Switch application to HML mode.
//
// #return No return value.
void TextEditor_ui::useHMLmode(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->setCurrentMode(TEXT_EDITOR_MODE_HML);
     updateModeChecked();
}
// -----------------------------------------------------------------------------

// #brief Switch application to HMR mode.
//
// #return No return value.
void TextEditor_ui::useHMRmode(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->setCurrentMode(TEXT_EDITOR_MODE_HMR);
     updateModeChecked();
}
// -----------------------------------------------------------------------------

// #brief Switch application to ECMA mode.
//
// #return No return value.
void TextEditor_ui::useEcmaMode(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->setCurrentMode(TEXT_EDITOR_MODE_ECMA);
     updateModeChecked();
}
// -----------------------------------------------------------------------------

// #brief Close editor.
//
// #return No return value.
void TextEditor_ui::closeForm(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when window is closed.
//
// #param event - Pointer to close event object.
// #return No return value.
void TextEditor_ui::closeEvent(QCloseEvent* /*event*/)
{
     emit onclose();
}
// -----------------------------------------------------------------------------

// #brief Print current document.
//
// #return No return value.
void TextEditor_ui::printDoc(void)
{
     if(!printer)
          printer = new QPrinter;
          
     if((adoc < 0) || (adoc >= docs.size()) || (printer == NULL))
          return;
          
     docs.at(adoc)->doc()->document()->print(printer);
}
// -----------------------------------------------------------------------------

// #brief Show print setup window.
//
// #return No return value.
void TextEditor_ui::printSetup(void)
{
     if(!printer)
          printer = new QPrinter;
          
     if(printer == NULL)
          return;
     
     QPrintDialog *dlg = new QPrintDialog(printer, this);
     if(dlg->exec() != QDialog::Accepted)
          return;

     printDoc();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press undo button
//
// #return No return value.
void TextEditor_ui::undo(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->undo();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press redo button
//
// #return No return value.
void TextEditor_ui::redo(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->redo();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press copy button
//
// #return No return value.
void TextEditor_ui::copy(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->copy();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press cut button
//
// #return No return value.
void TextEditor_ui::cut(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;

     docs.at(adoc)->doc()->cut();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press paste button
//
// #return No return value.
void TextEditor_ui::paste(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->paste();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press selectAll button
//
// #return No return value.
void TextEditor_ui::selectAll(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->selectAll();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press delete button
//
// #return No return value.
void TextEditor_ui::deleteSelection(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     docs.at(adoc)->doc()->textCursor().removeSelectedText();
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press wrap lines button
//
// #return No return value.
void TextEditor_ui::onWrapLineClick(void)
{
     if((adoc < 0) || (adoc >= docs.size()))
          return;
          
     if(!actionWrap_lines->isChecked())       // turning off wrping lines
     {
          docs.at(adoc)->doc()->setWordWrapMode(QTextOption::NoWrap);
          docs.at(adoc)->doc()->setLineWrapMode(QTextEdit::NoWrap);
     }
     if(actionWrap_lines->isChecked())       // turning on wrping lines
     {
          docs.at(adoc)->doc()->setLineWrapMode(QTextEdit::WidgetWidth);
          docs.at(adoc)->doc()->setWordWrapMode(QTextOption::WordWrap);
     }
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press wrap lines button
//
// #return No return value.
void TextEditor_ui::useInsertMode(void)
{
     actionOverwrite_mode->setVisible(true);
     actionOverwrite_mode->setShortcut(QKeySequence(Qt::Key_Insert));
     actionInsert_mode->setVisible(false);
     actionInsert_mode->setShortcut(QKeySequence(""));
     
     if((adoc < 0) || (adoc >= docs.size()))
          return;
     docs.at(adoc)->doc()->setOverwriteMode(false);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press wrap lines button
//
// #return No return value.
void TextEditor_ui::useOverwriteMode(void)
{
     actionInsert_mode->setVisible(true);
     actionInsert_mode->setShortcut(QKeySequence(Qt::Key_Insert));
     actionOverwrite_mode->setVisible(false);
     actionOverwrite_mode->setShortcut(QKeySequence(""));
     
     if((adoc < 0) || (adoc >= docs.size()))
          return;
     docs.at(adoc)->doc()->setOverwriteMode(true);
}
// -----------------------------------------------------------------------------

// #brief Function that is triggered when user press mode configuration button
//
// #return No return value.
void TextEditor_ui::modeConfig(void)
{
     docs.at(adoc)->configMode();
}
// -----------------------------------------------------------------------------

// #brief Function that converts model to prolog and displays it in the text editor
//
// #return No return value.
void TextEditor_ui::viewPrologCode(void)
{
     newClick();

     QString content, msg;

     QBuffer buf;
     buf.open(QBuffer::ReadWrite);
     int pgr = vas_out_SWI_Prolog(&buf, msg);

     if(pgr == VAS_SWI_PROLOG_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "Text Editor", "Hmr code generation error: unknown device.", QMessageBox::Ok);
     if(pgr == VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "Text Editor", "Hmr code generation error: the device is not writable.", QMessageBox::Ok);
     if(pgr == VAS_SWI_PROLOG_ERROR)
          QMessageBox::critical(this, "Text Editor", msg, QMessageBox::Ok);
     if(pgr != VAS_SWI_PROLOG_SUCCES)
          return;

     content = QString(buf.data());
     QString filename = hqed::modelList()->activeModel()->xttFileName();
     docs.last()->setFileName(filename);
     docs.last()->doc()->setPlainText(content);
     docs.last()->doc()->document()->setModified(true);
     useHMRmode();
}
// -----------------------------------------------------------------------------

// #brief Function that converts model to xml and displays it in the text editor
//
// #return No return value.
void TextEditor_ui::viewXmlCode(void)
{
     QBuffer* buffer = new QBuffer;
     buffer->open(QIODevice::ReadWrite);

     int parts = VAS_XML_MODEL_PARTS_XTT_TYPES;
     parts |=  VAS_XML_MODEL_PARTS_ARD_PROPS;
     parts |=  VAS_XML_MODEL_PARTS_ARD_TPH;
     parts |=  VAS_XML_MODEL_PARTS_ARD_DPNDS;
     parts |=  VAS_XML_MODEL_PARTS_ARD_ATTRS;
     parts |=  VAS_XML_MODEL_PARTS_XTT_ATTRS;
     parts |= VAS_XML_MODEL_PARTS_XTT_TABLS;
     parts |= VAS_XML_MODEL_PARTS_XTT_STATS;
     int result = vas_out_HML_2_0(buffer, Settings->xmlAutoformating(), parts);
     
     // on error
     if(result == VAS_XML_UNKNOWN_DEVICE)
     {
          QMessageBox::critical(NULL, "HQEd", "XML generation error: unknown output device.", QMessageBox::Ok);
          return;
     }
     if(result == VAS_XML_DEVICE_NOT_WRITABLE)
     {
          QMessageBox::critical(NULL, "HQEd", "XML generation error: output device is not writable.", QMessageBox::Ok);
          return;
     }
     
     newClick();
     QString content = QString(buffer->data());
     delete buffer;
     
     QString filename = hqed::modelList()->activeModel()->xttFileName();
     docs.last()->setFileName(filename);
     docs.last()->doc()->setPlainText(content);
     docs.last()->doc()->document()->setModified(true);
     useHMLmode();
}
// -----------------------------------------------------------------------------
