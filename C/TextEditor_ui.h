 /**
 * \file       TextEditor_ui.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date       25.07.2009
 * \version    1.0
 * \brief      This file contains class definition that arranges text editor window.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef TEXTEDITOR_H
#define TEXTEDITOR_H
// -----------------------------------------------------------------------------

#define TE_MODE_NORMAL_EDIT 0
#define TE_MODE_SCRIPT_EDIT 1
// -----------------------------------------------------------------------------

#include <QList>

#include "te/TextEditorDoc.h"
#include "ui_TextEditor.h"
// -----------------------------------------------------------------------------

class QTabWidget;
// -----------------------------------------------------------------------------

/**
* \class      TextEditor_ui
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       24.07.2009
* \brief      Class definition that arranges text editor window.
*/
class TextEditor_ui : public QMainWindow, public Ui_TextEditor
{
    Q_OBJECT

private:
     
     QPrinter* printer;                      ///< Pointer to printer.

     QMenu* toolBarMenu;                     ///< Pointer to menu toolbar.
     QMenu* dockedWindows;                   ///< Pointer to menu docked widnows.

     QToolBar* fileToolBar;                  ///< Pointer to file toolbar.
     QToolBar* printToolBar;                 ///< Pointer to print toolbar.
     QToolBar* undoredoToolBar;              ///< Pointer to undo, redo toolbar.
     QToolBar* ccpToolBar;                   ///< Pointer to copy, cut, paste toolbar.
     QToolBar* cfgToolBar;                   ///< Pointer to cconfiguration toolbar.
     
     QTabWidget* tabs;                       ///< Tabs for docs
     QList<TextEditorDoc*> docs;             ///< List of docs
     int adoc;                               ///< Index of active doc
     int _mode;                              ///< The text editor mode

     /// \brief Create toolbars.
     ///
     /// \return No return value.
     void createToolBars(void);

     /// \brief Create statusbar.
     ///
     /// \return No return value.
     void createStatusBar(void);
     
     /// \brief Function that returns suffix of the file
     ///
     /// \param __filename - given file name
     /// \return suffix of the file
     QString fileSuffix(QString __filename);
     
     /// \brief Function that creates tab title acording to the given file name
     ///
     /// \param __filename - given file name
     /// \return tab title
     QString createTabTitle(QString __filename);

protected:

     /// \brief Function is triggered when window is closed.
     ///
     /// \param event - Pointer to close event object.
     /// \return No return value.
     void closeEvent(QCloseEvent* event);
 
public:

     /// \brief Constructor TextEditor_ui class.
     ///
     /// \param *parent - Pointer to parent.
     TextEditor_ui(QMainWindow *parent = 0);

     /// \brief Destructor TextEditor_ui class.
     ~TextEditor_ui(void);

     /// \brief Function that sets the initial values of the window class
     ///
     /// \return no values returns
     void initWindow(void);
     
     /// \brief Function that sets the dialog working mode
     ///
     /// \param __mode - mode identifier
     /// \return no values returns
     void setMode(int __mode);

     /// \brief returns if the document is modified
     ///
     /// \return True when document is changed, otherwise false.
     bool changed(void);
     
     /// \brief function that returns the editor mode
     ///
     /// \return the editor mode
     int mode(void);
     
     /// \brief Returns the list of documents
     ///
     /// \return the list of documents
     QList<TextEditorDoc*>& documents(void);

     /// \brief Set status of document modification.
     /// 
     /// \param _c - Status of document modification.
     /// \return No return value.
     void setChanged(bool _c);

     /// \brief Handle event of dragging file on window area and check it data tape.
     ///
     /// \param event Pointer to parameters of dragging data on form.
     /// \return No return value.
     void dragEnterEvent(QDragEnterEvent* event);

     /// \brief Handle event of dropping file on window area and check it data tape.
     ///
     /// \param event - Pointer to parameters of dropping data on form.
     /// \return No return value.
     void dropEvent(QDropEvent* event);
     
     /// \brief Updates the state of checked actions
     ///
     /// \return No return value.
     void updateModeChecked(void);
     
     /// \brief Function that is treggered when user switches between documents
     ///
     /// \return No return value.
     void onSwitchDocs(void);
     
     /// \brief Opens a file.
     ///
     /// \param __tabIndex - index of tab where file will be loaded
     /// \param _fileName - File name to open.
     /// \return No return value.
     void openFile(int __tabIndex, QString _fileName);
     
     /// \brief Saves the content of the given doc to file
     ///
     /// \param __tabIndex - index of the doc
     /// \param _fileName - name of the file where content will be saved
     /// \return No return value.
     void saveDoc(int __tabIndex, QString _fileName);
     
     /// \brief Closes given document
     ///
     /// \param __tabIndex - index of the doc to be closed
     /// \return No return value.
     void closeDoc(int __tabIndex);
     
public slots:
     
     /// \brief Function that is triggered when user switch beetween tabs
     ///
     /// \param __index - index of the current tab
     /// \return No return value.
     void currentDocIndexChanged(int __index);

     /// \brief Open file dialog.
     ///
     /// \return No return value.
     void openFileDialog(void);
     
     /// \brief reloads document from file
     ///
     /// \return No return value.
     void reload(void);

     /// \brief Save file as.
     ///
     /// \return No return value.
     void saveAs(void);

     /// \brief Save changes.
     ///
     /// \return No return value.
     void save(void);

     /// \brief Save all changes in all docs
     ///
     /// \return No return value.
     void saveAll(void);

     /// \brief Create new dosument.
     ///
     /// \return No return value.
     void newClick(void);

     /// \brief Removes all the data from current document
     ///
     /// \return No return value.
     void clear(void);

     /// \brief Closes current docuemnt
     ///
     /// \return No return value.
     void closeDoc(void);

     /// \brief Closes all documents
     ///
     /// \param __confirm - determines confirmation of the action
     /// \return No return value.
     void closeAllDocs(bool __confirm = true);

     /// \brief Closes all documents except current
     ///
     /// \return No return value.
     void closeAllDocsExceptCurrent(void);

     /// \brief Switch application to Text mode.
     ///
     /// \return No return value.
     void useTextMode(void);
     
     /// \brief Switch application to PROLOG mode.
     ///
     /// \return No return value.
     void usePrologMode(void);

     /// \brief Switch application to HML mode.
     ///
     /// \return No return value.
     void useHMLmode(void);

     /// \brief Switch application to HMR mode.
     ///
     /// \return No return value.
     void useHMRmode(void);

     /// \brief Switch application to ECMA mode.
     ///
     /// \return No return value.
     void useEcmaMode(void);

     /// \brief Close editor.
     ///
     /// \return No return value.
     void closeForm(void);

     /// \brief Print current document.
     ///
     /// \return No return value.
     void printDoc(void);

     /// \brief Show print setup window.
     ///
     /// \return No return value.
     void printSetup(void);
     
     /// \brief Function that is triggered when user press undo button
     ///
     /// \return No return value.
     void undo(void);
     
     /// \brief Function that is triggered when user press redo button
     ///
     /// \return No return value.
     void redo(void);
     
     /// \brief Function that is triggered when user press copy button
     ///
     /// \return No return value.
     void copy(void);
     
     /// \brief Function that is triggered when user press cut button
     ///
     /// \return No return value.
     void cut(void);
     
     /// \brief Function that is triggered when user press paste button
     ///
     /// \return No return value.
     void paste(void);
     
     /// \brief Function that is triggered when user press selectAll button
     ///
     /// \return No return value.
     void selectAll(void);
     
     /// \brief Function that is triggered when user press delete button
     ///
     /// \return No return value.
     void deleteSelection(void);
     
     /// \brief Function that is triggered when user press mode configuration button
     ///
     /// \return No return value.
     void modeConfig(void);
     
     /// \brief Function that is triggered when state of the modification has been changed
     ///
     /// \param __modified - new state of modification
     /// \return No return value.
     void modificationStateChanged(bool __modified);
     
     /// \brief Function that converts model to prolog and displays it in the text editor
     ///
     /// \return No return value.
     void viewPrologCode(void);
     
     /// \brief Function that converts model to xml and displays it in the text editor
     ///
     /// \return No return value.
     void viewXmlCode(void);
     
     /// \brief Function that is triggered when user press wrap lines button
     ///
     /// \return No return value.
     void onWrapLineClick(void);
     
     /// \brief Function that is triggered when user press wrap lines button
     ///
     /// \return No return value.
     void useInsertMode(void);
     
     /// \brief Function that is triggered when user press wrap lines button
     ///
     /// \return No return value.
     void useOverwriteMode(void);
     
signals:

     /// \brief Signal that is emited when edior is closed
     ///
     /// \return no values return
     void onclose(void);
};
// -----------------------------------------------------------------------------

extern TextEditor_ui* TextEditor;
// -----------------------------------------------------------------------------

#endif
