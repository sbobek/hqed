/**
 * \file	Settings_ui.cpp
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	06.11.2007
 * \version	1.0
 * \brief	This file defines class arranges window configuration.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QSettings>
#include <QTcpSocket>
#include <QUdpSocket>

#include "../M/ARD/ARD_LevelList.h"
#include "../M/ARD/ARD_PropertyList.h"
#include "../V/ARD/gARDlevelList.h"
#include "../V/ARD/gTPHtree.h"

#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_Attribute.h"
#include "../M/XTT/XTT_Cell.h"

#include "widgets/sysTrayObject.h"

#include "ARD/widgets/ardGraphicsView.h"
#include "XTT/widgets/MyGraphicsView.h"
#include "XTT/widgets/StateGraphicsView.h"
#include "XTT/widgets/MyGraphicsScene.h"
#include "XTT/widgets/StateGraphicsScene.h"
#include "XTT/PluginEditor_ui.h"

#include "../namespaces/ns_hqed.h"
#include "../namespaces/ns_xtt.h"

#include "devPluginInterface/devPlugin.h"
#include "devPluginInterface/devPluginController.h"

#include "MainWin_ui.h"
#include "Settings_ui.h"
// -----------------------------------------------------------------------------

Settings_ui* Settings;
// -----------------------------------------------------------------------------

// #brief Constructor of Settings_ui class.
//
// #param parent Pointer to parent object.
Settings_ui::Settings_ui(QWidget*)
{
    setupUi(this);

    setWindowFlags(Qt::WindowMinimizeButtonHint);
    setWindowIcon(QIcon(":/all_images/images/mainico.png"));

    connect(listWidget, SIGNAL(currentRowChanged(int)), stackedWidget, SLOT(setCurrentIndex(int)));
    connect(listWidget_2, SIGNAL(currentRowChanged(int)), this, SLOT(onPluginSelectionChanged(int)));
    connect(stackedWidget, SIGNAL(currentChanged(int)), this, SLOT(currentTabChanged(int)));
    connect(pushButton, SIGNAL(released()), this, SLOT(applyButtonClick()));
    connect(pushButton_2, SIGNAL(released()), this, SLOT(closeButtonClick()));

    connect(toolButton_31, SIGNAL(clicked()), this, SLOT(onSelectColor14()));
    connect(toolButton_21, SIGNAL(clicked()), this, SLOT(onSelectColor15()));
    connect(toolButton_22, SIGNAL(clicked()), this, SLOT(onSelectColor16()));
    connect(toolButton_23, SIGNAL(clicked()), this, SLOT(onSelectColor17()));
    connect(toolButton_24, SIGNAL(clicked()), this, SLOT(onSelectColor18()));
    connect(toolButton_25, SIGNAL(clicked()), this, SLOT(onSelectColor19()));
    connect(toolButton_26, SIGNAL(clicked()), this, SLOT(onSelectColor20()));
    connect(toolButton_27, SIGNAL(clicked()), this, SLOT(onSelectColor21()));
    connect(toolButton_28, SIGNAL(clicked()), this, SLOT(onSelectColor22()));
    connect(toolButton_29, SIGNAL(clicked()), this, SLOT(onSelectColor23()));
    connect(toolButton_32, SIGNAL(clicked()), this, SLOT(onSelectColor24()));
    connect(toolButton_30, SIGNAL(clicked()), this, SLOT(onSelectColor25()));
    connect(toolButton_20, SIGNAL(clicked()), this, SLOT(onSelectColor26()));
    connect(toolButton_33, SIGNAL(clicked()), this, SLOT(onSelectColor27()));
    connect(toolButton_42, SIGNAL(clicked()), this, SLOT(onSelectColor28()));
    connect(toolButton_43, SIGNAL(clicked()), this, SLOT(onSelectColor29()));

    connect(toolButton, SIGNAL(clicked()), this, SLOT(onSelectColor1()));
    connect(toolButton_2, SIGNAL(clicked()), this, SLOT(onSelectColor2()));
    connect(toolButton_3, SIGNAL(clicked()), this, SLOT(onSelectColor3()));
    connect(toolButton_4, SIGNAL(clicked()), this, SLOT(onSelectColor4()));
    connect(toolButton_5, SIGNAL(clicked()), this, SLOT(onSelectColor5()));
    connect(toolButton_6, SIGNAL(clicked()), this, SLOT(onSelectColor6()));
    connect(toolButton_7, SIGNAL(clicked()), this, SLOT(onSelectColor7()));
    connect(toolButton_8, SIGNAL(clicked()), this, SLOT(onSelectColor8()));
    connect(toolButton_9, SIGNAL(clicked()), this, SLOT(onSelectColor9()));
    connect(toolButton_10, SIGNAL(clicked()), this, SLOT(onSelectColor10()));
    connect(toolButton_11, SIGNAL(clicked()), this, SLOT(onSelectColor11()));
    connect(toolButton_12, SIGNAL(clicked()), this, SLOT(onSelectColor12()));
    connect(toolButton_13, SIGNAL(clicked()), this, SLOT(onSelectColor13()));
    connect(toolButton_44, SIGNAL(clicked()), this, SLOT(onSelectColor44()));
    connect(toolButton_14, SIGNAL(clicked()), this, SLOT(changeTableFont()));
    connect(toolButton_15, SIGNAL(clicked()), this, SLOT(changeTableHeaderFont()));
    connect(toolButton_16, SIGNAL(clicked()), this, SLOT(changeTableTitleFont()));
    connect(toolButton_17, SIGNAL(clicked()), this, SLOT(changeConnectionLabelFont()));
    connect(toolButton_18, SIGNAL(clicked()), this, SLOT(changeTPHFont()));
    connect(toolButton_19, SIGNAL(clicked()), this, SLOT(changeARDdiagramFont()));

    connect(toolButton_34, SIGNAL(clicked()), this, SLOT(onEditPlugin()));
    connect(toolButton_35, SIGNAL(clicked()), this, SLOT(onDeletePlugin()));
    connect(toolButton_36, SIGNAL(clicked()), this, SLOT(onAddPlugin()));
    connect(toolButton_37, SIGNAL(toggled(bool)), this, SLOT(onPlUnlockChecked(bool)));
    connect(toolButton_38, SIGNAL(toggled(bool)), this, SLOT(onPlLockChecked(bool)));
    connect(toolButton_39, SIGNAL(clicked()), this, SLOT(onSelectEcmascriptDirectory()));
    connect(toolButton_40, SIGNAL(clicked()), this, SLOT(onSelectDefaultProjectDirectory()));
    connect(toolButton_41, SIGNAL(clicked()), this, SLOT(onSelectConfigurationDirectory()));

    connect(horizontalSlider, SIGNAL(valueChanged(int)), this, SLOT(onSlider1ChangePosition(int)));
    connect(horizontalSlider_2, SIGNAL(valueChanged(int)), this, SLOT(onSlider2ChangePosition(int)));
    connect(horizontalSlider_3, SIGNAL(valueChanged(int)), this, SLOT(onSlider3ChangePosition(int)));
    connect(horizontalSlider_4, SIGNAL(valueChanged(int)), this, SLOT(onSlider4ChangePosition(int)));
    connect(horizontalSlider_5, SIGNAL(valueChanged(int)), this, SLOT(onSlider5ChangePosition(int)));
    connect(horizontalSlider_6, SIGNAL(valueChanged(int)), this, SLOT(onSlider6ChangePosition(int)));
    connect(horizontalSlider_7, SIGNAL(valueChanged(int)), this, SLOT(onSlider7ChangePosition(int)));
    connect(horizontalSlider_8, SIGNAL(valueChanged(int)), this, SLOT(onSlider8ChangePosition(int)));
    connect(horizontalSlider_9, SIGNAL(valueChanged(int)), this, SLOT(onSlider9ChangePosition(int)));
    connect(horizontalSlider_10, SIGNAL(valueChanged(int)), this, SLOT(onSlider10ChangePosition(int)));
    connect(horizontalSlider_11, SIGNAL(valueChanged(int)), this, SLOT(onSlider11ChangePosition(int)));
    connect(horizontalSlider_12, SIGNAL(valueChanged(int)), this, SLOT(onSlider12ChangePosition(int)));
    connect(horizontalSlider_13, SIGNAL(valueChanged(int)), this, SLOT(onSlider13ChangePosition(int)));
    connect(horizontalSlider_14, SIGNAL(valueChanged(int)), this, SLOT(changeXttZoomFactorSliderBar(int)));
    connect(horizontalSlider_15, SIGNAL(valueChanged(int)), this, SLOT(onSlider15ChangePosition(int)));
    connect(horizontalSlider_16, SIGNAL(valueChanged(int)), this, SLOT(onSlider16ChangePosition(int)));
    connect(horizontalSlider_17, SIGNAL(valueChanged(int)), this, SLOT(onSlider17ChangePosition(int)));
    connect(horizontalSlider_18, SIGNAL(valueChanged(int)), this, SLOT(onSlider18ChangePosition(int)));
    connect(horizontalSlider_19, SIGNAL(valueChanged(int)), this, SLOT(onSlider19ChangePosition(int)));
    connect(horizontalSlider_20, SIGNAL(valueChanged(int)), this, SLOT(onSlider20ChangePosition(int)));
    connect(horizontalSlider_21, SIGNAL(valueChanged(int)), this, SLOT(onSlider21ChangePosition(int)));
    connect(horizontalSlider_22, SIGNAL(valueChanged(int)), this, SLOT(onSlider22ChangePosition(int)));
    connect(horizontalSlider_23, SIGNAL(valueChanged(int)), this, SLOT(onSlider23ChangePosition(int)));
    connect(horizontalSlider_24, SIGNAL(valueChanged(int)), this, SLOT(onSlider24ChangePosition(int)));
    connect(horizontalSlider_25, SIGNAL(valueChanged(int)), this, SLOT(onSlider25ChangePosition(int)));
    connect(horizontalSlider_26, SIGNAL(valueChanged(int)), this, SLOT(onSlider26ChangePosition(int)));
    connect(horizontalSlider_27, SIGNAL(valueChanged(int)), this, SLOT(onSlider27ChangePosition(int)));
    connect(horizontalSlider_28, SIGNAL(valueChanged(int)), this, SLOT(onSlider28ChangePosition(int)));
    connect(horizontalSlider_29, SIGNAL(valueChanged(int)), this, SLOT(onSlider29ChangePosition(int)));
    connect(horizontalSlider_14, SIGNAL(valueChanged(int)), this, SLOT(changeXttZoomFactorSliderBar(int)));
    connect(horizontalSlider_30, SIGNAL(valueChanged(int)), this, SLOT(changeArdZoomFactorSliderBar(int)));
    connect(horizontalSlider_31, SIGNAL(valueChanged(int)), this, SLOT(onSlider31ChangePosition(int)));
    connect(horizontalSlider_32, SIGNAL(valueChanged(int)), this, SLOT(onSlider32ChangePosition(int)));
    connect(horizontalSlider_33, SIGNAL(valueChanged(int)), this, SLOT(onSlider33ChangePosition(int)));
    connect(horizontalSlider_34, SIGNAL(valueChanged(int)), this, SLOT(onSlider34ChangePosition(int)));

    connect(doubleSpinBox, SIGNAL(valueChanged(double)), this, SLOT(changeXttZoomFactorSpinBox(double)));
    connect(doubleSpinBox_2, SIGNAL(valueChanged(double)), this, SLOT(changeArdZoomFactorSpinBox(double)));

    connect(checkBox_9, SIGNAL(toggled(bool)), this, SLOT(checkBox9changed(bool)));
    connect(checkBox_11, SIGNAL(toggled(bool)), this, SLOT(checkBox11changed(bool)));
    connect(checkBox_15, SIGNAL(toggled(bool)), this, SLOT(checkBox15changed(bool)));
    connect(checkBox_29, SIGNAL(toggled(bool)), this, SLOT(onCheckBox29checked(bool)));
    connect(checkBox_35, SIGNAL(toggled(bool)), this, SLOT(onCheckBox35checked(bool)));
    connect(lineEdit, SIGNAL(textChanged(QString)), this, SLOT(spaceCharacterChange(QString)));

    // Zaladowanie opcji
    conf = new QSettings(hqed::appRootPath() + "/hqed.ini", QSettings::IniFormat);

    listWidget->setCurrentRow(0);
    label_59->setText("");
    label_60->setText("");
    label_61->setText("");
    label_62->setText("");

    // Wczytanie opcji
    loadOptions();

    // Form layout
    layoutForm();
    hqed::centerWindow(this);
    listWidget->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
}
// -----------------------------------------------------------------------------

// #brief Destructor of Settings_ui class.
Settings_ui::~Settings_ui(void)
{
    delete swp0gb27GL;
    delete swp0gb28GL;
    delete swp0gb29GL;
    delete swp0gb30GL;
    delete swp0gb35GL;
    delete swp0gb36GL;
    delete swp0gb39GL;
    delete swp0GL;

    delete swp1gb20GL;
    delete swp1gb21GL;
    delete swp1gb25GL;
    delete swp1gb26GL;
    delete swp1GL;

    delete swp2gb17GL;
    delete swp2gb18GL;
    delete swp2gb19GL;
    delete swp2gb34GL;
    delete swp2GL;

    delete swp3gb0GL;
    delete swp3gb3GL;
    delete swp3gb8GL;
    delete swp3GL;

    delete swp4GL;
    delete swp4gb7GL;
    delete swp4gb9GL;
    delete swp4gb10GL;
    delete swp4gb11GL;
    delete swp4gb14GL;
    delete swp4gb31GL;
    delete swp4gb38GL;

    delete swp5gb2GL;
    delete swp5gb13GL;
    delete swp5gb12GL;
    delete swp5gb37GL;
    delete swp5gb33GL;
    delete swp5GL;

    delete swp6gb15GL;
    delete swp6gb16GL;
    delete swp6gb32GL;
    delete swp6GL;

    delete mainGL;
}
// -----------------------------------------------------------------------------

// #brief Functions that layouts the items on the widget
//
// #return no values return
void Settings_ui::layoutForm(void)
{
    this->resize(10, height());
    listWidget->resize(5, 5);

    mainGL = new QGridLayout;
    mainGL->addWidget(listWidget,    0, 0, 2, 1);
    mainGL->addWidget(stackedWidget, 0, 1, 1, 3);
    mainGL->addWidget(pushButton,    1, 2);
    mainGL->addWidget(pushButton_2,  1, 3);
    mainGL->setColumnMinimumWidth(0, 150);
    mainGL->setRowStretch(2, 1);
    mainGL->setColumnStretch(1, 1);
    mainGL->setColumnStretch(0, 0);
    hqed::setupLayout(mainGL);
    setLayout(mainGL);

    swp0GL = new QGridLayout;
    swp0GL->addWidget(groupBox_27, 0, 0);
    swp0GL->addWidget(groupBox_28, 1, 0);
    swp0GL->addWidget(groupBox_29, 2, 0);
    swp0GL->addWidget(groupBox_30, 3, 0);
    swp0GL->addWidget(groupBox_39, 4, 0);
    swp0GL->addWidget(groupBox_35, 0, 1, 4, 1);
    swp0GL->addWidget(groupBox_36, 4, 1, 2, 1);
    swp0GL->setRowStretch(5, 1);
    swp0GL->setColumnStretch(2, 1);
    hqed::setupLayout(swp0GL);
    stackedWidget->widget(0)->setLayout(swp0GL);

    swp0gb27GL = new QGridLayout;
    swp0gb27GL->addWidget(checkBox_54, 0, 0);
    hqed::setupLayout(swp0gb27GL);
    groupBox_27->setLayout(swp0gb27GL);

    swp0gb28GL = new QGridLayout;
    swp0gb28GL->addWidget(checkBox_29, 0, 0);
    swp0gb28GL->addWidget(checkBox_30, 1, 0);
    hqed::setupLayout(swp0gb28GL);
    groupBox_28->setLayout(swp0gb28GL);

    swp0gb29GL = new QGridLayout;
    swp0gb29GL->addWidget(comboBox_2, 0, 0);
    hqed::setupLayout(swp0gb29GL);
    groupBox_29->setLayout(swp0gb29GL);

    swp0gb30GL = new QGridLayout;
    swp0gb30GL->addWidget(checkBox_33, 0, 0);
    hqed::setupLayout(swp0gb30GL);
    groupBox_30->setLayout(swp0gb30GL);

    swp1GL = new QGridLayout;
    swp1GL->addWidget(groupBox_21, 0, 0, 4, 1);
    swp1GL->addWidget(groupBox_20, 0, 1);
    swp1GL->addWidget(groupBox_25, 1, 1);
    swp1GL->addWidget(groupBox_26, 2, 1);
    swp1GL->setRowStretch(4, 1);
    swp1GL->setColumnStretch(0, 1);
    hqed::setupLayout(swp1GL);
    stackedWidget->widget(1)->setLayout(swp1GL);

    swp1gb20GL = new QGridLayout;
    swp1gb20GL->addWidget(checkBox_24, 0, 0);
    swp1gb20GL->addWidget(checkBox_22, 1, 0);
    swp1gb20GL->addWidget(checkBox_23, 2, 0);
    hqed::setupLayout(swp1gb20GL);
    groupBox_20->setLayout(swp1gb20GL);

    swp1gb21GL = new QGridLayout;
    swp1gb21GL->addWidget(groupBox_22,   0, 0);
    swp1gb21GL->addWidget(groupBox_23,   0, 1);
    swp1gb21GL->addWidget(groupBox_24,   0, 2);
    swp1gb21GL->addWidget(label_43,            1, 0);
    swp1gb21GL->addWidget(toolButton_31,       1, 1);
    swp1gb21GL->addWidget(horizontalSlider_22, 1, 2);
    swp1gb21GL->addWidget(label_33,            2, 0);
    swp1gb21GL->addWidget(toolButton_21,       2, 1);
    swp1gb21GL->addWidget(horizontalSlider_23, 2, 2);
    swp1gb21GL->addWidget(label_41,            3, 0);
    swp1gb21GL->addWidget(toolButton_23,       3, 1);
    swp1gb21GL->addWidget(horizontalSlider_25, 3, 2);
    swp1gb21GL->addWidget(label_40,            4, 0);
    swp1gb21GL->addWidget(toolButton_24,       4, 1);
    swp1gb21GL->addWidget(horizontalSlider_26, 4, 2);
    swp1gb21GL->addWidget(label_39,            5, 0);
    swp1gb21GL->addWidget(toolButton_25,       5, 1);
    swp1gb21GL->addWidget(horizontalSlider_27, 5, 2);
    swp1gb21GL->addWidget(label_42,            6, 0);
    swp1gb21GL->addWidget(toolButton_22,       6, 1);
    swp1gb21GL->addWidget(horizontalSlider_24, 6, 2);
    swp1gb21GL->addWidget(label_38,            7, 0);
    swp1gb21GL->addWidget(toolButton_26,       7, 1);
    swp1gb21GL->addWidget(horizontalSlider_28, 7, 2);
    swp1gb21GL->addWidget(label_32,            8, 0);
    swp1gb21GL->addWidget(toolButton_27,       8, 1);
    swp1gb21GL->addWidget(horizontalSlider_29, 8, 2);
    swp1gb21GL->addWidget(label_37,            9, 0);
    swp1gb21GL->addWidget(toolButton_28,       9, 1);
    swp1gb21GL->addWidget(horizontalSlider_21, 9, 2);
    swp1gb21GL->addWidget(label_36,            10, 0);
    swp1gb21GL->addWidget(toolButton_29,       10, 1);
    swp1gb21GL->addWidget(horizontalSlider_20, 10, 2);
    swp1gb21GL->addWidget(label_35,            11, 0);
    swp1gb21GL->addWidget(toolButton_32,       11, 1);
    swp1gb21GL->addWidget(horizontalSlider_19, 11, 2);
    swp1gb21GL->addWidget(label_47,            12, 0);
    swp1gb21GL->addWidget(toolButton_33,       12, 1);
    swp1gb21GL->addWidget(horizontalSlider_31, 12, 2);
    swp1gb21GL->addWidget(label_34,            13, 0);
    swp1gb21GL->addWidget(toolButton_30,       13, 1);
    swp1gb21GL->addWidget(horizontalSlider_18, 13, 2);
    swp1gb21GL->addWidget(label_31,            14, 0);
    swp1gb21GL->addWidget(toolButton_20,       14, 1);
    swp1gb21GL->addWidget(horizontalSlider_17, 14, 2);
    swp1gb21GL->addWidget(checkBox_25,         15, 0);
    swp1gb21GL->setRowStretch(16, 1);
    swp1gb21GL->setColumnStretch(1, 1);
    hqed::setupLayout(swp1gb21GL);
    groupBox_21->setLayout(swp1gb21GL);

    swp1gb25GL = new QGridLayout;
    swp1gb25GL->addWidget(label_44,            0, 0);
    swp1gb25GL->addWidget(doubleSpinBox_2,     0, 1, 1, 2);
    swp1gb25GL->addWidget(horizontalSlider_30, 1, 0, 1, 3);
    swp1gb25GL->addWidget(checkBox_26,         2, 0, 1, 2);
    hqed::setupLayout(swp1gb25GL);
    groupBox_25->setLayout(swp1gb25GL);

    swp1gb26GL = new QGridLayout;
    swp1gb26GL->addWidget(checkBox_27, 0, 0);
    hqed::setupLayout(swp1gb26GL);
    groupBox_26->setLayout(swp1gb26GL);

    swp2GL = new QGridLayout;
    swp2GL->addWidget(groupBox_19, 0, 0);
    swp2GL->addWidget(groupBox_17, 1, 0);
    swp2GL->addWidget(groupBox_18, 2, 0);
    swp2GL->addWidget(groupBox_34, 0, 1);
    swp2GL->setRowStretch(3, 1);
    swp2GL->setColumnStretch(2, 1);
    hqed::setupLayout(swp2GL);
    stackedWidget->widget(2)->setLayout(swp2GL);

    swp2gb19GL = new QGridLayout;
    swp2gb19GL->addWidget(label_28,      0, 0);
    swp2gb19GL->addWidget(toolButton_18, 1, 0, 1, 2);
    swp2gb19GL->addWidget(label_30,      2, 0);
    swp2gb19GL->addWidget(toolButton_19, 3, 0, 1, 2);
    swp2gb19GL->setColumnStretch(1, 1);
    hqed::setupLayout(swp2gb19GL);
    groupBox_19->setLayout(swp2gb19GL);

    swp2gb17GL = new QGridLayout;
    swp2gb17GL->addWidget(label_29,    0, 0);
    swp2gb17GL->addWidget(spinBox_7,   0, 1);
    swp2gb17GL->addWidget(checkBox_34, 1, 0, 1, 3);
    swp2gb17GL->setColumnStretch(3, 1);
    hqed::setupLayout(swp2gb17GL);
    groupBox_17->setLayout(swp2gb17GL);

    swp2gb18GL = new QGridLayout;
    swp2gb18GL->addWidget(label_27,            0, 0);
    swp2gb18GL->addWidget(horizontalSlider_16, 1, 0, 1, 2);
    swp2gb18GL->addWidget(label_49,            2, 0);
    swp2gb18GL->addWidget(comboBox_4,          3, 0, 1, 2);
    hqed::setupLayout(swp2gb18GL);
    groupBox_18->setLayout(swp2gb18GL);

    swp2gb34GL = new QGridLayout;
    swp2gb34GL->addWidget(checkBox_50, 0, 0);
    swp2gb34GL->addWidget(checkBox_48, 1, 0);
    swp2gb34GL->addWidget(checkBox_49, 2, 0);
    hqed::setupLayout(swp2gb34GL);
    groupBox_34->setLayout(swp2gb34GL);

    swp3GL = new QGridLayout;
    swp3GL->addWidget(groupBox,   0, 0, 3, 1);
    swp3GL->addWidget(groupBox_3, 0, 1);
    swp3GL->addWidget(groupBox_8, 1, 1);
    swp3GL->setRowStretch(3, 1);
    swp3GL->setColumnStretch(0, 1);
    hqed::setupLayout(swp3GL);
    stackedWidget->widget(3)->setLayout(swp3GL);

    swp3gb0GL = new QGridLayout;
    swp3gb0GL->addWidget(groupBox_4, 0, 0);
    swp3gb0GL->addWidget(groupBox_5, 0, 1);
    swp3gb0GL->addWidget(groupBox_6, 0, 2);
    swp3gb0GL->addWidget(label,               1, 0);
    swp3gb0GL->addWidget(toolButton,          1, 1);
    swp3gb0GL->addWidget(horizontalSlider,    1, 2);
    swp3gb0GL->addWidget(label_66,            2, 0);
    swp3gb0GL->addWidget(toolButton_42,       2, 1);
    swp3gb0GL->addWidget(horizontalSlider_32, 2, 2);
    swp3gb0GL->addWidget(label_2,             3, 0);
    swp3gb0GL->addWidget(toolButton_2,        3, 1);
    swp3gb0GL->addWidget(horizontalSlider_2,  3, 2);
    swp3gb0GL->addWidget(label_67,            4, 0);
    swp3gb0GL->addWidget(toolButton_43,       4, 1);
    swp3gb0GL->addWidget(horizontalSlider_33, 4, 2);
    swp3gb0GL->addWidget(label_12,            5, 0);
    swp3gb0GL->addWidget(toolButton_12,       5, 1);
    swp3gb0GL->addWidget(horizontalSlider_12, 5, 2);
    swp3gb0GL->addWidget(label_71,            6, 0);
    swp3gb0GL->addWidget(toolButton_44,       6, 1);
    swp3gb0GL->addWidget(horizontalSlider_34, 6, 2);
    swp3gb0GL->addWidget(label_13,            7, 0);
    swp3gb0GL->addWidget(toolButton_13,       7, 1);
    swp3gb0GL->addWidget(horizontalSlider_13, 7, 2);
    swp3gb0GL->addWidget(label_7,             8, 0);
    swp3gb0GL->addWidget(toolButton_6,        8, 1);
    swp3gb0GL->addWidget(horizontalSlider_6,  8, 2);
    swp3gb0GL->addWidget(label_8,             9, 0);
    swp3gb0GL->addWidget(toolButton_7,        9, 1);
    swp3gb0GL->addWidget(horizontalSlider_7,  9, 2);
    swp3gb0GL->addWidget(label_11,            10, 0);
    swp3gb0GL->addWidget(toolButton_10,       10, 1);
    swp3gb0GL->addWidget(horizontalSlider_10, 10, 2);
    swp3gb0GL->addWidget(label_10,            11, 0);
    swp3gb0GL->addWidget(toolButton_11,       11, 1);
    swp3gb0GL->addWidget(horizontalSlider_11, 11, 2);
    swp3gb0GL->addWidget(label_3,             12, 0);
    swp3gb0GL->addWidget(toolButton_3,        12, 1);
    swp3gb0GL->addWidget(horizontalSlider_3,  12, 2);
    swp3gb0GL->addWidget(label_4,             13, 0);
    swp3gb0GL->addWidget(toolButton_4,        13, 1);
    swp3gb0GL->addWidget(horizontalSlider_4,  13, 2);
    swp3gb0GL->addWidget(label_5,             14, 0);
    swp3gb0GL->addWidget(toolButton_5,        14, 1);
    swp3gb0GL->addWidget(horizontalSlider_5,  14, 2);
    swp3gb0GL->addWidget(label_9,             15, 0);
    swp3gb0GL->addWidget(toolButton_8,        15, 1);
    swp3gb0GL->addWidget(horizontalSlider_8,  15, 2);
    swp3gb0GL->addWidget(label_6,             16, 0);
    swp3gb0GL->addWidget(toolButton_9,        16, 1);
    swp3gb0GL->addWidget(horizontalSlider_9,  16, 2);
    swp3gb0GL->addWidget(checkBox, 17, 0);
    swp3gb0GL->setRowStretch(18, 1);
    swp3gb0GL->setColumnStretch(1, 1);
    hqed::setupLayout(swp3gb0GL);
    groupBox->setLayout(swp3gb0GL);

    swp3gb3GL = new QGridLayout;
    swp3gb3GL->addWidget(checkBox_2, 0, 0);
    swp3gb3GL->addWidget(checkBox_6, 1, 0);
    swp3gb3GL->addWidget(checkBox_7, 2, 0);
    swp3gb3GL->setRowStretch(3, 1);
    hqed::setupLayout(swp3gb3GL);
    groupBox_3->setLayout(swp3gb3GL);

    swp3gb8GL = new QGridLayout;
    swp3gb8GL->addWidget(label_18,            0, 0);
    swp3gb8GL->addWidget(doubleSpinBox,       0, 1, 1, 2);
    swp3gb8GL->addWidget(horizontalSlider_14, 1, 0, 1, 3);
    swp3gb8GL->addWidget(checkBox_8,          2, 0, 1, 2);
    swp3gb8GL->setRowStretch(3, 1);
    swp3gb8GL->setColumnStretch(3, 1);
    hqed::setupLayout(swp3gb8GL);
    groupBox_8->setLayout(swp3gb8GL);

    swp4GL = new QGridLayout;
    swp4GL->addWidget(groupBox_7,  0, 0, 2, 1);
    swp4GL->addWidget(groupBox_9,  2, 0, 2, 1);
    swp4GL->addWidget(groupBox_10, 0, 1);
    swp4GL->addWidget(groupBox_11, 1, 1, 2, 1);
    swp4GL->addWidget(groupBox_14, 3, 1, 1, 2);
    swp4GL->addWidget(groupBox_31, 0, 2, 1, 1);
    swp4GL->addWidget(groupBox_38, 1, 2, 2, 1);
    swp4GL->setRowStretch(4, 1);
    swp4GL->setColumnStretch(4, 1);
    hqed::setupLayout(swp4GL);
    stackedWidget->widget(4)->setLayout(swp4GL);

    swp4gb7GL = new QGridLayout;
    swp4gb7GL->addWidget(label_14,      0, 0);
    swp4gb7GL->addWidget(toolButton_14, 1, 0, 1, 2);
    swp4gb7GL->addWidget(label_15,      2, 0);
    swp4gb7GL->addWidget(toolButton_15, 3, 0, 1, 2);
    swp4gb7GL->addWidget(label_16,      4, 0);
    swp4gb7GL->addWidget(toolButton_16, 5, 0, 1, 2);
    swp4gb7GL->addWidget(label_17,      6, 0);
    swp4gb7GL->addWidget(toolButton_17, 7, 0, 1, 2);
    swp4gb7GL->setColumnStretch(2, 1);
    hqed::setupLayout(swp4gb7GL);
    groupBox_7->setLayout(swp4gb7GL);

    swp4gb9GL = new QGridLayout;
    swp4gb9GL->addWidget(checkBox_35, 0, 0, 1, 2);
    swp4gb9GL->addWidget(label_19,    1, 0);
    swp4gb9GL->addWidget(spinBox,     1, 1);
    swp4gb9GL->addWidget(label_20,    2, 0);
    swp4gb9GL->addWidget(spinBox_2,   2, 1);
    swp4gb9GL->addWidget(label_21,    3, 0);
    swp4gb9GL->addWidget(spinBox_3,   3, 1);
    swp4gb9GL->setColumnStretch(2, 1);
    hqed::setupLayout(swp4gb9GL);
    groupBox_9->setLayout(swp4gb9GL);

    swp4gb10GL = new QGridLayout;
    swp4gb10GL->addWidget(checkBox_10, 0, 0, 1, 2);
    swp4gb10GL->addWidget(checkBox_9,  1, 0, 1, 2);
    swp4gb10GL->addWidget(label_64,    2, 0);
    swp4gb10GL->addWidget(lineEdit_3,  2, 1);
    swp4gb10GL->addWidget(label_23,    3, 0);
    swp4gb10GL->addWidget(lineEdit_2,  3, 1);
    hqed::setupLayout(swp4gb10GL);
    groupBox_10->setLayout(swp4gb10GL);

    swp4gb11GL = new QGridLayout;
    swp4gb11GL->addWidget(label_22,    0, 0);
    swp4gb11GL->addWidget(lineEdit,    0, 1);
    swp4gb11GL->addWidget(label_25,    1, 0);
    swp4gb11GL->addWidget(spinBox_5,   1, 1);
    swp4gb11GL->addWidget(label_26,    2, 0);
    swp4gb11GL->addWidget(spinBox_6,   2, 1);
    swp4gb11GL->addWidget(checkBox_31, 3, 0, 1, 2);
    swp4gb11GL->addWidget(checkBox_32, 4, 0, 1, 2);
    hqed::setupLayout(swp4gb11GL);
    groupBox_11->setLayout(swp4gb11GL);

    swp4gb14GL = new QGridLayout;
    swp4gb14GL->addWidget(label_24,            0, 0, 1, 2);
    swp4gb14GL->addWidget(horizontalSlider_15, 0, 2);
    swp4gb14GL->addWidget(label_48,            1, 0);
    swp4gb14GL->addWidget(comboBox_3,          1, 1, 1, 2);
    hqed::setupLayout(swp4gb14GL);
    groupBox_14->setLayout(swp4gb14GL);

    swp0gb39GL = new QGridLayout;
    swp0gb39GL->addWidget(label_70,    0, 0);
    swp0gb39GL->addWidget(spinBox_10,  0, 1);
    swp0gb39GL->addWidget(checkBox_53, 1, 0, 1, 2);
    hqed::setupLayout(swp0gb39GL);
    groupBox_39->setLayout(swp0gb39GL);

    swp0gb35GL = new QGridLayout;
    swp0gb35GL->addWidget(listWidget_2,   0, 0, 6, 2);
    swp0gb35GL->addWidget(toolButton_38,  0, 2);
    swp0gb35GL->addWidget(toolButton_37,  1, 2);
    swp0gb35GL->addWidget(toolButton_36,  3, 2);
    swp0gb35GL->addWidget(toolButton_34,  4, 2);
    swp0gb35GL->addWidget(toolButton_35,  5, 2);
    swp0gb35GL->addWidget(label_55,  6, 0);
    swp0gb35GL->addWidget(label_56,  7, 0);
    swp0gb35GL->addWidget(label_57,  8, 0);
    swp0gb35GL->addWidget(label_58,  9, 0);
    swp0gb35GL->addWidget(label_59,  6, 1, 1, 2);
    swp0gb35GL->addWidget(label_60,  7, 1, 1, 2);
    swp0gb35GL->addWidget(label_61,  8, 1, 1, 2);
    swp0gb35GL->addWidget(label_62,  9, 1, 1, 2);
    swp0gb35GL->setColumnStretch(1, 1);
    swp0gb35GL->setRowStretch(2, 1);
    hqed::setupLayout(swp0gb35GL);
    groupBox_35->setLayout(swp0gb35GL);

    swp0gb36GL = new QGridLayout;
    swp0gb36GL->addWidget(label_50,      0, 0);
    swp0gb36GL->addWidget(label_51,      1, 0);
    swp0gb36GL->addWidget(toolButton_39, 1, 1);
    swp0gb36GL->addWidget(label_52,      2, 0);
    swp0gb36GL->addWidget(label_53,      3, 0);
    swp0gb36GL->addWidget(toolButton_40, 3, 1);
    swp0gb36GL->addWidget(label_63,      4, 0);
    swp0gb36GL->addWidget(label_54,      5, 0);
    swp0gb36GL->addWidget(toolButton_41, 5, 1);
    hqed::setupLayout(swp0gb36GL);
    groupBox_36->setLayout(swp0gb36GL);

    swp4gb31GL = new QGridLayout;
    swp4gb31GL->addWidget(checkBox_52, 0, 0);
    swp4gb31GL->addWidget(checkBox_36, 1, 0);
    swp4gb31GL->addWidget(checkBox_40, 2, 0);
    swp4gb31GL->addWidget(checkBox_41, 3, 0);
    swp4gb31GL->addWidget(checkBox_44, 4, 0);
    swp4gb31GL->addWidget(checkBox_45, 5, 0);
    hqed::setupLayout(swp4gb31GL);
    groupBox_31->setLayout(swp4gb31GL);

    swp4gb38GL = new QGridLayout;
    swp4gb38GL->addWidget(checkBox_46, 0, 0);
    swp4gb38GL->addWidget(checkBox_47, 1, 0);
    swp4gb38GL->addWidget(label_68,    2, 0);
    swp4gb38GL->addWidget(comboBox_5,  3, 0);
    hqed::setupLayout(swp4gb38GL);
    groupBox_38->setLayout(swp4gb38GL);

    swp5GL = new QGridLayout;
    swp5GL->addWidget(groupBox_2,  0, 0);
    swp5GL->addWidget(groupBox_13, 1, 0);
    swp5GL->addWidget(groupBox_12, 2, 0);
    swp5GL->addWidget(groupBox_37, 3, 0);
    swp5GL->addWidget(groupBox_33, 0, 1);
    swp5GL->setRowStretch(4, 1);
    swp5GL->setColumnStretch(2, 1);
    hqed::setupLayout(swp5GL);
    stackedWidget->widget(5)->setLayout(swp5GL);

    swp5gb2GL = new QGridLayout;
    swp5gb2GL->addWidget(checkBox_3, 0, 0);
    swp5gb2GL->addWidget(checkBox_4, 1, 0);
    swp5gb2GL->addWidget(checkBox_5, 2, 0);
    hqed::setupLayout(swp5gb2GL);
    groupBox_2->setLayout(swp5gb2GL);

    swp5gb13GL = new QGridLayout;
    swp5gb13GL->addWidget(checkBox_12, 0, 0);
    hqed::setupLayout(swp5gb13GL);
    groupBox_13->setLayout(swp5gb13GL);

    swp5gb12GL = new QGridLayout;
    swp5gb12GL->addWidget(checkBox_11, 0, 0);
    swp5gb12GL->addWidget(checkBox_13, 1, 0);
    swp5gb12GL->addWidget(checkBox_14, 2, 0);
    hqed::setupLayout(swp5gb12GL);
    groupBox_12->setLayout(swp5gb12GL);

    swp5gb37GL = new QGridLayout;
    swp5gb37GL->addWidget(label_69,  0, 0);
    swp5gb37GL->addWidget(spinBox_9, 0, 1);
    swp5gb37GL->addWidget(label_65,  1, 0);
    swp5gb37GL->addWidget(spinBox_8, 1, 1);
    hqed::setupLayout(swp5gb37GL);
    groupBox_37->setLayout(swp5gb37GL);

    swp5gb33GL = new QGridLayout;
    swp5gb33GL->addWidget(checkBox_42,   0, 0);
    swp5gb33GL->addWidget(checkBox_43,   1, 0);
    swp5gb33GL->addWidget(checkBox_51,   2, 0);
    swp5gb33GL->setColumnStretch(1, 1);
    hqed::setupLayout(swp5gb33GL);
    groupBox_33->setLayout(swp5gb33GL);

    swp6GL = new QGridLayout;
    swp6GL->addWidget(groupBox_15, 0, 0);
    swp6GL->addWidget(groupBox_16, 1, 0);
    swp6GL->addWidget(groupBox_32, 2, 0);
    swp6GL->setRowStretch(3, 1);
    swp6GL->setColumnStretch(1, 1);
    hqed::setupLayout(swp6GL);
    stackedWidget->widget(6)->setLayout(swp6GL);

    swp6gb15GL = new QGridLayout;
    swp6gb15GL->addWidget(checkBox_15, 0, 0);
    swp6gb15GL->addWidget(checkBox_16, 1, 0);
    swp6gb15GL->addWidget(checkBox_17, 2, 0);
    hqed::setupLayout(swp6gb15GL);
    groupBox_15->setLayout(swp6gb15GL);

    swp6gb16GL = new QGridLayout;
    swp6gb16GL->addWidget(checkBox_18, 0, 0);
    swp6gb16GL->addWidget(checkBox_21, 1, 0);
    swp6gb16GL->addWidget(checkBox_19, 2, 0);
    swp6gb16GL->addWidget(checkBox_20, 3, 0);
    hqed::setupLayout(swp6gb16GL);
    groupBox_16->setLayout(swp6gb16GL);

    swp6gb32GL = new QGridLayout;
    swp6gb32GL->addWidget(checkBox_37, 0, 0);
    swp6gb32GL->addWidget(checkBox_38, 1, 0);
    swp6gb32GL->addWidget(checkBox_39, 2, 0);
    hqed::setupLayout(swp6gb32GL);
    groupBox_32->setLayout(swp6gb32GL);
}
// -----------------------------------------------------------------------------

// #brief Call when close button is clicked.
//
// #return No return value.
void Settings_ui::closeButtonClick(void)
{
    loadOptions();
    applyOptions();
    close();
}
// -----------------------------------------------------------------------------

// #brief Call when apply button is clicked.
//
// #return No return value.
void Settings_ui::applyButtonClick(void)
{
    applyOptions();
    saveOptions();
}
// -----------------------------------------------------------------------------

// #brief Apply settings in application.
//
// #return No return value.
void Settings_ui::applyOptions(void)
{
    bool cm = colorMode(MainWin->currentMode());
    bool am = useAntialiasing(MainWin->currentMode());
    bool atm = useTextAntialiasing(MainWin->currentMode());
    bool sptm = useSmoothPixmapTransform(MainWin->currentMode());
    MainWin->setColorMode(cm);
    MainWin->setAntialiasingStatus(am);
    MainWin->setTextAntialiasingStatus(atm);
    MainWin->setSmoothPixmapTransformStatus(sptm);

    ardPropertyList->tphTree()->refresh();
    ardLevelList->gLevelList()->alignLevels(0, true);
    MainWin->ard_modelView->setPalette(ardSceneColor());
    MainWin->ard_modelView->scene()->setBackgroundBrush(ardSceneColor());
    MainWin->ard_tphView->setPalette(ardTPHsceneColor());
    MainWin->ard_tphView->scene()->setBackgroundBrush(ardTPHsceneColor());

    TableList->RefreshAll();
    MainWin->xtt_graphicsView->setPalette(xttSceneBackgroundColor());
    MainWin->xtt_graphicsView->scene()->setBackgroundBrush(xttSceneBackgroundColor());
    MainWin->xtt_state_graphicsView->setPalette(xttSceneBackgroundColor());
    MainWin->xtt_state_graphicsView->scene()->setBackgroundBrush(xttSceneBackgroundColor());

    // Sprawdzanie czy system operacyjny obsluguje systemTray functions
    if((showSystemTrayIcon()) && (!QSystemTrayIcon::isSystemTrayAvailable()))
    {
        QMessageBox::warning(NULL, "HQEd", "This operating system does not support system tray.\n\"Show system tray icon\" option will be reset to false.", QMessageBox::Ok);
        checkBox_29->setChecked(false);
        saveOptions();
    }

    if((showTrayTips()) && (!QSystemTrayIcon::supportsMessages()))
    {
        QMessageBox::warning(NULL, "HQEd", "This operating system does not support system tray messages.\n\"Show tips\" option will be reset to false.", QMessageBox::Ok);
        checkBox_30->setChecked(false);
        saveOptions();
    }

    // Pokazanie/ukrycie ikony w zasobniku systemowym
    if(QSystemTrayIcon::isSystemTrayAvailable())
        MainWin->trayIcon->setVisible(showSystemTrayIcon());

    // Paski przewijania
    MainWin->xtt_graphicsView->setHorizontalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->xtt_graphicsView->setVerticalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->xtt_state_graphicsView->setHorizontalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->xtt_state_graphicsView->setVerticalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->ard_modelView->setHorizontalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->ard_modelView->setVerticalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->ard_tphView->setHorizontalScrollBarPolicy(sceneScorllBarPolicy());
    MainWin->ard_tphView->setVerticalScrollBarPolicy(sceneScorllBarPolicy());

    // Pelna weryfikacja modelu
    xtt::fullVerification();
}
// -----------------------------------------------------------------------------

// #brief Save settings in application.
//
// #return No return value.
void Settings_ui::saveOptions(void)
{
    // Application options
    conf->beginGroup("Application_options");
    conf->setValue("useStartupDialog", checkBox_54->isChecked());
    conf->setValue("showSystemTrayIcon", checkBox_29->isChecked());
    conf->setValue("showSystemTrayTips", checkBox_30->isChecked());
    conf->setValue("scrollBarsPolicy", comboBox_2->currentIndex());
    conf->setValue("xmlAutoformating", checkBox_33->isChecked());
    conf->endGroup();

    conf->beginGroup("Server_options");
    conf->setValue("listeningPort", spinBox_10->value());
    conf->setValue("autostart", checkBox_53->isChecked());
    conf->endGroup();

    conf->beginGroup("Directories");
    conf->setValue("ecmascriptsPath", label_51->text());
    conf->setValue("defaultProjectPath", label_53->text());
    conf->setValue("configurationPath", label_54->text());
    conf->endGroup();

    // ARD Apperance
    conf->beginGroup("Colors");
    conf->setValue("TPHnodeColor", toolButton_31->palette().color(QPalette::Button).name());
    conf->setValue("TPHselectedNodeColor", toolButton_21->palette().color(QPalette::Button).name());
    conf->setValue("TPHnotTransformatedNodeColor", toolButton_22->palette().color(QPalette::Button).name());
    conf->setValue("TPHfontColor", toolButton_23->palette().color(QPalette::Button).name());
    conf->setValue("TPHbranchColor", toolButton_24->palette().color(QPalette::Button).name());
    conf->setValue("TPHsceneColor", toolButton_25->palette().color(QPalette::Button).name());
    conf->setValue("ARDpropertyColor", toolButton_26->palette().color(QPalette::Button).name());
    conf->setValue("ARDselectedPropertyColor", toolButton_27->palette().color(QPalette::Button).name());
    conf->setValue("ARDnodeFontColor", toolButton_28->palette().color(QPalette::Button).name());
    conf->setValue("ARDlevelLabelFontColor", toolButton_29->palette().color(QPalette::Button).name());
    conf->setValue("ARDdependencyColor", toolButton_32->palette().color(QPalette::Button).name());
    conf->setValue("ARDsceneColor", toolButton_30->palette().color(QPalette::Button).name());
    conf->setValue("ARDmarginsColor", toolButton_20->palette().color(QPalette::Button).name());
    conf->setValue("ARDselectedDependencyColor", toolButton_33->palette().color(QPalette::Button).name());

    conf->setValue("TPHnodeAplphaChannel", horizontalSlider_22->value());
    conf->setValue("TPHselectedNodeAplphaChannel", horizontalSlider_23->value());
    conf->setValue("TPHnotTransformatedNodeAplphaChannel", horizontalSlider_24->value());
    conf->setValue("TPHfontAplphaChannel", horizontalSlider_25->value());
    conf->setValue("TPHbranchAplphaChannel", horizontalSlider_26->value());
    conf->setValue("TPHsceneAplphaChannel", horizontalSlider_27->value());
    conf->setValue("ARDpropertyAplphaChannel", horizontalSlider_28->value());
    conf->setValue("ARDselectedPropertyAplphaChannel", horizontalSlider_29->value());
    conf->setValue("ARDnodeFontAplphaChannel", horizontalSlider_21->value());
    conf->setValue("ARDlevelLabelFontAplphaChannel", horizontalSlider_20->value());
    conf->setValue("ARDdependencyAplphaChannel", horizontalSlider_19->value());
    conf->setValue("ARDsceneAplphaChannel", horizontalSlider_18->value());
    conf->setValue("ARDmarginsAplphaChannel", horizontalSlider_17->value());
    conf->setValue("ARDselectedDependencyAplphaChannel", horizontalSlider_31->value());
    conf->setValue("ardBlackWhiteColorMode", checkBox_25->isChecked());

    // XTT Apperance
    conf->setValue("TableColor", toolButton->palette().color(QPalette::Button).name());
    conf->setValue("TableColor2", toolButton_42->palette().color(QPalette::Button).name());
    conf->setValue("SelectedTableColor", toolButton_2->palette().color(QPalette::Button).name());
    conf->setValue("SelectedTableColor2", toolButton_43->palette().color(QPalette::Button).name());
    conf->setValue("TableHeaderColor", toolButton_44->palette().color(QPalette::Button).name());
    conf->setValue("TableHeaderFontColor", toolButton_3->palette().color(QPalette::Button).name());
    conf->setValue("TableBorderColor", toolButton_4->palette().color(QPalette::Button).name());
    conf->setValue("TableFontColor", toolButton_5->palette().color(QPalette::Button).name());
    conf->setValue("TableTitleFontColor", toolButton_6->palette().color(QPalette::Button).name());
    conf->setValue("HoveredCellColor", toolButton_7->palette().color(QPalette::Button).name());
    conf->setValue("CellErrorColor", toolButton_8->palette().color(QPalette::Button).name());
    conf->setValue("ConnectionColor", toolButton_9->palette().color(QPalette::Button).name());
    conf->setValue("SelectedConnectionColor", toolButton_10->palette().color(QPalette::Button).name());
    conf->setValue("CutConnectionColor", toolButton_11->palette().color(QPalette::Button).name());
    conf->setValue("ConnectionLabelFontColor", toolButton_12->palette().color(QPalette::Button).name());
    conf->setValue("SceneBackgroundColor", toolButton_13->palette().color(QPalette::Button).name());

    conf->setValue("TableAplphaChannel", horizontalSlider->value());
    conf->setValue("TableAplphaChannel2", horizontalSlider_32->value());
    conf->setValue("SelectedTableAplphaChannel", horizontalSlider_2->value());
    conf->setValue("SelectedTableAplphaChannel2", horizontalSlider_33->value());
    conf->setValue("TableHeaderAplphaChannel", horizontalSlider_34->value());
    conf->setValue("TableHeaderFontAplphaChannel", horizontalSlider_3->value());
    conf->setValue("TableBorderAplphaChannel", horizontalSlider_4->value());
    conf->setValue("TableFontAplphaChannel", horizontalSlider_5->value());
    conf->setValue("TableTitleFontAplphaChannel", horizontalSlider_6->value());
    conf->setValue("HoveredCellAplphaChannel", horizontalSlider_7->value());
    conf->setValue("CellErrorAplphaChannel", horizontalSlider_8->value());
    conf->setValue("ConnectionAplphaChannel", horizontalSlider_9->value());
    conf->setValue("SelectedConnectionAplphaChannel", horizontalSlider_10->value());
    conf->setValue("CutConnectionAplphaChannel", horizontalSlider_11->value());
    conf->setValue("ConnectionLabelFontAplphaChannel", horizontalSlider_12->value());
    conf->setValue("SceneBackgroundAplphaChannel", horizontalSlider_13->value());
    conf->setValue("xttBlackWhiteColorMode", checkBox->isChecked());
    conf->endGroup();

    conf->beginGroup("ARD_and_TPH_nodes");
    conf->setValue("useFramedNodes", checkBox_27->isChecked());
    conf->endGroup();


    conf->beginGroup("Zoom_options");
    conf->setValue("ardZoomFactor", doubleSpinBox_2->value());
    conf->setValue("ardUseMouseWheel", checkBox_26->isChecked());
    conf->setValue("xttZoomFactor", doubleSpinBox->value());
    conf->setValue("xttUseMouseWheel", checkBox_8->isChecked());
    conf->endGroup();

    conf->beginGroup("Render_hints");
    conf->setValue("ardUseAntialiasing", checkBox_24->isChecked());
    conf->setValue("ardUseTextAntialiasing", checkBox_22->isChecked());
    conf->setValue("ardUseSmoothPixmapTransform", checkBox_23->isChecked());
    conf->setValue("xttUseAntialiasing", checkBox_2->isChecked());
    conf->setValue("xttUseTextAntialiasing", checkBox_6->isChecked());
    conf->setValue("xttUseSmoothPixmapTransform", checkBox_7->isChecked());
    conf->endGroup();

    // ARD
    conf->beginGroup("Fonts");
    conf->setValue("ardTPHfont", toolButton_18->font().toString());
    conf->setValue("ardARDdiagramFont", toolButton_19->font().toString());
    conf->endGroup();

    conf->beginGroup("ARD_scene");
    conf->setValue("ardImageCompression", horizontalSlider_16->value());
    conf->setValue("ardNodesSelectingPolicy", comboBox_4->currentIndex());
    conf->endGroup();

    conf->beginGroup("ARD_others");
    conf->setValue("ardMoveElementsDistance", spinBox_7->value());
    conf->setValue("ardEnabledAutocentering", checkBox_34->isChecked());
    conf->endGroup();

    conf->beginGroup("ARD_dock_widgets_visibility");
    conf->setValue("tphDiagram", checkBox_50->isChecked());
    conf->setValue("ardLocalizator", checkBox_48->isChecked());
    conf->setValue("tphLocalizator", checkBox_49->isChecked());
    conf->endGroup();

    // XTT
    conf->beginGroup("Fonts");
    conf->setValue("xttTableFont", toolButton_14->font().toString());
    conf->setValue("xttTableHeaderFont", toolButton_15->font().toString());
    conf->setValue("xttTableTitleFont", toolButton_16->font().toString());
    conf->setValue("xttConnectionLabelFont", toolButton_17->font().toString());
    conf->endGroup();

    conf->beginGroup("XTT_Table_default_dimesions");
    conf->setValue("xttAutoCellSize", checkBox_35->isChecked());
    conf->setValue("xttColWidth", spinBox->value());
    conf->setValue("xttRowHeight", spinBox_2->value());
    conf->setValue("xttHeaderHeight", spinBox_3->value());
    conf->endGroup();

    conf->beginGroup("XTT_Tables");
    conf->setValue("xttAutoHide", checkBox_46->isChecked());
    conf->setValue("xttHideInitialTables", checkBox_47->isChecked());
    conf->setValue("xttDefaultTableEditionMode", comboBox_5->currentIndex());
    conf->endGroup();

    conf->beginGroup("XTT_auto_completion");
    conf->setValue("xttAutoAttributeAcronym", checkBox_10->isChecked());
    conf->setValue("xttAutoTableName", checkBox_9->isChecked());
    conf->setValue("xttInitialTableDefaultName", lineEdit_3->text());
    conf->setValue("xttTableBaseName", lineEdit_2->text());
    conf->endGroup();

    conf->beginGroup("XTT_scene");
    conf->setValue("xttImageCompression", horizontalSlider_15->value());
    conf->setValue("xttTablesSelectingPolicy", comboBox_3->currentIndex());
    conf->endGroup();

    conf->beginGroup("XTT_others");
    conf->setValue("xttAttributeSpaceChar", lineEdit->text());
    conf->setValue("xttMoveElementsDistance", spinBox_5->value());
    conf->setValue("xttConnectionsDistance", spinBox_6->value());
    conf->setValue("xttEnabledAutocentering", checkBox_31->isChecked());
    conf->setValue("xttEnabledAutoSceneResize", checkBox_32->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_dock_widgets_visibility");
    conf->setValue("finder", checkBox_36->isChecked());
    conf->setValue("error", checkBox_40->isChecked());
    conf->setValue("execute", checkBox_41->isChecked());
    conf->setValue("localizator", checkBox_44->isChecked());
    conf->setValue("plugineventlistener", checkBox_45->isChecked());
    conf->setValue("states", checkBox_52->isChecked());
    conf->endGroup();

    // XTT feature
    conf->beginGroup("XTT_errors_signalization");
    conf->setValue("xttErrorMessage", checkBox_3->isChecked());
    conf->setValue("xttErrorHighlightCell", checkBox_4->isChecked());
    conf->setValue("xttErrorCellTip", checkBox_5->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_cell_verification");
    conf->setValue("xttCheckAtomicValues", checkBox_12->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_attribute_to_attribute_comparsion");
    conf->setValue("enabled", checkBox_11->isChecked());
    conf->setValue("executeCellWhenOff", checkBox_13->isChecked());
    conf->setValue("executeCellWhenOn", checkBox_14->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_domain");
    conf->setValue("largeDomainSize", spinBox_8->value());
    conf->setValue("defaultAccuracy", spinBox_9->value());
    conf->endGroup();

    conf->beginGroup("XTT_value_presentation");
    conf->setValue("neutralZerosBefore", checkBox_42->isChecked());
    conf->setValue("neutralZerosAfter", checkBox_43->isChecked());
    conf->setValue("acronymReferences", checkBox_51->isChecked());
    conf->endGroup();

    // Executing
    conf->beginGroup("XTT_execute_preparing");
    conf->setValue("autoTableTypeRecognition", checkBox_15->isChecked());
    conf->setValue("warningMessageAboutAmbiguousTableType", checkBox_16->isChecked());
    conf->setValue("warningMessageAboutInaccesibleRow", checkBox_17->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_execute_running");
    conf->setValue("createMapMovement", checkBox_18->isChecked());
    conf->setValue("highlightCurrentCell", checkBox_19->isChecked());
    conf->setValue("showRunCellStatus", checkBox_21->isChecked());
    conf->setValue("finishBell", checkBox_20->isChecked());
    conf->endGroup();

    conf->beginGroup("XTT_generating");
    conf->setValue("createInitialTables", checkBox_37->isChecked());
    conf->setValue("allowDuplicateConnections", checkBox_38->isChecked());
    conf->setValue("xttOptimalization", checkBox_39->isChecked());
    conf->endGroup();
}
// -----------------------------------------------------------------------------

// #brief Load settings in application.
//
// #return No return value.
void Settings_ui::loadOptions(void)
{
    // Application options
    checkBox_54->setChecked(conf->value("Application_options/useStartupDialog", true).toBool());
    checkBox_29->setChecked(conf->value("Application_options/showSystemTrayIcon", true).toBool());
    checkBox_30->setChecked(conf->value("Application_options/showSystemTrayTips", true).toBool());
    comboBox_2->setCurrentIndex(conf->value("Application_options/scrollBarsPolicy", 0).toInt());
    checkBox_33->setChecked(conf->value("Application_options/xmlAutoformating", true).toBool());

    label_51->setText(conf->value("Directories/ecmascriptsPath", "%APPDIR%/ecmas").toString());
    label_53->setText(conf->value("Directories/defaultProjectPath", "%APPDIR%/examples").toString());
    label_54->setText(conf->value("Directories/configurationPath", "%APPDIR%/conf").toString());

    spinBox_10->setValue(conf->value("Server_options/listeningPort", 8082).toInt());
    checkBox_53->setChecked(conf->value("Server_options/autostart", false).toBool());

    // Apperance ARD
    checkBox_24->setChecked(conf->value("Render_hints/ardUseAntialiasing", true).toBool());
    checkBox_22->setChecked(conf->value("Render_hints/ardUseTextAntialiasing", true).toBool());
    checkBox_23->setChecked(conf->value("Render_hints/ardUseSmoothPixmapTransform", true).toBool());
    toolButton_31->setPalette(QPalette(QColor(conf->value("Colors/TPHnodeColor", "#55aa7f").toString())));
    toolButton_21->setPalette(QPalette(QColor(conf->value("Colors/TPHselectedNodeColor", "#aaaaff").toString())));
    toolButton_22->setPalette(QPalette(QColor(conf->value("Colors/TPHnotTransformatedNodeColor", "#e0e26e").toString())));
    toolButton_23->setPalette(QPalette(QColor(conf->value("Colors/TPHfontColor", "#000000").toString())));
    toolButton_24->setPalette(QPalette(QColor(conf->value("Colors/TPHbranchColor", "#aa0000").toString())));
    toolButton_25->setPalette(QPalette(QColor(conf->value("Colors/TPHsceneColor", "#000000").toString())));
    toolButton_26->setPalette(QPalette(QColor(conf->value("Colors/ARDpropertyColor", "#55aa7f").toString())));
    toolButton_27->setPalette(QPalette(QColor(conf->value("Colors/ARDselectedPropertyColor", "#aaaaff").toString())));
    toolButton_28->setPalette(QPalette(QColor(conf->value("Colors/ARDnodeFontColor", "#000000").toString())));
    toolButton_29->setPalette(QPalette(QColor(conf->value("Colors/ARDlevelLabelFontColor", "#5500ff").toString())));
    toolButton_32->setPalette(QPalette(QColor(conf->value("Colors/ARDdependencyColor", "#ffaa00").toString())));
    toolButton_30->setPalette(QPalette(QColor(conf->value("Colors/ARDsceneColor", "#000000").toString())));
    toolButton_20->setPalette(QPalette(QColor(conf->value("Colors/ARDmarginsColor", "#0000ff").toString())));
    toolButton_33->setPalette(QPalette(QColor(conf->value("Colors/ARDselectedDependencyColor", "#0000ff").toString())));
    horizontalSlider_22->setValue(conf->value("Colors/TPHnodeAplphaChannel", 255-150).toInt());
    horizontalSlider_23->setValue(conf->value("Colors/TPHselectedNodeAplphaChannel", 255-150).toInt());
    horizontalSlider_24->setValue(conf->value("Colors/TPHnotTransformatedNodeAplphaChannel", 0).toInt());
    horizontalSlider_25->setValue(conf->value("Colors/TPHfontAplphaChannel", 0).toInt());
    horizontalSlider_26->setValue(conf->value("Colors/TPHbranchAplphaChannel", 0).toInt());
    horizontalSlider_27->setValue(conf->value("Colors/TPHsceneAplphaChannel", 0).toInt());
    horizontalSlider_28->setValue(conf->value("Colors/ARDpropertyAplphaChannel", 0).toInt());
    horizontalSlider_29->setValue(conf->value("Colors/ARDselectedPropertyAplphaChannel", 0).toInt());
    horizontalSlider_21->setValue(conf->value("Colors/ARDnodeFontAplphaChannel", 0).toInt());
    horizontalSlider_19->setValue(conf->value("Colors/ARDlevelLabelFontAplphaChannel", 255-125).toInt());
    horizontalSlider_18->setValue(conf->value("Colors/ARDdependencyAplphaChannel", 255-125).toInt());
    horizontalSlider_17->setValue(conf->value("Colors/ARDsceneAplphaChannel", 0).toInt());
    horizontalSlider_25->setValue(conf->value("Colors/ARDmarginsAplphaChannel", 0).toInt());
    horizontalSlider_31->setValue(conf->value("Colors/ARDselectedDependencyAplphaChannel", 0).toInt());
    checkBox_25->setChecked(conf->value("Colors/ardBlackWhiteColorMode", false).toBool());
    doubleSpinBox_2->setValue(conf->value("Zoom_options/ardZoomFactor", 5.0).toDouble());
    checkBox_26->setChecked(conf->value("Zoom_options/ardUseMouseWheel", true).toBool());
    checkBox_27->setChecked(conf->value("ARD_and_TPH_nodes/useFramedNodes", true).toBool());

    // Apperance XTT
    checkBox_2->setChecked(conf->value("Render_hints/xttUseAntialiasing", true).toBool());
    checkBox_6->setChecked(conf->value("Render_hints/xttUseTextAntialiasing", true).toBool());
    checkBox_7->setChecked(conf->value("Render_hints/xttUseSmoothPixmapTransform", true).toBool());
    toolButton->setPalette(QPalette(QColor(conf->value("Colors/TableColor", "#b4b4b4").toString())));
    toolButton_42->setPalette(QPalette(QColor(conf->value("Colors/TableColor2","#a5a5a5").toString())));
    toolButton_2->setPalette(QPalette(QColor(conf->value("Colors/SelectedTableColor", "#00c8ff").toString())));
    toolButton_43->setPalette(QPalette(QColor(conf->value("Colors/SelectedTableColor2", "#00c855").toString())));
    toolButton_44->setPalette(QPalette(QColor(conf->value("Colors/TableHeaderColor", "#00c8ff").toString())));
    toolButton_3->setPalette(QPalette(QColor(conf->value("Colors/TableHeaderFontColor", "#000000").toString())));
    toolButton_4->setPalette(QPalette(QColor(conf->value("Colors/TableBorderColor", "#0000ff").toString())));
    toolButton_5->setPalette(QPalette(QColor(conf->value("Colors/TableFontColor", "#ff0000").toString())));
    toolButton_6->setPalette(QPalette(QColor(conf->value("Colors/TableTitleFontColor", "#000000").toString())));
    toolButton_7->setPalette(QPalette(QColor(conf->value("Colors/HoveredCellColor", "#000000").toString())));
    toolButton_8->setPalette(QPalette(QColor(conf->value("Colors/CellErrorColor", "#000000").toString())));
    toolButton_9->setPalette(QPalette(QColor(conf->value("Colors/ConnectionColor", "#ffffff").toString())));
    toolButton_10->setPalette(QPalette(QColor(conf->value("Colors/SelectedConnectionColor", "#ffff00").toString())));
    toolButton_11->setPalette(QPalette(QColor(conf->value("Colors/CutConnectionColor", "#ff0000").toString())));
    toolButton_12->setPalette(QPalette(QColor(conf->value("Colors/ConnectionLabelFontColor", "#000000").toString())));
    toolButton_13->setPalette(QPalette(QColor(conf->value("Colors/SceneBackgroundColor", "#000000").toString())));
    horizontalSlider->setValue(conf->value("Colors/TableAplphaChannel", 255-150).toInt());
    horizontalSlider_32->setValue(conf->value("Colors/TableAplphaChannel2", 255-150).toInt());
    horizontalSlider_2->setValue(conf->value("Colors/SelectedTableAplphaChannel", 255-150).toInt());
    horizontalSlider_33->setValue(conf->value("Colors/SelectedTableAplphaChannel2", 255-150).toInt());
    horizontalSlider_3->setValue(conf->value("Colors/TableHeaderFontAplphaChannel", 0).toInt());
    horizontalSlider_4->setValue(conf->value("Colors/TableBorderAplphaChannel", 0).toInt());
    horizontalSlider_5->setValue(conf->value("Colors/TableFontAplphaChannel", 0).toInt());
    horizontalSlider_6->setValue(conf->value("Colors/TableTitleFontAplphaChannel", 0).toInt());
    horizontalSlider_7->setValue(conf->value("Colors/HoveredCellAplphaChannel", 0).toInt());
    horizontalSlider_8->setValue(conf->value("Colors/CellErrorAplphaChannel", 0).toInt());
    horizontalSlider_9->setValue(conf->value("Colors/ConnectionAplphaChannel", 0).toInt());
    horizontalSlider_10->setValue(conf->value("Colors/SelectedConnectionAplphaChannel", 255-125).toInt());
    horizontalSlider_11->setValue(conf->value("Colors/CutConnectionAplphaChannel", 255-125).toInt());
    horizontalSlider_12->setValue(conf->value("Colors/ConnectionLabelFontAplphaChannel", 0).toInt());
    horizontalSlider_13->setValue(conf->value("Colors/SceneBackgroundAplphaChannel", 0).toInt());
    checkBox->setChecked(conf->value("Colors/xttBlackWhiteColorMode", false).toBool());
    doubleSpinBox->setValue(conf->value("Zoom_options/xttZoomFactor", 5.0).toDouble());
    checkBox_8->setChecked(conf->value("Zoom_options/xttUseMouseWheel", true).toBool());

    // ARD
    QFont currFont;
    currFont.fromString(conf->value("Fonts/ardTPHfont", "MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0").toString());
    toolButton_18->setFont(currFont);
    currFont.fromString(conf->value("Fonts/ardARDdiagramFont", "MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0").toString());
    toolButton_19->setFont(currFont);
    horizontalSlider_16->setValue(conf->value("ARD_scene/ardImageCompression", 0).toInt());
    comboBox_4->setCurrentIndex(conf->value("ARD_scene/ardNodesSelectingPolicy", 2).toInt());
    spinBox_7->setValue(conf->value("ARD_others/ardMoveElementsDistance", 100).toInt());
    checkBox_34->setChecked(conf->value("ARD_others/ardEnabledAutocentering", false).toBool());
    checkBox_48->setChecked(conf->value("ARD_dock_widgets_visibility/ardLocalizator", true).toBool());
    checkBox_49->setChecked(conf->value("ARD_dock_widgets_visibility/tphLocalizator", true).toBool());
    checkBox_50->setChecked(conf->value("ARD_dock_widgets_visibility/tphDiagram", true).toBool());

    // XTT
    currFont.fromString(conf->value("Fonts/xttTableFont", "MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0").toString());
    toolButton_14->setFont(currFont);
    currFont.fromString(conf->value("Fonts/xttTableHeaderFont", "MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0").toString());
    toolButton_15->setFont(currFont);
    currFont.fromString(conf->value("Fonts/xttTableTitleFont", "MS Shell Dlg 2,8.25,-1,5,50,0,0,0,0,0").toString());
    toolButton_16->setFont(currFont);
    currFont.fromString(conf->value("Fonts/xttConnectionLabelFont", "Courier New,8,-1,5,50,0,0,0,0,0").toString());
    toolButton_17->setFont(currFont);
    checkBox_46->setChecked(conf->value("XTT_Table_default_dimesions/xttAutoHide", true).toBool());
    checkBox_35->setChecked(conf->value("XTT_Table_default_dimesions/xttAutoCellSize", true).toBool());
    spinBox->setValue(conf->value("XTT_Table_default_dimesions/xttColWidth", 120).toInt());
    spinBox_2->setValue(conf->value("XTT_Table_default_dimesions/xttRowHeight", 20).toInt());
    spinBox_3->setValue(conf->value("XTT_Table_default_dimesions/xttHeaderHeight", 20).toInt());
    checkBox_10->setChecked(conf->value("XTT_auto_completion/xttAutoAttributeAcronym", true).toBool());
    checkBox_9->setChecked(conf->value("XTT_auto_completion/xttAutoTableName", true).toBool());
    lineEdit_2->setText(conf->value("XTT_auto_completion/xttTableBaseName", "Table").toString());
    lineEdit_3->setText(conf->value("XTT_auto_completion/xttInitialTableDefaultName", "init").toString());
    horizontalSlider_15->setValue(conf->value("XTT_scene/xttImageCompression", 0).toInt());
    comboBox_3->setCurrentIndex(conf->value("XTT_scene/xttTablesSelectingPolicy", 2).toInt());
    lineEdit->setText(conf->value("XTT_others/xttAttributeSpaceChar", "_").toString());
    spinBox_5->setValue(conf->value("XTT_others/xttMoveElementsDistance", 100).toInt());
    spinBox_6->setValue(conf->value("XTT_others/xttConnectionsDistance", 5).toInt());
    checkBox_31->setChecked(conf->value("XTT_others/xttEnabledAutocentering", true).toBool());
    checkBox_32->setChecked(conf->value("XTT_others/xttEnabledAutoSceneResize", false).toBool());
    checkBox_36->setChecked(conf->value("XTT_dock_widgets_visibility/finder", false).toBool());
    checkBox_40->setChecked(conf->value("XTT_dock_widgets_visibility/error", true).toBool());
    checkBox_41->setChecked(conf->value("XTT_dock_widgets_visibility/execute", true).toBool());
    checkBox_44->setChecked(conf->value("XTT_dock_widgets_visibility/localizator", true).toBool());
    checkBox_45->setChecked(conf->value("XTT_dock_widgets_visibility/plugineventlistener", true).toBool());
    checkBox_52->setChecked(conf->value("XTT_dock_widgets_visibility/states", true).toBool());
    checkBox_46->setChecked(conf->value("XTT_Tables/xttAutoHide", true).toBool());
    checkBox_47->setChecked(conf->value("XTT_Tables/xttHideInitialTables", true).toBool());
    comboBox_5->setCurrentIndex(conf->value("XTT_Tables/xttDefaultTableEditionMode", XTT_TABLE_EDIT_MODE_FULL).toInt());

    // XTT features
    checkBox_3->setChecked(conf->value("XTT_errors_signalization/xttErrorMessage", false).toBool());
    checkBox_4->setChecked(conf->value("XTT_errors_signalization/xttErrorHighlightCell", true).toBool());
    checkBox_5->setChecked(conf->value("XTT_errors_signalization/xttErrorCellTip", false).toBool());
    checkBox_11->setChecked(conf->value("XTT_attribute_to_attribute_comparsion/enabled", true).toBool());
    checkBox_13->setChecked(conf->value("XTT_attribute_to_attribute_comparsion/executeCellWhenOff", true).toBool());
    checkBox_14->setChecked(conf->value("XTT_attribute_to_attribute_comparsion/executeCellWhenOn", true).toBool());
    checkBox_12->setChecked(conf->value("XTT_cell_verification/xttCheckAtomicValues", true).toBool());
    checkBox_42->setChecked(conf->value("XTT_value_presentation/neutralZerosBefore", false).toBool());
    checkBox_43->setChecked(conf->value("XTT_value_presentation/neutralZerosAfter", false).toBool());
    checkBox_51->setChecked(conf->value("XTT_value_presentation/acronymReferences", false).toBool());
    spinBox_8->setValue(conf->value("XTT_domain/largeDomainSize", 30).toInt());
    spinBox_9->setValue(conf->value("XTT_domain/defaultAccuracy", 10).toInt());

    // Ececuting
    checkBox_15->setChecked(conf->value("XTT_execute_preparing/autoTableTypeRecognition", true).toBool());
    checkBox_16->setChecked(conf->value("XTT_execute_preparing/warningMessageAboutAmbiguousTableType", true).toBool());
    checkBox_17->setChecked(conf->value("XTT_execute_preparing/warningMessageAboutInaccesibleRow", true).toBool());
    checkBox_18->setChecked(conf->value("XTT_execute_running/createMapMovement", true).toBool());
    checkBox_19->setChecked(conf->value("XTT_execute_running/highlightCurrentCell", false).toBool());
    checkBox_20->setChecked(conf->value("XTT_execute_running/finishBell", true).toBool());
    checkBox_21->setChecked(conf->value("XTT_execute_running/showRunCellStatus", false).toBool());

    checkBox_37->setChecked(conf->value("XTT_generating/createInitialTables", true).toBool());
    checkBox_38->setChecked(conf->value("XTT_generating/allowDuplicateConnections", false).toBool());
    checkBox_39->setChecked(conf->value("XTT_generating/xttOptimalization", true).toBool());

    // Wywolanie dodoatkowych funckji ustawiajacych okienko
    checkBox9changed(checkBox_9->isChecked());
    checkBox11changed(checkBox_11->isChecked());
    checkBox15changed(checkBox_15->isChecked());
    onCheckBox29checked(checkBox_29->isChecked());
    onCheckBox35checked(checkBox_35->isChecked());

    onSlider1ChangePosition(horizontalSlider->value());
    onSlider2ChangePosition(horizontalSlider_2->value());
    onSlider3ChangePosition(horizontalSlider_3->value());
    onSlider4ChangePosition(horizontalSlider_4->value());
    onSlider5ChangePosition(horizontalSlider_5->value());
    onSlider6ChangePosition(horizontalSlider_6->value());
    onSlider7ChangePosition(horizontalSlider_7->value());
    onSlider8ChangePosition(horizontalSlider_8->value());
    onSlider9ChangePosition(horizontalSlider_9->value());
    onSlider10ChangePosition(horizontalSlider_10->value());
    onSlider11ChangePosition(horizontalSlider_11->value());
    onSlider12ChangePosition(horizontalSlider_12->value());
    onSlider13ChangePosition(horizontalSlider_13->value());
    onSlider15ChangePosition(horizontalSlider_15->value());
    onSlider16ChangePosition(horizontalSlider_16->value());
    onSlider17ChangePosition(horizontalSlider_17->value());
    onSlider18ChangePosition(horizontalSlider_18->value());
    onSlider19ChangePosition(horizontalSlider_19->value());
    onSlider20ChangePosition(horizontalSlider_20->value());
    onSlider21ChangePosition(horizontalSlider_21->value());
    onSlider22ChangePosition(horizontalSlider_22->value());
    onSlider23ChangePosition(horizontalSlider_23->value());
    onSlider24ChangePosition(horizontalSlider_24->value());
    onSlider25ChangePosition(horizontalSlider_25->value());
    onSlider26ChangePosition(horizontalSlider_26->value());
    onSlider27ChangePosition(horizontalSlider_27->value());
    onSlider28ChangePosition(horizontalSlider_28->value());
    onSlider29ChangePosition(horizontalSlider_29->value());
    onSlider31ChangePosition(horizontalSlider_31->value());

    readPlugins();
    updatePlugins();
}
// -----------------------------------------------------------------------------

// #brief Set color mode.
//
// #param _cm Set or unset graphic mode.
// #param appMode Mode of application.
// #return No return value.
void Settings_ui::setColorMode(bool _cm, int appMode) 
{	
    if(appMode == ARD_MODE)
        checkBox_25->setChecked(_cm);
    if(appMode == XTT_MODE)
        checkBox->setChecked(_cm);
}
// -----------------------------------------------------------------------------

// #brief Set atnialiasing mode.
//
// #param _nm Set or unset atnialiasing mode.
// #param appMode Mode of application.
// #return No return value.
void Settings_ui::setAntialiasingMode(bool _nm, int appMode)
{
    if(appMode == ARD_MODE)
        checkBox_24->setChecked(_nm);
    if(appMode == XTT_MODE)
        checkBox_2->setChecked(_nm);
}
// -----------------------------------------------------------------------------

// #brief Set atnialiasing mode for text.
//
// #param _nm Set or unset atnialiasing mode for text.
// #param appMode Mode of application.
// #return No return value.
void Settings_ui::setTextAntialiasingMode(bool _nm, int appMode)
{
    if(appMode == ARD_MODE)
        checkBox_22->setChecked(_nm);
    if(appMode == XTT_MODE)
        checkBox_6->setChecked(_nm);
}
// -----------------------------------------------------------------------------

// #brief Set smooth tranformation for bitmap.
//
// #param _nm Set or unset smooth tranformation for bitmap.
// #param appMode Mode of application.
// #return No return value.
void Settings_ui::setSmoothPixmapTransformMode(bool _nm, int appMode)
{
    if(appMode == ARD_MODE)
        checkBox_23->setChecked(_nm);
    if(appMode == XTT_MODE)
        checkBox_7->setChecked(_nm);
}
// -----------------------------------------------------------------------------

// #brief Call when current tab changed.
//
// #return No return value.
void Settings_ui::currentTabChanged(int currTab)
{
    listWidget->setCurrentRow(currTab);
}
// -----------------------------------------------------------------------------

// #brief Call when table font is about to changed.
//
// #return No return value.
void Settings_ui::changeTableFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_14->font(), this);
    if(ok)
        toolButton_14->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when table header font is about to changed.
//
// #return No return value.
void Settings_ui::changeTableHeaderFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_15->font(), this);
    if(ok)
        toolButton_15->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when table title font is about to changed.
//
// #return No return value.
void Settings_ui::changeTableTitleFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_16->font(), this);
    if(ok)
        toolButton_16->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when connection label font is about to changed.
//
// #return No return value.
void Settings_ui::changeConnectionLabelFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_17->font(), this);
    if(ok)
        toolButton_17->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when TPH font is about to changed.
//
// #return No return value.
void Settings_ui::changeTPHFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_18->font(), this);
    if(ok)
        toolButton_18->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when ARD diagram font is about to changed.
//
// #return No return value.
void Settings_ui::changeARDdiagramFont(void)
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok, toolButton_19->font(), this);
    if(ok)
        toolButton_19->setFont(font);
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor1(void)
{
    QColor color = QColorDialog::getColor(toolButton->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton->setPalette(QPalette(color));
    toolButton->setToolTip(toolButton->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor2(void)
{
    QColor color = QColorDialog::getColor(toolButton_2->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_2->setPalette(QPalette(color));
    toolButton_2->setToolTip(toolButton_2->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor3(void)
{
    QColor color = QColorDialog::getColor(toolButton_3->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_3->setPalette(QPalette(color));
    toolButton_3->setToolTip(toolButton_3->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor4(void)
{
    QColor color = QColorDialog::getColor(toolButton_4->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_4->setPalette(QPalette(color));
    toolButton_4->setToolTip(toolButton_4->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor5(void)
{
    QColor color = QColorDialog::getColor(toolButton_5->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_5->setPalette(QPalette(color));
    toolButton_5->setToolTip(toolButton_5->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor6(void)
{
    QColor color = QColorDialog::getColor(toolButton_6->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_6->setPalette(QPalette(color));
    toolButton_6->setToolTip(toolButton_6->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor7(void)
{
    QColor color = QColorDialog::getColor(toolButton_7->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_7->setPalette(QPalette(color));
    toolButton_7->setToolTip(toolButton_7->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor8(void)
{
    QColor color = QColorDialog::getColor(toolButton_8->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_8->setPalette(QPalette(color));
    toolButton_8->setToolTip(toolButton_8->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor9(void)
{
    QColor color = QColorDialog::getColor(toolButton_9->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_9->setPalette(QPalette(color));
    toolButton_9->setToolTip(toolButton_9->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor10(void)
{
    QColor color = QColorDialog::getColor(toolButton_10->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_10->setPalette(QPalette(color));
    toolButton_10->setToolTip(toolButton_10->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor11(void)
{
    QColor color = QColorDialog::getColor(toolButton_11->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_11->setPalette(QPalette(color));
    toolButton_11->setToolTip(toolButton_11->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor12(void)
{
    QColor color = QColorDialog::getColor(toolButton_12->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_12->setPalette(QPalette(color));
    toolButton_12->setToolTip(toolButton_12->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor13(void)
{
    QColor color = QColorDialog::getColor(toolButton_13->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_13->setPalette(QPalette(color));
    toolButton_13->setToolTip(toolButton_13->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor14(void)
{
    QColor color = QColorDialog::getColor(toolButton_31->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_31->setPalette(QPalette(color));
    toolButton_31->setToolTip(toolButton_31->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor15(void)
{
    QColor color = QColorDialog::getColor(toolButton_21->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_21->setPalette(QPalette(color));
    toolButton_21->setToolTip(toolButton_21->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor16(void)
{
    QColor color = QColorDialog::getColor(toolButton_22->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_22->setPalette(QPalette(color));
    toolButton_22->setToolTip(toolButton_22->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor17(void)
{
    QColor color = QColorDialog::getColor(toolButton_23->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_23->setPalette(QPalette(color));
    toolButton_23->setToolTip(toolButton_23->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor18(void)
{
    QColor color = QColorDialog::getColor(toolButton_24->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_24->setPalette(QPalette(color));
    toolButton_24->setToolTip(toolButton_24->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor19(void)
{
    QColor color = QColorDialog::getColor(toolButton_25->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_25->setPalette(QPalette(color));
    toolButton_25->setToolTip(toolButton_25->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor20(void)
{
    QColor color = QColorDialog::getColor(toolButton_26->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_26->setPalette(QPalette(color));
    toolButton_26->setToolTip(toolButton_26->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor21(void)
{
    QColor color = QColorDialog::getColor(toolButton_27->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_27->setPalette(QPalette(color));
    toolButton_27->setToolTip(toolButton_27->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor22(void)
{
    QColor color = QColorDialog::getColor(toolButton_28->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_28->setPalette(QPalette(color));
    toolButton_28->setToolTip(toolButton_28->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor23(void)
{
    QColor color = QColorDialog::getColor(toolButton_29->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_29->setPalette(QPalette(color));
    toolButton_29->setToolTip(toolButton_29->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor24(void)
{
    QColor color = QColorDialog::getColor(toolButton_32->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_32->setPalette(QPalette(color));
    toolButton_32->setToolTip(toolButton_32->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor25(void)
{
    QColor color = QColorDialog::getColor(toolButton_30->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_30->setPalette(QPalette(color));
    toolButton_30->setToolTip(toolButton_30->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor26(void)
{
    QColor color = QColorDialog::getColor(toolButton_20->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_20->setPalette(QPalette(color));
    toolButton_20->setToolTip(toolButton_20->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor27(void)
{
    QColor color = QColorDialog::getColor(toolButton_33->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_33->setPalette(QPalette(color));
    toolButton_33->setToolTip(toolButton_33->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor28(void)
{
    QColor color = QColorDialog::getColor(toolButton_42->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_42->setPalette(QPalette(color));
    toolButton_42->setToolTip(toolButton_42->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor29(void)
{
    QColor color = QColorDialog::getColor(toolButton_43->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_43->setPalette(QPalette(color));
    toolButton_43->setToolTip(toolButton_43->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when button of color selector is clicked.
//
// #return No return value.
void Settings_ui::onSelectColor44(void)
{
    QColor color = QColorDialog::getColor(toolButton_44->palette().color(QPalette::Button), this);
    if(color.isValid())
        toolButton_44->setPalette(QPalette(color));
    toolButton_44->setToolTip(toolButton_44->palette().color(QPalette::Button).name());
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider1ChangePosition(int _newVal)
{
    horizontalSlider->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider2ChangePosition(int _newVal)
{
    horizontalSlider_2->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider3ChangePosition(int _newVal)
{
    horizontalSlider_3->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider4ChangePosition(int _newVal)
{
    horizontalSlider_4->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider5ChangePosition(int _newVal)
{
    horizontalSlider_5->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider6ChangePosition(int _newVal)
{
    horizontalSlider_6->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider7ChangePosition(int _newVal)
{
    horizontalSlider_7->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider8ChangePosition(int _newVal)
{
    horizontalSlider_8->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider9ChangePosition(int _newVal)
{
    horizontalSlider_9->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider10ChangePosition(int _newVal)
{
    horizontalSlider_10->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider11ChangePosition(int _newVal)
{
    horizontalSlider_11->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider12ChangePosition(int _newVal)
{
    horizontalSlider_12->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #return No return value.
void Settings_ui::onSlider13ChangePosition(int _newVal)
{
    horizontalSlider_13->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of picture compression is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider15ChangePosition(int _newVal)
{
    label_24->setText("Scene screen compresion: " + QString::number(_newVal) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of picture compression is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider16ChangePosition(int _newVal)
{
    label_27->setText("Scene screen compresion: " + QString::number(_newVal) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider17ChangePosition(int _newVal)
{
    horizontalSlider_17->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider18ChangePosition(int _newVal)
{
    horizontalSlider_18->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider19ChangePosition(int _newVal)
{
    horizontalSlider_19->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider20ChangePosition(int _newVal)
{
    horizontalSlider_20->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider21ChangePosition(int _newVal)
{
    horizontalSlider_21->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider22ChangePosition(int _newVal)
{
    horizontalSlider_22->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider23ChangePosition(int _newVal)
{
    horizontalSlider_23->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider24ChangePosition(int _newVal)
{
    horizontalSlider_24->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider25ChangePosition(int _newVal)
{
    horizontalSlider_25->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider26ChangePosition(int _newVal)
{
    horizontalSlider_26->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider27ChangePosition(int _newVal)
{
    horizontalSlider_27->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider28ChangePosition(int _newVal)
{
    horizontalSlider_28->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider29ChangePosition(int _newVal)
{
    horizontalSlider_29->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider31ChangePosition(int _newVal)
{
    horizontalSlider_31->setToolTip("Transparency = " + QString::number(((float)_newVal/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider32ChangePosition(int _nv)
{
    horizontalSlider_32->setToolTip("Transparency = " + QString::number(((float)_nv/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider33ChangePosition(int _nv)
{
    horizontalSlider_33->setToolTip("Transparency = " + QString::number(((float)_nv/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when position of slider of transparency is chaged.
//
// #param _nv New slider position.
// #return No return value.
void Settings_ui::onSlider34ChangePosition(int _nv)
{
    horizontalSlider_34->setToolTip("Transparency = " + QString::number(((float)_nv/255.)*100., 'f', 2) + "%");
}
// -----------------------------------------------------------------------------

// #brief Call when status of tray icon is changed.
//
// #param _nv New status.
// #return No return value.
void Settings_ui::onCheckBox29checked(bool _newState)
{
    checkBox_30->setEnabled(_newState);
    if(!_newState)
        checkBox_30->setChecked(false);
}
// -----------------------------------------------------------------------------

// #brief Call when status of autosize of cell is changed.
//
// #param _nv New status.
// #return No return value.
void Settings_ui::onCheckBox35checked(bool _newState)
{
    spinBox->setEnabled(!_newState);
    spinBox_2->setEnabled(!_newState);
    spinBox_3->setEnabled(!_newState);
}
// -----------------------------------------------------------------------------

// #brief Call when fuctor zoom is changed in ARD mode.
//
// #param _nv New value of fuctor zoom.
// #return No return value.
void Settings_ui::changeArdZoomFactorSpinBox(double _nv)
{
    int sliderValue = (int)(_nv * 100.0);
    horizontalSlider_30->setValue(sliderValue);
}
// -----------------------------------------------------------------------------

// #brief Call when fuctor zoom is changed in ARD mode.
//
// #param _nv New value of fuctor zoom.
// #return No return value.
void Settings_ui::changeArdZoomFactorSliderBar(int _newVal)
{
    double spinBoxValue = (double)_newVal/100.0;
    doubleSpinBox_2->setValue(spinBoxValue);
}
// -----------------------------------------------------------------------------

// #brief Call when fuctor zoom is changed in XTT mode.
//
// #param _nv New value of fuctor zoom.
// #return No return value.
void Settings_ui::changeXttZoomFactorSpinBox(double _nv)
{
    int sliderValue = (int)(_nv * 100.0);
    horizontalSlider_14->setValue(sliderValue);
}
// -----------------------------------------------------------------------------

// #brief Call when fuctor zoom is changed in XTT mode.
//
// #param _nv New value of fuctor zoom.
// #return No return value.
void Settings_ui::changeXttZoomFactorSliderBar(int _newVal)
{
    double spinBoxValue = (double)_newVal/100.0;
    doubleSpinBox->setValue(spinBoxValue);
}
// -----------------------------------------------------------------------------

// #brief Call when checkbox state is changed.
//
// #param _nv New state of checkbox.
// #return No return value.
void Settings_ui::checkBox9changed(bool _nv)
{
    label_23->setEnabled(_nv);
    lineEdit_2->setEnabled(_nv);
}
// -----------------------------------------------------------------------------

// #brief Call when attribute comparing status is changed.
//
// #param _nv New state of checkbox.
// #return No return value.
void Settings_ui::checkBox11changed(bool _nv)
{
    checkBox_13->setEnabled(!_nv);
    checkBox_14->setEnabled(_nv);
}
// -----------------------------------------------------------------------------

// #brief Call when auto detection of table type is changed.
//
// #param _nv New state of checkbox.
// #return No return value.
void Settings_ui::checkBox15changed(bool _nv)
{
    checkBox_16->setEnabled(_nv);
}
// -----------------------------------------------------------------------------

// #brief Call when replacing space character is changed.
//
// #param _newVal New value of string replacing space character.
// #return No return value.
void Settings_ui::spaceCharacterChange(QString _newVal)
{
    // Usuwanie wszystkich spacji
    int spaceIndex = _newVal.indexOf(" ");
    while(spaceIndex > -1)
    {
        _newVal = _newVal.remove(spaceIndex, 1);
        spaceIndex = _newVal.indexOf(" ");
    }
    int cp = lineEdit->cursorPosition();
    lineEdit->setText(_newVal);
    lineEdit->setCursorPosition(cp);
}
// -----------------------------------------------------------------------------

// #brief Get zoom value for the scene.
//
// #param appMode Mode of application.
// #return Zoom value.
double Settings_ui::zoomFactorValue(int appMode)
{	
    double res = 1.0;
    if(appMode == ARD_MODE)
        res = doubleSpinBox_2->value();
    if(appMode == XTT_MODE)
        res = doubleSpinBox->value();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get decision about using zoom mouse wheel.
//
// #param appMode Mode of application.
// #return Decision about using zoom mouse whee.
// #li True - use zoom mouse whee.
// #li False - don't use zoom mouse whee.
bool Settings_ui::useZoomMouseWhell(int appMode)
{	
    bool res = false;
    if(appMode == ARD_MODE)
        res = checkBox_26->isChecked();
    if(appMode == XTT_MODE)
        res = checkBox_8->isChecked();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that changes the status of scene scrolling/zooming by wheel
//
// #return No return value.
void Settings_ui::changeXttScrollByWheelStatus(void)
{
    checkBox_8->setChecked(!checkBox_8->isChecked());
    saveOptions();
}
// -----------------------------------------------------------------------------

// #brief Get information when scrollbars should be visible.
//
// #return Scroll bar policy.
Qt::ScrollBarPolicy Settings_ui::sceneScorllBarPolicy(void)
{
    Qt::ScrollBarPolicy policies[3] = {Qt::ScrollBarAsNeeded, Qt::ScrollBarAlwaysOff, Qt::ScrollBarAlwaysOn};
    return policies[comboBox_2->currentIndex()];
}
// -----------------------------------------------------------------------------

// #brief Get color of ordinary TPH node.
//
// #return Color of ordinary TPH node.
QColor Settings_ui::ardTPHnodeColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_31->palette().color(QPalette::Button).red(), toolButton_31->palette().color(QPalette::Button).green(), toolButton_31->palette().color(QPalette::Button).blue(), 255-horizontalSlider_22->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of selected TPH node.
//
// #return Color of selected TPH node.
QColor Settings_ui::ardTPHselectedNodeColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_21->palette().color(QPalette::Button).red(), toolButton_21->palette().color(QPalette::Button).green(), toolButton_21->palette().color(QPalette::Button).blue(), 255-horizontalSlider_23->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of not tranformed TPH node.
//
// #return Color of not transformed TPH node.
QColor Settings_ui::ardTPHnotTransformatedNodeColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_22->palette().color(QPalette::Button).red(), toolButton_22->palette().color(QPalette::Button).green(), toolButton_22->palette().color(QPalette::Button).blue(), 255-horizontalSlider_24->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get font color of TPH node.
//
// #return Font color of TPH node.
QColor Settings_ui::ardTPHfontColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_23->palette().color(QPalette::Button).red(), toolButton_23->palette().color(QPalette::Button).green(), toolButton_23->palette().color(QPalette::Button).blue(), 255-horizontalSlider_25->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get branch color of TPH node.
//
// #return Branch color of TPH node.
QColor Settings_ui::ardTPHbranchColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_24->palette().color(QPalette::Button).red(), toolButton_24->palette().color(QPalette::Button).green(), toolButton_24->palette().color(QPalette::Button).blue(), 255-horizontalSlider_26->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get scene color of TPH.
//
// #return Scene color of TPH.
QColor Settings_ui::ardTPHsceneColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_25->palette().color(QPalette::Button).red(), toolButton_25->palette().color(QPalette::Button).green(), toolButton_25->palette().color(QPalette::Button).blue(), 255-horizontalSlider_27->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of ARD properties.
//
// #return Color of ARD properties.
QColor Settings_ui::ardPropertyColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_26->palette().color(QPalette::Button).red(), toolButton_26->palette().color(QPalette::Button).green(), toolButton_26->palette().color(QPalette::Button).blue(), 255-horizontalSlider_28->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of selected ARD properties.
//
// #return Color of selected ARD properties.
QColor Settings_ui::ardSelectedPropertyColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_27->palette().color(QPalette::Button).red(), toolButton_27->palette().color(QPalette::Button).green(), toolButton_27->palette().color(QPalette::Button).blue(), 255-horizontalSlider_29->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get font color of ARD properties.
//
// #return Font color of ARD properties.
QColor Settings_ui::ardPropertyFontColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_28->palette().color(QPalette::Button).red(), toolButton_28->palette().color(QPalette::Button).green(), toolButton_28->palette().color(QPalette::Button).blue(), 255-horizontalSlider_21->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get font color of ARD level label.
//
// #return Font color of ARD level label.
QColor Settings_ui::ardLevelLabelFontColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_29->palette().color(QPalette::Button).red(), toolButton_29->palette().color(QPalette::Button).green(), toolButton_29->palette().color(QPalette::Button).blue(), 255-horizontalSlider_20->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get line color of ARD dependency.
//
// #return Line color of ARD dependency.
QColor Settings_ui::ardDependencyColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_32->palette().color(QPalette::Button).red(), toolButton_32->palette().color(QPalette::Button).green(), toolButton_32->palette().color(QPalette::Button).blue(), 255-horizontalSlider_19->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get active line color of ARD dependency.
//
// #return Active line color of ARD dependency.
QColor Settings_ui::ardSelectedDependencyColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_33->palette().color(QPalette::Button).red(), toolButton_33->palette().color(QPalette::Button).green(), toolButton_33->palette().color(QPalette::Button).blue(), 255-horizontalSlider_31->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of ARD scene.
//
// #return Color of ARD scene.
QColor Settings_ui::ardSceneColor(void)
{
    QColor result = Qt::white;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_30->palette().color(QPalette::Button).red(), toolButton_30->palette().color(QPalette::Button).green(), toolButton_30->palette().color(QPalette::Button).blue(), 255-horizontalSlider_18->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of ARD margin.
//
// #return Color of ARD margin.
QColor Settings_ui::ardMarginsColor(void)
{
    QColor result = Qt::black;
    if(!checkBox_25->isChecked())
        result = QColor(toolButton_20->palette().color(QPalette::Button).red(), toolButton_20->palette().color(QPalette::Button).green(), toolButton_20->palette().color(QPalette::Button).blue(), 255-horizontalSlider_17->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Returns if the TPH diagram should be displayed
//
// #return true if the TPH diagram should be displayed, otherwise false
bool Settings_ui::ardTPHdiagramDockWidget(void)
{
    return checkBox_50->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the ARD localizator should be displayed
//
// #return true if the ARD localizator should be displayed, otherwise false
bool Settings_ui::ardLocalizatorDockWidget(void)
{
    return checkBox_48->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the TPH localizator should be displayed
//
// #return true if the TPH localizator should be displayed, otherwise false
bool Settings_ui::tphLocalizatorDockWidget(void)
{
    return checkBox_49->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get color of table in XTT model.
//
// #return Color of table.
QColor Settings_ui::xttTableColor(void)
{ 
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton->palette().color(QPalette::Button).red(), toolButton->palette().color(QPalette::Button).green(), toolButton->palette().color(QPalette::Button).blue(), 255-horizontalSlider->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of selected table in XTT model.
//
// #return Color of selected table.
QColor Settings_ui::xttSelectedTableColor(void)
{ 
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton_2->palette().color(QPalette::Button).red(), toolButton_2->palette().color(QPalette::Button).green(), toolButton_2->palette().color(QPalette::Button).blue(), 255-horizontalSlider_2->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of connection in XTT model.
//
// #return Color of connection.
QColor Settings_ui::xttConnectionColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_3->palette().color(QPalette::Button).red(), toolButton_3->palette().color(QPalette::Button).green(), toolButton_3->palette().color(QPalette::Button).blue(), 255-horizontalSlider_3->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of selected connection in XTT model.
//
// #return Color of selected connection.
QColor Settings_ui::xttSelectedConnectionColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_4->palette().color(QPalette::Button).red(), toolButton_4->palette().color(QPalette::Button).green(), toolButton_4->palette().color(QPalette::Button).blue(), 255-horizontalSlider_4->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of cut connection in XTT model.
//
// #return Color of cut connection.
QColor Settings_ui::xttCutConnectionColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_5->palette().color(QPalette::Button).red(), toolButton_5->palette().color(QPalette::Button).green(), toolButton_5->palette().color(QPalette::Button).blue(), 255-horizontalSlider_5->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get table font color in XTT model.
//
// #return Table font color.
QColor Settings_ui::xttTableFontColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_6->palette().color(QPalette::Button).red(), toolButton_6->palette().color(QPalette::Button).green(), toolButton_6->palette().color(QPalette::Button).blue(), 255-horizontalSlider_6->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get table font color and table title in XTT model.
//
// #return Color table font color and table title.
QColor Settings_ui::xtTableTitleFontColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_7->palette().color(QPalette::Button).red(), toolButton_7->palette().color(QPalette::Button).green(), toolButton_7->palette().color(QPalette::Button).blue(), 255-horizontalSlider_7->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get font color of connection in XTT model.
//
// #return Table font color of connection.
QColor Settings_ui::xttConnectionLabelColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_8->palette().color(QPalette::Button).red(), toolButton_8->palette().color(QPalette::Button).green(), toolButton_8->palette().color(QPalette::Button).blue(), 255-horizontalSlider_8->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get scene background color in XTT model.
//
// #return Scene background color.
QColor Settings_ui::xttSceneBackgroundColor(void)
{ 
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton_9->palette().color(QPalette::Button).red(), toolButton_9->palette().color(QPalette::Button).green(), toolButton_9->palette().color(QPalette::Button).blue(), 255-horizontalSlider_9->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of highlighted cell in XTT model.
//
// #return Color of highlighted cell.
QColor Settings_ui::xttHoveredCellColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_10->palette().color(QPalette::Button).red(), toolButton_10->palette().color(QPalette::Button).green(), toolButton_10->palette().color(QPalette::Button).blue(), 255-horizontalSlider_10->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get cell color with error in XTT model.
//
// #return Cell color with error.
QColor Settings_ui::xttErrorCellColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_11->palette().color(QPalette::Button).red(), toolButton_11->palette().color(QPalette::Button).green(), toolButton_11->palette().color(QPalette::Button).blue(), 255-horizontalSlider_11->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of the font of xtt table header
//
// #return Color of the font of xtt table header
QColor Settings_ui::xttTableHeaderFontColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_12->palette().color(QPalette::Button).red(), toolButton_12->palette().color(QPalette::Button).green(), toolButton_12->palette().color(QPalette::Button).blue(), 255-horizontalSlider_12->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get color of the xtt table header
//
// #return Color of the xtt table header
QColor Settings_ui::xttTableHeaderColor(void)
{
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton_44->palette().color(QPalette::Button).red(), toolButton_44->palette().color(QPalette::Button).green(), toolButton_44->palette().color(QPalette::Button).blue(), 255-horizontalSlider_34->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get cell color with error in XTT model.
//
// #return Cell color with error.
QColor Settings_ui::xttTableBorderColor(void)
{ 
    QColor result = Qt::black;
    if(!checkBox->isChecked())
        result = QColor(toolButton_13->palette().color(QPalette::Button).red(), toolButton_13->palette().color(QPalette::Button).green(), toolButton_13->palette().color(QPalette::Button).blue(), 255-horizontalSlider_13->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the second color of the table rows
//
// #return second color of the table rows
QColor Settings_ui::xttTable2Color(void)
{
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton_42->palette().color(QPalette::Button).red(), toolButton_42->palette().color(QPalette::Button).green(), toolButton_42->palette().color(QPalette::Button).blue(), 255-horizontalSlider_32->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the second color of the selected table rows
//
// #return second color of the selected table rows
QColor Settings_ui::xttSelectedTable2Color(void)
{
    QColor result = Qt::white;
    if(!checkBox->isChecked())
        result = QColor(toolButton_43->palette().color(QPalette::Button).red(), toolButton_43->palette().color(QPalette::Button).green(), toolButton_43->palette().color(QPalette::Button).blue(), 255-horizontalSlider_33->value());

    return result;
}
// -----------------------------------------------------------------------------

// #brief Get table font in XTT model.
//
// #return Table font in XTT model.
QFont Settings_ui::xttTableFont(void)
{
    return toolButton_14->font();
}
// -----------------------------------------------------------------------------

// #brief Get table header font in XTT model.
//
// #return Table header font in XTT model.
QFont Settings_ui::xttTableHeaderFont(void)
{
    return toolButton_15->font();
}
// -----------------------------------------------------------------------------

// #brief Get table title font in XTT model.
//
// #return Table title font in XTT model.
QFont Settings_ui::xttTableTitleFont(void)
{
    return toolButton_16->font();
}
// -----------------------------------------------------------------------------

// #brief Get connection lable font in XTT model.
//
// #return Connection label font in XTT model.
QFont Settings_ui::xttConnectionLabelFont(void)
{
    return toolButton_17->font();
}
// -----------------------------------------------------------------------------

// #brief Get TPH font in ARD model.
//
// #return TPH font in ARD model.
QFont Settings_ui::ardTPHFont(void)
{
    return toolButton_18->font();
}
// -----------------------------------------------------------------------------

// #brief Get diagram font in ARD model.
//
// #return Diagram font in ARD model.
QFont Settings_ui::ardARDdiagramFont(void)
{
    return toolButton_19->font();
}
// -----------------------------------------------------------------------------

// #brief Get port number for hqedServer
//
// #return TCP port number for listening by hqedServer
int Settings_ui::hqedServerListenPort(void)
{
    return spinBox_10->value();
}
// -----------------------------------------------------------------------------

// #brief Get default column width.
//
// #return Default column width.
int Settings_ui::xttDefaultColumnWidth(void)
{
    int res = spinBox->value();
    if(checkBox_35->isChecked())
        res = 16;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get default row height.
//
// #return Default column height.
int Settings_ui::xttDefaultRowHeight(void)
{
    int res = spinBox_2->value();
    if(checkBox_35->isChecked())
        res = 16;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get default header height.
//
// #return Default header height.
int Settings_ui::xttDefaultHeaderHeight(void)
{
    int res = spinBox_3->value();
    if(checkBox_35->isChecked())
        res = 16;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get distance of ARD element on the scene when button for shifting is clicked.
//
// #return Distance of element.
int Settings_ui::ardMoveElementsDistance(void)
{
    return spinBox_7->value();
}
// -----------------------------------------------------------------------------

int Settings_ui::xttMoveElementsDistance(void)
{
    return spinBox_5->value();
}
// -----------------------------------------------------------------------------

// #brief Get progress (in percent) of compression graphic file being screen of scene ARD model.
//
// #return Distance progress (in percent) of compression.
int Settings_ui::ardImageCompression(void)
{
    return horizontalSlider_16->value();
}
// -----------------------------------------------------------------------------

// #brief Get progress (in percent) of compression graphic file being screen of scene XTT model.
//
// #return Distance progress (in percent) of compression.
int Settings_ui::xttImageCompression(void)
{
    return horizontalSlider_15->value();
}
// -----------------------------------------------------------------------------

// #brief Get decision about automatic creating name of acronym.
//
// #return Decision about automatic creating name of acronym.
// #li True - create name of acronym.
// #li False - don't create name of acronym.
bool Settings_ui::xttCreateAutoAcronym(void)
{
    return checkBox_10->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about automatic creating name of table.
//
// #return Decision about automatic creating name of table.
// #li True - create name of table.
// #li False - don't create name of table.
bool Settings_ui::xttCreateTableAutoName(void)
{
    return checkBox_9->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying message when error occurs.
//
// #return Decision about displaying error message.
// #li True - display error message.
// #li False - don't display error message.
bool Settings_ui::xttShowErrorMessages(void)
{
    return xttShowErrorCellMessage();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying error cell message.
//
// #return Decision about displaying error cell message.
// #li True - display error cell message.
// #li False - don't display error cell message.
bool Settings_ui::xttShowErrorCellMessage(void)
{
    return checkBox_3->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about highlighting error cell.
//
// #return Decision about highlighting error cell.
// #li True - highlight error cell.
// #li False - don't highlight error cell.
bool Settings_ui::xttHighlightErrorCell(void)
{
    return checkBox_4->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying erron tool tip in cell.
//
// #return Decision about displaying erron tool tip in cell.
// #li True - display error.
// #li False - don't display error.
bool Settings_ui::xttShowErrorInToolTip(void)
{
    return checkBox_5->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get default value of attribute comparing in cell.
//
// #return Default value of attribute comparing in cell.
// #li True - the same attributes.
// #li False - differnet attributes.
bool Settings_ui::xttAttrToAttrComp(void)
{
    return checkBox_11->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about checking values (that are not arythmic value) in cell.
//
// #return Decision about checking values (that are not arythmic value) in cell.
// #li True - check values in cell.
// #li False - don't check values in cell.
bool Settings_ui::xttCheckAtomicValues(void)
{
    return checkBox_12->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about executing cell during analysis when attribute comparing is turned off.
//
// #return Decision about executing cell during analysis when attribute comparing is turned off.
// #li True - execute cell.
// #li False - don't execute cell.
bool Settings_ui::xttExecuteCellWhenOff(void)
{
    return checkBox_13->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about executing cell during analysis when attribute comparing is turned on.
//
// #return Decision about executing cell during analysis when attribute comparing is turned on.
// #li True - execute cell.
// #li False - don't execute cell.
bool Settings_ui::xttExecuteCellWhenOn(void)
{
    return checkBox_14->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about automatic recognizing table type.
//
// #return Decision about automatic recognizing table type.
// #li True - turn on automatic recognizing table type.
// #li False - don't turn on automatic recognizing table type.
bool Settings_ui::autoTableTypeRecognition(void)
{
    return checkBox_15->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying worning about wrong table type.
//
// #return Decision about displaying wrong table type.
// #li True - display worning about wrong table type.
// #li False - don't display worning about wrong table type.
bool Settings_ui::messageAboutAmbiguousTableType(void)
{
    return checkBox_16->isChecked();
}
// -----------------------------------------------------------------------------

bool Settings_ui::massageAboutInaccesibleRow(void)
{
    return checkBox_17->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying worning about wrong table type.
//
// #return Decision about displaying wrong table type.
// #li True - display worning about wrong table type.
// #li False - don't display worning about wrong table type.
bool Settings_ui::createMapMovement(void)
{
    return checkBox_18->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about highlightling current cell.
//
// #return Decision about highlightling current cell.
// #li True - highlight current cell.
// #li False - don't highlight current cell.
bool Settings_ui::highlightCurrentCell(void)
{
    return checkBox_19->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about beeping after analisys.
//
// #return Decision about beeping after analisys.
// #li True - beep after analisys.
// #li False - don't beep after analisys.
bool Settings_ui::beepOnFinish(void)
{
    return checkBox_20->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about beeping after analisys.
//
// #return Decision about beeping after analisys.
// #li True - beep after analisys.
// #li False - don't beep after analisys.
bool Settings_ui::showRunCellStatus(void)
{
    return checkBox_21->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying system tray icon.
//
// #return Decision about displaying system tray icon.
// #li True - display system tray icon,
// #li False - don't display system tray icon
bool Settings_ui::showSystemTrayIcon(void)
{
    return checkBox_29->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about displaying tray tip.
//
// #return Decision about displaying tray tip.
// #li True - display tray tip.
// #li False - don't display tray tip.
bool Settings_ui::showTrayTips(void)
{
    return checkBox_30->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about autocentring ARD elements.
//
// #return Decision about autocentring ARD elements.
// #li True - autocentre ARD elements.
// #li False - don't autocentre ARD elements.
bool Settings_ui::ardEnabledAutocentering(void)
{
    return checkBox_34->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about autocentring XTT elements.
//
// #return Decision about autocentring XTT elements.
// #li True - autocentre XTT elements.
// #li False - don't autocentre XTT elements.
bool Settings_ui::xttEnabledAutocentering(void)
{
    return checkBox_31->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about autoresizing XTT scene.
//
// #return Decision about autoresizing XTT scene.
// #li True - autoresize XTT scene.
// #li False - don't autoresize XTT scene.
bool Settings_ui::xttAutoSceneResize(void)
{
    return checkBox_32->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about formating XML files in hierarchy way.
//
// #return Decision about formating XML files in hierarchy way.
// #li True - format XML files in hierarchy way.
// #li False - don't format XML files in hierarchy way.
bool Settings_ui::xmlAutoformating(void)
{
    return checkBox_33->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get if the hqedSerwer should start automatically when application starts
//
// #return true if the hqedSerwer should start automatically when application starts, otherwise false
bool Settings_ui::hqedServerAutostart(void)
{
    return checkBox_53->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about creating initial tables during the XTT schema generation process.
//
// #return Get decision about creating initial tables during the XTT schema generation process.
bool Settings_ui::createInitialTables(void)
{
    return checkBox_37->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about policy of duplicate connections.
//
// #return Get decision about policy of duplicate connections.
bool Settings_ui::allowDuplicateConnection(void)
{
    return checkBox_38->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about XTT optimalization after schema generating
//
// #return Get decision about XTT optimalization after schema generating.
bool Settings_ui::optimizeXTT(void)
{
    return checkBox_39->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the search widget should be displayed
//
// #return true if the search widget should be displayed, otherwise false
bool Settings_ui::xttSearchDockWidget(void)
{
    return checkBox_36->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the error dock widget should be displayed
//
// #return true if the error odck widget should be displayed, otherwise false
bool Settings_ui::xttErrorDockWidget(void)
{
    return checkBox_40->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the execute dock widget should be displayed. The widget contains messages generated during executing model.
//
// #return true if the execute dock widget should be displayed, otherwise false
bool Settings_ui::xttExecuteDockWidget(void)
{
    return checkBox_41->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the localizator dock widget should be displayed. The widget supports navigation in the model
//
// #return true if the localizator dock widget should be displayed, otherwise false
bool Settings_ui::xttLocalizatorDockWidget(void)
{
    return checkBox_44->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the plugin event listener dock widget should be displayed.
//
// #return true if the plugin event listener dock widget should be displayed, otherwise false
bool Settings_ui::xttPluginEventListenerDockWidget(void)
{
    return checkBox_45->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the states dock widget should be displayed.
//
// #return true if the states dock widget should be displayed, otherwise false
bool Settings_ui::xttStatesDockWidget(void)
{
    return checkBox_52->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the neutral zeros must be added before value w.r.t. length value
//
// #return true if the neutral zeros must be added before value w.r.t. length value, otherwise false
bool Settings_ui::addNeutralZerosBefore(void)
{
    return checkBox_42->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the neutral zeros must be added after value w.r.t. scale value
//
// #return true if the neutral zeros must be added after value w.r.t. scale value, otherwise false
bool Settings_ui::addNeutralZerosAfter(void)
{
    return checkBox_43->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Returns if the attributes should be refered by acronyms instead of names
//
// #return true if the attributes should be refered by acronyms instead of names
bool Settings_ui::attsReferenceByAcronyms(void)
{
    return checkBox_51->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Function that changes the type of references to the XTT attributes
//
// #return No return value.
void Settings_ui::changeXttAttributeReferenceTypeStatus(void)
{
    checkBox_51->setChecked(!checkBox_51->isChecked());
    saveOptions();
    TableList->RefreshAll();
    MainWin->xtt_state_scene->update();
}
// -----------------------------------------------------------------------------

// #brief Get default name of the initial tables
//
// #return String that is a default name of the initial tables
QString Settings_ui::xttInitialTableDefaultName(void)
{
    return lineEdit_3->text();
}
// -----------------------------------------------------------------------------

// #brief Get base name for automatic table.
//
// #return String containing base name for automatic table.
QString Settings_ui::xttTableBaseName(void)
{
    return lineEdit_2->text();
}
// -----------------------------------------------------------------------------

// #brief Get distance between connections comming out of the same table.
//
// #return Distance between connections.
int Settings_ui::xttConnectionsDistance(void)
{
    return spinBox_6->value();
}
// -----------------------------------------------------------------------------

// #brief Get string that replayces space in attribute values.
//
// #return String replacing space in attribute values.
QString Settings_ui::spaceReplacer(void)
{
    return lineEdit->text();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the path of the ecmascripts directory
//
// #return the path of the ecmascripts directory as string
QString Settings_ui::ecmascriptPath(void)
{
    return hqed::processAppVariables(label_51->text());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the path of the configuration directory
//
// #return the path of the configuration directory as string
QString Settings_ui::configurationPath(void)
{
    return hqed::processAppVariables(label_54->text());
}
// -----------------------------------------------------------------------------

// #brief Function that returns the path of the default project directory
//
// #return the path of the default project directory as string
QString Settings_ui::projectPath(void)
{
    return hqed::processAppVariables(label_53->text());
}
// -----------------------------------------------------------------------------

// #brief Get color type for application mode.
//
// #param appMode Mode of application.
// #return Color type for application mode.
bool Settings_ui::colorMode(int appMode)
{
    bool res = false;
    if(appMode == ARD_MODE)
        res = checkBox_25->isChecked();
    if(appMode == XTT_MODE)
        res = checkBox->isChecked();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get decision about using framed nodes ARD and TPH.
//
// #return Decision about using framed nodes.
// #li True - use framed nodes.
// #li False - don't use framed nodes.
bool Settings_ui::useFramedNodes(void)
{
    return checkBox_27->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get decision about using antyaliasing for application mode.
//
// #param appMode Mode of application.
// #return Decision about using antyaliasing for application mode.
// #li True - use antyaliasing.
// #li False - don't use antyaliasing.
bool Settings_ui::useAntialiasing(int appMode)
{
    bool res = false;
    if(appMode == ARD_MODE)
        res = checkBox_24->isChecked();
    if(appMode == XTT_MODE)
        res = checkBox_2->isChecked();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get decision about using text antyaliasing for application mode.
//
// #param appMode Mode of application.
// #return Decision about using text antyaliasing for application mode.
// #li True - use text antyaliasing.
// #li False - don't use text antyaliasing.
bool Settings_ui::useTextAntialiasing(int appMode)
{
    bool res = false;
    if(appMode == ARD_MODE)
        res = checkBox_22->isChecked();
    if(appMode == XTT_MODE)
        res = checkBox_6->isChecked();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get decision about using smooth tranformation for bitmap.
//
// #param appMode Mode of application.
// #return Decision about using smooth tranformation for bitmap.
// #li True - use smooth tranformation.
// #li False - don't use smooth tranformation.
bool Settings_ui::useSmoothPixmapTransform(int appMode)
{
    bool res = false;
    if(appMode == ARD_MODE)
        res = checkBox_23->isChecked();
    if(appMode == XTT_MODE)
        res = checkBox_7->isChecked();

    return res;
}
// -----------------------------------------------------------------------------

// #brief Get information whether startup dialog should be shown.
//
// #return Information whether startup dialog should be shown
// #li True - startup dialog should be shown.
// #li False - startup dialog should not be shown.
bool Settings_ui::useStartupDialog(void)
{
    return checkBox_54->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Set whether startup dialog should be shown.
//
// #param use - set or unset startup dialog showing
// #return No return value.
void Settings_ui::setStartupDialogShowing(bool use)
{
    conf->setValue("Application_options/useStartupDialog", use);
    checkBox_54->setChecked(use);
}
// -----------------------------------------------------------------------------

// #brief Get if the tables should be hide automatically
//
// #return Type of background for start screen.
// #li 0 - tables will not be hide automatically
// #li 1 - tables will be hide automatically
bool Settings_ui::xttTableAutoHide(void)
{
    return checkBox_46->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Function that changes the status of hidding XTT tables
//
// #return No return value.
void Settings_ui::changeXttTableAutoHideStatus(void)
{
    checkBox_46->setChecked(!checkBox_46->isChecked());
    saveOptions();
    TableList->RefreshAll();
}
// -----------------------------------------------------------------------------

// #brief Indicates if the initial tables should be displayed on the main xtt scene
//
// #return if the initial tables should be displayed on the main xtt scene:
// #li 0 - tables will not be showed on main xtt scene
// #li 1 - tables will be showed on main xtt scene
bool Settings_ui::xttShowInitialTables(void)
{
    return checkBox_47->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Function that changes the status of hidding initial tables
//
// #return No return value.
void Settings_ui::changeXttInitialTableHideStatus(void)
{
    checkBox_47->setChecked(!checkBox_47->isChecked());
    saveOptions();
    TableList->tablesLayout();
    TableList->RefreshAll();
    MainWin->xtt_scene->AdaptSceneSize();
}
// -----------------------------------------------------------------------------

// #brief Function that returns the type of default edit mode of xtt table
//
// #return the type of default edit mode of xtt table
int Settings_ui::xttTableEditMode(void)
{
    int types[] = {XTT_TABLE_EDIT_MODE_FULL, XTT_TABLE_EDIT_MODE_SIMPLE};
    return types[comboBox_5->currentIndex()];
}
// -----------------------------------------------------------------------------

// Zwraca status automatycznego powiekszania rozmiarow komorki
// #brief Get decision about using autosize for cell.
//
// #param appMode Mode of application.
// #return Decision about using autosize for cell.
// #li True - use autosize.
// #li False - don't use autosize.
bool Settings_ui::xttCellAutosize(void)
{
    return checkBox_35->isChecked();
}
// -----------------------------------------------------------------------------

// #brief Get node select policy in ARD model.
//
// #return Node select policy.
int Settings_ui::ardNodeSelectPolicy(void)
{
    int res[3] = {SELECT_ONLY_NODE, SELECT_CONNECTIONS, SELECT_CONNECTIONS_WITH_CTRL};
    return res[comboBox_4->currentIndex()];
}
// -----------------------------------------------------------------------------

// #brief Get node select policy in XTT model.
//
// #return Node select policy.
int Settings_ui::xttTableSelectPolicy(void)
{
    int res[3] = {SELECT_ONLY_NODE, SELECT_CONNECTIONS, SELECT_CONNECTIONS_WITH_CTRL};
    return res[comboBox_3->currentIndex()];
}
// -----------------------------------------------------------------------------	

// #brief Get node select policy in XTT model.
//
// #return Node select policy.
int Settings_ui::largeDomainSize(void)
{
    return spinBox_8->value();
}
// -----------------------------------------------------------------------------	

// #brief get the default accuracy for numeric types
//
// #return the default accuracy for numeric types
int Settings_ui::defaultAccuracy(void)
{
    return spinBox_9->value();
}
// -----------------------------------------------------------------------------	

// #brief Call when add plugin button is pressed
//
// #return No return value.
void Settings_ui::onAddPlugin(void)
{
    delete PluginEditor;
    PluginEditor = new PluginEditor_ui;
    PluginEditor->setMode(PLUGINDIALOGEDITORMODE_NEW, -1);
    connect(PluginEditor, SIGNAL(onOkClose()), this, SLOT(onClosePluginEditor()));
    PluginEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Call when edit plugin button is pressed
//
// #return No return value.
void Settings_ui::onEditPlugin(void)
{
    int plindex = listWidget_2->currentRow();
    int plc = pluginsCount();
    if((plindex < 0) || (plindex >= plc))
        return;

    delete PluginEditor;
    PluginEditor = new PluginEditor_ui;
    PluginEditor->setMode(PLUGINDIALOGEDITORMODE_EDT, plindex);
    connect(PluginEditor, SIGNAL(onOkClose()), this, SLOT(onClosePluginEditor()));
    PluginEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Call when delete plugin button is pressed
//
// #return No return value.
void Settings_ui::onDeletePlugin(void)
{
    int plindex = listWidget_2->currentRow();
    int plc = pluginsCount();
    if((plindex < 0) || (plindex >= plc))
        return;

    if(QMessageBox::warning(this, "HQEd", "Do you really want to remove plugin \'" + listWidget_2->currentItem()->text() + "\'", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
        return;
    removePlugin(plindex);
}
// -----------------------------------------------------------------------------

// #brief Function that returns the number of the registered plugins
//
// #return the number of the registered plugins
int Settings_ui::pluginsCount(void)
{
    return conf->value(pluginsGroupName() + "/plscount", 0).toInt();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the number of plugins
//
// #param __cnt - new number of plugins
// #return no values return
void Settings_ui::setPluginsCount(int __cnt)
{
    if(__cnt < 0)
    {
        QMessageBox::critical(NULL, "HQEd", "The number of registered plugins cannot be less than zero.\nCandidate value: " + QString::number(__cnt), QMessageBox::Ok);
        return;
    }

    conf->beginGroup(pluginsGroupName());
    conf->setValue("plscount", __cnt);
    conf->endGroup();
}
// ----------------------------------------------------------------------------

// #brief Function that returns the plugins group name in the config file
//
// #return the plugins group name in the config file
QString Settings_ui::pluginsGroupName(void)
{
    return "Plugins";
}
// ----------------------------------------------------------------------------

// #brief Function that returns the property string for given plugin index and property name
//
// #param __plindex - index of the plugin
// #param __property - name of the plugin property
// #return the property string for given plugin index and property name
QString Settings_ui::pluginPropertyString(int __plindex, QString __property)
{
    QString res = "pl" + QString::number(__plindex) + __property;
    return res;
}
// ----------------------------------------------------------------------------

// #brief Function that returns the key configuration for given plugin nad property
//
// #param __plindex - index of the plugin
// #param __property - name of the plugin property
// #return the key configuration for given plugin nad property
QString Settings_ui::pluginKeyPropertyString(int __plindex, QString __property)
{
    QString res = pluginsGroupName() + "/" + pluginPropertyString(__plindex, __property);
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that reads the plugin propery value
//
// #param __plindex - index of the plugin
// #param __property - the name of the plugin property
// #param __defval - the default value that is returned when the source value cannot be found
// #return the plugin's property value
QString Settings_ui::readPluginProperty(int __plindex, QString __property, QString __defval)
{
    return conf->value(pluginKeyPropertyString(__plindex, __property), __defval).toString();
}
// -----------------------------------------------------------------------------

// #brief Function that sets the plugin propery value
//
// #param __plindex - index of the plugin
// #param __property - the name of the plugin property
// #param __value - the new value of the property
// #return no values return
void Settings_ui::setPluginProperty(int __plindex, QString __property, QString __value)
{
    QString plsect = pluginsGroupName();

    int pc = pluginsCount();
    QString pluginbasename = "pl" + QString::number(__plindex);

    if((__plindex >= pc) || (__plindex < 0))
    {
        QMessageBox::critical(NULL, "HQEd", "Plugin index out of range.\nCurrent plugins count: " + QString::number(pc) + "\nIndex candidate: " + QString::number(__plindex), QMessageBox::Ok);
        return;
    }

    conf->beginGroup(plsect);
    conf->setValue(pluginbasename + __property, __value);
    conf->endGroup();
}
// -----------------------------------------------------------------------------

// #brief Function that is called when PluginEditor is closed by OK button
//
// #return No value returns.
void Settings_ui::onClosePluginEditor(void)
{
    int pc = pluginsCount();
    int currpls = pc;
    QString oldPluginName = "";

    if(PluginEditor->mode() == 0)
        pc++;
    if(PluginEditor->mode() == 1)
    {
        currpls = PluginEditor->plindex();
        oldPluginName = readPluginProperty(currpls, "name", "");
    }

    QString pluginbasename = "pl" + QString::number(currpls);
    QString plenabled = readPluginProperty(currpls, "enabled", "true");

    // Collection information about plugin
    QString plname = PluginEditor->plname();
    QString pladdr = PluginEditor->pladdress();
    QString plparams = PluginEditor->plparams();
    int pltimeout = PluginEditor->pltimeout();
    int plpnumber = PluginEditor->plportnumber();
    int plconfig = PluginEditor->plconfiguration();

    setPluginsCount(pc);
    setPluginProperty(currpls, "name", plname);
    setPluginProperty(currpls, "address", pladdr);
    setPluginProperty(currpls, "params", plparams);
    setPluginProperty(currpls, "pnumber", QString::number(plpnumber));
    setPluginProperty(currpls, "ctimeout", QString::number(pltimeout));
    setPluginProperty(currpls, "config", QString::number(plconfig));
    setPluginProperty(currpls, "enabled", plenabled);

    // Setting name of the plugin inside controller - this must be done. In other case there will be created a new additional plugin
    int pio = devPC->indexOfName(oldPluginName);
    if((PluginEditor->mode() == 1) && (pio > -1))
        devPC->plugins()->at(pio)->setName(plname);

    readPlugins();
    updatePlugins();

    // Adding new plugin to main window
    if(PluginEditor->mode() == 0)
        MainWin->registerPlugin(devPC->plugins()->last());
}
// -----------------------------------------------------------------------------

// #brief Function that is called when directory selectoris clicked
//
// #return No value returns.
void Settings_ui::onSelectEcmascriptDirectory(void)
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    hqed::processAppVariables(ecmascriptPath()),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    if(dir.isEmpty())
        return;

    dir = dir.replace(hqed::appRootPath(), "%appdir%");
    if(dir.endsWith("/"))
        dir.chop(1);
    label_51->setText(dir);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when directory selectoris clicked
//
// #return No value returns.
void Settings_ui::onSelectDefaultProjectDirectory(void)
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    hqed::processAppVariables(projectPath()),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    if(dir.isEmpty())
        return;

    dir = dir.replace(hqed::appRootPath(), "%appdir%");
    if(dir.endsWith("/"))
        dir.chop(1);
    label_53->setText(dir);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when directory selectoris clicked
//
// #return No value returns.
void Settings_ui::onSelectConfigurationDirectory(void)
{
    QString dir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                    hqed::processAppVariables(configurationPath()),
                                                    QFileDialog::ShowDirsOnly
                                                    | QFileDialog::DontResolveSymlinks);

    if(dir.isEmpty())
        return;

    dir = dir.replace(hqed::appRootPath(), "%appdir%");
    if(dir.endsWith("/"))
        dir.chop(1);
    label_54->setText(dir);
}
// -----------------------------------------------------------------------------

// #brief Function that is called on plugin selection changed
//
// #param __plindex - new plugin index
// #return No value returns.
void Settings_ui::onPluginSelectionChanged(int __plindex)
{
    if((__plindex >= pluginsCount()) || (__plindex < 0))
        return;

    bool penabled = readPluginProperty(__plindex, "enabled", "true") == "true";
    int pconf = readPluginProperty(__plindex, "config", "0").toInt();
    QString ppnum = readPluginProperty(__plindex, "pnumber", "0");
    QString pname = readPluginProperty(__plindex, "name", "0");
    QString paddr = readPluginProperty(__plindex, "address", "0");
    QString plparams = readPluginProperty(__plindex, "params", "");

    QString ptype = "unknown";
    if(((pconf & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0) && ((pconf & DEV_PLUGIN_SOCKET_TYPE_TCP) > 0))
        ptype = "TCP/IP";
    if(((pconf & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0) && ((pconf & DEV_PLUGIN_SOCKET_TYPE_UDP) > 0))
        ptype = "UDP/IP";
    if((pconf & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
        ptype = "Process";

    if((pconf & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0)
        paddr = paddr + ":" + ppnum;
    if((pconf & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
        paddr = paddr + " " + plparams;

    QString pmods = "";
    pmods += ((pconf & DEV_PLUGIN_SUPPORTED_MODULES_V) > 0 ? "Verification " : "");
    pmods += ((pconf & DEV_PLUGIN_SUPPORTED_MODULES_P) > 0 ? "Presentation " : "");
    pmods += ((pconf & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0 ? "Simulation " : "");
    pmods += ((pconf & DEV_PLUGIN_SUPPORTED_MODULES_T) > 0 ? "Translation " : "");

    toolButton_38->setChecked(penabled);
    toolButton_37->setChecked(!penabled);
    label_59->setText(pname);
    label_60->setText(ptype);
    label_61->setText(paddr);
    label_62->setText(pmods);
}
// -----------------------------------------------------------------------------

// #brief Function that is called on lock plugin button state changed
//
// #param __checked - new state of the button
// #return No value returns.
void Settings_ui::onPlLockChecked(bool __checked)
{
    if(__checked == false)
        return;

    toolButton_37->setChecked(!__checked);

    // Saving state of the plugin
    int pluginIndex = listWidget_2->currentRow();
    setPluginProperty(pluginIndex, "enabled", "true");
}
// -----------------------------------------------------------------------------

// #brief Function that is called on unlock plugin button state changed
//
// #param __checked - new state of the button
// #return No value returns.
void Settings_ui::onPlUnlockChecked(bool __checked)
{
    if(__checked == false)
        return;

    toolButton_38->setChecked(!__checked);

    // Saving state of the plugin
    int pluginIndex = listWidget_2->currentRow();
    setPluginProperty(pluginIndex, "enabled", "false");
}
// -----------------------------------------------------------------------------

// #brief Function that returns the index of the plugin that has given name.
//
// #param __name - name of the plugin
// #param __ignoreindex - index that is ignored while searching
// #return index of the plugin or -1 if plugin with given name does not exist
int Settings_ui::pluginIndexOfname(QString __name, int __ignoreindex)
{
    int pc = pluginsCount();
    for(int i=0;i<pc;++i)
    {
        QString plname = readPluginProperty(i, "name", "");
        if((__name == plname) && (i != __ignoreindex))
            return i;
    }
    return -1;
}
// -----------------------------------------------------------------------------

// #brief Function that updates the plugin configuration
//
// #param __name - name of the plugin
// #param __configuration - plugin configuration
// #return no values return.
void Settings_ui::pluginUpdateConfiguration(QString __name, int __configuration)
{
    int io = pluginIndexOfname(__name, -1);
    if(io == -1)
        return;

    setPluginProperty(io, "config", QString::number(__configuration));
}
// -----------------------------------------------------------------------------

// #brief Function that reads the plugin names from the settings
//
// #return no values return.
void Settings_ui::readPlugins(void)
{
    int pc = pluginsCount();
    int ii = listWidget_2->currentRow();

    listWidget_2->clear();
    for(int i=0;i<pc;++i)
    {
        QString pluginbasename = "pl" + QString::number(i);
        listWidget_2->addItem(readPluginProperty(i, "name", ""));
    }

    if(ii < listWidget_2->count())
        listWidget_2->setCurrentRow(ii);
    if(ii >= listWidget_2->count())
        listWidget_2->setCurrentRow(listWidget_2->count()-1);
}
// -----------------------------------------------------------------------------

// #brief Function that updates the plugins parameters w.r.t. settings
//
// #return no values return.
void Settings_ui::updatePlugins(void)
{
    // updating plugins w.r.t. settings
    int pc = pluginsCount();
    for(int i=0;i<pc;++i)
    {
        QString plname = readPluginProperty(i, "name", "");
        QString pladdr = readPluginProperty(i, "address", "");
        QString plparams = readPluginProperty(i, "params", "");
        int plpnumber = readPluginProperty(i, "pnumber", "1").toInt();
        int plctimeout = readPluginProperty(i, "ctimeout", "-1").toInt();
        int plconfig = readPluginProperty(i, "config", "0").toInt();
        bool plenabled = readPluginProperty(i, "enabled", "true").toLower() == "true";

        devPlugin* devpl = NULL;
        int pio = devPC->indexOfName(plname);
        if(pio == -1)
            devpl = devPC->createNewPlugin(plname);
        if(pio > -1)
            devpl = devPC->plugins()->at(pio);

        // Setting new values
        if(!devpl->setConfiguration(plconfig))
        {
            QMessageBox::critical(NULL, "HQEd", "The configuration of the plugin \'" + plname + "\' is out of domain.", QMessageBox::Ok);
            continue;
        }

        // Creating plugin i/o device
        QIODevice* cd = NULL;
        if(((plconfig & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0) && ((plconfig & DEV_PLUGIN_SOCKET_TYPE_TCP) > 0))
            cd = new QTcpSocket(NULL);
        //cd = new QAbstractSocket(QAbstractSocket::TcpSocket, NULL);
        if(((plconfig & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0) && ((plconfig & DEV_PLUGIN_SOCKET_TYPE_UDP) > 0))
            cd = new QUdpSocket(NULL);
        //cd = new QAbstractSocket(QAbstractSocket::UdpSocket, NULL);
        if((plconfig & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
            cd = new QProcess(this);

        devpl->setDevice(cd, plname);
        devpl->setName(plname);
        devpl->setAddress(pladdr);
        devpl->setParams(plparams);
        devpl->setPortNumber(plpnumber);
        devpl->setTimeout(plctimeout);
        devpl->setEnabled(plenabled);
    }
}
// ----------------------------------------------------------------------------

// #brief Function that removes plugin
//
// #brief __plindex - plugin index
// #return no values return.
void Settings_ui::removePlugin(int __plindex)
{
    int pc = pluginsCount();
    if((__plindex < 0) || (__plindex >= pc))
        return;

    devPC->removePlugin(readPluginProperty(__plindex, "name", ""));    // removing plugin from plugin controller
    for(int i=__plindex;i<pc-1;++i)
    {
        setPluginProperty(i, "name", readPluginProperty(i+1, "name", ""));
        setPluginProperty(i, "address", readPluginProperty(i+1, "address", ""));
        setPluginProperty(i, "params", readPluginProperty(i+1, "params", ""));
        setPluginProperty(i, "pnumber", readPluginProperty(i+1, "pnumber", ""));
        setPluginProperty(i, "ctimeout", readPluginProperty(i+1, "ctimeout", ""));
        setPluginProperty(i, "config", readPluginProperty(i+1, "config", ""));
        setPluginProperty(i, "enabled", readPluginProperty(i+1, "enabled", ""));
    }
    conf->remove(pluginKeyPropertyString(pc-1, "name"));
    conf->remove(pluginKeyPropertyString(pc-1, "address"));
    conf->remove(pluginKeyPropertyString(pc-1, "params"));
    conf->remove(pluginKeyPropertyString(pc-1, "pnumber"));
    conf->remove(pluginKeyPropertyString(pc-1, "ctimeout"));
    conf->remove(pluginKeyPropertyString(pc-1, "config"));
    conf->remove(pluginKeyPropertyString(pc-1, "enabled"));
    setPluginsCount(pc-1);

    readPlugins();
}
// ----------------------------------------------------------------------------

// #brief Function that adds a filename to recent used files list
//
// #param fileName filename
// #return no values return
void Settings_ui::recentFile(QString fileName)
{
     QStringList files = conf->value("Recent_files/recentFilesList").toStringList();
     files.removeAll(fileName);
     files.prepend(fileName);
     while (files.size() > 10)
     {
          files.removeLast();
     }

     conf->setValue("Recent_files/recentFilesList", files);
}
// ----------------------------------------------------------------------------
 
// #brief Function that returns the recent used files list
//
// #return the recent used files list
QStringList Settings_ui::recentFiles(void)
{
     return conf->value("Recent_files/recentFilesList").toStringList();
}
// ----------------------------------------------------------------------------

// #brief Clear recent files list.
//
// #return No return value.
void Settings_ui::clearRecentFiles(void)
{
     conf->remove("Recent_files");
}
