/**
* \file   TypesWidget.cpp
* \author Mateusz Zieliński
* \date   20.03.2013
* \version 1.0
* \brief  This file defines class representing widget aloowing to create and edit types.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include "TypesWidget.h"
#include "MainWin_ui.h"
#include "../M/hType.h"
#include "../M/hTypeList.h"

#define UNSAVED_INDICATOR 0 //0 stands for '*'in save button text, other numbers mean disabling/enabling save button

// #brief Constructor of TypesWidget class.
//
// #param parent - pointer to parent object
TypesWidget::TypesWidget(QWidget*)
{
     setupUi(this);

     for (int i = 0; i < hpTypesInfo.ptc; i++)
          baseTypeComboBox->addItem(hpTypesInfo.ptn.at(i));
        
     QStringList colHeadersLabels = QStringList() << "Name" << "Description" << "Base type" << "Domain";
     int colCount = colHeadersLabels.count();     
     tableWidget->setColumnCount(colCount);
     tableWidget->setHorizontalHeaderLabels(colHeadersLabels);
     tableWidget->horizontalHeader()->setSortIndicatorShown(true);
     tableWidget->horizontalHeader()->setStretchLastSection(true);
     
     updateTable();
     clearForm();
     
     connect(nameTextField, SIGNAL(textChanged(QString)), this, SLOT(nameChanged(QString)));
     connect(descriptionTextField, SIGNAL(textChanged()), this, SLOT(validateDescription()));
     connect(baseTypeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(baseTypeChanged(int)));
     connect(addTypeButton, SIGNAL(clicked()), this, SLOT(addTypeClicked()));
     connect(addTypeButton, SIGNAL(clicked()), this, SLOT(clearForm()));
     connect(clearFormButton, SIGNAL(clicked()), this, SLOT(clearForm()));
     connect(saveTypeButton, SIGNAL(clicked()), this, SLOT(saveTypeClicked()));
     connect(removeTypeButton, SIGNAL(clicked()), this, SLOT(removeTypeClicked()));
     connect(constraintsButton, SIGNAL(clicked()), this, SLOT(showConstraintsDialog()));
     connect(tableWidget, SIGNAL(itemClicked(QTableWidgetItem*)), this, SLOT(selectionChanged()));
     connect(tableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(cellTextContentEdited(int, int)));
     connect(tableWidget, SIGNAL(cellChanged(int, int)), this, SLOT(selectionChanged()));
     connect(tableWidget, SIGNAL(cellDoubleClicked(int, int)), this, SLOT(inCellComplexEdit(int, int)));
     connect(tableWidget->horizontalHeader(), SIGNAL(sectionClicked(int)), this, SLOT(sortTableByColumn(int)));
}

// #brief Destructor of TypesWidget class.
TypesWidget::~TypesWidget(void)
{
}

// #brief Maps type selected in table to the existing type.
//
// #return Pointer to type selected in table
hType* TypesWidget::getSelectedType()
{
     if (tableWidget->currentRow() > -1)
          return typeList->at(hpTypesInfo.ptc + tableWidget->currentRow());
     return NULL;
}

// #brief Enables or disables constraints button depending on the chosen type's base.
//
// #param baseTypeIndex - index of chosen type
// #return No return value
void TypesWidget::baseTypeChanged(int baseTypeIndex)
{ 
     hType* selectedType = getSelectedType();
     
     if (selectedType == NULL)
          return;
     
     if (selectedType->baseType() == baseTypeIndex)
          removeUnsavedIndicator();
     else
          showUnsavedIndicator();
}

// #brief Adds new type based on form data.
//
// #return No return value
void TypesWidget::addTypeClicked(void)
{
     tableWidget->blockSignals(true);
     
     hType* newType = new hType(typeList, NO_TYPE_CLASS, baseTypeComboBox->currentIndex());
     newType->setName(nameTextField->text());
     newType->setDescription(descriptionTextField->toPlainText());
     typeList->add(newType);
     
     tableWidget->blockSignals(false);
     
     updateTable();
}

// #brief Saves existing type.
//
// #return No return value
void TypesWidget::saveTypeClicked(void)
{
     QString enteredName = nameTextField->text().trimmed();
     QString previousName = tableWidget->item(tableWidget->currentRow(), 0)->text().trimmed();     
     
     if ((enteredName != previousName) && !isValidName(enteredName))
     {
          displayInvalidNameMessage(enteredName);
          return;   
     }
      
     hType* existingType = getSelectedType();
     existingType->setName(nameTextField->text());
     existingType->setDescription(descriptionTextField->toPlainText());
     
     int newBaseType = baseTypeComboBox->currentIndex();
     if (existingType->baseType() != newBaseType)
          existingType->domain()->flush();
     
     existingType->setType(newBaseType);

     if (newBaseType == BOOLEAN_BASED)
          existingType->setClass(NO_TYPE_CLASS);
     
     removeUnsavedIndicator();
     addTypeButton->setEnabled(false);
     
     updateTable();
}

// #brief Displays message box with info why selected name is invalid.
//
// #param name - selected name
// #return No return value
void TypesWidget::displayInvalidNameMessage(QString name)
{
     QString info;
               
     if (name.isEmpty())
          info = "Type name cannot be empty.";
     else 
          info = "Type named \'" + name + "\' already exists. Choose a different name.";
               
     QMessageBox::information(this, "HQEd", info, QMessageBox::Ok);
}

// #brief Removes existing type.
//
// #return No return value
void TypesWidget::removeTypeClicked(void)
{
     tableWidget->blockSignals(true);
    
     int currentIndex = tableWidget->currentRow();
     if (currentIndex == -1)
          return;
          
     int typesInfoIndex = hpTypesInfo.ptc + currentIndex;
     if (typesInfoIndex >= typeList->size())
          return;
          
     if (QMessageBox::information(this, "HQEd", "Do you really want to remove type \'" + typeList->at(typesInfoIndex)->name() + "\'?",
         QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)   
          return;
          
     typeList->remove(typesInfoIndex);
     updateTable();
     clearForm();
     tableWidget->blockSignals(false);
}


// #brief Fills text fields with selected type properties or clears them when none selected.
//
// #return No return value
void TypesWidget::selectionChanged(void)
{
     hType* type = getSelectedType();
     
     if (type != NULL)
     {
          removeTypeButton->setEnabled(true);
          if (UNSAVED_INDICATOR == 0)
               saveTypeButton->setEnabled(true);
          
          int baseType = type->baseType();
     
          nameTextField->setText(type->name());
          descriptionTextField->setText(type->description());
          baseTypeComboBox->setCurrentIndex(baseType);
          
          constraintsButton->setEnabled(baseType != BOOLEAN_BASED);
     }
     else
          clearForm();
}

// #brief Registers change made by editing cell content to the model.
//
// #param row - edited cell's row
// #param column - edited cell's column
// #return No return value
void TypesWidget::cellTextContentEdited(int row, int column)
{
     hType* existingType = typeList->at(hpTypesInfo.ptc + row);
     QString enteredText = tableWidget->item(row, column)->text().trimmed();
     
     if ((column == 0) && (enteredText != existingType->name()))
     {
          if (isValidName(enteredText))
               existingType->setName(enteredText);
          else
          {
               displayInvalidNameMessage(enteredText);
               tableWidget->item(row, 0)->setText(existingType->name());
          }
     }   
     else if ((column == 1) && (enteredText != existingType->description()))
          existingType->setDescription(enteredText);
}

// #brief Allows complex type editing from table cells.
//
// #param row - edited cell's row
// #param column - edited cell's column
// #return No return value
void TypesWidget::inCellComplexEdit(int row, int column)
{ 
     if (column == 2)
          baseTypeCellEdit(row);
     else if (column == 3)
          constraintsCellEdit(row);
}

// #brief Handles editing constraints in table cells.
//
// #param row - edited cell's row
// #return No return value
void TypesWidget::constraintsCellEdit(int row)
{
     int basicTypeIndex = hpTypesInfo.ptc + row;
     
     if (typeList->at(basicTypeIndex)->baseType() == BOOLEAN_BASED)
          QMessageBox::information(this, "HQEd", "Boolean-based type cannot have constraints.", QMessageBox::Ok);
     else
          showConstraintsDialog();    
}

// #brief Handles editing base type in table cells.
//
// #param row - edited cell's row
// #return No return value
void TypesWidget::baseTypeCellEdit(int row)
{
     int basicTypeIndex = hpTypesInfo.ptc + row;
     QComboBox* typeComboBox = new QComboBox();
     typeComboBox->setModel(baseTypeComboBox->model());
     typeComboBox->setCurrentIndex(typeList->at(basicTypeIndex)->baseType());
     connect(typeComboBox, SIGNAL(currentIndexChanged(int)), this, SLOT(typeEditedInCell(int)));
     
     tableWidget->removeCellWidget(row, 2);
     tableWidget->setCellWidget(row, 2, typeComboBox);
     
     //showPopup() has a bug - it closes immediately, so it must be done manually:
     QMouseEvent event(QEvent::MouseButtonPress, QPoint(0,0), Qt::LeftButton, Qt::LeftButton, Qt::NoModifier);
     QApplication::sendEvent(typeComboBox, &event);
}

// #brief Allows to edit type's base in cell.
//
// #param selectedTypeIndex - selected base type index
// #return No return value    
void TypesWidget::typeEditedInCell(int selectedTypeIndex)
{
     int tableRow = tableWidget->currentRow();
     typeList->at(hpTypesInfo.ptc + tableRow)->setType(selectedTypeIndex);
     
     if (selectedTypeIndex == BOOLEAN_BASED)
          typeList->at(hpTypesInfo.ptc + tableRow)->setClass(NO_TYPE_CLASS);
     
     QTableWidgetItem* item = new QTableWidgetItem();
     item->setFlags(item->flags() & ~Qt::ItemIsEditable);
     tableWidget->removeCellWidget(tableRow, 2);
     tableWidget->setItem(tableRow, 2, item);
     
     updateTable();
}

// #brief Handles name changing.
//
// #param enteredName - new name
// #return No return value
void TypesWidget::nameChanged(QString enteredName)
{
     enteredName = enteredName.trimmed();
     
     if (isValidName(enteredName))
          addTypeButton->setEnabled(true);
     else
          addTypeButton->setEnabled(false);
     
     if (tableWidget->currentRow() > -1)
     {
          if (getSelectedType()->name() == enteredName)
               removeUnsavedIndicator();
          else
               showUnsavedIndicator();
     }
}

// #brief Checks if entered name contains at least one char and already exists.
//
// #param name - name to valid
// #return True if name is valid and doesn't exist yet
bool TypesWidget::isValidName(QString name)
{ 
     if (name.isEmpty() || (typeList->indexOfName(name) > -1))
          return false;
     else
          return true;
}

// #brief Monitors description changes.
//
// #return No return value
void TypesWidget::validateDescription(void)
{
     hType* selectedType = getSelectedType();
     
     if (selectedType == NULL)
          return;
     
     if (selectedType->description() == descriptionTextField->toPlainText())
          removeUnsavedIndicator();
     else
          showUnsavedIndicator();
}
    
// #brief Reads information about types and fills the preview table with them.
//
// #return No return value
void TypesWidget::updateTable(void)
{
     int rowCount = typeList->size() - hpTypesInfo.ptc;
     tableWidget->setRowCount(rowCount);  
     
     for (int row = 0; row < rowCount; row++)
        fillTableRow(row);
}

// #brief Fills single table row with type info. Type comes from hpTypesInfo, its index is the same as table row (except primitives).
//
// #param rowIndex - index of row to fill and of type to put in its info
// #return No return value  
void TypesWidget::fillTableRow(int rowIndex)
{
     int typesInfoRow = hpTypesInfo.ptc + rowIndex;     
     int primitiveTypeIndex = hpTypesInfo.iopti(typeList->at(typesInfoRow)->baseType());
     QString baseType = hpTypesInfo.ptn.at(primitiveTypeIndex);

     tableWidget->setItem(rowIndex, 0, new QTableWidgetItem(typeList->at(typesInfoRow)->name()));
     tableWidget->setItem(rowIndex, 1, new QTableWidgetItem(typeList->at(typesInfoRow)->description()));
               
     QTableWidgetItem* item = new QTableWidgetItem(baseType);
     item->setFlags(item->flags() & ~Qt::ItemIsEditable);
     tableWidget->setItem(rowIndex, 2, item);
                         
     item = new QTableWidgetItem();
     item->setFlags(item->flags() & ~Qt::ItemIsEditable);
     
     QString domainString;
     if (!makeDomainString(typesInfoRow, &domainString))
     {
          QFont font = item->font();
          font.setItalic(true);
          item->setFont(font);
     }
     item->setText(domainString);
               
     tableWidget->setItem(rowIndex, 3, item);    
}

// #brief Creates string containing domain of selected type.
//
// #param typesInfoRow - index of type in hpTypesInfo
// #param string - pointer to string to put in the domain as text
// #return True if selected type has domain 
bool TypesWidget::makeDomainString(int typesInfoRow, QString* string)
{
     if (typeList->at(typesInfoRow)->typeClass() == TYPE_CLASS_DOMAIN)
     {
          if (typeList->at(typesInfoRow)->domain()->ordered())
               *string = "ordered\n";
          else 
               *string = "not ordered\n";
               
          *string += typeList->at(typesInfoRow)->domain()->toString();
          return true;
     }
     else
     {
          *string = "no constraints";
          return false;
     }
}

// #brief Clears input fields.
//
// #return No return value
void TypesWidget::clearForm(void)
{
     constraintsButton->setEnabled(false);
     addTypeButton->setEnabled(false);
     removeTypeButton->setEnabled(false);
     saveTypeButton->setEnabled(false);
     
     nameTextField->setText("");
     descriptionTextField->setText("");
     baseTypeComboBox->setCurrentIndex(0);
}

// #brief Displays Constraints_ui dialog.
//
// #return No return value
void TypesWidget::showConstraintsDialog(void)
{
     constraintsDialog = new TypeConstraintsDialog(getSelectedType());
     connect(constraintsDialog, SIGNAL(accepted()), this, SLOT(updateTable()));
     constraintsDialog->show();
}

// #brief Shows unsaved changes indicator.
//
// #return No return value
void TypesWidget::showUnsavedIndicator(void)
{
     if (UNSAVED_INDICATOR == 0)
          saveTypeButton->setText("Save*");
     else
          saveTypeButton->setEnabled(true);
}
   
// #brief Removes unsaved changes indicator.
//
// #return No return value
void TypesWidget::removeUnsavedIndicator(void)
{
     if (UNSAVED_INDICATOR == 0)
          saveTypeButton->setText("Save");
     else
          saveTypeButton->setEnabled(false);
}

// #brief Sorts table by column of clicked header.
//
// #param columnIndex - index of clicked column header
// #return No return value
void TypesWidget::sortTableByColumn(int columnIndex)
{
     Qt::SortOrder sortOrder = tableWidget->horizontalHeader()->sortIndicatorOrder();
     
     if (sortOrder != Qt::AscendingOrder) //strange that this works
          sortOrder = Qt::DescendingOrder;
     else
          sortOrder = Qt::AscendingOrder;
     
     tableWidget->sortByColumn(columnIndex, sortOrder);
}
