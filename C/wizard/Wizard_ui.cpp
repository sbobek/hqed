/**
* \file   Wizard_ui.cpp
* \author Opioła, Pasek, Zieliński
* \date   10.05.2012
* \version  1.0
* \brief  This file defines class representing new project wizard.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include "Wizard_ui.h"
#include "TypesWidget.h"
#include "../namespaces/ns_hqed.h"

Wizard_ui* Wizard;

// #brief Constructor of Wizard_ui class.
//
// #param parent Pointer to parent object
Wizard_ui::Wizard_ui(QWidget*)
{
     stepTitles << "Types";
     pages << new TypesWidget;
     
     setupUi(this);
     hqed::centerWindow(this);
     setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); //for Mac not to show maximize
     setFixedSize(width(), height()); //for Linux not to show maximize
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     setAttribute(Qt::WA_DeleteOnClose);
     
     QList<QWizard::WizardButton> layout;
     layout << QWizard::Stretch << QWizard::BackButton << QWizard::NextButton 
               << QWizard::FinishButton << QWizard::CancelButton;
     setButtonLayout(layout);
     setOption(QWizard::HaveFinishButtonOnEarlyPages, true);
     setOption(QWizard::HaveNextButtonOnLastPage, true);
     
     createPages();
}

// #brief Destructor of Wizard_ui class.
Wizard_ui::~Wizard_ui(void)
{
     for (int i = 0; i < pages.size(); i++)
          delete pages.at(i);
     
     pages.clear();
}

// #brief Creates wizard pages.
//
// #return No return value
void Wizard_ui::createPages()
{
     makeStepTitles();

     for (int i = 0; i < pages.size(); i++)
          formPage(i);
}

// #brief Makes step titles list.
//
// #return No return value
void Wizard_ui::makeStepTitles()
{
     QString label;
     
     for (int i = 0; i < stepTitles.count(); i++)
     {
          QString label = QString("  Step ").append(QString::number(i + 1)).append(" - ").append(stepTitles.at(i));
          //white spaces make left margin, CSS doesn't work for it
          fullStepTitles.append(label);
     }
}

 // #brief Forms single page of given index.
 //
 // #param index - index of page to form
 // #return No return value
void Wizard_ui::formPage(int index)
{
     QWizardPage* page = new QWizardPage;
     QHBoxLayout* layout = new QHBoxLayout(page);
     QListWidget* steps = new QListWidget;
     fillStepsList(steps, index);
     
     layout->setContentsMargins(0, 0, 0, 0);
     steps->setMaximumWidth(steps->sizeHintForColumn(0) + 4);    
     layout->addWidget(steps);
     layout->addWidget(pages[index]);
     addPage(page);
     
}

// #brief Fills steps list with step titles.
//
// #param stepsList - pointer to the list
// #param pageIndex - index of quering page
// #return No return value
void Wizard_ui::fillStepsList(QListWidget* stepsList, int pageIndex)
{
     QFont font;
     font.setPointSize(12);
     
     for (int i = 0; i < fullStepTitles.count(); i++)
     {
          QListWidgetItem* qListWidgetItem = new QListWidgetItem(stepsList);
          if (i == pageIndex)
          {
               QBrush systemHighlightBrush = QApplication::palette().brush(QPalette::Highlight);
               qListWidgetItem->setBackground(systemHighlightBrush);
          }
          qListWidgetItem->setFont(font);
          qListWidgetItem->setText(fullStepTitles.at(i));
          qListWidgetItem->setFlags(Qt::ItemIsUserCheckable|Qt::ItemIsEnabled);
     }
     stepsList->setFocusPolicy(Qt::NoFocus);
     stepsList->setStyleSheet("QListWidget::item { margin: 10px }");
}
