 /**
* \file   TypeConstraintsDialog.h
* \author Mateusz Zieliński
* \date   20.12.2012
* \version  1.0
* \brief  This file defines class representing type constraints edit dialog.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef TYPECONSTRAINTSDIALOG_H
#define TYPECONSTRAINTSDIALOG_H

#include <QDialog>
#include "ui_TypeConstraintsDialog.h"
#include "XTT/SetEditor_ui.h"
#include "../M/hDomain.h"

/**
* \class  TypeConstraintsDialog
* \author Mateusz Zieliński
* \date   20.12.2012
* \brief  This class represents type constraints edit dialog.
*/

class TypeConstraintsDialog : public QDialog, private Ui_TypeConstraintsDialog
{
     Q_OBJECT

     hType* definedType;                      ///< Pointer to the type being constrainted
     SetEditor_ui* setEditor;                 ///< Pointer to set editor dialog
    
public:
  
     /// \brief Constructor of TypeConstraintsDialog class.
     ///
     /// \param type Pointer to the type being constrainted
     /// \param parent Pointer to parent object
     TypeConstraintsDialog(hType* type, QWidget* parent = 0);
     
     /// \brief Destructor of TypeConstraintsDialog class.
     ~TypeConstraintsDialog(void);
    
protected:
        
     /// \brief Triggered when key is pressed.
     ///
     /// \param event Pointer to event object
     /// \return No return value
     virtual void keyPressEvent(QKeyEvent *event);
    
private:
    
     /// \brief Initializes fields with edited constraints parameters.
     ///
     /// \return No return value
     void initializeFields(void);
         
     /// \brief Validates domain items.
     ///
     /// \return True if all ok, false otherwise
     bool domainItemsValid(void);
     
     /// \brief Displays the domain.
     ///
     /// \return No return value
     void displayDomain(void);
 
private slots:
    
     /// \brief Disables input fields.
     ///
     /// \return No return value
     void disableAllComponents(void);
     
     /// \brief Enables input fields appropriate to base type.
     ///
     /// \return No return value
     void enableComponents(void);
     
     /// \brief Saves changes to edited type.
     ///
     /// \return No return value
     void saveChanges(void);
     
     /// \brief Displays set editor dialog.
     ///
     /// \return No return value
     void showSetEditorDialog(void);
          
     /// \brief Saves changes to defined type's domain.
     ///
     /// \return No return value
     void domainChanged(void);
};

#endif // TYPECONSTRAINTSDIALOG_H
