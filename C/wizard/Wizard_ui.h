/**
* \file   Wizard_ui.h
* \author Opioła, Pasek, Zieliński
* \date   10.05.2012
* \version  1.0
* \brief  This file defines class representing new project wizard.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef WIZARD_UI_H
#define WIZARD_UI_H

#include <QListWidget>
#include <QWizard>
#include "ui_Wizard.h"

/**
* \class 	Wizard_ui
* \author Opioła, Pasek, Zieliński
* \date   10.05.2012
* \brief  This class represents new project wizard.
*/

class Wizard_ui : public QWizard, private Ui_Wizard
{
    Q_OBJECT

public:
    /// \brief Constructor of Wizard_ui class.
    ///
    /// \param parent Pointer to parent object
    Wizard_ui(QWidget *parent = 0);
    
    /// \brief Destructor of Wizard_ui class.
    ~Wizard_ui(void);
    
private:
    QStringList stepTitles;             ///< List of user defined step titles
    QStringList fullStepTitles;         ///< List of displayed step titles with indices
    QList<QWidget*> pages;              ///< List of page widgets
    
    /// \brief Creates wizard pages.
    ///
    /// \return No return value
    void createPages();
    
    /// \brief Makes step titles list.
    ///
    /// \return No return value
    void makeStepTitles();
    
    /// \brief Forms single page of given index.
    ///
    /// \param index - index of page to form
    /// \return No return value
    void formPage(int index);
    
    /// \brief Fills steps list with step titles.
    ///
    /// \param stepsList - pointer to the list
    /// \param pageIndex - index of quering page
    /// \return No return value
    void fillStepsList(QListWidget* stepsList, int pageIndex);
};

extern Wizard_ui* Wizard;

#endif // WIZARD_UI_H
