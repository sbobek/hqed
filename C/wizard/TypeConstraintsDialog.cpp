 /**
* \file   TypeConstraintsDialog.cpp
* \author Mateusz Zieliński
* \date   20.12.2012
* \version  1.0
* \brief  This file defines class representing type constraints edit dialog.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include <QKeyEvent>
#include <QMessageBox>
#include "TypeConstraintsDialog.h"
#include "../namespaces/ns_hqed.h"
#include "../M/hType.h"
#include "../M/hTypeList.h"
#include "../M/hSetItem.h"
  
// #brief Constructor of TypeConstraintsDialog class.
//
// #param type Pointer to the type being constrainted
// #param parent Pointer to parent object
TypeConstraintsDialog::TypeConstraintsDialog(hType* type, QWidget*) : definedType(type)
{ 
     setupUi(this);
     setModal(true);
     setLayout(verticalLayout_3);
     hqed::centerWindow(this);
     setAttribute(Qt::WA_DeleteOnClose);

     connect(noConstraintsRadioButton, SIGNAL(clicked()), this, SLOT(disableAllComponents())); 
     connect(domainRadioButton, SIGNAL(clicked()), this, SLOT(enableComponents()));
     connect(editPushButton, SIGNAL(clicked()), this, SLOT(showSetEditorDialog()));
     connect(okCancelButtonBox, SIGNAL(accepted()), this, SLOT(saveChanges()));
        
     disableAllComponents();
     initializeFields();
}
    
// #brief Destructor of TypeConstraintsDialog class.
TypeConstraintsDialog::~TypeConstraintsDialog(void)
{
}

// #brief Triggered when key is pressed.
//
// #param event Pointer to event object
// #return No return value
void TypeConstraintsDialog::keyPressEvent(QKeyEvent *event)
{
     switch(event->key())
     {
          case Qt::Key_Return:
               okCancelButtonBox->button(QDialogButtonBox::Ok)->click();
               break;
          case Qt::Key_Escape:
               okCancelButtonBox->button(QDialogButtonBox::Cancel)->click();
               break;
          default:
               QWidget::keyPressEvent(event);
     }
}

// #brief Disables input fields.
//
// #return No return value
void TypeConstraintsDialog::disableAllComponents(void)
{
     parametersGroupBox->setEnabled(false);
     domainGroupBox->setEnabled(false);
}
    
// #brief Enables input fields appropriate to base type.
//
// #return No return value
void TypeConstraintsDialog::enableComponents(void)
{
     if (definedType->baseType() == SYMBOLIC_BASED)
          definedType->setClass(TYPE_CLASS_DOMAIN);
    
     parametersGroupBox->setEnabled(true);
     lengthLabel->setEnabled(false);
     lengthSpinBox->setEnabled(false);
     scaleLabel->setEnabled(false);
     scaleSpinBox->setEnabled(false);
     orderedCheckBox->setEnabled(false);
    
     switch(definedType->baseType())
     {
          case NUMERIC_BASED:
               scaleLabel->setEnabled(true);
               scaleSpinBox->setEnabled(true);
          case INTEGER_BASED:
               lengthLabel->setEnabled(true);
               lengthSpinBox->setEnabled(true);
               orderedCheckBox->setCheckState(Qt::Checked);
               break;
          case SYMBOLIC_BASED:
               orderedCheckBox->setEnabled(true);
     }
    
     domainGroupBox->setEnabled(true);
}

// #brief Initializes fields with edited constraints parameters.
//
// #return No return value.
void TypeConstraintsDialog::initializeFields(void)
{
     if (definedType->typeClass() == NO_TYPE_CLASS)
          noConstraintsRadioButton->setChecked(true);
     else
     {
          domainRadioButton->click();
          lengthSpinBox->setValue(definedType->length());
          scaleSpinBox->setValue(definedType->scale());
          orderedCheckBox->setChecked(definedType->domain()->ordered());
          displayDomain();
     }
}

// #brief Saves changes to edited type.
//
// #return No return value
void TypeConstraintsDialog::saveChanges(void)
{    
     if (domainRadioButton->isChecked())
     {
          definedType->setClass(TYPE_CLASS_DOMAIN);
          definedType->setLength(lengthSpinBox->value());
          definedType->setScale(scaleSpinBox->value());
          definedType->domain()->setOrdered(orderedCheckBox->isChecked());
     }
     else
          definedType->setClass(NO_TYPE_CLASS);
     
     emit(accepted());   
}

// #brief Displays set editor dialog.
//
// #return No return value
void TypeConstraintsDialog::showSetEditorDialog(void)
{     
     if (!domainItemsValid())
     {
          QMessageBox::critical(NULL, "HQEd", "Current type is incorrect. Check primitive type and constraints values.", QMessageBox::Ok);
          return;
     }
     
     if ((definedType->baseType() == SYMBOLIC_BASED) && definedType->domain()->ordered() && !definedType->domain()->updateItemsNumbering())
     {
          QMessageBox::critical(NULL, "HQEd", definedType->domain()->lastError(), QMessageBox::Ok);
          return;
     }
     
     setEditor = new SetEditor_ui;    
     setEditor->setAttribute(Qt::WA_DeleteOnClose);
     connect(setEditor, SIGNAL(okClicked()), this, SLOT(domainChanged()));
     int setModeResult = setEditor->setMode(definedType, definedType->domain(), 1, false, false, NULL);
     
     if (setModeResult == SET_WINDOW_MODE_FAIL)
     {
          QMessageBox::critical(NULL, "HQEd", "Current mode is incorrect.", QMessageBox::Ok);
          return;
     }
     else if (setModeResult == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
          return;
          
     setEditor->show();
}

// #brief Validates domain items.
//
// #return True if all ok, false otherwise
bool TypeConstraintsDialog::domainItemsValid(void)
{
     if ( (definedType->typeClass() == TYPE_CLASS_DOMAIN) && ( (definedType->length() != lengthSpinBox->value()) || (definedType->scale() != scaleSpinBox->value()) ) )
     {
          for (int i = 0; i < definedType->domain()->size(); ++i)
               if (definedType->isItemCorrect(definedType->domain()->at(i), false) != H_TYPE_ERROR_NO_ERROR)
               {
                    QMessageBox::critical(NULL, "HQEd", "The domain item: " + definedType->domain()->at(i)->toString() + " is incorrect.\nError message: " + definedType->lastError(), QMessageBox::Ok);
                    return false;
               }
     }
     return true;
}

// #brief Saves changes to defined type's domain.
//
// #return No return value
void TypeConstraintsDialog::domainChanged(void)
{
     hSet* result = setEditor->result();
     hSetItem* item;
     
     definedType->domain()->flush();
     for (int i = 0; i < result->size(); ++i)
     {
          item = result->at(i)->copyTo();
          if (definedType->domain()->add(item) != H_SET_ERROR_NO_ERROR)
               QMessageBox::information(NULL, "HQEd", "Error adding element: " + item->toString() + "\Error message: " + definedType->domain()->lastError(), QMessageBox::Ok);
     }
     definedType->domain()->setParentType(definedType);
     
     if ((definedType->baseType() == SYMBOLIC_BASED) && (definedType->domain()->ordered()))
     {
          if (!definedType->domain()->updateNumbering())
          {
               QMessageBox::critical(NULL, "HQEd", definedType->domain()->lastError(), QMessageBox::Ok);
               return;
          }
     }

     delete result;
     displayDomain();
}

// #brief Displays the domain.
//
// #return No return value
void TypeConstraintsDialog::displayDomain(void)
{
     domainListWidget->clear();
     domainListWidget->addItems(definedType->domain()->stringItems());
}