/**
* \file   TypesWidget.h
* \author Mateusz Zieliński
* \date   20.03.2013
* \version 1.0
* \brief  This file defines class representing widget aloowing to create and edit types.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef TYPESWIDGET_UI_H
#define TYPESWIDGET_UI_H

#include <QWidget>
#include "ui_TypesWidget.h"
#include "Wizard_ui.h"
#include "TypeConstraintsDialog.h"

/**
* \class  TypesWidget
* \author Mateusz Zieliński
* \date   20.03.2013
* \brief  This class represents the types creating step of new project wizard.
*/

class TypesWidget : public QWidget, private Ui_TypesWidget
{
    Q_OBJECT

public:
     /// \brief Constructor of TypesWidget class.
     ///
     /// \param parent - pointer to parent object
     TypesWidget(QWidget* parent = 0);
     
     /// \brief Destructor of TypesWidget class.
     ~TypesWidget(void);
    
private:
     TypeConstraintsDialog* constraintsDialog;       ///< Pointer to constraints dialog
     
     /// \brief Maps type selected in table to the existing type.
     ///
     /// \return Pointer to type selected in table
     hType* getSelectedType();
           
     /// \brief Fills single table row with type info. Type comes from hpTypesInfo, its index is the same as table row (except primitives).
     ///
     /// \param rowIndex - index of row to fill and of type to put in its info
     /// \return No return value    
     void fillTableRow(int rowIndex);
     
     /// \brief Creates string containing domain of selected type.
     ///
     /// \param typesInfoRow - index of type in hpTypesInfo
     /// \param string - pointer to string to put in the domain as text
     /// \return True if selected type has domain 
     bool makeDomainString(int typesInfoRow, QString* string);
     
     /// \brief Checks if entered name contains at least one char and already exists.
     ///
     /// \param name - name to valid
     /// \return True if name is valid and doesn't exist yet
     bool isValidName(QString name);
     
     /// \brief Displays message box with info why selected name is invalid.
     ///
     /// \param name - selected name
     /// \return No return value
     void displayInvalidNameMessage(QString name);
     
     /// \brief Handles editing constraints in table cells.
     ///
     /// \param row - edited cell's row
     /// \return No return value
     void constraintsCellEdit(int row);
     
     /// \brief Handles editing base type in table cells.
     ///  
     /// \param row - edited cell's row
     /// \return No return value
     void baseTypeCellEdit(int row);

     /// \brief Shows unsaved changes indicator.
     ///
     /// \return No return value
     void showUnsavedIndicator(void);
     
     /// \brief Removes unsaved changes indicator.
     ///
     /// \return No return value
     void removeUnsavedIndicator(void);    
    
private slots:
    
     /// \brief Clears input fields.
     ///
     /// \return No return value
     void clearForm(void);
     
     /// \brief Enables or disables constraints button depending on the chosen type's base.
     ///
     /// \param baseTypeIndex - index of chosen type
     /// \return No return value
     void baseTypeChanged(int baseTypeIndex);
     
     /// \brief Adds new type based on form data.
     ///
     /// \return No return value
     void addTypeClicked(void);
     
     /// \brief Saves existing type.
     ///
     /// \return No return value
     void saveTypeClicked(void);
     
     /// \brief Removes existing type.
     ///
     /// \return No return value
     void removeTypeClicked(void);
     
     /// \brief Reads information about types and fills the preview table with them.
     ///
     /// \return No return value
     void updateTable(void);
     
     /// \brief Fills text fields with selected type properties or clears them when none selected.
     ///
     /// \return No return value
     void selectionChanged(void);
     
     /// \brief Allows complex type editing from table cells.
     ///
     /// \param row - edited cell's row
     /// \param column - edited cell's column
     /// \return No return value
     void inCellComplexEdit(int row, int column);
     
     /// \brief Allows to edit type's base in cell.
     ///
     /// \param selectedTypeIndex - selected base type index
     /// \return No return value    
     void typeEditedInCell(int selectedTypeIndex);
     
     /// \brief Registers change made by editing cell content to the model.
     ///
     /// \param row - edited cell's row
     /// \param column - edited cell's column
     /// \return No return value
     void cellTextContentEdited(int row, int column);
     
     /// \brief Handles name changing.
     ///
     /// \param enteredName - new name
     /// \return No return value
     void nameChanged(QString enteredName);
     
     /// \brief Monitors description changes.
     ///
     /// \return No return value
     void validateDescription(void);
     
     /// \brief Displays Constraints_ui dialog.
     ///
     /// \return No return value
     void showConstraintsDialog(void);
     
     /// \brief Sorts table by column of clicked header.
     ///
     /// \param columnIndex - index of clicked column header
     /// \return No return value
     void sortTableByColumn(int columnIndex);
};

#endif // TYPESWIDGET_UI_H
