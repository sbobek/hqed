/*
 *	  $Id: devIODeviceSingalHandler.cpp,v 1.1.2.1 2011-08-05 14:02:49 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QHostAddress>
#include <QFile>
#include <QBuffer>
#include <QUdpSocket>
#include <QTcpSocket>

#include "devIODeviceSingalHandler.h"
//---------------------------------------------------------------------------

// #brief Construcotr of the devIODeviceSingalHandler class
//
// #param __objectType - Defines the type of the object that derivates this class (ex. server, plugin)
devIODeviceSingalHandler::devIODeviceSingalHandler(QString __objectType)
{
     _objectType = __objectType;
}
//---------------------------------------------------------------------------

// #brief Destructor of the devIODeviceSingalHandler class
devIODeviceSingalHandler::~devIODeviceSingalHandler(void)
{
}
//---------------------------------------------------------------------------

// #brief Returns the type of the instance
//
// #return the type of the instance
QString devIODeviceSingalHandler::objectType(void)
{
     return _objectType;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new last error message
//
// #param __lastError - new error message
// #param __errorType - type of the error. Defined types are: DEV_PLUGIN_MESSAGE_TYPE_CONNECTION, DEV_PLUGIN_MESSAGE_TYPE_FORMAT, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE
// #return error code
int devIODeviceSingalHandler::setLastError(QString __lastError, int /*__errorType*/)
{
     _lastError = __lastError;
     createEvent(0, _lastError);
     
     return DEV_RESULT_CODE_ERROR;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new name for the plugin
//
// #param __name - new name of the plugin
// #return no values return
void devIODeviceSingalHandler::setName(QString __name)
{
     createEvent(2, "Rename " + _objectType + " from '" + _name + "' to '" + __name + "'.");
     _name = __name;
}
//---------------------------------------------------------------------------

// #brief Returns the last error message
//
// #return the last error message as string
QString devIODeviceSingalHandler::lastError(void)
{
     return _lastError;
}
//---------------------------------------------------------------------------

// #brief Returns name of the plugin
//
// #return name of the plugin as string
QString devIODeviceSingalHandler::name(void)
{
     return _name;
}
//---------------------------------------------------------------------------

// #brief Function that emits eventListnerEvent signal
//
// #param __eventType - 0 - error, 1 - warning, 2 - information, 3 - spacer
// #param __message - event message
// #return no values return
void devIODeviceSingalHandler::createEvent(int __eventType, QString __message)
{
     QString msg = __message;
     if(__eventType != 3)
          msg = name() + ": " + msg;
     emit eventListnerEvent(__eventType, msg);
}
//---------------------------------------------------------------------------

// #brief Function that returns the String representation of socket error
//
// #param __socketError - the error code
// #return String representation of socket error
QString devIODeviceSingalHandler::error2String(QAbstractSocket::SocketError __socketError)
{
     QString msg;
     QString msgs[] = {"The connection was refused by the peer (or timed out).",
                       "The remote host closed the connection.",
                       "The host address was not found.",
                       "The socket operation failed because the application lacked the required privileges.",
                       "The local system ran out of resources (e.g., too many sockets).",
                       "The socket operation timed out.",
                       "The datagram was larger than the operating system's limit (which can be as low as 8192 bytes).",
                       "An error occurred with the network (e.g., the network cable was accidentally plugged out).",
                       "The address specified is already in use and was set to be exclusive.",
                       "The address specified does not belong to the host.",
                       "The requested socket operation is not supported by the local operating system (e.g., lack of IPv6 support).",
                       "The socket is using a proxy, and the proxy requires authentication.",
                       "An unidentified error occurred."};
                       
     if(__socketError == QAbstractSocket::ConnectionRefusedError)
          msg = msgs[0];
     if(__socketError == QAbstractSocket::RemoteHostClosedError)
          msg = msgs[1];
     if(__socketError == QAbstractSocket::HostNotFoundError)
          msg = msgs[2];
     if(__socketError == QAbstractSocket::SocketAccessError)
          msg = msgs[3];
     if(__socketError == QAbstractSocket::SocketResourceError)
          msg = msgs[4];
     if(__socketError == QAbstractSocket::SocketTimeoutError)
          msg = msgs[5];
     if(__socketError == QAbstractSocket::DatagramTooLargeError)
          msg = msgs[6];
     if(__socketError == QAbstractSocket::NetworkError)
          msg = msgs[7];
     if(__socketError == QAbstractSocket::AddressInUseError)
          msg = msgs[8];
     if(__socketError == QAbstractSocket::SocketAddressNotAvailableError)
          msg = msgs[9];
     if(__socketError == QAbstractSocket::UnsupportedSocketOperationError)
          msg = msgs[10];
#if QT_VERSION >= 0x040300
     if(__socketError == QAbstractSocket::ProxyAuthenticationRequiredError)
          msg = msgs[11];
#endif
     if(__socketError == QAbstractSocket::UnknownSocketError)
          msg = msgs[12];

     createEvent(0, msg);
     return msg;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the String representation of socket error
//
// #param __socketState - the error code
// #return String representation of socket error
QString devIODeviceSingalHandler::state2String(QAbstractSocket::SocketState __socketState)
{
     QString msg;
     QString msgs[] = { "Server is not connected.",
                        "HQEd is performing a host name lookup.",
                        "HQEd has started establishing a connection.",
                        "A connection is established.",
                        "The socket is bound to an address and port (for servers).",
                        "The socket is about to close (data may still be waiting to be written)."};
                       
     if(__socketState == QAbstractSocket::UnconnectedState)
          msg = msgs[0];
     if(__socketState == QAbstractSocket::HostLookupState)
          msg = msgs[1];
     if(__socketState == QAbstractSocket::ConnectingState)
          msg = msgs[2];
     if(__socketState == QAbstractSocket::ConnectedState)
          msg = msgs[3];
     if(__socketState == QAbstractSocket::BoundState)
          msg = msgs[4];
     if(__socketState == QAbstractSocket::ClosingState)
          msg = msgs[5];

     createEvent(2, msg);
     return msg;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the String representation of process error
//
// #param __processError - the error code
// #return String representation of process error
QString devIODeviceSingalHandler::error2String(QProcess::ProcessError __processError)
{
     QString msg;
     QString msgs[] = { "The server process failed to start. Either the invoked program is missing, or you may have insufficient permissions to invoke the program.",
                        "The server process crashed some time after starting successfully.",
                        "An error occurred when attempting to write to the process. For example, the process may not be running, or it may have closed its input channel.",
                        "An error occurred when attempting to read from the process. For example, the process may not be running.",
                        "An unknown error occurred."};
                        
     if(__processError == QProcess::FailedToStart)
          msg = msgs[0];
     if(__processError == QProcess::Crashed)
          msg = msgs[1];
     if(__processError == QProcess::WriteError)
          msg = msgs[2];
     if(__processError == QProcess::ReadError)
          msg = msgs[3];
     if(__processError == QProcess::UnknownError)
          msg = msgs[4];
          
     createEvent(0, msg);
     return msg;
}
// -----------------------------------------------------------------------------

// #brief Function that returns the String representation of process state
//
// #param __processState - the error code
// #return String representation of process state
QString devIODeviceSingalHandler::state2String(QProcess::ProcessState __processState)
{
     QString msg;
     QString msgs[] = { "The process is not running.",
                        "The process is starting, but the program has not yet been invoked.",
                        "The process is running and is ready for reading and writing."};
                        
     if(__processState == QProcess::NotRunning)
          msg = msgs[0];
     if(__processState == QProcess::Starting)
          msg = msgs[1];
     if(__processState == QProcess::Running)
          msg = msgs[2];
          
     createEvent(2, msg);
     return msg;
}
// -----------------------------------------------------------------------------
