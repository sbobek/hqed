/*
 *	  $Id: devPluginController.cpp,v 1.10 2010-01-08 19:47:18 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QByteArray>
#include <QTime>
#include <QApplication>

#include "../../namespaces/ns_hqed.h"

#include "devPlugin.h"
#include "devPluginController.h"
//---------------------------------------------------------------------------

devPluginController* devPC;
//---------------------------------------------------------------------------

// #brief Construcotr of the hPluginController class
devPluginController::devPluginController(void)
{
     qsrand((uint)QTime::currentTime().msec());
     _plugins = new QList<devPlugin*>;
     _instanceIdentifier = hqed::hqedVersion().toLower() + "_" + QString::number(qrand());
     _pd = NULL;
}
//---------------------------------------------------------------------------

// #brief Destructor of the hPluginController class
devPluginController::~devPluginController(void)
{
     while(_plugins->size())
          delete _plugins->takeFirst();
          
     if(_pd != NULL)
          delete _pd;
}
//---------------------------------------------------------------------------

// #brief Function that connects signals from plugin to this object. The signals are used to displaying progress.
//
// #param __plugin - pointer to the plugin
// #return no values returns
void devPluginController::connectPluginToProgressDlg(devPlugin* __plugin)
{
     // connecting signals
     connect(__plugin, SIGNAL(setOperationTitle(QString)), this, SLOT(setTitle(QString)));
     connect(__plugin, SIGNAL(setMaximumSteps(int)), this, SLOT(setSteps(int)));
     connect(__plugin, SIGNAL(onProgress(void)), this, SLOT(onProgress(void)));
}
//---------------------------------------------------------------------------

// #brief Function that disconnects all signals from plugin to this object.
//
// #param __plugin - pointer to the plugin
// #return no values returns
void devPluginController::disconnectPluginFromProgressDlg(devPlugin* __plugin)
{
     // disconnecting signals
     __plugin->disconnect(this, SLOT(setTitle(QString)));
     __plugin->disconnect(this, SLOT(setSteps(int)));
     __plugin->disconnect(this, SLOT(onProgress(void)));
}
//---------------------------------------------------------------------------

// #brief Returns pointer to the list of registered plugins
//
// #return pointer to the list of registered plugins
QList<devPlugin*>* devPluginController::plugins(void)
{
     return _plugins;
}
//---------------------------------------------------------------------------

// #brief Returns pointer to the list of plugins that support the given protocol modules
//
// #param __supportedModules - indicates the modules that the plugin must support
// #param __onlyEnabled - indicates if the plugin must be enabled
// #return list of plugins that support the given protocol modules
QList<devPlugin*>* devPluginController::pluginsFilter(int __supportedModules, bool __onlyEnabled)
{
     QList<devPlugin*>* result = new QList<devPlugin*>;
     for(int i=0;i<_plugins->size();++i)
          if(((!__onlyEnabled) || (_plugins->at(i)->isEnabled())) && ((_plugins->at(i)->configuration() & __supportedModules) != 0))
               result->append(_plugins->at(i));
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Searches for plugin that use the __device to communication
//
// #param __device - pointer to the device
// #return index of the plugin object
int devPluginController::indexOfDevice(QIODevice* __device)
{
     for(int i=0;i<_plugins->size();++i)
          if(_plugins->at(i)->device() == __device)
               return i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Searches for plugin that use the __device to communication
//
// #param __device - pointer to the device
// #param __ignoreindex - the index that is ignored while searching
// #return index of the plugin object
int devPluginController::indexOfName(QString __name, int __ignoreindex)
{
     for(int i=0;i<_plugins->size();++i)
          if((_plugins->at(i)->name() == __name) && (i != __ignoreindex))
               return i;
               
     return -1;
}
//---------------------------------------------------------------------------

// #brief Function that creates new plugin and appends it to the list
//
// #param __name - name of the plugin
// #return pointer to the new plugin
devPlugin* devPluginController::createNewPlugin(QString __name)
{
     if(indexOfName(__name) > -1)
          return NULL;
          
     devPlugin* np = new devPlugin(this, NULL, __name);
     np->setName(__name);
     _plugins->append(np);
     
     return np;
}
//---------------------------------------------------------------------------

// #brief Function that remove plugin with the given name
//
// #param __name - name of the plugin
// #return no values returns
void devPluginController::removePlugin(QString __name)
{
     removePlugin(indexOfName(__name));
}
//---------------------------------------------------------------------------

// #brief Function that remove plugin with the given index
//
// #param __index - index of the plugin
// #return no values returns
void devPluginController::removePlugin(int __index)
{
     if((__index < 0) || (__index >= _plugins->size()))
          return;
          
     delete _plugins->takeAt(__index);
}
//---------------------------------------------------------------------------

// #brief Function that returns if the table objects are correct on the logical level
//
// #param __schema - title of verified table
// #param __error_message - the reference to the error message(s)
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int devPluginController::logicalTableVerification(QString __schema, QString& _error_message)
{
     progressDlgInit();
     int tmpresult, result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
     for(int i=0;i<_plugins->size();++i)
     {
          if(((_plugins->at(i)->configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0) || (!_plugins->at(i)->isEnabled()))
               continue;

          connectPluginToProgressDlg(_plugins->at(i));
          _error_message += _plugins->at(i)->tableVerification(__schema, tmpresult);
          disconnectPluginFromProgressDlg(_plugins->at(i));
          
          if(tmpresult == DEV_PLUGIN_RESULT_CODE_ERROR)
               result = tmpresult;
     }
     progressDlgDestroy();
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Function that returns if the model is correct on the logical level
//
// #param __error_message - the reference to the error message(s)
// #return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
int devPluginController::logicalFullVerification(QString& _error_message)
{
     progressDlgInit();
     int tmpresult, result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
     for(int i=0;i<_plugins->size();++i)
     {
          if(((_plugins->at(i)->configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0) || (!_plugins->at(i)->isEnabled()))
               continue;

          connectPluginToProgressDlg(_plugins->at(i));
          _error_message += _plugins->at(i)->fullVerification(tmpresult);
          disconnectPluginFromProgressDlg(_plugins->at(i));
          
          if(tmpresult == DEV_PLUGIN_RESULT_CODE_ERROR)
               result = tmpresult;
     }
     progressDlgDestroy();
     
     return result;
}
//---------------------------------------------------------------------------

// #brief Function triggered when plugin device has been destroyed
//
// #param __plugin - pointer to the plugin object that contains destroyed device
// #return no values return
void devPluginController::onPluginDestroy(devPlugin* __plugin)
{
     int io = _plugins->indexOf(__plugin);
     if(io == -1)
          return;
          
     delete _plugins->takeAt(io);
}
//---------------------------------------------------------------------------

// #brief Function that returns the instance identifier of the application. This identifier is used for client recognition in case when thaere is more than one hqed connected to server.
//
// #return the instance identisier of the application
QString devPluginController::instanceIdentifier(void)
{
     return _instanceIdentifier;
}
//---------------------------------------------------------------------------

// #brief Function triggered when plugin dialog is initialized
//
// #return no values return
void devPluginController::progressDlgInit(void)
{
     progressDlgDestroy();
     _pd = new QProgressDialog("", "", 0, 1, NULL);
     _pd->setWindowModality(Qt::ApplicationModal);
     _pd->setCancelButton(NULL);
     QApplication::processEvents();
}
//---------------------------------------------------------------------------

// #brief Function triggered when number steps of progress changes
//
// #param __steps - new value of steps
// #return no values return
void devPluginController::setSteps(int __steps)
{
     if(_pd == NULL)
          return;
          
     _pd->setMaximum(__steps);
     QApplication::processEvents();
}
//---------------------------------------------------------------------------

// #brief Function triggered when title of progress changes
//
// #param __title - new title
// #return no values return
void devPluginController::setTitle(QString __title)
{
     if(_pd == NULL)
          return;
          
     if(!_pd->isVisible())
          _pd->show();
          
     _pd->setLabelText(__title);
     QApplication::processEvents();
}
//---------------------------------------------------------------------------

// #brief Function triggered when progress of progress dialog changes
//
// #return no values return
void devPluginController::onProgress(void)
{
     if(_pd == NULL)
          return;
          
     if(!_pd->isVisible())
          _pd->show();

     _pd->setValue(_pd->value()+1);
     QApplication::processEvents();
}
//---------------------------------------------------------------------------

// #brief Function triggered when plugin dialog is destroyed
//
// #return no values return
void devPluginController::progressDlgDestroy(void)
{
     if(_pd == NULL)
          return;
     
     delete _pd;
     _pd = NULL;
}
//---------------------------------------------------------------------------
