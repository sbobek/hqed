/**
 * \file	devPlugin.h
 * \author      Krzysztof Kaczor kinio4444@gmail.com
 * \date 	25.05.2009 
 * \version	1.0
 * \brief	This file contains class definitioan that represents the single HQEd plugin
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HDEVPLUGIN
#define HDEVPLUGIN
//---------------------------------------------------------------------------

#include <QList>
#include <QMenu>
#include <QObject>
#include <QIODevice>
#include <QProcess>
#include <QAction>
#include <QAbstractSocket>
//--------------------------------------------------------------------------

#include "devIODeviceSingalHandler.h"
//--------------------------------------------------------------------------

#define DEV_PLUGIN_CONNECTION_TYPE_NETWORK 0x00000001
#define DEV_PLUGIN_CONNECTION_TYPE_PROCESS 0x00000002

#define DEV_PLUGIN_PREFFERED_FORMAT_HMR 0x00000010
#define DEV_PLUGIN_PREFFERED_FORMAT_HML 0x00000020

#define DEV_PLUGIN_SUPPORTED_MODULES_V 0x00010000
#define DEV_PLUGIN_SUPPORTED_MODULES_P 0x00020000
#define DEV_PLUGIN_SUPPORTED_MODULES_S 0x00040000
#define DEV_PLUGIN_SUPPORTED_MODULES_T 0x00080000
#define DEV_PLUGIN_SUPPORTED_MODULES_ALL (DEV_PLUGIN_SUPPORTED_MODULES_V|DEV_PLUGIN_SUPPORTED_MODULES_P|DEV_PLUGIN_SUPPORTED_MODULES_S|DEV_PLUGIN_SUPPORTED_MODULES_T)

#define DEV_PLUGIN_SOCKET_TYPE_TCP 0x01000000
#define DEV_PLUGIN_SOCKET_TYPE_UDP 0x02000000

#define DEV_PLUGIN_RESULT_CODE_SUCCESS  0
#define DEV_PLUGIN_RESULT_CODE_ERROR    1

#define DEV_PLUGIN_MESSAGE_TYPE_CONNECTION      0x00000100
#define DEV_PLUGIN_MESSAGE_TYPE_FORMAT          0x00000200
#define DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE 0x00000400
#define DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG   0x00000800
#define DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS   0x00001000

#define DEV_PLUGIN_VER_TYPE_SUBSUME          1
#define DEV_PLUGIN_VER_TYPE_REDUCTION        2
#define DEV_PLUGIN_VER_TYPE_CONTRADICTION    4
#define DEV_PLUGIN_VER_TYPE_COMPLETENESS     8
//--------------------------------------------------------------------------

class PluginConsole_ui;
class SimulationEditor_ui;
class PluginValuePresentation_ui;
class devPluginController;
class xttSimulationParams;
//--------------------------------------------------------------------------

/**
* \class 	devPlugin
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	25.05.2009 
* \brief 	Class definitioan that represents the single HQEd plugin
*/
class devPlugin : public devIODeviceSingalHandler
{
     Q_OBJECT

private:

     bool _isHelloSent;                      ///< Indicates if before execute any message, hello message should be sent
     
     PluginConsole_ui* _console;             ///< Window console
     SimulationEditor_ui* _simEditor;        ///< Built-in simulation editor window
     PluginValuePresentation_ui* _presDialog;///< Pointer to the value presentation dialog
     QMenu* _menu;                           ///< Pointer to the plugin's menu
     QMenu* _simmenu;                        ///< Pointer to the plugin's simulation menu
     QMenu* _vermenu;                        ///< Pointer to the plugin's verification menu
     QMenu* _prsmenu;                        ///< Pointer to the plugin's presentation menu
     QMenu* _trsmenu;                        ///< Pointer to the plugin's translation menu
     QAction* _sendHelloAction;              ///< Pointer to the action menu button that sends hello message
     QAction* _sendHmrModel;                 ///< Pointer to the action menu button that sends model to the server in hmr format
     QAction* _sendHmlModel;                 ///< Pointer to the action menu button that sends model to the server in hml format
     QAction* _getModels;                    ///< Pointer to the action menu button that get models list from server
     QAction* _delModel;                     ///< Pointer to the action menu button that removes model from the server
     QAction* _runModel;                     ///< Pointer to the action menu button that simulates model
     
     QAction* _verCmplModel;                 ///< Pointer to the action menu button that verifies model w.r.t. completeness
     QAction* _verCntrModel;                 ///< Pointer to the action menu button that verifies model w.r.t. contradiction
     QAction* _verSbsmModel;                 ///< Pointer to the action menu button that verifies model w.r.t. subsumption
     QAction* _verRdcsModel;                 ///< Pointer to the action menu button that verifies model w.r.t. reduction
     QAction* _verModel;                     ///< Pointer to the action menu button that verifies whole model
     
     QAction* _prsDialog;                    ///< Pointer to the action menu button that calls for the value presentation dialog

     QAction* _rdmDialog;                    ///< Pointer to the action menu button that calls for the reading model dialog
     
     QAction* _consoleAction;                ///< Pointer to the action menu button that calls the console window
     
     devPluginController* _parent;           ///< Pointer to the parent controller
     QIODevice* _device;                     ///< Device
     QString _address;                       ///< Address of the connection if network - IP if process - path
     QString _params;                        ///< Process execute parameters
     QStringList _presentationServices;      ///< The list of names of presentation services
     bool _enabled;                          ///< Indicates if the plugin is enabled to automatic use
     int _timeOut;                           ///< Connection time out
     int _portNumber;                        ///< Only in case of network connection
     int _configuration;                     ///< Configuration of the plugin 0xAABBCCDD
     /// \li AA - the type of the plugin:
     /// \li 01 - if socket - TCP socket
     /// \li 02 - if socket - UDP socket
     /// \li 01 - if process - normal process
     
     /// \li BB - supported modules of the protocol
     /// \li 01 - verification
     /// \li 02 - presentation
     /// \li 04 - simulation
     /// \li 08 - translation
     
     /// \li CC - error signalization
     /// \li 01 - connection
     /// \li 02 - parsing message
     /// \li 04 - from plugin
     
     /// \li DD - type of the connection
     /// \li 01 - network connection
     /// \li 02 - connection to the process

public:

     /// \brief Construcotr of the devPlugin class
     ///
     /// \param __parent - pointer to the parent controller
     /// \param __device - pointer to the device
     /// \param __name - name of the plugin
     devPlugin(devPluginController* __parent, QIODevice* __device, QString __name = "");
     
     /// \brief Destructor of the devPlugin class
     ~devPlugin(void);
     
     /// \brief Function that creates menu of the plugin
     ///
     /// \return no values return
     void createMenu(void);
     
     /// \brief Function that returns pointer to the menu
     ///
     /// \return pointer to the menu
     QMenu* menu(void);
     
     /// \brief Function that returns pointer to the simulation menu
     ///
     /// \return pointer to the simulation menu
     QMenu* simmenu(void);
     
     /// \brief Allow to set a new device
     ///
     /// \param __device - pointer to the device
     /// \param __name - name of the plugin
     /// \return no values return
     void setDevice(QIODevice* __device, QString __name = "");
     
     /// \brief Allow to set a new last error message
     ///
     /// \param __lastError - new error message
     /// \param __errorType - type of the error. Defined types are: DEV_PLUGIN_MESSAGE_TYPE_CONNECTION, DEV_PLUGIN_MESSAGE_TYPE_FORMAT, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE
     /// \return error code
     int setLastError(QString __lastError, int __errorType);
     
     /// \brief Allow to set a new name for the plugin
     ///
     /// \param __name - new name of the plugin
     /// \return no values return
     void setName(QString __name);
     
     /// \brief Allow to set a new address of the plugin
     ///
     /// \param __address - new address of the plugin
     /// \return true if address is ok, otherwise false
     bool setAddress(QString __address);
     
     /// \brief Allow to set a parameters of process execute
     ///
     /// \param __params - process execute arameters
     /// \return no values return
     void setParams(QString __params);
     
     /// \brief Allow to set a new port number
     ///
     /// \param __portNumber - new port numebr
     /// \return no values return
     void setPortNumber(int __portNumber);
     
     /// \brief Allow to set a connection timeout
     ///
     /// \param __timeout - new timeout value
     /// \return no values return
     void setTimeout(int __timeout);
     
     /// \brief Allow to set a new configuration of the plugin
     ///
     /// \param __configuration - new configuration of the plugin
     /// \return true if configuration is ok, otherwise false
     bool setConfiguration(int __configuration);
     
     /// \brief Allow to set if the plugin is enabled
     ///
     /// \param __enabled - a new value
     /// \return no values return
     void setEnabled(bool __enabled);
     
     /// \brief Returns pointer to the current device
     ///
     /// \return pointer to the current device
     QIODevice* device(void);
     
     /// \brief Returns addres of the plugin
     ///
     /// \return addres of the plugin. When plugin is a socket-based then this is IP addres and when process-based then this is executable file path
     QString address(void);
     
     /// \brief Returns process execute parameters
     ///
     /// \return process execute parameters as string
     QString params(void);

     /// \brief Returns the name of the preferred format in  which this plugin sends model to the server
     ///
     /// \return the preferred format in  which this plugin sends model to the server
     QString formatName(void);
     
     /// \brief Returns the list of presentation services
     ///
     /// \return list of presentation services
     QStringList& presentationServices(void);
     
     /// \brief Returns the number of the port
     ///
     /// \return the number of the port
     int portNumber(void);
     
     /// \brief Returns the timeout value
     ///
     /// \return the timeout value
     int timeout(void);
     
     /// \brief Returns if the plugin is enabled
     ///
     /// \return if the plugin is enabled
     bool isEnabled(void);
     
     /// \brief Returns the configuration of the plugin
     ///
     /// \return configuration of the plugin
     int configuration(void);
     
     /// \brief Returns the whole configuration space
     ///
     /// \return the whole configuration space
     int confSpace(void);
     
     /// \brief Function that checks if the configuration __conf belongs to the whole configuration space
     ///
     /// \param __conf - input configuration that must be chceked
     /// \return true if yes, otherwise false
     bool checkConfig(int __conf);
     
     /// \brief Function that allow for sending and receive the message to/from the plugin
     ///
     /// \param __text - the message that will be send
     /// \param __result - reference to the value where the result code of function is stored
     /// \return the received message
     QString pluginDataExchange(QString __text, int& __result);
     
     /// \brief Function that parses the message specified with the help of __message.
     ///
     /// \param __message - the message that will be parsed
     /// \param rcode - result code of the function
     /// \return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well
     QStringList parseMessage(QString __message, int& rcode);
     
     /// \brief Function that checks the result stored in responce from server
     ///
     /// \param __response - the message that will be parsed
     /// \param rcode - result code of the function
     /// \return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well.
     QStringList checkResult(QString __response, int& rcode);
     
     /// \brief Function that creates the PROLOG list using given argument
     ///
     /// \param __list - result code of the function
     /// \return string that is a PROLOG-formated list
     QString createList(QStringList __list);
     
     /// \brief Function that returns string representation of the simulation type
     ///
     /// \param __simType - numeric identifier of simulation type
     /// \return string representation of the simulation type
     QString simStrType(int __simType);
     
     /// \brief Function that returns the value that represents all types of verifications. This value is equal to logical OR of defined values DEV_PLUGIN_VER_TYPE_*
     ///
     /// \return value that represents all types of verifications
     int allVerificationTypes(void);
     
     /// \brief Function that executes the simulation
     ///
     /// \param __simParams - reference to the simulation parameters
     /// \return no values return
     void startSimulation(xttSimulationParams& __simParams);
     
     // ----------------------------------------------------------------------------------------------------------
     // Methods that communicates with server
     
     /// \brief Function that sends hello message
     ///
     /// \param _result - result code of the function
     /// \return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
     bool hello(int& _result);
     
     /// \brief Function that sends model int the appropriate format to the server
     ///
     /// \param _result - result code of the function
     /// \return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
     bool sendModel(int& _result);

     /// \brief Function that sends hmr model to the server
     ///
     /// \param __buf - pointer to the buffere where the model is stored
     /// \param _result - result code of the function
     /// \return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
     bool sendHmrModel(QIODevice* __buf, int& _result);

     /// \brief Function that sends hml model to the server
     ///
     /// \param __buf - pointer to the buffere where the model is stored
     /// \param _result - result code of the function
     /// \return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
     bool sendHmlModel(QIODevice* __buf, int& _result);
     
     /// \brief Function that retrives models from server
     ///
     /// \param _result - result code of the function
     /// \return Model list. A single record consist of two elemente of the returned list. First is a username and second model name.
     QStringList getModels(int& _result);
     
     /// \brief Function that removes model from server
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model that we want to remove
     /// \param __username - the owner of the mode that we want to remove
     /// \return true on remove, otherwise false
     bool removeModel(int& _result, QString __modelname, QString __username);

     /// \brief Function that reads model from server
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model that we want to read
     /// \param __username - the owner of the mode that we want to read
     /// \return true on remove, otherwise false
     bool readModel(int& _result, QString __modelname, QString __username);
     
     /// \brief Function that executes model simulation
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __type - type of simulation
     /// \param __tables - list of tables
     /// \param __state - name of the state OR state definition
     /// \return two elelemnt array of list of string lists. The first element of array has the following format:
     /// \li attribute name
     /// \li value_0
     /// \li value_1
     /// \li value_2
     /// \li ...
     /// \li value_n
     /// \li the second element contains only one list of string that has the followin format:
     /// \li table_1 name
     /// \li rule index
     /// \li ...
     /// \li table_n name
     /// \li rule index
     QList<QStringList>* runModel(int& _result, QString __modelname, QString __username, int __type, QStringList __tables, QString __state);
     
     /// \brief function that checks the completeness of the given table in the given model
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __schema - the name of the table that should be checked
     /// \return The list of messages that describe the anomalies of completeness in the given table
     QStringList* checkCompleteness(int& _result, QString __modelname, QString __username, QString __schema);
     
     /// \brief function that checks the subsumptions of the given table in the given model
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __schema - the name of the table that should be checked
     /// \return The list of messages that describe the anomalies of subsumptions in the given table. The list has the following format:
     /// \li at(2n) - rule_id
     /// \li at(2n+1) - description
     /// \li n >= 0
     QStringList* checkSubsumptions(int& _result, QString __modelname, QString __username, QString __schema);
     
     /// \brief function that checks the contradictions of the given table in the given model
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __schema - the name of the table that should be checked
     /// \return The list of messages that describe the anomalies of contradictions in the given table. The list has the following format:
     /// \li at(2n) - rule_id
     /// \li at(2n+1) - description
     /// \li n >= 0
     QStringList* checkContradictions(int& _result, QString __modelname, QString __username, QString __schema);
     
     /// \brief function that checks the reductions of the given table in the given model
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __schema - the name of the table that should be checked
     /// \return The list of messages that describe the anomalies of reductions in the given table. The list has the following format:
     /// \li at(2n) - rule_id
     /// \li at(2n+1) - description
     /// \li n >= 0
     QStringList* checkReductions(int& _result, QString __modelname, QString __username, QString __schema);
     
     /// \brief function that checks the model w.r.t. given anomalies
     ///
     /// \param _result - result code of the function
     /// \param __modelname - the name of the model
     /// \param __username - the name of the model owner user
     /// \param __schema - the name of the table that should be checked
     /// \param __anomaliesTypes - the types of anomalies
     /// \return One string created by hqed::createErrorString function that contains all the infomations about anomalies
     QString checkModel(int& _result, QString __modelname, QString __username, QString __schema, int __anomaliesTypes);
     
     /// \brief function that checks the given table w.r.t. all anomalies
     ///
     /// \param __schema - name of the table that must be checked
     /// \param _result - result code of the function
     /// \return One string created by hqed::createErrorString function that contains all the infomations about anomalies
     QString tableVerification(QString __schema, int& _result);
     
     /// \brief function that checks the model w.r.t. all anomalies
     ///
     /// \param _result - result code of the function
     /// \return One string created by hqed::createErrorString function that contains all the infomations about anomalies
     QString fullVerification(int& _result);
     
     /// \brief Function that executes the value translation
     ///
     /// \param __serviceName - name of the translator
     /// \param __value - value to be translated
     /// \param __result - result code of the function
     /// \return translated value as string
     QString valueTranslation(QString __serviceName, QString __value, int& __result);
     
     // ----------------------------------------------------------------------------------------------------------

public slots:

     /// \brief Function that is triggered when _device object is destroyed
     ///
     /// \return no values return
     void onDestroy(void);
     
     /// \brief Function that is triggered when _console action is clicked
     ///
     /// \return no values return
     void onConsoleClick(void);
     
     /// \brief Function that is triggered when presentation dialog action is clicked
     ///
     /// \return no values return
     void onPresentationDialogClick(void);
     
     /// \brief Function that is triggered when _sendHelloAction action is clicked
     ///
     /// \return no values return
     void onSendHelloClick(void);
     
     /// \brief Function that is triggered when _sendModelAction action is clicked
     ///
     /// \return no values return
     void onSendModelClick(void);
     
     /// \brief Function that is triggered when _getModels action is clicked
     ///
     /// \return no values return
     void onGetModelsClick(void);
     
     /// \brief Function that is triggered when _delModel action is clicked
     ///
     /// \return no values return
     void onDelModelClick(void);

     /// \brief Function that is triggered when _rdmDialog action is clicked
     ///
     /// \return no values return
     void onReadModelDialogClick(void);
     
     /// \brief Function that is triggered when _runModel action is clicked
     ///
     /// \return no values return
     void onRunModelClick(void);
     
     /// \brief Function that is triggered when simulationEditor is closed with start button
     ///
     /// \return no values return
     void onSimEditorStart(void);
     
     /// \brief Function that is triggered when _verModel action is clicked
     ///
     /// \return no values return
     void onVerModelClick(void);
     
     /// \brief Function that is triggered when _verCmplModel action is clicked
     ///
     /// \return no values return
     void onModelCmplVerification(void);
     
     /// \brief Function that is triggered when _verCntrModel action is clicked
     ///
     /// \return no values return
     void onModelCntrVerification(void);
     
     /// \brief Function that is triggered when _verSbsmModel action is clicked
     ///
     /// \return no values return
     void onModelSbsmVerification(void);
     
     /// \brief Function that is triggered when _verRdcsModel action is clicked
     ///
     /// \return no values return
     void onModelRdcsVerification(void);
     
signals:

     /// \brief Signal emitted before action performing
     ///
     /// \param __steps - the number of steps for execute
     /// \return no values return
     void setMaximumSteps(int __steps);
     
     /// \brief Signal emitted when operation type changes
     ///
     /// \param __title - title/desc of operation
     /// \return no values return
     void setOperationTitle(QString __title);
     
     /// \brief Signal emitted on progress
     ///
     /// \return no values return
     void onProgress(void);
};
//---------------------------------------------------------------------------
#endif
