/**
 * \file	devPluginController.h
 * \author      Krzysztof Kaczor kinio4444@gmail.com
 * \date 	25.05.2009 
 * \version	1.0
 * \brief	This file contains class definitioan that represents the controller of the plugins
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HDEVPLUGINCONTROLLER
#define HDEVPLUGINCONTROLLER
//---------------------------------------------------------------------------

#define DEV_PLUGIN_SUCCESS       0
#define DEV_PLUGIN_DEVICE_EXISTS 1
#define DEV_PLUGIN_DEVICE_ERROR  2

#define DEV_STR_ID_FIELD_SEPARATOR           0
#define DEV_STR_ID_INTERNAL_SEPARATOR        1
#define DEV_STR_ID_TRUE_VALUE                2
#define DEV_STR_ID_FALSE_VALUE               3
#define DEV_STR_ID_COMMAND_SEPARATOR         4
#define DEV_STR_ID_QUOTATION_CHARACTER       5
#define DEV_STR_ID_MESSAGE_PARTS_SEPARATOR   6

#define DEV_MNB_RES_SUCCES              0    // On succes
#define DEV_MNB_RES_NOT_EXIST           1    // When brackets not exists
#define DEV_MNB_RES_QUOTATION_OP_ERR    2    // On lack of opening quotation
#define DEV_MNB_RES_QUOTATION_CL_ERR    3    // On lack of closing quotation
#define DEV_MNB_RES_BRACKET_OP_ERR      4    // On lack of opening bracket
#define DEV_MNB_RES_BRACKET_CL_ERR      4    // On lack of closing bracket
//---------------------------------------------------------------------------

#include <QList>
#include <QIODevice>
#include <QObject>
#include <QProgressDialog>

#include "devPlugin.h"
//--------------------------------------------------------------------------

class devPlugin;
//--------------------------------------------------------------------------

/**
* \class 	devluginController
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	25.05.2009 
* \brief 	Class definitioan that represents the controller of the plugins
*/
class devPluginController : public QObject
{
	Q_OBJECT

private:

     QList<devPlugin*>* _plugins;         ///< The list of registered plugins
     QString _instanceIdentifier;         ///< The uniqe identifier of this application instance
     QProgressDialog* _pd;                ///< Pointer to the progress dialog
     
     /// \brief Function that connects signals from plugin to this object. The signals are used to displaying progress.
     ///
     /// \param __plugin - pointer to the plugin
     /// \return no values returns
     void connectPluginToProgressDlg(devPlugin* __plugin);
     
     /// \brief Function that disconnects all signals from plugin to this object.
     ///
     /// \param __plugin - pointer to the plugin
     /// \return no values returns
     void disconnectPluginFromProgressDlg(devPlugin* __plugin);

public:

     /// \brief Construcotr of the hPluginController class
     devPluginController(void);
     
     /// \brief Destructor of the hPluginController class
     ~devPluginController(void);

     /// \brief Returns pointer to the list of registered plugins
     ///
     /// \return pointer to the list of registered plugins
     QList<devPlugin*>* plugins(void);
     
     /// \brief Returns pointer to the list of plugins that support the given protocol modules
     ///
     /// \param __supportedModules - indicates the modules that the plugin must support
     /// \param __onlyEnabled - indicates if the plugin must be enabled
     /// \return list of plugins that support the given protocol modules
     QList<devPlugin*>* pluginsFilter(int __supportedModules = DEV_PLUGIN_SUPPORTED_MODULES_ALL, bool __onlyEnabled = true);

     /// \brief Function that creates new plugin and appends it to the list
     ///
     /// \param __name - name of the plugin
     /// \return pointer to the new plugin
     devPlugin* createNewPlugin(QString __name);
     
     /// \brief Function that remove plugin with the given name
     ///
     /// \param __name - name of the plugin
     /// \return no values returns
     void removePlugin(QString __name);
     
     /// \brief Function that remove plugin with the given index
     ///
     /// \param __index - index of the plugin
     /// \return no values returns
     void removePlugin(int __index);
     
     /// \brief Searches for plugin that use the __device to communication
     ///
     /// \param __device - pointer to the device
     /// \return index of the plugin object
     int indexOfDevice(QIODevice* __device);
     
     /// \brief Searches for plugin
     ///
     /// \param __name - plugin name
     /// \param __ignoreindex - the index that is ignored while searching
     /// \return index of the plugin object
     int indexOfName(QString __name, int __ignoreindex = -1);

     /// \brief Function that returns the instance identifier of the application. This identifier is used for client recognition in case when thaere is more than one hqed connected to server.
     ///
     /// \return the instance identisier of the application
     QString instanceIdentifier(void);
     
     /// \brief Function that returns if the table objects is correct on the logical level
     ///
     /// \param __schema - title of verified table
     /// \param _error_message - the reference to the error message(s)
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalTableVerification(QString __schema, QString& _error_message);
     
     /// \brief Function that returns if the model is correct on the logical level
     ///
     /// \param _error_message - the reference to the error message(s)
     /// \return DEV_PLUGIN_RESULT_CODE_SUCCESS on success or DEV_PLUGIN_RESULT_CODE_ERROR on error
     int logicalFullVerification(QString& _error_message);

public slots:
     
     /// \brief Function triggered when plugin device has been destroyed
     ///
     /// \param __plugin - pointer to the plugin object that contains destroyed device
     /// \return no values return
     void onPluginDestroy(devPlugin* __plugin);
     
     /// \brief Function triggered when plugin dialog is initialized
     ///
     /// \return no values return
     void progressDlgInit(void);
     
     /// \brief Function triggered when number steps of progress changes
     ///
     /// \param __steps - new value of steps
     /// \return no values return
     void setSteps(int __steps);
     
     /// \brief Function triggered when title of progress changes
     ///
     /// \param __title - new title
     /// \return no values return
     void setTitle(QString __title);
     
     /// \brief Function triggered when progress of progress dialog changes
     ///
     /// \return no values return
     void onProgress(void);
     
     /// \brief Function triggered when plugin dialog is destroyed
     ///
     /// \return no values return
     void progressDlgDestroy(void);
};
//---------------------------------------------------------------------------

extern devPluginController* devPC;
//---------------------------------------------------------------------------
#endif
