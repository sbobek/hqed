/**
 * \file	devIODeviceSingalHandler.h
 * \author      Krzysztof Kaczor kinio4444@gmail.com
 * \date 	05.08.2011
 * \version	1.0
 * \brief	This file contains class definitioan that holds the signals of IODevice class
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HDEVIODEVICESIGNALHANDLER
#define HDEVIODEVICESIGNALHANDLER
//---------------------------------------------------------------------------

#include <QList>
#include <QMenu>
#include <QObject>
#include <QIODevice>
#include <QProcess>
#include <QAction>
#include <QAbstractSocket>
//--------------------------------------------------------------------------

#define DEV_RESULT_CODE_SUCCESS  0
#define DEV_RESULT_CODE_ERROR    1
//--------------------------------------------------------------------------

/**
* \class 	devIODeviceSingalHandler
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	05.08.2011
* \brief 	Class definitioan that holds the IODevices singnals
*/
class devIODeviceSingalHandler : public QObject
{
	Q_OBJECT

private:

     QString _lastError;                     ///< Contains the last error message
     QString _name;                          ///< The name of the plugin
     QString _objectType;                    ///< Defines the type of the object that derivates this class (ex. server, plugin)

protected:

     /// \brief Returns the type of the instance
     ///
     /// \return the type of the instance
     QString objectType(void);

public:

     /// \brief Construcotr of the devIODeviceSingalHandler class
     ///
     /// \param __objectType - Defines the type of the object that derivates this class (ex. server, plugin)
     devIODeviceSingalHandler(QString __objectType);
     
     /// \brief Destructor of the devIODeviceSingalHandler class
     ~devIODeviceSingalHandler(void);
     
     /// \brief Allow to set a new last error message
     ///
     /// \param __lastError - new error message
     /// \param __errorType - type of the error. Defined types are: DEV_PLUGIN_MESSAGE_TYPE_CONNECTION, DEV_PLUGIN_MESSAGE_TYPE_FORMAT, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE
     /// \return error code
     virtual int setLastError(QString __lastError, int __errorType);
     
     /// \brief Allow to set a new name for the plugin
     ///
     /// \param __name - new name of the plugin
     /// \return no values return
     virtual void setName(QString __name);
     
     /// \brief Returns the last error message
     ///
     /// \return the last error message as string
     virtual QString lastError(void);
     
     /// \brief Function that emits eventListnerEvent signal
     ///
     /// \param __eventType - 0 - error, 1 - warning, 2 - information, 3 - spacer
     /// \param __message - event message
     /// \return no values return
     virtual void createEvent(int __eventType, QString __message);
     
     /// \brief Returns name of the plugin
     ///
     /// \return name of the plugin as string
     virtual QString name(void);

public slots:

     /// \brief Function that returns the String representation of socket error
     ///
     /// \param __socketError - the error code
     /// \return String representation of socket error
     virtual QString error2String(QAbstractSocket::SocketError __socketError);
     
     /// \brief Function that returns the String representation of socket state
     ///
     /// \param __socketState - the error code
     /// \return String representation of socket state
     virtual QString state2String(QAbstractSocket::SocketState __socketState);
     
     /// \brief Function that returns the String representation of process error
     ///
     /// \param __processError - the error code
     /// \return String representation of process error
     virtual QString error2String(QProcess::ProcessError __processError);
     
     /// \brief Function that returns the String representation of process state
     ///
     /// \param __processState - the error code
     /// \return String representation of process state
     virtual QString state2String(QProcess::ProcessState __processState);
     
signals:

     /// \brief Signal emitted for each event in the plugin
     ///
     /// \param __eventType - 0 - error, 1 - warning, 2 - information
     /// \param __message - event message
     /// \return no values return
     void eventListnerEvent(int __eventType, QString __message);
};
//---------------------------------------------------------------------------
#endif
