/*
 *	  $Id: devPlugin.cpp,v 1.24.4.2.2.5 2011-08-05 14:02:49 kkr Exp $
 *
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#include <QMessageBox>
#include <QHostAddress>
#include <QFile>
#include <QBuffer>
#include <QUdpSocket>
#include <QTcpSocket>

#include "../Settings_ui.h"
#include "../XTT/xttSimulationParams.h"
#include "../XTT/PluginConsole_ui.h"
#include "../XTT/SimulationEditor_ui.h"
#include "../XTT/PluginValuePresentation_ui.h"
#include "../../namespaces/ns_hqed.h"
#include "../../V/vasXML.h"
#include "../../V/vasProlog.h"
#include "../../M/hModel.h"
#include "../../M/hModelList.h"
#include "../../M/XTT/XTT_AttributeGroups.h"
#include "../../M/XTT/XTT_TableList.h"
#include "../../M/XTT/XTT_Table.h"
#include "../../M/XTT/XTT_State.h"
#include "../../M/XTT/XTT_States.h"
#include "../../M/XTT/XTT_Statesgroup.h"

#include "devIODeviceSingalHandler.h"
#include "devPluginController.h"
#include "devPlugin.h"
//---------------------------------------------------------------------------

// #brief Construcotr of the devPlugin class
//
// #param __parent - pointer to the parent controller
// #param __device - pointer to the device
devPlugin::devPlugin(devPluginController* __parent, QIODevice* __device, QString __name) : devIODeviceSingalHandler("plugin")
{
    _device = NULL;
    _parent = __parent;
    _console = new PluginConsole_ui;
    _simEditor = new SimulationEditor_ui;
    _presDialog = new PluginValuePresentation_ui;
    _isHelloSent = false;

    _menu = NULL;
    _simmenu = NULL;
    _vermenu = NULL;
    _prsmenu = NULL;
    _trsmenu = NULL;
    _sendHelloAction = NULL;
    _sendHmrModel = NULL;
    _sendHmlModel = NULL;
    _getModels = NULL;
    _runModel = NULL;
    _verModel = NULL;
    _verCmplModel = NULL;
    _verCntrModel = NULL;
    _verSbsmModel = NULL;
    _verRdcsModel = NULL;
    _consoleAction = NULL;
    _rdmDialog = NULL;

    setDevice(__device, __name);
    createMenu();
}
//---------------------------------------------------------------------------

// #brief Destructor of the devPlugin class
devPlugin::~devPlugin(void)
{
    delete _presDialog;
    delete _simEditor;
    delete _console;
    delete _menu;

    if(_device != NULL)
        disconnect(_device, 0, this, 0);
    _device = NULL;
}
//---------------------------------------------------------------------------

// #brief Function that creates menu of the plugin
//
// #return no values return
void devPlugin::createMenu(void)
{
    if(_menu == NULL)
        _menu = new QMenu(name());
    if(_simmenu == NULL)
        _simmenu = new QMenu(name() + " simulation");
    if(_vermenu == NULL)
        _vermenu = new QMenu("Verification");
    if(_prsmenu == NULL)
        _prsmenu = new QMenu("Presentation");
    if(_trsmenu == NULL)
        _trsmenu = new QMenu("Presentation");
    _menu->clear();
    _simmenu->clear();
    _vermenu->clear();
    _prsmenu->clear();
    _trsmenu->clear();

    _sendHelloAction = _menu->addAction("Send hello");
    _sendHelloAction->setStatusTip("Sends hello message to the server.");
    connect(_sendHelloAction, SIGNAL(triggered()), this, SLOT(onSendHelloClick()));

    if((configuration() & (DEV_PLUGIN_SUPPORTED_MODULES_V | DEV_PLUGIN_SUPPORTED_MODULES_S | DEV_PLUGIN_SUPPORTED_MODULES_T)) > 0)
    {
        if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HMR) > 0)
        {
            _sendHmrModel = _menu->addAction("Send HMR model");
            _sendHmrModel->setStatusTip("Sends HMR model to the server.");
            connect(_sendHmrModel, SIGNAL(triggered()), this, SLOT(onSendModelClick()));
        }
        if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HML) > 0)
        {
            _sendHmlModel = _menu->addAction("Send HML model");
            _sendHmlModel->setStatusTip("Sends HML model to the server.");
            connect(_sendHmlModel, SIGNAL(triggered()), this, SLOT(onSendModelClick()));
        }

        _getModels = _menu->addAction("Get model list");
        _getModels->setStatusTip("Retrieves model list from the server.");
        connect(_getModels, SIGNAL(triggered()), this, SLOT(onGetModelsClick()));

        _delModel = _menu->addAction("Remove model...");
        _delModel->setStatusTip("Removes model from the server.");
        connect(_delModel, SIGNAL(triggered()), this, SLOT(onDelModelClick()));
    }

    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0)
    {
        _runModel = _simmenu->addAction("Simulation...");
        _runModel->setStatusTip("Run simulation of the model.");
        connect(_runModel, SIGNAL(triggered()), this, SLOT(onRunModelClick()));
        _menu->addMenu(_simmenu);
    }

    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) > 0)
    {
        _verCmplModel = _vermenu->addAction("Completeness");
        _verCmplModel->setStatusTip("Checks model for completeness.");
        _verCntrModel = _vermenu->addAction("Contradiction");
        _verCntrModel->setStatusTip("Checks model for contradictions.");
        _verSbsmModel = _vermenu->addAction("Subsumption");
        _verSbsmModel->setStatusTip("Checks model for subsumptions.");
        _verRdcsModel = _vermenu->addAction("Reduce");
        _verRdcsModel->setStatusTip("Checks model for reduce.");
        _vermenu->addSeparator();
        _verModel = _vermenu->addAction("Do them all");
        _verModel->setStatusTip("Checks model for all types of anomalies.");
        _menu->addMenu(_vermenu);

        connect(_verCmplModel, SIGNAL(triggered()), this, SLOT(onModelCmplVerification()));
        connect(_verCntrModel, SIGNAL(triggered()), this, SLOT(onModelCntrVerification()));
        connect(_verSbsmModel, SIGNAL(triggered()), this, SLOT(onModelSbsmVerification()));
        connect(_verRdcsModel, SIGNAL(triggered()), this, SLOT(onModelRdcsVerification()));
        connect(_verModel, SIGNAL(triggered()), this, SLOT(onVerModelClick()));
    }

    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_P) > 0)
    {
        _prsDialog = _prsmenu->addAction("Presentation value...");
        _prsDialog->setStatusTip("Call for the value presentation dialog.");
        connect(_prsDialog, SIGNAL(triggered()), this, SLOT(onPresentationDialogClick()));
        _menu->addMenu(_prsmenu);
    }

    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_T) > 0)
    {
        _rdmDialog = _menu->addAction("Read model...");
        _rdmDialog->setStatusTip("Reads the selected model.");
        connect(_rdmDialog, SIGNAL(triggered()), this, SLOT(onReadModelDialogClick()));
        _menu->addMenu(_trsmenu);
    }

    _menu->addSeparator();

    _consoleAction = _menu->addAction("Show console");
    _consoleAction->setStatusTip("Show console that allow for manual communication with server.");
    connect(_consoleAction, SIGNAL(triggered()), this, SLOT(onConsoleClick()));
}
//---------------------------------------------------------------------------

// #brief Function that returns pointer to the menu
//
// #return pointer to the menu
QMenu* devPlugin::menu(void)
{
    return _menu;
}
//---------------------------------------------------------------------------

// #brief Function that returns pointer to the simulation menu
//
// #return pointer to the simulation menu
QMenu* devPlugin::simmenu(void)
{
    return _simmenu;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new device
//
// #param __device - pointer to the device
// #return no values return
void devPlugin::setDevice(QIODevice* __device, QString __name)
{
    if(__device == NULL)
        return;

    if(_device != NULL)
    {
        disconnect(_device, 0, this, 0);
        delete _device;
    }

    _device = __device;
    setName(__name);

    connect(_device, SIGNAL(destroyed()), this, SLOT(onDestroy()));
}
//---------------------------------------------------------------------------

// #brief Allow to set a new last error message
//
// #param __lastError - new error message
// #param __errorType - type of the error. Defined types are: DEV_PLUGIN_MESSAGE_TYPE_CONNECTION, DEV_PLUGIN_MESSAGE_TYPE_FORMAT, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE
// #return error code
int devPlugin::setLastError(QString __lastError, int __errorType)
{
    devIODeviceSingalHandler::setLastError(__lastError, __errorType);
    if((_configuration & __errorType) > 0)
        QMessageBox::critical(NULL, "HQEd", name() + " plugin error: " + __lastError, QMessageBox::Ok);

    return DEV_PLUGIN_RESULT_CODE_ERROR;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new name for the plugin
//
// #param __name - new name of the plugin
// #return no values return
void devPlugin::setName(QString __name)
{
    createEvent(2, "Rename " + objectType() + " from '" + name() + "' to '" + __name + "'.");
    devIODeviceSingalHandler::setName(__name);
    _menu->setTitle(name());
    _simmenu->setTitle(name() + " simulation");
}
//---------------------------------------------------------------------------

// #brief Allow to set a new address of the plugin
//
// #param __address - new address of the plugin
// #return no values return
bool devPlugin::setAddress(QString __address)
{
    createEvent(2, "Attempt to address change...");

    bool res = false;
    if((configuration() & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0)
    {
        QHostAddress ha;
        res = ha.setAddress(__address);
    }
    else if((configuration() & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
    {
        res = QFile::exists(__address);
    }

    if(!res)
    {
        createEvent(1, "Configuration has not been changed - reason: incorrect server addres: '" + __address + "'.");
        return res;
    }

    _address = __address;
    createEvent(2, "Addres changed to " + _address + ".");

    return true;
}
//---------------------------------------------------------------------------

// #brief Allow to set a parameters of process execute
//
// #param __params - process execute arameters
// #return no values return
void devPlugin::setParams(QString __params)
{
    _params = __params;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new port number
//
// #param __portNumber - new port numebr
// #return no values return
void devPlugin::setPortNumber(int __portNumber)
{
    _portNumber = __portNumber;
}
//---------------------------------------------------------------------------

// #brief Allow to set a connection timeout
//
// #param __timeout - new timeout value
// #return no values return
void devPlugin::setTimeout(int __timeout)
{
    if(__timeout < 0)
        return;

    _timeOut = __timeout;
}
//---------------------------------------------------------------------------

// #brief Allow to set a new configuration of the plugin
//
// #param __configuration - new configuration of the plugin
// #return true if configuration is ok, otherwise false
bool devPlugin::setConfiguration(int __configuration)
{
    if(!checkConfig(__configuration))
    {
        setLastError("Attempt to change configuration failed. The given configuration is out of domain.\nCall plugin event listener to see details.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        createEvent(1, QString("Configuration domian is %1. While the configuration candidate is %2. The configuration will not be changed.").arg(confSpace(), 32, 2, QChar('0')).arg(__configuration, 32, 2, QChar('0')));
        return false;
    }

    _isHelloSent = false;         // Hello message must be send in case of bad configuration parameters.
    _configuration = __configuration;
    createMenu();

    return true;
}
//---------------------------------------------------------------------------

// #brief Allow to set if the plugin is enabled
//
// #param __enabled - a new value
// #return no values return
void devPlugin::setEnabled(bool __enabled)
{
    _enabled = __enabled;
}
//---------------------------------------------------------------------------

// #brief Returns pointer to the current device
//
// #return pointer to the current device
QIODevice* devPlugin::device(void)
{
    return _device;
}
//---------------------------------------------------------------------------

// #brief Returns addres of the plugin
//
// #return addres of the plugin. When plugin is a socket-based then this is IP addres and when process-based then this is executable file path
QString devPlugin::address(void)
{
    return _address;
}
//---------------------------------------------------------------------------

// #brief Returns process execute parameters
//
// #return process execute parameters as string
QString devPlugin::params(void)
{
    return _params;
}
//---------------------------------------------------------------------------

// #brief Returns the name of the preferred format in  which this plugin sends model to the server
//
// #return the preferred format in  which this plugin sends model to the server
QString devPlugin::formatName(void)
{
    if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HMR) > 0)
        return "hmr";
    if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HML) > 0)
        return "hml";

    return "";
}
//---------------------------------------------------------------------------

// #brief Returns the list of presentation services
//
// #return list of presentation services
QStringList& devPlugin::presentationServices(void)
{
    int _result;
    if((!_isHelloSent) && (!hello(_result)))
        createEvent(0, "Retrieving presentation services failed. The hello message returned false.");

    return _presentationServices;
}
//---------------------------------------------------------------------------

// #brief Returns the number of the port
//
// #return the number of the port
int devPlugin::portNumber(void)
{
    return _portNumber;
}
//---------------------------------------------------------------------------

// #brief Returns the timeout value
//
// #return the timeout value
int devPlugin::timeout(void)
{
    return _timeOut;
}
//---------------------------------------------------------------------------

// #brief Returns the configuration of the plugin
//
// #return configuration of the plugin
int devPlugin::configuration(void)
{
    return _configuration;
}
//---------------------------------------------------------------------------

// #brief Returns if the plugin is enabled
//
// #return if the plugin is enabled
bool devPlugin::isEnabled(void)
{
    return _enabled;
}
//---------------------------------------------------------------------------

// #brief Returns the whole configuration space
//
// #return the whole configuration space
int devPlugin::confSpace(void)
{
    int result = 0x00000000;

    result |= DEV_PLUGIN_CONNECTION_TYPE_NETWORK;
    result |= DEV_PLUGIN_CONNECTION_TYPE_PROCESS;

    result |= DEV_PLUGIN_PREFFERED_FORMAT_HMR;
    result |= DEV_PLUGIN_PREFFERED_FORMAT_HML;

    result |= DEV_PLUGIN_SUPPORTED_MODULES_ALL;

    result |= DEV_PLUGIN_SOCKET_TYPE_TCP;
    result |= DEV_PLUGIN_SOCKET_TYPE_UDP;

    result |= DEV_PLUGIN_MESSAGE_TYPE_CONNECTION;
    result |= DEV_PLUGIN_MESSAGE_TYPE_FORMAT;
    result |= DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE;
    result |= DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG;
    result |= DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS;

    return result;
}
//---------------------------------------------------------------------------

// #brief Function that checks if the configuration __conf belongs to the whole configuration space
//
// #param __conf - input configuration that must be chceked
// #return true if yes, otherwise false
bool devPlugin::checkConfig(int __conf)
{
    return ((__conf & (~confSpace())) == 0);
}
//---------------------------------------------------------------------------

// #brief Function that returns string representation of the simulation type
//
// #param __simType - numeric identifier of simulation type
// #return string representation of the simulation type
QString devPlugin::simStrType(int __simType)
{
    if(__simType == DEV_PLUGIN_SIM_TYPE_DDI)
        return "ddi";
    if(__simType == DEV_PLUGIN_SIM_TYPE_TDI)
        return "tdi";
    if(__simType == DEV_PLUGIN_SIM_TYPE_GDI)
        return "gdi";

    return "";
}
//---------------------------------------------------------------------------

// #brief Function that returns the value that represents all types of verifications. This value is equal to logical OR of defined values DEV_PLUGIN_VER_TYPE_*
//
// #return value that represents all types of verifications
int devPlugin::allVerificationTypes(void)
{
    return DEV_PLUGIN_VER_TYPE_SUBSUME | DEV_PLUGIN_VER_TYPE_REDUCTION | DEV_PLUGIN_VER_TYPE_CONTRADICTION | DEV_PLUGIN_VER_TYPE_COMPLETENESS;
}
//---------------------------------------------------------------------------

// #brief Function that allow for sending and receive the message to/from the plugin
//
// #param __text - the message that will be send
// #param __result - reference to the value where the result code of function is stored
// #return the received message
QString devPlugin::pluginDataExchange(QString __text, int& __result)
{
    QString res = "";
    bool waitresult;
    _device->disconnect();        // disconnects all the signals from the object

    // Adding endl character
    QString msg = __text;
    if((msg.size() > 0) && (msg[msg.size()-1] != '\n'))
        msg = msg + "\n";

    createEvent(2, "Initializing data exchange...");

    // checking address
    if(!setAddress(address()))
    {
        __result = setLastError("Incorrect server addres: \'" + address() + "\'.", DEV_PLUGIN_MESSAGE_TYPE_CONNECTION);
        return res;
    }

    // Socket connection
    if((configuration() & DEV_PLUGIN_CONNECTION_TYPE_NETWORK) > 0)
    {
        QAbstractSocket* soc = (QAbstractSocket*)_device;
        connect(soc, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(error2String(QAbstractSocket::SocketError)));
        connect(soc, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(state2String(QAbstractSocket::SocketState)));

        soc->connectToHost(_address, _portNumber, QIODevice::ReadWrite);

        // Waiting for process started
        waitresult = soc->waitForConnected(_timeOut);
        if(!waitresult)
        {
            __result = setLastError("Connection timeout.", DEV_PLUGIN_MESSAGE_TYPE_CONNECTION);
            return res;
        }
    }

    // Process connection
    else if((configuration() & DEV_PLUGIN_CONNECTION_TYPE_PROCESS) > 0)
    {
        QProcess* proc = (QProcess*)_device;
        connect(proc, SIGNAL(error(QProcess::ProcessError)), this, SLOT(error2String(QProcess::ProcessError)));
        connect(proc, SIGNAL(stateChanged(QProcess::ProcessState)), this, SLOT(state2String(QProcess::ProcessState)));

        QString runcmd = address() + " " + params();
        proc->start(runcmd, QIODevice::ReadWrite);

        // Waiting for process started
        waitresult = proc->waitForStarted(_timeOut);
        if(!waitresult)
        {
            __result = setLastError("Starting process timeout.", DEV_PLUGIN_MESSAGE_TYPE_CONNECTION);
            return res;
        }
    }


    // The further process of data exchange goes in the same way for each class of devices

    // Waiting for data write
    createEvent(2, "Sending data: " + __text);
    _device->write(msg.toAscii());
    createEvent(2, "Data sent.");
    /*waitresult = _device->waitForBytesWritten(_timeOut);
     if(!waitresult)
     {
          __result = setLastError("Waiting for server ready timeout.");;
          return res;
     }*/

    // Waiting for response
    waitresult = _device->waitForReadyRead(_timeOut);
    if(!waitresult)
    {
        __result = setLastError("Waiting for response timeout.", DEV_PLUGIN_MESSAGE_TYPE_CONNECTION);
        return res;
    }

    createEvent(2, "Reading response from server...");
    res = QString(_device->readAll());
    createEvent(2, "Response: " + res.simplified());

    createEvent(2, "Closing connection to server.");
    _device->close();

    __result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return res;
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _device object is destroyed
//
// #return no values return
void devPlugin::onDestroy(void)
{
    if(_parent == NULL)
        return;

    _parent->onPluginDestroy(this);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _console action is clicked
//
// #return no values return
void devPlugin::onConsoleClick(void)
{
    int smr = _console->setMode(this);
    if(smr == SET_WINDOW_MODE_FAIL)
        return;

    if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
        return;

    createEvent(2, "Call for plugin console.");
    _console->show();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when presentation dialog action is clicked
//
// #return no values return
void devPlugin::onPresentationDialogClick(void)
{
    int r;
    if(!hello(r))
    {
        QMessageBox::critical(NULL, "HQEd", "Error during value presenation dialog calling.\nCall plugin event listener to see more information.", QMessageBox::Ok);
        return;
    }

    int smr = _presDialog->setMode(this);
    if(smr == SET_WINDOW_MODE_FAIL)
        return;

    if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
        return;

    createEvent(2, "Call for value presentation dialog.");
    _presDialog->show();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _sendHelloAction action is clicked
//
// #return no values return
void devPlugin::onSendHelloClick(void)
{
    int r;
    if(hello(r))
        QMessageBox::information(NULL, "HQEd", "Send hello success.", QMessageBox::Ok);
    else
        QMessageBox::critical(NULL, "HQEd", "Send hello error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _sendModelAction action is clicked
//
// #return no values return
void devPlugin::onSendModelClick(void)
{
    int r;
    if(sendModel(r))
        QMessageBox::information(NULL, "HQEd", "Sending model success.", QMessageBox::Ok);
    else
        QMessageBox::critical(NULL, "HQEd", "Sending model error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _getModels action is clicked
//
// #return no values return
void devPlugin::onGetModelsClick(void)
{
    int r;
    QStringList mds = getModels(r);

    if(r == DEV_PLUGIN_RESULT_CODE_SUCCESS)
    {
        QString ms = "";
        for(int i=0;i<mds.count();i+=2)
            ms += "\nuser: " + mds.at(i) + ", model name: " + mds.at(i+1);
        QMessageBox::information(NULL, "HQEd", "Retrieving models list success.\nAvailible models:" + ms, QMessageBox::Ok);
    }
    if(r == DEV_PLUGIN_RESULT_CODE_ERROR)
        QMessageBox::critical(NULL, "HQEd", "Retrieving models list error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _delModel action is clicked
//
// #return no values return
void devPlugin::onDelModelClick(void)
{
    int r;
    QStringList mds = getModels(r);

    if(r == DEV_PLUGIN_RESULT_CODE_SUCCESS)
    {
        QStringList models;
        for(int i=0;i<mds.count();i+=2)
            models << "user: " + mds.at(i) + ", model name: " + mds.at(i+1);

        bool ok;
        QString item = QInputDialog::getItem(NULL, tr("Removing model"), tr("Select model:"), models, 0, false, &ok);
        if ((ok) && (!item.isEmpty()))
        {
            int io = models.indexOf(item);
            if(removeModel(r, mds.at(io*2+1), mds.at(io*2)))
                QMessageBox::information(NULL, "HQEd", "Model removed successfully.", QMessageBox::Ok);
            else
                QMessageBox::critical(NULL, "HQEd", "Model removing error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
        }
    }
    else if(r == DEV_PLUGIN_RESULT_CODE_ERROR)
        QMessageBox::critical(NULL, "HQEd", "Retrieving models list error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _rdmDialog action is clicked
//
// #return no values return
void devPlugin::onReadModelDialogClick(void)
{
    int r;
    QStringList mds = getModels(r);

    if(r == DEV_PLUGIN_RESULT_CODE_SUCCESS)
    {
        QStringList models;
        for(int i=0;i<mds.count();i+=2)
            models << "user: " + mds.at(i+1) + ", model name: " + mds.at(i);

        bool ok;
        QString item = QInputDialog::getItem(NULL, tr("Reading model"), tr("Select model:"), models, 0, false, &ok);
        if ((ok) && (!item.isEmpty()))
        {
            int io = models.indexOf(item);

            if(readModel(r, mds.at(io*2), mds.at(io*2+1)))
                QMessageBox::information(NULL, "HQEd", "Model read successfully.", QMessageBox::Ok);
            else
                QMessageBox::critical(NULL, "HQEd", "Model reading error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
        }
    }
    else if(r == DEV_PLUGIN_RESULT_CODE_ERROR)
        QMessageBox::critical(NULL, "HQEd", "Retrieving models list error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _runModel action is clicked
//
// #return no values return
void devPlugin::onRunModelClick(void)
{
    if(_simEditor != NULL)
    {
        _simEditor->disconnect();
        delete _simEditor;
    }
    _simEditor = new SimulationEditor_ui;
    connect(_simEditor, SIGNAL(onStartClick()), this, SLOT(onSimEditorStart()));
    int smr = _simEditor->setMode();

    if(smr == SET_WINDOW_MODE_FAIL)
        return;
    if(smr == SET_WINDOW_MODE_OK_YOU_CANNOT_SHOW)
        return;

    _simEditor->show();
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when simulationEditor is closed with start button
//
// #return no values return
void devPlugin::onSimEditorStart(void)
{
    xttSimulationParams params;
    params.getDataFromSimulationEditor(_simEditor);

    //QMessageBox::critical(NULL, "Error localization", "sim tables: " + tables.join(", "), QMessageBox::Ok);
    //QMessageBox::critical(NULL, "Error localization", "sim type: " + simStrType(simtype), QMessageBox::Ok);
    //QMessageBox::critical(NULL, "Error localization", "sim state: " + simInitState, QMessageBox::Ok);

    startSimulation(params);
}
//---------------------------------------------------------------------------

// #brief Function that executes the simulation
//
// #param __simParams - reference to the simulation parameters
// #return no values return
void devPlugin::startSimulation(xttSimulationParams& __simParams)
{
    int result;

    // Sending model - updating
    if(!sendModel(result))
    {
        QMessageBox::critical(NULL, "HQEd", "Simulation error. Model has not been sent successfully.", QMessageBox::Ok);
        return;
    }

    // Execute simulation
    QList<QStringList>* runResult = runModel(result, __simParams.modelName(), __simParams.userName(), __simParams.simulationType(), __simParams.tables(), __simParams.initialState());
    if(result == DEV_PLUGIN_RESULT_CODE_SUCCESS)
    {
        AttributeGroups->setModelState(runResult, __simParams.usedState());
        TableList->addSimulationResult(runResult+1, __simParams.usedState());
        if(__simParams.usedState() != NULL)
            __simParams.usedState()->parent()->refreshUI();
    }
    if(result == DEV_PLUGIN_RESULT_CODE_ERROR)
        QMessageBox::critical(NULL, "HQEd", "Simulation error.\nCall plugin event listener to see more information.", QMessageBox::Ok);

    delete [] runResult;
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _verModel action is clicked
//
// #return no values return
void devPlugin::onVerModelClick(void)
{
    int result;
    QString msgs = fullVerification(result);

    if(result == DEV_PLUGIN_RESULT_CODE_ERROR)
        QMessageBox::critical(NULL, "HQEd", "Verification error.\nCall plugin event listener to see more information.", QMessageBox::Ok);
    if(result == DEV_PLUGIN_RESULT_CODE_SUCCESS)
    {
        QMessageBox::information(NULL, "HQEd", "Verification finished successfully.", QMessageBox::Ok);
        hqed::showMessages(msgs);
    }
}
//---------------------------------------------------------------------------

// #brief function that checks the given table w.r.t. all anomalies
//
// #param __schema - name of the table that must be checked
// #param _result - result code of the function
// #return One string created by hqed::createErrorString function that contains all the infomations about anomalies
QString devPlugin::tableVerification(QString __schema, int& _result)
{
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();

    // Sending model - updating
    if(!sendModel(_result))
        return "";

    emit setMaximumSteps(4);
    return checkModel(_result, modelname, username, __schema, allVerificationTypes());
}
//---------------------------------------------------------------------------

// #brief function that checks the model w.r.t. all anomalies
//
// #param _result - result code of the function
// #return One string created by hqed::createErrorString function that contains all the infomations about anomalies
QString devPlugin::fullVerification(int& _result)
{
    int result;
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QList<XTT_Table*>* tables = TableList->findTables("", 1);

    // Sending model - updating
    if(!sendModel(_result))
        return "";

    emit setMaximumSteps(4*tables->size());
    QString res = "";
    for(int i=0;i<tables->size();++i)
    {
        res += checkModel(result, modelname, username, tables->at(i)->Title(), allVerificationTypes());
        if(result == DEV_PLUGIN_RESULT_CODE_ERROR)
            _result = result;
    }
    delete tables;

    return res;
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _verCmplModel action is clicked
//
// #return no values return
void devPlugin::onModelCmplVerification(void)
{
    bool ok = true;
    int result;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QList<XTT_Table*>* tables = TableList->findTables("", 1);

    // Sending model - updating
    if(!sendModel(result))
    {
        QMessageBox::critical(NULL, "HQEd", "Verification of completeness error. Model has not been sent successfully.", QMessageBox::Ok);
        return;
    }

    QStringList resultmsgs;
    for(int i=0;i<tables->size();++i)
    {
        QStringList* msgs = checkCompleteness(result, modelname, username, tables->at(i)->Title());
        resultmsgs << *msgs;
        delete msgs;
        ok = ok && (result == DEV_PLUGIN_RESULT_CODE_SUCCESS);
    }
    delete tables;

    if(!ok)
        QMessageBox::critical(NULL, "HQEd", "Verification of completeness finished with errors. See plugin event listener for details.", QMessageBox::Ok);

    if(ok)
        QMessageBox::information(NULL, "HQEd", "Verification of completeness finished successfully.\nThe response messages:\n" + resultmsgs.join("\n"), QMessageBox::Ok);
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _verCntrModel action is clicked
//
// #return no values return
void devPlugin::onModelCntrVerification(void)
{
    bool ok = true;
    int result;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QList<XTT_Table*>* tables = TableList->findTables("", 1);

    // Sending model - updating
    if(!sendModel(result))
    {
        QMessageBox::critical(NULL, "HQEd", "Verification of cotradictions error. Model has not been sent successfully.", QMessageBox::Ok);
        return;
    }

    QStringList resultmsgs;
    for(int i=0;i<tables->size();++i)
    {
        QStringList* msgs = checkContradictions(result, modelname, username, tables->at(i)->Title());
        resultmsgs << *msgs;
        delete msgs;
        ok = ok && (result == DEV_PLUGIN_RESULT_CODE_SUCCESS);
    }
    delete tables;

    if(!ok)
        QMessageBox::critical(NULL, "HQEd", "Verification of cotradictions finished with errors. See plugin event listener for details.", QMessageBox::Ok);

    if(ok)
    {
        QString msgs = "";
        for(int i=0;i<resultmsgs.size();i+=2)
            msgs += "Rule index: " + resultmsgs.at(i) + " error: " + resultmsgs.at(i+1) + "\n";
        QMessageBox::information(NULL, "HQEd", "Verification of cotradictions finished successfully.\nThe response messages:\n" + msgs, QMessageBox::Ok);
    }
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _verSbsmModel action is clicked
//
// #return no values return
void devPlugin::onModelSbsmVerification(void)
{
    bool ok = true;
    int result;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QList<XTT_Table*>* tables = TableList->findTables("", 1);

    // Sending model - updating
    if(!sendModel(result))
    {
        QMessageBox::critical(NULL, "HQEd", "Verification of subsumption error. Model has not been sent successfully.", QMessageBox::Ok);
        return;
    }

    QStringList resultmsgs;
    for(int i=0;i<tables->size();++i)
    {
        QStringList* msgs = checkSubsumptions(result, modelname, username, tables->at(i)->Title());
        resultmsgs << *msgs;
        delete msgs;
        ok = ok && (result == DEV_PLUGIN_RESULT_CODE_SUCCESS);
    }
    delete tables;

    if(!ok)
        QMessageBox::critical(NULL, "HQEd", "Verification of subsumptions finished with errors. See plugin event listener for details.", QMessageBox::Ok);

    if(ok)
    {
        QString msgs = "";
        for(int i=0;i<resultmsgs.size();i+=2)
            msgs += "Rule index: " + resultmsgs.at(i) + " error: " + resultmsgs.at(i+1) + "\n";
        QMessageBox::information(NULL, "HQEd", "Verification of subsumptions finished successfully.\nThe response messages:\n" + msgs, QMessageBox::Ok);
    }
}
//---------------------------------------------------------------------------

// #brief Function that is triggered when _verRdcsModel action is clicked
//
// #return no values return
void devPlugin::onModelRdcsVerification(void)
{
    bool ok = true;
    int result;
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QList<XTT_Table*>* tables = TableList->findTables("", 1);

    // Sending model - updating
    if(!sendModel(result))
    {
        QMessageBox::critical(NULL, "HQEd", "Verification of reductions error. Model has not been sent successfully.", QMessageBox::Ok);
        return;
    }

    QStringList resultmsgs;
    for(int i=0;i<tables->size();++i)
    {
        QStringList* msgs = checkReductions(result, modelname, username, tables->at(i)->Title());
        resultmsgs << *msgs;
        delete msgs;
        ok = ok && (result == DEV_PLUGIN_RESULT_CODE_SUCCESS);
    }
    delete tables;

    if(!ok)
        QMessageBox::critical(NULL, "HQEd", "Verification of reductions finished with errors. See plugin event listener for details.", QMessageBox::Ok);

    if(ok)
    {
        QString msgs = "";
        for(int i=0;i<resultmsgs.size();i+=2)
            msgs += "Rule index: " + resultmsgs.at(i) + " error: " + resultmsgs.at(i+1) + "\n";
        QMessageBox::information(NULL, "HQEd", "Verification of reductions finished successfully.\nThe response messages:\n" + msgs, QMessageBox::Ok);
    }
}
//---------------------------------------------------------------------------



// #brief Function that parses the message specified with the help of __message.
//
// #param __message - the message that will be parsed
// #param rcode - result code of the function
// #return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well
QStringList devPlugin::parseMessage(QString __message, int& rcode)
{
    QString rmsg;
    QStringList result = hqed::parseProtocolV1Message(__message, rcode, rmsg);
    if(rcode != PROTOCOL_PARSING_RESULT_SUCCESS)
    {
        if(rcode == PROTOCOL_PARSING_RESULT_ERROR_FORMAT)
            rcode = setLastError(rmsg, DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
    }

    return result;
}
// -----------------------------------------------------------------------------

// #brief Function that creates the PROLOG list using given argument
//
// #param __list - result code of the function
// #return string that is a PROLOG-formated list
QString devPlugin::createList(QStringList __list)
{
    return "[" + __list.join(",") + "]";
}
// -----------------------------------------------------------------------------

// #brief Function that checks the result stored in responce from server
//
// #param __message - the message that will be parsed
// #param rcode - result code of the function
// #return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well.
QStringList devPlugin::checkResult(QString __response, int& rcode)
{
    // if succes
    QStringList pl = parseMessage(__response, rcode);
    if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        return pl;

    // if succes
    if(pl.count() == 0)
    {
        rcode = setLastError("Incorrect format of received message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE);
        createEvent(0, __response);
        return pl;
    }

    // checking responce
    if(pl.at(0).toLower() == "false")
    {
        QString errorMSG = pl.count() > 1 ? pl.at(1) : "";
        rcode = setLastError("Server has returned false. Server error message:\n" + errorMSG, DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_RESPONSE);
        return pl;
    }

    return pl;
}
// -----------------------------------------------------------------------------

// #brief Function that sends hello message
//
// #param _result - result code of the function
// #return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
bool devPlugin::hello(int& _result)
{
    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    QString message = createList(QStringList() << "hello" << _parent->instanceIdentifier()) + ".";
    createEvent(2, "Sending hello message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes

    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received \'hello\' message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return false;
    }

    // checking configuration
    createEvent(2, "Parsing the second part of the response.");
    pl = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes

    if(pl.count() != 5)
    {
        _result = setLastError("Incorrect format of received \'hello\' message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return false;
    }

    bool okconf;
    int conf = pl.at(3).toInt(&okconf);
    if(!okconf)
    {
        _result = setLastError("Incorrect format of received \'hello\' message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, "Received configuration value has incorrect format: " + pl.at(3));
        return false;
    }

    // If the configuration is correct
    int sconf = conf << 16;
    int conftpl = _configuration & DEV_PLUGIN_SUPPORTED_MODULES_ALL;
    createEvent(2, QString("Received configuration from server: %1: 0x%2(16) = %3(2)").arg(conf).arg(sconf, 8, 16, QChar('0')).arg(sconf, 32, 2, QChar('0')));
    createEvent(2, "Checking configuration compatibility...");
    if(conftpl != sconf)
    {
        QString sm = "", cm = "";
        sm += (sconf & DEV_PLUGIN_SUPPORTED_MODULES_V) > 0 ? "V " : "";
        sm += (sconf & DEV_PLUGIN_SUPPORTED_MODULES_P) > 0 ? "P " : "";
        sm += (sconf & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0 ? "S " : "";
        sm += (sconf & DEV_PLUGIN_SUPPORTED_MODULES_T) > 0 ? "T " : "";
        cm += (conftpl & DEV_PLUGIN_SUPPORTED_MODULES_V) > 0 ? "V " : "";
        cm += (conftpl & DEV_PLUGIN_SUPPORTED_MODULES_P) > 0 ? "P " : "";
        cm += (conftpl & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0 ? "S " : "";
        cm += (conftpl & DEV_PLUGIN_SUPPORTED_MODULES_T) > 0 ? "T " : "";

        _configuration = _configuration & (~DEV_PLUGIN_SUPPORTED_MODULES_ALL);          // Supported modules bits reset
        setConfiguration(_configuration | sconf);
        Settings->pluginUpdateConfiguration(name(), _configuration);

        _result = setLastError("Incorrect plugin configuration.\n" + name() + " plugin is defined to support " + cm + " modules, while server supports " + sm + ".\nThe configuration will be changed automatically.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return false;
    }
    createEvent(2, "Checking configuration compatibility... OK");

    // checking the list of trasnlation services
    _presentationServices.clear();
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_P) != 0)
    {
        createEvent(2, "Parsing a list of presentation services...");
        pl = parseMessage(pl.at(4), _result);
        if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
            return false;
        if(pl.isEmpty())
        {
            createEvent(0, "The presentation plugin does not contain any presentation services.");
            return false;
        }
        _presentationServices = pl;
    }

    _isHelloSent = true;
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that reads model from server
//
// #param _result - result code of the function
// #param __modelname - the name of the model that we want to read
// #param __username - the owner of the mode that we want to read
// #return true on remove, otherwise false
bool devPlugin::readModel(int& _result, QString __modelname, QString __username)
{
    // Message format: [model, get, +format, +modelname, +username, +[+parts]]

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Reading model failed. The hello message returned false.");
        return false;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Reading model initializing: checking configuration...");
    if((configuration() & (DEV_PLUGIN_SUPPORTED_MODULES_V | DEV_PLUGIN_SUPPORTED_MODULES_S | DEV_PLUGIN_SUPPORTED_MODULES_T)) == 0)
    {
        _result = setLastError(name() + " does not support \'send\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return false;
    }

    createEvent(2, "Reading model from " + name() + "...");
    createEvent(2, "Initializing receiver device...");

    // Sending request
    QString message = createList(QStringList() << "model,get,hml"  << __modelname << __username << createList(QStringList() << createList(QStringList() << "all"))) + ".";
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes
    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // Checking format of the responce
    if(pl.size() != 2)
    {
        _result = setLastError("Incorrect format of responce for \'mode, get\' message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return false;
    }

    // Loading model
    int parts = VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS | VAS_XML_MODEL_PARTS_XTT_TABLS | VAS_XML_MODEL_PARTS_XTT_STATS;
    QBuffer buf;
    QString msg ,frmt;
    QString modelcontent = pl.at(1);
    buf.setData(modelcontent.toAscii());

    int rcode = vas_in_XML(&buf, msg, frmt, parts);
    if(rcode == VAS_XML_UNKNOWN_DEVICE)
    {
        _result = setLastError("Loading error: device is not specified.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_DEVICE_NOT_READABLE)
    {
        _result = setLastError("Loading error: device is not readable.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_FORMAT_ERROR)
    {
        _result = setLastError("Loading error: " + hqed::retrieveMessageParts(msg), DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_FORMAT_NOT_SUPPORTED)
    {
        _result = setLastError("Loading error: the file format is not supported.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_VERSION_NOT_SUPPORTED)
    {
        _result = setLastError("Loading error: the xml version is not supported or not specified.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_XTT_SYNTAX_ERROR)
    {
        _result = setLastError(msg, DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }
    if(rcode == VAS_XML_NOTHING_DONE)
    {
        _result = setLastError("Reading error: nothing has been done.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        return false;
    }

    MainWin->onLoad_HML_2_0_server();


    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sends model int the appropriate format to the server
//
// #param _result - result code of the function
// #return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
bool devPlugin::sendModel(int& _result)
{
    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Sending model failed. The hello message returned false.");
        return false;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Sending model initializing: checking configuration...");
    if((configuration() & (DEV_PLUGIN_SUPPORTED_MODULES_V | DEV_PLUGIN_SUPPORTED_MODULES_S | DEV_PLUGIN_SUPPORTED_MODULES_T)) == 0)
    {
        _result = setLastError(name() + " does not support \'send\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return false;
    }

    createEvent(2, "Sending model to " + name() + "...");
    createEvent(2, "Initializing receiver device...");

    QBuffer* buf = new QBuffer;
    buf->open(QIODevice::ReadWrite);

    createEvent(2, "Translation model to protocol version...");
    if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HMR) > 0)
    {
        bool mg = sendHmrModel(buf, _result);
        if(!mg)
            return false;
    }
    else if((configuration() & DEV_PLUGIN_PREFFERED_FORMAT_HML) > 0)
    {
        bool mg = sendHmlModel(buf, _result);
        if(!mg)
            return false;
    }
    else
    {
        _result = setLastError(name() + ": Sending model: unrecogniozed preferred format.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return false;
    }

    // Model reading
    QString modelname = hqed::modelList()->activeModel()->modelID();
    QString username = _parent->instanceIdentifier();
    QString plmodel(buf->data());
    buf->close();
    delete buf;

    // Changing quotations and new line characters
    int qindex = 0;
    int sindex = 0;
    do
    {
        sindex = plmodel.indexOf("\n");
        if(sindex > -1)
            plmodel.replace(sindex, 1, " ");

        qindex = plmodel.indexOf("\'", qindex);
        if(qindex > -1)
        {
            plmodel.insert(qindex, "\\");
            qindex += 2;
        }
    }
    while((qindex > -1) || (sindex > -1));
    plmodel = "\'" + plmodel + "\'";

    QString message = createList(QStringList() << "model,add" << formatName() << modelname << username << plmodel) + ".";
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes
    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sends hmr model to the server
//
// #param __buf - pointer to the buffere where the model is stored
// #param _result - result code of the function
// #return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
bool devPlugin::sendHmrModel(QIODevice* __buf, int& _result)
{
    QString msg;
    int result = vas_out_SWI_Prolog(__buf, msg, 0, "", "", true);

    // Error handling
    if(result == VAS_SWI_PROLOG_UNKNOWN_DEVICE)
    {
        _result = setLastError("Sending model: device is not specified.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS);
        return false;
    }
    if(result == VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE)
    {
        _result = setLastError("Sending model: device is not writable.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS);
        return false;
    }
    if(result == VAS_SWI_PROLOG_ERROR)
    {
        if((configuration() & DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS) > 0)
            hqed::showMessages(msg);
        _result = setLastError("HMR generation error.", 0);
        return false;
    }

    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that sends hml model to the server
//
// #param __buf - pointer to the buffere where the model is stored
// #param _result - result code of the function
// #return true if everything is ok, false when error occurs or the configuration of the supported modules is incorrect
bool devPlugin::sendHmlModel(QIODevice* __buf, int& _result)
{
    int parts = VAS_XML_MODEL_PARTS_XTT_TYPES;
    parts |=  VAS_XML_MODEL_PARTS_ARD_PROPS;
    parts |=  VAS_XML_MODEL_PARTS_ARD_TPH;
    parts |=  VAS_XML_MODEL_PARTS_ARD_DPNDS;
    parts |=  VAS_XML_MODEL_PARTS_ARD_ATTRS;
    parts |=  VAS_XML_MODEL_PARTS_XTT_ATTRS;
    parts |= VAS_XML_MODEL_PARTS_XTT_TABLS;
    parts |= VAS_XML_MODEL_PARTS_XTT_STATS;


    int result = vas_out_HML_2_0(__buf, false, parts);

    // Error handling
    if(result == VAS_XML_UNKNOWN_DEVICE)
    {
        _result = setLastError("Sending model: device is not specified.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS);
        return false;
    }
    if(result == VAS_XML_DEVICE_NOT_WRITABLE)
    {
        _result = setLastError("Sending model: device is not writable.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_OTHERS);
        return false;
    }

    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that retrives models from server
//
// #param _result - result code of the function
// #return Model list. A single record consist of two elemente of the returned list. First is a username and second model name.
QStringList devPlugin::getModels(int& _result)
{
    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Retivering list of models model failed. The hello message returned false.");
        return QStringList();
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Retrieving models list initializing: checking configuration...");
    if((configuration() & (DEV_PLUGIN_SUPPORTED_MODULES_V | DEV_PLUGIN_SUPPORTED_MODULES_S | DEV_PLUGIN_SUPPORTED_MODULES_T)) == 0)
    {
        _result = setLastError(name() + " does not support \'get\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return QStringList();
    }

    createEvent(2, "Retrieving model list...");

    QString message = createList(QStringList() << "model" << "getlist") + ".";
    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return QStringList();

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return QStringList();

    // if succes

    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received model list message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, "Received response: " + response);
        return QStringList();
    }

    // Parsing models list
    // checking configuration
    QStringList res,
            modellist = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return QStringList();

    for(int i=0;i<modellist.size();++i)
    {
        int rcode;
        QStringList md = parseMessage(modellist.at(i), rcode);

        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
            continue;

        if(md.count() != 2)
        {
            rcode = setLastError("Incorrect format of model information.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
            createEvent(0, "Part of received message: " + modellist.at(i));
            continue;
        }

        for(int i=0;i<md.count();++i)
            res << md.at(i);
    }

    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that removes model from server
//
// #param _result - result code of the function
// #param __modelname - the name of the model that we want to remove
// #param __username - the owner of the mode that we want to remove
// #return true on remove, otherwise false
bool devPlugin::removeModel(int& _result, QString __modelname, QString __username)
{
    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Removing model failed. The hello message returned false.");
        return false;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Removing model initializing: checking configuration...");
    if((configuration() & (DEV_PLUGIN_SUPPORTED_MODULES_V | DEV_PLUGIN_SUPPORTED_MODULES_S)) == 0)
    {
        _result = setLastError(name() + " does not support \'remove\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return false;
    }

    createEvent(2, "Removing model from server: username: \'" + __username + "\', model name: \'" + __modelname + "\'");

    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);

    QString message = createList(QStringList() << "model" << "remove" << __modelname << __username) + ".";
    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return false;

    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return true;
}
// -----------------------------------------------------------------------------

// #brief Function that executes model simulation
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __type - type of simulation
// #param __tables - list of tables
// #param __state - name of the state OR state definition
// #return two elelemnt array of list of string lists. The first element of array has the following format:
// #li attribute name
// #li value_0
// #li value_1
// #li value_2
// #li ...
// #li value_n
// The second element contains only one list of string that has the followin format:
// #li table_1 name
// #li rule index
// #li ...
// #li table_n name
// #li rule index
QList<QStringList>* devPlugin::runModel(int& _result, QString __modelname, QString __username, int __type, QStringList __tables, QString __state)
{
    QList<QStringList>* simResponse = new QList<QStringList>[2];

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Executing model failed. The hello message returned false.");
        return simResponse;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Executing model initializing: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_S) == 0)
    {
        _result = setLastError(name() + " does not support \'run\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return simResponse;
    }

    createEvent(2, "Executing model: username: \'" + __username + "\', model name: \'" + __modelname + "\'");
    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);

    QString message = createList(QStringList() << "model" << "run" << __modelname << __username << simStrType(__type) << createList(__tables) << __state) + ".";

    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return simResponse;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return simResponse;

    // Parsing model state
    if(pl.count() != 3)
    {
        _result = setLastError("Incorrect format of received \'run\' result message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return simResponse;
    }

    // Parsing list of attributes
    QStringList modelAtts = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return simResponse;

    // Parsing list of fired rules
    QStringList firedRules = parseMessage(pl.at(2), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return simResponse;

    // Parsing atts and its values
    for(int i=0;i<modelAtts.size();++i)
    {
        int rcode;
        QStringList modelAtt = parseMessage(modelAtts.at(i), rcode);
        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        {
            createEvent(0, "Unable to parsing state attribute: " + modelAtts.at(i) + ".\nAttribute index = " + QString::number(i) + ".");
            continue;
        }

        // checking if the value is not defined as set
        if((modelAtt.size() == 2) && (modelAtt.at(1).trimmed().at(0) == '['))
        {
            QStringList modelAttVals = parseMessage(modelAtt.at(1), rcode);
            if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
            {
                createEvent(0, "Unable to parsing values of attribute: " + modelAtt.at(1) + ".\nAttribute index = " + QString::number(i) + ".");
                continue;
            }
            modelAtt.removeAt(1);
            modelAtt << modelAttVals;
        }

        simResponse[0].append(modelAtt);
    }

    // Adding list of fired rules
    simResponse[1].append(firedRules);

    createEvent(2, "Simulation finished successfully.");
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return simResponse;
}
// -----------------------------------------------------------------------------

// #brief function that checks the completeness of the given table in the given model
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __schema - the name of the table that should be checked
// #return The list of messages that describe the anomalies of completeness in the given table
QStringList* devPlugin::checkCompleteness(int& _result, QString __modelname, QString __username, QString __schema)
{
    QStringList* verResponse = new QStringList;

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Completeness checking failed. The hello message returned false.");
        return verResponse;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Verifying table \'" + __schema + "\' completeness initializing: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0)
    {
        _result = setLastError(name() + " does not support \'verify\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return verResponse;
    }

    createEvent(2, "Verifying completeness of table \'" + __schema + "\' in the model: username: \'" + __username + "\', model name: \'" + __modelname + "\'");
    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);
    __schema = hqed::mcwp(__schema);

    QString message = createList(QStringList() << "model" << "verify" << "vcomplete " << __modelname << __username << __schema) + ".";

    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing model state
    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received \'verify\' result message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return verResponse;
    }

    // Parsing list of messages to separated messages
    QStringList verMsgs = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing each messages
    for(int i=0;i<verMsgs.size();++i)
    {
        int rcode;
        QStringList verMsg = parseMessage(verMsgs.at(i), rcode);

        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        {
            createEvent(0, "Unable to parsing message: " + verMsgs.at(i) + ".");
            continue;
        }

        if(verMsg.size() != 1)
        {
            createEvent(0, "Incorrect format of completeness verification response message: " + verMsgs.at(i) + ".");
            continue;
        }

        verResponse->append("error from " + name() + ": " + verMsg.at(0));
    }

    createEvent(2, "Verification of table \'" + __schema + "\' completeness finished successfully.");
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return verResponse;
}
// -----------------------------------------------------------------------------

// #brief function that checks the completeness of the given table in the given model
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __schema - the name of the table that should be checked
// #return The list of messages that describe the anomalies of completeness in the given table. The list has the following format:
// #li at(2n) - rule_id
// #li at(2n+1) - description
// #li n >= 0
QStringList* devPlugin::checkSubsumptions(int& _result, QString __modelname, QString __username, QString __schema)
{
    QStringList* verResponse = new QStringList;

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Subsumptions checking failed. The hello message returned false.");
        return verResponse;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Verifying table \'" + __schema + "\' subsumptions initializing: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0)
    {
        _result = setLastError(name() + " does not support \'verify\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return verResponse;
    }

    createEvent(2, "Verifying subsumptions of table \'" + __schema + "\' in the model: username: \'" + __username + "\', model name: \'" + __modelname + "\'");
    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);
    __schema = hqed::mcwp(__schema);

    QString message = createList(QStringList() << "model" << "verify" << "vsubsume" << __modelname << __username << __schema) + ".";

    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing model state
    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received \'verify\' result message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return verResponse;
    }

    // Parsing list of messages to separated messages
    QStringList verMsgs = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing each messages
    for(int i=0;i<verMsgs.size();++i)
    {
        int rcode;
        QStringList verMsg = parseMessage(verMsgs.at(i), rcode);
        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        {
            createEvent(0, "Unable to parsing message: " + verMsgs.at(i) + ".");
            continue;
        }

        if(verMsg.size() != 2)
        {
            createEvent(0, "Incorrect format of subsumptions verification response message: " + verMsgs.at(i) + ".");
            continue;
        }

        verResponse->append(verMsg.at(0));
        verResponse->append("error from " + name() + ": " + verMsg.at(1));
    }

    createEvent(2, "Verification of table \'" + __schema + "\' subsumptions finished successfully.");
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return verResponse;
}
// -----------------------------------------------------------------------------

// #brief function that checks the contradictions of the given table in the given model
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __schema - the name of the table that should be checked
// #return The list of messages that describe the anomalies of contradictions in the given table. The list has the following format:
// #li at(2n) - rule_id
// #li at(2n+1) - description
// #li n >= 0
QStringList* devPlugin::checkContradictions(int& _result, QString __modelname, QString __username, QString __schema)
{
    QStringList* verResponse = new QStringList;

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Contradictions checking failed. The hello message returned false.");
        return verResponse;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Verifying table \'" + __schema + "\' contradictions initializing: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0)
    {
        _result = setLastError(name() + " does not support \'verify\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return verResponse;
    }

    createEvent(2, "Verifying contradictions of table \'" + __schema + "\' in the model: username: \'" + __username + "\', model name: \'" + __modelname + "\'");
    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);
    __schema = hqed::mcwp(__schema);

    QString message = createList(QStringList() << "model" << "verify" << "vcontradict" << __modelname << __username << __schema) + ".";

    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing model state
    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received \'verify\' result message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return verResponse;
    }

    // Parsing list of messages to separated messages
    QStringList verMsgs = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing each messages
    for(int i=0;i<verMsgs.size();++i)
    {
        int rcode;
        QStringList verMsg = parseMessage(verMsgs.at(i), rcode);
        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        {
            createEvent(0, "Unable to parsing message: " + verMsgs.at(i) + ".");
            continue;
        }

        if(verMsg.size() != 2)
        {
            createEvent(0, "Incorrect format of contradictions verification response message: " + verMsgs.at(i) + ".");
            continue;
        }

        verResponse->append(verMsg.at(0));
        verResponse->append("error from " + name() + ": " + verMsg.at(1));
    }

    createEvent(2, "Verification of table \'" + __schema + "\' contradictions finished successfully.");
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return verResponse;
}
// -----------------------------------------------------------------------------

// #brief function that checks the reductions of the given table in the given model
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __schema - the name of the table that should be checked
// #return The list of messages that describe the anomalies of reductions in the given table. The list has the following format:
// #li at(2n) - rule_id
// #li at(2n+1) - description
// #li n >= 0
QStringList* devPlugin::checkReductions(int& _result, QString __modelname, QString __username, QString __schema)
{
    QStringList* verResponse = new QStringList;

    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Reductions checking failed. The hello message returned false.");
        return verResponse;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Verifying table \'" + __schema + "\' reductions initializing: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_V) == 0)
    {
        _result = setLastError(name() + " does not support \'verify\' command.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return verResponse;
    }

    createEvent(2, "Verifying reductions of table \'" + __schema + "\' in the model: username: \'" + __username + "\', model name: \'" + __modelname + "\'");
    __modelname = hqed::mcwp(__modelname);
    __username = hqed::mcwp(__username);
    __schema = hqed::mcwp(__schema);

    QString message = createList(QStringList() << "model" << "verify" << "vreduce" << __modelname << __username << __schema) + ".";

    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, _result);

    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing model state
    if(pl.count() != 2)
    {
        _result = setLastError("Incorrect format of received \'verify\' result message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, response);
        return verResponse;
    }

    // Parsing list of messages to separated messages
    QStringList verMsgs = parseMessage(pl.at(1), _result);
    if(_result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return verResponse;

    // Parsing each messages
    for(int i=0;i<verMsgs.size();++i)
    {
        int rcode;
        QStringList verMsg = parseMessage(verMsgs.at(i), rcode);
        if(rcode == DEV_PLUGIN_RESULT_CODE_ERROR)
        {
            createEvent(0, "Unable to parsing message: " + verMsgs.at(i) + ".");
            continue;
        }

        if(verMsg.size() != 2)
        {
            createEvent(0, "Incorrect format of reductions verification response message: " + verMsgs.at(i) + ".");
            continue;
        }

        verResponse->append(verMsg.at(0));
        verResponse->append("error from " + name() + ": " + verMsg.at(1));
    }

    createEvent(2, "Verification of table \'" + __schema + "\' reductions finished successfully.");
    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return verResponse;
}
// -----------------------------------------------------------------------------

// #brief function that checks the model w.r.t. given anomalies
//
// #param _result - result code of the function
// #param __modelname - the name of the model
// #param __username - the name of the model owner user
// #param __schema - the name of the table that should be checked
// #param __anomaliesTypes - the types of anomalies
// #return One string created by hqed::createErrorString function that contains all the infomations about anomalies
QString devPlugin::checkModel(int& _result, QString __modelname, QString __username, QString __schema, int __anomaliesTypes)
{
    QStringList* resultOfCmpl = NULL;
    QStringList* resultOfCntr = NULL;
    QStringList* resultOfSbsm = NULL;
    QStringList* resultOfRdcs = NULL;
    int rcodeCmpl = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    int rcodeSbsm = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    int rcodeRdcs = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    int rcodeCntr = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    QString res = "";

    int tindex = TableList->indexOfTitle(__schema);
    if(tindex == -1)         // When table not fount
        return hqed::createErrorString("", "", "Model verification error: given table \'" + __schema + "\' not fonoud.", 0, 2);

    QString tid = TableList->Table(tindex)->TableId();

    // Checking for hello message
    if((!_isHelloSent) && (!hello(_result)))
    {
        createEvent(0, "Reductions checking failed. The hello message returned false.");
        return res;
    }

    // Checking completeness
    emit setOperationTitle(name() + ": checking of \'" + __schema + "\' completeness...");
    if((__anomaliesTypes | DEV_PLUGIN_VER_TYPE_COMPLETENESS) > 0)
    {
        resultOfCmpl = checkCompleteness(rcodeCmpl, __modelname, __username, __schema);
        if(rcodeCmpl == DEV_PLUGIN_RESULT_CODE_ERROR)
            res += hqed::createErrorString(tid, "", "Error while verification of the model with respect to completeness.\nSee plugin event listener for more information.", 0, 3);
        if(rcodeCmpl == DEV_PLUGIN_RESULT_CODE_SUCCESS)
        {
            // Adding messages
            for(int i=0;i<resultOfCmpl->size();++i)
                res += hqed::createErrorString(tid, "", resultOfCmpl->at(i), 0, 3);
        }
        delete resultOfCmpl;
    }
    emit onProgress();

    // Checking subsumption
    emit setOperationTitle(name() + ": checking of \'" + __schema + "\' subsumptions...");
    if((__anomaliesTypes | DEV_PLUGIN_VER_TYPE_SUBSUME) > 0)
    {
        resultOfSbsm = checkSubsumptions(rcodeSbsm, __modelname, __username, __schema);
        if(rcodeSbsm == DEV_PLUGIN_RESULT_CODE_ERROR)
            res += hqed::createErrorString(tid, "", "Error while verification of the model with respect to subsumptions.\nSee plugin event listener for more information.", 0, 3);
        if(rcodeSbsm == DEV_PLUGIN_RESULT_CODE_SUCCESS)
        {
            // Adding messages
            for(int i=0;i<resultOfSbsm->size();i+=2)
            {
                bool convok;
                int tc = 3;
                int rindex = resultOfSbsm->at(i).toInt(&convok)-1;
                QString msg = resultOfSbsm->at(i+1);
                QString rid = "";
                if(convok)
                {
                    int trc = TableList->Table(tindex)->RowCount();
                    if((rindex < 0) || (rindex >= trc))
                        res += hqed::createErrorString(tid, "", "Subsumptions model verification: rule index \'" + QString::number(rindex+1) + "\' is out of range. The table " + __schema + " has " + QString::number(trc) + " rows.", 0, 3);
                    if((rindex >= 0) && (rindex < trc))
                    {
                        rid = TableList->Table(tindex)->Row(rindex)->Cell(0)->CellId();
                        tc = 5;
                    }
                }
                if(!convok)
                    res += hqed::createErrorString(tid, rid, "Subsumptions model verification: incorrect format of rule index \'" + resultOfSbsm->at(i) + "\'.", 0, 3);

                res += hqed::createErrorString(tid, rid, msg, 0, tc);
            }
        }
        delete resultOfSbsm;
    }
    emit onProgress();

    // Checking Contradictions
    emit setOperationTitle(name() + ": checking of \'" + __schema + "\' contradictions...");
    if((__anomaliesTypes | DEV_PLUGIN_VER_TYPE_CONTRADICTION) > 0)
    {
        resultOfCntr = checkContradictions(rcodeCntr, __modelname, __username, __schema);
        if(rcodeCntr == DEV_PLUGIN_RESULT_CODE_ERROR)
            res += hqed::createErrorString(tid, "", "Error while verification of the model with respect to contradictions.\nSee plugin event listener for more information.", 0, 3);
        if(rcodeCntr == DEV_PLUGIN_RESULT_CODE_SUCCESS)
        {
            // Adding messages
            for(int i=0;i<resultOfCntr->size();i+=2)
            {
                bool convok;
                int tc = 3;
                int rindex = resultOfCntr->at(i).toInt(&convok)-1;
                QString msg = resultOfCntr->at(i+1);
                QString rid = "";
                if(convok)
                {
                    int trc = TableList->Table(tindex)->RowCount();
                    if((rindex < 0) || (rindex >= trc))
                        res += hqed::createErrorString(tid, "", "Contradictions model verification: rule index \'" + QString::number(rindex+1) + "\' is out of range. The table " + __schema + " has " + QString::number(trc) + " rows.", 0, 3);
                    if((rindex >= 0) && (rindex < trc))
                    {
                        rid = TableList->Table(tindex)->Row(rindex)->Cell(0)->CellId();
                        tc = 5;
                    }
                }
                if(!convok)
                    res += hqed::createErrorString(tid, rid, "Contradictions model verification: incorrect format of rule index \'" + resultOfCntr->at(i) + "\'.", 0, 3);

                res += hqed::createErrorString(tid, rid, msg, 0, tc);
            }
        }
        delete resultOfCntr;
    }
    emit onProgress();

    // Checking Reductions
    emit setOperationTitle(name() + ": checking of \'" + __schema + "\' reductions...");
    if((__anomaliesTypes | DEV_PLUGIN_VER_TYPE_REDUCTION) > 0)
    {
        resultOfRdcs = checkReductions(rcodeRdcs, __modelname, __username, __schema);
        if(rcodeRdcs == DEV_PLUGIN_RESULT_CODE_ERROR)
            res += hqed::createErrorString(tid, "", "Error while verification of the model with respect to reductions.\nSee plugin event listener for more information.", 0, 3);
        if(rcodeRdcs == DEV_PLUGIN_RESULT_CODE_SUCCESS)
        {
            // Adding messages
            for(int i=0;i<resultOfRdcs->size();i+=2)
            {
                bool convok;
                int tc = 3;
                int rindex = resultOfRdcs->at(i).toInt(&convok)-1;
                QString msg = resultOfRdcs->at(i+1);
                QString rid = "";
                if(convok)
                {
                    int trc = TableList->Table(tindex)->RowCount();
                    if((rindex < 0) || (rindex >= trc))
                        res += hqed::createErrorString(tid, "", "Reductions model verification: rule index \'" + QString::number(rindex+1) + "\' is out of range. The table " + __schema + " has " + QString::number(trc) + " rows.", 0, 3);
                    if((rindex >= 0) && (rindex < trc))
                    {
                        rid = TableList->Table(tindex)->Row(rindex)->Cell(0)->CellId();
                        tc = 5;
                    }
                }
                if(!convok)
                    res += hqed::createErrorString(tid, rid, "Reductions model verification: incorrect format of rule index \'" + resultOfRdcs->at(i) + "\'.", 0, 3);

                res += hqed::createErrorString(tid, rid, msg, 0, tc);
            }
        }
        delete resultOfRdcs;
    }
    emit onProgress();

    _result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    if((rcodeCmpl == DEV_PLUGIN_RESULT_CODE_ERROR)    ||
            (rcodeSbsm == DEV_PLUGIN_RESULT_CODE_ERROR)  ||
            (rcodeRdcs == DEV_PLUGIN_RESULT_CODE_ERROR)  ||
            (rcodeCntr == DEV_PLUGIN_RESULT_CODE_ERROR))
        _result = DEV_PLUGIN_RESULT_CODE_ERROR;

    return res;
}
// -----------------------------------------------------------------------------

// #brief Function that executes the value translation
//
// #param __serviceName - name of the translator
// #param __value - value to be translated
// #param __result - result code of the function
// #return translated value as string
QString devPlugin::valueTranslation(QString __serviceName, QString __value, int& __result)
{
    QString result = __value;

    if((!_isHelloSent) && (!hello(__result)))
    {
        createEvent(0, "Value translation failed. The hello message returned false.");
        return result;
    }

    createEvent(3, "-------------------------------------------------------------------------------------------------------");
    createEvent(2, "Initializing value translation: checking configuration...");
    if((configuration() & DEV_PLUGIN_SUPPORTED_MODULES_P) == 0)
    {
        __result = setLastError(name() + " does not support presentation module.", DEV_PLUGIN_MESSAGE_TYPE_PLUGIN_CONFIG);
        return result;
    }

    createEvent(2, "Translating value \'" + __value + "\' using \'" + name() + "\' \'" + __serviceName + "\' service...");

    QString message = createList(QStringList() << "value" << "translate" << "\'" + __serviceName + "\'" << "\'" + __value + "\'") + ".";
    createEvent(2, "Sending request message...");
    QString response = pluginDataExchange(message, __result);

    if(__result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return result;

    // if succes

    // Checking result of the request
    QStringList pl = checkResult(response, __result);
    if(__result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return result;

    // if succes
    if(pl.count() != 2)
    {
        __result = setLastError("Incorrect format of received translation message.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, "Received response: " + response);
        return result;
    }

    // Parsing models list
    // checking configuration
    QStringList translated = parseMessage(pl.at(1), __result);
    if(__result == DEV_PLUGIN_RESULT_CODE_ERROR)
        return result;

    if(translated.size() > 1)
    {
        __result = setLastError("Incorrect format of received translation.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, "Received response: " + response);
        return result;
    }
    if(translated.size() == 0)
    {
        __result = setLastError("Empty translation.\nCall plugin event listener to see received message.", DEV_PLUGIN_MESSAGE_TYPE_FORMAT);
        createEvent(0, "Received response: " + response);
        return result;
    }

    if(translated.size() == 1)
        result = translated.at(0);

    __result = DEV_PLUGIN_RESULT_CODE_SUCCESS;
    return result;
}
// -----------------------------------------------------------------------------
