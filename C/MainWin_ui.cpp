/*
 *	  $Id: MainWin_ui.cpp,v 1.60.4.6.2.22 2011-07-28 06:51:18 kkr Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>
#include <QDebug>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPainter>
#include <QPrinter>
#include <QMessageBox>
#include <QInputDialog>
#include <QProcess>

#include <QPixmap>

#include <Qt>
#include <QtGui>
#include <QDir>

#include <QDial>
#include <QSpinBox>
#include <QListWidget>
#include <QProgressDialog>

#include <QDomDocument>

#if QT_VERSION >= 0x040300
     #include <QXmlStreamWriter>
#endif

#include "../M/hModel.h"
#include "../M/hModelList.h"

#include "../M/ARD/ARD_Level.h"
#include "../M/ARD/ARD_Property.h"
#include "../M/ARD/ARD_LevelList.h"
#include "../M/ARD/ARD_Dependency.h"
#include "../M/ARD/ARD_PropertyList.h"
#include "../M/ARD/ARD_AttributeList.h"
#include "../M/ARD/ARD_DependencyList.h"

#include "../V/vasProlog.h"
#include "../V/vasPicture.h"
#include "../V/vasXML.h"

#include "../V/ARD/gTPHtree.h"
#include "../V/ARD/gARDlevel.h"
#include "../V/ARD/gARDlevelList.h"
#include "../V/XTT/GTable.h"
#include "../C/XTT/AttributeImport_ui.h"

#include "XTT/widgets/MyGraphicsView.h"
#include "XTT/widgets/MyGraphicsScene.h"
#include "XTT/widgets/StateGraphicsView.h"
#include "XTT/widgets/StateGraphicsScene.h"
#include "XTT/widgets/ErrorsListWidget.h"
#include "XTT/widgets/ExecuteMessagesListWidget.h"

#include "../M/XTT/XTT_Row.h"
#include "../M/XTT/XTT_Cell.h"
#include "../M/XTT/XTT_Table.h"
#include "../M/XTT/XTT_States.h"
#include "../M/XTT/XTT_TableList.h"
#include "../M/XTT/XTT_AttributeGroups.h"
#include "../M/XTT/XTT_ConnectionsList.h"
#include "../M/XTT/XTT_CallbackContainer.h"

#include "../M/hType.h"
#include "../M/hTypeList.h"
#include "../M/hMultipleValue.h"
#include "../M/hMultipleValueResult.h"

#include "ARD/ardAttributeManager.h"

#include "ARD/widgets/ardGraphicsView.h"
#include "ARD/widgets/ardGraphicsScene.h"

#include "XTT/ItemFinder_ui.h"
#include "XTT/CellEditor_ui.h"
#include "XTT/TableEditor_ui.h"
#include "XTT/DeleteTable_ui.h"
#include "XTT/TypeManager_ui.h"
#include "XTT/ColumnEditor_ui.h"
#include "XTT/GroupManager_ui.h"
#include "XTT/AttributeDelete_ui.h"
#include "XTT/AttributeEditor_ui.h"
#include "XTT/AttributeManager_ui.h"
#include "XTT/DeleteConnection_ui.h"
#include "XTT/UnusedAttributes_ui.h"
#include "XTT/ConnectionEditor_ui.h"
#include "XTT/ConnectionManager_ui.h"
#include "XTT/PluginEventListner_ui.h"
#include "XTT/StateSchemaEditor_ui.h"

#include "devPluginInterface/devPlugin.h"
#include "devPluginInterface/devPluginController.h"

#include "../namespaces/ns_ard.h"
#include "../namespaces/ns_xtt.h"
#include "../namespaces/ns_hqed.h"

#include "hqedserver.h"

#include "TextEditor_ui.h"
#include "About_ui.h"
#include "Settings_ui.h"
#include "widgets/sysTrayObject.h"
#include "wizard/Wizard_ui.h"
#include "StartupDialog_ui.h"

#include "MainWin_ui.h"

#include "help/helpengine.h"
#include "help/helpwidget.h"
// -----------------------------------------------------------------------------

#define ARD_DOCKED_WIDGETS 3
// -----------------------------------------------------------------------------

// Main application window.
MainWin_ui* MainWin;
// -----------------------------------------------------------------------------

// #brief Constructor MainWin_ui class.
//
// #param *parent Pointer to parent.
MainWin_ui::MainWin_ui(QMainWindow*)
{
     setupUi(this);
}
// -----------------------------------------------------------------------------

// #brief Function that sets the initial values of the window class
//
// #param __initModel - pointer to the optional initialize model
// #return no values returns
void MainWin_ui::initWindow(hModel* __initModel)
{
     // Initializing model
     if(__initModel != NULL)
     {
          for(int i=0;i<DOCKED_WIDGETS;++i)
               fDockWidgetsVisibleStatus[i] = __initModel->dockWidgetsVisibleStatus(i);

          isARDFile = __initModel->isARDFile();
          fArdFileName = __initModel->ardFileName();
          ard_modelScene = __initModel->ard_modelScene();
          ard_thpScene = __initModel->ard_thpScene();
          fCurrentARDscene = __initModel->ardCurrentARDscene();
          isXTTFile = __initModel->isXTTFile();
          fXttFileName = __initModel->xttFileName();
          xtt_scene = __initModel->xtt_scene();
          xtt_state_scene = __initModel->xtt_state_scene();
     }

     printer = 0;
     appMode = -1;  // to correct setup of ARD docked windows visibility
     publicProgressDialog = NULL;
     
     setAcceptDrops(true);
     setDockNestingEnabled(true);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);

     // ARD
     ard_modelView = new ardGraphicsView(centralwidget);
     ard_modelView->setObjectName(QString::fromUtf8("ard_graphicsView"));
     gridLayout->addWidget(ard_modelView, 0, 0, 1, 1);
     fCurrentARDscene = ard_modelScene;

     // XTT
     xtt_graphicsView = new MyGraphicsView(centralwidget);
     xtt_graphicsView->setObjectName(QString::fromUtf8("graphicsView"));
     xtt_graphicsView->setScene(xtt_scene);
     gridLayout->addWidget(xtt_graphicsView, 0, 0, 1, 1);
     
     action_New->setStatusTip("Create new blank model");
     action_New_from_wizard->setStatusTip("Create new model from wizard");
     action_Open->setStatusTip("Open project...");
     action_Save->setStatusTip("Save project");
     actionSave_as->setStatusTip("Save project under new name...");
     action_Close->setStatusTip("Close program");
     actionEdit_tabele->setStatusTip("Run table editor...");
     actionEdit_attribute->setStatusTip("Run attribute editor...");
     actionNew_tabele->setStatusTip("Create new table dialog...");
     actionEdit_Attribute->setStatusTip("Delete table dialog...");
     actionAbout->setStatusTip("Show information about this program...");
     action_New_attribute->setStatusTip("Create new attribute...");
     actionDelete_attribute->setStatusTip("Delete attribute dialog...");
     action_Attribute_manager->setStatusTip("Run XTT attributes magager...");
     action_Attribute_manager_2->setStatusTip("Run ARD attributes magager...");
     actionGroups_manager->setStatusTip("Run groups manager...");
     actionQLine->setStatusTip("qline");
     actionZoom_in->setStatusTip("Increase zoom factor");
     actionZoom_out->setStatusTip("Decrease zoom factor");
     actionRotate_90->setStatusTip("rot90");
     actionCell_editor->setStatusTip("Run cell editor...");
     action_Add_new_connection->setStatusTip("Add new table dialog...");
     actionConnection_manager->setStatusTip("Run connections manager...");
     action_Edit_connection->setStatusTip("Connections editor...");
     action_Delete_connection->setStatusTip("Delete connections dialog...");
     actionZoom_in_2->setStatusTip("Increase zoom factor");
     actionZoom_out_2->setStatusTip("Decrease zoom factor");
     actionMove_all_left->setStatusTip("Moves all items left");
     actionMove_all_right->setStatusTip("Moves all items right");
     actionMove_all_down->setStatusTip("Moves all items down");
     actionMove_all_up->setStatusTip("Moves all items up");
     actionAdapt_scene_size->setStatusTip("Adapt scene size");
     actionAntialiasing->setStatusTip("On/Off antialiasing");
     actionTextAntialiasing->setStatusTip("On/Off text antialiasing");
     actionSmooth_pixmap_transform->setStatusTip("On/Off smooth pixmap transform");
     actionUnused_attributes->setStatusTip("Show unused attributes...");
     actionNew_with_the_same_attributes->setStatusTip("Cleans current model, but keep defined attributes. This action does not create new model");
     actionRun->setStatusTip("Verify and execute model");
     actionGenerate_PROLOG_code->setStatusTip("Generates the HMR code according to the XTT model");
     actionB_W_view->setStatusTip("Change color mode");
     actionXTTML_file_ver_1_0->setStatusTip("Save model to xttml ver. 1.0 file...");
     actionXTTML_file_ver_2_0->setStatusTip("Save model to xttml ver. 2.0 file...");
     actionXTTML_file_ver_3_0->setStatusTip("Save model to xttml ver. 3.0 file...");
     actionXTTML_file_ver_4_0->setStatusTip("Save model to xttml ver. 4.0 file...");
     actionATTML_file_ver_2_0->setStatusTip("Save attributes to attml ver. 2.0 file...");
     actionAttribute_import->setStatusTip("Import attributes from attml file...");
     actionExoprt_scene_to_SVG_file->setStatusTip("Save scene as SVG picture (Scalable Vector Graphics)");
     actionExoprt_scene_to_PNG_file->setStatusTip("Save scene as PNG picture (Portable Network Graphics)");
     actionExoprt_scene_to_BMP_file->setStatusTip("Save scene as BMP picture (Windows Bitmap)");
     actionExoprt_scene_to_JPG_file->setStatusTip("Save scene as JPG picture (Joint Photographic Experts Group)");
     actionExoprt_scene_to_PPM_file->setStatusTip("Save scene as PPM picture (Portable Pixmap)");
     actionExoprt_scene_to_XBM_file->setStatusTip("Save scene as XBM picture (X11 Bitmap)");
     actionExoprt_scene_to_XPM_file->setStatusTip("Save scene as XPM picture (X11 Pixmap)");
     actionPrint->setStatusTip("Print the scene");
     actionPrint_setup->setStatusTip("Printer setup...");
     actionSettings->setStatusTip("Application settings");
     actionType_manager->setStatusTip("The type manager dialog window");
     actionClear_ARD_model->setStatusTip("Clear the ARD model");
     actionClear_XTT_model->setStatusTip("Clear the XTT model");
     actionClear_both_models->setStatusTip("Clear both ARd and XTT models");
     actionClose_model->setStatusTip("Close current model");
     actionClose_all_models->setStatusTip("Close all models");
     actionClose_all_models_except_current->setStatusTip("Close all models except current");
     actionSave_all->setStatusTip("Saves all the changes in all models");
     actionPlugins_event_listener->setStatusTip("Calls for event listener window dialog");
     actionRun_server->setStatusTip("Starts the HQEd server");
     actionStop_server->setStatusTip("Stops the HQEd server");
     actionLay_out_tables->setText("Tables layout");

     // signals/slots mechanism in action
     connect(action_New_attribute, SIGNAL(triggered()), this, SLOT(AddNewAttribute()));
     connect(actionEdit_attribute, SIGNAL(triggered()), this, SLOT(EditAttribute()));
     connect(actionDelete_attribute, SIGNAL(triggered()), this, SLOT(DeleteAttribute()));
     connect(actionGroups_manager, SIGNAL(triggered()), this, SLOT(GroupManagerShow()));
     connect(action_Attribute_manager, SIGNAL(triggered()), this, SLOT(xttAttributeManagerShow()));
     connect(action_Attribute_manager_2, SIGNAL(triggered()), this, SLOT(ardAttributeManagerShow()));
     connect(actionNew_tabele, SIGNAL(triggered()), this, SLOT(TableEditorShow()));
     connect(actionEdit_tabele, SIGNAL(triggered()), this, SLOT(TableEditorEdit()));
     connect(actionEdit_Attribute, SIGNAL(triggered()), this, SLOT(DeleteTableShow()));
     connect(actionCell_editor, SIGNAL(triggered()), this, SLOT(ShowCellEditor()));
     connect(action_Add_new_connection, SIGNAL(triggered()), this, SLOT(ShowConnEditor()));
     connect(action_Edit_connection, SIGNAL(triggered()), this, SLOT(ShowConnEditorInEditMode()));
     connect(action_Delete_connection, SIGNAL(triggered()), this, SLOT(ShowConnectionDeleteWindow()));
     connect(actionConnection_manager, SIGNAL(triggered()), this, SLOT(ShowConnectionManager()));
     connect(actionAntialiasing, SIGNAL(triggered()), this, SLOT(UpdateRenderHints()));
     connect(actionTextAntialiasing, SIGNAL(triggered()), this, SLOT(UpdateRenderHints()));
     connect(actionSmooth_pixmap_transform, SIGNAL(triggered()), this, SLOT(UpdateRenderHints()));
     connect(this, SIGNAL(destroyed()), this, SLOT(CloseForm()));
     connect(action_Close, SIGNAL(triggered()), this, SLOT(CloseForm()));
     connect(actionUnused_attributes, SIGNAL(triggered()), this, SLOT(ShowUnusedAttributes()));
     connect(actionAbout, SIGNAL(triggered()), this, SLOT(ShowAboutWin()));
     connect(action_New, SIGNAL(triggered()), this, SLOT(NewXttClick()));
     connect(action_New_from_wizard, SIGNAL(triggered()), this, SLOT(NewXttWithWizardClick()));
     connect(actionNew_with_the_same_attributes, SIGNAL(triggered()), this, SLOT(NewWhithOldAttributiesClick()));
     connect(action_Save, SIGNAL(triggered()), this, SLOT(save()));
     connect(actionSave_as, SIGNAL(triggered()), this, SLOT(saveAs_HML_2_0()));
     connect(actionCreate_XTT, SIGNAL(triggered()), this, SLOT(generateXTT()));
     connect(actionGenerate_PROLOG_code, SIGNAL(triggered()), this, SLOT(generatePROLOG()));
     connect(actionRun, SIGNAL(triggered()), this, SLOT(ExecuteModel()));
     connect(actionB_W_view, SIGNAL(triggered()), this, SLOT(SwitchColorMode()));
     //connect(actionAttribute_import, SIGNAL(triggered()), this, SLOT(openATTMLFile()));
     connect(att_export_to_hml_file, SIGNAL(triggered()), this, SLOT(exportAttributesToHML20file()));
     connect(att_import_from_hml_file, SIGNAL(triggered()), this, SLOT(importAttributesFromHML20file()));
     connect(actionExoprt_scene_to_SVG_file, SIGNAL(triggered()), this, SLOT(exportSceneToSVGfile()));
     connect(actionExoprt_scene_to_PNG_file, SIGNAL(triggered()), this, SLOT(exportSceneToPNGfile()));
     connect(actionExoprt_scene_to_BMP_file, SIGNAL(triggered()), this, SLOT(exportSceneToBMPfile()));
     connect(actionExoprt_scene_to_JPG_file, SIGNAL(triggered()), this, SLOT(exportSceneToJPGfile()));
     connect(actionExoprt_scene_to_PPM_file, SIGNAL(triggered()), this, SLOT(exportSceneToPPMfile()));
     connect(actionExoprt_scene_to_XBM_file, SIGNAL(triggered()), this, SLOT(exportSceneToXBMfile()));
     connect(actionExoprt_scene_to_XPM_file, SIGNAL(triggered()), this, SLOT(exportSceneToXPMfile()));
     connect(actionExoprt_TPH_diagram_to_SVG_file, SIGNAL(triggered()), this, SLOT(exportTPHtoSVGfile()));
     connect(actionExoprt_TPH_diagram_to_PNG_file, SIGNAL(triggered()), this, SLOT(exportTPHtoPNGfile()));
     connect(actionExoprt_TPH_diagram_to_BMP_file, SIGNAL(triggered()), this, SLOT(exportTPHtoBMPfile()));
     connect(actionExoprt_TPH_diagram_to_JPG_file, SIGNAL(triggered()), this, SLOT(exportTPHtoJPGfile()));
     connect(actionExoprt_TPH_diagram_to_PPM_file, SIGNAL(triggered()), this, SLOT(exportTPHtoPPMfile()));
     connect(actionExoprt_TPH_diagram_to_XBM_file, SIGNAL(triggered()), this, SLOT(exportTPHtoXBMfile()));
     connect(actionExoprt_TPH_diagram_to_XPM_file, SIGNAL(triggered()), this, SLOT(exportTPHtoXPMfile()));
     
     connect(actionExport_TPH_diagram_to_SVG_file, SIGNAL(triggered()), this, SLOT(exportSTStoSVGfile()));
     connect(actionExport_TPH_diagram_to_PNG_file, SIGNAL(triggered()), this, SLOT(exportSTStoPNGfile()));
     connect(actionExport_TPH_diagram_to_BMP_file, SIGNAL(triggered()), this, SLOT(exportSTStoBMPfile()));
     connect(actionExport_TPH_diagram_to_JPG_file, SIGNAL(triggered()), this, SLOT(exportSTStoJPGfile()));
     connect(actionExport_TPH_diagram_to_PPM_file, SIGNAL(triggered()), this, SLOT(exportSTStoPPMfile()));
     connect(actionExport_TPH_diagram_to_XBM_file, SIGNAL(triggered()), this, SLOT(exportSTStoXBMfile()));
     connect(actionExport_TPH_diagram_to_XPM_file, SIGNAL(triggered()), this, SLOT(exportSTStoXPMfile()));
     
     connect(actionAbout_Qt, SIGNAL(triggered()), qApp, SLOT(aboutQt()));
     connect(actionPrint, SIGNAL(triggered()), this, SLOT(printModel()));
     connect(actionPrint_setup, SIGNAL(triggered()), this, SLOT(printSetup()));
     connect(actionSettings, SIGNAL(triggered()), this, SLOT(showConfigurationDialog()));
     connect(actionARD_editor, SIGNAL(triggered()), this, SLOT(useARDmode()));
     connect(actionXTT_editor, SIGNAL(triggered()), this, SLOT(useXTTmode()));
     connect(actionLay_out_tables, SIGNAL(triggered()), this, SLOT(tablesArrangment()));
     connect(actionType_manager, SIGNAL(triggered()), this, SLOT(typeManagerShow()));
     connect(actionClear_ARD_model, SIGNAL(triggered()), this, SLOT(clearARDmodel()));
     connect(actionClear_XTT_model, SIGNAL(triggered()), this, SLOT(clearXTTmodel()));
     connect(actionClear_both_models, SIGNAL(triggered()), this, SLOT(clearBothNModels()));
     connect(actionClose_model, SIGNAL(triggered()), this, SLOT(closeModel()));
     connect(actionClose_all_models, SIGNAL(triggered()), this, SLOT(closeAllModels()));
     connect(actionClose_all_models_except_current, SIGNAL(triggered()), this, SLOT(closeAllModelsExceptCurrent()));
     connect(actionSave_all, SIGNAL(triggered()), this, SLOT(saveAll()));
     connect(actionPlugins_event_listener, SIGNAL(triggered()), this, SLOT(onPluginsEventListener()));
     connect(actionHQEd_text_editor, SIGNAL(triggered()), this, SLOT(hqedTextEditor()));
     connect(actionAdd_new_group, SIGNAL(triggered()), this, SLOT(onAddNewStateGroup()));
     connect(actionStates_layout, SIGNAL(triggered()), this, SLOT(statesArrangment()));
     actionHQEd_text_editor->setIcon(QIcon(":/all_images/images/texteditor.png"));

     connect(actionAdapt_scene_size, SIGNAL(triggered()), this, SLOT(updateModelScenes()));

     connect(actionRun_server, SIGNAL(triggered()), this, SLOT(hqedServerStart()));
     connect(actionStop_server, SIGNAL(triggered()), this, SLOT(hqedServerStop()));
     connect(hqedServerInstance, SIGNAL(eventListnerEvent(int, QString)), pluginEventListener, SLOT(onNewEvent(int, QString)));

     // Create dock windows and toolbars.
     xtt_createMenu();
     ard_createDockWindows();
     xtt_createDockWindows();
     xtt_createToolBars();
     createStatusBar();
     createHelpWidget();
     getPlugins();
     if(CallbackContainer->loadFormLibrary() != 0)
          QMessageBox::about(this, tr("Error!"),tr("Unable to read Callback Library File."));
     if(Settings->hqedServerAutostart())
          hqedServerStart();

     // Add shortcut to menu.
     actionMove_all_right->setShortcut(Qt::CTRL + Qt::Key_Right);
     actionMove_all_left->setShortcut(Qt::CTRL + Qt::Key_Left);
     actionMove_all_up->setShortcut(Qt::CTRL + Qt::Key_Up);
     actionMove_all_down->setShortcut(Qt::CTRL + Qt::Key_Down);
     actionARD_editor->setShortcut(tr("F11"));
     actionXTT_editor->setShortcut(tr("F12"));

     // Set active render buttons.
     actionAntialiasing->setChecked(true);
     actionTextAntialiasing->setChecked(true);
     actionSmooth_pixmap_transform->setChecked(true);

     xtt_graphicsView->connectScene(xtt_scene);
     xtt_graphicsView->setCacheMode(QGraphicsView::CacheBackground);
     xtt_graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
     xtt_graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
     xtt_graphicsView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
     xtt_scene->AdaptSceneSize();

     ard_modelView->connectScene(ard_modelScene);
     ard_modelView->setCacheMode(QGraphicsView::CacheBackground);
     ard_modelView->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
     ard_modelView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
     ard_modelView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
     ard_modelScene->AdaptSceneSize();

     ard_tphView->connectScene(ard_thpScene);
     ard_tphView->setCacheMode(QGraphicsView::CacheBackground);
     ard_tphView->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
     ard_tphView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
     ard_tphView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
     ard_thpScene->AdaptSceneSize();

     menu_Execute->setTearOffEnabled(true);
     menu_Edit->setTearOffEnabled(true);
     menu_Connections->setTearOffEnabled(true);
     menu_View->setTearOffEnabled(true);
     menu_Help->setTearOffEnabled(true);
     menu_File->setTearOffEnabled(true);
     menuAttribute_export->setTearOffEnabled(true);
     menuExport_scene_view->setTearOffEnabled(true);
     menuTools->setTearOffEnabled(true);
     menu_View_2->setTearOffEnabled(true);
     menuRender_hint->setTearOffEnabled(true);
     menuMode->setTearOffEnabled(true);
     menuExport_TPH_diagram->setTearOffEnabled(true);
     menuTypes->setTearOffEnabled(true);
     menuPlugins->setTearOffEnabled(true);
     menuWindow->setTearOffEnabled(true);
     menuServer->setTearOffEnabled(true);

     // Determine whether tips for various program procedures should be visible.
     fShowARDtip = Settings->showTrayTips();
     fShowXTTtip = Settings->showTrayTips();

     // Set icon in resources.
     trayIcon = new sysTrayObject(this);
     #ifdef Q_OS_WIN32
     if((Settings->showSystemTrayIcon()) && (QSystemTrayIcon::isSystemTrayAvailable()))
          trayIcon->show();
     #endif
     
     setCurrentMode(__initModel->appMode());
     updateWindowTitle();
     formRecentFilesList();     
     
     if(Settings->useStartupDialog())
          showStartupDialog();
}
// -----------------------------------------------------------------------------

// #brief Destructore MainWin_ui class.
MainWin_ui::~MainWin_ui(void)
{
     delete xtt_graphicsView;
     delete xtt_state_graphicsView;
     delete ard_modelView;
     delete ard_tphView;

     if(printer)
          delete printer;

     delete hqedServerInstance;

     delete toolBarMenu;
     delete fileToolBar;
     delete viewToolBar;
     delete tableToolBar;
     delete attributesToolBar;
     delete connectionsToolBar;
     delete executeToolBar;
     delete helpToolBar;
     delete fCurrentARDscene;
     delete dockedWindows;

     delete xtt_runModelMessagesDockWindowControlLabel;
     delete xtt_runModelMessagesDockWindowControlPlay;
     delete xtt_runModelMessagesDockWindowControlStop;
     delete xtt_runModelMessagesDockWindowControlPause;
     delete xtt_runModelMessagesDockWindowControlStepBack;
     delete xtt_runModelMessagesDockWindowControlStepNext;
     delete xtt_runModelMessagesDockWindowControlBegin;
     delete xtt_runModelMessagesDockWindowControlEnd;
     delete xtt_runModelMessagesDockWindowControlInterval;

     delete xtt_runModelMessagesDockWindowContents;
     delete xtt_runModelMessagesDockWindowGridLayout;
     delete xtt_runModelMessagesDockWindowMessagesList;
     delete xtt_runModelMessagesDockWindowSpacerItem;
     delete xtt_runModelMessagesDockWindowInfoCheckBox;
     delete xtt_runModelMessagesDockWindowWarningCheckBox;
     delete xtt_runModelMessagesDockWindowErrorCheckBox;
     delete xtt_runModelMessagesDockWindowLabel;

     delete ard_localizator;
     delete tph_localizator;
     delete xtt_localizator;

     delete ard_tphDockWindow;
     delete ard_localizatorDockWindow;
     delete tph_localizatorDockWindow;
     delete xtt_errorsDockWindow;
     delete xtt_statesDockWindow;
     delete xtt_runModelMessagesDockWindow;
     delete xtt_localizatorDockWindow;
     delete xtt_pluginEventListenerDockWindow;

     delete trayIcon;
     
     if(publicProgressDialog != NULL)
          delete publicProgressDialog;
}
// -----------------------------------------------------------------------------

void MainWin_ui::createHelpWidget()
{
    helpEngine = new HelpEngine("doc/hqedhelp.qhc");
    helpEngine->insertKeyword(focusWidget(), "mainWindow.someButton1");
    helpWidget = new HelpWidget(helpEngine);
    QDockWidget* dock = new QDockWidget(tr("Help"), this);
    dock->setWindowFlags(Qt::WindowStaysOnTopHint);
    dock->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
    dock->setWidget(helpWidget);
    helpWidget->setDockWidget(dock);
    helpWidget->setMainWindow(this);
    helpWidget->setVisible(false);
    addDockWidget(Qt::RightDockWidgetArea,dock);
}

// #brief Function executes when window is showing.
//
// #param event Event parameters.
// #return No return value.
void MainWin_ui::showEvent(QShowEvent*)
{
     if((qApp->arguments().size() > 1) && (QFile::exists(qApp->arguments().last())))
          openFile(qApp->arguments().last());
}
// -----------------------------------------------------------------------------

// #brief Handle event of dragging file on window area and check it data tape.
//
// #param event Pointer to parameters of dragging data on form.
// #return No return value.
void MainWin_ui::dragEnterEvent(QDragEnterEvent *event)
{
     if(event->mimeData()->hasFormat("text/uri-list"))
          event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Handle event of dropping file on window area and check it data tape.
//
// #param event Pointer to parameters of dropping data on form.
// #return No return value.
void MainWin_ui::dropEvent(QDropEvent *event)
{
     QList<QUrl> urlList = event->mimeData()->urls();
     if(urlList.size() > 0)
     {
            QString url = urlList.at(0).toLocalFile();
            openFile(url);
     }

     event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Function that registers plugin in main window
//
// #param __pluginPtr - pointer to the plugin
// #return no values return
void MainWin_ui::registerPlugin(devPlugin* __pluginPtr)
{
     if(__pluginPtr == NULL)
          return;
          
     menuPlugins->addMenu(__pluginPtr->menu());
     if((__pluginPtr->configuration() & DEV_PLUGIN_SUPPORTED_MODULES_S) > 0)
          menu_Execute->insertMenu(actionRun, __pluginPtr->simmenu());
     
     connect(__pluginPtr, SIGNAL(eventListnerEvent(int, QString)), pluginEventListener, SLOT(onNewEvent(int, QString)));
}
// -----------------------------------------------------------------------------

// #brief Function that collects information about plugins
//
// #return no values return
void MainWin_ui::getPlugins(void)
{
     for(int i=0;i<devPC->plugins()->size();++i)
          registerPlugin(devPC->plugins()->at(i));
}
// -----------------------------------------------------------------------------

// #brief Function that returns a pointer to one of the main window menus
//
// #param __menuIndex - index of menu
// #return pointer to one of the main window menus
QMenu* MainWin_ui::getMenu(int __menuIndex)
{
     int menuCount = 16;
     if((__menuIndex < 0) || (__menuIndex >= menuCount))
          return NULL;
     
     QMenu** menus = new QMenu*[menuCount];
     
     menus[0] = menu_File;
     menus[1] = menuAttribute_export;
     menus[2] = menuExport_scene_view;
     menus[3] = menuExport_TPH_diagram;
     menus[4] = menuMode;
     menus[5] = menu_View_2;
     menus[6] = menuRender_hint;
     menus[7] = menuTypes;
     menus[8] = menu_View;
     menus[9] = menu_Edit;
     menus[10] = menu_Connections;
     menus[11] = menuPlugins;
     menus[12] = menu_Execute;
     menus[13] = menuTools;
     menus[14] = menuWindow;
     menus[15] = menu_Help;
     
     QMenu* result = menus[__menuIndex];
     delete [] menus;

     return result;
}
// -----------------------------------------------------------------------------

// #brief Create menu for XTT mode.
//
// #return No return value.
void MainWin_ui::xtt_createMenu(void)
{
     toolBarMenu = new QMenu("Tollbar");
     menu_View_2->insertMenu(actionZoom_in_2, toolBarMenu);
     menu_View_2->insertSeparator(actionZoom_in_2);

     dockedWindows = new QMenu("Docked windows");
     menu_View_2->insertMenu(actionZoom_in_2, dockedWindows);
     menu_View_2->insertSeparator(actionZoom_in_2);
}
// -----------------------------------------------------------------------------

// #brief Create toolbars for XTT mode.
//
// #return No return value.
void MainWin_ui::xtt_createToolBars(void)
{
     // Menu File.
     fileToolBar = addToolBar(tr("File"));
     fileToolBar->addAction(action_Close);
     fileToolBar->addSeparator();
     fileToolBar->addAction(action_New);
     fileToolBar->addAction(action_Open);
     fileToolBar->addAction(action_Save);
     fileToolBar->addAction(actionClose_model);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionExoprt_scene_to_SVG_file);
     fileToolBar->addSeparator();
     fileToolBar->addAction(actionPrint);
     fileToolBar->addAction(actionPrint_setup);
     toolBarMenu->addAction(fileToolBar->toggleViewAction());

     // Menu view.
     viewToolBar = addToolBar(tr("View"));
     viewToolBar->addAction(actionZoom_in_2);
     viewToolBar->addAction(actionZoom_out_2);
     viewToolBar->addSeparator();
     viewToolBar->addAction(actionMove_all_left);
     viewToolBar->addAction(actionMove_all_right);
     viewToolBar->addAction(actionMove_all_up);
     viewToolBar->addAction(actionMove_all_down);
     viewToolBar->addSeparator();
     viewToolBar->addAction(actionAdapt_scene_size);
     viewToolBar->addSeparator();
     toolBarMenu->addAction(viewToolBar->toggleViewAction());

     // Menu of types.
     typeToolBar = addToolBar(tr("Type"));
     typeToolBar->addAction(actionType_manager);
     toolBarMenu->addAction(typeToolBar->toggleViewAction());

     // Menu of attributes.
     attributesToolBar = addToolBar(tr("Atributes"));
     attributesToolBar->addAction(action_New_attribute);
     attributesToolBar->addAction(actionEdit_attribute);
     attributesToolBar->addAction(actionDelete_attribute);
     attributesToolBar->addSeparator();
     attributesToolBar->addAction(actionUnused_attributes);
     attributesToolBar->addSeparator();
     attributesToolBar->addAction(action_Attribute_manager);
     attributesToolBar->addAction(action_Attribute_manager_2);
     attributesToolBar->addSeparator();
     attributesToolBar->addAction(actionGroups_manager);
     toolBarMenu->addAction(attributesToolBar->toggleViewAction());

     // Menu of tabels.
     tableToolBar = addToolBar(tr("Table"));
     tableToolBar->addAction(actionNew_tabele);
     tableToolBar->addAction(actionEdit_tabele);
     tableToolBar->addAction(actionEdit_Attribute);
     tableToolBar->addSeparator();
     tableToolBar->addAction(actionCell_editor);
     toolBarMenu->addAction(tableToolBar->toggleViewAction());

     // Menu of connections.
     connectionsToolBar = addToolBar(tr("Connections"));
     connectionsToolBar->addAction(action_Add_new_connection);
     connectionsToolBar->addAction(action_Edit_connection);
     connectionsToolBar->addAction(action_Delete_connection);
     connectionsToolBar->addSeparator();
     connectionsToolBar->addAction(actionConnection_manager);
     toolBarMenu->addAction(connectionsToolBar->toggleViewAction());

     // Menu of runtime.
     executeToolBar = addToolBar(tr("Execute"));
     executeToolBar->addAction(actionRun);
     executeToolBar->addSeparator();
     executeToolBar->addAction(actionGenerate_PROLOG_code);
     toolBarMenu->addAction(executeToolBar->toggleViewAction());

     // Menu of tools.
     toolsToolBar = addToolBar(tr("Settings"));
     toolsToolBar->addAction(actionSettings);
     executeToolBar->addSeparator();
     toolsToolBar->addAction(actionHQEd_text_editor);
     toolBarMenu->addAction(toolsToolBar->toggleViewAction());

     // Menu of help.
     helpToolBar = addToolBar(tr("Help"));
     helpToolBar->addAction(actionAbout);
     toolBarMenu->addAction(helpToolBar->toggleViewAction());

     addToolBar(Qt::LeftToolBarArea, viewToolBar);
}
// -----------------------------------------------------------------------------

// #brief Create dock windows for ARD mode.
//
// #return No return value.
void MainWin_ui::ard_createDockWindows(void)
{
     // Create windows for error raporting.
     ard_tphDockWindow = new QDockWidget(tr("TPH diagram"), this);
     ard_tphView = new ardGraphicsView(ard_tphDockWindow);
     ard_tphDockWindow->setWidget(ard_tphView);
     addDockWidget(Qt::LeftDockWidgetArea, ard_tphDockWindow);
     dockedWindows->addAction(ard_tphDockWindow->toggleViewAction());
     
     // Create TPH windows for localization.
     tph_localizatorDockWindow = new QDockWidget(tr("TPH localizator"), this);
     tph_localizatorDockWindow->setWindowFlags(Qt::WindowStaysOnTopHint);
     tph_localizatorDockWindow->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
     tph_localizator = new GSceneLocalizator(NULL);
     tph_localizatorDockWindow->setWidget(tph_localizator);
     tph_localizator->setScene(ard_thpScene);
     tph_localizator->update();
     addDockWidget(Qt::BottomDockWidgetArea, tph_localizatorDockWindow);
     dockedWindows->addAction(tph_localizatorDockWindow->toggleViewAction());
     connect(ard_tphView, SIGNAL(viewChanged(QRectF)), tph_localizator, SLOT(setFrameRect(QRectF)));
     connect(tph_localizator, SIGNAL(framePositionChanged(QRectF)), ard_tphView, SLOT(setViewRect(QRectF)));
     
     // Create ARD windows for localization.
     ard_localizatorDockWindow = new QDockWidget(tr("ARD localizator"), this);
     ard_localizatorDockWindow->setWindowFlags(Qt::WindowStaysOnTopHint);
     ard_localizatorDockWindow->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
     ard_localizator = new GSceneLocalizator(NULL);
     ard_localizatorDockWindow->setWidget(ard_localizator);
     ard_localizator->setScene(ard_modelScene);
     ard_localizator->update();
     addDockWidget(Qt::BottomDockWidgetArea, ard_localizatorDockWindow);
     dockedWindows->addAction(ard_localizatorDockWindow->toggleViewAction());
     connect(ard_modelView, SIGNAL(viewChanged(QRectF)), ard_localizator, SLOT(setFrameRect(QRectF)));
     connect(ard_localizator, SIGNAL(framePositionChanged(QRectF)), ard_modelView, SLOT(setViewRect(QRectF)));
}
// -----------------------------------------------------------------------------

// #brief Create dock windows for XTT mode.
//
// #return No return value.
void MainWin_ui::xtt_createDockWindows(void)
{
     // Create windows for runtime raporting.
     bool toolButtonsAutoRaise = false;
     xtt_runModelMessagesDockWindow = new QDockWidget(tr("Execute messages"), this);
     xtt_runModelMessagesDockWindow->setObjectName(QString::fromUtf8("Execute messages"));
     xtt_runModelMessagesDockWindow->setGeometry(QRect(40, 0, 261, 221));
     xtt_runModelMessagesDockWindowContents = new QWidget(xtt_runModelMessagesDockWindow);
     xtt_runModelMessagesDockWindowContents->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowContents"));
     xtt_runModelMessagesDockWindowMessagesList = new ExecMsgListWidget(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowMessagesList->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowMessagesList"));
     xtt_runModelMessagesDockWindowMessagesList->setSelectionMode(QAbstractItemView::ExtendedSelection);
     xtt_runModelMessagesDockWindowInfoCheckBox = new QCheckBox(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowInfoCheckBox->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowInfoCheckBox"));
     xtt_runModelMessagesDockWindowInfoCheckBox->setIcon(QIcon(QString::fromUtf8(":/all_images/images/information_small.png")));
     xtt_runModelMessagesDockWindowWarningCheckBox = new QCheckBox(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowWarningCheckBox->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowWarningCheckBox"));
     xtt_runModelMessagesDockWindowWarningCheckBox->setIcon(QIcon(QString::fromUtf8(":/all_images/images/warning_small.png")));
     xtt_runModelMessagesDockWindowLabel = new QLabel(this);
     xtt_runModelMessagesDockWindowLabel->setText("Message filter:");
     xtt_runModelMessagesDockWindowLabel->setMargin(5);
     xtt_runModelMessagesDockWindowErrorCheckBox = new QCheckBox(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowErrorCheckBox->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowErrorCheckBox"));
     xtt_runModelMessagesDockWindowErrorCheckBox->setIcon(QIcon(QString::fromUtf8(":/all_images/images/critical_small.png")));
     xtt_runModelMessagesDockWindowInfoCheckBox->setText(QString());
     xtt_runModelMessagesDockWindowWarningCheckBox->setText(QString());
     xtt_runModelMessagesDockWindowErrorCheckBox->setText(QString());
     xtt_runModelMessagesDockWindowInfoCheckBox->setChecked(true);
     xtt_runModelMessagesDockWindowWarningCheckBox->setChecked(true);
     xtt_runModelMessagesDockWindowErrorCheckBox->setChecked(true);
     xtt_runModelMessagesDockWindowErrorCheckBox->setIconSize(QSize(18, 18));
     xtt_runModelMessagesDockWindowWarningCheckBox->setIconSize(QSize(18, 18));
     xtt_runModelMessagesDockWindowInfoCheckBox->setIconSize(QSize(18, 18));
     xtt_runModelMessagesDockWindowSpacerItem = new QSpacerItem(131, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);
     
     xtt_runModelMessagesDockWindowControlLabel = new QLabel(this);
     xtt_runModelMessagesDockWindowControlLabel->setText("Message control:");
     xtt_runModelMessagesDockWindowControlLabel->setMargin(5);

     xtt_runModelMessagesDockWindowControlPlay = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlPlay->setIcon(QIcon(QString::fromUtf8(":/all_images/images/greenPlay.png")));
     xtt_runModelMessagesDockWindowControlPlay->setToolTip("Play \n    Jump to the next message automatically after interval");
     xtt_runModelMessagesDockWindowControlPlay->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlStop = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlStop->setIcon(QIcon(QString::fromUtf8(":/all_images/images/redStop.png")));
     xtt_runModelMessagesDockWindowControlStop->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlStop->setToolTip("Stop");
     xtt_runModelMessagesDockWindowControlPause = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlPause->setIcon(QIcon(QString::fromUtf8(":/all_images/images/greenPause.png")));
     xtt_runModelMessagesDockWindowControlPause->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlPause->setToolTip("Pause");
     xtt_runModelMessagesDockWindowControlStepBack = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlStepBack->setIcon(QIcon(QString::fromUtf8(":/all_images/images/greenStepBack.png")));
     xtt_runModelMessagesDockWindowControlStepBack->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlStepBack->setToolTip("Jump to the previous message and stop");
     xtt_runModelMessagesDockWindowControlStepNext = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlStepNext->setIcon(QIcon(QString::fromUtf8(":/all_images/images/greenStepForward.png")));
     xtt_runModelMessagesDockWindowControlStepNext->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlStepNext->setToolTip("Jump to the next message and stop");
     xtt_runModelMessagesDockWindowControlBegin = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlBegin->setIcon(QIcon(QString::fromUtf8(":/all_images/images/yellowBegin.png")));
     xtt_runModelMessagesDockWindowControlBegin->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlBegin->setToolTip("Jump to the first message and stop");
     xtt_runModelMessagesDockWindowControlEnd = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlEnd->setIcon(QIcon(QString::fromUtf8(":/all_images/images/yellowEnd.png")));
     xtt_runModelMessagesDockWindowControlEnd->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlEnd->setToolTip("Jump to the last message and stop");
     xtt_runModelMessagesDockWindowControlInterval = new QToolButton(xtt_runModelMessagesDockWindowContents);
     xtt_runModelMessagesDockWindowControlInterval->setIcon(QIcon(QString::fromUtf8(":/all_images/images/interval.png")));
     xtt_runModelMessagesDockWindowControlInterval->setAutoRaise(toolButtonsAutoRaise);
     xtt_runModelMessagesDockWindowControlInterval->setToolTip("Set the interval for play (current 1.00s)");
     
     xtt_runModelMessagesDockWindowGridLayout = new QGridLayout(xtt_runModelMessagesDockWindowContents);
     #ifndef Q_OS_MAC
          xtt_runModelMessagesDockWindowGridLayout->setSpacing(6);
     #endif
     #ifndef Q_OS_MAC
          xtt_runModelMessagesDockWindowGridLayout->setMargin(9);
     #endif
     #ifdef __USE_SVG
          xtt_runModelMessagesDockWindowGridLayout->setContentsMargins(0, 0, 0, 0);
     #endif
     hqed::setupLayout(xtt_runModelMessagesDockWindowGridLayout);
     xtt_runModelMessagesDockWindowGridLayout->setObjectName(QString::fromUtf8("xtt_runModelMessagesDockWindowGridLayout"));
     //xtt_runModelMessagesDockWindowGridLayout->addItem(xtt_runModelMessagesDockWindowSpacerItem,        0, 9, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowLabel,           0, 0, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowErrorCheckBox,   0, 1, 1, 2);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowWarningCheckBox, 0, 3, 1, 2);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowInfoCheckBox,    0, 5, 1, 2);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlLabel,    1, 0, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlBegin,    1, 1, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlStepBack, 1, 2, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlPlay,     1, 3, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlPause,    1, 4, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlStop,     1, 5, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlStepNext, 1, 6, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlEnd,      1, 7, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowControlInterval, 1, 9, 1, 1);
     xtt_runModelMessagesDockWindowGridLayout->addWidget(xtt_runModelMessagesDockWindowMessagesList,    2, 0, 1, 10);
     xtt_runModelMessagesDockWindowGridLayout->setColumnStretch(8, 1);
     
     xtt_runModelMessagesDockWindow->setWidget(xtt_runModelMessagesDockWindowContents);
     connect(xtt_runModelMessagesDockWindowErrorCheckBox, SIGNAL(toggled(bool)), xtt_runModelMessagesDockWindowMessagesList, SLOT(showErrors(bool)));
     connect(xtt_runModelMessagesDockWindowWarningCheckBox, SIGNAL(toggled(bool)), xtt_runModelMessagesDockWindowMessagesList, SLOT(showWarnings(bool)));
     connect(xtt_runModelMessagesDockWindowInfoCheckBox, SIGNAL(toggled(bool)), xtt_runModelMessagesDockWindowMessagesList, SLOT(showInformations(bool)));
     
     connect(xtt_runModelMessagesDockWindowControlBegin, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onFirstMsg()));
     connect(xtt_runModelMessagesDockWindowControlStepBack, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onStepBack()));
     connect(xtt_runModelMessagesDockWindowControlPlay, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onPlay()));
     connect(xtt_runModelMessagesDockWindowControlPause, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onPause()));
     connect(xtt_runModelMessagesDockWindowControlStop, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onStop()));
     connect(xtt_runModelMessagesDockWindowControlStepNext, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onStepNext()));
     connect(xtt_runModelMessagesDockWindowControlEnd, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onLastMsg()));
     connect(xtt_runModelMessagesDockWindowControlInterval, SIGNAL(clicked()), xtt_runModelMessagesDockWindowMessagesList, SLOT(onIntervalChange()));
     connect(xtt_runModelMessagesDockWindowMessagesList, SIGNAL(intervalChanged(int)), this, SLOT(xttRunModelMessagesDockWindowTimerIntervalChanged(int)));
     dockedWindows->addAction(xtt_runModelMessagesDockWindow->toggleViewAction());
     
     // Create windows for error raporting.
     xtt_errorsDockWindow = new QDockWidget(tr("Errors"), this);
     xtt_errorsDockWindow->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
     xtt_errorsList = new ErrorsListWidget(xtt_errorsDockWindow);
     xtt_errorsList->setSelectionMode(QAbstractItemView::ExtendedSelection);
     xtt_errorsDockWindow->setWidget(xtt_errorsList);
     dockedWindows->addAction(xtt_errorsDockWindow->toggleViewAction());
     
     // Create windows for states displaying
     xtt_statesDockWindow = new QDockWidget(tr("States"), this);
     xtt_statesDockWindow->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea | Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
     xtt_state_graphicsView = new StateGraphicsView(xtt_state_scene, xtt_statesDockWindow);
     xtt_state_graphicsView->setCacheMode(QGraphicsView::CacheBackground);
     xtt_state_graphicsView->setRenderHints(QPainter::Antialiasing | QPainter::TextAntialiasing | QPainter::SmoothPixmapTransform);
     xtt_state_graphicsView->setTransformationAnchor(QGraphicsView::AnchorUnderMouse);
     xtt_state_graphicsView->setResizeAnchor(QGraphicsView::AnchorViewCenter);
     xtt_statesDockWindow->setWidget(xtt_state_graphicsView);
     dockedWindows->addAction(xtt_statesDockWindow->toggleViewAction());

     // Create windows for localization.
     xtt_localizatorDockWindow = new QDockWidget(tr("Localizator"), this);
     xtt_localizatorDockWindow->setWindowFlags(Qt::WindowStaysOnTopHint);
     xtt_localizatorDockWindow->setAllowedAreas(Qt::TopDockWidgetArea | Qt::BottomDockWidgetArea);
     xtt_localizator = new GSceneLocalizator(NULL);
     xtt_localizatorDockWindow->setWidget(xtt_localizator);
     xtt_localizator->setScene(xtt_scene);
     xtt_localizator->update();
     dockedWindows->addAction(xtt_localizatorDockWindow->toggleViewAction());
     connect(xtt_graphicsView, SIGNAL(viewChanged(QRectF)), xtt_localizator, SLOT(setFrameRect(QRectF)));              ///< TREBA PODLACZYC SYGNAL
     connect(xtt_localizator, SIGNAL(framePositionChanged(QRectF)), xtt_graphicsView, SLOT(setViewRect(QRectF)));      ///< TREBA PODLACZYC SYGNAL

     // Creating itemFinder dock window
     xtt_itemFinderDockWindow = new QDockWidget(tr("Item finder"), this);
     xtt_itemFinderDockWindow->setAllowedAreas(Qt::AllDockWidgetAreas);
     xtt_itemFinderDockWindow->setWidget(ItemFinderWin);
     addDockWidget(Qt::LeftDockWidgetArea, xtt_itemFinderDockWindow);
     QAction* ifa = xtt_itemFinderDockWindow->toggleViewAction();
     ifa->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_F));
     dockedWindows->addAction(ifa);
     
     // Creating plugin event listener docked window
     xtt_pluginEventListenerDockWindow = new QDockWidget(tr("Plugin event listener"), this);
     xtt_pluginEventListenerDockWindow->setAllowedAreas(Qt::AllDockWidgetAreas);
     xtt_pluginEventListenerDockWindow->setWidget(pluginEventListener);
     QAction* pela = xtt_pluginEventListenerDockWindow->toggleViewAction();
     pela->setShortcut(QKeySequence(Qt::CTRL + Qt::Key_P));
     dockedWindows->addAction(pela);
     
     addDockWidget(Qt::BottomDockWidgetArea, xtt_errorsDockWindow);
     addDockWidget(Qt::BottomDockWidgetArea, xtt_runModelMessagesDockWindow);
     tabifyDockWidget(xtt_runModelMessagesDockWindow, xtt_errorsDockWindow);
     addDockWidget(Qt::BottomDockWidgetArea, xtt_pluginEventListenerDockWindow);
     tabifyDockWidget(xtt_pluginEventListenerDockWindow, xtt_runModelMessagesDockWindow);
     addDockWidget(Qt::BottomDockWidgetArea, xtt_localizatorDockWindow);
     addDockWidget(Qt::LeftDockWidgetArea, xtt_statesDockWindow);
}
// -----------------------------------------------------------------------------

// #brief Create statusbar.
//
// #return No return value.
void MainWin_ui::createStatusBar(void)
{
     statusBar()->showMessage(tr("Ready"));
}
// -----------------------------------------------------------------------------

// #brief Function emited when timer interval has been changed
//
// #return No return value.
void MainWin_ui::xttRunModelMessagesDockWindowTimerIntervalChanged(int __newInterval)
{
     double secs = (int)__newInterval/1000.;
     xtt_runModelMessagesDockWindowControlInterval->setToolTip("Set the interval for play (current " + QString::number(secs, 'f', 2) + " s)");
}
// -----------------------------------------------------------------------------

// #brief Refreshes the content of the model scenes
//
// #return no values return
void MainWin_ui::updateModelScenes(void)
{
     ard_tphView->update();
     ard_modelView->update();
     xtt_graphicsView->update();
     tph_localizator->update();
     ard_localizator->update();
     xtt_localizator->update();
     ard_modelScene->update();
     ard_thpScene->update();
     xtt_scene->update();
     xtt_state_scene->update();

     ard_modelScene->AdaptSceneSize();
     ard_thpScene->AdaptSceneSize();
     xtt_scene->AdaptSceneSize();
     xtt_state_scene->AdaptSceneSize();
}
// -----------------------------------------------------------------------------

// #brief Stores current values to the given model
//
// #param __model - pointer to the model
// #return no values return
void MainWin_ui::updateModel(hModel* __model)
{
     // Zapisanie ustawien
     if(__model == NULL)
          return;
     
     __model->setAppMode(appMode);
     for(int i=0;i<DOCKED_WIDGETS;++i)
          __model->setDockWidgetsVisibleStatus(i, fDockWidgetsVisibleStatus[i]);
     __model->setARDisFile(isARDFile);
     __model->setXTTisFile(isXTTFile);
     __model->setARDfileName(fArdFileName);
     __model->setXTTfileName(fXttFileName);
}
// -----------------------------------------------------------------------------

// #brief Switches beetween two models
//
// #param __current - pointer to the current model
// #param __destination - pointer to the destination model model
// #return no values return
void MainWin_ui::switchModels(hModel* __current, hModel* __destination)
{
     updateModel(__current);
     
     for(int i=0;i<DOCKED_WIDGETS;++i)
          fDockWidgetsVisibleStatus[i] = __destination->dockWidgetsVisibleStatus(i);

     isARDFile = __destination->isARDFile();
     fArdFileName = __destination->ardFileName();
     ard_modelScene = __destination->ard_modelScene();
     ard_thpScene = __destination->ard_thpScene();
     fCurrentARDscene = __destination->ardCurrentARDscene();

     isXTTFile = __destination->isXTTFile();
     fXttFileName = __destination->xttFileName();
     xtt_scene = __destination->xtt_scene();
     xtt_state_scene = __destination->xtt_state_scene();

     setCurrentMode(__destination->appMode());
     
     // Updating views
     ard_modelView->connectScene(ard_modelScene);
     ard_tphView->connectScene(ard_thpScene);
     xtt_graphicsView->connectScene(xtt_scene);
     xtt_state_graphicsView->connectScene(xtt_state_scene);
     ard_localizator->setScene(ard_modelScene);
     tph_localizator->setScene(ard_thpScene);
     xtt_localizator->setScene(xtt_scene);

     updateModelScenes();
}
// -----------------------------------------------------------------------------

// #brief Get current mode of application.
//
// #return 0 - editor ARD, 1 - editor XTT.
int MainWin_ui::currentMode(void)
{
     return appMode;
}
// -----------------------------------------------------------------------------

// #brief Set current mode of application.
//
// #param _newMode New mode of application: 0 - editor ARD, 1 - editor XTT.
// #return No return value.
void MainWin_ui::setCurrentMode(int _newMode)
{
     if((_newMode < 0) || (_newMode > 1))
          return;

     // Saving visibility status
     if(appMode == ARD_MODE)
     {
          fDockWidgetsVisibleStatus[0] = ard_tphDockWindow->isVisible();
          fDockWidgetsVisibleStatus[5] = ard_localizatorDockWindow->isVisible();
          fDockWidgetsVisibleStatus[6] = tph_localizatorDockWindow->isVisible();
     }
     if(appMode == XTT_MODE)
     {
          fDockWidgetsVisibleStatus[1] = xtt_itemFinderDockWindow->isVisible();
          fDockWidgetsVisibleStatus[2] = xtt_errorsDockWindow->isVisible();
          fDockWidgetsVisibleStatus[3] = xtt_runModelMessagesDockWindow->isVisible();
          fDockWidgetsVisibleStatus[4] = xtt_localizatorDockWindow->isVisible();
          fDockWidgetsVisibleStatus[7] = xtt_pluginEventListenerDockWindow->isVisible();
          fDockWidgetsVisibleStatus[8] = xtt_statesDockWindow->isVisible();
     }

     ard_modelView->setVisible(_newMode == ARD_MODE);
     ard_tphDockWindow->setVisible((_newMode == ARD_MODE) && (fDockWidgetsVisibleStatus[0]));
     ard_localizatorDockWindow->setVisible((_newMode == ARD_MODE) && (fDockWidgetsVisibleStatus[5]));
     tph_localizatorDockWindow->setVisible((_newMode == ARD_MODE) && (fDockWidgetsVisibleStatus[6]));
     actionARD_editor->setChecked(_newMode == ARD_MODE);
     action_Attribute_manager_2->setVisible(_newMode == ARD_MODE);

     actionExoprt_TPH_diagram_to_SVG_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_BMP_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_PNG_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_JPG_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_PPM_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_XBM_file->setVisible(_newMode == ARD_MODE);
     actionExoprt_TPH_diagram_to_XPM_file->setVisible(_newMode == ARD_MODE);
     actionExport_TPH_diagram_to_SVG_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_BMP_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_PNG_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_JPG_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_PPM_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_XBM_file->setVisible(_newMode == XTT_MODE);
     actionExport_TPH_diagram_to_XPM_file->setVisible(_newMode == XTT_MODE);

     actionGroups_manager->setVisible(_newMode == XTT_MODE);
     
     actionCreate_XTT->setVisible(_newMode == ARD_MODE);
     actionARDML_file_ver_1_0->setVisible(_newMode == ARD_MODE);

     //action_New->setVisible(_newMode == XTT_MODE);
     att_export_to_hml_file->setVisible(_newMode == XTT_MODE);
     xtt_graphicsView->setVisible(_newMode == XTT_MODE);
     xtt_itemFinderDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[1]));
     xtt_errorsDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[2]));
     xtt_runModelMessagesDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[3]));
     xtt_localizatorDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[4]));
     xtt_pluginEventListenerDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[7]));
     xtt_statesDockWindow->setVisible((_newMode == XTT_MODE) && (fDockWidgetsVisibleStatus[8]));
     actionXTT_editor->setChecked(_newMode == XTT_MODE);
     tableToolBar->setVisible(_newMode == XTT_MODE);
     //attributesToolBar->setVisible(_newMode == XTT_MODE);
     connectionsToolBar->setVisible(_newMode == XTT_MODE);
     executeToolBar->setVisible(_newMode == XTT_MODE);
     actionRun->setVisible(_newMode == XTT_MODE);
     actionGenerate_PROLOG_code->setVisible(_newMode == XTT_MODE);
     actionNew_with_the_same_attributes->setVisible(_newMode == XTT_MODE);
     //action_Save->setVisible(_newMode == XTT_MODE);
     //actionSave_as->setVisible(_newMode == XTT_MODE);
     action_New_attribute->setVisible(_newMode == XTT_MODE);
     actionEdit_attribute->setVisible(_newMode == XTT_MODE);
     actionDelete_attribute->setVisible(_newMode == XTT_MODE);
     actionUnused_attributes->setVisible(_newMode == XTT_MODE);
     action_Attribute_manager->setVisible(_newMode == XTT_MODE);

     for(int i=0;i<dockedWindows->actions().size();++i)
          dockedWindows->actions().at(i)->setVisible(((i < ARD_DOCKED_WIDGETS) && (_newMode == ARD_MODE)) || ((i >= ARD_DOCKED_WIDGETS) && (_newMode == XTT_MODE)));
     tabifyDockWidget(xtt_errorsDockWindow, xtt_runModelMessagesDockWindow);

     // Disconnect slots.
     disconnect(action_Open, 0, 0, 0);
     disconnect(action_New, 0, 0, 0);
     disconnect(actionZoom_out_2, SIGNAL(triggered()), 0, 0);
     disconnect(actionZoom_in_2, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_right, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_left, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_up, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_down, SIGNAL(triggered()), 0, 0);

     if(_newMode == ARD_MODE)
     {
          setWindowTitle("HQEd - [" + fArdFileName + "]");
          menu_Execute->setTitle("&XTT");
          connect(action_Open, SIGNAL(triggered()), this, SLOT(openARDFileDialog()));
          connect(action_New, SIGNAL(triggered()), this, SLOT(NewArdClick()));

          connect(actionZoom_out_2, SIGNAL(triggered()), fCurrentARDscene, SLOT(zout()));
          connect(actionZoom_in_2, SIGNAL(triggered()), fCurrentARDscene, SLOT(zin()));
          connect(actionMove_all_right, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsRight()));
          connect(actionMove_all_left, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsLeft()));
          connect(actionMove_all_up, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsUp()));
          connect(actionMove_all_down, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsDown()));

          menuExport_TPH_diagram->setTitle("Export TPH diagram");
          //menu_View->setTitle("");
          menu_Edit->setTitle("");
          menuState->setTitle("");
          menu_Connections->setTitle("");

          // Show tips for ARD mode.
          if(((QSystemTrayIcon::isSystemTrayAvailable())) && (QSystemTrayIcon::supportsMessages()) && (Settings->showSystemTrayIcon()) && (fShowARDtip))
          {
               fShowARDtip = false;
               trayIcon->drawTip(ARD_MODE);
               trayIcon->showDrawedTip();
          }
     }

     if(_newMode == XTT_MODE)
     {
          updateWindowTitle();
          menu_Execute->setTitle("&Execute");
          connect(action_Open, SIGNAL(triggered()), this, SLOT(openXTTFileDialog()));
          connect(action_New, SIGNAL(triggered()), this, SLOT(NewXttClick()));

          connect(actionZoom_out_2, SIGNAL(triggered()), xtt_scene, SLOT(zout()));
          connect(actionZoom_in_2, SIGNAL(triggered()), xtt_scene, SLOT(zin()));
          connect(actionMove_all_right, SIGNAL(triggered()), xtt_scene, SLOT(MoveItemsRight()));
          connect(actionMove_all_left, SIGNAL(triggered()), xtt_scene, SLOT(MoveItemsLeft()));
          connect(actionMove_all_up, SIGNAL(triggered()), xtt_scene, SLOT(MoveItemsUp()));
          connect(actionMove_all_down, SIGNAL(triggered()), xtt_scene, SLOT(MoveItemsDown()));

          menuExport_TPH_diagram->setTitle("Export state diagram");
          menu_View->setTitle("&Attributes");
          menu_Edit->setTitle("&Table");
          menuState->setTitle("&States");
          menu_Connections->setTitle("&Connections");

          // Show tips for ARD mode.
          if(((QSystemTrayIcon::isSystemTrayAvailable())) && (QSystemTrayIcon::supportsMessages()) && (Settings->showSystemTrayIcon()) && (fShowXTTtip))
          {
               fShowXTTtip = false;
               trayIcon->drawTip(XTT_MODE);
               trayIcon->showDrawedTip();
          }
     }

     appMode = _newMode;

     // Uaktualnienie trybu kolorow
     actionB_W_view->setChecked(Settings->colorMode(_newMode));
     SwitchColorMode();

     // Uaktualnienie flag renderingu
     actionAntialiasing->setChecked(Settings->useAntialiasing(_newMode));
     actionTextAntialiasing->setChecked(Settings->useTextAntialiasing(_newMode));
     actionSmooth_pixmap_transform->setChecked(Settings->useSmoothPixmapTransform(_newMode));
     UpdateRenderHints();
}
// -----------------------------------------------------------------------------

// #brief Switch application in ARD mode.
//
// #return No return value.
void MainWin_ui::useARDmode(void)
{
     setCurrentMode(ARD_MODE);
}
// -----------------------------------------------------------------------------

// #brief Switch application in XTT mode.
//
// #return No return value.
void MainWin_ui::useXTTmode(void)
{
     setCurrentMode(XTT_MODE);
}
// -----------------------------------------------------------------------------

// #brief Set current ART scene.
//
// #param ardncs Pointer to current scene ARD.
// #return No return value..
void MainWin_ui::setCurrentARDscene(ardGraphicsScene* ardncs)
{
     if(fCurrentARDscene == ardncs)
          return;

     fCurrentARDscene = ardncs;

     // Disconnect slot.
     disconnect(actionZoom_out_2, SIGNAL(triggered()), 0, 0);
     disconnect(actionZoom_in_2, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_right, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_left, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_up, SIGNAL(triggered()), 0, 0);
     disconnect(actionMove_all_down, SIGNAL(triggered()), 0, 0);

     // Create new slots.
     connect(actionZoom_out_2, SIGNAL(triggered()), fCurrentARDscene, SLOT(zout()));
     connect(actionZoom_in_2, SIGNAL(triggered()), fCurrentARDscene, SLOT(zin()));
     connect(actionMove_all_right, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsRight()));
     connect(actionMove_all_left, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsLeft()));
     connect(actionMove_all_up, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsUp()));
     connect(actionMove_all_down, SIGNAL(triggered()), fCurrentARDscene, SLOT(MoveItemsDown()));
}
// -----------------------------------------------------------------------------

// #brief Print model.
//
// #return No return value.
void MainWin_ui::printModel(void)
{
     if(!printer)
          printer = new QPrinter;

     if(printer)
     {
          QPainter pp(printer);

          if(appMode == ARD_MODE)
          {
               QMessageBox msgBox(this);
               msgBox.setText("Which scene would you like print?");
               QPushButton *tphButton = msgBox.addButton(tr("TPH"), QMessageBox::YesRole);
               QPushButton *ardButton = msgBox.addButton(tr("ARD"), QMessageBox::NoRole);
               msgBox.exec();

               if(msgBox.clickedButton() == tphButton)
                    ard_thpScene->render(&pp);
               else if (msgBox.clickedButton() == ardButton)
                    ard_modelScene->render(&pp);
          }

          if(appMode == XTT_MODE)
               xtt_scene->render(&pp);
     }
}
// -----------------------------------------------------------------------------

// #brief Show print setup window.
//
// #return No return value.
void MainWin_ui::printSetup(void)
{
     if(!printer)
          printer = new QPrinter;

     QPrintDialog *dlg = new QPrintDialog(printer, this);
     if(dlg->exec() != QDialog::Accepted)
          return;

     printModel();
}
// -----------------------------------------------------------------------------

// #brief Calls for plugins' event listener window dialog
//
// #return No return value.
void MainWin_ui::onPluginsEventListener(void)
{
     pluginEventListener->show();
}
// -----------------------------------------------------------------------------

// #brief Close application.
//
// #return No return value.
void MainWin_ui::CloseForm(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Function triggered when window is closed.
//
// #param event Pointer to event object.
// #return No return value.
void MainWin_ui::closeEvent(QCloseEvent* event)
{
     // Confirm exit from application.
     QString msgCaption = "HQEd";
     if(QMessageBox::question(this, msgCaption, "Do you really want to exit?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
     {
          event->ignore();
          return;
     }

     closeAllModels(false);

     // Hide icon.
     trayIcon->hide();

     // End application.
     hqed::destroyClipboard();
     QCoreApplication::exit();
}
// -----------------------------------------------------------------------------

// #brief Switch color of model (colour, black and white).
//
// #return No return value.
void MainWin_ui::SwitchColorMode(void)
{
     // Refresh window.
     Settings->setColorMode(actionB_W_view->isChecked(), currentMode());

     if(appMode == ARD_MODE)
     {
          ardPropertyList->refreshDisplay();
          ardLevelList->refreshDisplay();
     }
     if(appMode == XTT_MODE)
     {
          for(unsigned int i=0;i<ConnectionsList->Count();i++)
               ConnectionsList->Connection(i)->Repaint();
          TableList->RefreshAll();
          xtt_state_scene->update();
     }
}
// -----------------------------------------------------------------------------

// #brief Set antialiasing status.
//
// #param isON Determines whether antiasliasing in on.
// #return No return value.
void MainWin_ui::setAntialiasingStatus(bool isON)
{
     actionAntialiasing->setChecked(isON);
     UpdateRenderHints();
}
// -----------------------------------------------------------------------------

// #brief Set antialiasing status for text.
//
// #param isON Determines whether antiasliasing in on.
// #return No return value.
void MainWin_ui::setTextAntialiasingStatus(bool isON)
{
     actionTextAntialiasing->setChecked(isON);
     UpdateRenderHints();
}
// -----------------------------------------------------------------------------

// #brief Set smooth pixel map tranformation status.
//
// #param isON Determines whether smooth pixel map tranformation status is active.
// #return No return value.
void MainWin_ui::setSmoothPixmapTransformStatus(bool isON)
{
     actionSmooth_pixmap_transform->setChecked(isON);
     UpdateRenderHints();
}
// -----------------------------------------------------------------------------

// #brief Set color mode.
//
// #param isBW Determines whether black and white model should be visible.
// #return No return value.
void MainWin_ui::setColorMode(bool isBW)
{
     actionB_W_view->setChecked(isBW);
     SwitchColorMode();
}
// -----------------------------------------------------------------------------

// #brief Calls for StateSchemaEditor
//
// #return No return value.
void MainWin_ui::onAddNewStateGroup(void)
{
     delete StateSchemaEditor;
     StateSchemaEditor = new StateSchemaEditor_ui;
     StateSchemaEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Delete connection.
//
// #param cID Id connection.
// #return No return value.
void MainWin_ui::deleteConnection(QString cID)
{
     int cIndex = ConnectionsList->IndexOfID(cID);

     if(cIndex == -1)
          return;

     if(QMessageBox::question(NULL, "HQEd", "Do you really want to delete selected connection?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Jezeli wybrane bylo zaznaczone
     if(xtt_scene->SelectedConnectionID() == cID)
          xtt_scene->setSelectedConnectionID("");

     // Delete connection.
     ConnectionsList->Delete(cIndex);
}
// -----------------------------------------------------------------------------

// #brief Delete table.
//
// #param tID Table id.
// #return No return value.
void MainWin_ui::deleteTable(QString tID)
{
     int tIndex = TableList->IndexOfID(tID);

     if(tIndex == -1)
          return;

     // Potwierdzenie usuniecia
     if(QMessageBox::question(NULL, "HQEd", "Do you really want to remove selected table?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     // Jezeli wybrana byla zaznaczona
     if(xtt_scene->SelectedTableID() == tID)
          xtt_scene->setSelectedTableID("");

     // Usuwanie
     TableList->Delete(tIndex);
}
// -----------------------------------------------------------------------------

// #brief Delete selected table.
//
// #return No return value.
void MainWin_ui::deleteSelectedTable(void)
{
     deleteTable(xtt_scene->SelectedTableID());
     xtt_scene->setSelectedTableID("");
}
// -----------------------------------------------------------------------------

// #brief Delete selected connection.
//
// #return No return value.
void MainWin_ui::deleteSelectedConnection(void)
{
     deleteConnection(xtt_scene->SelectedConnectionID());
     xtt_scene->setSelectedConnectionID("");
}
// -----------------------------------------------------------------------------

// #brief Function is triggered when key is pressed.
//
// #param event Pointer to event object.
// #return No return value.
void MainWin_ui::keyPressEvent(QKeyEvent *event)
{
     if(appMode == ARD_MODE)
     {
          switch(event->key())
          {
               case Qt::Key_F5:
                    fCurrentARDscene->AdaptSceneSize();
                    break;

               case Qt::Key_F9:
                    break;

               case Qt::Key_Insert:
                    break;

               case Qt::Key_Delete:
                    break;

               case Qt::Key_Plus:
                    fCurrentARDscene->zin();
                    break;

               case Qt::Key_Minus:
                    fCurrentARDscene->zout();
                    break;

               default:
                    QWidget::keyPressEvent(event);
          }
     }

     if(appMode == XTT_MODE)
     {
          switch(event->key())
          {
               case Qt::Key_F5:
                    xtt_scene->AdaptSceneSize();
                    break;

               case Qt::Key_F9:
                    ExecuteModel();
                    break;

               case Qt::Key_Insert:
                    AddNewAttribute();
                    break;

               case Qt::Key_Delete:
                    if((xtt_scene->SelectedTableID() != "") && (TableList->Count() > 0))
                         deleteTable(xtt_scene->SelectedTableID());
                    else if((xtt_scene->SelectedConnectionID() != "") && (ConnectionsList->Count() > 0))
                         deleteConnection(xtt_scene->SelectedConnectionID());
                    xtt::fullVerification();
                    break;

               case Qt::Key_Plus:
                    xtt_scene->zin();
                    break;

               case Qt::Key_Minus:
                    xtt_scene->zout();
                    break;

               default:
                    QWidget::keyPressEvent(event);
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Change antialiasing status.
//
// #return No return value.
void MainWin_ui::ChangeAntialiasingStatus(void)
{
     QPainter::RenderHints flags = 0;
     if(actionAntialiasing->isChecked())
          flags = flags | QPainter::Antialiasing;
     if(actionTextAntialiasing->isChecked())
          flags = flags | QPainter::TextAntialiasing;
     if(actionSmooth_pixmap_transform->isChecked())
          flags = flags | QPainter::SmoothPixmapTransform;

     xtt_graphicsView->setRenderHints(flags);
}
// -----------------------------------------------------------------------------

// #brief Update render hints.
//
// #return No return value.
void MainWin_ui::UpdateRenderHints(void)
{
     QPainter::RenderHints flags = 0;
     if(actionAntialiasing->isChecked())
          flags = flags | QPainter::Antialiasing;
     if(actionTextAntialiasing->isChecked())
          flags = flags | QPainter::TextAntialiasing;
     if(actionSmooth_pixmap_transform->isChecked())
          flags = flags | QPainter::SmoothPixmapTransform;

     Settings->setAntialiasingMode(actionAntialiasing->isChecked(), currentMode());
     Settings->setTextAntialiasingMode(actionTextAntialiasing->isChecked(), currentMode());
     Settings->setSmoothPixmapTransformMode(actionSmooth_pixmap_transform->isChecked(), currentMode());

     if(appMode == ARD_MODE)
     {
          QList<QGraphicsView*> views_list = ard_modelScene->views();
          for(int i=0;i<views_list.count();i++)
               views_list.at(i)->setRenderHints(flags);

          views_list = ard_thpScene->views();
          for(int i=0;i<views_list.count();i++)
               views_list.at(i)->setRenderHints(flags);
     }

     if(appMode == XTT_MODE)
     {
          QList<QGraphicsView*> views_list = xtt_scene->views();
          for(int i=0;i<views_list.count();i++)
               views_list.at(i)->setRenderHints(flags);
               
          QList<QGraphicsView*> state_views_list = xtt_state_scene->views();
          for(int i=0;i<state_views_list.count();i++)
               state_views_list.at(i)->setRenderHints(flags);
     }
}
// -----------------------------------------------------------------------------

// #brief Show configuration dialog.
//
// #return No return value.
void MainWin_ui::showConfigurationDialog(void)
{
     Settings->show();
}
// -----------------------------------------------------------------------------

// #brief Show window where you can add new attribute.
//
// #return No return value.
void MainWin_ui::AddNewAttribute(void)
{
     delete AttributeEditor;
     AttributeEditor = new AttrEditor(true, 0, -1);
     AttributeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Show window where you can edit attribute.
//
// #return No return value.
void MainWin_ui::EditAttribute(void)
{
     if(AttributeGroups->IsEmpty())
     {
          const QString str2 = "There is no attributes defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     delete AttributeEditor;
     AttributeEditor = new AttrEditor(false, 0, -1);
     AttributeEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Show window where you can delete attribute.
//
// #return No return value.
void MainWin_ui::DeleteAttribute(void)
{
     if(AttributeGroups->IsEmpty())
     {
          const QString str2 = "There is no attributes defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     delete AttributeDelete;
     AttributeDelete = new AttributeDelete_ui(0);
     AttributeDelete->show();
}
// -----------------------------------------------------------------------------

// #brief Show group menager window.
//
// #return No return value.
void MainWin_ui::GroupManagerShow(void)
{
     delete GroupMan;
     GroupMan = new GroupManager_ui;
     GroupMan->show();
}
// -----------------------------------------------------------------------------

// #brief Show the ARD attribute menager window.
//
// #return No return value.
void MainWin_ui::ardAttributeManagerShow(void)
{
     delete ardAttributeManager;
     ardAttributeManager = new ardAttributeManager_ui;
     ardAttributeManager->show();
}
// -----------------------------------------------------------------------------

// #brief Show the XTT attribute menager window.
//
// #return No return value.
void MainWin_ui::xttAttributeManagerShow(void)
{
     delete AttrManager;
     AttrManager = new AttributeManager_ui;
     AttrManager->show();
}
// -----------------------------------------------------------------------------

// #brief Show table editor window.
//
// #return No return value.
void MainWin_ui::TableEditorShow(void)
{
     delete TableEdit;
     TableEdit = new TableEditor_ui(true, 0);
     TableEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Show table editor window in edit mode.
//
// #return No return value.
void MainWin_ui::TableEditorEdit(void)
{
     if(TableList->IsEmpty())
     {
          const QString str2 = "There is no tables defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonej tabeli
     int tindex = TableList->IndexOfID(xtt_scene->SelectedTableID());
     if(tindex == -1)
          tindex = 0;

     delete TableEdit;
     TableEdit = new TableEditor_ui(false, tindex);
     TableEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Show delete table window.
//
// #return No return value.
void MainWin_ui::DeleteTableShow(void)
{
     if(TableList->IsEmpty())
     {
          const QString str2 = "There is no tables defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonej tabeli
     int tindex = TableList->IndexOfID(xtt_scene->SelectedTableID());
     if(tindex == -1)
          tindex = 0;

     delete DelTable;
     DelTable = new DeleteTable_ui(tindex, 0);
     DelTable->show();
}
// -----------------------------------------------------------------------------

// #brief Performs tables autoarrangment procedure
//
// #return No return value.
void MainWin_ui::tablesArrangment(void)
{
     xtt_scene->AdaptSceneSize();
     TableList->tablesLayout();
     xtt_scene->AdaptSceneSize();
}
// -----------------------------------------------------------------------------

// #brief Performs tables autoarrangment procedure
//
// #return No return value.
void MainWin_ui::statesArrangment(void)
{
     xtt_state_scene->AdaptSceneSize();
     States->groupsReposition();
     xtt_state_scene->AdaptSceneSize();
}
// -----------------------------------------------------------------------------

// #brief Show cell editor window.
//
// #return No return value.
void MainWin_ui::ShowCellEditor(void)
{
     if(TableList->IsEmpty())
     {
          const QString str2 = "There is no tables defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonej tabeli
     int tindex = TableList->IndexOfID(xtt_scene->SelectedTableID());
     if(tindex == -1)
          tindex = 0;

     delete CellEdit;
     CellEdit = new CellEditor_ui(tindex, 0);
     CellEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Show connetion editor window.
//
// #return No return value.
void MainWin_ui::ShowConnEditor(void)
{
     // Wyszukiwanie indexu zaznaczonej tabeli
     int tindex = TableList->IndexOfID(xtt_scene->SelectedTableID());
     if(tindex == -1)
          tindex = 0;

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(true, tindex);
     ConnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Show connetion editor window in edit mode.
//
// #return No return value.
void MainWin_ui::ShowConnEditorInEditMode(void)
{
     if(ConnectionsList->Count() == 0)
     {
          const QString str2 = "There is no connections defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonego polaczenia
     int tindex = ConnectionsList->IndexOfID(xtt_scene->SelectedConnectionID());
     if(tindex == -1)
          tindex = 0;

     delete ConnEdit;
     ConnEdit = new ConnectionEditor_ui(false, tindex);
     ConnEdit->show();
}
// -----------------------------------------------------------------------------

// #brief Show delete connetion window.
//
// #return No return value.
void MainWin_ui::ShowConnectionDeleteWindow(void)
{
     if(ConnectionsList->Count() == 0)
     {
          const QString str2 = "There is no connections defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonej tabeli
     int conn_ind = ConnectionsList->IndexOfID(xtt_scene->SelectedConnectionID());
     if(conn_ind == -1)
          conn_ind = 0;
     QString tid = ConnectionsList->Connection(conn_ind)->SrcTable();
     QString rid = ConnectionsList->Connection(conn_ind)->SrcRow();

     int tindex = TableList->IndexOfID(tid);
     int rindex = TableList->IndexOfID(tid);
     if(tindex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Table with ID = " + tid + " not exist.", QMessageBox::Ok);
          return;
     }
     // Wyszukiwanie wiersza o podanym ID
     for(unsigned int r=0;r<TableList->Table(tindex)->RowCount();r++)
          if(TableList->Table(tindex)->Row(r)->RowId() == rid)
          {
               rindex = r;
               break;
          }
     if(rindex == -1)
     {
          QMessageBox::critical(NULL, "HQEd", "Row with ID = " + rid + " not exist in table with ID = " + tid + ".", QMessageBox::Ok);
          return;
     }

     delete DelConn;
     DelConn = new DeleteConnection_ui(tindex, rindex, conn_ind, 0);
     DelConn->show();
}
// -----------------------------------------------------------------------------

// #brief Show connetion menager window.
//
// #return No return value.
void MainWin_ui::ShowConnectionManager(void)
{
     if(TableList->IsEmpty())
     {
          const QString str2 = "There is no tables defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     // Wyszukiwanie indexu zaznaczonej tabeli
     int tindex = TableList->IndexOfID(xtt_scene->SelectedTableID());
     if(tindex == -1)
          tindex = 0;

     delete ConnManager;
     ConnManager = new ConnectionManager_ui(tindex, 0);
     ConnManager->show();
}
// -----------------------------------------------------------------------------

// #brief Open type manager
//
// #return No return value.
void MainWin_ui::typeManagerShow(void)
{
     delete TypeManager;
     TypeManager = new TypeManager_ui;
     TypeManager->show();
}
// -----------------------------------------------------------------------------

// #brief Function that shows hqedTextEditor
//
// #return No return value.
void MainWin_ui::hqedTextEditor(void)
{
     TextEditor->show();
}
// -----------------------------------------------------------------------------

// #brief Show list of unused attributes.
//
// #return No return value.
void MainWin_ui::ShowUnusedAttributes(void)
{
     if(AttributeGroups->IsEmpty())
     {
          const QString str2 = "There is no attributes defined yet.";
          QMessageBox::information(NULL, "HQEd", str2, QMessageBox::Ok);
          return;
     }

     delete UnusedAttr;
     UnusedAttr = new UnusedAtrtribute_ui;
     UnusedAttr->show();
}
// -----------------------------------------------------------------------------

// #brief Show program information.
//
// #return No return value.
void MainWin_ui::ShowAboutWin(void)
{
     //delete AboutWin;
     //AboutWin = new About_ui;
     AboutWin->show();
}
// -----------------------------------------------------------------------------

// #brief Function that updates the windows title
//
// #return No return value.
void MainWin_ui::updateWindowTitle(void)
{
     QString mde, fn, ttl, srvsts;

     if(currentMode() == ARD_MODE)
     {
          mde = "ARD Editor";
          fn = fArdFileName;
     }
     if(currentMode() == XTT_MODE)
     {
          mde = "HQEd";
          fn = fXttFileName;
     }

     if(hqedServerInstance->isListening())
          srvsts = " [Listening on " + QString::number(hqedServerInstance->port()) + "...]";

     ttl = mde + " - [" + fn + "]" + srvsts;
     setWindowTitle(ttl);
}
// -----------------------------------------------------------------------------

// #brief Check ARD model changes.
//
// #return True when model is changed, otherwise false.
bool MainWin_ui::ARD_Changed(void)
{
     return ardAttributeList->changed() || ardPropertyList->changed() || ardDependencyList->changed();
}
// -----------------------------------------------------------------------------

// #brief Check XTT model changes.
//
// #return True when model is changed, otherwise false.
bool MainWin_ui::XTT_Changed(void)
{
     return AttributeGroups->Changed() || ConnectionsList->Changed() || TableList->Changed();
}
// -----------------------------------------------------------------------------

// #brief Set status of modyfication ARD model.
//
// #param _c Status of modyfication.
// #return No return value.
void MainWin_ui::setARD_Changed(bool _c)
{
     ardAttributeList->setChanged(_c);
     ardPropertyList->setChanged(_c);
     ardDependencyList->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Set status of modyfication XTT model.
//
// #param _c Status of modyfication.
// #return No return value.
void MainWin_ui::setXTT_Changed(bool _c)
{
     ConnectionsList->setChanged(_c);
     TableList->setChanged(_c);
     AttributeGroups->setChanged(_c);
}
// -----------------------------------------------------------------------------

// #brief Ask the user if the changes should be saved
//
// #param __mode - denotes where the changes must be confirmed
// #li ARD_MODE - changes in the ARD model
// #li XTT_MODE - changes in the XTT model
// #return 0 - Cancel, 1 - Yes, -1 - No.
int MainWin_ui::confirmDiscardChanges(int __mode)
{
     bool reqModelChanged = ((__mode == ARD_MODE) && (ARD_Changed())) || ((__mode == XTT_MODE) && (XTT_Changed()));

     int res = 1;
     if(reqModelChanged)
     {
          switch(QMessageBox::question(NULL, "HQEd", "Current project has been changed. Do you want to save changes?", QMessageBox::Yes | QMessageBox::No | QMessageBox::Cancel))
          {
          case QMessageBox::Yes:
               save();
               res = 1;
               break;
          case QMessageBox::No:
               res = -1;
               break;
          case QMessageBox::Cancel:
               return 0;
               break;
          default:
               return 0;
          }
     }

     return res;
}
// -----------------------------------------------------------------------------

// #brief Create new ARD project.
//
// #param __startnew - indicates if the new ARD model should start from editable root (true) or it will be read from file (false)
// #return no values return
void MainWin_ui::CreateNewARDProject(bool __startnew)
{
     QApplication::setOverrideCursor(Qt::WaitCursor);
     fArdFileName = "New project";
     updateWindowTitle();
     setARD_Changed(false);

     // Kasowanie list elementow
     ard::clear();
     if(__startnew)
          ard::startNew();
     isARDFile = false;

     QApplication::restoreOverrideCursor();
}
// -----------------------------------------------------------------------------

// #brief Create new XTT project.
//
// #param _keepAttributes Determines whether leave argument list.
// #return no values return
void MainWin_ui::CreateNewXTTProject(bool _keepAttributes)
{
     QApplication::setOverrideCursor(Qt::WaitCursor);
     fXttFileName = "New project";
     updateWindowTitle();

     xtt_scene->setSelectedConnectionID("");
     xtt_scene->setSelectedTableID("");
     xtt_runModelMessagesDockWindowMessagesList->deleteAll();
     xtt_errorsList->deleteAll();

     xtt::clear(_keepAttributes);
     isXTTFile = false;

     QApplication::restoreOverrideCursor();
     setXTT_Changed(false);
}
// -----------------------------------------------------------------------------

// #brief Create new ard project.
//
// #return No return value.
void MainWin_ui::NewArdClick(void)
{
     hqed::modelList()->createNewModel(true, ARD_MODE)->registerModel();
     ard::startNew();
}
// -----------------------------------------------------------------------------

// #brief Create new xtt project.
//
// #return No return value.
void MainWin_ui::NewXttClick(void)
{
     hqed::modelList()->createNewModel(true, XTT_MODE)->registerModel();
}
// -----------------------------------------------------------------------------

// #brief Create new project with the same attributes.
//
// #return No return value.
void MainWin_ui::NewWhithOldAttributiesClick(void)
{
     if(confirmDiscardChanges(XTT_MODE))
          CreateNewXTTProject(true);
}
// -----------------------------------------------------------------------------

// #brief Removes all the data from ARD model
//
// #return No return value.
void MainWin_ui::clearARDmodel(void)
{
     if(confirmDiscardChanges(ARD_MODE))
          CreateNewARDProject(true);
}
// -----------------------------------------------------------------------------

// #brief Removes all the data from XTT model
//
// #return No return value.
void MainWin_ui::clearXTTmodel(void)
{
     if(confirmDiscardChanges(XTT_MODE))
          CreateNewXTTProject(false);
}
// -----------------------------------------------------------------------------

// #brief Removes all the data from ARD and XTT model
//
// #return No return value.
void MainWin_ui::clearBothNModels(void)
{
     if(confirmDiscardChanges(ARD_MODE))
          CreateNewARDProject(true);
     if(confirmDiscardChanges(XTT_MODE))
          CreateNewXTTProject(false);
}
// -----------------------------------------------------------------------------

// #brief Closes current model
//
// #return No return value.
void MainWin_ui::closeModel(void)
{
     if(QMessageBox::question(NULL, "HQEd", "This action closes <b>both</b> ARd and XTT models.\nAre you sure that you want to close \'" + hqed::modelList()->activeModel()->modelTitle() + "\' model?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
     
     updateModel(hqed::modelList()->activeModel());
     if(hqed::modelList()->activeModel()->changed())
     {
          if(QMessageBox::question(NULL, "HQEd", "Model \'" + hqed::modelList()->activeModel()->modelTitle() + "\' has been changed. Do you want to save changes?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
               save();
     }
     hqed::modelList()->closeActiveModel();
}
// -----------------------------------------------------------------------------

// #brief Starts the hqed server
//
// #return No return value.
void MainWin_ui::hqedServerStart(void)
{
     hqedServerInstance->listen(Settings->hqedServerListenPort());
     //menuServer->setIcon(QIcon(":/all_images/images/listening-small.png"));
     updateWindowTitle();
}
// -----------------------------------------------------------------------------

// #brief Stops the hqed server
//
// #return No return value.
void MainWin_ui::hqedServerStop(void)
{
     hqedServerInstance->stop();
     //menuServer->setIcon(QIcon());
     updateWindowTitle();
}
// -----------------------------------------------------------------------------

// #brief Closes all models
//
// #param __confirm - determines confirmation of the action
// #return No return value.
void MainWin_ui::closeAllModels(bool __confirm)
{
     if((__confirm) && (QMessageBox::warning(NULL, "HQEd", "This action closes <b>all</b> the opened models.\nAre you sure that you want to continue?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No))
          return;

     int size = hqed::modelList()->size();

     updateModel(hqed::modelList()->activeModel());
     for(int i=0;i<size;++i)
     {
          hqed::modelList()->activateModel(0);
          if(hqed::modelList()->activeModel()->changed())
          {
               if(QMessageBox::question(NULL, "HQEd", "Model \'" + hqed::modelList()->activeModel()->modelTitle() + "\' has been changed. Do you want to save changes?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
                    save();
          }
          hqed::modelList()->closeActiveModel();
     }
}
// -----------------------------------------------------------------------------

// #brief Closes all models except current
//
// #return No return value.
void MainWin_ui::closeAllModelsExceptCurrent(void)
{
     if(QMessageBox::warning(NULL, "HQEd", "This action closes all the opened models <b>except</b> current \'" + hqed::modelList()->activeModel()->modelTitle() + "\'.\nAre you sure that you want to continue?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;
          
     int deletedcount = 0;
     int currindex = 0;
     int index = hqed::modelList()->activeModelIndex();
     
     updateModel(hqed::modelList()->activeModel());
     while(hqed::modelList()->size() > 1)
     {
          if(deletedcount == index)
               currindex = 1;
          
          hqed::modelList()->activateModel(currindex);
          if(hqed::modelList()->activeModel()->changed())
          {
               if(QMessageBox::question(NULL, "HQEd", "Model \'" + hqed::modelList()->activeModel()->modelTitle() + "\' has been changed. Do you want to save changes?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::Yes)
                    save();
          }
          hqed::modelList()->closeActiveModel();
               
          deletedcount++;
     }
}
// -----------------------------------------------------------------------------

// #brief Open XTT file dialog.
//
// #return No return value.
void MainWin_ui::openXTTFileDialog(void)
{
     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getOpenFileName(this,
                                 tr("Open project"),
                                 Settings->projectPath(),
                                 tr("HML files (*.hml);;XTTML files (*.xttml);;ATTML files (*.attml);;All files (*)"));

     openFile(fileName);
}
// -----------------------------------------------------------------------------

// #brief Open ARD file dialog.
//
// #return No return value.
void MainWin_ui::openARDFileDialog(void)
{
     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getOpenFileName(this,
                                 tr("Open project"),
                                 Settings->projectPath(),
                                 tr("HML files (*.hml);;ARDML files (*.ardml);;All files (*)"));

     openFile(fileName);
}
// -----------------------------------------------------------------------------

// #brief Open file.
//
// #param _fileName File name to open.
// #return No return value.
void MainWin_ui::openFile(QString _fileName)
{
     // Gdy nie wybrano zadnego pliku
     if(_fileName.isEmpty())
          return;

     if(!QFile::exists(_fileName))
     {
          QMessageBox::critical(this, "HQEd", "File " + _fileName + " does not exists.");
          return;
     }

     // Czytanie pliku
     QFile file(_fileName);
     if(!file.open(QFile::ReadOnly | QFile::Text))
     {
          QMessageBox::critical(this, tr("HQEd"), tr("Cannot read file %1:\n%2.").arg(_fileName).arg(file.errorString()));
          return;
     }
     
     Settings->recentFile(_fileName);
     formRecentFilesList();

     QString msg = "", format = "";
     QApplication::setOverrideCursor(Qt::WaitCursor);
     int result = vas_in_XML(&file, msg, format);
     QApplication::restoreOverrideCursor();

     if(result == VAS_XML_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Loading error: device is not specified.");
     if(result == VAS_XML_DEVICE_NOT_READABLE)
          QMessageBox::critical(this, "HQEd", "Loading error: device is not readable.");
     if(result == VAS_XML_FORMAT_ERROR)
          QMessageBox::critical(this, "HQEd", "Loading error: " + hqed::retrieveMessageParts(msg));
     if(result == VAS_XML_FORMAT_NOT_SUPPORTED)
          QMessageBox::critical(this, "HQEd", "Loading error: the file format is not supported.");
     if(result == VAS_XML_VERSION_NOT_SUPPORTED)
          QMessageBox::critical(this, "HQEd", "Loading error: the xml version is not supported or not specified.");
     if(result == VAS_XML_XTT_SYNTAX_ERROR)
          hqed::showMessages(msg);
     if(result == VAS_XML_NOTHING_DONE)
          return;
          
     // Information about loading finish
     //if(result == VAS_XML_SUCCES)       // When no errors occur
     //QMessageBox::information(this, "HQEd", "Loading finished.");

     // Gdy plik typu xttml
     if(format == "xttml2.0")
          onLoad_XTTML_2_0_file(_fileName);

     // Gdy plik typu ardml
     if(format == "ardml1.0")
          onLoad_ARDML_1_0_file(_fileName);

     // Gdy plik typu hml1.0
     if(format == "hml1.0")
          onLoad_HML_1_0_file(_fileName);

     // Gdy plik typu hml2.0
     if(format == "hml2.0")
          onLoad_HML_2_0_file(_fileName, msg);

     updateModel(hqed::modelList()->activeModel());
     updateModelScenes();
}
// -----------------------------------------------------------------------------

// #brief Function that is called when HML 2.0 model has been loaded from protocol message
//
// #return No return value.
void MainWin_ui::onLoad_HML_2_0_server(void)
{
     // sprawdzenie modelu 2x
     xtt::fullVerification();
     xtt::fullVerification();
     xtt::logicalFullVerification();

     // Usuniecie elementu ktory podaza za wskaznikiem myszki
     xtt_scene->resetNewItem();

     TableList->tablesLayout();
     States->groupsReposition();

     setXTT_Changed(false);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when XTTML 2.0 file has been loaded
//
// #param __sourceName - the name of the source
// #return No return value.
void MainWin_ui::onLoad_XTTML_2_0_file(QString __sourceName)
{
     setCurrentMode(XTT_MODE);
     fXttFileName = __sourceName;
     updateWindowTitle();
     isXTTFile = true;

     onLoad_HML_2_0_server();
}
// -----------------------------------------------------------------------------

// #brief Function that is called when ARDML 1.0 file has been loaded
//
// #param __sourceName - the name of the source
// #return No return value.
void MainWin_ui::onLoad_ARDML_1_0_file(QString __sourceName)
{
     onLoad_HML_1_0_file(__sourceName);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when HML 1.0 file has been loaded
//
// #param __sourceName - the name of the source
// #return No return value.
void MainWin_ui::onLoad_HML_1_0_file(QString __sourceName)
{
     setCurrentMode(ARD_MODE);
     fArdFileName = __sourceName;
     updateWindowTitle();
     isARDFile = true;

     ardLevelList->createBaseLevel();
     
     setARD_Changed(false);
}
// -----------------------------------------------------------------------------

// #brief Function that is called when HML 2.0 file has been loaded
//
// #param __sourceName - Reference to section with file contants.
// #param __msgs - the reading messages.
// #return No return value.
void MainWin_ui::onLoad_HML_2_0_file(QString __sourceName, QString __msgs)
{
     if(__msgs.contains("HASARD"))
          onLoad_ARDML_1_0_file(__sourceName);

     if(__msgs.contains("HASXTT"))
          onLoad_XTTML_2_0_file(__sourceName);
}
// -----------------------------------------------------------------------------

// #brief Save file as.
//
// #param varsion Filter file version.
// #return No return value.
void MainWin_ui::saveAs(int version)
{
     QStringList filters;
     QString filterHml = "HML 2.0 files (*.hml)";
     QString filterDrools = "Drools Flow (*.rf *.csv)";
     QString selectedFilter;

     filters.append(filterHml);
     //filters.append(filterDrools);

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
          tr("Save project as"),
          QDir::currentPath(),
          filters.join(";;"),
          &selectedFilter);

     // Gdy nie wybrano zandego pliku
     if(fileName.isEmpty())
          return;

     isXTTFile = true;

     if(selectedFilter == filterHml)
     {
          if(version == 2)
               saveTo_HML_2_0(fileName);
     }
     else if(selectedFilter == filterDrools)
     {
          saveTo_Drools_5_0(fileName);
     }

     updateModel(hqed::modelList()->activeModel());
     Settings->recentFile(fileName);
     formRecentFilesList();
}
// -----------------------------------------------------------------------------

// #brief Save changes.
//
// #return No return value.
void MainWin_ui::save(void)
{
     saveTo_HML_2_0(fXttFileName);
}
// -----------------------------------------------------------------------------

// #brief Save all changes in all models
//
// #return No return value.
void MainWin_ui::saveAll(void)
{
     int ami = hqed::modelList()->activeModelIndex();
     for(int i=0;i<hqed::modelList()->size();++i)
     {
          hqed::modelList()->activateModel(i);
          saveTo_HML_2_0(fXttFileName);
     }
     hqed::modelList()->activateModel(ami);
}
// -----------------------------------------------------------------------------

// #brief Executes action of saving file to hml version 2.0.
//
// #return No return value.
void MainWin_ui::saveAs_HML_2_0(void)
{
     saveAs(2);
}
// -----------------------------------------------------------------------------

// #brief Save file to format xttml 2.0.
//
// #param __fileName - the destination file name
// #return No return value.
void MainWin_ui::saveTo_HML_2_0(QString __fileName)
{
     if(!isXTTFile)
     {
          saveAs(2);
          return;
     }

     // Changing the file extesion
     QString fName = __fileName;
     QFileInfo fi(fName);
     QString ext = fi.suffix();
     if(ext.toLower() != "hml")
     {
          fName.remove(ext, Qt::CaseInsensitive);
          fName += "hml";
     }

     // Zapis danych do pliku
     QFile file(fName);
     if(!file.open(QFile::WriteOnly | QFile::Text))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(fName).arg(file.errorString()));
         return;
     }

     QApplication::setOverrideCursor(Qt::WaitCursor);
     int parts = VAS_XML_MODEL_PARTS_XTT_TYPES;
     parts |=  VAS_XML_MODEL_PARTS_ARD_PROPS;
     parts |=  VAS_XML_MODEL_PARTS_ARD_TPH;
     parts |=  VAS_XML_MODEL_PARTS_ARD_DPNDS;
     parts |=  VAS_XML_MODEL_PARTS_ARD_ATTRS;
     parts |=  VAS_XML_MODEL_PARTS_XTT_ATTRS;
     parts |= VAS_XML_MODEL_PARTS_XTT_TABLS;
     parts |= VAS_XML_MODEL_PARTS_XTT_STATS;
     int result = vas_out_HML_2_0(&file, Settings->xmlAutoformating(), parts);
     QApplication::restoreOverrideCursor();

     if(result == VAS_XML_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not specified.");
     if(result == VAS_XML_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not writable.");

     // Zapisanie stanu statusu modyfikacji modelu XTT
     setXTT_Changed(result != VAS_XML_SUCCES);

     fXttFileName = fName;
     updateWindowTitle();
}
// -----------------------------------------------------------------------------

// #brief Save file to Drools 5.0 Flow format.
//
// #param __fileName - the destination file name
// #return No return value.
void MainWin_ui::saveTo_Drools_5_0(QString __dirName)
{
     QString dName = __dirName;

     if(QDir(dName).exists())
     {
          QMessageBox::warning(this,tr("HQEd"),tr("The directory %1 already exists").arg(dName));
          return;
     }
     
     QDir().mkdir(dName);
     
     QFile fileRuleFlow(dName + "/RuleFlow.rf");
     QFile fileDecTab(dName + "/DecTable.txt");
     QFile fileWorkspace(dName + "/Workspace.java");
     
     if(!fileRuleFlow.open(QFile::WriteOnly | QFile::Text))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(fileRuleFlow.fileName()).arg(fileRuleFlow.errorString()));
         return;
     }
     
     if(!fileDecTab.open(QFile::WriteOnly))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(fileDecTab.fileName()).arg(fileDecTab.errorString()));
         return;
     }
     
     if(!fileWorkspace.open(QFile::WriteOnly | QFile::Text))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(fileWorkspace.fileName()).arg(fileWorkspace.errorString()));
         return;
     }

     QApplication::setOverrideCursor(Qt::WaitCursor);
     int result = vas_out_Drools_5_0(&fileRuleFlow,&fileDecTab,&fileWorkspace);
     QApplication::restoreOverrideCursor();

     if(result == VAS_XML_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not specified.");
     if(result == VAS_XML_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not writable.");
}
// -----------------------------------------------------------------------------

// #brief Export attributes to HML 2.0 file.
//
// #return No return value.
void MainWin_ui::exportAttributesToHML20file(void)
{
     QStringList filters;
     filters << "HML 2.0 files (*.hml)";

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
                                 tr("Export atrributes to HML 2.0 format"),
                                 QDir::currentPath(),
                                 filters.at(0));

     // Gdy nie wybrano zandego pliku
     if(fileName.isEmpty())
          return;

     // Sprawdzenie poprawnosci rozszerzenia
     if(!fileName.contains(".hml"))
         fileName += ".hml";

     // Export code
     QFile file(fileName);
     if(!file.open(QFile::WriteOnly | QFile::Text))
     {
         QMessageBox::warning(this, tr("HQEd"), tr("Cannot write file %1:\n%2.").arg(fXttFileName).arg(file.errorString()));
         return;
     }

     QApplication::setOverrideCursor(Qt::WaitCursor);
     int result = vas_out_HML_2_0(&file, Settings->xmlAutoformating(), VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS);
     QApplication::restoreOverrideCursor();

     if(result == VAS_XML_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not specified.");
     if(result == VAS_XML_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not writable.");
}
// -----------------------------------------------------------------------------

// #brief Import attributes from HML 2.0 file.
//
// #return No return value.
void MainWin_ui::importAttributesFromHML20file(void)
{
     // Choosing file's name
     QString fileName = QFileDialog::getOpenFileName(this,
                                                     tr("Import atrributes from HML 2.0 format"),
                                                     Settings->projectPath(),
                                                     tr("HML files (*.hml);;All files (*)"));
     
     if(fileName.isEmpty())
          return;

     // Reading the file's content
     QFile file(fileName);
     QDomDocument doc;
     if(!file.open(QFile::ReadOnly | QFile::Text))
     {
          QMessageBox::critical(this, tr("HQEd"), tr("Cannot read file %1:\n%2.").arg(fileName).arg(file.errorString()));
          return;
     }
     if (!doc.setContent(&file))
     {
          file.close();
          return;
     }
     file.close();

     // Searching for duplicated IDs
     QList<QDomElement> conTypes;                  // Lista typow tworzacych konflikty
     QList<QDomElement> conAttrs;                  // Lista atrybutow tworzacych konflikty
     QStringList newTypes;                         // Lista identyfikatorow wszystkich importowanych typow
     QStringList newAttrs;                         // Lista identyfikatorow wszystkich importowanych atrybutow
     QDomElement docElem = doc.documentElement();  // Importowany plik

     // Wykrywanie konfliktow w importowanych typach
     QDomElement types = docElem.firstChildElement("types");
     if(!types.isNull())
     {
          QDomElement type = types.firstChildElement("type");
          while(!type.isNull())
          {
               // Jezeli wykryto konflikt, to kopiujemy element XML w celu pozniejszych zmian
               if(typeList->indexOfID(type.attribute("id", "")) != -1)
                    conTypes.append(type);
               // Jezeli nie ma konfliktow to dodajemy ID wczytanego typu do listy typow nie powodujacych konfliktow
               else
                    newTypes << type.attribute("id","");
               type = type.nextSiblingElement("type");
          }
     }

     // Analogicznie jak z typami powyzej postepujemy z atrybutami
     QDomElement attrs = docElem.firstChildElement("attributes");
     if(!attrs.isNull())
     {
          // Usuwanie zbednych informacji o grupach
          QDomElement agroup = attrs.firstChildElement("agroup");
          while(!agroup.isNull())
          {
               attrs.removeChild(agroup);
               agroup = agroup.nextSiblingElement("agroup");
          }

          // Wykrywanie konfliktow w importowanych atrybutach
          QDomElement attr = attrs.firstChildElement("attr");
          while(!attr.isNull())
          {
               // Jezeli wykryto konflikt, to kopiujemy element XML w celu pozniejszych zmian
               if(AttributeGroups->indexOfAttId(attr.attribute("id", "")) != NULL)
                    conAttrs.append(attr);
               // Jezeli nie ma konfliktow to dodajemy ID wczytanego atrybutu do listy atrybutow nie powodujacych konfliktow
               else
                    newAttrs << attr.attribute("id","");
               attr = attr.nextSiblingElement("attr");
          }
     }

     // Jezeli znaleziono konflikty to przy pomocy specjalnego dialogu decydujemy co z nimi mamy zrobic

     // Choosing actions for duplicated IDs
     if(!conTypes.isEmpty() || !conAttrs.isEmpty())
     {
          // Uruchomienie dialogu
          AttributeImport *conWin = new AttributeImport(this, conTypes, conAttrs);
          conWin->exec();

          // Jezeli kliknieto OK
          if(conWin->result() == QDialog::Accepted)
          {
               QList<QDomElement> selTypes = conWin->getSTypes();          // Lista typow powodujacych konflikty ktore zostaly wybrane przez uzytkownika
               QList<QDomElement> unselTypes = conWin->getUTypes();        // Lista typow powodujacych konflikty ktore nie zostaly wybrane przez uzytkownika
               QList<QDomElement> selAttrs = conWin->getSAttrs();          // Lista atrybutow powodujacych konflikty ktore zostaly wybrane przez uzytkownika
               QList<QDomElement> unselAttrs = conWin->getUAttrs();        // Lista atrybutow powodujacych konflikty ktore nie zostaly wybrane przez uzytkownika

               // Jezeli dla zaznaczonych elementow nalezy zmienic ich ID
               if(conWin->getSelChoice() == "load with changed id")
               {
                    if(!selTypes.isEmpty())
                    {
                         QStringList usedIds;                              // Lista nowouzytych identyfikatorow
                         docElem.removeChild(types);

                         // Rozwiazywanie kolejnych konfliktow
                         for(int i=0;i<selTypes.size();++i)
                         {
                              types.removeChild(selTypes[i]);

                              // Wyszukiwanie nowego, unikalnego ID
                              int id_index = 1;
                              QString candidate = "";
                              do
                              {
                                   candidate = "tpe_" + QString::number(id_index++);
                              }
                              while((usedIds.indexOf(candidate) != -1) || (typeList->indexOfID(candidate) != -1) || (newTypes.indexOf(candidate) != -1));

                              // Sprawdzanie czy jakies atrybuty nie sa typu w ktorym zmieniono ID
                              QString oldTypeId = selTypes[i].attribute("id", "");   // Stare ID typu
                              QString newTypeId = candidate;                         // Nowe ID typu
                              docElem.removeChild(attrs);
                              QList<QDomElement> attrList;
                              QDomElement attr = attrs.firstChildElement("attr");

                              // Kopiowanie listy importowanych atrybutow
                              while(!attr.isNull())
                              {
                                   attrList.append(attr);
                                   attr = attr.nextSiblingElement("attr");
                              }

                              // Zmiana ID w atrybutach
                              for(int j=0;j<attrList.size();++j)
                                   if(attrList[j].attribute("type", "") == oldTypeId)
                                   {
                                        attrs.removeChild(attrList[j]);
                                        attrList[j].setAttribute("type", newTypeId);
                                        attrs.appendChild(attrList[j]);
                                   }
                              docElem.appendChild(attrs);

                              // Zapisanie nowych ustawien
                              usedIds << candidate;                        // Dodanie nowego ID do listy juz uzytych
                              selTypes[i].setAttribute("id", candidate);   // Zmiana ID w elemencie XML
                              types.appendChild(selTypes[i]);
                         }
                         docElem.appendChild(types);
                    }

                    if(!selAttrs.isEmpty())
                    {
                         QStringList usedIds;                              // Lista nowouzytych identyfikatorow
                         docElem.removeChild(attrs);

                         // Rozwiazywanie kolejnych konfliktow
                         for(int i=0;i<selAttrs.size();++i)
                         {
                              attrs.removeChild(selAttrs[i]);

                              // Wyszukiwanie  unikalnego ID atrybutu
                              int id_index = 1;
                              QString candidate = "";
                              do
                              {
                                   candidate = "att_xtt_" + QString::number(id_index++);
                              }
                              while((usedIds.indexOf(candidate) != -1) || (AttributeGroups->indexOfAttId(candidate) != NULL) || (newAttrs.indexOf(candidate) != -1));

                              // Zapisanie nowych ustawien
                              usedIds << candidate;
                              selAttrs[i].setAttribute("id", candidate);
                              attrs.appendChild(selAttrs[i]);
                         }
                         docElem.appendChild(attrs);
                    }
               }

               // Jezeli dla niezaznaczonych elementow nalezy zmienic ich ID
               if(conWin->getOthChoice() == "load with changed id")
               {
                    if(!unselTypes.isEmpty())
                    {
                         QStringList usedIds;                                   // Lista nowouzytych identyfikatorow
                         docElem.removeChild(types);

                         // Rozwiazywanie kolejnych konfliktow
                         for(int i=0;i<unselTypes.size();++i)
                         {
                              types.removeChild(unselTypes[i]);

                              // Wyszukiwanie nowego, unikalnego ID
                              int id_index = 1;
                              QString candidate = "";
                              do
                              {
                                   candidate = "tpe_" + QString::number(id_index++);
                              }
                              while(usedIds.indexOf(candidate) != -1 || typeList->indexOfID(candidate) != -1 || newTypes.indexOf(candidate) != -1);

                              // Sprawdzanie czy jakies atrybuty nie sa typu w ktorym zmieniono ID
                              QString oldTypeId = unselTypes[i].attribute("id", "");      // Stare ID typu
                              QString newTypeId = candidate;                              // Nowe ID typu
                              QList<QDomElement> attrList;
                              docElem.removeChild(attrs);
                              QDomElement attr = attrs.firstChildElement("attr");

                              // Kopiowanie listy importowanych atrybutow
                              while(!attr.isNull())
                              {
                                   attrList.append(attr);
                                   attr = attr.nextSiblingElement("attr");
                              }

                              // Zmiana ID w atrybutach
                              for(int j=0;j<attrList.size();++j)
                                   if(attrList[j].attribute("type", "") == oldTypeId)
                                   {
                                        attrs.removeChild(attrList[j]);
                                        attrList[j].setAttribute("type", newTypeId);
                                        attrs.appendChild(attrList[j]);
                                   }
                              docElem.appendChild(attrs);

                              // Zapisanie nowych ustawien
                              usedIds << candidate;                             // Dodanie nowego ID do listy juz uzytych
                              unselTypes[i].setAttribute("id", candidate);      // Zmiana ID w elemencie XML
                              types.appendChild(unselTypes[i]);
                         }
                         docElem.appendChild(types);
                    }

                    if(!unselAttrs.isEmpty())
                    {
                         QStringList usedIds;
                         docElem.removeChild(attrs);

                         // Rozwiazywanie kolejnych konfliktow
                         for(int i=0;i<unselAttrs.size();++i)
                         {
                              attrs.removeChild(unselAttrs[i]);

                              // Wyszukiwanie  unikalnego ID atrybutu
                              int id_index = 1;
                              QString candidate = "";
                              do
                              {
                                   candidate = "att_xtt_" + QString::number(id_index++);
                              }
                              while((usedIds.indexOf(candidate) != -1) || (AttributeGroups->indexOfAttId(candidate) != NULL) || (newAttrs.indexOf(candidate) != -1));

                              // Zapisanie nowych ustawien
                              usedIds << candidate;
                              unselAttrs[i].setAttribute("id", candidate);
                              attrs.appendChild(unselAttrs[i]);
                         }
                         docElem.appendChild(attrs);
                    }
               }


               // Jezeli w przypadku zaznaczonych elementow nie nalezy ich wczytywac
               if(conWin->getSelChoice() == "leave unchanged")
               {
                    if(!selTypes.isEmpty())                           // Zaznaczone typy powodujace konflikty
                    {
                         docElem.removeChild(types);
                         for(int i=0;i<selTypes.size();++i)
                              types.removeChild(selTypes.at(i));      // Usuwamy elementy DOM typow powodujace konflikty
                         docElem.appendChild(types);
                    }
                    if(!selAttrs.isEmpty())                           // Zaznaczone atrybuty powodujace konflikty
                    {
                         docElem.removeChild(attrs);
                         for(int i=0;i<selAttrs.size();++i)
                              attrs.removeChild(selAttrs.at(i));      // Usuwamy elementy DOM typow powodujace konflikty
                         docElem.appendChild(attrs);
                    }
               }

               // Jezeli w przypadku nie zaznaczonych elementow nie nalezy ich wczytywac
               if(conWin->getOthChoice() == "leave unchanged")
               {
                    if(!unselTypes.isEmpty())                         // Nie zaznaczone typy powodujace konflikty
                    {
                         docElem.removeChild(types);
                         for(int i = 0; i < unselTypes.size(); ++i)
                              types.removeChild(unselTypes.at(i));
                         docElem.appendChild(types);
                    }
                    if(!unselAttrs.isEmpty())                         // Nie zaznaczone atrybuty powodujace konflikty
                    {
                         docElem.removeChild(attrs);
                         for(int i = 0; i < unselAttrs.size(); ++i)
                              attrs.removeChild(unselAttrs.at(i));
                         docElem.appendChild(attrs);
                    }
               }

               if(conWin->getSelChoice() == "overwrite")
               {
                    for(int i=selTypes.size()-1;i>=0;--i)
                    {
                         QString msg = "";
                         int _oldIndex = typeList->indexOfID(selTypes[i].attribute("id", ""));
                         hType* _old = typeList->_indexOfID(selTypes[i].attribute("id", ""));
                         hType* _new = new hType(typeList);
                         _new->in_HML_2_0(selTypes[i], msg);
                         typeList->replace(_oldIndex, _new);
                         typeList->updateTypePointer(_old, _new);
                         docElem.removeChild(types);
                         types.removeChild(selTypes[i]);
                         docElem.appendChild(types);
                         delete _old;
                    }

                    for(int i=0;i<selAttrs.size();++i)
                    {
                         QString msg = "";
                         QString errs = "";
                         XTT_Attribute* _old = AttributeGroups->indexOfAttId(selAttrs[i].attribute("id", ""));
                         XTT_Attribute* _new = new XTT_Attribute();
                         _old->setId("eFeBeEjuBYHSLyQdkapm3SLqhfdcfv"); //random string
                         _new->in_HML_2_0(selAttrs[i], msg);
                         _old->copyProperties(_new);
                         if(!hqed::sendEventMessage(&errs, XTT_SIGNAL_ATTRIBUTE_CHANGED, _old))
                              hqed::showMessages(errs);
                         xtt::fullVerification();
                         xtt::logicalFullVerification();
                         TableList->RefreshAll();
                         docElem.removeChild(attrs);
                         attrs.removeChild(selAttrs[i]);
                         docElem.appendChild(attrs);
                         delete _new;
                    }
               }

               if(conWin->getOthChoice() == "overwrite")
               {
                    for(int i=unselTypes.size()-1;i>=0;--i)
                    {
                         QString msg = "";
                         int _oldIndex = typeList->indexOfID(unselTypes[i].attribute("id", ""));
                         hType* _old = typeList->_indexOfID(unselTypes[i].attribute("id", ""));
                         hType* _new = new hType(typeList);
                         _new->in_HML_2_0(unselTypes[i], msg);
                         typeList->replace(_oldIndex, _new);
                         typeList->updateTypePointer(_old, _new);
                         docElem.removeChild(types);
                         types.removeChild(unselTypes[i]);
                         if(!types.firstChildElement("type").isNull())
                              docElem.appendChild(types);
                         delete _old;
                    }

                    for(int i=0;i<unselAttrs.size();++i)
                    {
                         QString msg = "";
                         QString errs = "";
                         XTT_Attribute* _old = AttributeGroups->indexOfAttId(unselAttrs[i].attribute("id", ""));
                         XTT_Attribute* _new = new XTT_Attribute();
                         _old->setId("eFeBeEjuBYHSLyQdkapm3SLqhfdcfv"); //random string
                         _new->in_HML_2_0(unselAttrs[i], msg);
                         _old->copyProperties(_new);
                         if(!hqed::sendEventMessage(&errs, XTT_SIGNAL_ATTRIBUTE_CHANGED, _old))
                              hqed::showMessages(errs);
                         xtt::fullVerification();
                         xtt::logicalFullVerification();
                         TableList->RefreshAll();
                         docElem.removeChild(attrs);
                         attrs.removeChild(unselAttrs[i]);
                         docElem.appendChild(attrs);
                         delete _new;
                    }
               }

               delete conWin;
          }
          else
          {
               delete conWin;
               return;
          }
     }

     // load specified content
     QString msg = "";
     QApplication::setOverrideCursor(Qt::WaitCursor);
     int result = vas_in_HML_2_0(docElem, msg, VAS_XML_MODEL_PARTS_XTT_TYPES | VAS_XML_MODEL_PARTS_XTT_ATTRS);
     QApplication::restoreOverrideCursor();

     if(result == VAS_XML_FORMAT_ERROR)
          QMessageBox::critical(this, "HQEd", "Loading error: " + hqed::retrieveMessageParts(msg));
     if(result == VAS_XML_XTT_SYNTAX_ERROR)
          hqed::showMessages(msg);
}
// -----------------------------------------------------------------------------

// #brief Export scene to svg file.
//
// #return No return value.
void MainWin_ui::exportSceneToSVGfile(void)
{
#if QT_VERSION >= 0x040300

     QStringList filters;
     filters      << "SVG files (*.svg)";

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
                                 tr("Export scene picture"),
                                 QDir::currentPath(),
                                 filters.at(0));
     if(fileName.isEmpty())
          return;

     QFile dev(fileName);
     if(!dev.open(QIODevice::WriteOnly /*| QIODevice::Text*/))
     {
          QMessageBox::critical(this, "HQEd", "Export error: cannot open file.");
          return;
     }
     QGraphicsScene* scene = NULL;

     // Gdy tryb ARD
     if(appMode == ARD_MODE)
     {
          ard_modelScene->AdaptSceneSize();
          scene = ard_modelScene;
     }

     // Gdy tryb XTT
     if(appMode == XTT_MODE)
     {
          xtt_scene->AdaptSceneSize();
          scene = xtt_scene;
     }

     int result = vasSVG(&dev, scene);
     if(result == VAS_PICTURE_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not specified.");
     if(result == VAS_PICTURE_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not writable.");

#else
     QMessageBox::information(this, "HQEd", "No SVG supported");
#endif
}
// -----------------------------------------------------------------------------

// #brief Export scene as a picture.
//
// #param _format File format.
// #return No return value.
void MainWin_ui::exportSceneAsPicture(QString _format)
{
     QStringList formats, hints, filters;
     formats << "BMP" << "JPG" << "PNG" << "PPM" << "XBM" << "XPM";
     hints << "Windows Bitmap" << "Joint Photographic Experts Group" << "Portable Network Graphics" << "Portable Pixmap" << "X11 Bitmap" << "X11 Pixmap";
     int imageCompression = 100-Settings->xttImageCompression();
     if(appMode == ARD_MODE)
          imageCompression = 100-Settings->ardImageCompression();

     QString msgTitle = "HQEd";

     int fi = formats.indexOf(_format.toUpper());
     if(fi == -1)
     {
          QMessageBox::information(this, msgTitle, "Unknown file format " + _format + ".");
          return;
     }

     filters << hints.at(fi) + " files (*." + _format.toLower() + ")";

     // Ustalanie nazwy pliku
     QString fileName = "";
     fileName = QFileDialog::getSaveFileName(this,
                       tr("Export scene picture"),
                       QDir::currentPath(),
                       filters.at(0));

     if(fileName.isEmpty())
          return;

     QFile dev(fileName);
     if(!dev.open(QIODevice::WriteOnly))
     {
          QMessageBox::critical(this, "HQEd", "Export error: cannot open file.");
          return;
     }
     QGraphicsScene* scene = NULL;

     // Gdy tryb ARD
     if(appMode == ARD_MODE)
     {
          ard_modelScene->AdaptSceneSize();
          scene = ard_modelScene;
     }

     // Gdy tryb XTT
     if(appMode == XTT_MODE)
     {
          xtt_scene->AdaptSceneSize();
          scene = xtt_scene;
     }

     int result = vasPicture(&dev, scene, _format, imageCompression);
     if(result == VAS_PICTURE_UNKNOWN_FORMAT)
          QMessageBox::critical(this, "HQEd", "Export error: the given format is not supported.");
     if(result == VAS_PICTURE_NOT_SAVED)
          QMessageBox::critical(this, "HQEd", "Export error: saving error.");
     if(result == VAS_PICTURE_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not specified.");
     if(result == VAS_PICTURE_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not writable.");
}
// -----------------------------------------------------------------------------

// #brief Export scene to png file.
//
// #return No return value.
void MainWin_ui::exportSceneToPNGfile(void)
{
     exportSceneAsPicture("PNG");
}
// -----------------------------------------------------------------------------

// #brief Export scene to bmp file.
//
// #return No return value.
void MainWin_ui::exportSceneToBMPfile(void)
{
     exportSceneAsPicture("BMP");
}
// -----------------------------------------------------------------------------

// #brief Export scene to jpg file.
//
// #return No return value.
void MainWin_ui::exportSceneToJPGfile(void)
{
     exportSceneAsPicture("JPG");
}
// -----------------------------------------------------------------------------

// #brief Export scene to ppm file.
//
// #return No return value.
void MainWin_ui::exportSceneToPPMfile(void)
{
     exportSceneAsPicture("PPM");
}
// -----------------------------------------------------------------------------

// #brief Export scene to xbm file.
//
// #return No return value.
void MainWin_ui::exportSceneToXBMfile(void)
{
     exportSceneAsPicture("XBM");
}
// -----------------------------------------------------------------------------

// #brief Export scene to xpm file.
//
// #return No return value.
void MainWin_ui::exportSceneToXPMfile(void)
{
     exportSceneAsPicture("XPM");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to svg file.
//
// #return No return value.
void MainWin_ui::exportTPHtoSVGfile(void)
{
#if QT_VERSION >= 0x040300

     QStringList filters;
     filters      << "SVG files (*.svg)";

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
                                 tr("Export scene picture"),
                                 QDir::currentPath(),
                                 filters.at(0));
     if(fileName.isEmpty())
          return;

     QFile dev(fileName);
     if(!dev.open(QIODevice::WriteOnly | QIODevice::Text))
     {
          QMessageBox::critical(this, "HQEd", "Export error: cannot open file.");
          return;
     }
     QGraphicsScene* scene;

     ard_thpScene->AdaptSceneSize();
     scene = ard_thpScene;

     int result = vasSVG(&dev, scene);
     if(result == VAS_PICTURE_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not specified.");
     if(result == VAS_PICTURE_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not writable.");

#else
     QMessageBox::information(this, "HQEd", "No SVG supported");
#endif
}
// -----------------------------------------------------------------------------

// #brief Export tph as picture.
//
// #param _format File format.
// #return No return value.
void MainWin_ui::exportTPHasPicture(QString _format)
{
     QStringList formats, hints, filters;
     formats << "BMP" << "JPG" << "PNG" << "PPM" << "XBM" << "XPM";
     hints << "Windows Bitmap" << "Joint Photographic Experts Group" << "Portable Network Graphics" << "Portable Pixmap" << "X11 Bitmap" << "X11 Pixmap";
     const char* cFormats[] = {"BMP", "JPG", "PNG", "PPM", "XBM", "XPM"};
     int imageCompression = 100-Settings->ardImageCompression();

     int fi = formats.indexOf(_format.toUpper());
     if(fi == -1)
     {
          QMessageBox::information(this, "HQEd", "Unknown file format " + _format + ".");
          return;
     }

     filters << hints.at(fi) + " files (*." + _format.toLower() + ")";

     // Ustalanie nazwy pliku
     QString fileName = "";
     fileName = QFileDialog::getSaveFileName(this,
                       tr("Export scene picture"),
                       QDir::currentPath(),
                       filters.at(0));

     if(fileName == "")
          return;

     // Obszar sceny do zrzutu
     QRect srect;

     // Poprawienie rozmiaru sceny, tak aby plik svg mial odpowiedni rozmiar

     // Gdy tryb ARD
     ard_thpScene->AdaptSceneSize();

     // Zapisanie wymiarow sceny
     srect.setX((int)ard_thpScene->sceneRect().x());
     srect.setY((int)ard_thpScene->sceneRect().y());
     srect.setWidth((int)ard_thpScene->sceneRect().width());
     srect.setHeight((int)ard_thpScene->sceneRect().height());

     // Tworzenie pliku graficznego
     QPainter painter;
     QPixmap* pixmap = new QPixmap(srect.width(), srect.height());

     painter.begin(pixmap);

     painter.setPen(QColor(Qt::black));
     painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
     painter.drawRect(pixmap->rect());

     ard_thpScene->render(&painter);

     painter.end();

     if(!pixmap->save(fileName, cFormats[fi], imageCompression))
          QMessageBox::information(this, "HQEd", "File not saved correctly.");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to png file.
//
// #return No return value.
void MainWin_ui::exportTPHtoPNGfile(void)
{
     exportTPHasPicture("PNG");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to bmp file.
//
// #return No return value.
void MainWin_ui::exportTPHtoBMPfile(void)
{
     exportTPHasPicture("BMP");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to jpg file.
//
// #return No return value.
void MainWin_ui::exportTPHtoJPGfile(void)
{
     exportTPHasPicture("JPG");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to ppm file.
//
// #return No return value.
void MainWin_ui::exportTPHtoPPMfile(void)
{
     exportTPHasPicture("PPM");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to xbm file.
//
// #return No return value.
void MainWin_ui::exportTPHtoXBMfile(void)
{
     exportTPHasPicture("XBM");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to xpm file.
//
// #return No return value.
void MainWin_ui::exportTPHtoXPMfile(void)
{
     exportTPHasPicture("XPM");
}
// -----------------------------------------------------------------------------

// #brief Export tph as picture.
//
// #param _format - File format.
// #return No return value.
void MainWin_ui::exportSTSasPicture(QString _format)
{
     QStringList formats, hints, filters;
     formats << "BMP" << "JPG" << "PNG" << "PPM" << "XBM" << "XPM";
     hints << "Windows Bitmap" << "Joint Photographic Experts Group" << "Portable Network Graphics" << "Portable Pixmap" << "X11 Bitmap" << "X11 Pixmap";
     const char* cFormats[] = {"BMP", "JPG", "PNG", "PPM", "XBM", "XPM"};
     int imageCompression = 100-Settings->ardImageCompression();

     int fi = formats.indexOf(_format.toUpper());
     if(fi == -1)
     {
          QMessageBox::information(this, "HQEd", "Unknown file format " + _format + ".");
          return;
     }

     filters << hints.at(fi) + " files (*." + _format.toLower() + ")";

     // Ustalanie nazwy pliku
     QString fileName = "";
     fileName = QFileDialog::getSaveFileName(this,
                       tr("Export scene picture"),
                       QDir::currentPath(),
                       filters.at(0));

     if(fileName == "")
          return;

     // Obszar sceny do zrzutu
     QRect srect;

     // Poprawienie rozmiaru sceny, tak aby plik svg mial odpowiedni rozmiar

     // Gdy tryb ARD
     xtt_state_scene->AdaptSceneSize();

     // Zapisanie wymiarow sceny
     srect.setX((int)xtt_state_scene->sceneRect().x());
     srect.setY((int)xtt_state_scene->sceneRect().y());
     srect.setWidth((int)xtt_state_scene->sceneRect().width());
     srect.setHeight((int)xtt_state_scene->sceneRect().height());

     // Tworzenie pliku graficznego
     QPainter painter;
     QPixmap* pixmap = new QPixmap(srect.width(), srect.height());

     painter.begin(pixmap);

     painter.setPen(QColor(Qt::black));
     painter.setBrush(QBrush(Qt::white, Qt::SolidPattern));
     painter.drawRect(pixmap->rect());

     xtt_state_scene->render(&painter);

     painter.end();

     if(!pixmap->save(fileName, cFormats[fi], imageCompression))
          QMessageBox::information(this, "HQEd", "File not saved correctly.");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to svg file.
//
// #return No return value.
void MainWin_ui::exportSTStoSVGfile(void)
{
     #if QT_VERSION >= 0x040300

     QStringList filters;
     filters      << "SVG files (*.svg)";

     // Ustalanie nazwy pliku
     QString fileName = QFileDialog::getSaveFileName(this,
                                 tr("Export scene picture"),
                                 QDir::currentPath(),
                                 filters.at(0));
     if(fileName.isEmpty())
          return;

     QFile dev(fileName);
     if(!dev.open(QIODevice::WriteOnly | QIODevice::Text))
     {
          QMessageBox::critical(this, "HQEd", "Export error: cannot open file.");
          return;
     }
     QGraphicsScene* scene;

     xtt_state_scene->AdaptSceneSize();
     scene = xtt_state_scene;

     int result = vasSVG(&dev, scene);
     if(result == VAS_PICTURE_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not specified.");
     if(result == VAS_PICTURE_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Export error: device is not writable.");

#else
     QMessageBox::information(this, "HQEd", "No SVG supported");
#endif
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to bmp file.
//
// #return No return value.
void MainWin_ui::exportSTStoBMPfile(void)
{
     exportSTSasPicture("BMP");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to jpg file.
//
// #return No return value.
void MainWin_ui::exportSTStoJPGfile(void)
{
     exportSTSasPicture("JPG");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to png file.
//
// #return No return value.
void MainWin_ui::exportSTStoPNGfile(void)
{
     exportSTSasPicture("PNG");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to ppm file.
//
// #return No return value.
void MainWin_ui::exportSTStoPPMfile(void)
{
     exportSTSasPicture("PPN");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to xbm file.
//
// #return No return value.
void MainWin_ui::exportSTStoXBMfile(void)
{
     exportSTSasPicture("XBM");
}
// -----------------------------------------------------------------------------

// #brief Export tph scene to xpm file.
//
// #return No return value.
void MainWin_ui::exportSTStoXPMfile(void)
{
     exportSTSasPicture("XPM");
}
// -----------------------------------------------------------------------------

// #brief Performs XTT+ model generation based on ARD+ model
//
// #return No return value.
void MainWin_ui::generateXTT(void)
{
     QString msgs[] = {"XTT generation succes",
                       "No ARD levels are defined",
                       "The last ARD level is not finalized"
                         };

     QApplication::setOverrideCursor(Qt::WaitCursor);
     int res = hqed::generateXTT();
     QApplication::restoreOverrideCursor();

     if(res > 0)
     {
          QMessageBox::critical(this, "HQEd", msgs[res]);
          return;
     }

     setCurrentMode(XTT_MODE);
     QMessageBox::information(this, "HQEd", msgs[res]);
     tablesArrangment();
}
// ----------------------------------------------------------------------------

// #brief Performs PROLOG code generation
//
// #return No return value.
void MainWin_ui::generatePROLOG(void)
{
     QString deffn = fXttFileName;
     if(deffn.contains("."))
          deffn.remove(QRegExp("\\.(xttml|hml)\\b"));

     QString plFileName = QFileDialog::getSaveFileName(this, tr("Generate To File"),
                              deffn,
                              tr("*.pl (*.pl)"));

     if(plFileName.isEmpty())
          return;

     QFile file(plFileName);
     if(!file.open(QIODevice::WriteOnly | QIODevice::Text))
     {
          QMessageBox::critical(this, tr("HQEd"), tr("Cannot read file %1:\n%2.").arg(plFileName).arg(file.errorString()));
          return;
     }

     QString msg;
     int result = vas_out_SWI_Prolog(&file, msg);

     if(result == VAS_SWI_PROLOG_UNKNOWN_DEVICE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not specified.");
     if(result == VAS_SWI_PROLOG_DEVICE_NOT_WRITABLE)
          QMessageBox::critical(this, "HQEd", "Saving error: device is not writable.");
     if(result == VAS_SWI_PROLOG_ERROR)
          hqed::showMessages(msg);
}
// ----------------------------------------------------------------------------

// #brief Exectue mode.
//
// #return No return value.
void MainWin_ui::ExecuteModel(void)
{
     xtt::run();
}
// ----------------------------------------------------------------------------

// #brief Create new xtt project with wizard.
//
// #return No return value.
void MainWin_ui::NewXttWithWizardClick(void)
{
     hqed::modelList()->createNewModel(true, XTT_MODE)->registerModel();
     showWizardDialog();
}
// -----------------------------------------------------------------------------

// #brief Show wizard dialog.
//
// #return No return value.
void MainWin_ui::showWizardDialog(void)
{
    Wizard = new Wizard_ui();
    Wizard->setWindowModality(Qt::ApplicationModal);
    Wizard->show();
}
// ----------------------------------------------------------------------------

// #brief Show startup dialog.
//
// #return No return value.
void MainWin_ui::showStartupDialog(void)
{
    delete StartupDialog;
    StartupDialog = new StartupDialog_ui();
    StartupDialog->setWindowModality(Qt::ApplicationModal);
    StartupDialog->setVisible(true);
}
// ----------------------------------------------------------------------------

// #brief Form 'open recent files' menu.
//
// #return No return value.
void MainWin_ui::formRecentFilesList(void)
{
    QStringList files = Settings->recentFiles();
    int numRecentFiles = qMin(files.size(), 5);
    
    if (numRecentFiles > 0)
    {
        menuOpen_recent->clear();
        menuOpen_recent->setEnabled(true);
        
        for (int i = 0; i < numRecentFiles; i++)
        {
            QAction* action_file = new QAction("recentFile", this);
            action_file->setText(QFileInfo(files[i]).baseName());
            action_file->setToolTip(files[i]);
            menuOpen_recent->addAction(action_file);
        }
        
        menuOpen_recent->addSeparator();
        menuOpen_recent->addAction(action_Clear_recent);
     
        connect(menuOpen_recent, SIGNAL(triggered(QAction*)), this, SLOT(openRecentClick(QAction*)));
        disconnect(action_Clear_recent, SIGNAL(triggered()), 0, 0);
        connect(action_Clear_recent, SIGNAL(triggered()), this, SLOT(clearRecentList()));
    }
    else
    {
        menuOpen_recent->setEnabled(false);   
    }
}
// ----------------------------------------------------------------------------

// #brief Open recent file.
//
// #param action - Action associated with menu entry of file to open.
// #return No return value.  
void MainWin_ui::openRecentClick(QAction* action)
{
    openFile(action->toolTip());
}
// ----------------------------------------------------------------------------

// #brief Clear recent files list.
//
// #return No return value.
void MainWin_ui::clearRecentList(void)
{
    Settings->clearRecentFiles();
    menuOpen_recent->setEnabled(false);
}
