#include <QVBoxLayout>
#include <QPushButton>
#include <QSplitter>
#include <QtHelp/QHelpContentWidget>
#include <QtHelp/QHelpIndexWidget>
#include <QEvent>
#include <QKeyEvent>
#include <QDebug>
#include <QTabWidget>
#include <QWidget>
#include <QtHelp/QHelpSearchResultWidget>
#include <QtHelp/QHelpSearchEngine>
#include <QtHelp/QHelpSearchQuery>
#include <QMessageBox>
#include <QList>
#include <QUrl>
#include <QString>
#include <QMainWindow>

#include "helpwidget.h"


HelpWidget::HelpWidget(HelpEngine *helpEngine, QWidget *parent) :
    QWidget(parent), dock(0) {
    this->helpEngine = helpEngine;
    setUp();
}

void HelpWidget::setUp() {
    // Main Page
    QWidget *content = helpEngine->getContentWidget();
    connect(content, SIGNAL(linkActivated(const QUrl &)), this, SLOT(showPage(QUrl)));

    // Search
    QWidget *search_page = new QWidget();
    QVBoxLayout *layout_search_page = new QVBoxLayout;

    searchQueryWidget = helpEngine->getSearchQueryWidget();
    searchQueryWidget->collapseExtendedSearch();
    searchResultWidget = helpEngine->getSearchResultWidget();

    connect(searchQueryWidget, SIGNAL(search()), this, SLOT(searchForQuery()));
    connect(searchResultWidget, SIGNAL(requestShowLink(QUrl)), this, SLOT(showRequestedLink(QUrl)));

    layout_search_page->addWidget(searchQueryWidget);
    layout_search_page->addWidget(searchResultWidget);
    search_page->setLayout(layout_search_page);

    // Index
    QWidget *index = helpEngine->getIndexWidget();//
    connect(index, SIGNAL(linkActivated(QUrl,QString)), this, SLOT(showRequestedLink(QUrl, QString)));

    // Left Tab
    leftTabWidget = new QTabWidget(this);

    leftTabWidget->addTab(content, "Main page");
    leftTabWidget->addTab(search_page, "Search");
    leftTabWidget->addTab(index, "Index");

    // Right Tab
    rightTabWidget = new QTabWidget(this);

    rightTabWidget->setTabsClosable(true);
    connect(rightTabWidget, SIGNAL(tabCloseRequested(int)), this, SLOT(removeTab(int)));

    // main layout
    QSplitter *splitter = new QSplitter(Qt::Horizontal);
    splitter->addWidget(leftTabWidget);
    splitter->addWidget(rightTabWidget);
    splitter->setStretchFactor(0, 1);
    splitter->setStretchFactor(1, 3);

    QVBoxLayout *layout_main = new QVBoxLayout;
    layout_main->addWidget(splitter);
    setLayout(layout_main);
}

void HelpWidget::setVisible(bool visible) {
    if (dock) {
        dock->setVisible(visible);
    } else {
        QWidget::setVisible(visible);
    }
}

void HelpWidget::setDockWidget(QDockWidget *d) {
    dock = d;
}

void HelpWidget::setMainWindow(QMainWindow *w) {
    w->installEventFilter(this);
}

bool HelpWidget::eventFilter(QObject *obj, QEvent *event) {
    if (event->type() == QEvent::KeyPress) { // F1
        QKeyEvent *ke = static_cast<QKeyEvent *>(event);
        if (ke->key() == Qt::Key_F1 || ke->key() == Qt::Key_Help) {
            if (obj->isWidgetType()) {
                QWidget *widget = static_cast<QWidget *>(obj) ->focusWidget();
                setVisible(true);
                showPage(widget);
                return true;
            }
        }
    }
    return false;
}

//
void HelpWidget::searchForQuery(){
    QList<QHelpSearchQuery> list = searchQueryWidget->query();
    if (list.isEmpty()) {
        return;
    }

    QStringList wordList = list.at(0).wordList;
    if (wordList.isEmpty()) {
        return;
    }

    query = wordList.at(0);

    helpEngine->search(list);
}

void HelpWidget::showRequestedLink(QUrl url){
    // TODO pozbyć się jakoś query
    HelpBrowser *browser = new HelpBrowser(helpEngine);

    browser->showHelpPage(url);

    QTextDocument *document = browser->getDocument();

    QTextCursor highlightCursor(document);

    QTextCharFormat *colorFormat = new QTextCharFormat();
    colorFormat->setBackground(Qt::yellow);

    while (!highlightCursor.isNull() && !highlightCursor.atEnd()) {
        highlightCursor = document->find(query, highlightCursor, QTextDocument::FindWholeWords);

        if (!highlightCursor.isNull()) {
            highlightCursor.movePosition(QTextCursor::EndOfWord, QTextCursor::KeepAnchor);
            QString str = highlightCursor.selectedText();
            int length = str.length();
            if (str.at(length - 1).isPunct()) {
                highlightCursor.movePosition(QTextCursor::Left, QTextCursor::KeepAnchor);
            }
            highlightCursor.mergeCharFormat(*colorFormat);
            highlightCursor.movePosition(QTextCursor::WordRight);
        }
    }

    rightTabWidget->addTab(browser, "Result");
    rightTabWidget->setCurrentWidget(browser);
}

void HelpWidget::showRequestedLink(QUrl url, QString str){
    HelpBrowser *browser = new HelpBrowser(helpEngine);
    browser->showHelpPage(url);
    rightTabWidget->addTab(browser, str);
    rightTabWidget->setCurrentWidget(browser);
}

void HelpWidget::showPage(QUrl url) {
    // TODO zmienić sposób wyznaczania nazw kart
    showRequestedLink(url, tr("Help"));
}

void HelpWidget::showPage(QWidget *widget) {
    HelpBrowser *browser = new HelpBrowser(helpEngine);
    if (browser->showHelpPage(widget)) {
        rightTabWidget->addTab(browser, tr("Help"));
        rightTabWidget->setCurrentWidget(browser);
    }
}

void HelpWidget::removeTab(int index){
    if(index >= 0) {
        rightTabWidget->removeTab(index);
    }
    return;
}
