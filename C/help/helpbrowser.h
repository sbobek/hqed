#ifndef HELPBROWSER_H
#define HELPBROWSER_H

#include <QTextBrowser>

#include "helpengine.h"

/**
 * \class HelpBrowser
 * \brief Help browser widget class.
 */
class HelpBrowser : public QWidget
{
    Q_OBJECT
public:
    /**
     * \brief Constructor.
     * \param helpEngine help engine, containing help
     */
    explicit HelpBrowser(HelpEngine *helpEngine, QWidget *parent = 0);

    /**
     * \brief Function shows help page for a given widget.
     * \param widget widget
     */
    bool showHelpPage(QWidget *);

    /**
     * \brief Function shows page under an URL.
     * \param url url
     */
    void showHelpPage(QUrl);

    /**
     * \brief QTextBrowser.document()
     */
    QTextDocument *getDocument();

signals:

public slots:
private:
    class Browser;

    HelpEngine *helpEngine;
    Browser *browser;
};

/**
 * \class HelpBrowser::Browser
 * \brief Simple private browser class, extending QTextBrowser.
 */
class HelpBrowser::Browser : public QTextBrowser {
public:
    Browser(QHelpEngine *engine, QWidget *parent = 0) :
        QTextBrowser(parent) {
        this->engine = engine;
    }

    QVariant loadResource(int type, const QUrl &url);

private:
    QHelpEngine *engine;
};

#endif // HELPBROWSER_H
