#ifndef HELPWIDGET_H
#define HELPWIDGET_H

#include <QWidget>
#include <QtHelp/QHelpEngine>
#include <QDockWidget>
#include <QMap>
#include <QString>
#include <QtHelp/QHelpSearchEngine>
#include <QtHelp/QHelpSearchQueryWidget>
#include <QTabWidget>
#include <QList>

#include "helpbrowser.h"
/**
 * \class HelpWidget
 * \brief A help widget class.
 *
 */
class HelpWidget : public QWidget
{
    Q_OBJECT
public:
    /**
     * \brief Constructor.
     * \param helpEngine Help engine containing help.
     * \param parent Parent widget.
     */
    explicit HelpWidget(HelpEngine *helpEngine, QWidget *parent = 0);

    /**
     * \brief Sets widget visibility.
     * If there is a dock widget set, it invokes its setVisible() method;
     * otherwise, it invokes this widget's setVisible() method.
     * \brief visibility visibility
     */
    void setVisible(bool);

    /**
     * \brief Sets dock widget.
     * \param dockWidget Dock widget.
     */
    void setDockWidget(QDockWidget *);

    /**
     * \brief Set main window.
     * It doesn't actually set the main window property (there is none),
     * it just invokes installEventFilter() method.
     * \param mainWindown The main window.
     */
    void setMainWindow(QMainWindow *);

protected:
    /**
     * \brief Event filtering method.
     * Shows this widget (if it's hidden) and opens a help browser on F1 key press.
     */
    bool eventFilter(QObject *, QEvent *);

signals:

public slots:
    /**
     * \brief Slot used to invoke HelpEngine search() method.
     */
    void searchForQuery();

    /**
     * \brief Show a link in a new opened tab (browser).
     * Highlights searched query.
     * \param url URL.
     */
    void showRequestedLink(QUrl);

    /**
     * \brief Show a link in a new opened tab (browser).
     * \param url URL.
     * \param label Tab title.
     */
    void showRequestedLink(QUrl, QString);

    /**
     * \brief Opens a page under the given URL.
     * \param url URL.
     */
    void showPage(QUrl);

    /**
     * \brief Opens a help page for a given widget.
     * \param widget Widget.
     */
    void showPage(QWidget *);

    /**
     * \brief This slot removes a tab.
     * \param i The tab index.
     */
    void removeTab(int);

private:
    /**
     * \brief Sets up an instance.
     */
    void setUp();

    HelpEngine *helpEngine;

    QDockWidget *dock;

    QHelpSearchQueryWidget  *searchQueryWidget;
    QHelpSearchResultWidget *searchResultWidget;

    QString query;

    QTabWidget *leftTabWidget ;
    QTabWidget *rightTabWidget ;
};

#endif // HELPWIDGET_H
