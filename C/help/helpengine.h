#ifndef HELPENGINE_H
#define HELPENGINE_H

#include <QObject>
#include <QHelpEngine>
#include <QHelpSearchQueryWidget>
#include <QHelpSearchResultWidget>

/**
 * \class HelpEngine
 * \brief An "extension" of QHelpEngine.
 * Contains map QWidget -> QString (wigdet -> label).
 */
class HelpEngine : public QObject
{
    Q_OBJECT
public:
    /**
     * \brief Constructor.
     * \param filename Name of .qhc file containing compiled help collection.
     * \param parent Parent object.
     */
    HelpEngine(QString filename, QObject *parent = 0);

    /**
     * \brief Constructor.
     * \param helpEngine QHelpEngine instance containing help.
     * \param parent Parent object.
     */
    HelpEngine(QHelpEngine *helpEngine, QObject *parent = 0);

    // helpful getters
    QHelpEngine *getQHelpEngine();
    QHelpContentWidget *getContentWidget();
    QHelpIndexWidget *getIndexWidget();
    QHelpSearchQueryWidget *getSearchQueryWidget();
    QHelpSearchResultWidget * getSearchResultWidget();

    /**
     * \brief Inserts (widget, label) pair into the map.
     * \param widget widget
     * \param label label
     */
    void insertKeyword(QWidget *, QString);

    /**
     * \brief Gets a help URL for a given widget.
     * Looks up widget's label in the map, and then finds the URL for that label in the help engine.
     * \param widget widget
     */
    QUrl getWidgetHelp(QWidget *);

signals:

public slots:
    /**
     * \brief Invokes QHelpSearchEngine's search() method.
     * \param list List of search queries.
     */
    void search(const QList<QHelpSearchQuery> &);

private:
    /**
     * \brief Sets up an instance.
     */
    void setUp();

    QHelpEngine *helpEngine;
    QMap<QWidget *, QString> map;
};

#endif // HELPENGINE_H
