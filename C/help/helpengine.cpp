#include <QDebug>

#include "helpengine.h"

HelpEngine::HelpEngine(QString filename, QObject *parent) :
    QObject(parent) {
    helpEngine = new QHelpEngine(filename, this);
    setUp();
}

HelpEngine::HelpEngine(QHelpEngine *helpEngine, QObject *parent) :
    QObject(parent) {
    this->helpEngine = helpEngine;
    setUp();
}

void HelpEngine::setUp() {
    if (!helpEngine->setupData()) {
        qDebug() << "HelpEngine(): QHelpEngine::setupData() failed";
        qDebug() << helpEngine->error();
        delete helpEngine;
        helpEngine = 0;
        return;
    }
    connect(helpEngine, SIGNAL(setupFinished()), helpEngine->searchEngine(), SLOT(reindexDocumentation()));
}

QHelpEngine *HelpEngine::getQHelpEngine() {
    return helpEngine;
}

QHelpContentWidget *HelpEngine::getContentWidget() {
    return helpEngine->contentWidget();
}

QHelpIndexWidget *HelpEngine::getIndexWidget() {
    return helpEngine->indexWidget();
}

QHelpSearchQueryWidget *HelpEngine::getSearchQueryWidget() {
    return helpEngine->searchEngine()->queryWidget();
}

QHelpSearchResultWidget * HelpEngine::getSearchResultWidget() {
    return helpEngine->searchEngine()->resultWidget();
}

void HelpEngine::insertKeyword(QWidget *widget, QString label) {
    map.insert(widget, label);
}

QUrl HelpEngine::getWidgetHelp(QWidget *widget) {
    if (!helpEngine) {
        return QUrl();
    }

    QMap<QWidget *, QString>::const_iterator it = map.find(widget);
    if (it == map.constEnd()) {
        return QUrl();
    }

    QString keyword = *it;
    QMap<QString, QUrl> links = helpEngine->linksForIdentifier(keyword);
    if (links.count()) {
        return links.constBegin().value();
    }
    return QUrl();
}

void HelpEngine::search(const QList<QHelpSearchQuery> &list) {
    helpEngine->searchEngine()->search(list);
}
