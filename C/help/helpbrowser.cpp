#include <QPushButton>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QDebug>

#include "helpbrowser.h"

HelpBrowser::HelpBrowser(HelpEngine *help, QWidget *parent) :
    QWidget(parent)
{
    if (!help) {
        qDebug() << "HelpBrowser(): null helpEngine\n";
        return;
    }

    helpEngine = help;
    browser = new Browser(helpEngine->getQHelpEngine());

    QPushButton *backButton = new QPushButton(tr("Go back"));
    QPushButton *forwardButton = new QPushButton(tr("Go forward"));

    connect(backButton, SIGNAL(clicked()), browser, SLOT(backward()));
    connect(forwardButton, SIGNAL(clicked()), browser, SLOT(forward()));

    QHBoxLayout *buttonLayout = new QHBoxLayout;
    buttonLayout->addWidget(backButton);
    buttonLayout->addWidget(forwardButton);
    buttonLayout->addStretch();

    QVBoxLayout *mainLayout = new QVBoxLayout;
    mainLayout->addLayout(buttonLayout);
    mainLayout->addWidget(browser);
    setLayout(mainLayout);
}

bool HelpBrowser::showHelpPage(QWidget *widget) {
    QUrl url = helpEngine->getWidgetHelp(widget);
    if (url.isEmpty()) {
        return false;
    }

    browser->setSource(url);
    return true;
}

void HelpBrowser::showHelpPage(QUrl url) {
    browser->setSource(url);
}

QTextDocument *HelpBrowser::getDocument() {
    return browser->document();
}

QVariant HelpBrowser::Browser::loadResource(int type, const QUrl &url) {
    if (url.scheme() == "qthelp") {
        return QVariant(engine->fileData(url));
    } else {
        return QTextBrowser::loadResource(type, url);
    }
}
