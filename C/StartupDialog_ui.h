/**
* \file	    StartupDialog_ui.h
* \author	Opioła, Pasek, Zieliński
* \date 	27.07.2012
* \version  1.0
* \brief	This file defines class that represents startup dialog
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef STARTUPDIALOG_UI_H
#define STARTUPDIALOG_UI_H

#include <QDialog>
#include "ui_StartupDialog.h"

/**
* \class 	StartupDialog_ui
* \author   Opioła, Pasek, Zieliński
* \date 	27.07.2012
* \brief 	This class represents startup dialog
*/

class StartupDialog_ui : public QDialog, public Ui_StartupDialog
{
    Q_OBJECT

private:
    /// \brief Forms a list of recently used files.
    ///
    /// \return No return value.
    void formLastUsedList(void);

public:
  
    /// \brief Constructor of Start_ui class.
    ///
    /// \param parent Pointer to parent object.
    StartupDialog_ui(QWidget *parent = 0);
     
    /// \brief Destructor of Start_ui class.
    ~StartupDialog_ui();
    
private slots:
  
    /// \brief Opens clicked recent file.
    ///
    /// \param item Pointer to clicked list item
    /// \return No return value.
    void recentFileClicked(QListWidgetItem *item);

    /// \brief Closes startup dialog.
    ///
    /// \return No return value.
    void closeDialog(void);
};

extern StartupDialog_ui *StartupDialog;

#endif // STARTUPDIALOG_UI_H
