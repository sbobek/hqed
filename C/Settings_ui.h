/**
 * \file	Settings_ui.h
 * \author	Krzysztof Kaczor kinio4444@gmail.com
 * \date 	06.11.2007 
 * \version	1.0
 * \brief	This file defines class arranges window configuration.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \bug No destructor.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef SettingsH
#define SettingsH
// -----------------------------------------------------------------------------

#include <QGridLayout>
#include <QAbstractSocket>

#include "ui_Settings.h"
// -----------------------------------------------------------------------------

#define SELECT_ONLY_NODE 0		///< Node click behavior.
#define SELECT_CONNECTIONS 1	///< Node click behavior
#define SELECT_CONNECTIONS_WITH_CTRL 2	///< Node click behavior.

#define XTT_TABLE_EDIT_MODE_FULL 0
#define XTT_TABLE_EDIT_MODE_SIMPLE 1
// -----------------------------------------------------------------------------

class QSettings;
// -----------------------------------------------------------------------------
/**
* \class 	Settings_ui
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	14.01.2008
* \brief 	This class arranges window configuration.
*/
class Settings_ui : public QWidget, private Ui_Settings
{
    Q_OBJECT
    
private:

    QSettings* conf;          ///< Pointer to application configuration.

    QGridLayout* mainGL;          ///< Layout of the widget
    QGridLayout* swp0GL;          ///< Layout of stacked widget page 0
    QGridLayout* swp0gb27GL;      ///< Layout of stacked widget page 0 groupBox27
    QGridLayout* swp0gb28GL;      ///< Layout of stacked widget page 0 groupBox28
    QGridLayout* swp0gb29GL;      ///< Layout of stacked widget page 0 groupBox29
    QGridLayout* swp0gb30GL;      ///< Layout of stacked widget page 0 groupBox30
    QGridLayout* swp0gb35GL;      ///< Layout of stacked widget page 0 groupBox35
    QGridLayout* swp0gb36GL;      ///< Layout of stacked widget page 0 groupBox36
    QGridLayout* swp0gb39GL;      ///< Layout of stacked widget page 0 groupBox39

    QGridLayout* swp1GL;          ///< Layout of stacked widget page 1
    QGridLayout* swp1gb20GL;      ///< Layout of stacked widget page 1 groupBox20
    QGridLayout* swp1gb21GL;      ///< Layout of stacked widget page 1 groupBox21
    QGridLayout* swp1gb25GL;      ///< Layout of stacked widget page 1 groupBox25
    QGridLayout* swp1gb26GL;      ///< Layout of stacked widget page 1 groupBox26

    QGridLayout* swp2GL;          ///< Layout of stacked widget page 2
    QGridLayout* swp2gb17GL;      ///< Layout of stacked widget page 2 groupBox17
    QGridLayout* swp2gb18GL;      ///< Layout of stacked widget page 2 groupBox18
    QGridLayout* swp2gb19GL;      ///< Layout of stacked widget page 2 groupBox19
    QGridLayout* swp2gb34GL;      ///< Layout of stacked widget page 2 groupBox34

    QGridLayout* swp3GL;          ///< Layout of stacked widget page 3
    QGridLayout* swp3gb0GL;       ///< Layout of stacked widget page 3 groupBox
    QGridLayout* swp3gb3GL;       ///< Layout of stacked widget page 3 groupBox3
    QGridLayout* swp3gb8GL;       ///< Layout of stacked widget page 3 groupBox8

    QGridLayout* swp4GL;          ///< Layout of stacked widget page 4
    QGridLayout* swp4gb7GL;       ///< Layout of stacked widget page 4 groupBox
    QGridLayout* swp4gb9GL;       ///< Layout of stacked widget page 4 groupBox3
    QGridLayout* swp4gb10GL;      ///< Layout of stacked widget page 4 groupBox8
    QGridLayout* swp4gb11GL;      ///< Layout of stacked widget page 4 groupBox8
    QGridLayout* swp4gb14GL;      ///< Layout of stacked widget page 4 groupBox8
    QGridLayout* swp4gb31GL;      ///< Layout of stacked widget page 4 groupBox31groupBox31
    QGridLayout* swp4gb38GL;      ///< Layout of stacked widget page 4 groupBox38

    QGridLayout* swp5GL;          ///< Layout of stacked widget page 5
    QGridLayout* swp5gb2GL;       ///< Layout of stacked widget page 5 groupBox2
    QGridLayout* swp5gb13GL;      ///< Layout of stacked widget page 5 groupBox13
    QGridLayout* swp5gb12GL;      ///< Layout of stacked widget page 5 groupBox12
    QGridLayout* swp5gb37GL;      ///< Layout of stacked widget page 5 groupBox37
    QGridLayout* swp5gb33GL;      ///< Layout of stacked widget page 5 groupBox33

    QGridLayout* swp6GL;          ///< Layout of stacked widget page 6
    QGridLayout* swp6gb15GL;      ///< Layout of stacked widget page 6 groupBox15
    QGridLayout* swp6gb16GL;      ///< Layout of stacked widget page 6 groupBox16
    QGridLayout* swp6gb32GL;      ///< Layout of stacked widget page 6 groupBox32

    /// \brief Functions that layouts the items on the widget
    ///
    /// \return no values return
    void layoutForm(void);
     
public:

     /// \brief Constructor of Settings_ui class.
     ///
     /// \param parent Pointer to parent object.
     Settings_ui(QWidget *parent = 0);

     /// \brief Destructor of Settings_ui class.
     ~Settings_ui(void);	

     /// \brief Get information when startup dialog should be shown.
     ///
     /// \return Information whether startup dialog should be shown
     /// \li True - startup dialog should be shown.
     /// \li False - startup dialog should not be shown.
     bool useStartupDialog(void);
     
     /// \brief Set whether startup dialog should be shown.
     ///
     /// \param use - set or unset startup dialog showing
     /// \return No return value.
     void setStartupDialogShowing(bool use);
     
     /// \brief Get if the tables should be hide automatically
     ///
     /// \return if the tables should be automatically hide:
     /// \li 0 - tables will not be hide automatically
     /// \li 1 - tables will be hide automatically
     bool xttTableAutoHide(void);
     
     /// \brief Indicates if the initial tables should be displayed on the main xtt scene
     ///
     /// \return if the initial tables should be displayed on the main xtt scene:
     /// \li 0 - tables will not be showed on main xtt scene
     /// \li 1 - tables will be showed on main xtt scene
     bool xttShowInitialTables(void);
     
     /// \brief Function that returns the type of default edit mode of xtt table
     ///
     /// \return the type of default edit mode of xtt table
     int xttTableEditMode(void);

     /// \brief Get information when scrollbars should be visible.
     ///
     /// \return Scroll bar policy.
     Qt::ScrollBarPolicy sceneScorllBarPolicy(void);

     /// \brief Get zoom value for the scene.
     ///
     /// \param appMode Mode of application.
     /// \return Zoom value.
     double zoomFactorValue(int appMode);

     /// \brief Get decision about using framed nodes ARD and TPH.
     ///
     /// \return Decision about using framed nodes.
     /// \li True - use framed nodes.
     /// \li False - don't use framed nodes.
     bool useFramedNodes(void);

     /// \brief Get decision about using zoom mouse wheel.
     ///
     /// \param appMode Mode of application.
     /// \return Decision about using zoom mouse whee.
     /// \li True - use zoom mouse whee.
     /// \li False - don't use zoom mouse whee.
     bool useZoomMouseWhell(int appMode);

     /// \brief Get decision about automatic creating name of acronym.
     ///
     /// \return Decision about automatic creating name of acronym.
     /// \li True - create name of acronym.
     /// \li False - don't create name of acronym.
     bool xttCreateAutoAcronym(void);

     /// \brief Get decision about automatic creating name of table.
     ///
     /// \return Decision about automatic creating name of table.
     /// \li True - create name of table.
     /// \li False - don't create name of table.
     bool xttCreateTableAutoName(void);

     /// \brief Get decision about displaying message when error occurs.
     ///
     /// \return Decision about displaying error message.
     /// \li True - display error message.
     /// \li False - don't display error message.
     bool xttShowErrorMessages(void);

     /// \brief Get decision about displaying error cell message.
     ///
     /// \return Decision about displaying error cell message.
     /// \li True - display error cell message.
     /// \li False - don't display error cell message.
     bool xttShowErrorCellMessage(void);

     /// \brief Get decision about highlighting error cell.
     ///
     /// \return Decision about highlighting error cell.
     /// \li True - highlight error cell.
     /// \li False - don't highlight error cell.
     bool xttHighlightErrorCell(void);

     /// \brief Get decision about checking values (that are not arythmic value) in cell.
     ///
     /// \return Decision about checking values (that are not arythmic value) in cell.
     /// \li True - check values in cell.
     /// \li False - don't check values in cell.
     bool xttCheckAtomicValues(void);

     /// \brief Get decision about executing cell during analysis when attribute comparing is turned off.
     ///
     /// \return Decision about executing cell during analysis when attribute comparing is turned off.
     /// \li True - execute cell.
     /// \li False - don't execute cell.
     bool xttExecuteCellWhenOff(void);

     /// \brief Get decision about executing cell during analysis when attribute comparing is turned on.
     ///
     /// \return Decision about executing cell during analysis when attribute comparing is turned on.
     /// \li True - execute cell.
     /// \li False - don't execute cell.
     bool xttExecuteCellWhenOn(void);

     /// \brief Get decision about displaying erron tool tip in cell.
     ///
     /// \return Decision about displaying erron tool tip in cell.
     /// \li True - display error.
     /// \li False - don't display error.
     bool xttShowErrorInToolTip(void);

     /// \brief Get default value of attribute comparing in cell.
     ///
     /// \return Default value of attribute comparing in cell.
     /// \li True - the same attributes.
     /// \li False - differnet attributes.
     bool xttAttrToAttrComp(void);

     /// \brief Get decision about automatic recognizing table type.
     ///
     /// \return Decision about automatic recognizing table type.
     /// \li True - turn on automatic recognizing table type.
     /// \li False - don't turn on automatic recognizing table type.
     bool autoTableTypeRecognition(void);

     /// \brief Get decision about displaying worning about wrong table type.
     ///
     /// \return Decision about displaying wrong table type.
     /// \li True - display worning about wrong table type.
     /// \li False - don't display worning about wrong table type.
     bool messageAboutAmbiguousTableType(void);

     /// \brief Get decision about displaying worning about wrong table type.
     ///
     /// \return Decision about displaying wrong table type.
     /// \li True - display worning about wrong table type.
     /// \li False - don't display worning about wrong table type.
     bool massageAboutInaccesibleRow(void);

     /// \brief Get decision about displaying map movement for table.
     ///
     /// \return Decision about displaying map movement for table.
     /// \li True - display map movement for table.
     /// \li False - don't display map movement for table.
     bool createMapMovement(void);

     /// \brief Get decision about highlightling current cell.
     ///
     /// \return Decision about highlightling current cell.
     /// \li True - highlight current cell.
     /// \li False - don't highlight current cell.
     bool highlightCurrentCell(void);

     /// \brief Get decision about beeping after analisys.
     ///
     /// \return Decision about beeping after analisys.
     /// \li True - beep after analisys.
     /// \li False - don't beep after analisys.
     bool beepOnFinish(void);

     /// \brief Get decision about beeping after analisys.
     ///
     /// \return Decision about beeping after analisys.
     /// \li True - beep after analisys.
     /// \li False - don't beep after analisys.
     bool showRunCellStatus(void);

     /// \brief Get decision about displaying system tray icon.
     ///
     /// \return Decision about displaying system tray icon.
     /// \li True - display system tray icon,
     /// \li False - don't display system tray icon
     bool showSystemTrayIcon(void);

     /// \brief Get decision about displaying tray tip.
     ///
     /// \return Decision about displaying tray tip.
     /// \li True - display tray tip.
     /// \li False - don't display tray tip.
     bool showTrayTips(void);

     /// \brief Get decision about autocentring ARD elements.
     ///
     /// \return Decision about autocentring ARD elements.
     /// \li True - autocentre ARD elements.
     /// \li False - don't autocentre ARD elements.
     bool ardEnabledAutocentering(void);

     /// \brief Get decision about autocentring XTT elements.
     ///
     /// \return Decision about autocentring XTT elements.
     /// \li True - autocentre XTT elements.
     /// \li False - don't autocentre XTT elements.
     bool xttEnabledAutocentering(void);

     /// \brief Get decision about autoresizing XTT scene.
     ///
     /// \return Decision about autoresizing XTT scene.
     /// \li True - autoresize XTT scene.
     /// \li False - don't autoresize XTT scene.
     bool xttAutoSceneResize(void);

     /// \brief Get decision about formating XML files in hierarchy way.
     ///
     /// \return Decision about formating XML files in hierarchy way.
     /// \li True - format XML files in hierarchy way.
     /// \li False - don't format XML files in hierarchy way.
     bool xmlAutoformating(void);

     /// \brief Get if the hqedSerwer should start automatically when application starts
     ///
     /// \return true if the hqedSerwer should start automatically when application starts, otherwise false
     bool hqedServerAutostart(void);

     /// \brief Get decision about creating initial tables during the XTT schema generation process.
     ///
     /// \return Get decision about creating initial tables during the XTT schema generation process.
     bool createInitialTables(void);

     /// \brief Get decision about policy of duplicate connections.
     ///
     /// \return Get decision about policy of duplicate connections.
     bool allowDuplicateConnection(void);

     /// \brief Get decision about XTT optimalization after schema generating
     ///
     /// \return Get decision about XTT optimalization after schema generating.
     bool optimizeXTT(void);

     /// \brief Returns if the TPH diagram should be displayed
     ///
     /// \return true if the TPH diagram should be displayed, otherwise false
     bool ardTPHdiagramDockWidget(void);
     
     /// \brief Returns if the ARD localizator should be displayed
     ///
     /// \return true if the ARD localizator should be displayed, otherwise false
     bool ardLocalizatorDockWidget(void);
     
     /// \brief Returns if the TPH localizator should be displayed
     ///
     /// \return true if the TPH localizator should be displayed, otherwise false
     bool tphLocalizatorDockWidget(void);
     
     /// \brief Returns if the search widget should be displayed
     ///
     /// \return true if the search widget should be displayed, otherwise false
     bool xttSearchDockWidget(void);

     /// \brief Returns if the error dock widget should be displayed
     ///
     /// \return true if the error odck widget should be displayed, otherwise false
     bool xttErrorDockWidget(void);

     /// \brief Returns if the execute dock widget should be displayed. The widget contains messages generated during executing model.
     ///
     /// \return true if the execute dock widget should be displayed, otherwise false
     bool xttExecuteDockWidget(void);
     
     /// \brief Returns if the localizator dock widget should be displayed. The widget supports navigation in the model
     ///
     /// \return true if the localizator dock widget should be displayed, otherwise false
     bool xttLocalizatorDockWidget(void);
     
     /// \brief Returns if the plugin event listener dock widget should be displayed.
     ///
     /// \return true if the plugin event listener dock widget should be displayed, otherwise false
     bool xttPluginEventListenerDockWidget(void);
     
     /// \brief Returns if the states dock widget should be displayed.
     ///
     /// \return true if the states dock widget should be displayed, otherwise false
     bool xttStatesDockWidget(void);

     /// \brief Returns if the neutral zeros must be added before value w.r.t. length value
     ///
     /// \return true if the neutral zeros must be added before value w.r.t. length value, otherwise false
     bool addNeutralZerosBefore(void);
     
     /// \brief Returns if the neutral zeros must be added after value w.r.t. scale value
     ///
     /// \return true if the neutral zeros must be added after value w.r.t. scale value, otherwise false
     bool addNeutralZerosAfter(void);
     
     /// \brief Returns if the attributes should be refered by acronyms instead of names
     ///
     /// \return true if the attributes should be refered by acronyms instead of names
     bool attsReferenceByAcronyms(void);

     /// \brief Get default name of the initial tables
     ///
     /// \return String that is a default name of the initial tables
     QString xttInitialTableDefaultName(void);
     
     /// \brief Get base name for automatic table.
     ///
     /// \return String containing base name for automatic table.
     QString xttTableBaseName(void);

     /// \brief Get string that replayces space in attribute values.
     ///
     /// \return String replacing space in attribute values.
     QString spaceReplacer(void);

     /// \brief Get port number for hqedServer
     ///
     /// \return TCP port number for listening by hqedServer
     int hqedServerListenPort(void);

     /// \brief Get default column width.
     ///
     /// \return Default column width.
     int xttDefaultColumnWidth(void);

     /// \brief Get default row height.
     ///
     /// \return Default column height.
     int xttDefaultRowHeight(void);

     /// \brief Get default header height.
     ///
     /// \return Default header height.
     int xttDefaultHeaderHeight(void);

     /// \brief Get distance of ARD element on the scene when button for shifting is clicked.
     ///
     /// \return Distance of element.
     int ardMoveElementsDistance(void);

     /// \brief Get distance of XTT element on the scene when button for shifting is clicked.
     ///
     /// \return Distance of element.
     int xttMoveElementsDistance(void);

     /// \brief Get distance between connections comming out of the same table.
     ///
     /// \return Distance between connections.
     int xttConnectionsDistance(void);

     /// \brief Get progress (in percent) of compression graphic file being screen of scene ARD model.
     ///
     /// \return Distance progress (in percent) of compression.
     int ardImageCompression(void);

     /// \brief Get progress (in percent) of compression graphic file being screen of scene XTT model.
     ///
     /// \return Distance progress (in percent) of compression.
     int xttImageCompression(void);

     /// \brief Get node select policy in ARD model.
     ///
     /// \return Node select policy.
     int ardNodeSelectPolicy(void);

     /// \brief Get node select policy in XTT model.
     ///
     /// \return Node select policy.
     int xttTableSelectPolicy(void);
     
     /// \brief get the default accuracy for numeric types
     ///
     /// \return the default accuracy for numeric types
     int defaultAccuracy(void);
     
     /// \brief Get node select policy in XTT model.
     ///
     /// \return Node select policy.
     int largeDomainSize(void);

     /// \brief Get color of table in XTT model.
     ///
     /// \return Color of table.
     QColor xttTableColor(void);

     /// \brief Get color of selected table in XTT model.
     ///
     /// \return Color of selected table.
     QColor xttSelectedTableColor(void);

     /// \brief Get color of connection in XTT model.
     ///
     /// \return Color of connection.
     QColor xttConnectionColor(void);

     /// \brief Get color of selected connection in XTT model.
     ///
     /// \return Color of selected connection.
     QColor xttSelectedConnectionColor(void);

     /// \brief Get color of cut connection in XTT model.
     ///
     /// \return Color of cut connection.
     QColor xttCutConnectionColor(void);

     /// \brief Get table font color in XTT model.
     ///
     /// \return Table font color.
     QColor xttTableFontColor(void);

     /// \brief Get table font color and table title in XTT model.
     ///
     /// \return Color table font color and table title.
     QColor xtTableTitleFontColor(void);

     /// \brief Get font color of connection in XTT model.
     ///
     /// \return Table font color of connection.
     QColor xttConnectionLabelColor(void);

     /// \brief Get scene background color in XTT model.
     ///
     /// \return Scene background color.
     QColor xttSceneBackgroundColor(void);

     /// \brief Get color of highlighted cell in XTT model.
     ///
     /// \return Color of highlighted cell.
     QColor xttHoveredCellColor(void);

     /// \brief Get cell color with error in XTT model.
     ///
     /// \return Cell color with error.
     QColor xttErrorCellColor(void);

     /// \brief Get cell color with error in XTT model.
     ///
     /// \return Cell color with error.
     QColor xttTableBorderColor(void);
     
     /// \brief Function that returns the second color of the table rows
     ///
     /// \return second color of the table rows
     QColor xttTable2Color(void);
     
     /// \brief Function that returns the second color of the selected table rows
     ///
     /// \return second color of the selected table rows
     QColor xttSelectedTable2Color(void);

     /// \brief Get color of ordinary TPH node.
     ///
     /// \return Color of ordinary TPH node.
     QColor ardTPHnodeColor(void);

     /// \brief Get color of selected TPH node.
     ///
     /// \return Color of selected TPH node.
     QColor ardTPHselectedNodeColor(void);

     /// \brief Get color of not tranformed TPH node.
     ///
     /// \return Color of not transformed TPH node.
     QColor ardTPHnotTransformatedNodeColor(void);

     /// \brief Get font color of TPH node.
     ///
     /// \return Font color of TPH node.
     QColor ardTPHfontColor(void);

     /// \brief Get branch color of TPH node.
     ///
     /// \return Branch color of TPH node.
     QColor ardTPHbranchColor(void);

     /// \brief Get scene color of TPH.
     ///
     /// \return Scene color of TPH.
     QColor ardTPHsceneColor(void);

     /// \brief Get color of ARD properties.
     ///
     /// \return Color of ARD properties.
     QColor ardPropertyColor(void);

     /// \brief Get color of selected ARD properties.
     ///
     /// \return Color of selected ARD properties.
     QColor ardSelectedPropertyColor(void);

     /// \brief Get font color of ARD properties.
     ///
     /// \return Font color of ARD properties.
     QColor ardPropertyFontColor(void);

     /// \brief Get font color of ARD level label.
     ///
     /// \return Font color of ARD level label.
     QColor ardLevelLabelFontColor(void);

     /// \brief Get line color of ARD dependency.
     ///
     /// \return Line color of ARD dependency.
     QColor ardDependencyColor(void);

     /// \brief Get active line color of ARD dependency.
     ///
     /// \return Active line color of ARD dependency.
     QColor ardSelectedDependencyColor(void);

     /// \brief Get color of ARD scene.
     ///
     /// \return Color of ARD scene.
     QColor ardSceneColor(void);

     /// \brief Get color of ARD margin.
     ///
     /// \return Color of ARD margin.
     QColor ardMarginsColor(void);

     /// \brief Get color of the font of xtt table header
     ///
     /// \return Color of the font of xtt table header
     QColor xttTableHeaderFontColor(void);

     /// \brief Get color of the xtt table header
     ///
     /// \return Color of the xtt table header
     QColor xttTableHeaderColor(void);

     /// \brief Get table font in XTT model.
     ///
     /// \return Table font in XTT model.
     QFont xttTableFont(void);

     /// \brief Get table header font in XTT model.
     ///
     /// \return Table header font in XTT model.
     QFont xttTableHeaderFont(void);

     /// \brief Get table title font in XTT model.
     ///
     /// \return Table title font in XTT model.
     QFont xttTableTitleFont(void);

     /// \brief Get connection lable font in XTT model.
     ///
     /// \return Connection label font in XTT model.
     QFont xttConnectionLabelFont(void);

     /// \brief Get TPH font in ARD model.
     ///
     /// \return TPH font in ARD model.
     QFont ardTPHFont(void);

     /// \brief Get diagram font in ARD model.
     ///
     /// \return Diagram font in ARD model.
     QFont ardARDdiagramFont(void);

     /// \brief Apply settings in application.
     ///
     /// \return No return value.
     void applyOptions(void);

     /// \brief Save settings in application.
     ///
     /// \return No return value.
     void saveOptions(void);

     /// \brief Load settings in application.
     ///
     /// \return No return value.
     void loadOptions(void);

     /// \brief Set color mode.
     ///
     /// \param _cm Set or unset graphic mode.
     /// \param appMode Mode of application.
     /// \return No return value.
     void setColorMode(bool _cm, int appMode);

     /// \brief Set atnialiasing mode.
     ///
     /// \param _nm Set or unset atnialiasing mode.
     /// \param appMode Mode of application.
     /// \return No return value.
     void setAntialiasingMode(bool _nm, int appMode);

     /// \brief Set atnialiasing mode for text.
     ///
     /// \param _nm Set or unset atnialiasing mode for text.
     /// \param appMode Mode of application.
     /// \return No return value.
     void setTextAntialiasingMode(bool _nm, int appMode);

     /// \brief Set smooth tranformation for bitmap.
     ///
     /// \param _nm Set or unset smooth tranformation for bitmap.
     /// \param appMode Mode of application.
     /// \return No return value.
     void setSmoothPixmapTransformMode(bool _nm, int appMode);

     /// \brief Get color type for application mode.
     ///
     /// \param appMode Mode of application.
     /// \return Color type for application mode.
     bool colorMode(int appMode);

     /// \brief Get decision about using antyaliasing for application mode.
     ///
     /// \param appMode Mode of application.
     /// \return Decision about using antyaliasing for application mode.
     /// \li True - use antyaliasing.
     /// \li False - don't use antyaliasing.
     bool useAntialiasing(int appMode);

     /// \brief Get decision about using text antyaliasing for application mode.
     ///
     /// \param appMode Mode of application.
     /// \return Decision about using text antyaliasing for application mode.
     /// \li True - use text antyaliasing.
     /// \li False - don't use text antyaliasing.
     bool useTextAntialiasing(int);

     /// \brief Get decision about using smooth tranformation for bitmap.
     ///
     /// \param appMode Mode of application.
     /// \return Decision about using smooth tranformation for bitmap.
     /// \li True - use smooth tranformation.
     /// \li False - don't use smooth tranformation.
     bool useSmoothPixmapTransform(int appMode);

     /// \brief Get decision about using autosize for cell.
     ///
     /// \return Decision about using autosize for cell.
     /// \li True - use autosize.
     /// \li False - don't use autosize.
     bool xttCellAutosize(void);
     
     /// \brief Function that reads the plugin names from the settings
     ///
     /// \return no values return.
     void readPlugins(void);
     
     /// \brief Function that returns the index of the plugin that has given name.
     ///
     /// \param __name - name of the plugin
     /// \param __ignoreindex - index that is ignored while searching
     /// \return index of the plugin or -1 if plugin with given name does not exist
     int pluginIndexOfname(QString __name, int __ignoreindex);
     
     /// \brief Function that updates the plugin configuration
     ///
     /// \param __name - name of the plugin
     /// \param __configuration - plugin configuration
     /// \return no values return.
     void pluginUpdateConfiguration(QString __name, int __configuration);
     
     /// \brief Function that removes plugin
     ///
     /// \brief __plindex - plugin index
     /// \return no values return.
     void removePlugin(int __plindex);
     
     /// \brief Function that updates the plugins parameters w.r.t. settings
     ///
     /// \return no values return.
     void updatePlugins(void);
     
     /// \brief Function that returns the plugins group name in the config file
     ///
     /// \return the plugins group name in the config file
     QString pluginsGroupName(void);
     
     /// \brief Function that returns the property string for given plugin index and property name
     ///
     /// \param __plindex - index of the plugin
     /// \param __property - name of the plugin property
     /// \return the property string for given plugin index and property name
     QString pluginPropertyString(int __plindex, QString __property);
     
     /// \brief Function that returns the key configuration for given plugin nad property
     ///
     /// \param __plindex - index of the plugin
     /// \param __property - name of the plugin property
     /// \return the key configuration for given plugin nad property
     QString pluginKeyPropertyString(int __plindex, QString __property);
     
     /// \brief Function that reads the plugin propery value
     ///
     /// \param __plindex - index of the plugin
     /// \param __property - the name of the plugin property
     /// \param __defval - the default value that is returned when the source value cannot be found
     /// \return the plugin's property value
     QString readPluginProperty(int __plindex, QString __property, QString __defval);
     
     /// \brief Function that returns the path of the ecmascripts directory
     ///
     /// \return the path of the ecmascripts directory as string
     QString ecmascriptPath(void);
     
     /// \brief Function that returns the path of the configuration directory
     ///
     /// \return the path of the configuration directory as string
     QString configurationPath(void);
     
     /// \brief Function that returns the path of the default project directory
     ///
     /// \return the path of the default project directory as string
     QString projectPath(void);
     
     /// \brief Function that sets the plugin propery value
     ///
     /// \param __plindex - index of the plugin
     /// \param __property - the name of the plugin property
     /// \param __value - the new value of the property
     /// \return no values return
     void setPluginProperty(int __plindex, QString __property, QString __value);
     
     /// \brief Function that returns the number of the registered plugins
     ///
     /// \return the number of the registered plugins
     int pluginsCount(void);
     
     /// \brief Function that sets the number of plugins
     ///
     /// \param __cnt - new number of plugins
     /// \return no values return
     void setPluginsCount(int __cnt);
     
     /// \brief Function that adds a filename to recent used files list
     ///
     /// \param fileName filename
     /// \return no values return
     void recentFile(QString fileName);
     
     /// \brief Function that returns the recent used files list
     ///
     /// \return the recent used files list
     QStringList recentFiles(void);
     
     /// \brief Clear recent files list.
     ///
     /// \return No return value.
     void clearRecentFiles(void);

public slots:

     /// \brief Function that changes the status of scene scrolling/zooming by wheel
     ///
     /// \return No return value.
     void changeXttScrollByWheelStatus(void);
     
     /// \brief Function that changes the status of hidding XTT tables
     ///
     /// \return No return value.
     void changeXttTableAutoHideStatus(void);
     
     /// \brief Function that changes the status of hidding initial tables
     ///
     /// \return No return value.
     void changeXttInitialTableHideStatus(void);
     
     /// \brief Function that changes the type of references to the XTT attributes
     ///
     /// \return No return value.
     void changeXttAttributeReferenceTypeStatus(void);

     /// \brief Call when replacing space character is changed.
     ///
     /// \param _newVal New value of string replacing space character.
     /// \return No return value.
     void spaceCharacterChange(QString _newVal);

     /// \brief Call when checkbox state is changed.
     ///
     /// \param _nv New state of checkbox.
     /// \return No return value.
     void checkBox9changed(bool _nv);
	
     /// \brief Call when attribute comparing status is changed.
     ///
	/// \param _nv New state of checkbox.
	/// \return No return value.
	void checkBox11changed(bool _nv);
	
	/// \brief Call when auto detection of table type is changed.
	///
	/// \param _nv New state of checkbox.
	/// \return No return value.
	void checkBox15changed(bool _nv);

	/// \brief Call when fuctor zoom is changed in ARD mode.
	///
	/// \param _nv New value of fuctor zoom.
	/// \return No return value.
	void changeArdZoomFactorSpinBox(double _nv);
	
	/// \brief Call when fuctor zoom is changed in ARD mode.
	///
	/// \param _nv New value of fuctor zoom.
	/// \return No return value.
	void changeArdZoomFactorSliderBar(int _nv);
	
	/// \brief Call when fuctor zoom is changed in XTT mode.
	///
	/// \param _nv New value of fuctor zoom.
	/// \return No return value.
	void changeXttZoomFactorSpinBox(double _nv);
	
	/// \brief Call when fuctor zoom is changed in XTT mode.
	///
	/// \param _nv New value of fuctor zoom.
	/// \return No return value.
	void changeXttZoomFactorSliderBar(int _nv);

	/// \brief Call when table font is about to changed.
	///
	/// \return No return value.
	void changeTableFont(void);
	
	/// \brief Call when table header font is about to changed.
	///
	/// \return No return value.
	void changeTableHeaderFont(void);
	
	/// \brief Call when table title font is about to changed.
	///
	/// \return No return value.
	void changeTableTitleFont(void);
	
	/// \brief Call when connection label font is about to changed.
	///
	/// \return No return value.
	void changeConnectionLabelFont(void);
	
	/// \brief Call when TPH font is about to changed.
	///
	/// \return No return value.
	void changeTPHFont(void);
	
	/// \brief Call when ARD diagram font is about to changed.
	///
	/// \return No return value.
	void changeARDdiagramFont(void);

	/// \brief Call when apply button is clicked.
	///
	/// \return No return value.
	void applyButtonClick(void);
	
	/// \brief Call when close button is clicked.
	///
	/// \return No return value.
	void closeButtonClick(void);

	/// \brief Call when current tab changed.
	///
	/// \return No return value.
	void currentTabChanged(int);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor1(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor2(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor3(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor4(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor5(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor6(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor7(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor8(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor9(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor10(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor11(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor12(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor13(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor14(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor15(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor16(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor17(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor18(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor19(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor20(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.u
	void onSelectColor21(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor22(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor23(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor24(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor25(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor26(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor27(void);
     
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor28(void);
	
	/// \brief Call when button of color selector is clicked.
	///
	/// \return No return value.
	void onSelectColor29(void);

        /// \brief Call when button of color selector is clicked.
        ///
        /// \return No return value.
        void onSelectColor44(void);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \return No return value.
	void onSlider1ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider2ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider3ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider4ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider5ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider6ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider7ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider8ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider9ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider10ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider11ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider12ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider13ChangePosition(int _nv);
	
	/// \brief Call when position of slider of picture compression is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider15ChangePosition(int _nv);
	
	/// \brief Call when position of slider of picture compression is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider16ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider17ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider18ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider19ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider20ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider21ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider22ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider23ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider24ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider25ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider26ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider27ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider28ChangePosition(int _nv);

	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider29ChangePosition(int _nv);
	
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider31ChangePosition(int _nv);
     
        /// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider32ChangePosition(int _nv);
     
	/// \brief Call when position of slider of transparency is chaged.
	///
	/// \param _nv New slider position.
	/// \return No return value.
	void onSlider33ChangePosition(int _nv);

     /// \brief Call when position of slider of transparency is chaged.
     ///
     /// \param _nv New slider position.
     /// \return No return value.
     void onSlider34ChangePosition(int _nv);

     /// \brief Call when status of tray icon is changed.
	///
	/// \param _nv New status.
	/// \return No return value.
	void onCheckBox29checked(bool _nv);
	
	/// \brief Call when status of autosize of cell is changed.
	///
	/// \param _nv New status.
	/// \return No return value.
	void onCheckBox35checked(bool _nv);
     
     /// \brief Call when add plugin button is pressed
     ///
     /// \return No return value.
     void onAddPlugin(void);
     
     /// \brief Call when edit plugin button is pressed
     ///
     /// \return No return value.
     void onEditPlugin(void);
     
     /// \brief Call when delete plugin button is pressed
     ///
     /// \return No return value.
     void onDeletePlugin(void);
     
     /// \brief Function that is called when PluginEditor is closed by OK button
     ///
     /// \return No value returns.
     void onClosePluginEditor(void);
     
     /// \brief Function that is called when directory selectoris clicked
     ///
     /// \return No value returns.
     void onSelectEcmascriptDirectory(void);
     
     /// \brief Function that is called when directory selectoris clicked
     ///
     /// \return No value returns.
     void onSelectDefaultProjectDirectory(void);
     
     /// \brief Function that is called when directory selectoris clicked
     ///
     /// \return No value returns.
     void onSelectConfigurationDirectory(void);
     
     /// \brief Function that is called on plugin selection changed
     ///
     /// \param __plindex - new plugin index
     /// \return No value returns.
     void onPluginSelectionChanged(int __plindex);
     
     /// \brief Function that is called on lock plugin button state changed
     ///
     /// \param __checked - new state of the button
     /// \return No value returns.
     void onPlLockChecked(bool __checked);
     
     /// \brief Function that is called on unlock plugin button state changed
     ///
     /// \param __checked - new state of the button
     /// \return No value returns.
     void onPlUnlockChecked(bool __checked);
};
// -----------------------------------------------------------------------------

extern Settings_ui* Settings;
// -----------------------------------------------------------------------------

#endif
