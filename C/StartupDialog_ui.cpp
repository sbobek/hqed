/**
* \file	    StartupDialog_ui.h
* \author	Opioła, Pasek, Zieliński
* \date 	27.07.2012
* \version  1.0
* \brief	This file defines class that represents startup dialog
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include "StartupDialog_ui.h"
#include "MainWin_ui.h"
#include "Settings_ui.h"
#include "../namespaces/ns_hqed.h"

StartupDialog_ui *StartupDialog;

// #brief Constructor of StartupDialog_ui class.
//
// #param parent Pointer to parent object.
StartupDialog_ui::StartupDialog_ui(QWidget*)
{
    setupUi(this);
    hqed::centerWindow(this);
    setWindowFlags( (windowFlags() | Qt::CustomizeWindowHint) & ~Qt::WindowMaximizeButtonHint); //for Mac not to show maximize
    setFixedSize(width(), height()); //for linux not to show maximize
    setWindowIcon(QIcon(":/all_images/images/mainico.png"));
    
    label_2->setAlignment(Qt::AlignHCenter);
    label_3->setAlignment(Qt::AlignHCenter);
    
    checkBox->setChecked(Settings->useStartupDialog());
    
    connect(pushButton, SIGNAL(clicked()), MainWin, SLOT(NewXttClick()));
    connect(pushButton, SIGNAL(clicked()), this, SLOT(closeDialog()));
    connect(pushButton_2, SIGNAL(clicked()), MainWin, SLOT(showWizardDialog()));
    connect(pushButton_2, SIGNAL(clicked()), this, SLOT(closeDialog()));
    connect(pushButton_3, SIGNAL(clicked()), MainWin, SLOT(openXTTFileDialog()));
    connect(pushButton_3, SIGNAL(clicked()), this, SLOT(closeDialog()));
    connect(pushButton_4, SIGNAL(clicked()), this, SLOT(closeDialog()));
    
    formLastUsedList();
}

// #brief Destructor of StartupDialog_ui class.
StartupDialog_ui::~StartupDialog_ui()
{
}

// #brief Opens clicked recent file.
//
// #param item Pointer to clicked list item
// #return No return value.
void StartupDialog_ui::recentFileClicked(QListWidgetItem *item)
{
    MainWin->openFile(item->toolTip());
}

// #brief Close dialog.
//
// #return No return value.
void StartupDialog_ui::closeDialog(void)
{
    Settings->setStartupDialogShowing(checkBox->isChecked());
    this->setVisible(false);
}

// #brief Forms a list of recently used files.
//
// #return No return value.
void StartupDialog_ui::formLastUsedList(void)
{
    QStringList files = Settings->recentFiles();
    int numRecentFiles = qMin(files.size(), 10);
    QListWidgetItem* item;
    
    for (int i = 0; i < numRecentFiles; i++)
    {
        item = new QListWidgetItem(QFileInfo(files[i]).baseName());
        item->setToolTip(files[i]);
        listWidget->addItem(item);
    }
     
    listWidget->setMouseTracking(true);
    
    QString systemHighlightBrushName = QApplication::palette().brush(QPalette::Highlight).color().name();
    QString systemTextHighlightBrushName = QApplication::palette().brush(QPalette::HighlightedText ).color().name(); 
    QString style = QString("QListWidget::item:hover {background-color:%1; color:%2}").arg(systemHighlightBrushName, systemTextHighlightBrushName);
    style.append("QListWidget::item {padding: 6px 0px;}");
    this->setStyleSheet(style);
 
    connect(listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(recentFileClicked(QListWidgetItem*)));
    connect(listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(closeDialog()));
}
