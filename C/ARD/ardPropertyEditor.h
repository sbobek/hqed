/**
* \file     ardPropertyEditor.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardPropertyEditor definition of a dialog
* used for editing the list of Attributes of a given property.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDPROPERTYEDITOR_H
#define ARDPROPERTYEDITOR_H
// -----------------------------------------------------------------------------

#include <QList>
#include <QWidget>
#include <QGridLayout>
#include <QMessageBox>
// -----------------------------------------------------------------------------

class gARDnode;
class ARD_Property;
class ARD_Attribute;
class ardAttributeChooser;

namespace Ui {
     class ardPropertyEditor;
}
// -----------------------------------------------------------------------------

class ardPropertyEditor : public QWidget
{
     Q_OBJECT

private:
     QGridLayout* mainGL;                    ///< Layout of the widget
     Ui::ardPropertyEditor *ui;               ///< Pointer to the UI of the Widget.
     gARDnode* itemEdited;                    ///< Pointer to the item being edited
     QList<ARD_Attribute*> _allAtts;          ///< Lista attrybutów tego obiektu
     QList<ARD_Attribute*> _thisAtts;         ///< Lista wszystkich atrybutów
     QList<ARD_Attribute*>* _oldThisAtts;     ///< List of old attributes(used to updateParents)

     ardAttributeChooser* _attchooser;        ///< Pointer to the attribute chooser

     /// \brief Functions that layouts the items on the widget
     ///
     /// \return no values return
     void layoutForm(void);

public:

     ardPropertyEditor(gARDnode* _itemEdited, QWidget *parent = 0);
     ~ardPropertyEditor();

protected:

     /**
      * \brief Manages changes to the widget.
      * The standard implementation manages a retranslate of the UI.
      */
     void changeEvent(QEvent *e);

     /** \brief Adds attribute to the property which is being edited
      *
      * \return No return value
      */
     void addAttribute(const QString &);

     /** \brief Updates the tree of the given property.
      *
      * \return No return value
      */
     bool updateParents();

     /**
      * \brief Defines what happens when a key is pressed.
      * Implemented only for Qt::Key_Escape and Qt::Key_Enter. The first
      * closes the Widget, while the second triggers buttonOkClicked().
      * \see buttonOkClicked()
      */
     void keyPressEvent(QKeyEvent *event);

public slots:

     /// \brief Slot used to handle when the pushButtonOk is clicked
     void buttonOkClicked(void);

signals:


     /** \brief Signal emited when ok button is pressed
      *
      * \return No return value.
      */
     void okClicked(void);
};
// -----------------------------------------------------------------------------

#endif // ARDPROPERTYEDITOR_H
