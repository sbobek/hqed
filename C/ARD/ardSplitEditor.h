/**
* \file     ardSplitEditor.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditor definition of a dialog
* used to prepare and perform a Split step.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDSPLITEDITOR_H
#define ARDSPLITEDITOR_H
// -----------------------------------------------------------------------------

#include <QWidget>
#include "../../V/ARD/ardSplitEditorNode.h"
// -----------------------------------------------------------------------------

class gARDnode;
class ARD_Property;
class ardSplitEditorScene;
class ardSplitEditorNode;

namespace Ui {
class ardSplitEditor;
}
// -----------------------------------------------------------------------------

class ardSplitEditor : public QWidget
{
     Q_OBJECT

public:

     /**
       * \brief Constructor for the ardSplitEditor
       * \param _itemEdited - the item that is being split
       * \param parent - parent of ardSplitEditor object
       */
     explicit ardSplitEditor(gARDnode* _itemEdited, QWidget *parent = 0);

     /**
       * \brief Standard destructor
       *
       */
     ~ardSplitEditor();

     /**
       * \brief This method gets the dependencies of the property beign splitted, and add them to the scene.
       **/
     void loadDependencyPropeprties();

     /**
       * \brief This method puts the _depList nodes unto the scene.
       * \note This is for use only when _depLists, are purely either dependent or independent from
       * the property beign splitted.
       * \param _depList - const reference to the list of nodes, that are to be put on the scene.
       * \param type - defines the type of elements that are on the list. Type ardSplitEditorNode::FutureChild
       * does nothing.
       **/
     void placeDependecyProperties(const QList<ardSplitEditorNode *> &_depList,
                                  ardSplitEditorNode::PropertyRelation type);

     /**
       * \brief Checks whether there are satisfactory dependencies to do a Split
       * \note Includes a message box to communicate with the user.
       * \returns true if the dependency check is passed, otherwise false.
       */
     bool dependencyCheck();

     /**
       * \brief Checks whether the graph on scene is connected.
       * The connectivity is analysed asuming that the graph is non-directed.
       * \note Includes a message box to communicate with the user.
       * \return true if the graph is connected, otherwise false.
       */
     bool connectivityCheck();

public slots:
     /**
       * \brief This slot when node is selected loads the list of it's
       *  attributes to the ui->ard_attributeChooser.
       * \param node - the node that has been selected.
       */
     void selectNode(ardSplitEditorNode* node);

     /**
       * \brief Used when anything else than a single node is selected.
       *  Clears the ard_attributeChooser list.
       */
     void unselectNode();

     /**
       * \brief Slot manages what happens when the selection of the scene is changed
       */
     void onSelectionChange();

     /**
       * \brief The heart of the ardSplitEditor. The slot called when the Ok button is clicked.
       * \par This slot first checks if all of the requirements for a Split are met. That is:
       * 1. The properties must be not null.
       * 2. All of the attributes of the Property being Splitted must be among the children.
       * 3. For each Independent Properties of the Property being Splitted there must be at least one
       * Dependent Property among the children of that Property.
       * \par After all of the tests are passed, everything is added to the ARD Diagram.
       */
     void onOkClicked();

protected slots:
     /**
       * \brief Ensures that the changes of the editedNode will be processed by the scene.
       */
     void onEditedPropertyChange();
     /** \brief Manages what happens when a Property on the scene is to be deleted.
       * Reloads the Attributes from the Property that is to be deleted to the ard_AttributeChooser
       * Parent Property List.
       */
     void reloadAttsFromProperty(ARD_Property* _property);

protected:
     /**
       * \brief Defines what happens when a key is pressed.
       * Implemented only for Qt::Key_Escape and Qt::Key_Enter. The first
       * closes the Widget, while the second triggers onOkClicked().
       * \see onOkClicked()
       */
     void keyPressEvent(QKeyEvent *event);

private:
     Ui::ardSplitEditor *ui;                 ///< Pointer to the UI of the widget.
     gARDnode* itemEdited;                   ///< Pointer to the edited item.
     ardSplitEditorScene* scene;             ///< Pointer to the scene underlying the ui->graphicsView widget.
     ardSplitEditorNode* editedNode;         ///< Pointer to the edited Node.
     QList<ARD_Property*> dependentList;     ///< List of Dependent Properties.
     QList<ARD_Property*> independentList;   ///< List of Independent Properties.

     // ------------------------------------GRAPH CONNECTIVITY CHECK MEMBERS-----------------------//

     QMap<ardSplitEditorNode*,ardSplitEditorNode*> nodeMap;/**< Temporarily represents the graph of connections when
                                                             * performing the connectivity check for the graph on scene.
                                                             * \see connectivityCheck()
                                                             */
     QList<ardSplitEditorNode*> nodeList;/**< Temporarily holds a list of Nodes that where visited while moving across
                                           * the graph on scene
                                           * \see connectivityCheck()
                                           */

     /**
       * \brief Performs a move to another node in the graph connectivity check.
       */
     void nextStep(ardSplitEditorNode* atNode);
};
// -----------------------------------------------------------------------------

#endif // ARDSPLITEDITOR_H
