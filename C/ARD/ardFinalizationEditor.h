/**
* \file     ardFinalizationEditor.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardFinalizationEditor definition of a widget
* dialog used for preparing and performing a Finalization step.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDFINALIZATIONEDITOR_H
#define ARDFINALIZATIONEDITOR_H
// -----------------------------------------------------------------------------

#include <QWidget>
// -----------------------------------------------------------------------------

class gARDnode;

namespace Ui {
class ardFinalizationEditor;
}
// -----------------------------------------------------------------------------

class ardFinalizationEditor : public QWidget
{
     Q_OBJECT

public:
     /** \brief This constructor accepts the pointer to the node that's being edited
       *
       * \param _itemEdited - pointer to the item being edited
       * \param parent - pointer to the QWidget parent.
       */
     explicit ardFinalizationEditor(gARDnode* _itemEdited,QWidget *parent = 0);

     /**
       * \brief Standard destructor.
       */
     ~ardFinalizationEditor();

public slots:

     /** \brief This slot is responsible for doing the actual Finalization
       * \paragraph This Slot does the actual finalization for the widget. It is connected to the Ok button on the widget.
       * It checks if the chosen attribute list is empty, if not it doesn't procede. Next it creates the apriopriate level,
       * property, dependencies, so that the finalization could take place.
       **/
     void onOkClicked();

protected:

     /**
       * \brief Defines what happens when a key is pressed.
       * Implemented only for Qt::Key_Escape and Qt::Key_Enter. The first
       * closes the Widget, while the second triggers onOkClicked().
       * \see onOkClicked()
       */
     void keyPressEvent(QKeyEvent *event);

private:

     Ui::ardFinalizationEditor *ui;     ///< This is the pointer to the UI of the widget.
     gARDnode* itemEdited;              ///< This field is the pointer to the edited item.
};
// -----------------------------------------------------------------------------

#endif // ARDFINALIZATIONEDITOR_H
