/**
* \file	ardAttributeChooser.cpp
* \class  ardAttributeChooser
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a widget used for choosing a group of Attributes
* from a larger group of Attributes.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include <QKeyEvent>

#include "../../namespaces/ns_hqed.h"
#include "ardAttributeChooser.h"
#include "ui_ardAttributeChooser.h"
// -----------------------------------------------------------------------------

ardAttributeChooser::ardAttributeChooser(QWidget *parent) :
          QWidget(parent),
          ui(new Ui::ardAttributeChooser)
{
     ui->setupUi(this);

     ui->listWidgetLeftAttrbs->loadAttributes(&_leftAttList);
     ui->listWidgetRightAttrbs->loadAttributes(&_rightAttList);

     connect(ui->listWidgetLeftAttrbs, SIGNAL(elementsMoved(QList<int>*)), this, SLOT(onAttsRemoveFromProperty(QList<int>*)));
     connect(ui->listWidgetRightAttrbs, SIGNAL(elementsMoved(QList<int>*)), this, SLOT(onAttsAddToProperty(QList<int>*)));

     connect(ui->toolButtonAdd,SIGNAL(clicked()),this,SLOT(buttonAddClicked()));
     connect(ui->toolButtonDelete,SIGNAL(clicked()),this,SLOT(buttonDeleteClicked()));

     // Form layout
     layoutForm();
     hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

ardAttributeChooser::~ardAttributeChooser()
{
     delete mainGL;
     delete ui;
}
// -----------------------------------------------------------------------------

// #brief Functions that layouts the items on the widget
//
// #return no values return
void ardAttributeChooser::layoutForm(void)
{
     mainGL = new QGridLayout;

     mainGL->addWidget(ui->leftLabel,            0,0);
     mainGL->addWidget(ui->listWidgetLeftAttrbs, 1,0,5,1);
     mainGL->addWidget(ui->toolButtonAdd,        2,1);
     mainGL->addWidget(ui->toolButtonDelete,     4,1);
     mainGL->addWidget(ui->rightLabel,           0,2);
     mainGL->addWidget(ui->listWidgetRightAttrbs,1,2,5,1);

     mainGL->setRowMinimumHeight(3, 10);
     mainGL->setRowStretch(1, 10);
     mainGL->setRowStretch(3, 1);
     mainGL->setRowStretch(5, 10);
     mainGL->setColumnStretch(0, 1);
     mainGL->setColumnStretch(2, 1);

     hqed::setupLayout(mainGL);
     setLayout(mainGL);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::loadLeftAttributeList(const QList<ARD_Attribute *> &__attlist)
{
     _leftAttList = __attlist;
     ui->listWidgetLeftAttrbs->showList();
     //ui->listWidgetLeftAttrbs->loadAttributes(&_leftAttList);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::loadRightAttributeList(const QList<ARD_Attribute *> &__attlist)
{
     _rightAttList = __attlist;
     ui->listWidgetRightAttrbs->showList();
     //ui->listWidgetRightAttrbs->loadAttributes(&_rightAttList);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::setLeftLabel(const QString &label)
{
     ui->leftLabel->setText(label);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::setRightLabel(const QString &label)
{
     ui->rightLabel->setText(label);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::setLeftListFocus(Qt::FocusReason reason)
{
     ui->listWidgetLeftAttrbs->setFocus(reason);
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::onAttsAddToProperty(QList<int>* __items)
{
     for(int i=__items->size()-1;i>=0;--i)
          _rightAttList.append(_leftAttList.takeAt(__items->at(i)));

     ui->listWidgetLeftAttrbs->showList();
     ui->listWidgetRightAttrbs->showList();
     emit rightListChanged();
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::onAttsRemoveFromProperty(QList<int>* __items)
{
     for(int i=__items->size()-1;i>=0;--i)
          _leftAttList.append(_rightAttList.takeAt(__items->at(i)));

     ui->listWidgetLeftAttrbs->showList();
     ui->listWidgetRightAttrbs->showList();
     emit rightListChanged();
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::buttonAddClicked()
{
     QList<int>* list = ui->listWidgetLeftAttrbs->chosen();
     onAttsAddToProperty(list);
     delete list;
}
// -----------------------------------------------------------------------------

void ardAttributeChooser::buttonDeleteClicked()
{
     QList<int>* list = ui->listWidgetRightAttrbs->chosen();
     onAttsRemoveFromProperty(list);
     delete list;
}
// -----------------------------------------------------------------------------

