/**
* \file	ardAttributeManager.cpp
* \class  ardAttributeManager
* \author	Krzysztof Kaczor
* \date 	05.06.2011
* \version	1.0
* \brief	Class definition of a widget used for managing set of ARD Attributes
* from a larger group of Attributes.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include <QKeyEvent>
#include <QMessageBox>

#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/ARD/ARD_AttributeList.h"

#include "../../namespaces/ns_hqed.h"
#include "ui_ardAttributeManager.h"
#include "ardAttributeManager.h"
// -----------------------------------------------------------------------------

ardAttributeManager_ui* ardAttributeManager;
// -----------------------------------------------------------------------------

ardAttributeManager_ui::ardAttributeManager_ui(QWidget *parent) :
          QWidget(parent),
          ui(new Ui::ardAttributeManager)
{
     ui->setupUi(this);

     // Form layout
     layoutForm();
     setWindowModality(Qt::ApplicationModal);
     setWindowFlags(Qt::WindowCloseButtonHint | Qt::WindowMaximizeButtonHint);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);

     populateAttributesList();

     connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(closeClicked()));
     connect(ui->pushButton_3, SIGNAL(clicked()), this, SLOT(refreshClicked()));
     connect(ui->toolButton, SIGNAL(clicked()), this, SLOT(renameClicked()));
     connect(ui->toolButton_2, SIGNAL(clicked()), this, SLOT(addnewClicked()));
     connect(ui->toolButton_3, SIGNAL(clicked()), this, SLOT(removeClicked()));
     connect(ui->listWidget, SIGNAL(currentRowChanged(int)), this, SLOT(onAttributeSelectionChange(int)));
}
// -----------------------------------------------------------------------------

ardAttributeManager_ui::~ardAttributeManager_ui()
{
     delete mainGL;
     delete ui;
}
// -----------------------------------------------------------------------------

// #brief Functions that layouts the items on the widget
//
// #return no values return
void ardAttributeManager_ui::layoutForm(void)
{
     mainGL = new QGridLayout;

     mainGL->addWidget(ui->label,       0,0,1,4);
     mainGL->addWidget(ui->listWidget,  1,0,3,4);
     mainGL->addWidget(ui->pushButton_3,1,4);
     mainGL->addWidget(ui->lineEdit,    4,0);
     mainGL->addWidget(ui->toolButton,  4,1);
     mainGL->addWidget(ui->toolButton_2,4,2);
     mainGL->addWidget(ui->toolButton_3,4,3);
     mainGL->addWidget(ui->pushButton,  4,4);

     mainGL->setRowStretch(2, 1);
     mainGL->setColumnStretch(0, 1);

     hqed::setupLayout(mainGL);
     setLayout(mainGL);
}
// -----------------------------------------------------------------------------

// #brief Called on key press
//
// #param __event - pointer to the object that handles the event parameters
// #return no values return
void ardAttributeManager_ui::keyPressEvent(QKeyEvent* __event)
{
     switch(__event->key())
     {
          case Qt::Key_Escape:
               closeClicked();
               break;
          case Qt::Key_F5:
               refreshClicked();
               break;
          case Qt::Key_Delete:
               removeClicked();
               break;
     }
}
// -----------------------------------------------------------------------------

// #brief Populates the list of attributes
//
// #return no values return
void ardAttributeManager_ui::populateAttributesList(void)
{
     ui->listWidget->clear();
     ardAttributeList->sort();

     for(int i=0;i<ardAttributeList->size();++i)
     {
          QListWidgetItem* item = new QListWidgetItem(ardAttributeList->at(i)->name());
          item->setToolTip(ardAttributeList->at(i)->description());
          ui->listWidget->addItem(item);
     }
}
// -----------------------------------------------------------------------------

// #brief Called when close button is clicked
//
// #return no values return
void ardAttributeManager_ui::closeClicked(void)
{
     close();
}
// -----------------------------------------------------------------------------

// #brief Called when refresh button is clicked
//
// #return no values return
void ardAttributeManager_ui::refreshClicked(void)
{
     populateAttributesList();
}
// -----------------------------------------------------------------------------

// #brief Called when add button is clicked
//
// #return no values return
void ardAttributeManager_ui::addnewClicked(void)
{
     QString newname = ui->lineEdit->text();
     int result = hqed::checkAttributeName(newname);
     if(result > 0)
     {
          if(result == 1)
               QMessageBox::warning (this, "Adding ARD attribute", "The name of the new ARD attribute is not specified.");
          if(result == 2)
               QMessageBox::warning (this, "Adding ARD attribute", "The name of thr new ARD attribute starts with incorrect character. The list of permitted character is:\n" + hqed::attributeNameNotAllowedStartingCharactersList());
          return;
     }

     int addresult = addNewArdAttribute(newname);
     if(addresult != 0)
     {
          if(addresult == 1)
               QMessageBox::warning (this, "Adding ARD attribute", "The attribute \'" + newname + "\' already exists. Please give another name.");
          return;
     }

     ui->lineEdit->setText("");
     refreshClicked();
}
// -----------------------------------------------------------------------------

// #brief Called when rename button is clicked
//
// #return no values return
void ardAttributeManager_ui::renameClicked(void)
{
     if(ui->listWidget->currentRow() == -1)
          return;

     QString oname = ui->listWidget->currentItem()->text();
     QString nname = ui->lineEdit->text();

     int result = hqed::checkAttributeName(nname);
     if(result > 0)
     {
          if(result == 1)
               QMessageBox::warning (this, "Adding ARD attribute", "The new name of the new ARD attribute is not specified.");
          if(result == 2)
               QMessageBox::warning (this, "Adding ARD attribute", "The new name of thr new ARD attribute starts with incorrect character. The list of permitted character is:\n" + hqed::attributeNameNotAllowedStartingCharactersList());
          return;
     }


     int renresult = renameArdAttribute(oname, nname);
     if(renresult != 0)
     {
          if(renresult == 1)
               QMessageBox::warning (this, "Adding ARD attribute", "The attribute \'" + nname + "\' already exists. Please give another name.");
          if(renresult == 2)
               QMessageBox::warning (this, "Adding ARD attribute", "The attribute \'" + oname + "\' does not exists.");
         return;
     }

     ui->lineEdit->setText("");
     refreshClicked();
}
// -----------------------------------------------------------------------------

// #brief Called OK close button is clicked
//
// #return no values return
void ardAttributeManager_ui::removeClicked(void)
{
     if(ui->listWidget->currentRow() == -1)
          return;

     QString attname = ui->listWidget->currentItem()->text();

     if(QMessageBox::warning(this, "ARD attribute remove", "Do you really want to remove the attribute \'" + attname + "\'?", QMessageBox::Yes | QMessageBox::No) == QMessageBox::No)
          return;

     int remresult = removeArdAttribute(attname);
     if(remresult != 0)
     {
          if(remresult == 1)
               QMessageBox::warning (this, "Adding ARD attribute", "The attribute \'" + attname + "\'' does not exists. Please select another name.");
     }
     refreshClicked();
}
// -----------------------------------------------------------------------------

// #brief Called when the selection of the attribute is changed
//
// #param __newindex - index of the selected attribute
// #return no values return
void ardAttributeManager_ui::onAttributeSelectionChange(int __newindex)
{
     if(__newindex == -1)
     {
          ui->lineEdit->setText("");
          return;
     }
     ui->lineEdit->setText(ui->listWidget->currentItem()->text());
}
// -----------------------------------------------------------------------------

// #brief Adds a new attribute to the ARDattlist
//
// #param __name - name of the new attribute
// #return exit code
// #li 0 - on success
// #li 1 - when attribute with given name already exists
int ardAttributeManager_ui::addNewArdAttribute(QString __name)
{
     int ion = ardAttributeList->indexOfName(__name);
     if(ion > -1)
          return 1;

     ARD_Attribute* newardatt = ardAttributeList->add();
     newardatt->setName(__name);

     return 0;

}
// -----------------------------------------------------------------------------

// #brief Rename an ARD attributet
//
// #param __oldname - the old name of the attribute
// #param __newname - the new name of the attribute
// #return exit code
// #li 0 - on success
// #li 1 - when attribute with given newname already exists
// #li 2 - when attribute with given old do not exists
int ardAttributeManager_ui::renameArdAttribute(QString __oldname, QString __newname)
{
     int ioon = ardAttributeList->indexOfName(__oldname);
     int ionn = ardAttributeList->indexOfName(__newname);
     if(ioon == -1)
          return 2;
     if((ionn > -1) && (ionn != ioon))
          return 1;

     ARD_Attribute* ardatt = ardAttributeList->at(ioon);
     ardatt->setName(__newname);
     ardatt->setXTTequivalent(NULL);

     return 0;
}
// -----------------------------------------------------------------------------

// #brief Remove an ARD attributet
//
// #param __name - name of the removed attribute
// #return exit code
// #li 0 - on success
// #li 1 - when attribute with given name does not exists
int ardAttributeManager_ui::removeArdAttribute(QString __name)
{
     int ion = ardAttributeList->indexOfName(__name);
     if(ion == -1)
          return 1;

     ardAttributeList->remove(ion);

     return 0;
}
// -----------------------------------------------------------------------------

