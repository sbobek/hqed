/**
* \file     ardSplitEditorScene.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditorScene definition of a scene
* customized for use with ardSplitEditorView.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDSPLITEDITORSCENE_H
#define ARDSPLITEDITORSCENE_H
// -----------------------------------------------------------------------------

#include <QGraphicsScene>
// -----------------------------------------------------------------------------

class ardSplitEditorNode;
class ARD_Property;
class ARD_Dependency;
// -----------------------------------------------------------------------------

class ardSplitEditorScene : public QGraphicsScene
{
     Q_OBJECT
public:
     /** \brief Standard same as QGraphicsScene::QGraphicsScene(). */
     explicit ardSplitEditorScene(QObject *parent = 0);

     /** \brief Destructor.
       * Deletes the ARD_Properties created for the Split Editor
       * (they will not be added to the ARD diagram).
       */
     ~ardSplitEditorScene();

     /** \brief Performs a zoom with a given scale.Sporzada zoom z zadanym powikeszeniem
       * \param scaleFactor - the factor for which the zoom should be performed.
       */
     void scaleView(qreal scaleFactor);

     /**
       * \brief Deletes the Dependencies on scene, which contain node.
       * \param node - pointer to the ardSplitEditorNode which all the dependencies will be deleted.
       */
     void deleteDependencies(ardSplitEditorNode* node);

     /**
       * \brief Checks whether a Dependency exists.
       * \param _start - pointer to the Independent of the Dependency.
       * \param _end - pointer to the Dependent of the Dependency.
       * If _start or _end are NULL the method does nothing.
       * \returns true - if dependency exists.
       * \returns false - otherwise.
       */
     bool existDependency (ardSplitEditorNode* _start, ardSplitEditorNode* _end);

signals:

     /**
       * \brief Signal sent before a Node is deleted.
       * This signal requests for adding its Attributes to the Attributes
       * of the Parent Property which have not been distributed.
       */
     void reloadAttsFromProperty(ARD_Property*);
public slots:

     /**
       * \brief Performs a repaint of the Dependencies connected with node.
       * \param node - pointer to the ardSplitEditorNode whose dependencies will be repainted.
       */
     void repaintDependencies(ardSplitEditorNode* node);

     /**
       * \brief Adds a node to the scene.
       * \param node - pointer to the ardSplitEditorNode that will be added to the scene.
       * This method adds the node to the scene and does the performs the apriopriate connections.
       */
     void addNode(ardSplitEditorNode* node);

     /**
       * \brief Adds a node to the specified point here.
       * \param here - the place where a new node is to be added.
       * This function calls addNode() once it creates a ARD_Property, ardSplitEditorNode and
       * sets the Nodes position to here.
       */
     void addNewNodeThere(const QPointF&here);

     /**
       * \brief Performs a delete of the Selected Items.
       * \note This function doesn't allow to delete Nodes that represent Properties on the ARD diagram.
       */
     void deleteSelected();

     /** \brief Performs a zoom out.
       * The scale factor used in the zoom is taken from the ARD Diagram settings.
       */
     void zout(void);

     /** \brief Performs a zoom in.
       * The scale factor used in the zoom is taken from the ARD Diagram settings.
       */
     void zin(void);
};
// -----------------------------------------------------------------------------

#endif // ARDSPLITEDITORSCENE_H
