/**
* \file	        ardGraphicsView.h
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	12.10.2007
* \version	1.0
* \brief	This file contains derived class definition that is the screen
* on which scene is drawn
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/


#ifndef ARDGRAPHICSVIEWH
#define ARDGRAPHICSVIEWH
// -----------------------------------------------------------------------------

#include <QGraphicsView>
#include "../../widgets/hBaseScene.h"
#include "../../widgets/GSceneView.h"
// -----------------------------------------------------------------------------

class QGraphicsView;
// -----------------------------------------------------------------------------

class ardGraphicsView : public GSceneView
{
    Q_OBJECT
     
public:

     /// \brief Constructor ardGraphicsView class
     ardGraphicsView(QWidget* oParent = 0);
     
     /// \brief Constructs a MyGraphicsView and sets the visualized scene.
     ///
     /// \param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
     /// \param __parent - Pointer to parent object, is passed to QWidget's constructor.
     ardGraphicsView(hBaseScene* __scene, QWidget* __parent = 0);
	
     /// \brief This method is invoked when the file is drag into the window
     void dragEnterEvent(QDragEnterEvent*);
	
     /// \brief This method is invoked when the file is drop into the window
     void dropEvent(QDropEvent*);
	
     /// \brief This method is invoked when moving mouse while drag element
     void dragMoveEvent(QDragMoveEvent*);
	
     /// \brief This methos is invoked on mouse scroll event
     void wheelEvent(QWheelEvent*);
};
// -----------------------------------------------------------------------------
#endif
