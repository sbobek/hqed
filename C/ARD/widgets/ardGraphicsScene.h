/**
* \file	        ardGraphicsScene.h
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	12.10.2007
* \version	1.0
* \brief	This file contains derived class definition that is the canvas
* on which the scene is drawn
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/

#ifndef ARDGRAPHICSSCENEH
#define ARDGRAPHICSSCENEH
// -----------------------------------------------------------------------------

#include "C/widgets/hBaseScene.h"
// -----------------------------------------------------------------------------

class QGraphicsView;
class QGraphicsItem;
// -----------------------------------------------------------------------------

class ardGraphicsScene : public hBaseScene
{
    Q_OBJECT
    
private:

     int fSelectedPropertyID; ///< Stores the currently selected property
     int fSelectedDependenciesID; ///< Stores the currently highlighted dependency
	
protected:

     /// \brief This method is called when cursor is moving over the scene
     void mouseMoveEvent(QGraphicsSceneMouseEvent*);

     /// \brief Method invoked when key was pressed on scene
     void mousePressEvent(QGraphicsSceneMouseEvent*);

     /// \brief The method invoked on RMB event
     ///
     /// \param __contextMenuEvent - event parameters
     void contextMenuEvent (QGraphicsSceneContextMenuEvent* __contextMenuEvent);
     
public:

     /// \brief Constructor ardGraphicsScene class
     ardGraphicsScene(QObject* oParent = 0);

     /// \brief Destructor
     ~ardGraphicsScene(void);

     /// \brief Sets currently selected property
     ///
     /// \return no values return
     void setSelectedPropertyID(int _id){ fSelectedPropertyID = _id; }

     /// \brief Returns ID value of the currently selected property
     ///
     /// \return ID of currently selected property
     int SelectedPropertyID(void){ return fSelectedPropertyID; }

     /// \brief Sets currently selected dependency
     ///
     /// \return no values return
     void setSelectedDependenciesID(int _id){ fSelectedDependenciesID = _id; }

     /// \brief Returns ID value of the currently selected dependency
     ///
     /// \return ID of currently selected dependency
     int SelectedDependenciesID(void){ return fSelectedDependenciesID; }
};
// -----------------------------------------------------------------------------
#endif
