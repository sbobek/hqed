/**
* \file	ardSplitEditorScene.cpp
* \class  ardSplitEditorScene
* \author	Piotr Held
* \date 	16.03.2011
* \version	1.0
* \brief  Class definition of a scene customized for use with ardSplitEditorView.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//----------------------------------------------------------------------------------

#include "../../../C/Settings_ui.h"
#include "../../../C/MainWin_ui.h"

#include "../../../M/ARD/ARD_Property.h"
#include "../../../M/ARD/ARD_Dependency.h"
#include "../../../M/ARD/ARD_DependencyList.h"

#include "../../../V/ARD/ardSplitEditorWire.h"
#include "../../../V/ARD/ardSplitEditorNode.h"
#include "../../../V/ARD/ardSplitEditorLoop.h"
#include "ardSplitEditorScene.h"
//----------------------------------------------------------------------------------

ardSplitEditorScene::ardSplitEditorScene(QObject *parent) :
          QGraphicsScene(parent)
{
     setBackgroundBrush(QBrush(Settings->ardSceneColor()));
}
//----------------------------------------------------------------------------------

ardSplitEditorScene::~ardSplitEditorScene()
{
     QList<QGraphicsItem*> onScene = items();
     QGraphicsItem* temp;
     ardSplitEditorNode* tempNode;
     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorNode::Type)
          {
               tempNode = dynamic_cast<ardSplitEditorNode*>(temp);
               if (tempNode->getRelation() == ardSplitEditorNode::FutureChild)
               {
                    tempNode->update();
                    removeItem(temp);
                    delete tempNode->parentProperty();
                    delete tempNode;
               }
          }
     }
}
//----------------------------------------------------------------------------------

void ardSplitEditorScene::addNode(ardSplitEditorNode *node)
{
     connect(node,SIGNAL(repaintDependencies(ardSplitEditorNode*)),this,SLOT(repaintDependencies(ardSplitEditorNode*)));
     addItem(node);
}
//----------------------------------------------------------------------------------

void ardSplitEditorScene::addNewNodeThere(const QPointF &here)
{
     ardSplitEditorNode* node = new ardSplitEditorNode;
     addNode(node);
     node->setPos(here);
}

//----------------------------------------------------------------------------------

void ardSplitEditorScene::repaintDependencies(ardSplitEditorNode *node)
{
     QList<QGraphicsItem*> onScene = items();
     QGraphicsItem* temp;
     ardSplitEditorWire* tempWire;
     ardSplitEditorLoop* tempLoop;
     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = dynamic_cast<ardSplitEditorWire*>(temp);
               if (tempWire->startItem() == node || tempWire->endItem() == node)
               {
                    tempWire->update();
                    invalidate(tempWire->boundingRect());

               }
          }
          if (temp->type() == ardSplitEditorLoop::Type)
          {
               tempLoop = qgraphicsitem_cast<ardSplitEditorLoop*>(temp);
               if (tempLoop->node() == node )
               {
                    tempLoop->update();
                    invalidate(tempLoop->boundingRect());
               }
          }
     }
     update();
}
//----------------------------------------------------------------------------------

void ardSplitEditorScene::deleteDependencies(ardSplitEditorNode *node)
{
     QList<QGraphicsItem*> onScene = items();
     QGraphicsItem* temp;
     ardSplitEditorWire* tempWire;
     ardSplitEditorLoop* tempLoop;

     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = qgraphicsitem_cast<ardSplitEditorWire*>(temp);
               if (tempWire->startItem() == node || tempWire->endItem() == node)
               {
                    removeItem(temp);
                    delete tempWire;
               }
          }
          else if (temp->type() == ardSplitEditorLoop::Type)
          {
               tempLoop = qgraphicsitem_cast<ardSplitEditorLoop*>(temp);
               if (tempLoop->node() == node )
               {
                    removeItem(temp);
                    delete tempLoop;
               }
          }
     }
}
//----------------------------------------------------------------------------------

void ardSplitEditorScene::deleteSelected()
{
     QList<QGraphicsItem*> selected = selectedItems();
     QGraphicsItem* temp;
     ardSplitEditorWire* tempWire;
     ardSplitEditorNode* tempNode;
     ardSplitEditorLoop* tempLoop;

     while( selected.isEmpty() == false )
     {
          temp = selected.first();
          if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = qgraphicsitem_cast<ardSplitEditorWire*>(temp);
               removeItem(temp);
               delete tempWire;
          }
          else if (temp->type() == ardSplitEditorLoop::Type)
          {
               tempLoop = qgraphicsitem_cast<ardSplitEditorLoop*>(temp);
               removeItem(temp);
               delete tempLoop;
          }
          else if (temp->type() == ardSplitEditorNode::Type)
          {
               tempNode = qgraphicsitem_cast<ardSplitEditorNode* > (temp);
               if ( tempNode->getRelation() == ardSplitEditorNode::Dependent)
               {
                    QMessageBox::warning(NULL,\
                                         tr("Error deleting"),\
                                         tr("You cannot delete a property that is dependent to the property being splitted!"));
                    tempNode->setSelected(false);
               }
               else if (tempNode->getRelation() == ardSplitEditorNode::Independent)
               {
                    QMessageBox::warning(NULL,\
                                         tr("Error deleting"),\
                                         tr("You cannot delete a property that is independent to the property being splitted!"));
                    tempNode->setSelected(false);
               }
               else
               {
                    deleteDependencies(tempNode);
                    removeItem(temp);
                    emit reloadAttsFromProperty(tempNode->parentProperty());
                    delete tempNode->parentProperty();
                    delete tempNode;
               }
          }
          selected = selectedItems();
     }
}
// -----------------------------------------------------------------------------

bool ardSplitEditorScene::existDependency(ardSplitEditorNode *_start, ardSplitEditorNode *_end)
{
     if (_start == NULL || _end == NULL)
          return false;
     QList<QGraphicsItem*> onScene = items();
     QGraphicsItem* temp;
     ardSplitEditorWire* tempWire;
     ardSplitEditorLoop* tempLoop;
     if (_start == _end)
     {
          foreach(temp,onScene)
          {
               if (temp->type() == ardSplitEditorLoop::Type)
               {
                    tempLoop = qgraphicsitem_cast<ardSplitEditorLoop*> (temp);
                    if (tempLoop->node() == _start)
                         return true;
               }
          }
     }
     else
     {
          foreach(temp,onScene)
          {
               if (temp->type() == ardSplitEditorWire::Type)
               {
                    tempWire = qgraphicsitem_cast<ardSplitEditorWire*> (temp);
                    if (tempWire->startItem() == _start && tempWire->endItem() == _end)
                         return true;
               }
          }
     }
     return false;
}
// -----------------------------------------------------------------------------

void ardSplitEditorScene::zin(void)
{
     double factor = (Settings->zoomFactorValue(MainWin->currentMode())/100.0)+1.0;
     scaleView(factor);
}
// -----------------------------------------------------------------------------

void ardSplitEditorScene::zout(void)
{
     double factor = (Settings->zoomFactorValue(MainWin->currentMode())/100.0)+1.0;
     scaleView(1./factor);
}
// -----------------------------------------------------------------------------

void ardSplitEditorScene::scaleView(qreal scaleFactor)
{
     QList<QGraphicsView*> views_list = views();

     for(int i=0;i<views_list.count();i++)
     {
          qreal factor = views_list.at(i)->matrix().scale(scaleFactor, scaleFactor).mapRect(QRectF(0, 0, 1, 1)).width();
          if (factor < 0.007 || factor > 100)
               continue;
          views_list.at(i)->scale(scaleFactor, scaleFactor);
     }
}
// -----------------------------------------------------------------------------
