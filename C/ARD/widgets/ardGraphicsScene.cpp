 /**
 *                                                                             *
 * @file MyGraphicsScene.cpp                                                   *
 * Project name: HeKatE hqed                                                   *
 * $Id: ardGraphicsScene.cpp,v 1.2.8.2 2011-08-31 18:02:15 hqeddoxy Exp $              *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 12.10.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje klasy pochodnej elementu bedacego       *
 * plotnem na ktorym rysowana jest scena                                       *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "ardGraphicsScene.h"
// -----------------------------------------------------------------------------

// #brief Konstruktor kalsy
//
// #param QObject *parent - Wskanik do rodzica
// #return Brak zwracanych wartosci
ardGraphicsScene::ardGraphicsScene(QObject* oParent) : hBaseScene(oParent)
{	
	fSelectedPropertyID = -1;
	fSelectedDependenciesID = -1;
}
// -----------------------------------------------------------------------------

// #brief Destruktor klasy, kasuje dynamicznie zaalokowana pamiec
//
// #return Brak zwracanych wartosci
ardGraphicsScene::~ardGraphicsScene(void)
{
}
// -----------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie kiedy nastapil ruch kursora nad scena
//
// #param QGraphicsSceneMouseEvent* mouseEvent - Wskanik do obiektu z parametrami zdarzenia
// #return Brak zwracanych wartosci
void ardGraphicsScene::mouseMoveEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	// Ustawiamy bierzaca scene jako aktualna
	MainWin->setCurrentARDscene(this);

	// Jezeli mamy jakis nowy przedmiot to jest on priorytetm do tego aby zawsze byl widoczny
	if(fNewItem)
	{
		fNewItem->setPos(mouseEvent->scenePos());
		if(Settings->ardEnabledAutocentering())
			for(int i=0;i<views().count();i++)
				views().at(i)->ensureVisible(fNewItem);
	}
		
	// Jezeli cos chwycilismy myszka
	if((!fNewItem) && (mouseGrabberItem()))
	{
		if(Settings->ardEnabledAutocentering())
			for(int i=0;i<views().count();i++)
				views().at(i)->ensureVisible(mouseGrabberItem());
	}
		
	QGraphicsScene::mouseMoveEvent(mouseEvent);
}
// -----------------------------------------------------------------------------

// #brief Metoda wywolywana w momencie kiedy nastapilo klikniecie na scenie
//
// #param QGraphicsSceneMouseEvent* mouseEvent - Wskanik do obiektu z parametrami zdarzenia
// #return Brak zwracanych wartosci
void ardGraphicsScene::mousePressEvent(QGraphicsSceneMouseEvent* mouseEvent)
{
	if(fNewItem)
		resetNewItem();
		
	QGraphicsScene::mousePressEvent(mouseEvent);
}
// -----------------------------------------------------------------------------

// #brief The method invoked on RMB event
//
// #param __contextMenuEvent - event parameters
void ardGraphicsScene::contextMenuEvent(QGraphicsSceneContextMenuEvent* __contextMenuEvent)
{
     QGraphicsScene::contextMenuEvent(__contextMenuEvent);
}
// -----------------------------------------------------------------------------
