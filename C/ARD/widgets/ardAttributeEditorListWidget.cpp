/**
* \file	ardAttributeEditorListWidget.cpp
* \class  ardAttributeEditorListWidget
* \author	Piotr Heldxx
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a List Widget customized for the ardAttributeChooser.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../Settings_ui.h"
#include "../../../M/ARD/ARD_Attribute.h"
#include "../../../M/ARD/ARD_AttributeList.h"

#include "ardAttributeEditorListWidget.h"
// -----------------------------------------------------------------------------

ardAttributeEditorListWidget::ardAttributeEditorListWidget(QWidget* __parent)
: QListWidget(__parent)
{
     initObject();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::loadAttributes(QList<ARD_Attribute*>* __attlist)
{
     initObject();
     _attlist = __attlist;
     showList();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::initObject(void)
{
    setSelectionMode(QAbstractItemView::ExtendedSelection);
    setAcceptDrops(true);
     _attlist = NULL;
     _filter = QList<int>() << ARD_ATT_CONCEPTUAL << ARD_ATT_PHYSICAL;
}
// -----------------------------------------------------------------------------

QList<int>* ardAttributeEditorListWidget::chosen(void)
{
     QList<int>* result = new QList<int>;
     for(int i=0;i<count();++i)
          if(item(i)->isSelected())
               result->append(item(i)->statusTip().toInt());

     return result;
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::showList(void)
{
     clear();
     if(_attlist == NULL)
          return;

     for(int i=0;i<_attlist->size();++i)
     {
          if(!_filter.contains((int)_attlist->at(i)->type()))
              continue;

          QListWidgetItem* item = new QListWidgetItem(QIcon(":/all_images/images/NewAttribute.png"), (_attlist->at(i)->name()), this, QListWidgetItem::UserType);
          item->setFlags(Qt::ItemIsEnabled  | Qt::ItemIsSelectable /*| Qt::ItemIsUserCheckable*/);
          item->setStatusTip(QString::number(i));
          addItem(item);
     }
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::setFilter(QList<int>& __filter)
{
     _filter = __filter;
     showList();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::dragEnterEvent(QDragEnterEvent* __event)
{
     if((__event->mimeData()->hasFormat("text/plain")) && (__event->source() != this))
                __event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::dropEvent(QDropEvent* __event)
{
     // Collecting data
     QString items = __event->mimeData()->text();
     QStringList indexlist = items.split(";", QString::SkipEmptyParts);

     QList<int>* sig = new QList<int>;
     for(int i=0;i<indexlist.size();++i)
     {
          bool convok;
          int val = indexlist.at(i).toInt(&convok);
          if(!convok)
               continue;

          sig->append(val);
     }
     emit elementsMoved(sig);
     delete sig;

     __event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::dragMoveEvent(QDragMoveEvent* __event)
{
     if((__event->mimeData()->hasFormat("text/plain")) && (__event->source() != this))
          __event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::mousePressEvent(QMouseEvent* __event)
{
     if (__event->button() == Qt::LeftButton)
         _dragStartPosition = __event->pos();

     QListWidget::mousePressEvent(__event);
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::mouseMoveEvent(QMouseEvent* __event)
{
     if(!(__event->buttons() & Qt::LeftButton))
          return;
     if ((__event->pos() - _dragStartPosition).manhattanLength() < QApplication::startDragDistance())
          return;
     if(itemAt(__event->pos()) == NULL)
          return;

     QList<int>* clist = chosen();
     QString mimetext = "";
     if(clist->size() > 0)
          mimetext = QString::number(clist->at(0));
     for(int i=1;i<clist->size();++i)
          mimetext += ";" + QString::number(clist->at(i));
     delete clist;

     QDrag* drag = new QDrag(this);
     QMimeData* mimeData = new QMimeData;

     mimeData->setText(mimetext);
     drag->setMimeData(mimeData);

#if QT_VERSION >= 0x040300
     drag->exec();
#elif QT_VERSION < 0x040300
     drag->start();
#endif
}
// -----------------------------------------------------------------------------

void ardAttributeEditorListWidget::keyPressEvent ( QKeyEvent * event )
{
    if ( event->key() == Qt::Key_Space && event->modifiers().testFlag(Qt::ControlModifier)\
              && event->modifiers().testFlag(Qt::ShiftModifier))
        emit doubleClicked(currentIndex());
    else
        QListWidget::keyPressEvent(event);
}
// -----------------------------------------------------------------------------
