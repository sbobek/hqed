/**
* \file     ardSplitEditorView.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardSplitEditorView definition of a View
* customized for use in ardSplitEditor dialog.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDSPLITEDITORVIEW_H
#define ARDSPLITEDITORVIEW_H
// -----------------------------------------------------------------------------

#include <QGraphicsView>
// -----------------------------------------------------------------------------

class ardSplitEditorScene;
// -----------------------------------------------------------------------------

class ardSplitEditorView : public QGraphicsView
{
     Q_OBJECT
public:

     /** \brief Standard same as QGraphicsView::QGraphicsView(). */
     explicit ardSplitEditorView(QWidget *parent = 0);

     /**
       * \brief Manages wheel events.
       * \param event - holds the parameters of the event.
       * When scrolling in with Ctrl pressed the scene performs zoom in or zoom out.
       * When scrolling with Shift pressed the scene moves left or right.
       */
     void wheelEvent(QWheelEvent *event);

     /** \brief Manages the key presses
       * On:
       *  - Qt::Key_Delete - this sends a keyDeletePressed() signal.
       */
     void keyPressEvent(QKeyEvent *event);

     /** \brief Opens a context menu.
       * It doesn't open if the mouse was over a item on scene.
       * It allows to:
       *  - "New Property" - creates a new Property under the mouse click.
       */
     void contextMenuEvent(QContextMenuEvent *event);

     /**
       * \brief Same as QGraphicsView::setScene()
       * Ensures that the scene is ardSplitEditorScene.
       */
     void setScene(ardSplitEditorScene *scene);

signals:

     /** \brief Is sent when user request for a new Property. */
     void newNodeRequested(QPointF here);

     /** \brief Is sent when the Qt::Key_Delete was pressed. */
     void keyDeletePressed();
};
// -----------------------------------------------------------------------------

#endif // ARDSPLITEDITORVIEW_H
