 /**
 *                                                                             *
 * @file ardGraphicsView.cpp                                                   *
 * Project name: HeKatE hqed                                                   *
 * $Id: ardGraphicsView.cpp,v 1.2.4.1.2.2 2011-08-31 19:25:02 hqeddoxy Exp $              *
 * Author: Krzysztof Kaczor                                                    *
 * Date: 18.12.2007                                                            *
 *                                                                             *
 * Implementation by Krzysztof Kaczor <kinio4444@gmail.com>                    *
 *                                                                             *
 * Copyright (C) 2006-9 by the HeKatE Project                                  *
 *                                                                             *
 * HQEd has been develped within the HeKatE Project,                           *
 * see http://hekate.ia.agh.edu.pl                                             *
 *                                                                             *
 * This file is part of HQEd.                                                  *
 *                                                                             *
 * HQEd is free software: you can redistribute it and/or modify                *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation, either version 3 of the License, or           *
 * (at your option) any later version.                                         *
 *                                                                             *
 * HQEd is distributed in the hope that it will be useful,                     *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with HQEd.  If not, see <http://www.gnu.org/licenses/>.               *
 *                                                                             *
 * Comment: Plik zawierajacy definicje klasy pochodnej elementu bedacego       *
 * ekranem na ktorym rysowana jest scena ARD                                   *
 *                                                                             *
 */

#include <QMessageBox>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QKeyEvent>
#include <QGraphicsScene>
#include <QGraphicsView>

#include "../../MainWin_ui.h"
#include "../../Settings_ui.h"
#include "ardGraphicsScene.h"
#include "ardGraphicsView.h"
// -----------------------------------------------------------------------------


// #brief Konstruktor klasy
//
// #param QObject *parent - Wska�nik do rodzica
// #return Brak zwracanych wartosci
ardGraphicsView::ardGraphicsView(QWidget* oParent) 
: GSceneView(oParent)
{	
	setAcceptDrops(true);
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
}
// -----------------------------------------------------------------------------

// #brief Constructs a MyGraphicsView and sets the visualized scene.
//
// #param __scene - Pointer to QGraphicsScene object which GSceneLocalizer displays.
// #param __parent - Pointer to parent object, is passed to QWidget's constructor.
ardGraphicsView::ardGraphicsView(hBaseScene* __scene, QWidget* __parent)
: GSceneView(__scene, __parent)
{
     setAcceptDrops(true);
	setAlignment(Qt::AlignLeft | Qt::AlignTop);
}
// -----------------------------------------------------------------------------

// #brief Metoda obslugujaca zdarzenie upuszczania pliku na obszar okienka, sprawdza typ danych w upuszczanym elemencie
//
// #param QDragEnterEvent *event - parametry danych upuszczanych na formularz
// #return Brak zwracanych warto�ci
void ardGraphicsView::dragEnterEvent(QDragEnterEvent *event)
{
     if(event->mimeData()->hasFormat("text/uri-list"))
		event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Metoda wobslugujaca zdarzenie upuszczania pliku na obszar okienka
//
// #param QDragEnterEvent *event - parametry danych upuszczanych na formularz
// #return Brak zwracanych warto�ci
void ardGraphicsView::dropEvent(QDropEvent *event)
{
	QList<QUrl> urlList = event->mimeData()->urls();
	if(urlList.size() > 0)
	{
		  QString url = urlList.at(0).toLocalFile();
		  MainWin->openFile(url);
	}

     event->acceptProposedAction();
} 
// -----------------------------------------------------------------------------

// #brief Metoda wywolywana w omencie poruszania kursorem z uchwyconym elementem
//
// #param QDragEnterEvent *event - parametry danych upuszczanych na formularz
// #return Brak zwracanych warto�ci
void ardGraphicsView::dragMoveEvent(QDragMoveEvent *event)
{
	event->acceptProposedAction();
}
// -----------------------------------------------------------------------------

// #brief Metoa wywolywana w momencie kiedy zostanie przekrecone kolko myszki
//
// #param QWheelEvent* event - Wska�nik do obiektu przechwoujacego informacje o zdarzeniu
// #return Brak zwracanych warto�ci
void ardGraphicsView::wheelEvent(QWheelEvent* event)
{
	if(!Settings->useZoomMouseWhell(MainWin->currentMode()))
		return;
     if (event->modifiers() == Qt::ControlModifier)
     {
          if(event->delta() < 0)
               MainWin->currentARDscene()->zin();
          if(event->delta() > 0)
               MainWin->currentARDscene()->zout();
     }
     else
     {
          QGraphicsView::wheelEvent(event);
     }
     //scene->scaleView(pow((double)2, -event->delta() / 960.0));
}
// -----------------------------------------------------------------------------
