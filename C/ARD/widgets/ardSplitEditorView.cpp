/**
* \file	ardSplitEditorView.cpp
* \class  ardSplitEditorView
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a View customized for use in ardSplitEditor dialog.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//----------------------------------------------------------------------------

#include <QKeyEvent>
#include <QMenu>
#include <QScrollBar>

#include "../../../V/ARD/ardSplitEditorNode.h"
#include "ardSplitEditorScene.h"
#include "ardSplitEditorView.h"
//----------------------------------------------------------------------------

ardSplitEditorView::ardSplitEditorView(QWidget *parent) :
          QGraphicsView(parent)
{
     setDragMode(RubberBandDrag);
}
//-----------------------------------------------------------------------

void ardSplitEditorView::wheelEvent(QWheelEvent *event)
{
     if (event->modifiers() == Qt::ControlModifier)
     {
          ardSplitEditorScene* myScene = dynamic_cast<ardSplitEditorScene*>(scene());
          if(event->delta() < 0)
               myScene->zin();
          if(event->delta() > 0)
               myScene->zout();
     }
     else if (event->modifiers() == Qt::ShiftModifier)
     {
          if(event->delta() < 0)
               horizontalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepSub);
          if(event->delta() > 0)
               horizontalScrollBar()->triggerAction(QAbstractSlider::SliderPageStepAdd);
     }
     else
     {
          QGraphicsView::wheelEvent(event);
     }
}
//-----------------------------------------------------------------------

void ardSplitEditorView::keyPressEvent(QKeyEvent *event)
{
     if(event->key() == Qt::Key_Delete)
     {
          emit keyDeletePressed();
     }
     else
          QGraphicsView::keyPressEvent(event);
}
//-----------------------------------------------------------------------

void ardSplitEditorView::contextMenuEvent(QContextMenuEvent *event)
{
     if (scene()->itemAt(mapToScene(event->pos())) != NULL)
          return ;
     QMenu menu;
     QAction *newPropertyAction = menu.addAction(tr("&New Property"));
     QAction *selectedAction = menu.exec(event->globalPos());
     if (selectedAction == newPropertyAction)
     {
          emit newNodeRequested(mapToScene(event->pos()));
     }
}
//-----------------------------------------------------------------------

void ardSplitEditorView::setScene(ardSplitEditorScene *scene)
{
     QGraphicsView::setScene(scene);
}
//----------------------------------------------------------------------------
