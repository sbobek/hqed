/**
* \file	ardPropertyEditor.cpp
* \class  ardPropertyEditor
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a dialog used for editing the list of Attributes of a given
* property.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#include "../../C/MainWin_ui.h"
#include "../../V/ARD/gARDnode.h"
#include "../../V/ARD/gARDlevel.h"
#include "../../V/ARD/gARDlevelList.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/ARD/ARD_AttributeList.h"
#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../namespaces/ns_hqed.h"

#include "widgets/ardGraphicsScene.h"
#include "ardAttributeChooser.h"
#include "ui_ardPropertyEditor.h"
#include "ardPropertyEditor.h"
// -----------------------------------------------------------------------------

ardPropertyEditor::ardPropertyEditor(gARDnode* _itemEdited, QWidget *parent) :
          QWidget(parent),
          ui(new Ui::ardPropertyEditor)
{
     setAttribute(Qt::WA_DeleteOnClose, true);

     ui->setupUi(this);

     setWindowModality(Qt::ApplicationModal);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     setWindowFlags(Qt::WindowMaximizeButtonHint);
     setWindowTitle(tr("Property Editor"));


     itemEdited = _itemEdited;
     _thisAtts =  *dynamic_cast<QList<ARD_Attribute*> *>(itemEdited->parent());
     _attchooser = new ardAttributeChooser(this);
     _attchooser->setLeftListFocus(Qt::PopupFocusReason);
     _attchooser->loadLeftAttributeList(hqed::listSubtraction(*(dynamic_cast<QList<ARD_Attribute*> * >(ardAttributeList)),_thisAtts));
     _attchooser->loadRightAttributeList(_thisAtts);

     connect(ui->pushButtonOk,SIGNAL(clicked()),this, SLOT(buttonOkClicked()));

     // Form layout
     layoutForm();
     hqed::centerWindow(this);
}
// -----------------------------------------------------------------------------

ardPropertyEditor::~ardPropertyEditor()
{
     itemEdited->setBeingEdited(false);
     delete _attchooser;
     delete ui;
}
// -----------------------------------------------------------------------------

// #brief Functions that layouts the items on the widget
//
// #return no values return
void ardPropertyEditor::layoutForm(void)
{
     mainGL = new QGridLayout;

     mainGL->addWidget(_attchooser,         0,0,1,4);
     mainGL->addWidget(ui->label_3,         1,1);
     mainGL->addWidget(ui->pushButtonCancel,1,2);
     mainGL->addWidget(ui->pushButtonOk,    1,3);
     mainGL->setColumnStretch(0, 1);

     hqed::setupLayout(mainGL);
     setLayout(mainGL);
}
// -----------------------------------------------------------------------------

void ardPropertyEditor::buttonOkClicked()
{
     if(_attchooser->rightAttributeList().isEmpty())
     {
          QMessageBox::warning(this,tr("Property Editor Error"),tr("The Edited Property cannot be empty!"));
          return ;
     }
     updateParents();
     ardPropertyList->refreshDisplay();
     ardLevelList->refreshDisplay();
     close();

}
// -----------------------------------------------------------------------------

bool ardPropertyEditor::updateParents()
{
     //Update TPH tree
     bool ok = ardPropertyList->updatePropertyWithParents(itemEdited->parent(),_attchooser->rightAttributeList());

     //The following code commented code rebuilds all of the levels apart from the edited one

     //     while(ardLevelList->size() > 1)
     //          ardLevelList->remove(0);
     //     ardLevelList->buildLevels(ardLevelList->size()-1);
     ardLevelList->gLevelList()->alignLevels(0, true);

     return ok;
}
// -----------------------------------------------------------------------------

void ardPropertyEditor::keyPressEvent(QKeyEvent *event)
{
     if (event->key() == Qt::Key_Escape)
          close();
     else if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
          buttonOkClicked();
     else QWidget::keyPressEvent(event);
}
// -----------------------------------------------------------------------------

void ardPropertyEditor::changeEvent(QEvent *e)
{
     QWidget::changeEvent(e);
     switch (e->type()) {
     case QEvent::LanguageChange:
          ui->retranslateUi(this);
          break;
     default:
          break;
     }
}
// -----------------------------------------------------------------------------
