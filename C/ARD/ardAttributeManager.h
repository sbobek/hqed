/**
* \file     ardAttributeManager.h
* \author   Krzysztof Kaczor
* \date     05.06.2011
* \version   1.0
* \brief     This file contains class ardAttributeManager definition of a widget
* used for choosing a group of Attributes from a larger group of Attributes.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDATTRIBUTEMANAGER_H
#define ARDATTRIBUTEMANAGER_H
// -----------------------------------------------------------------------------

#include <QWidget>
#include <QGridLayout>
// -----------------------------------------------------------------------------

class ARD_Attribute;

namespace Ui {
class ardAttributeManager;
}
// -----------------------------------------------------------------------------

class ardAttributeManager_ui : public QWidget
{
     Q_OBJECT

protected:

     /// \brief Called on key press
     ///
     /// \param __event - pointer to the object that handles the event parameters
     /// \return no values return
     void keyPressEvent(QKeyEvent* __event);

public:

     /// \brief Constructor of a object
     ///
     /// \param parent - pointer to the parent widget
     explicit ardAttributeManager_ui(QWidget *parent = 0);

     /// \brief Destructor
     ~ardAttributeManager_ui();

     /// \brief Populates the list of attributes
     ///
     /// \return no values return
     void populateAttributesList(void);

     /// \brief Adds a new attribute to the ARDattlist
     ///
     /// \param __name - name of the new attribute
     /// \return exit code
     /// \li 0 - on success
     /// \li 1 - when attribute with given name already exists
     int addNewArdAttribute(QString __name);

     /// \brief Rename an ARD attributet
     ///
     /// \param __oldname - the old name of the attribute
     /// \param __newname - the new name of the attribute
     /// \return exit code
     /// \li 0 - on success
     /// \li 1 - when attribute with given newname already exists
     /// \li 2 - when attribute with given old do not exists
     int renameArdAttribute(QString __oldname, QString __newname);

     /// \brief Remove an ARD attributet
     ///
     /// \param __name - name of the removed attribute
     /// \return exit code
     /// \li 0 - on success
     /// \li 1 - when attribute with given name does not exists
     int removeArdAttribute(QString __name);

public slots:

     /// \brief Called when close button is clicked
     ///
     /// \return no values return
     void closeClicked(void);

     /// \brief Called when refresh button is clicked
     ///
     /// \return no values return
     void refreshClicked(void);

     /// \brief Called when remove button is clicked
     ///
     /// \return no values return
     void removeClicked(void);

     /// \brief Called when add button is clicked
     ///
     /// \return no values return
     void addnewClicked(void);

     /// \brief Called when rename button is clicked
     ///
     /// \return no values return
     void renameClicked(void);

     /// \brief Called when the selection of the attribute is changed
     ///
     /// \param __newindex - index of the selected attribute
     /// \return no values return
     void onAttributeSelectionChange(int __newindex);

signals:

private:

     QGridLayout* mainGL;                    ///< Layout of the widget
     Ui::ardAttributeManager *ui;            ///< Pointer to the graphical interface for this widget

     /// \brief Functions that layouts the items on the widget
     ///
     /// \return no values return
     void layoutForm(void);
};
// -----------------------------------------------------------------------------

extern ardAttributeManager_ui* ardAttributeManager;
// -----------------------------------------------------------------------------

#endif // ARDATTRIBUTEMANAGER_H
