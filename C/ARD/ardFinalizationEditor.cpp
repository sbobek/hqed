/**
* \file	ardFinalizationEditor.cpp
* \class  ardFinalizationEditor
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a widget dialog used for preparing and performing
* a Finalization step.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//------------------------------------------------------------------------------------

#include <QList>
#include <QMessageBox>
#include <QKeyEvent>

#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Dependency.h"
#include "../../M/ARD/ARD_DependencyList.h"
#include "../../M/ARD/ARD_AttributeList.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../V/ARD/gARDdependency.h"
#include "../../V/ARD/gARDnode.h"
#include "../../V/ARD/gARDlevel.h"
#include "../../V/ARD/gARDlevelList.h"
#include "../../V/ARD/gTPHnode.h"
#include "../../V/ARD/gTPHtree.h"
#include "../../namespaces/ns_hqed.h"

#include "ardFinalizationEditor.h"
#include "ui_ardFinalizationEditor.h"
//------------------------------------------------------------------------------------

ardFinalizationEditor::ardFinalizationEditor(gARDnode* _itemEdited,QWidget *parent) :
     QWidget(parent),
     ui(new Ui::ardFinalizationEditor),
     itemEdited(_itemEdited)
{
     ui->setupUi(this);

     setAttribute(Qt::WA_DeleteOnClose, true);

     setWindowModality(Qt::ApplicationModal);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);

     setWindowFlags(Qt::WindowMaximizeButtonHint );

     setWindowTitle(tr("Finalization Editor"));

     // Generating a list of excluded Attributes (that is the ones that already are on the ARD Diagram).
     QList<ARD_Attribute*> _excluded;
     QList<ARD_Attribute*>* tempAttrList;
     QList<ARD_Property*>* tempPropList = dynamic_cast<QList<ARD_Property*>*>(ardPropertyList);
     foreach(ARD_Property* temp, *tempPropList)
     {
          tempAttrList = dynamic_cast<QList<ARD_Attribute*>*> (temp);
          foreach(ARD_Attribute* tempAttr, *tempAttrList)
               if(_excluded.contains(tempAttr) == false)
                    _excluded.append(tempAttr);
     }


     QList<ARD_Attribute*>* _allAtts = dynamic_cast<QList<ARD_Attribute*>*> (ardAttributeList);

     // Loading a list of possible Attributes for the Finalization.
     ui->ard_AttributeChooser->loadLeftAttributeList(hqed::listSubtraction(*_allAtts,_excluded));

     ui->label->setText(tr("Choose attributes for finalization of: ").append(itemEdited->parent()->at(0)->name()));
     ui->ard_AttributeChooser->setLeftLabel(tr("Possible attributes:"));
     ui->ard_AttributeChooser->setRightLabel(tr("Chosen attributes:"));

     connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(onOkClicked()));
     connect(ui->buttonBox,SIGNAL(rejected()),this,SLOT(close()));

}
//------------------------------------------------------------------------------------

ardFinalizationEditor::~ardFinalizationEditor()
{
     delete ui;
}
//------------------------------------------------------------------------------------

void ardFinalizationEditor::onOkClicked()
{
     if (ui->ard_AttributeChooser->rightAttributeList().isEmpty())
     {
          QMessageBox::about(this,tr("Finalization Error"),tr("Cannot finalize with no attributes chosen for the child Property."));
          return ;
     }
     // Creating and setting up the property
     ARD_Property* newProp = ardPropertyList->add();
     ARD_Property* oldProp = itemEdited->parent();
     newProp->setAttList(ui->ard_AttributeChooser->rightAttributeList());
     ardPropertyList->setTPHparent(newProp,oldProp);
     newProp->graphicTPHnode()->setTPHparent(oldProp);

     //Check if the tphRoot didn't give a NULL;
     ardPropertyList->tphTree()->createTree(ardPropertyList->tphRoot(oldProp));

     // Creating level
     ARD_Level* newLevel = ardLevelList->add();
     ARD_Level* parentLevel = ardLevelList->at(newLevel->level() - 1);

     // Setting up properties for level
     newLevel->addProperties(*(parentLevel->properties()));
     newLevel->removeProperty(oldProp);
     newLevel->addProperty(newProp);

     // Setting up dependencies
     QList<ARD_Dependency*> newDependencies = *(parentLevel->dependencies());

     ARD_Dependency* tmp;
     ARD_Dependency* newDependency;

     foreach(tmp,newDependencies)
     {
          if (tmp->independent() == oldProp || tmp->dependent() == oldProp)
          {
               newDependency = ardDependencyList->add();

               if (tmp->independent() == oldProp && tmp->dependent() == oldProp)
               {
                    newDependency->setIndependent(newProp);
                    newDependency->setDependent(newProp);
               }
               else if (tmp->independent() == oldProp)
               {
                    newDependency->setIndependent(newProp);
                    newDependency->setDependent(tmp->dependent());
               }
               else if (tmp->dependent() == oldProp)
               {
                    newDependency->setIndependent(tmp->independent());
                    newDependency->setDependent(newProp);
               }
               newLevel->addDependency(newDependency);
          }
          else
               newLevel->addDependency(tmp);
     }

     newLevel->gLevel()->placeObjects();

     ardLevelList->refreshDisplay();
     ardPropertyList->refreshDisplay();

     close();

}
//------------------------------------------------------------------------------------

void ardFinalizationEditor::keyPressEvent(QKeyEvent *event)
{
     if (event->key() == Qt::Key_Escape)
          close();
     else if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
          onOkClicked();
     else QWidget::keyPressEvent(event);
}
//------------------------------------------------------------------------------------
