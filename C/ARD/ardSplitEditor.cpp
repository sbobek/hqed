/**
* \file	ardSplitEditor.cpp
* \class  ardSplitEditor
* \author	Piotr Held
* \date 	15.03.2011
* \version	1.0
* \brief	Class definition of a dialog used to prepare and perform a Split step.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
//-------------------------------------------------------------------------------------------------------

#include <QGraphicsView>
#include <QMessageBox>
#include <QKeyEvent>

#include "../../V/ARD/ardSplitEditorNode.h"
#include "../../V/ARD/ardSplitEditorWire.h"
#include "../../V/ARD/ardSplitEditorLoop.h"
#include "widgets/ardSplitEditorScene.h"

#include "../../V/ARD/gARDnode.h"
#include "../../V/ARD/gTPHnode.h"
#include "../../V/ARD/gTPHtree.h"
#include "../../V/ARD/gARDlevel.h"
#include "../../V/ARD/gARDlevelList.h"
#include "../../M/ARD/ARD_Property.h"
#include "../../M/ARD/ARD_PropertyList.h"
#include "../../M/ARD/ARD_Attribute.h"
#include "../../M/ARD/ARD_AttributeList.h"
#include "../../M/ARD/ARD_Level.h"
#include "../../M/ARD/ARD_LevelList.h"
#include "../../M/ARD/ARD_Dependency.h"
#include "../../M/ARD/ARD_DependencyList.h"

#include "../../namespaces/ns_hqed.h"

#include "ui_ardSplitEditor.h"
#include "ardSplitEditor.h"
//-------------------------------------------------------------------------------------------------------

ardSplitEditor::ardSplitEditor(gARDnode* _itemEdited,QWidget *parent) :
     QWidget(parent),
     ui(new Ui::ardSplitEditor),
     itemEdited(_itemEdited),
     scene(new ardSplitEditorScene)
{
     ui->setupUi(this);
     setAttribute(Qt::WA_DeleteOnClose, true);

     setWindowModality(Qt::ApplicationModal);
     setWindowIcon(QIcon(":/all_images/images/mainico.png"));
     hqed::centerWindow(this);

     setWindowFlags(Qt::WindowMaximizeButtonHint );

     setWindowTitle(tr("Split Editor"));

     ui->graphicsView->setScene(scene);

     ui->gridLayout->setColumnStretch(0,5);
     ui->gridLayout->setColumnStretch(1,4);

     // Loading everything to the ui->ard_AttributeChooser.
     QList<ARD_Attribute*> *list = dynamic_cast<QList<ARD_Attribute*>*>(itemEdited->parent());
     ui->ard_AttributeChooser->loadLeftAttributeList(*list);
     ui->ard_AttributeChooser->setEnabled(false);

     ui->ard_AttributeChooser->setLeftLabel(tr("Parent's Attributes:"));
     ui->ard_AttributeChooser->setRightLabel(tr("Edited Propertie's Attributes:"));

     // Laying out the apripriate nodes
     loadDependencyPropeprties();

     ardSplitEditorNode* it1 = new ardSplitEditorNode(new ARD_Property);
     ardSplitEditorNode* it2 = new ardSplitEditorNode(new ARD_Property);

     qreal right = 30.;
     it1->moveBy( right, ui->graphicsView->height());
     it2->moveBy( right, 0. );

     scene->addNode(it1);
     scene->addNode(it2);

     scene->update();

     // Connecting the slot-signals
     connect(scene,SIGNAL(selectionChanged()),this,SLOT(onSelectionChange()));

     connect(scene,SIGNAL(reloadAttsFromProperty(ARD_Property*)),this,SLOT(reloadAttsFromProperty(ARD_Property*)));

     connect(ui->ard_AttributeChooser,SIGNAL(rightListChanged()),this,SLOT(onEditedPropertyChange()));

     connect(ui->graphicsView,SIGNAL(keyDeletePressed()),scene,SLOT(deleteSelected()));
     connect(ui->graphicsView,SIGNAL(newNodeRequested(QPointF)),scene,SLOT(addNewNodeThere(QPointF)));

     connect(ui->buttonBox,SIGNAL(rejected()),this,SLOT(close()));
     connect(ui->buttonBox,SIGNAL(accepted()),this,SLOT(onOkClicked()));

}
//-------------------------------------------------------------------------------------------------------

ardSplitEditor::~ardSplitEditor()
{
     delete ui->graphicsView->scene();
     delete ui;
}
//-------------------------------------------------------------------------------------------------------

void ardSplitEditor::loadDependencyPropeprties()
{
     // the scene takes care of the deletin of the nodes.
     QList<ARD_Dependency*>* depList= itemEdited->gParent()->parent()->dependencies();
     ARD_Property* splitProperty = itemEdited->parent();
     ARD_Dependency* tmpDep;
     // Making an dependent and independent list.
     foreach(tmpDep,*depList)
     {
          if (tmpDep->dependent() == splitProperty)
          {
               //Checking if not self-dependant
               if (tmpDep->independent() != splitProperty)
               {
                    if (independentList.contains(tmpDep->independent()) == false )
                         independentList.append(tmpDep->independent());
               }
          }
          else if (tmpDep->independent() == splitProperty)
          {
               if (dependentList.contains(tmpDep->dependent()) == false)
                    dependentList.append(tmpDep->dependent());
          }
     }

     // Loading the apriopriate nodes on scene.
     QList<ardSplitEditorNode*> nodeList;
     ardSplitEditorNode* tmpNode;
     ARD_Property* tmpProp;

     foreach(tmpProp,dependentList)
     {
         tmpNode = new ardSplitEditorNode(tmpProp);
         tmpNode->setRelation(ardSplitEditorNode::Dependent);
         nodeList.append(tmpNode);
     }

     placeDependecyProperties(nodeList,ardSplitEditorNode::Dependent);
     nodeList.clear();

     foreach(tmpProp,independentList)
     {
         tmpNode = new ardSplitEditorNode(tmpProp);
         tmpNode->setRelation(ardSplitEditorNode::Independent);
         nodeList.append(tmpNode);
     }
     placeDependecyProperties(nodeList,ardSplitEditorNode::Independent);
}
//-------------------------------------------------------------------------------------------------------

void ardSplitEditor::placeDependecyProperties(const QList<ardSplitEditorNode *> &_depList, ardSplitEditorNode::PropertyRelation type)
{
     if (type == ardSplitEditorNode::FutureChild)
          return ;
     if (_depList.isEmpty())
          return ;

     qreal totalHeight = 0;
     qreal posX;
     qreal posY = 0.0;

     ardSplitEditorNode* tmpNode;
     if (type == ardSplitEditorNode::Dependent)
     {
          posX = ui->graphicsView->width()* 3./2.;
     }
     else // type == ardSplitEditorNode::Independent
     {
          posX = -(ui->graphicsView->width()* 3./2.) ;
     }
     foreach(tmpNode, _depList)
     {
          if (tmpNode == NULL)
               continue;

          scene->addNode(tmpNode);
          tmpNode->setPos(posX,posY);

          totalHeight += tmpNode->boundingRect().height() + tmpNode->verticalMargin();
          posY = totalHeight;
     }

     scene->update();

}
//-------------------------------------------------------------------------------------------------------

void ardSplitEditor::onSelectionChange()
{
     QList<QGraphicsItem*> selected = scene->selectedItems();
     if (selected.count() == 1)
     {
          ardSplitEditorNode* node = qgraphicsitem_cast<ardSplitEditorNode*> (selected.first());
          if (node != NULL && node->getRelation() == ardSplitEditorNode::FutureChild )
          {
               selectNode(node);
          }
          else
               unselectNode();
     }
     else
          unselectNode();
}

//------------------------------------------------------------------------------------------------

void ardSplitEditor::selectNode(ardSplitEditorNode* node)
{
     if (node == NULL)
          return ;
     editedNode = node;
     QList<ARD_Attribute*>* _list = dynamic_cast<QList<ARD_Attribute*>*>(node->parentProperty());
     ui->ard_AttributeChooser->loadRightAttributeList(*_list);
     ui->ard_AttributeChooser->setEnabled(true);
}
//------------------------------------------------------------------------------------------------

void ardSplitEditor::unselectNode()
{

     QList<ARD_Attribute*> blankList;
     ui->ard_AttributeChooser->loadRightAttributeList(blankList);
     ui->ard_AttributeChooser->setEnabled(false);
     editedNode = NULL;

}
//------------------------------------------------------------------------------------------------

void ardSplitEditor::onEditedPropertyChange()
{
     if (editedNode == NULL)
          return ;
     ARD_Property* parentOfNode = editedNode->parentProperty();
     if (parentOfNode == NULL)
          return
     editedNode->prepareRectChange();
     parentOfNode->setAttList(ui->ard_AttributeChooser->rightAttributeList());
     scene->update();
}
//------------------------------------------------------------------------------------------------

void ardSplitEditor::reloadAttsFromProperty(ARD_Property *_property)
{
     if (_property == NULL)
          return ;
     QList<ARD_Attribute*> listAtt=ui->ard_AttributeChooser->leftAttributeList();
     QList<ARD_Attribute*>* propList = dynamic_cast<QList<ARD_Attribute*>*>(_property);
     foreach(ARD_Attribute* temp, *propList)
          listAtt.append(temp);
     ui->ard_AttributeChooser->loadLeftAttributeList(listAtt);
}

//------------------------------------------------------------------------------------------------

void ardSplitEditor::onOkClicked()
{
     // Checking whether all current Split Editor state passes all of the constraints.

     // Checking whether all of the Attributes of the parent Property have been used.
     if (ui->ard_AttributeChooser->leftAttributeList().isEmpty() == false)
     {
          QMessageBox::warning(this,\
                               tr("Split Error"),\
                               tr("Cannot split if not all attributes of the property that is to be split are among it's children."));
          return ;
     }

     /*
     Checking if the future child properties are empty, if yes than an error message is displayed
     and the slot ends its work.     
     */
     QList<QGraphicsItem*> onScene = scene->items();
     QList<ARD_Property*> futureChildren;
     QGraphicsItem* temp;
     ardSplitEditorNode* tempNode;
     ardSplitEditorWire* tempWire;
     ardSplitEditorLoop* tempLoop;

     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorNode::Type)
          {
               tempNode = qgraphicsitem_cast<ardSplitEditorNode*>(temp);
               if ( tempNode->getRelation() == ardSplitEditorNode::FutureChild )
               {
                    if (tempNode->parentProperty()->isEmpty())
                    {
                         QMessageBox::warning(this,\
                                              tr("Split Error"),\
                                              tr("Cannot split if some of the future child properties are empty."));
                         return ;
                    }

                    futureChildren.append(tempNode->parentProperty());
               }
          } 
     }

     // Checking if the dependencies pass the constraints.
     if (dependencyCheck() == false)
          return ;

     // Checking the connectivity of the graph on scene.
     if (connectivityCheck() == false)
          return ;
     // Setting up the properties and the level
     ARD_Property* tempProp;
     ARD_Property* newProp;
     ARD_Property* parentProp = itemEdited->parent();

     ARD_Level* newLevel = ardLevelList->add();
     ARD_Level* parentLevel = ardLevelList->at(newLevel->level() - 1);

     newLevel->addProperties(*(parentLevel->properties()));
     newLevel->removeProperty(parentProp);

     QMap<ARD_Property*,ARD_Property*> onSceneInArdLink;

     foreach(tempProp,futureChildren)
     {
          // Setting up the property
          newProp = ardPropertyList->add();

          newProp->setAttList(*(dynamic_cast<QList<ARD_Attribute*>*>(tempProp)));
          ardPropertyList->setTPHparent(newProp,parentProp);
          newProp->graphicTPHnode()->setTPHparent(parentProp);

          // Linking the property on scene with the new property that will be in the diagrams.
          onSceneInArdLink.insert(tempProp,newProp);

          // Setting up the level
          newLevel->addProperty(newProp);
     }

     // Placing everything on the TPH diagram.
     ardPropertyList->tphTree()->createTree(ardPropertyList->tphRoot(parentProp));

     // Setting up the dependencies.
     QList<ARD_Dependency*> oldDependencies = *(parentLevel->dependencies());

     ARD_Dependency* tmp;

     // Adding the dependencies that didn't change
     foreach(tmp,oldDependencies)
     {
          if (tmp->independent() != parentProp && tmp->dependent() != parentProp)
                newLevel->addDependency(tmp);
     }

     // Generating a list of new dependencies based on the ardSplitEditorWire's on scene.
     QList<ARD_Dependency*> newDependencies;
     ARD_Dependency* tempDep;

     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = dynamic_cast<ardSplitEditorWire*>(temp);
               tempDep = ardDependencyList->add();

               if (tempWire->endItem()->getRelation() == ardSplitEditorNode::FutureChild)
                    tempDep->setDependent(onSceneInArdLink.value(tempWire->endItem()->parentProperty()));
               else
                    tempDep->setDependent(tempWire->endItem()->parentProperty());

               if (tempWire->startItem()->getRelation() == ardSplitEditorNode::FutureChild)
                    tempDep->setIndependent(onSceneInArdLink.value(tempWire->startItem()->parentProperty()));
               else
                    tempDep->setIndependent(tempWire->startItem()->parentProperty());

               newDependencies.append(tempDep);
          }
          else if (temp->type() == ardSplitEditorLoop::Type)
          {
               tempLoop = dynamic_cast<ardSplitEditorLoop*>(temp);
               tempDep = ardDependencyList->add();
               tempDep->setSelfDependent(onSceneInArdLink.value(tempLoop->node()->parentProperty()));
               newDependencies.append(tempDep);
          }
     }

     // Adding the new dependencies.
     foreach(tmp,newDependencies)
     {
          newLevel->addDependency(tmp);
     }

     // Laying out the objects on the ARD diagram.
     newLevel->gLevel()->placeObjects();

     ardLevelList->refreshDisplay();
     ardPropertyList->refreshDisplay();

     close();

}
//-------------------------------------------------------------------------------------------------------

bool ardSplitEditor::dependencyCheck()
{
     QList<QGraphicsItem*> onScene = scene->items();
     QMap<ARD_Property*,bool> independentCheck;
     QMap<ARD_Property*,bool> dependentCheck;
     QGraphicsItem* temp;
     ardSplitEditorWire* tempWire;
     ARD_Property* tempProp;

     foreach(tempProp,dependentList)
          dependentCheck.insert(tempProp,false);
     foreach(tempProp,independentList)
          independentCheck.insert(tempProp,false);

     foreach(temp, onScene)
     {
          if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = qgraphicsitem_cast<ardSplitEditorWire*>(temp);
               if ( dependentList.contains(tempWire->endItem()->parentProperty()) )
               {
                    dependentCheck.find(tempWire->endItem()->parentProperty()).value() = true;
               }
               if ( independentList.contains((tempWire->startItem()->parentProperty())))
               {
                    independentCheck.find(tempWire->startItem()->parentProperty()).value() = true;
               }
          }
     }

     if ( dependentCheck.keys(false).isEmpty() == false)
     {
          QMessageBox::warning(this,\
                               tr("Split Error"),\
                               tr("Cannot split if all of the properties dependent to the property being split are not dependent to at least one child property."));
          return false;
     }
     if (independentCheck.keys(false).isEmpty() == false)
     {
          QMessageBox::warning(this,\
                               tr("Split Error"),\
                               tr("Cannot split if all of the properties independent from the property being split are not independent from at least one child property."));
          return false;
     }
     return true;
}
// -----------------------------------------------------------------------------

bool ardSplitEditor::connectivityCheck()
{
     nodeMap.clear();
     nodeList.clear();
     QList<QGraphicsItem*> onScene = scene->items();
     ardSplitEditorWire* tempWire;

     // Creating a Map of the connections and a list of all of the nodes.
     foreach(QGraphicsItem* temp, onScene)
     {
          if (temp->type() == ardSplitEditorNode::Type)
          {
               // Won't be copied two times, because each element on Scene is a separate object.
               nodeList.append(dynamic_cast<ardSplitEditorNode*>(temp));
          }
          else if (temp->type() == ardSplitEditorWire::Type)
          {
               tempWire = dynamic_cast<ardSplitEditorWire*>(temp);
               nodeMap.insertMulti(tempWire->startItem(),tempWire->endItem());
               nodeMap.insertMulti(tempWire->endItem(),tempWire->startItem());
          }
     }

     nextStep(nodeList.first());

     if (nodeList.empty() == false)
     {
          QMessageBox::warning(this,\
                               tr("Split Error"),\
                               tr("You cannot perform a Split if the graph is not connected."));
          return false;
     }

     return true;
}
// -----------------------------------------------------------------------------

void ardSplitEditor::nextStep(ardSplitEditorNode* atNode)
{
     if (nodeList.contains(atNode) == false)
          return ;
     nodeList.removeOne(atNode);
     QList<ardSplitEditorNode*> keyList = nodeMap.values(atNode);
     foreach(ardSplitEditorNode* temp,keyList)
     {
          nextStep(temp);
     }
}
// -----------------------------------------------------------------------------

void ardSplitEditor::keyPressEvent(QKeyEvent *event)
{
     if (event->key() == Qt::Key_Escape)
          close();
     else if (event->key() == Qt::Key_Enter || event->key() == Qt::Key_Return)
          onOkClicked();
     else QWidget::keyPressEvent(event);
}
//-------------------------------------------------------------------------------------------------------
