/**
* \file     ardAttributeChooser.h
* \author   Piotr Held
* \date     15.03.2011
* \version   1.0
* \brief     This file contains class ardAttributeChooser definition of a widget
* used for choosing a group of Attributes from a larger group of Attributes.
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/
// -----------------------------------------------------------------------------

#ifndef ARDATTRIBUTECHOOSER_H
#define ARDATTRIBUTECHOOSER_H
// -----------------------------------------------------------------------------

#include <QWidget>
#include <QGridLayout>
// -----------------------------------------------------------------------------

class ARD_Attribute;

namespace Ui {
class ardAttributeChooser;
}
// -----------------------------------------------------------------------------

class ardAttributeChooser : public QWidget
{
     Q_OBJECT

public:
     explicit ardAttributeChooser(QWidget *parent = 0);
     ~ardAttributeChooser();

     /// \brief Function that loads the list with Attributes.
     ///
     /// \param __attlist - pointer to the list of attributes that the element displays
     /// \return no return value     
     void loadLeftAttributeList(const QList<ARD_Attribute*>& __attlist);

     /// \brief Function that loads the list with Attributes.
     ///
     /// \param __attlist - pointer to the list of attributes that the element displays
     /// \return no return value
     void loadRightAttributeList(const QList<ARD_Attribute*>& __attlist);

     /// \brief Function that sets the Left Label to label
     ///
     /// \param label - the label that is going to be displayed above the left list.
     /// \return no return value
     void setLeftLabel(const QString& label);


     /// \brief Function that sets the right Label to label
     ///
     /// \param label - the label that is going to be displayed above the right list.
     /// \return no return value
     void setRightLabel(const QString& label);


     /// \brief Function that return a reference to the left attribute list.
     ///
     /// \return Reference to the left attribute list.
     const QList<ARD_Attribute*>& leftAttributeList() const
     { return _leftAttList; }

     /// \brief Function that return a reference to the right attribute list.
     ///
     /// \return Reference to the right attribute list.
     const QList<ARD_Attribute*>& rightAttributeList() const
     { return _rightAttList; }

     ///  \brief Sets the focus to the left list.
     void setLeftListFocus(Qt::FocusReason reason);

public slots:
     /// \brief Slot used to handle when the toolButtonAdd is clicked
     void buttonAddClicked(void);

     /// \brief Slot used to handle when the toolButtonDelete is clicked
     void buttonDeleteClicked(void);

protected slots:
    /// \brief Function that is triggered when user adds the attributes to the state schema
    ///
    /// \param __items - pointer to the list of indexes of the list items
    /// \return no values return
    void onAttsAddToProperty(QList<int>* __items);

    /// \brief Function that is triggered when user removes some attributes from the state schema
    ///
    /// \param __items - pointer to the list of indexes of the list items
    /// \return no values return
    void onAttsRemoveFromProperty(QList<int>* __items);

signals:

     /// \brief Signal that is emited when the right list contents changes.
     void rightListChanged();

private:

     QGridLayout* mainGL;                    ///< Layout of the widget
     Ui::ardAttributeChooser *ui;            ///< Pointer to the graphical interface for this widget
     QList<ARD_Attribute*> _leftAttList;     ///< The left list of attributes
     QList<ARD_Attribute*> _rightAttList;    ///< The right list of attributes

     /// \brief Functions that layouts the items on the widget
     ///
     /// \return no values return
     void layoutForm(void);
};
// -----------------------------------------------------------------------------

#endif // ARDATTRIBUTECHOOSER_H
