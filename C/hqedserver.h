/**
* \file	hqedserver.h
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	13.06.2011
* \version	1.0
* \brief	This file defines class that represents a hqedServer object
* \note This file is a part of HQEd. It has been develped within the HeKatE Project.
* HQEd is a free software that you can redistribute and/or modify
* under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License or
* (at your option) any later version.
* You should have received a copy of the GNU General Public License with HQEd.
* \note HQEd is distributed in hope that will be useful.
* It has NO ANY WARRANTY, neither implied warranty of
* MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE.
* \note Copyright (C) 2006-9 by the HeKatE Project.
* \bug No destructor.
* \see GNU General Public License for more details http://www.gnu.org/licenses/
* \see HeKatE project http://hekate.ia.agh.edu.pl
*/

#ifndef HQEDSERVER_H
#define HQEDSERVER_H
// -----------------------------------------------------------------------------

#include <QObject>
#include <QTcpServer>
// -----------------------------------------------------------------------------

#include "devPluginInterface/devIODeviceSingalHandler.h"
// -----------------------------------------------------------------------------

#define HQED_SERVER_MESSAGE_TYPE_FORMAT           0x00000200

#define HQED_SERVER_RESULT_CODE_SUCCESS           0
#define HQED_SERVER_RESULT_CODE_ERROR             1
// -----------------------------------------------------------------------------

/**
* \class 	hqedServer
* \author	Krzysztof Kaczor kinio4444@gmail.com
* \date 	13.06.2008
* \brief 	This class that represents a hqedServer object
*/
class hqedServer : public devIODeviceSingalHandler
{
     Q_OBJECT

private:

     QTcpServer* _server;          ///< Pointer to the object of TCP server

     /// \brief Returns the string denoting the success of message parsing
     ///
     /// \return the string denoting the success of message parsing
     QString interpretationSuccess(void);

     /// \brief Returns the string denoting the failed message parsing
     ///
     /// \return the string denoting the failed message parsing
     QString interpretationFailed(void);

     /// \brief Function that interprets the hello message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageHello(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the model add message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageModelAdd(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the model get message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageModelGet(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the model getlist message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageModelGetList(QStringList messageItems, int& __rcode);
     
     /// \brief Function that interprets the model exists message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageModelExists(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the model remove message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageModelRemove(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the state add message
     ///
     /// \param messageItems - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessageStateAdd(QStringList messageItems, int& __rcode);

     /// \brief Function that interprets the state remove message
	 ///
	 /// \param messageItems - the list of message elements
	 /// \param __rcode - result code
	 /// \return the message that should be sent to client as response
	 QString interpretMessageStateRemove(QStringList messageItems, int& __rcode);

     QString interpretMessageModelRun(QStringList messageItems, int& __rcode);

     /// \brief Function that parses the map command
     ///
     /// \param __parts - the string that provides information about required parts
     /// \param __rcode - result code
     /// \param __rmsg - result message
     /// \return the map that constains the information about parts for retrival. If the error occures the rmsg contains error message.
     QMap<QString, QString> parsePartsCommand(QString __parts, int& __rcode, QString& __rmsg);

     /// \brief Function that parses the state definition passed by [state, add] command
	 ///
	 /// \param __stateDef - the string that provides new state definition
	 /// \param __rcode    - result code
	 /// \param __rmsg     - result message
	 /// \return the map that constains the information about parts for retrival. If the error occures the rmsg contains error message.
	 QMap<QString, QString> parseStateDefCommand(QString __parts, int& __rcode, QString& __rmsg);

     /// \brief Creates the identifier of the model using modelname and username
     ///
     /// \param __modelname - the name of the model send by protocol
     /// \param __username - the name of the user send by protocol
     /// \return the identifier of the model using modelname and username
     QString createModelId(QString __modelname, QString __username);

     /// \brief Returns the prefix of the autogenerated model id
     ///
     /// \return the prefix of the autogenerated model id as string
     QString modelIdPrefix(void);

     /// \brief Returns the infix of the autogenerated model id
     ///
     /// \return the infix of the autogenerated model id as string
     QString modelIdInfix(void);

     /// \brief Returns the postfix of the autogenerated model id
     ///
     /// \return the postfix of the autogenerated model id as string
     QString modelIdPostfix(void);

public:

     /// \brief Default class constructor
     hqedServer(void);

     /// \brief Class destructor
     ~hqedServer(void);

     /// \brief Returns the status of the server
     ///
     /// \return true if the server is listening, otherwise false
     bool isListening(void);

     /// \brief Returns the port number on which the serwer listen
     ///
     /// \return the port number on which the serwer listen
     quint16 port(void);

     /// \brief Starts the server listening on the given port. When the server is already running it changes its port
     ///
     /// \param __port - number of the port to listen on
     /// \return no values return
     void listen(int __port);

     /// \brief Stops the server
     ///
     /// \return no values return
     void stop(void);

     /// \brief Allow to set a new last error message
     ///
     /// \param __lastError - new error message
     /// \param __errorType - type of the error.
     /// \return error code
     int setLastError(QString __lastError, int __errorType);

     /// \brief Function that parses the message specified with the help of __message.
     ///
     /// \param __message - the message that will be parsed
     /// \param rcode - result code of the function
     /// \param rmsg - result message
     /// \return list of parts of the list. Function doesnt work recursivly so some of the returned parts can be lists as well
     QStringList parseMessage(QString __message, int& rcode, QString& rmsg);

     /// \brief Function that interprets received message
     ///
     /// \param __message - the list of message elements
     /// \param __rcode - result code
     /// \return the message that should be sent to client as response
     QString interpretMessage(QString __message, int& __rcode);

     /// \brief Function that creates the PROLOG list using given argument
     ///
     /// \param __list - result code of the function
     /// \return string that is a PROLOG-formated list
     QString createList(QStringList __list);

     /// \brief Function that returns the string protocol message containing error message for the user
     ///
     /// \param __error4client - error message for client
     /// \return protocol message with given error message
     QString createErrorResponce(QString __error4client);


public slots:

     /// \brief Slot called when new connection incomes
     void onNewConnection(void);
};
// -----------------------------------------------------------------------------

extern hqedServer* hqedServerInstance;
// -----------------------------------------------------------------------------

#endif // HQEDSERVER_H
