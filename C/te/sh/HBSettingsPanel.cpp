/*
 *     $Id: HBSettingsPanel.cpp,v 1.2 2010-01-08 19:47:26 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include "HBSettingsPanel.h"
// -----------------------------------------------------------------------------

// #brief Constructor of this panel setups edited block and UI (with call to createUI()).
//
// #param parent QWidget's parent
// #param _block block pointing to edited block
// #return no values return
HBSettingsPanel::HBSettingsPanel(QWidget * parent,HighlightBlock * _block): QGroupBox(parent)
{
    block=_block;
    this->setTitle(block->getName()+" settings");
    groupLayout=new QVBoxLayout(this);
    infoSettings=new HBInfoPanel(this,block);
    groupLayout->addWidget(infoSettings);
    formatSettings=new HBFormatPanel(this,block);
    groupLayout->addWidget(formatSettings);
    if(block->type()==HighlightBlock::sl)
        patternSettings=new HBslPatternsPanel(this,block);
    else
        patternSettings=new HBmlPatternsPanel(this,block);
    groupLayout->addWidget(patternSettings);
}
// -----------------------------------------------------------------------------
