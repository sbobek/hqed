/*
 *     $Id: HSyntaxHighlighterSettings.cpp,v 1.2 2010-01-08 19:47:27 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtCore/QSettings>

#include "HSyntaxHighlighterSettings.h"
// -----------------------------------------------------------------------------

// #brief Basic constructor of settings class. It only calls setupHighlightBlocks()
HSyntaxHighlighterSettings::HSyntaxHighlighterSettings(void)
{
   modified=true;
}
// -----------------------------------------------------------------------------

// #brief Destructor We don't use any raw pointers here, so he has no job.
HSyntaxHighlighterSettings::~HSyntaxHighlighterSettings(void)
{
}
// -----------------------------------------------------------------------------

// #brief Functions saves configuration to file. This function is used to store all highlighter settings, such as matching patterns and formatting info selected ini file
//
// #param _fileName - complete path to ini file, where settings should be stored. If no specified, Applications directory and name default.ini are assumed.
// #return no values return
void HSyntaxHighlighterSettings::saveConfig(QString _fileName) const
{
     QSettings settings(_fileName,QSettings::IniFormat);
     settings.clear();
     QStringList blNames;
     foreach(SingleLineHighlightBlock block,singleLine.values())
     {
          blNames.append(block.getName());
     }
     settings.setValue("General/Single_Lines_Names",blNames.join("|"));
     blNames.clear();

     foreach(SingleLineHighlightBlock block,singleLine.values())
     {
          settings.setValue(block.getName()+"/description",block.getDescription());
          settings.setValue(block.getName()+"/pattern",block.getPattern());

          QFont font=block.getFont();
          settings.setValue(block.getName()+"/Font",font.toString());

          QString hexColor = QString::number(block.getForegroundColor().rgb(),16);
          settings.setValue(block.getName()+"/ForegroundColor", hexColor);
          hexColor = QString::number(block.getBackgroundColor().rgb(),16);
          settings.setValue(block.getName()+"/BackgroundColor", hexColor);
     }

     foreach(MultiLinesHighlightBlock block, multiLines)
     {
          blNames.append(block.getName());
     }
     settings.setValue("General/Multiple_Lines_Names",blNames.join("|"));

     foreach(MultiLinesHighlightBlock block, multiLines)
     {
          settings.setValue(block.getName()+"/description",block.getDescription());
          settings.setValue(block.getName()+"/starting_pattern",block.getStartingPattern());
          settings.setValue(block.getName()+"/ending_pattern",block.getEndingPattern());

          QFont font=block.getFont();
          settings.setValue(block.getName()+"/Font",font.toString());

          QString hexColor = QString::number(block.getForegroundColor().rgb(),16);
          settings.setValue(block.getName()+"/ForegroundColor", hexColor);
          hexColor = QString::number(block.getBackgroundColor().rgb(),16);
          settings.setValue(block.getName()+"/BackgroundColor", hexColor);
     }
}
// -----------------------------------------------------------------------------

// #brief Functions loads configuration from file. This function is used to load all highlighter settings, such as matching patterns and formatting form selected ini file
//
// #param _fileName complete path to ini file, where settings should be stored. If no specified, Applications directory and name default.ini are assumed.
// #return no values return
void HSyntaxHighlighterSettings::loadConfig(QString  _fileName)
{

     QSettings settings(_fileName,QSettings::IniFormat);
     QStringList blNames;

     blNames=settings.value("General/Single_Lines_Names").toString().split("|");
     singleLine.clear();
     foreach(QString name,blNames)
     {
          if(!name.isEmpty())
          {
               SingleLineHighlightBlock block;
               block.setName(name);
               block.setDescription(settings.value(block.getName()+"/description").toString());
               block.setPattern(settings.value(block.getName()+"/pattern").toString());

               QFont font;
               font.fromString(settings.value(block.getName()+"/Font").toString());
               block.setFont(font);

               QColor color=QColor::fromRgb(settings.value(block.getName()+"/ForegroundColor").toString().toUInt(0,16));
               block.setForegroundColor(color);
               color = QColor::fromRgb(settings.value(block.getName()+"/BackgroundColor").toString().toUInt(0,16));
               block.setBackgroundColor(color);
               singleLine[name]=block;
          }
     }

     blNames=settings.value("General/Multiple_Lines_Names").toString().split("|");
     multiLines.clear();
     foreach(QString name,blNames)
     {
          if(!name.isEmpty())
          {
               MultiLinesHighlightBlock block;
               block.setName(name);
               block.setDescription(settings.value(block.getName()+"/description").toString());
               block.setStartingPattern(settings.value(block.getName()+"/starting_pattern").toString());
               block.setEndingPattern(settings.value(block.getName()+"/ending_pattern").toString());

               QFont font;
               font.fromString(settings.value(block.getName()+"/Font").toString());
               block.setFont(font);

               QColor color=QColor::fromRgb(settings.value(block.getName()+"/ForegroundColor").toString().toUInt(0,16));
               block.setForegroundColor(color);
               color=QColor::fromRgb(settings.value(block.getName()+"/BackgroundColor").toString().toUInt(0,16));
               block.setBackgroundColor(color);

               multiLines[name]=block;
          }
     }
     modified=true;
}
// -----------------------------------------------------------------------------

// #brief Generates multiple line rules. This function generates multiple line rules for  HSyntaxHighlighter
//
// #return QVector of  mlFormatingRule class rules.
QVector<mlFormatingRule> HSyntaxHighlighterSettings::getMultiLinesRules(void) const
{
    mlFormatingRule rule;
    QVector<mlFormatingRule> result;

    foreach(MultiLinesHighlightBlock block,multiLines)
    {
        rule.beginingExp=block.getStartingExp();
        rule.endingExp=block.getEndingExp();
        rule.format=block.getFormat();
        result.append(rule);
    }
    return result;
}
// -----------------------------------------------------------------------------

// #brief Generates single line rules. This function generates single line rules for  HSyntaxHighlighter
//
// #return QVector of  slFormatingRule class rules.
QVector<slFormatingRule> HSyntaxHighlighterSettings::getSingleLineRules(void) const
{
    slFormatingRule rule;
    QVector<slFormatingRule> result;

    foreach(SingleLineHighlightBlock block,singleLine)
    {
        rule.exp=block.getExp();
        rule.format=block.getFormat();
        result.append(rule);
    }
    return result;
}
// -----------------------------------------------------------------------------

// #brief Function indicating if settings were changed, so user should opdate any cached info
//
// #return True if settings were modified.
bool HSyntaxHighlighterSettings::changed(void) const
{
     return modified;
}
// -----------------------------------------------------------------------------

// #brief Function that indicates blocks were cached cached after is called changed() will return false until next modification.
void HSyntaxHighlighterSettings::cached(void)
{
     modified=false;
}
// -----------------------------------------------------------------------------

// #brief adds new empty highlighting block to be filled by user.
//
// #param _name name of the new block
// #param _type
// #return no values return
void HSyntaxHighlighterSettings::addBlock(const QString& _name,HighlightBlock::HighlighType _type)
{
     if(!blockExists(_name))
     {
          if(_type == HighlightBlock::sl)
          {
               SingleLineHighlightBlock block;
               block.setName(_name);
               singleLine.insert(_name,block);
          }
          
          else if(_type == HighlightBlock::ml)
          {
               MultiLinesHighlightBlock block;
               block.setName(_name);
               multiLines.insert(_name,block);
          }
     }
     
     modified = true;
}
// -----------------------------------------------------------------------------

// #brief checks if block exists
//
// #param _name name of checked block
// #return true if exists, false otherwise
bool HSyntaxHighlighterSettings::blockExists(const QString& _name) const
{
     return singleLine.contains(_name)||multiLines.contains(_name);
}
// -----------------------------------------------------------------------------

// #brief Removes block if it exists
//
// #param _name Name of block to be removed
// #return no values return
void HSyntaxHighlighterSettings::removeBlock(const QString& _name)
{
     if (singleLine.contains(_name))
          singleLine.remove(_name);
     else if (multiLines.contains(_name))
          multiLines.remove(_name);

     modified=true;
}
// -----------------------------------------------------------------------------

// #brief Renames block
//
// #param _oldName - old ame of block
// #param _newName - new ame of block
// #return no values return
void HSyntaxHighlighterSettings::renameBlock(const QString& _oldName,const QString& _newName)
{
     if(singleLine.contains(_oldName))
     {
          SingleLineHighlightBlock temp=singleLine.take(_oldName);
          temp.setName(_newName);
          singleLine.insert(_newName,temp);
     }
     else if(multiLines.contains(_oldName))
     {
          MultiLinesHighlightBlock temp=multiLines.take(_oldName);
          temp.setName(_newName);
          multiLines.insert(_newName,temp);
     }
     modified=true;
}
// -----------------------------------------------------------------------------
