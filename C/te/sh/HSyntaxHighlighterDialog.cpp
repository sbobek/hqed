/*
 *     $Id: HSyntaxHighlighterDialog.cpp,v 1.5 2010-01-08 19:47:27 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include "../../../namespaces/ns_hqed.h"
#include "HSyntaxHighlighterDialog.h"
#include "HBSettingsPanel.h"
// -----------------------------------------------------------------------------

HSyntaxHighlighterDialog::HSyntaxHighlighterDialog(QWidget *parent)
: QDialog(parent)
{
     setupUi(this);
     connect(HighlightBlocks,SIGNAL(currentTextChanged(const QString &)),this,SLOT(editedItemChanged(const QString &)));
     connect(addBlockButton,SIGNAL(clicked()),this,SLOT(addNewItem()));
     connect(removeBlockButton,SIGNAL(clicked()),this,SLOT(removeCurrentItem()));
     connect(renameBlockButton,SIGNAL(clicked()),this,SLOT(renameCurrentItem()));

     layoutForm();
}
// -----------------------------------------------------------------------------

// #brief Destructor If settings were set, it deletes HighlightBlocks pointed by  slBlocks and  mlBlocks
HSyntaxHighlighterDialog::~HSyntaxHighlighterDialog(void)
{
     delete formLayout;
}
// -----------------------------------------------------------------------------

// #brief Function sets the form layout
//
// #return No return value.
void HSyntaxHighlighterDialog::layoutForm(void)
{
     resize(5,5);
     
     formLayout = new QGridLayout;
     formLayout->addWidget(HBLabel,           0, 0);
     formLayout->addWidget(HighlightBlocks,   1, 0);
     formLayout->addWidget(addBlockButton,    2, 0);
     formLayout->addWidget(renameBlockButton, 3, 0);
     formLayout->addWidget(removeBlockButton, 4, 0);
     formLayout->addWidget(blockSettings,     0, 1, 5, 3);
     formLayout->addWidget(acceptButton,      5, 2);
     formLayout->addWidget(cancelButton,      5, 3);
     formLayout->setRowStretch(1, 1);
     formLayout->setColumnStretch(1, 1);
     hqed::setupLayout(formLayout);
     setLayout(formLayout);
}
// -----------------------------------------------------------------------------

// #brief Set up initial settings. This function is used to setup initial settings of  HSyntaxHighlighterDialog, it should be used before executing dialog, if you forget about it behavior may be unexpected
//
// #param _settings Initial settings to store in  settings
// #return no values return
void HSyntaxHighlighterDialog::setSettings(HSyntaxHighlighterSettings _settings)
{
    settings = _settings;
    this->constructBlockList();
}
// -----------------------------------------------------------------------------

// #brief settings getter. This function modify current settings, by copying HighlightBlocks stored, and possibly modified in  slBlocks and  mlBlocks into settings
//
// #return Possibly modified  HSyntaxHighlighter settings.
HSyntaxHighlighterSettings HSyntaxHighlighterDialog::getSettings(void)
{
    settings.modified=true;
    return settings;
}
// -----------------------------------------------------------------------------

// #brief Function filling HighlightBlocks QListWidget with all HighlightBlocks pointed by  allBlocks
//
// #return no values return
void HSyntaxHighlighterDialog::constructBlockList(void)
{
    HighlightBlocks->clear();

    int i=0;
    foreach(const SingleLineHighlightBlock block,settings.singleLine.values())
    {
        if(!block.getName().isEmpty())
            {
                HighlightBlocks->addItem(block.getName());
                HighlightBlocks->item(i)->setToolTip(block.getDescription());
                ++i;
            }
    }
    foreach(const MultiLinesHighlightBlock block,settings.multiLines.values())
    {
        if(!block.getName().isEmpty())
            {
                HighlightBlocks->addItem(block.getName());
                HighlightBlocks->item(i)->setToolTip(block.getDescription());
                ++i;
            }
    }
}
// -----------------------------------------------------------------------------

// #brief Slot called to change edited item, after calling it destroys current groupBox and then replaces it with new appropriate  HBSettingPanel
//
// #param _name of new HighlightBlock to edit now.
// #return no values return
void HSyntaxHighlighterDialog::editedItemChanged(const QString& _name)
{
    HighlightBlock* block;
    QGroupBox* temp = blockSettings;
    formLayout->removeWidget(blockSettings);
    if(settings.singleLine.contains(_name))
        block = &(settings.singleLine[_name]);
    else
        block = &(settings.multiLines[_name]);
    blockSettings = new HBSettingsPanel(this, block);
    formLayout->addWidget(blockSettings, 0, 1, 5, 3);
    delete temp;
}
// -----------------------------------------------------------------------------

// #brief adds new block
//
// #return no values return
void HSyntaxHighlighterDialog::addNewItem(void)
{
     HSyntaxHighlighterNewBlock dialog(&settings,this);
     if (dialog.exec()==QDialog::Accepted)
     {
          constructBlockList();
          QListWidgetItem * tmp= HighlightBlocks->findItems(dialog.getName(),Qt::MatchFixedString).at(0);
          HighlightBlocks->setCurrentItem(tmp);
     }
}
// -----------------------------------------------------------------------------

// #brief removes selected block
//
// #return no values return
void HSyntaxHighlighterDialog::removeCurrentItem(void)
{
     if((HighlightBlocks->count() > 0) && (HighlightBlocks->currentRow() != -1))
     {
          QString name=HighlightBlocks->currentItem()->text();
          if (QMessageBox::Ok==QMessageBox::question(this,"Deleting block","Are you sure to delete block: "+name+"?",QMessageBox::Ok | QMessageBox::Cancel))
          {
               settings.removeBlock(name);
               constructBlockList();
          }
     }
}
// -----------------------------------------------------------------------------

// #brief renames current block
//
// #return no values return
void HSyntaxHighlighterDialog::renameCurrentItem(void)
{
     if((HighlightBlocks->count() > 0) && (HighlightBlocks->currentRow() != -1))
     {
          QString curr=HighlightBlocks->currentItem()->text();
          bool ok;
          QString text = QInputDialog::getText(this,"Renaming highlight block","Enter a new name for an "+curr+" highlight block", QLineEdit::Normal, curr, &ok);
          if (ok && !text.isEmpty())
          {
               if(curr != text)
               {
                    if(settings.blockExists(text))
                    {
                         QMessageBox::warning(this,"Error while renaming","Renaming " + curr + " to " + text + " failed, because name must be unique.");
                    }
                    else
                    {
                         settings.renameBlock(curr,text);
                         constructBlockList();
                    }
               }
          }
     }
}
// -----------------------------------------------------------------------------
