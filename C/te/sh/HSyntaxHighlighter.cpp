/*
 *     $Id: HSyntaxHighlighter.cpp,v 1.3 2010-01-08 19:47:27 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtGui/QSyntaxHighlighter>
#include <QtGui/QFileDialog>

#include "../../Settings_ui.h"
#include "HSyntaxHighlighter.h"
#include "HSyntaxHighlighterDialog.h"
// -----------------------------------------------------------------------------

// #brief Constructor. Loads default configuration into settings.
//
// #param parent Pointers to owners object
HSyntaxHighlighter::HSyntaxHighlighter(QObject * parent) : QSyntaxHighlighter(parent)
{
    settings.loadConfig();
}
// -----------------------------------------------------------------------------

// #brief Constructor. Loads default configuration into settings.
//
// #param parent Pointers to owners object and initial QTextDocument on which highlighting is applied
HSyntaxHighlighter::HSyntaxHighlighter(QTextDocument * parent) : QSyntaxHighlighter(parent)
{
    settings.loadConfig();
}
// -----------------------------------------------------------------------------

// #brief Constructor. Loads default configuration into settings.
//
// #param parent Pointers to owners object and QTextEdit which holds highlighted QTextDocument
HSyntaxHighlighter::HSyntaxHighlighter(QTextEdit * parent) : QSyntaxHighlighter(parent)
{
    settings.loadConfig();
}
// -----------------------------------------------------------------------------

// #brief Destructor. Since class store all data on stack, destructor only saves default AKA last used settings to file.
HSyntaxHighlighter::~HSyntaxHighlighter()
{
    settings.saveConfig();
}
// -----------------------------------------------------------------------------

// #brief Highlighting function. After document content is changed this function is call for all lines,
// from the current changed line to the end of document. Any line is separate block,
// so storing current state is needed for multiple line code blocks for example longer comments.
// Implementing this virtual QSyntaxHighlighter Method is needed to make it works
//
// #param text Text of a single line, that is send to this Highlighter.
// #return no values return
void HSyntaxHighlighter::highlightBlock(const QString &text)
{
     if(settings.changed())
          updateRules();
     foreach (slFormatingRule rule, sRules) 
     {
          int index = text.indexOf(rule.exp);
          while (index >= 0) 
          {
               int length = rule.exp.matchedLength();
               if(length==0)
                    break;
               setFormat(index, length, rule.format);
               index = text.indexOf(rule.exp, index + length);
          }
     }

     setCurrentBlockState(0);
     for (int i = 0; i < mRules.size(); ++i) 
     {
          int startIndex = 0;
          if (previousBlockState() != i+1) //we wasn't in this kind of Block
               startIndex = text.indexOf(mRules[i].beginingExp); //find if we are in it now

          while (startIndex >= 0)
          {
               int endIndex = text.indexOf(mRules[i].endingExp, startIndex); //find end of this HighlightBlock
               int length;
               
               if (endIndex == -1) 
               { //if we just started, or continue block, apply formatting
                    setCurrentBlockState(i+1);
                    length = text.length() - startIndex;
               } 
               else 
               {
                    length = endIndex - startIndex + mRules[i].endingExp.matchedLength();
               }
               
               setFormat(startIndex, length, mRules[i].format);
               startIndex = text.indexOf(mRules[i].beginingExp,
               startIndex + length);
               if(length==0)
                    break;
          }
     }
}
// -----------------------------------------------------------------------------

// #brief Method for (re)caching rules, must me called after settings are modified.
//
// #return no values return
void HSyntaxHighlighter::updateRules()
{
    sRules=settings.getSingleLineRules();
    mRules=settings.getMultiLinesRules();
    settings.cached();
}
// -----------------------------------------------------------------------------

// #brief Slot used to launch settings dialog for stored configuration
//
// #return no values return
void HSyntaxHighlighter::editConfig(void)
{
    HSyntaxHighlighterDialog configDialog;
    configDialog.setSettings(settings);
    configDialog.exec();
    if(configDialog.result()==QDialog::Accepted)
        settings=configDialog.getSettings();
    this->rehighlight();
}
// -----------------------------------------------------------------------------

// #brief Slot used to save settings to file
//
// #param __fileName - destination file of configuration
// #return no values return
void HSyntaxHighlighter::saveConfig(QString __fileName) const
{
    QString fileName = __fileName;
    if(!fileName.isEmpty())
        settings.saveConfig(fileName);
}
// -----------------------------------------------------------------------------

// #brief Slot used to launch Save Settings dialog for stored configuration
//
// #return no values return
void HSyntaxHighlighter::saveConfig(void) const
{
    QString fileName = QFileDialog::getSaveFileName(0,"Save highlighting settings",Settings->configurationPath(),"Config Files (*.ini)");
    saveConfig(fileName);
}
// -----------------------------------------------------------------------------

// #brief Slot used to launch Load Settings dialog for stored configuration
//
// #return no values return
void HSyntaxHighlighter::loadConfig(void)
{
    QString fileName = QFileDialog::getOpenFileName(0,"Load highlighting settings",Settings->configurationPath(),"Config Files (*.ini)");
    loadConfig(fileName);
}
// -----------------------------------------------------------------------------

// #brief Slot used to load settings for stored configuration
//
// #param __fileName - source file of configuration
// #return no values return
void HSyntaxHighlighter::loadConfig(QString __fileName)
{
    QString fileName = __fileName;
    if((!fileName.isEmpty()) && (QFile::exists(fileName)))
        settings.loadConfig(fileName);
    this->rehighlight();
}
// -----------------------------------------------------------------------------

// #brief Settings pointer.
//
// #return Pointer to be used for calling method to modify settings.
HSyntaxHighlighterSettings* HSyntaxHighlighter::config(void)
{
     return &settings;
}
// -----------------------------------------------------------------------------
