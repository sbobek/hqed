/**
 * \file     HBInfoPanel.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    This File holds declaration a class HBInfoPanel.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HBINFOPANEL_H_
#define HBINFOPANEL_H_
// -----------------------------------------------------------------------------

#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QLineEdit>
#include <QtGui/QLabel>
#include <QtGui/QTextEdit>

#include "HighlightBlock.h"
#include "HSyntaxHighlighterSettings.h"
// -----------------------------------------------------------------------------

/**
* \class      HBInfoPanel
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009
* \brief      Class specifying look and behavior of panel used to edit HighlightBlock formating
*/
class HBInfoPanel : public QGroupBox
{
    Q_OBJECT
    
private:

    HighlightBlock * block;                  ///< Currently edited block
    HSyntaxHighlighterSettings * settings;   ///< Currently edited settings(just for HSyntaxHighlighterSettings::renameBlock)
    QVBoxLayout * mainLayout;                ///< Layout organizing QGroupBox
    QTextEdit * descriptionEdit;             ///< Edit for description editing
    QLabel * descriptionLabel;               ///< Label above description Edit
    
public:

    /// \brief Constructor of this panel. Setups edited block and UI it also connects all edits with appropriate slots
    ///
    /// \param parent QWidget's parent
    /// \param _block block pointing to edited block
    /// \return no values return
    HBInfoPanel(QWidget * parent, HighlightBlock * _block);

private slots:

     /// \brief updating description in real time
     ///
     /// \return no values return
     void descriptionChanged(void);
};
// -----------------------------------------------------------------------------

#endif /* HBINFOPANEL_H_ */
