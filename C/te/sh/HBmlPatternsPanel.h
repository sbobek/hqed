/**
 * \file     HBmlPatternsPanel.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    This File holds declaration and implementation of class HBmlPatternsPanel
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HBMLPATTERNSPANEL_H_
#define HBMLPATTERNSPANEL_H_
// -----------------------------------------------------------------------------

#include <QtGui/QGroupBox>
#include <QtGui/QLabel>
#include <QtGui/QTextEdit>
#include <QtGui/QVBoxLayout>

#include "HighlightBlock.h"
// -----------------------------------------------------------------------------

/**
* \class      HBmlPatternsPanel
* \author     Krzysztof Wesołowski and Krzysztof Kaczor
* \date       22.07.2009 
* \brief      Multiple Line block pattern editing panel. Class used to display and edit both text patterns form MultiLinesHighlightBlock
*/
class HBmlPatternsPanel : public QGroupBox
{
    Q_OBJECT
    
private:

    MultiLinesHighlightBlock * block;   ///< Pointer to currently edited block
    QLabel * sPatternLabel;             ///< Label showing basic info above starting pattern edit
    QTextEdit * sPatternEdit;           ///< Plain Text Edit where starting pattern is edited
    QLabel * ePatternLabel;             ///< Label showing basic info above ending pattern edit
    QTextEdit * ePatternEdit;           ///< Plain Text Edit where ending pattern is edited
    QVBoxLayout * layout;
    
public:

    /// \brief Constructor of this panel.
    /// This constructor setups edited block and User interface.
    /// He also connects signals textChanged() form both edits with slot patternEditChanged(),
    /// so any changes made to text in editing fields affects block
    /// \param parent QWidget's parent
    /// \param _block block pointing to edited block
    HBmlPatternsPanel(QWidget * parent,HighlightBlock * _block);
    
private slots:

     /// \brief slot for updating pattern after any change
     ///
     /// \return no values return     
     void patternEditChanged(void);
};
// -----------------------------------------------------------------------------

#endif /* HBMLPATTERNSPANEL_H_ */
