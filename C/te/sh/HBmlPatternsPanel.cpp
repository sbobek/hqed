/*
 *     $Id: HBmlPatternsPanel.cpp,v 1.2 2010-01-08 19:47:26 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtGui/QMessageBox>

#include "HBmlPatternsPanel.h"
// -----------------------------------------------------------------------------

// #brief Constructor of this panel
// This constructor setups edited block and User interface.
// He also connects signal textChanged() with slot patternEditChanged(),
// so any changes made to text in editing field affects block
//
// #param parent QWidget's parent
// #param _block block pointing to edited block
HBmlPatternsPanel::HBmlPatternsPanel(QWidget * parent,HighlightBlock * _block): QGroupBox(parent)
{
     block=static_cast<MultiLinesHighlightBlock*>(_block);
     this->setTitle("Regular expression pattern settings");

     layout=new QVBoxLayout(this);
     sPatternLabel=new QLabel("Edit starting pattern",this);
     sPatternEdit=new QTextEdit("",this);
     sPatternEdit->setPlainText(block->getStartingPattern());
     ePatternLabel=new QLabel("Edit ending pattern",this);
     ePatternEdit=new QTextEdit("",this);
     ePatternEdit->setPlainText(block->getEndingPattern());

     layout->addWidget(sPatternLabel);
     layout->addWidget(sPatternEdit);
     layout->addWidget(ePatternLabel);
     layout->addWidget(ePatternEdit);

     connect(sPatternEdit,SIGNAL(textChanged()),this,SLOT(patternEditChanged()));
     connect(ePatternEdit,SIGNAL(textChanged()),this,SLOT(patternEditChanged()));
     patternEditChanged();
}
// -----------------------------------------------------------------------------

// #brief slot for updating pattern after any change
//
// #return no values return
void HBmlPatternsPanel::patternEditChanged(void)
{
    QString pattern=sPatternEdit->document()->toPlainText();
    block->setStartingPattern(pattern);
    QPalette palette=sPatternEdit->palette();
    if (QRegExp(pattern).isValid())
        palette.setColor(QPalette::Base,QColor(150,255,150));
    else
        palette.setColor(QPalette::Base,QColor(255,150,150));
    sPatternEdit->setPalette(palette);

    pattern=ePatternEdit->document()->toPlainText();
    block->setEndingPattern(pattern);
    palette=ePatternEdit->palette();
    if (QRegExp(pattern).isValid())
        palette.setColor(QPalette::Base,QColor(150,255,150));
    else
        palette.setColor(QPalette::Base,QColor(255,150,150));
    ePatternEdit->setPalette(palette);
}
// -----------------------------------------------------------------------------
