/*
 *     $Id: HSyntaxHighlighterNewBlock.cpp,v 1.2 2010-01-08 19:47:27 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtGui/QMessageBox>

#include "HSyntaxHighlighterNewBlock.h"
// -----------------------------------------------------------------------------

// #brief Constructs dialog with settings pointer
//
// #param _settings pointer to affected settings
// #param parent owner widget
// #return no values return
HSyntaxHighlighterNewBlock::HSyntaxHighlighterNewBlock(HSyntaxHighlighterSettings* _settings, QWidget *parent)
    : QDialog(parent)
{
    settings=_settings;
	ui.setupUi(this);
	connect(ui.cancelButton,SIGNAL(clicked()),this,SLOT(reject()));
	connect(ui.acceptButton,SIGNAL(clicked()),this,SLOT(addName()));

}
// -----------------------------------------------------------------------------

// #brief Default destructor
HSyntaxHighlighterNewBlock::~HSyntaxHighlighterNewBlock(void)
{
}
// -----------------------------------------------------------------------------

// #brief Checks if it is possible and creates new highlight block.
//
// #return no values return
void HSyntaxHighlighterNewBlock::addName(void)
{
     name=ui.editNameEdit->text();
     if(name.isEmpty())
     {
          QMessageBox::warning(this,"Name is null","Cannot add a new block, because block names are required to be not-null");
     }
     else if (settings->blockExists(name))
     {
          QMessageBox::warning(this,"Name already exists","Cannot add a new block, because block names are required to be unique");
     }
     else
     {
          if (ui.slRadio->isChecked())
               settings->addBlock(name,HighlightBlock::sl);
          else
               settings->addBlock(name,HighlightBlock::ml);
          accept();
     }
}
// -----------------------------------------------------------------------------

// #brief you can check used name
//
// #return Name that user entered
const QString& HSyntaxHighlighterNewBlock::getName(void)
{
     return name;
}
// -----------------------------------------------------------------------------
