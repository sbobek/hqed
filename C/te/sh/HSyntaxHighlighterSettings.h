/**
 * \file     HSyntaxHighlighterSettings.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    File contains declaration of HSyntaxHighlighterSettings class.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
// -----------------------------------------------------------------------------

#ifndef HSYNTAXHIGHLIGHTERSETTINGS_H_
#define HSYNTAXHIGHLIGHTERSETTINGS_H_
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtGui/QTextCharFormat>
#include <QtCore/QCoreApplication>

#include "HighlightBlock.h"
// -----------------------------------------------------------------------------

/**
* \struct     slFormatingRule
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      Enum containing used Single Lines highlights It's intended to make any tables/QVector containing highlight rules to be more intuitive, with indexes form this enum.
*/
struct slFormatingRule
{
    QRegExp exp;              ///< Matching Regular Expression
    QTextCharFormat format;   ///<Format applied to matched text
};
// -----------------------------------------------------------------------------

/**
* \struct     mlFormatingRule
* \author     Krzysztof Wesołowski kinio4444@gmail.com
* \date       24.04.2008
* \brief      Multiple lines formating rule. Simple struct designed to hold matching starting and ending regular expression and format of matched text together. Text matched by this rule can span over multiple lines.
*/
struct mlFormatingRule
{
    QRegExp beginingExp;   ///<Expression matching beginning of highlighted block
    QRegExp endingExp;     ///<Expression matching ending of highlighted block
    QTextCharFormat format;///<Format applied to matched text
};
// -----------------------------------------------------------------------------

/**
* \class     mlFormatingRule
* \author    Krzysztof Wesołowski kinio4444@gmail.com
* \date      24.04.2008
* \brief     Highlighting Settings. This class holds all Highlighting settings, used by HSyntaxHighlighter. It is used to generate formating rules.
* \li slFormatingRule
* \li mlFormatingRule
* \li HSyntaxHighlighterDialog
*/
class HSyntaxHighlighterSettings
{
    /*!
    * This class is friend, because HSyntaxHighlighterDialog is a visual interface,
    * and avoiding this friend will cause breaking Settings encapsulation,
    * with getters and setters given to everyone instead of this one class.
    */
    friend class HSyntaxHighlighterDialog;
    
private:

    QMap<QString,SingleLineHighlightBlock> singleLine;      ///< Map of single lines block indexed by names
    QMap<QString,MultiLinesHighlightBlock> multiLines;      ///< Map of multiple lines block indexed by names
    bool modified;                                          ///< variable indicating if settings were modified since last caching, indicated by call to cached()

public:

    /// \brief Basic constructor of settings class. It only calls setupHighlightBlocks()
    HSyntaxHighlighterSettings(void);
    
    /// \brief Destructor We don't use any raw pointers here, so he has no job.
    ~HSyntaxHighlighterSettings(void);

    /// \brief Function indicating if settings were changed, so user should opdate any cached info
    ///
    /// \return True if settings were modified.
    bool changed(void) const;

    /// \brief Function that indicates blocks were cached cached after is called changed() will return false until next modification.
    void cached(void);
    
    /// \brief Functions saves configuration to file. This function is used to store all highlighter settings, such as matching patterns and formatting info selected ini file
    ///
    /// \param _fileName - complete path to ini file, where settings should be stored. If no specified, Applications directory and name default.ini are assumed.
    /// \return no values return
    void saveConfig(QString _fileName=QCoreApplication::applicationDirPath()+"/default.ini") const;

    /// \brief Functions loads configuration from file. This function is used to load all highlighter settings, such as matching patterns and formatting form selected ini file
    ///
    /// \param _fileName complete path to ini file, where settings should be stored. If no specified, Applications directory and name default.ini are assumed.
    /// \return no values return
    void loadConfig(QString _fileName=QCoreApplication::applicationDirPath()+"/default.ini");
    
    /// \brief Generates single line rules. This function generates single line rules for  HSyntaxHighlighter
    ///
    /// \return QVector of  slFormatingRule class rules.
    QVector<slFormatingRule> getSingleLineRules(void) const;

    /// \brief Generates multiple line rules. This function generates multiple line rules for  HSyntaxHighlighter
    ///
    /// \return QVector of  mlFormatingRule class rules.
    QVector<mlFormatingRule> getMultiLinesRules(void) const;

    /// \brief adds new empty highlighting block to be filled by user.
    ///
    /// \param _name name of the new block
    /// \param _type
    /// \return no values return
    void addBlock(const QString& _name,HighlightBlock::HighlighType _type=HighlightBlock::sl);

    /// \brief checks if block exists
    ///
    /// \param _name name of checked block
    /// \return true if exists, false otherwise
    bool blockExists(const QString& _name) const;
    
    /// \brief Removes block if it exists
    ///
    /// \param _name Name of block to be removed
    /// \return no values return
    void removeBlock(const QString& _name);
    
    /// \brief Renames block
    ///
    /// \param _oldName - old ame of block
    /// \param _newName - new ame of block
    /// \return no values return
    void renameBlock(const QString& _oldName,const QString& _newName);
};
// -----------------------------------------------------------------------------

#endif /* HSYNTAXHIGHLIGHTERSETTINGS_H_ */
