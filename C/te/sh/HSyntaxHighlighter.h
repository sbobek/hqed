/**
 * \file     HSyntaxHighlighter.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    File contains declaration of HSyntaxHighlighter class.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HSYNTAXHIGHLIGHTER_H_
#define HSYNTAXHIGHLIGHTER_H_
// -----------------------------------------------------------------------------

#include "QtGui/QSyntaxHighlighter"
#include "QtGui/QMessageBox"

#include "HSyntaxHighlighterSettings.h"
// -----------------------------------------------------------------------------

/**
* \class      HSyntaxHighlighter
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      Syntax highlighter
 * This class implements simple Syntax Highlighter,
 * based on rules using QRegExp form matching parts of code to highlight,
 * and QTextCharFormat for code formating. It developed on basis of Qt's QSyntaxHighlighter,
 * which provide foundations for all Syntax Highlighting classes
 * It store all of it settings in settings
*/
class HSyntaxHighlighter: public QSyntaxHighlighter
{
    Q_OBJECT

private:

     QVector<slFormatingRule> sRules;         ///< Single line rules used to highlight (cached from settings)
     QVector<mlFormatingRule> mRules;         ///< Multiple line rules used to highlight (cached from settings)


     /// \brief Variable storing all HSyntaxHighlighter settings, used to generate sRules and mRules, easily accessible via config() method.
     HSyntaxHighlighterSettings settings;

     /// \brief Method for (re)caching rules, must me called after settings are modified.
     ///
     /// \return no values return
     void updateRules(void);

public:

     /// \brief Constructor. Loads default configuration into settings.
     ///
     /// \param parent Pointers to owners object
     HSyntaxHighlighter(QObject * parent);

     /// \brief Constructor. Loads default configuration into settings.
     ///
     /// \param parent Pointers to owners object and initial QTextDocument on which highlighting is applied
     HSyntaxHighlighter(QTextDocument * parent);

     /// \brief Constructor. Loads default configuration into settings.
     ///
     /// \param parent Pointers to owners object and QTextEdit which holds highlighted QTextDocument
     HSyntaxHighlighter(QTextEdit * parent);

     /// \brief Destructor. Since class store all data on stack, destructor only saves default AKA last used settings to file.
     ~HSyntaxHighlighter(void);

     /// \brief Highlighting function. After document content is changed this function is call for all lines,
     /// from the current changed line to the end of document. Any line is separate block,
     /// so storing current state is needed for multiple line code blocks for example longer comments.
     /// Implementing this virtual QSyntaxHighlighter Method is needed to make it works
     ///
     /// \param text Text of a single line, that is send to this Highlighter.
     /// \return no values return
     void highlightBlock(const QString &text);

     /// \brief Settings pointer.
     ///
     /// \return Pointer to be used for calling method to modify settings.
     HSyntaxHighlighterSettings * config(void);

public slots:

     /// \brief Slot used to launch settings dialog for stored configuration
     ///
     /// \return no values return
     void editConfig(void);

     /// \brief Slot used to save settings to file
     ///
     /// \param __fileName - destination file of configuration
     /// \return no values return
     void saveConfig(QString __fileName) const;
     
     /// \brief Slot used to launch Save Settings dialog for stored configuration
     ///
     /// \return no values return
     void saveConfig(void) const;

     /// \brief Slot used to launch Load Settings dialog for stored configuration
     ///
     /// \return no values return
     void loadConfig(void);
     
     /// \brief Slot used to load settings for stored configuration
     ///
     /// \param __fileName - source file of configuration
     /// \return no values return
     void loadConfig(QString __fileName);

};
#endif /* HSYNTAXHIGHLIGHTER_H_ */
