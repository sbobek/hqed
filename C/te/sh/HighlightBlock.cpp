/*
 *     $Id: HighlightBlock.cpp,v 1.2 2010-01-08 19:47:27 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include <QtGui/QMessageBox>

#include "HighlightBlock.h"
// -----------------------------------------------------------------------------

// #brief simply constructor, load default data
HighlightBlock::HighlightBlock(void)
{
format.setFontFamily("Courier New");
format.setFontPointSize(8);
format.setForeground(QColor(0,0,0));
format.setBackground(QColor(255,255,255));
}
// -----------------------------------------------------------------------------

// #brief Empty destructor because class doesn't allocate memory.
HighlightBlock::~HighlightBlock(void)
{
}
// -----------------------------------------------------------------------------

// #brief Formatting getter,
//
// #return Formatting used by this HighlightBlock
QTextCharFormat HighlightBlock::getFormat(void) const
{
     return format;
}
// -----------------------------------------------------------------------------

// #brief Formatting font getter
//
// #return currently used font
QFont HighlightBlock::getFont(void) const
{
     return format.font();
}
// -----------------------------------------------------------------------------

// #brief Formatting font setter provided for convenience, used for configuration purposes
//
// #param __font Font to be used in current format
// #return no values return
void HighlightBlock::setFont(const QFont& __font)
{
     format.setFont(__font);
}
// -----------------------------------------------------------------------------

// #brief Formatting foreground color getter provided for convenience, used for configuration purposes
//
// #return currently used foreground color
const QColor& HighlightBlock::getForegroundColor() const
{
     return format.foreground().color();
}
// -----------------------------------------------------------------------------

// #brief Formatting foreground color setter provided for convenience, used for configuration purposes
//
// #param color foreground color to be used
// #return no values return
void HighlightBlock::setForegroundColor(const QColor& color)
{
     format.setForeground(color);
}
// -----------------------------------------------------------------------------

// #brief Formatting background color getter provided for convenience, used for configuration purposes
//
// #return currently used background color
const QColor& HighlightBlock::getBackgroundColor() const
{
     return format.background().color();
}
// -----------------------------------------------------------------------------

// #brief Formatting background color setter provided for convenience, used for configuration purposes
//
// #param color background color to be used
// #return no values return
void HighlightBlock::setBackgroundColor(const QColor& color)
{
     format.setBackground(color);
}
// -----------------------------------------------------------------------------

// #brief HighlightBlock name getter
//
// #return user-friendly name of this block
QString HighlightBlock::getName(void) const
{
     return name;
}
// -----------------------------------------------------------------------------

// #brief HighlightBlock name setter
//
// #param _name new user-friendly name to be used
// #return no values return
void HighlightBlock::setName(QString _name)
{
     this->name = _name;
}
// -----------------------------------------------------------------------------

// #brief HighlightBlock description getter
//
// #return user-friendly description of this block
QString HighlightBlock::getDescription(void) const
{
     return description;
}
// -----------------------------------------------------------------------------

// #brief HighlightBlock description setter
//
// #param _description new user-friendly description to be used
// #return no values return
void HighlightBlock::setDescription(QString _description)
{
     this->description = _description;
}
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// #brief Creates Regular Expression from pattern used for caching purposes
//
// #return newly created RegularExpresion
QRegExp SingleLineHighlightBlock::getExp(void) const
{
     return QRegExp(pattern);
}
// -----------------------------------------------------------------------------

// #brief Text pattern setter used for configuration purposes
//
// #param _pattern new pattern to be used
// #return no values return
void SingleLineHighlightBlock::setPattern(QString _pattern) 
{
     pattern=_pattern;
}
// -----------------------------------------------------------------------------

// #brief function that returns type of highlihgt
//
// #return type of highlihgt
SingleLineHighlightBlock::HighlighType SingleLineHighlightBlock::type(void)
{
     return sl;
}
// -----------------------------------------------------------------------------

// #brief Text pattern getter used for configuration purposes
//
// #return current text pattern
QString SingleLineHighlightBlock::getPattern(void) const
{
     return pattern;
}
// -----------------------------------------------------------------------------
// -----------------------------------------------------------------------------

// #brief brief Creates Regular Expression from startingPattern used for caching purposes
//
// #return newly created RegularExpresion
QRegExp MultiLinesHighlightBlock::getStartingExp(void) const
{
     return QRegExp(startingPattern);
}
// -----------------------------------------------------------------------------

// #brief Creates Regular Expression from endingPattern used for caching purposes
//
// #return newly created RegularExpresion
QRegExp MultiLinesHighlightBlock::getEndingExp(void) const
{
     return QRegExp(endingPattern);
}
// -----------------------------------------------------------------------------

// #brief Text starting pattern setter used for configuration purposes
//
// #param _pattern new start pattern to be used
// #return no values return
void MultiLinesHighlightBlock::setStartingPattern(QString _pattern)
{
     startingPattern=_pattern;
}
// -----------------------------------------------------------------------------

// #brief Text ending pattern setter used for configuration purposes
//
// #param _pattern new end pattern to be used
// #return no values return
void MultiLinesHighlightBlock::setEndingPattern(QString _pattern)
{
     endingPattern=_pattern;
}
// -----------------------------------------------------------------------------

// #brief function that returns type of highlihgt
//
// #return type of highlihgt
SingleLineHighlightBlock::HighlighType MultiLinesHighlightBlock::type(void)
{
     return ml;
}
// -----------------------------------------------------------------------------

// #brief Text starting pattern getter used for configuration purposes
//
// #return current start text pattern
QString MultiLinesHighlightBlock::getStartingPattern(void) const
{
     return startingPattern;
}
// -----------------------------------------------------------------------------

// #brief Text ending pattern getter used for configuration purposes
//
// #return current end text pattern
QString MultiLinesHighlightBlock::getEndingPattern(void) const
{
     return endingPattern;
}
// -----------------------------------------------------------------------------
