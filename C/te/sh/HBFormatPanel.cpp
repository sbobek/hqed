/*
 *     $Id: HBFormatPanel.cpp,v 1.2 2010-01-08 19:47:26 kinio Exp $
 *     
 *     Implementation by Krzysztof Wesołowski and Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
// -----------------------------------------------------------------------------

#include "HBFormatPanel.h"
#include <QtGui/QFontDialog>
#include <QtGui/QPalette>
#include <QtGui/QColorDialog>
// -----------------------------------------------------------------------------=

// #brief Constructor of this panel. Setups edited block and UI it also connects all buttons with appropriate slots
//
// #param parent QWidget's parent
// #param _block block pointing to edited block
HBFormatPanel::HBFormatPanel(QWidget * parent,HighlightBlock * _block): QGroupBox(parent)
{
    block=_block;
    this->setTitle("Font and colors settings");
    mainLayout=new QVBoxLayout(this);
    editFontButton=new QPushButton("Change Font");
    fontSample=new QLabel("Sample formatted text");
    updateSample();
    mainLayout->addWidget(fontSample);

    buttonsLayout=new QHBoxLayout();
    editFontButton=new QPushButton("Change Font Style");;
    editFColorButton=new QPushButton("Change Foreground Color");
    editBColorButton=new QPushButton("Change Background Color");

    buttonsLayout->addWidget(editFontButton);
    buttonsLayout->addWidget(editFColorButton);
    buttonsLayout->addWidget(editBColorButton);

    mainLayout->addLayout(buttonsLayout);
    connect(editFontButton,SIGNAL(clicked()),this,SLOT(showFontDialog()));
    connect(editFColorButton,SIGNAL(clicked()),this,SLOT(showFColorDialog()));
    connect(editBColorButton,SIGNAL(clicked()),this,SLOT(showBColorDialog()));
}
// -----------------------------------------------------------------------------

// #brief  function used to update sample label with font, usually after font is changed.
//
// #return no values return
void HBFormatPanel::updateSample()
{
    fontSample->setFont(block->getFont());
    QPalette temp=fontSample->palette();
    temp.setColor(QPalette::WindowText,block->getForegroundColor());
    temp.setColor(fontSample->backgroundRole(),block->getBackgroundColor());
    fontSample->setPalette(temp);
    fontSample->setAutoFillBackground(true);
}
// -----------------------------------------------------------------------------

// #brief Shows QFontDialog, allowing user to edit font of current block, and then updates sample with updateSample();
//
// #return no values return
void HBFormatPanel::showFontDialog()
{
    bool ok;
    block->setFont(QFontDialog::getFont(&ok,block->getFont(),this));
    if(ok)
        updateSample();

}
// -----------------------------------------------------------------------------

// #brief Shows QColorDialog, allowing user to edit foreground color of current block, and then updates sample with updateSample();
//
// #return no values return
void HBFormatPanel::showFColorDialog()
{
    block->setForegroundColor(QColorDialog::getColor(block->getForegroundColor(),this));
    updateSample();
}
// -----------------------------------------------------------------------------

// #brief Shows QColorDialog, allowing user to edit background color of current block, and then updates sample with updateSample();
//
// #return no values return
void HBFormatPanel::showBColorDialog()
{
    block->setBackgroundColor(QColorDialog::getColor(block->getBackgroundColor(),this));
    updateSample();
}
// -----------------------------------------------------------------------------
