/**
 * \file     HSyntaxHighlighterDialog.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    File contains declaration of HSyntaxHighlighterNewBlock class.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
#ifndef HSYNTAXHIGHLIGHTERNEWBLOCK_H
#define HSYNTAXHIGHLIGHTERNEWBLOCK_H
// -----------------------------------------------------------------------------

#include <QtGui/QDialog>

#include "HSyntaxHighlighterSettings.h"
#include "ui_HSyntaxHighlighterNewBlock.h"
// -----------------------------------------------------------------------------

/**
* \class      HSyntaxHighlighterNewBlock
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009
* \brief      Class provides a simple dialog for creating new blocks
*/
class HSyntaxHighlighterNewBlock : public QDialog
{
    Q_OBJECT
    
private:

     Ui::HSyntaxHighlighterNewBlockClass ui;  ///< Auto generated GUI
     HSyntaxHighlighterSettings * settings;   ///< Pinter to settings
     QString name;                            ///< Stored name of new block
     
private slots:
     
     /// \brief Checks if it is possible and creates new highlight block.
     ///
     /// \return no values return
     void addName(void);

public:

     /// \brief Constructs dialog with settings pointer
     ///
     /// \param _settings pointer to affected settings
     /// \param parent owner widget
     /// \return no values return
     HSyntaxHighlighterNewBlock(HSyntaxHighlighterSettings* _settings, QWidget *parent = 0);
     
     /// \brief Default destructor
     ~HSyntaxHighlighterNewBlock(void);
     
     /// \brief you can check used name
     ///
     /// \return Name that user entered
     const QString& getName(void);
};
// -----------------------------------------------------------------------------

#endif // HSYNTAXHIGHLIGHTERNEWBLOCK_H
