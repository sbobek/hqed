/**
 * \file     HBslPatternsPanel.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    This File holds declaration a class HBSettingsPanel.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */

#ifndef HBSETTINGSPANEL_H_
#define HBSETTINGSPANEL_H_
// -----------------------------------------------------------------------------

#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>

#include "HighlightBlock.h"

#include "HBInfoPanel.h"
#include "HBFormatPanel.h"
#include "HBslPatternsPanel.h"
#include "HBmlPatternsPanel.h"
// -----------------------------------------------------------------------------
 
/**
* \class      HBSettingsPanel
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      This class provides GroupBox with all panels needed to edit block
*/
class HBSettingsPanel : public QGroupBox
{
    Q_OBJECT
    
private:

    HighlightBlock * block;             ///< Currently edited block pointer
    QVBoxLayout * groupLayout;          ///< Layout organizing formatSettings and patternSettings
    HBInfoPanel * infoSettings;         ///< Panel for editing informations
    HBFormatPanel * formatSettings;     ///< Panel for editing formating
    QGroupBox * patternSettings;        ///< Panel for editing pattern(s) could be HB(sl|ml)PatternsPanel
    
public:

    /// \brief Constructor of this panel setups edited block and UI (with call to createUI()).
    ///
    /// \param parent QWidget's parent
    /// \param _block block pointing to edited block
    /// \return no values return
    HBSettingsPanel(QWidget * parent,HighlightBlock * _block);
};
// -----------------------------------------------------------------------------

#endif /* HBSETTINGSPANEL_H_ */
