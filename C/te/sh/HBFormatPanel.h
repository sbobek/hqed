/**
 * \file     HBFormatPanel.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    This File holds declaration a class HBFormatPanel.
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
// -----------------------------------------------------------------------------

#ifndef HBFORMATPANEL_H_
#define HBFORMATPANEL_H_
// -----------------------------------------------------------------------------

#include <QtGui/QGroupBox>
#include <QtGui/QVBoxLayout>
#include <QtGui/QHBoxLayout>
#include <QtGui/QPushButton>
#include <QtGui/QLabel>
#include <QtGui/QSpacerItem>

#include "HighlightBlock.h"
// -----------------------------------------------------------------------------

/**
* \class      HBFormatPanel
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      Class specifying look and behavior of panel used to edit HighlightBlock formating
*/
class HBFormatPanel : public QGroupBox
{
    Q_OBJECT
    
private:

    HighlightBlock* block;             ///<Currently edited block
    QVBoxLayout* mainLayout;           ///<Layout organizing sample and buttons layout in 2 rows
    QHBoxLayout* buttonsLayout;        ///<Layout organizing buttons in columns
    QLabel* fontSample;                ///< Label with presentation of sample text using current formating
    QPushButton* editFontButton;       ///< button used to edit font
    QPushButton* editFColorButton;     ///< button used to edit foreground color
    QPushButton* editBColorButton;     ///< button used to edit background color

     /// \brief  function used to update sample label with font, usually after font is changed.
     ///
     /// \return no values return
     void updateSample(void);
    
public:


     /// \brief Constructor of this panel. Setups edited block and UI it also connects all buttons with appropriate slots
     ///
     /// \param parent QWidget's parent
     /// \param _block block pointing to edited block
     HBFormatPanel(QWidget * parent,HighlightBlock * _block);

private slots:

     /// \brief Shows QFontDialog, allowing user to edit font of current block, and then updates sample with updateSample();
     ///
     /// \return no values return
     void showFontDialog(void);

     /// \brief Shows QColorDialog, allowing user to edit foreground color of current block, and then updates sample with updateSample();
     ///
     /// \return no values return
     void showFColorDialog(void);

     /// \brief Shows QColorDialog, allowing user to edit background color of current block, and then updates sample with updateSample();
     ///
     /// \return no values return
     void showBColorDialog(void);
};
// -----------------------------------------------------------------------------

#endif /* HBFORMATPANEL_H_ */
