/**
 * \file     HSyntaxHighlighterDialog.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    File contains declaration of HSyntaxHighlighterDialog class
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
 
// -----------------------------------------------------------------------------

#ifndef HSYNTAXHIGHLIGHTERSETTINGSDIALOG_H
#define HSYNTAXHIGHLIGHTERSETTINGSDIALOG_H
// -----------------------------------------------------------------------------

#include <QtGui/QDialog>
#include <QtGui/QLineEdit>
#include <QtGui/QRadioButton>
#include <QtGui/QInputDialog>
#include <QtGui/QMessageBox>

#include "HSyntaxHighlighterSettings.h"
#include "HSyntaxHighlighterNewBlock.h"
#include "HighlightBlock.h"

#include "ui_HSyntaxHighlighterDialog.h"
// -----------------------------------------------------------------------------

/**
* \class      HSyntaxHighlighterDialog
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      Class implementing Dialog, which is used to edit HSyntaxHighlighterSettings, it contains list of all HighlightBlock s and QGroupBox with editing panel.
*/
class HSyntaxHighlighterDialog : public QDialog, private Ui_HSyntaxHighlighterDialogClass
{
    Q_OBJECT
    
private:

     //Ui::HSyntaxHighlighterDialogClass userInterface;   ///< Qt Designer Generated basic UI for this dialog
     HSyntaxHighlighterSettings settings;               ///< Local copy of settings, user edit it, and any time it can be read via getSettings()
     QGridLayout* formLayout;	                         ///< Pointer to grid layout form.

     /// \brief Copying constructor. Provided to ensure object ISN'T copied.
     HSyntaxHighlighterDialog(const HSyntaxHighlighterDialog&);

     /// \brief Function filling userInterface.HighlightBlocks QListWidget with all HighlightBlocks pointed by  allBlocks
     ///
     /// \return no values return
     void constructBlockList(void);

     /// \brief Function sets the form layout
     ///
     /// \return No return value.
     void layoutForm(void);
     
public:

     /// \brief Constructor. creates UI and initialize  slBlocks and  mlBlocks with NULL
     ///
     /// \param parent Pointer to parent QWidget, used by QDialog constructor
     HSyntaxHighlighterDialog(QWidget *parent = 0);

     /// \brief Destructor If settings were set, it deletes HighlightBlocks pointed by  slBlocks and  mlBlocks
     virtual ~HSyntaxHighlighterDialog(void);

     /// \brief settings getter. This function modify current settings, by copying HighlightBlocks stored, and possibly modified in  slBlocks and  mlBlocks into settings
     ///
     /// \return Possibly modified  HSyntaxHighlighter settings.
     HSyntaxHighlighterSettings getSettings(void);

     /// \brief Set up initial settings. This function is used to setup initial settings of  HSyntaxHighlighterDialog, it should be used before executing dialog, if you forget about it behavior may be unexpected
     ///
     /// \param _settings Initial settings to store in  settings
     /// \return no values return
     void setSettings(HSyntaxHighlighterSettings _settings);
    
private slots:

     /// \brief Slot called to change edited item, after calling it destroys current userInterface.groupBox and then replaces it with new appropriate  HBSettingPanel
     ///
     /// \param _name of new HighlightBlock to edit now.
     /// \return no values return
     void editedItemChanged(const QString& _name);

     /// \brief adds new block
     ///
     /// \return no values return
     void addNewItem(void);

     /// \brief removes selected block
     ///
     /// \return no values return
     void removeCurrentItem(void);

     /// \brief renames current block
     ///
     /// \return no values return
     void renameCurrentItem(void);

};
// -----------------------------------------------------------------------------

#endif // HSYNTAXHIGHLIGHTERSETTINGSDIALOG_H
