/**
 * \file     HighlightBlock.h
 * \author   Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
 * \date     22.07.2009 
 * \version  1.0
 * \brief    File contains declaration of HighlightBlock class
 * and its children MultiLinesHighlightBlock class, and SingleLineHighlightBlock
 * And since this classes are rather simple "containers",
 * all implementations of methods (no cpp file included)
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
// -----------------------------------------------------------------------------

#ifndef HIGHLIGHTBLOCK_H_
#define HIGHLIGHTBLOCK_H_
// -----------------------------------------------------------------------------

#include <QtCore/QString>
#include <QtGui/QTextCharFormat>
// -----------------------------------------------------------------------------

/**
* \class      HighlightBlock
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      Class representing highlighting block with its name/description and formating. Class holds generic info for all available highlight block, its name and description, used to store values in ini files and to build UI, and formatting aplied if block of code is detected.
*/
class HighlightBlock
{

private:

     QTextCharFormat format;   ///< Formatting applied to matched block
     QString name;             ///< User-friendly name of this block
     QString description;      ///< Description of this block
    
public:

     /// \brief Enum holding possible typed of Highlight block, used by HighlightBlock::type() method
     enum HighlighType
     {
          sl,//!< Indicates single line highlighting block
          ml //!< Indicates multiple lines highlighting block
     };

     /// \brief simply constructor, load default data
     HighlightBlock(void);
    
     /// \brief Empty destructor because class doesn't allocate memory.
     virtual ~HighlightBlock(void);

     /// \brief Function for indicating a sub-type of HighlighBlock
     ///
     /// \return no values return
     virtual HighlighType type(void) = 0;

     /// \brief Formatting getter,
     ///
     /// \return Formatting used by this HighlightBlock
     QTextCharFormat getFormat(void) const;

     /// \brief Formatting font getter
     ///
     /// \return currently used font
     QFont getFont(void) const;

     /// \brief Formatting font setter provided for convenience, used for configuration purposes
     ///
     /// \param __font Font to be used in current format
     /// \return no values return
     void setFont(const QFont& __font);

     /// \brief Formatting foreground color getter provided for convenience, used for configuration purposes
     ///
     /// \return currently used foreground color
     const QColor& getForegroundColor() const;

     /// \brief Formatting foreground color setter provided for convenience, used for configuration purposes
     ///
     /// \param color foreground color to be used
     /// \return no values return
     void setForegroundColor(const QColor& color);

     /// \brief Formatting background color getter provided for convenience, used for configuration purposes
     ///
     /// \return currently used background color
     const QColor& getBackgroundColor() const;

     /// \brief Formatting background color setter provided for convenience, used for configuration purposes
     ///
     /// \param color background color to be used
     /// \return no values return
     void setBackgroundColor(const QColor& color);

     /// \brief HighlightBlock name getter
     ///
     /// \return user-friendly name of this block
     QString getName(void) const;

     /// \brief HighlightBlock name setter
     ///
     /// \param _name new user-friendly name to be used
     /// \return no values return
     void setName(QString _name);

     /// \brief HighlightBlock description getter
     ///
     /// \return user-friendly description of this block
     QString getDescription(void) const;

     /// \brief HighlightBlock description setter
     ///
     /// \param _description new user-friendly description to be used
     /// \return no values return
     void setDescription(QString _description);
};
// -----------------------------------------------------------------------------

/**
* \class      SingleLineHighlightBlock
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      class providing Single Line Highlight block (Highlighting block couldn't span over multiple lines)
*/
class SingleLineHighlightBlock : public HighlightBlock
{
private:

     QString pattern; ///< String pattern used for genereting QRegExp
    
public:

     /// \brief Creates Regular Expression from pattern used for caching purposes
     ///
     /// \return newly created RegularExpresion
     QRegExp getExp(void) const;

     /// \brief Text pattern setter used for configuration purposes
     ///
     /// \param _pattern new pattern to be used
     /// \return no values return
     void setPattern(QString _pattern);

     /// \brief function that returns type of highlihgt
     ///
     /// \return type of highlihgt
     HighlighType type(void);

     /// \brief Text pattern getter used for configuration purposes
     ///
     /// \return current text pattern
     QString getPattern(void) const;
};
// -----------------------------------------------------------------------------

/**
* \class      MultiLinesHighlightBlock
* \author     Krzysztof Wesołowski and Krzysztof Kaczor kinio4444@gmail.com
* \date       22.07.2009 
* \brief      class providing Multiple Lines Highlight block (Highlighting block which can span over multiple lines)
*/
class MultiLinesHighlightBlock : public HighlightBlock
{
private:

     QString startingPattern; ///< Pattern to match beginning of block
     QString endingPattern;   ///< Pattern to match ending of block
    
public:

     /// \brief brief Creates Regular Expression from startingPattern used for caching purposes
     ///
     /// \return newly created RegularExpresion
     QRegExp getStartingExp(void) const;

     /// \brief Creates Regular Expression from endingPattern used for caching purposes
     ///
     /// \return newly created RegularExpresion
     QRegExp getEndingExp(void) const;

     /// \brief Text starting pattern setter used for configuration purposes
     ///
     /// \param _pattern new start pattern to be used
     /// \return no values return
     void setStartingPattern(QString _pattern);

     /// \brief Text ending pattern setter used for configuration purposes
     ///
     /// \param _pattern new end pattern to be used
     /// \return no values return
     void setEndingPattern(QString _pattern);

     /// \brief function that returns type of highlihgt
     ///
     /// \return type of highlihgt
     HighlighType type(void);

     /// \brief Text starting pattern getter used for configuration purposes
     ///
     /// \return current start text pattern
     QString getStartingPattern(void) const;

     /// \brief Text ending pattern getter used for configuration purposes
     ///
     /// \return current end text pattern
     QString getEndingPattern(void) const;
};
// -----------------------------------------------------------------------------

#endif /* HIGHLIGHTBLOCK_H_ */

