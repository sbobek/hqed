 /**
 * \file     TextEditorDoc.h
 * \author     Krzysztof Kaczor kinio4444@gmail.com
 * \date      25.07.2009
 * \version     1.0
 * \brief     This file contains class definition of textEditorDoc class that represents single textEditorDocument
 * \note This file is a part of HQEd. It has been develped within the HeKatE Project.
 * HQEd is a free software that you can redistribute and/or modify
 * under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License or 
 * (at your option) any later version.
 * You should have received a copy of the GNU General Public License with HQEd.
 * \note HQEd is distributed in hope that will be useful.
 * It has NO ANY WARRANTY, neither implied warranty of
 * MERCHANTABILITY nor FITNESS FOR A PARTICULAR PURPOSE. 
 * \note Copyright (C) 2006-9 by the HeKatE Project.
 * \see GNU General Public License for more details http://www.gnu.org/licenses/
 * \see HeKatE project http://hekate.ia.agh.edu.pl
 */
#ifndef TEXTEDITORDOC_H
#define TEXTEDITORDOC_H
// -----------------------------------------------------------------------------

#include <Qt>
#include <QtGui>
// -----------------------------------------------------------------------------

#define TEXT_EDITOR_MODE_TEXT      0
#define TEXT_EDITOR_MODE_PROLOG    1
#define TEXT_EDITOR_MODE_ECMA      2
#define TEXT_EDITOR_MODE_HML       3
#define TEXT_EDITOR_MODE_HMR       4
// -----------------------------------------------------------------------------

class HSyntaxHighlighter;
// -----------------------------------------------------------------------------

/**
* \class      TextEditorDoc
* \author     Krzysztof Kaczor kinio4444@gmail.com
* \date       25.07.2009
* \brief      This class definition of textEditorDoc class that represents single textEditorDocument
*/
class TextEditorDoc
{
private:

     int highlightMode;                      ///< Determines the mode of editor
     QString fFileName;                      ///< Name of current opened file.
     bool _isFile;                           ///< Determines whether current project is a file.
     HSyntaxHighlighter* _highlighter;       ///< Pointer to the document syntaxt highlighter
     QTextEdit* _document;                   ///< Pointer to the text document
     
     /// \brief Function that return name of the configuration file for given mode
     ///
     /// \param __mode - mode of editor
     /// \return name of the configuration file for given mode
     QString configFile(int __mode);
 
public:

     /// \brief Constructor TextEditorDoc class.
     TextEditorDoc(void);

     /// \brief Destructore TextEditorDoc class.
     ~TextEditorDoc(void);

     /// \brief Get current mode of editor.
     ///
     /// \return current mode if editor
     int currentMode(void);

     /// \brief Set current mode of application.
     ///
     /// \param _newMode - New mode of editor
     /// \return No return value.
     void setCurrentMode(int _newMode);
     
     /// \brief Set name of the file
     ///
     /// \param __fileName - new name if the file
     /// \return No return value.
     void setFileName(QString __fileName);
     
     /// \brief Set whethever the content comes from file
     ///
     /// \param __isFile - new status
     /// \return No return value.
     void setIsFile(bool __isFile);

     /// \brief returns if the document is modified
     ///
     /// \return True when document is changed, otherwise false.
     bool changed(void);

     /// \brief Set status of document modification.
     /// 
     /// \param _c - Status of document modification.
     /// \return No return value.
     void setChanged(bool _c);
     
     /// \brief Opens a file.
     ///
     /// \param _fileName - File name to open.
     /// \return No return value.
     void openFile(QString _fileName);

     /// \brief Save file as.
     ///
     /// \return No return value.
     void saveAs(void);

     /// \brief Save changes.
     ///
     /// \return No return value.
     void save(void);

     /// \brief Removes all the data from current document
     ///
     /// \return No return value.
     void clear(void);
     
     /// \brief Shows the configuration dialog
     ///
     /// \return No return value.
     void configMode(void);
     
     /// \brief Returns pointer to the document
     ///
     /// \return pointer to the document
     QTextEdit* doc(void);
     
     /// \brief Returns the file name of the document
     ///
     /// \return file name of the document
     QString fileName(void);
     
     /// \brief Returns whethewer document is loaded from file
     ///
     /// \return whethewer document is loaded from file
     bool isFile(void);
};
// -----------------------------------------------------------------------------

#endif
