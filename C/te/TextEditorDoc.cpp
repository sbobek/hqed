/*
 *	  $Id: TextEditorDoc.cpp,v 1.6 2010-01-08 19:47:23 kinio Exp $
 *	
 *     Implementation by Krzysztof Kaczor <kinio4444@gmail.com>
 *
 *     Copyright (C) 2006-9 by the HeKatE Project
 *
 *     HQEd has been develped within the HeKatE Project,
 *     see http://hekate.ia.agh.edu.pl
 *
 *     This file is part of HQEd.
 *
 *     HQEd is free software: you can redistribute it and/or modify
 *     it under the terms of the GNU General Public License as published by
 *     the Free Software Foundation, either version 3 of the License, or
 *     (at your option) any later version.
 *
 *     HQEd is distributed in the hope that it will be useful,
 *     but WITHOUT ANY WARRANTY; without even the implied warranty of
 *     MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *     GNU General Public License for more details.
 *
 *     You should have received a copy of the GNU General Public License
 *     along with HQEd.  If not, see <http://www.gnu.org/licenses/>.
 *
 */
#include <math.h>

#include <QList>
#include <QtGui>
#include <QDialog>
#include <QObject>
#include <QPainter>
#include <QPrinter>
#include <QMessageBox>
#include <QProcess>

#include "../Settings_ui.h"
#include "sh/HSyntaxHighlighter.h"
#include "TextEditorDoc.h"
// -----------------------------------------------------------------------------

// #brief Constructor TextEditorDoc class.
TextEditorDoc::TextEditorDoc(void)
{
     _document = new QTextEdit;
     _document->document()->setDefaultFont(QFont("Courier New", 8));
     _document->setLineWrapMode(QTextEdit::NoWrap);
     
     _highlighter = new HSyntaxHighlighter(_document);
     _isFile = false;
     fFileName = "New";
     setCurrentMode(TEXT_EDITOR_MODE_TEXT);
}
// -----------------------------------------------------------------------------

// #brief Destructore TextEditorDoc class.
TextEditorDoc::~TextEditorDoc(void)
{
     //delete _document;      // - this object is deleted automatically when tab is deleted
     delete _highlighter;
}
// -----------------------------------------------------------------------------

// #brief Returns pointer to the document
//
// #return pointer to the document
QTextEdit* TextEditorDoc::doc(void)
{
     return _document;
}
// -----------------------------------------------------------------------------

// #brief Function that return name of the configuration file for given mode
//
// #param __mode - mode of editor
// #return name of the configuration file for given mode
QString TextEditorDoc::configFile(int __mode)
{
     QString prefix = Settings->configurationPath() + "/";
     if(__mode == TEXT_EDITOR_MODE_TEXT)
          return prefix + "htetext.ini";
     else if(__mode == TEXT_EDITOR_MODE_PROLOG)
          return prefix + "htepl.ini";
     else if(__mode == TEXT_EDITOR_MODE_ECMA)
          return prefix + "hteecma.ini";
     else if(__mode == TEXT_EDITOR_MODE_HML)
          return prefix + "htehml.ini";
     else if(__mode == TEXT_EDITOR_MODE_HMR)
          return prefix + "htehmr.ini";
          
     return "";
}
// -----------------------------------------------------------------------------

// #brief Get current mode of editor.
//
// #return current mode if editor
int TextEditorDoc::currentMode(void)
{
     return highlightMode;
}
// -----------------------------------------------------------------------------

// #brief Set current mode of application.
//
// #param _newMode - New mode of editor
// #return No return value.
void TextEditorDoc::setCurrentMode(int _newMode)
{
     if((_newMode != TEXT_EDITOR_MODE_TEXT) && (_newMode != TEXT_EDITOR_MODE_PROLOG) && (_newMode != TEXT_EDITOR_MODE_ECMA) && (_newMode != TEXT_EDITOR_MODE_HML) && (_newMode != TEXT_EDITOR_MODE_HMR))
          return;
          
     _highlighter->saveConfig(configFile(highlightMode));
     _highlighter->loadConfig(configFile(_newMode));
     highlightMode = _newMode;
}
// -----------------------------------------------------------------------------

// #brief Set name of the file
//
// #param __fileName - new name if the file
// #return No return value.
void TextEditorDoc::setFileName(QString __fileName)
{
     fFileName = __fileName;
}
// -----------------------------------------------------------------------------

// #brief Set whethever the content comes from file
//
// #param __isFile - new status
// #return No return value.
void TextEditorDoc::setIsFile(bool __isFile)
{
     _isFile = __isFile;
}
// -----------------------------------------------------------------------------

// #brief returns if the document is modified
//
// #return True when document is changed, otherwise false.
bool TextEditorDoc::changed(void)
{
     return _document->document()->isModified();
}
// -----------------------------------------------------------------------------

// #brief Set status of document modification.
// 
// #param _c - Status of document modification.
// #return No return value.
void TextEditorDoc::setChanged(bool _c)
{
     _document->document()->setModified(_c);
}
// -----------------------------------------------------------------------------

// #brief Opens a file.
//
// #param _fileName - File name to open.
// #return No return value.
void TextEditorDoc::openFile(QString /*_fileName*/)
{
}
// -----------------------------------------------------------------------------

// #brief Save file as.
//
// #return No return value.
void TextEditorDoc::saveAs(void)
{
}
// -----------------------------------------------------------------------------

// #brief Save changes.
//
// #return No return value.
void TextEditorDoc::save(void)
{
}
// -----------------------------------------------------------------------------

// #brief Removes all the data from current document
//
// #return No return value.
void TextEditorDoc::clear(void)
{
     _document->clear();
}
// -----------------------------------------------------------------------------

// #brief Returns the file name of the document
//
// #return file name of the document
QString TextEditorDoc::fileName(void)
{
     return fFileName;
}
// -----------------------------------------------------------------------------

// #brief Returns whethewer document is loaded from file
//
// #return whethewer document is loaded from file
bool TextEditorDoc::isFile(void)
{    
     return _isFile;
}
// -----------------------------------------------------------------------------

// #brief Shows the configuration dialog
//
// #return No return value.
void TextEditorDoc::configMode(void)
{
     _highlighter->editConfig();
     _highlighter->saveConfig(configFile(currentMode()));
}
// -----------------------------------------------------------------------------
